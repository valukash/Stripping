

2020-03-17 Stripping v14r6
========================================

Release for the 2017 pNe full restripping (S33r2)
----------------------------------------

Based on Phys v25r10p2.
This version is released on 2018-patches branch.
Release notes are generated relative to Stripping v14r1p2.


- Fix compilation warnings, !1374 (@cattanem)   
  Exposed by change in compilation flags in the nightlies  
  (cherry picked from commit 4a769e1dfbd60277a5ff69210eeff1bc31f29cf9)  
    
  To be cherry-picked to `stripping24r2-28r2-patches` branch


- Stripping v14r6 tag, !1391 (@atully)   
  

- Dbase archives s33r2, !1389 (@atully)   
  

- Add IFT, !1384 (@nskidmor)   
  

- Prepare S33r2 on 2018-patches, !1383 (@atully)   
  

- LBOPG-114, !1355 (@nskidmor) [LBOPG-114]  
  
