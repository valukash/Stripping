#!/usr/bin/env python
###############################################################################
# (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################

#
# StrippingConfigurableUser
#

__all__ = ( 
	    'StrippingConfigurableUser'
	  )

from GaudiConf.Configuration import *
from Configurables import LHCbConfigurableUser

class StrippingConfigurableUser ( LHCbConfigurableUser ) :

    def getProps(self) :
        """
	From HltLinesConfigurableUser
	@todo Should be shared between Hlt and stripping
	"""
	d = dict()
	for (k,v) in self.getDefaultProperties().iteritems() :
    	    d[k] = getattr(self,k) if hasattr(self,k) else v
	return d
