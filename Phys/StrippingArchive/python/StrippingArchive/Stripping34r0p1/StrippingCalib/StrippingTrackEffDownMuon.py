###############################################################################
# (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
## #####################################################################
# A stripping selection for Downstream J/psi->mu+mu- decays
# To be used for tracking studies
#
# @authors G. Krocker, P. Seyfert, S. Wandernoth
# @date 2010-Aug-17
#
# @authors P. Seyfert, A. Jaeger
# @date 2011-Mar-17
#
# @author M. Kolpin
# @date 2015-Mar-23
#
# @author R. Kopecna
# @date 2019-Jan-25
#
#######################################################################

__author__ = ['Renata Kopecna']
__date__ = '25/01/2019'
__version__ = '$Revision: 2.0 $'

__all__ = ('StrippingTrackEffDownMuonConf',
           'default_config',
	   'selMuonPParts',
           'makeMyMuons',
           'DownJPsi',
            'makeResonanceMuMuTrackEff',
           'DownZ',
           'DownUpsilon',
           'longTrackFilter',
           'chargeFilter',
           'selHlt1Jpsi',
           'selHlt2Jpsi',
           'trackingDownPreFilter'
           )

#FIXME Write includes in a cleaner way
from Gaudi.Configuration import *
from LHCbKernel.Configuration import *
from DAQSys.DecoderClass import Decoder
from Configurables import GaudiSequencer
from Configurables import UnpackTrack, ChargedProtoParticleMaker, DelegatingTrackSelector, TrackSelector, BestPIDParticleMaker
from TrackFitter.ConfiguredFitters import ConfiguredFit
from Configurables import TrackStateInitAlg
from StrippingConf.StrippingLine import StrippingLine
from Configurables import ChargedProtoParticleAddMuonInfo, MuonIDAlgLite
from MuonID import ConfiguredMuonIDs
from Configurables import ChargedProtoCombineDLLsAlg, ProtoParticleMUONFilter
from PhysSelPython.Wrappers import Selection, DataOnDemand
from GaudiConfUtils.ConfigurableGenerators import FilterDesktop, CombineParticles
from StandardParticles import StdLooseMuons

from StrippingUtils.Utils import LineBuilder
from Configurables import LoKi__VoidFilter
from os import environ
from Gaudi.Configuration import *
import GaudiKernel.ProcessJobOptions
from GaudiKernel.SystemOfUnits import mm

from GaudiConfUtils.ConfigurableGenerators import TisTosParticleTagger
from StandardParticles import StdAllLooseMuons
from Configurables import GaudiSequencer
from Configurables import TrackToDST
from Configurables import TrackSys
from Configurables import PatSeeding, PatSeedingTool, PatLongLivedTracking
from PhysSelPython.Wrappers import AutomaticData
# Get the fitters
from TrackFitter.ConfiguredFitters import ConfiguredFit, ConfiguredFitSeed, ConfiguredFitDownstream
from PatAlgorithms import PatAlgConf

from SelPy.utils import ( UniquelyNamedObject,
                          ClonableObject,
                          SelectionBase )


default_config = {
    'NAME'        : 'TrackEffDownMuon',
    'WGs'         : ['Calib'],
    'BUILDERTYPE' : 'StrippingTrackEffDownMuonConf',
    'CONFIG'      : {
            #Mother paramteres #TODO organize here
                	'JpsiPt'		: 0.        #GeV
		,	'JpsiMassPreComb'	: 2000.     #MeV
		,	'JpsiMassPostComb'	: 200.      #MeV
		,	'JpsiDoca'		: 5.        #mm
		,	'JpsiVertexChi2'	: 5.        #adimensional
		,	'UpsilonMassPreComb'	: 100000.   #MeV
		,	'UpsilonMassPostComb'	: 1500.     #MeV TODO: this was set to zero?!
		,	'UpsilonDoca'		: 5.        #mm
		,	'UpsilonVertexChi2'	: 25.       #adimensional
		,	'ZMassPreComb'		: 100000.   #MeV
		,	'ZMassPostComb'		: 1500.     #MeV
		,	'ZDoca'			: 5.        #mm
		,	'ZVertexChi2'		: 25.       #adimensional
            #Mu parameters
 		,	'SeedingMinP'		: 1500.      #MeV
		,	'DataType'		: '2011'     #TODO
		,	'TrChi2'		: 10.        #adimensional
		,	'MuMom'			: 2.         #GeV
		,	'MuTMom'		: 0.2        #GeV
		,	'UpsilonMuMom'		: 0.         #GeV
		,	'UpsilonMuTMom'		: 0.5        #GeV
		,	'ZMuMinEta'		: 2.         #adimensional
		,	'ZMuMaxEta'		: 4.5        #adimensional
		,	'ZMuMom'		: 0.         #GeV
		,	'ZMuTMom'		: 20.        #GeV
            #Probe parameters
		,       'JpsiProbePt'          	: 0.5    #GeV
		,       'JpsiProbeP'           	: 5.     #GeV
		,       'JpsiProbeTrackChi2'   	: 10.    #adimensional
            #Tag parameters
		,       'JpsiTagPt'            	: 0.7    #GeV
		,       'JpsiTagP'             	: 5.     #GeV
		,       'JpsiTagMinIP'         	: 0.5    #mm
		,       'JpsiTagPID'           	: -2.    #adimensional
		,       'JpsiTagTrackChi2'      : 10.    #adimensional
            #Prescale
		,	'NominalLinePrescale'    : 0.2 # proposal: 0.2 to stay below 0.15% retention rate
		,	'NominalLinePostscale'   : 1.
		,	'ValidationLinePrescale' : 0.0015 # 0.5 in stripping15: 0.1 gives 1.42% retention rate , ValidationLine further prescaled
		,	'ValidationLinePostscale': 1.
		,	'JpsiLinePrescale'       : 1. # proposal: 0.2 to stay below 0.15% retention rate
		,	'JpsiLinePostscale'      : 1.
		,	'ZLinePrescale'          : 1. # proposal: 0.2 to stay below 0.15% retention rate
		,	'ZLinePostscale'         : 1.
		,	'UpsilonLinePrescale'    : 1. # proposal: 0.2 to stay below 0.15% retention rate
		,	'UpsilonLinePostscale'   : 1.
            #Trigger
		,   'JpsiHlt1Filter'        : 'Hlt1.*Decision'
            	,   'JpsiHlt2Filter'        : 'Hlt2.*Decision'
		,	'HLT1TisTosSpecs'       : { "Hlt1TrackMuonDecision%TOS" : 0, "Hlt1SingleMuonNoIPDecision%TOS" : 0} #no reg. expression allowed(see selHlt1Jpsi )
		,	'ZHLT1TisTosSpecs'      : { "Hlt1SingleMuonHighPTDecision%TOS" : 0} #no reg. expression allowed(see selHlt1Jpsi )
		,	'UpsilonHLT1TisTosSpecs': { "Hlt1SingleMuonHighPTDecision%TOS" : 0} #no reg. expression allowed(see selHlt1Jpsi )
		,	'HLT1PassOnAll'         : True
		,	'HLT2TisTosSpecs'       : { "Hlt2SingleMuon.*Decision%TOS" : 0, "Hlt2TrackEffDiMuonDownstream.*Decision%TOS" : 0 } #reg. expression allowed
		,	'ZHLT2TisTosSpecs'      : { "Hlt2SingleMuonHighPTDecision%TOS" : 0 } #reg. expression allowed
		,	'UpsilonHLT2TisTosSpecs': { "Hlt2SingleMuonLowPTDecision%TOS" : 0} #reg. expression allowed
		,	'HLT2PassOnAll'        : False
    },
    'STREAMS'     : { 'Dimuon' : ['StrippingTrackEffDownMuonNominalLine'
	                           ,'StrippingTrackEffDownMuonValidationLine'
                                   ,'StrippingTrackEffDownMuonLine1'
                                   ,'StrippingTrackEffDownMuonLine2'
                                  #                                   ,'StrippingTrackEffDownMuonZLine'
                                   ,'StrippingTrackEffDownMuonUpsilonLine']
                               }
    }


class StrippingTrackEffDownMuonConf(LineBuilder):
    """
    Definition of Downstream J/Psi stripping.
    """

    __configuration_keys__ = default_config['CONFIG'].keys()

    def __init__(self, name, config) :
          LineBuilder.__init__(self, name, config)
          nominal_name = name + 'Nominal'
          valid_name = name + 'Validation'
          Z_name = name + 'Z'
          Upsilon_name = name + 'Upsilon'

          self.TisTosPreFilter1Jpsi = selHlt1Jpsi('TisTosFilter1Jpsi'+name, HLT1TisTosSpecs = config['HLT1TisTosSpecs'], HLT1PassOnAll = config['HLT1PassOnAll'])
          self.TisTosPreFilter2Jpsi = selHlt2Jpsi('TisTosFilter2Jpsi'+name, hlt1Filter = self.TisTosPreFilter1Jpsi, HLT2TisTosSpecs = config['HLT2TisTosSpecs'], HLT2PassOnAll = config['HLT2PassOnAll'])

          self.TisTosPreFilter1Z = selHlt1Jpsi('TisTosFilter1Z'+name, HLT1TisTosSpecs = config['ZHLT1TisTosSpecs'], HLT1PassOnAll = config['HLT1PassOnAll'])
          self.TisTosPreFilter2Z = selHlt2Jpsi('TisTosFilter2Z'+name, hlt1Filter = self.TisTosPreFilter1Z, HLT2TisTosSpecs = config['ZHLT2TisTosSpecs'], HLT2PassOnAll = config['HLT2PassOnAll'])

          self.TisTosPreFilter1Upsilon = selHlt1Jpsi('TisTosFilter1Upsilon'+name, HLT1TisTosSpecs = config['UpsilonHLT1TisTosSpecs'], HLT1PassOnAll = config['HLT1PassOnAll'])
          self.TisTosPreFilter2Upsilon = selHlt2Jpsi('TisTosFilter2Upsilon'+name, hlt1Filter = self.TisTosPreFilter1Upsilon, HLT2TisTosSpecs = config['UpsilonHLT2TisTosSpecs'], HLT2PassOnAll = config['HLT2PassOnAll'])

          self.TrackingPreFilter = trackingDownPreFilter(nominal_name, self.TisTosPreFilter2Jpsi, config['SeedingMinP'])
          self.DownMuProtoPFilter = selMuonPParts(nominal_name, config['DataType'], self.TrackingPreFilter)
          self.DownMuPFilter = makeMyMuons(nominal_name, self.DownMuProtoPFilter)
          self.DownJpsiFilter = DownJPsi( 'DownJpsiSel'+nominal_name, self.DownMuPFilter, config['TrChi2'],
          		config['MuTMom'], config['MuMom'], config['JpsiMassPreComb'], config['JpsiDoca'],
          		config['JpsiMassPostComb'], config['JpsiVertexChi2'] )

          self.ZTrackingPreFilter = trackingDownPreFilter( Z_name, self.TisTosPreFilter2Z, config['SeedingMinP'])
          self.ZDownMuProtoPFilter = selMuonPParts(Z_name, config['DataType'], self.ZTrackingPreFilter)
          self.ZDownMuPFilter = makeMyMuons(Z_name, self.ZDownMuProtoPFilter)
          self.DownZFilter = DownZ( 'DownSel'+Z_name, self.ZDownMuPFilter,
          		config['ZMuMom'], config['ZMuTMom'], config['ZMuMinEta'], config['ZMuMaxEta'],
          		config['ZMassPreComb'], config['ZMassPostComb'], )

          self.UpsilonTrackingPreFilter = trackingDownPreFilter( Upsilon_name, self.TisTosPreFilter2Upsilon, config['SeedingMinP'])
          self.UpsilonDownMuProtoPFilter = selMuonPParts(Upsilon_name, config['DataType'], self.UpsilonTrackingPreFilter)
          self.UpsilonDownMuPFilter = makeMyMuons(Upsilon_name, self.UpsilonDownMuProtoPFilter)
          self.DownUpsilonFilter = DownUpsilon( 'DownSel'+Upsilon_name, self.UpsilonDownMuPFilter,
          		config['UpsilonMuMom'], config['UpsilonMuTMom'],
          		config['UpsilonMassPreComb'], config['UpsilonMassPostComb'], )

        # ##########################################
        # Please keep p and pT in [GeV], rest in [MeV]
          tagCut = "(TRCHI2DOF < %(JpsiTagTrackChi2)s) & (PT > %(JpsiTagPt)s*GeV) & (P > %(JpsiTagP)s*GeV) & (PIDmu >%(JpsiTagPID)s ) & (MIPDV(PRIMARY)>%(JpsiTagMinIP)s) " % config
          probeCut =  "(TRCHI2DOF < %(JpsiProbeTrackChi2)s) & (PT > %(JpsiProbePt)s*GeV) & (P > %(JpsiProbeP)s*GeV)" % config
          JpsiCombCut = "(AMAXDOCA('') < %(JpsiDoca)s*mm)" % config
          JpsiMotherCut = "(VFASPF(VCHI2/VDOF) < %(JpsiVertexChi2)s) & (PT > %(JpsiPt)s*GeV)" % config

        # ####################################

        # RECONSTRUCT TAG-TRACKS
          self.longbothJpsi = longtrackFilter( name+'LongJpsiBoth', trackAlgo = 'LongMu', partSource = StdLooseMuons, tagCut = tagCut)
          self.longMinusJpsi = chargeFilter( name+'LongJpsiMinus', trackAlgo = 'LongMu',  partSource = self.TisTosPreFilter2Jpsi, charge = -1, probeCut = probeCut, tagCut = tagCut)
          self.longPlusJpsi =  chargeFilter( name+'LongJpsiPlus',  trackAlgo =  'LongMu', partSource = self.TisTosPreFilter2Jpsi, charge = 1,  probeCut = probeCut, tagCut = tagCut)

        # RECONSTRUCT PROBE-TRACKS
          self.DownMuonMinusJpsi = chargeFilter(name+'DownMuonMinusJpsi', trackAlgo = 'DownMuon', partSource = self.DownMuPFilter, charge = -1, probeCut = probeCut, tagCut = tagCut)
          self.DownMuonPlusJpsi =  chargeFilter(name+'DownMuonJpsiPlus',  trackAlgo = 'DownMuon', partSource = self.DownMuPFilter, charge = 1,  probeCut = probeCut, tagCut = tagCut)
        #############1
        # def makeResonanceMuMuTrackEff(name, resonanceName, decayDescriptor, plusCharge, minusCharge,
        # mode, massWin, vertexChi2, resonancePT, DownMuonPT, longPT, longMuonPID, longMuonMinIPCHI2, longMuonTrackCHI2):

        # J/psi -> mu mu, tag-and-probe
          self.makeJpsiDownMuonTrackEff1 = makeResonanceMuMuTrackEff(name+'_MakeJpsiMuMuTrackEff1',
                                                               resonanceName = 'J/psi(1S)',
                                                               decayDescriptor = 'J/psi(1S) -> mu+ mu-',
                                                               plusCharge = self.DownMuonPlusJpsi,
                                                               minusCharge = self.longMinusJpsi,
                                                               mode = 1,
                                                               MassPreComb = config['JpsiMassPreComb'],
                                                               MassPostComb = config['JpsiMassPostComb'],
                                                               combCut = JpsiCombCut,
                                                               motherCut = JpsiMotherCut)
        # J/psi -> mu mu, probe-and-tag
          self.makeJpsiDownMuonTrackEff2 = makeResonanceMuMuTrackEff(name+'_MakeJpsiMuMuTrackEff2',
                                                               resonanceName = 'J/psi(1S)',
                                                               decayDescriptor = 'J/psi(1S) -> mu+ mu-',
                                                               minusCharge = self.DownMuonMinusJpsi,
                                                               plusCharge = self.longPlusJpsi,
                                                               mode = 2,
                                                               MassPreComb = config['JpsiMassPreComb'],
                                                               MassPostComb = config['JpsiMassPostComb'],
                                                               combCut = JpsiCombCut,
                                                               motherCut = JpsiMotherCut)
        #
          self.nominal_line =  StrippingLine(nominal_name + 'Line'
                                             , prescale = config['NominalLinePrescale']
                                             , postscale = config['NominalLinePostscale']
                                             , algos=[self.DownJpsiFilter]
                                             , HLT1 = "HLT_PASS_RE('%(JpsiHlt1Filter)s')" % config
                                             , HLT2 = "HLT_PASS_RE('%(JpsiHlt2Filter)s')" % config
                                             )

          self.nominal_line1 =  StrippingLine(name + 'Line1'
                                             , prescale = config['NominalLinePrescale']
                                             , postscale = config['NominalLinePostscale']
                                             , selection = self.makeJpsiDownMuonTrackEff1
                                             , HLT1 = "HLT_PASS_RE('%(JpsiHlt1Filter)s')" % config
                                             , HLT2 = "HLT_PASS_RE('%(JpsiHlt2Filter)s')" % config
                                             )

          self.nominal_line2 =  StrippingLine(name + 'Line2'
                                             , prescale = config['NominalLinePrescale']
                                             , postscale = config['NominalLinePostscale']
                                             , selection = self.makeJpsiDownMuonTrackEff2
                                             , HLT1 = "HLT_PASS_RE('%(JpsiHlt1Filter)s')" % config
                                             , HLT2 = "HLT_PASS_RE('%(JpsiHlt2Filter)s')" % config
                                             )

          self.valid_line = StrippingLine(valid_name + 'Line', prescale = config['ValidationLinePrescale'], postscale = config['ValidationLinePostscale'], algos=[self.TisTosPreFilter2Jpsi])

          self.Z_line =  StrippingLine(Z_name + 'Line',  prescale = config['ZLinePrescale'], postscale = config['ZLinePostscale'], algos=[self.DownZFilter])

          self.Upsilon_line =  StrippingLine(Upsilon_name + 'Line',  prescale = config['UpsilonLinePrescale'], postscale = config['UpsilonLinePostscale'], algos=[self.DownUpsilonFilter])

          self.registerLine(self.nominal_line1)
          self.registerLine(self.nominal_line2)
          self.registerLine(self.nominal_line)
          self.registerLine(self.valid_line)
          self.registerLine(self.Z_line)
          self.registerLine(self.Upsilon_line)


# ########################################################################################
# Make the protoparticles
# ########################################################################################
def selMuonPParts(name, DataType, downstreamSeq):
   """
       Make ProtoParticles out of Downstream tracks
   """
   unpacker = UnpackTrack(name+"UnpackTrack")  #TODO do we need this or is it here for historical reason ?
   unpacker.InputName="pRec/"+name+"_Downstream/FittedTracks"
   unpacker.OutputName="Rec/"+name+"_Downstream/FittedTracks"

   cm=ConfiguredMuonIDs.ConfiguredMuonIDs( DataType ) #data=DaVinci().getProp("DataType"))
   idalg = cm.configureMuonIDAlgLite(name+"IDalg")
   idalg.TracksLocations = ["Rec/"+name+"_Downstream/FittedTracks"]
   idalg.MuonIDLocation = "Rec/"+name+"_Muon/DownstreamMuonPID"
   idalg.MuonTrackLocation = "Rec/"+name+"_Track/MuonForDownstream" # I would call it FromDownstream

   downprotoseq = GaudiSequencer(name+"ProtoPSeq")
   downprotos = ChargedProtoParticleMaker(name+"ProtoPMaker")
   downprotos.Inputs = ["Rec/"+name+"_Downstream/FittedTracks"]
   downprotos.Output = "Rec/ProtoP/"+name+"_ProtoPMaker/ProtoParticles"
   downprotos.addTool( DelegatingTrackSelector, name="TrackSelector" )
   #tracktypes = [ "Long","Upstream","Downstream","Ttrack","Velo","VeloR" ] # only downstream needed
   tracktypes = ["Downstream"]
   #if (trackcont == "Best") :
   #	tracktypes = [ "Long" ]
   downprotos.TrackSelector.TrackTypes = tracktypes
   selector = downprotos.TrackSelector
   for tsname in tracktypes:
   	selector.addTool(TrackSelector,name=tsname)
   	ts = getattr(selector,tsname)
   	# Set Cuts
   	ts.TrackTypes = [tsname]
#	ts.MinNDoF = 1
   	ts.MaxChi2Cut = 10

   addmuonpid = ChargedProtoParticleAddMuonInfo(name+"addmuoninfo")
   addmuonpid.InputMuonPIDLocation = "Rec/"+name+"_Muon/DownstreamMuonPID"
   addmuonpid.ProtoParticleLocation = "Rec/ProtoP/"+name+"_ProtoPMaker/ProtoParticles"
   #addmuonpid.OutputLevel = 0
   combinedll = ChargedProtoCombineDLLsAlg(name+"CombineDLL")
   combinedll.ProtoParticleLocation = "Rec/ProtoP/"+name+"_ProtoPMaker/ProtoParticles"
   #combinedll.OutputLevel = 0
   # DST post treatment
   #TrackToDST(name+"TrackToDST").TracksInContainer = "Rec/Downstream/Tracks"
   #downprotoseq.Members += [ TrackToDST(name+"TrackToDST"), downprotos, addmuonpid, combinedll ]
   downprotoseq.Members += [ downprotos, addmuonpid, combinedll ]
#
   DataOnDemandSvc().AlgMap.update( {
                "/Event/Rec/"+name+"_Downstream/Tracks" : unpacker.getFullName(),
                "/Event/Rec/"+name+"_Muon/DownstreamMuonPID" : idalg.getFullName(),
#                "/Event/Rec/ProtoP/"+name+"ProtoPMaker" : downprotoseq.getFullName()
		} )

   return GSWrapper(name="WrappedDownMuonProtoPSeqFor"+name,    #TODO: Why do we need a GSWrapper?
                    sequencer=downprotoseq,
                    output='Rec/ProtoP/' + name +'_ProtoPMaker/ProtoParticles',
                    requiredSelections = [ downstreamSeq])
   #     return Selection(name+"_SelPParts", Algorithm = MuonTTPParts, OutputBranch="Rec/ProtoP", Extension="ProtoParticles",RequiredSelections=[downstreamSeq], InputDataSetter=None)

def makeMyMuons(name, protoParticlesMaker):
   """
     Make Particles out of the DOWN muon ProtoParticles
   """
   particleMaker =  BestPIDParticleMaker(name+"ParticleMaker" , Particle = "muon")
   particleMaker.addTool(ProtoParticleMUONFilter,name="muon")
   particleMaker.muon.Selection = ["RequiresDet='MUON' IsMuonLoose=True"]
   particleMaker.Particles = [ "muon" ]
   particleMaker.Input = "Rec/ProtoP/"+name+"_ProtoPMaker/ProtoParticles"
   #particleMaker.OutputLevel = 0

   DataOnDemandSvc().AlgMap.update( {
           "/Event/Phys/" + particleMaker.name() + '/Particles' : particleMaker.getFullName(),
           "/Event/Phys/" + particleMaker.name() + '/Vertices'  : particleMaker.getFullName()
   } )

   return Selection(name+"SelDownMuonParts", Algorithm = particleMaker, RequiredSelections = [protoParticlesMaker], InputDataSetter=None)
#

#########################################################################################

def makeResonanceMuMuTrackEff( name, resonanceName, decayDescriptor, minusCharge, plusCharge , mode , MassPreComb , MassPostComb, combCut, motherCut ):

   #self.makeMyMuons("DownMuonsForTrackEff", "Downstream")
   DownMuonResonance = CombineParticles()
   DownMuonResonance.configurable('_'+name)
   DownMuonResonance.DecayDescriptor = decayDescriptor
   DownMuonResonance.OutputLevel = 4

   if(mode == 1):
       #DownMuonResonance.DaughtersCuts = {"mu+": tagCut,
       #                                   "mu-": probeCut}
       DownMuonResonance.CombinationCut = "(ADAMASS('%(resonanceName)s') < %(MassPreComb)s * MeV) & %(combCut)s" % locals()# (AMAXDOCA('') < %(Doca)s*mm) )" % locals()
       DownMuonResonance.MotherCut = "(ADMASS('%(resonanceName)s') < %(MassPostComb)s * MeV) & %(motherCut)s" % locals()
       return Selection( name, Algorithm = DownMuonResonance, RequiredSelections = [minusCharge, plusCharge] )

   if(mode == 2):
       #DownMuonResonance.DaughtersCuts = {"mu-": tagCut  % locals(),
       #                                   "mu+": probeCut  % locals() }
       DownMuonResonance.CombinationCut = "(ADAMASS('%(resonanceName)s') < %(MassPreComb)s * MeV) & %(combCut)s" % locals()# (AMAXDOCA('') < %(Doca)s*mm) )" % locals()
       DownMuonResonance.MotherCut = "(ADMASS('%(resonanceName)s') < %(MassPostComb)s * MeV) & %(motherCut)s" % locals()
       return Selection( name, Algorithm = DownMuonResonance, RequiredSelections = [plusCharge, minusCharge] )

#########################################################################################
def DownJPsi( name, #TODO unify it, big time
		protoPartSel,
		TrChi2,
		MuTMom,
		MuMom,
		MassPreComb,
		Doca,
		MassPostComb,
		VertChi2 ) :
   #self.makeMyMuons("DownMuonsForTrackEff", "Downstream")

   _MuCuts =  "((TRCHI2DOF < %(TrChi2)s) & (PT > %(MuTMom)s*MeV) & (P > %(MuMom)s*MeV) )" % locals()
   _CombinationCuts = "((ADAMASS('J/psi(1S)') < %(MassPreComb)s * MeV) & (AMAXDOCA('') < %(Doca)s*mm) )" % locals()
   _MotherCuts = "((ADMASS('J/psi(1S)') < %(MassPostComb)s * MeV) & (VFASPF(VCHI2/VDOF) < %(VertChi2)s))" % locals()

   _MyDownJpsis = CombineParticles( DecayDescriptor = "J/psi(1S) -> mu+ mu-" ,
   DaughtersCuts = { "mu+": _MuCuts,  "mu-": _MuCuts }, CombinationCut = _CombinationCuts, MotherCut = _MotherCuts)

   return Selection ( name,
                      Algorithm = _MyDownJpsis,
                      RequiredSelections = [protoPartSel])


#########################################################################################

def DownZ( name,
		protoPartSel,
		MuMom,
		MuTMom,
		MuMinEta,
		MuMaxEta,
		MassPreComb,
		MassPostComb) :
   #self.makeMyMuons("DownMuonsForTrackEff", "Downstream")

   _MuCuts =  "((ETA < %(MuMaxEta)s*MeV) & (ETA > %(MuMinEta)s*MeV) & (P > %(MuMom)s*MeV) & (PT > %(MuTMom)s*MeV) )" % locals()
   _CombinationCuts = "((ADAMASS('Z0') < %(MassPreComb)s * MeV))" % locals()
   _MotherCuts = "((ADMASS('Z0') < %(MassPostComb)s * MeV))" % locals()

   _MyDownZ = CombineParticles( DecayDescriptor = "Z0 -> mu+ mu-" ,
   DaughtersCuts = { "mu+": _MuCuts,  "mu-": _MuCuts }, CombinationCut = _CombinationCuts, MotherCut = _MotherCuts)

   return Selection ( name,
                      Algorithm = _MyDownZ,
                      RequiredSelections = [protoPartSel])

#########################################################################################
def DownUpsilon( name,
		protoPartSel,
		MuMom,
		MuTMom,
		MassPreComb,
		MassPostComb) :
   #self.makeMyMuons("DownMuonsForTrackEff", "Downstream")

   _MuCuts =  "( (P > %(MuMom)s*MeV) & (PT > %(MuTMom)s*MeV) )" % locals()
   _CombinationCuts = "((ADAMASS('Z0') < %(MassPreComb)s * MeV))" % locals()
   _MotherCuts = "((ADMASS('Upsilon(1S)') < %(MassPostComb)s * MeV))" % locals()

   _MyDownZ = CombineParticles( DecayDescriptor = "Upsilon(1S) -> mu+ mu-" , #TODO
   DaughtersCuts = { "mu+": _MuCuts,  "mu-": _MuCuts }, CombinationCut = _CombinationCuts, MotherCut = _MotherCuts)

   return Selection ( name,
                      Algorithm = _MyDownZ,
                      RequiredSelections = [protoPartSel])

# ##########################
# high quality muons
# ##########################
def longtrackFilter(name, trackAlgo, partSource, tagCut):
    """
        Select plus or minus charge for longtrack
    """
    Filter = FilterDesktop() #there is maybe a change needed
    myFilter1 = Filter.configurable("mylongFilter1")

    myFilter1.Code = tagCut

    return Selection( name+'_longFilter'+'LongMu', Algorithm = myFilter1, RequiredSelections = [  partSource ] )
# ########################################################################################
# Charge filter, that filters, well, the charge and takes the particles from the right source (long or muonTT)
# ########################################################################################
def chargeFilter(name, trackAlgo,  partSource, charge, probeCut, tagCut):
    """
        Select plus or minus charge for Velomuon or long track
    """
    Filter = FilterDesktop() #there is maybe a change needed
    myFilter1 = Filter.configurable("myFilter1")

    if(charge == -1):
        myFilter1.Code = "(Q < 0) & "
    if(charge == 1):
        myFilter1.Code = "(Q > 0) & "

    if(trackAlgo == 'DownMuon'):
        myFilter1.Code += probeCut
    if(trackAlgo == "LongMu"):
        myFilter1.Code += tagCut
    if(trackAlgo == 'DownMuon'):
        return Selection( name+'_chargeFilter'+'DownMuon', Algorithm = myFilter1, RequiredSelections = [  partSource ] )
    if(trackAlgo == 'LongMu'):
        return Selection( name+'_chargeFilter'+'LongMu', Algorithm = myFilter1, RequiredSelections = [  partSource ] )
#########################################################################################

#"""
#Define TisTos Prefilters
##"""

# ########################################################################################
# HLT 1 lines we run on
# ########################################################################################
def selHlt1Jpsi(name, HLT1TisTosSpecs, HLT1PassOnAll):
   """
   Filter the long track muon to be TOS on a HLT1 single muon trigger,
   for J/psi selection
   """
   Hlt1Jpsi = TisTosParticleTagger()
   Hlt1Jpsi.TisTosSpecs = HLT1TisTosSpecs
   Hlt1Jpsi.ProjectTracksToCalo = False
   Hlt1Jpsi.CaloClustForCharged = False
   Hlt1Jpsi.CaloClustForNeutral = False
   Hlt1Jpsi.TOSFrac = { 4:0.0, 5:0.0 }
   Hlt1Jpsi.NoRegex = True
   Hlt1Jpsi.PassOnAll = HLT1PassOnAll

   return Selection(name+"_SelHlt1Jpsi", Algorithm = Hlt1Jpsi, RequiredSelections = [ StdAllLooseMuons ])
# ########################################################################################
# HLT 2 lines we run on
# ########################################################################################
def selHlt2Jpsi(name, hlt1Filter, HLT2TisTosSpecs, HLT2PassOnAll):
   """
   Filter the long track muon to be TOS on a HLT2 single muon trigger,
   for J/psi selection
   """
   Hlt2Jpsi = TisTosParticleTagger()
   Hlt2Jpsi.TisTosSpecs =HLT2TisTosSpecs
   Hlt2Jpsi.ProjectTracksToCalo = False
   Hlt2Jpsi.CaloClustForCharged = False
   Hlt2Jpsi.CaloClustForNeutral = False
   Hlt2Jpsi.TOSFrac = { 4:0.0, 5:0.0 }
   Hlt2Jpsi.NoRegex = False
   Hlt2Jpsi.PassOnAll = HLT2PassOnAll

   return Selection(name+"_SelHlt2Jpsi", Algorithm = Hlt2Jpsi, RequiredSelections = [ hlt1Filter ])
##########################################################


def trackingDownPreFilter(name, prefilter, seedcut):
    #Test code for debugging
    #Jpsi_already_there = LoKi__VoidFilter("Jpsi_already_there")
    #Jpsi_already_there.Code = "1 <= CONTAINS('Rec/Track/Downstream')"

    #Jpsi_not_yet_there = LoKi__VoidFilter("Jpsi_not_yet_there")
    #Jpsi_not_yet_there.Code = "1 > CONTAINS('Rec/Track/Downstream')"

    TrackToDST(name+"_DownTrackToDST").TracksInContainer = "Rec/"+name+"_Downstream/FittedTracks"

    jpsidotracking=GaudiSequencer("DownTrackingFor" + name)

    #Add seed tracking
    DownSeeding = PatSeeding(name+"_DownSeeding")
    DownSeeding.OutputTracksName = "Rec/"+name+"_DownSeeding/Tracks"
    PatAlgConf.SeedingConf().configureAlg( SeedAlg = DownSeeding )
    #DownSeeding.addTool(PatSeedingTool, name=name+"_PatSeedingTool")
    #DownSeeding.addTool(PatSeedingTool)
    #PatSeedingTool(name+"_PatSeedingTool").MinMomentum = seedcut
    seedtoolname = name+"_PatSeedingTool"
    DownSeeding.addTool(PatSeedingTool, name=seedtoolname)
    seedtool = getattr(DownSeeding, seedtoolname)
    seedtool.MinMomentum = seedcut

    jpsidotracking.Members += [DownSeeding]
    #Add Seed Fit
    jpsidotracking.Members += [GaudiSequencer(name+"_TrackSeedFitSeq")]
    #AddPatLongLivedTracking
    downstreamTracking = PatLongLivedTracking(name+"_PatLongLivedTracking")
    downstreamTracking.InputLocation = DownSeeding.OutputTracksName
    downstreamTracking.OutputLocation = 'Rec/'+name+'_Downstream/Tracks'
    jpsidotracking.Members += [ downstreamTracking ];
    #AddDownstreamFitSeq
    DownInitAlg = TrackStateInitAlg(name+"_InitSeedDownstream")
    jpsidotracking.Members += [DownInitAlg]
    DownInitAlg.TrackLocation = "Rec/"+name+"_Downstream/Tracks"
    downstreamFit = ConfiguredFitDownstream(name+"_FitDownstream")
    downstreamFit.TracksInContainer = 'Rec/'+name+'_Downstream/Tracks'
    downstreamFit.TracksOutContainer = 'Rec/'+name+'_Downstream/FittedTracks'
    jpsidotracking.Members += [downstreamFit]
    jpsidotracking.Members += [TrackToDST(name+"_DownTrackToDST")]

    return GSWrapper(name="WrappedDownstreamTracking"+name,
                     sequencer=jpsidotracking,
                     output='Rec/'+name+'_Downstream/FittedTracks',
                     requiredSelections = [ prefilter])



class GSWrapper(UniquelyNamedObject,
                ClonableObject,
                SelectionBase) :

    def __init__(self, name, sequencer, output, requiredSelections) :
        UniquelyNamedObject.__init__(self, name)
        ClonableObject.__init__(self, locals())
        SelectionBase.__init__(self,
                               algorithm = sequencer,
                               outputLocation = output,
                               requiredSelections = requiredSelections )
