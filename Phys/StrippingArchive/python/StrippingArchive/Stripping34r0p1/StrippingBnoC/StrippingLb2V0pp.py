###############################################################################
# (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""
Module for construction of Lb->V0h(h) stripping Selections and StrippingLines.
Provides functions to build Lambda0->DD, Lambda0->LL, and Lambda0->LD selections.
Stripping20 with an inclusive approach for Lb->Lambda h(h) modes.
Provides class Lb2V0ppConf, which constructs the Selections and StrippingLines
given a configuration dictionary.
Line based on Lb2V0ph by ["Thomas Latham", "Rafael Coutinho", "Christian Voss", "Christoph Hombach", "Daniel O'Hanlon"]
Exported symbols (use python help!):
   - Lb2V0ppConf
"""

__author__ = ["Vladimir Macko"]
__date__ = '06/01/2019'
__version__ = 'Stripping34'
__all__ = 'Lb2V0ppConf'

from Gaudi.Configuration import *
from GaudiConfUtils.ConfigurableGenerators import FilterDesktop, CombineParticles
from PhysSelPython.Wrappers import Selection, DataOnDemand
from StrippingConf.StrippingLine import StrippingLine
from StrippingUtils.Utils import LineBuilder

from StandardParticles import StdLooseProtons as Protons

default_config = {
    'NAME' : 'Lb2V0p',
    'WGs'  : ['BnoC'],
    'BUILDERTYPE' : 'Lb2V0ppConf',
    'CONFIG' : {'Trk_Chi2'                 : 3.0,    
                'Trk_GhostProb'            : 0.5,     
                'Lambda_DD_MassWindow'     : 20.0,    
                'Lambda_DD_VtxChi2'        : 9.0,     
                'Lambda_DD_FDChi2'         : 50.0,    
                'Lambda_DD_FD'             : 300.0,   
                'Lambda_DD_Pmin'           : 5000.0,  
                'Lambda_LL_MassWindow'     : 20.0,    
                'Lambda_LL_VtxChi2'        : 9.0,     
                'Lambda_LL_FDChi2'         : 0.0,     
                'Lambda_LD_MassWindow'     : 25.0,    
                'Lambda_LD_VtxChi2'        : 16.0,    
                'Lambda_LD_FDChi2'         : 50.0,    
                'Lambda_LD_FD'             : 300.0,   
                'Lambda_LD_Pmin'           : 5000.0,  
                'Lb_Mlow'                  : 419.0,   
                'Lb_Mhigh'                 : 581.0,   
                'Lb_2bodyMlow'             : 800.0,   
                'Lb_2bodyMhigh'            : 800.0,   
                'Lb_APTmin'                : 1000.0,  
                'Lb_PTmin'                 : 1050,    
                'LbDaug_MedPT_PT'          : 450.0,   
                'LbDaug_MaxPT_IP'          : 0.05,   
                'LbDaug_DD_maxDocaChi2'    : 16.0,    
                'LbDaug_LL_maxDocaChi2'    : 5.0,     
                'LbDaug_LD_maxDocaChi2'    : 5.0,     
                'LbDaug_DD_PTsum'          : 2000.0,  
                'LbDaug_LL_PTsum'          : 3000.0, 
                'LbDaug_LD_PTsum'          : 4200.0,  
                'Lbh_DD_PTMin'             : 0.0,    
                'Lbh_LL_PTMin'             : 0.0,    
                'Lbh_LD_PTMin'             : 500.0,   
                'Lb_VtxChi2'               : 16.0,    
                'Lb_DD_Dira'               : 0.9990,  
                'Lb_LL_Dira'               : 0.9990,  
                'Lb_LD_Dira'               : 0.9990, 
                'Lb_DD_IPCHI2wrtPV'        : 25.0,   
                'Lb_LL_IPCHI2wrtPV'        : 25.0,   
                'Lb_LD_IPCHI2wrtPV'        : 15.0,    
                'Lb_FDwrtPV'               : 0.8,    
                'Lb_DD_FDChi2'             : 0.5,    
                'Lb_LL_FDChi2'             : 0.5,    
                'Lb_LD_FDChi2'             : 30.0,  
                'GEC_MaxTracks'            : 250,    
                # 2012 Triggers
                #'HLT1Dec'                  : 'Hlt1TrackAllL0Decision',
                #'HLT2Dec'                  : 'Hlt2Topo[234]Body.*Decision',
                # 2015 Triggers
                #'HLT1Dec'                  : 'Hlt1(Two)?TrackMVADecision',
                #'HLT2Dec'                  : 'Hlt2Topo[234]BodyDecision',
                'Prescale'                 : 1.0,
                'Postscale'                : 1.0,
                'RelatedInfoTools' : [    { "Type" : "RelInfoConeVariables", 
                                            "ConeAngle" : 1.7, 
                                            "Variables" : ['CONEANGLE', 'CONEMULT', 'CONEPTASYM'], 
                                            "Location"  : 'ConeVar17'
                                            }, 
                                          { "Type" : "RelInfoConeVariables", 
                                            "ConeAngle" : 1.5, 
                                            "Variables" : ['CONEANGLE', 'CONEMULT', 'CONEPTASYM'], 
                                            "Location"  : 'ConeVar15'
                                            }, 
                                          { "Type" : "RelInfoConeVariables", 
                                            "ConeAngle" : 1.0, 
                                            "Variables" : ['CONEANGLE', 'CONEMULT', 'CONEPTASYM'], 
                                            "Location"  : 'ConeVar10'
                                            },
                                          { "Type" : "RelInfoConeVariables", 
                                            "ConeAngle" : 0.8, 
                                            "Variables" : ['CONEANGLE', 'CONEMULT', 'CONEPTASYM'], 
                                            "Location"  : 'ConeVar08'
                                            },
                                          { "Type" : "RelInfoVertexIsolation",
                                            "Location" : "VtxIsolationVar"
                                            }
                                          ]
                },
    'STREAMS' : ['Bhadron']
    }

class Lb2V0ppConf(LineBuilder) :
    """
    Builder of Lb->V0h(h) stripping Selection and StrippingLine.
    Constructs Lb -> V0 (h+) h- Selections and StrippingLines from a configuration dictionary.
    Usage:
    >>> config = { .... }
    >>> Lb2V0ppConf = Lb2V0ppConf('Lb2V0ppTest',config)
    >>> lb2v0hhLines = Lb2V0ppConf.lines
    >>> for line in line :
    >>>  print line.name(), line.outputLocation()
    The lines can be used directly to build a StrippingStream object.

    Exports as instance data members:
    selLambda2DD           : Lambda0 -> Down Down Selection object
    selLambda2LL           : Lambda0 -> Long Long Selection object
    selLambda2LD           : Lambda0 -> Long Down Selection object

    selLb2V0DDpp           : Lb -> Lambda0(DD) h+ h- Selection object
    selLb2V0LLpp           : Lb -> Lambda0(LL) h+ h- Selection object
    selLb2V0LDpp           : Lb -> Lambda0(LD) h+ h- Selection object            
    selLb2V0DDp            : Lb -> Lambda0(DD) h- Selection object
    selLb2V0LLp            : Lb -> Lambda0(LL) h- Selection object 
    selLb2V0LDp            : Lb -> Lambda0(LD) h- Selection object        

    Lb_dd_line             : StrippingLine made out of selLb2V0DDpp
    Lb_ll_line             : StrippingLine made out of selLb2V0LLpp 
    Lb_ld_line             : StrippingLine made out of selLb2V0LDpp

    lines                  : List of lines, [Lb_dd_line, Lb_ll_line, Lb_ld_line]

    Exports as class data member:
    Lb2V0ppConf.__configuration_keys__ : List of required configuration parameters.
    """

    __configuration_keys__ = default_config['CONFIG'].keys()

    def __init__(self, name, config) :

        LineBuilder.__init__(self, name, config)

        GECCode = {'Code' : "(recSummaryTrack(LHCb.RecSummary.nLongTracks, TrLONG) < %s)" % config['GEC_MaxTracks'],
                   'Preambulo' : ["from LoKiTracks.decorators import *"]}

        #self.hlt1Filter = {'Code' : "HLT_PASS_RE('%s')" % config['HLT1Dec'],
        #                   'Preambulo' : ["from LoKiCore.functions import *"]}
        #self.hlt2Filter = {'Code' : "HLT_PASS_RE('%s')" % config['HLT2Dec'],
        #                   'Preambulo' : ["from LoKiCore.functions import *"]}

        self.protons   = Protons

        self.makeLambda2DD( 'Lambda0ppDDLbLines', config )
        self.makeLambda2LL( 'Lambda0ppLLLbLines', config )
        self.makeLambda2LD( 'Lambda0ppLDLbLines', config )


        namesSelections = [ (name + 'DD', self.makeLb2V0DDp(name + 'DD', config)),
                            (name + 'LL', self.makeLb2V0LLp(name + 'LL', config)),
                            (name + 'LD', self.makeLb2V0LDp(name + 'LD', config)),

                            (name + 'pDD', self.makeLb2V0DDpp(name + 'pDD', config)),
                            (name + 'pLL', self.makeLb2V0LLpp(name + 'pLL', config)),
                            (name + 'pLD', self.makeLb2V0LDpp(name + 'pLD', config)),

                            (name + 'pDDSS', self.makeLb2V0DDpp(name + 'pDDSS', config)),
                            (name + 'pLLSS', self.makeLb2V0LLpp(name + 'pLLSS', config)),
                            (name + 'pLDSS', self.makeLb2V0LDpp(name + 'pLDSS', config)),
                          ]

        # make lines
        
        for selName, sel in namesSelections:

            extra = {}

            #if 'SS' in selName:
                #extra['HLT1'] = self.hlt1Filter
                #extra['HLT2'] = self.hlt2Filter

            line = StrippingLine(selName + 'Line',
                                 selection = sel,
                                 prescale = config['Prescale'],
                                 postscale = config['Postscale'],
                                 RelatedInfoTools = config['RelatedInfoTools'], 
                                 FILTER = GECCode,
                                 **extra) 

            self.registerLine(line)

    def makeLambda2DD( self, name, config ) :
        # define all the cuts
        _massCut          = "(ADMASS('Lambda0')<%s*MeV)"      % config['Lambda_DD_MassWindow']
        _vtxCut           = "(VFASPF(VCHI2)<%s)   "           % config['Lambda_DD_VtxChi2']
        _fdChi2Cut        = "(BPVVDCHI2>%s)"                  % config['Lambda_DD_FDChi2']
        _momCut           = "(P>%s*MeV)"                      % config['Lambda_DD_Pmin']
        _trkGhostProbCut1 = "(CHILDCUT((TRGHOSTPROB<%s),1))"  % config['Trk_GhostProb']
        _trkGhostProbCut2 = "(CHILDCUT((TRGHOSTPROB<%s),2))"  % config['Trk_GhostProb']

        _allCuts = _momCut
        _allCuts += '&'+_massCut
        _allCuts += '&'+_vtxCut 
        _allCuts += '&'+_fdChi2Cut
        #_allCuts += '&'+_trkGhostProbCut1
        #_allCuts += '&'+_trkGhostProbCut2

        # get the Lambda0's to filter
        _stdLambdaDD = DataOnDemand(Location = "Phys/StdLooseLambdaDD/Particles")
        
        # make the filter
        _filterLambdaDD = FilterDesktop( Code = _allCuts )

        # make and store the Selection object
        self.selLambda2DD = Selection( name, Algorithm = _filterLambdaDD, RequiredSelections = [_stdLambdaDD] )

        return self.selLambda2DD

    def makeLambda2LL( self, name, config ) : 
        # define all the cuts
        _massCut    = "(ADMASS('Lambda0')<%s*MeV)"           % config['Lambda_LL_MassWindow']
        _vtxCut     = "(VFASPF(VCHI2)<%s)"                   % config['Lambda_LL_VtxChi2']
        _trkChi2Cut1 = "(CHILDCUT((TRCHI2DOF<%s),1))"        % config['Trk_Chi2']
        _trkChi2Cut2 = "(CHILDCUT((TRCHI2DOF<%s),2))"        % config['Trk_Chi2']
        _trkGhostProbCut1 = "(CHILDCUT((TRGHOSTPROB<%s),1))" % config['Trk_GhostProb']
        _trkGhostProbCut2 = "(CHILDCUT((TRGHOSTPROB<%s),2))" % config['Trk_GhostProb']

        _allCuts = _massCut
        _allCuts += '&'+_trkChi2Cut1
        _allCuts += '&'+_trkChi2Cut2
        _allCuts += '&'+_vtxCut
        _allCuts += '&'+_trkGhostProbCut1
        _allCuts += '&'+_trkGhostProbCut2

        # get the Lambda's to filter
        _stdLambdaLL = DataOnDemand(Location = "Phys/StdLooseLambdaLL/Particles")

        # make the filter
        _filterLambdaLL = FilterDesktop( Code = _allCuts )
        
        # make and store the Selection object
        self.selLambda2LL = Selection( name, Algorithm = _filterLambdaLL, RequiredSelections = [_stdLambdaLL] )

        return self.selLambda2LL

    def makeLambda2LD( self, name, config ) :
        # define all the cuts
        _massCut          = "(ADMASS('Lambda0')<%s*MeV)"      % config['Lambda_DD_MassWindow']
        _vtxCut           = "(VFASPF(VCHI2)<%s)   "           % config['Lambda_DD_VtxChi2']
        _fdChi2Cut        = "(BPVVDCHI2>%s)"                  % config['Lambda_DD_FDChi2']
        _momCut           = "(P>%s*MeV)"                      % config['Lambda_DD_Pmin']
        _trkGhostProbCut1 = "(CHILDCUT((TRGHOSTPROB<%s),1))"  % config['Trk_GhostProb']
        _trkGhostProbCut2 = "(CHILDCUT((TRGHOSTPROB<%s),2))"  % config['Trk_GhostProb']

        _allCuts = _momCut
        _allCuts += '&'+_massCut
        _allCuts += '&'+_vtxCut
        _allCuts += '&'+_fdChi2Cut
        #_allCuts += '&'+_trkGhostProbCut1
        #_allCuts += '&'+_trkGhostProbCut2

        # get the Lambda0's to filter
        _stdLambdaLD = DataOnDemand(Location = "Phys/StdLooseLambdaLD/Particles")
        
        # make the filter
        _filterLambdaLD = FilterDesktop( Code = _allCuts )

        # make and store the Selection object
        self.selLambda2LD = Selection( name, Algorithm = _filterLambdaLD, RequiredSelections = [_stdLambdaLD] )

        return self.selLambda2LD

    def makeLb2V0DDp( self, name, config ) :
        """
        Create and store a Lb ->Lambda0(DD) p+ h- Selection object.
        Arguments:
        name             : name of the Selection.
        config           : config dictionary
        """

        _massCutLow     = "(AM>(5619-%s)*MeV)"               % config['Lb_2bodyMlow']
        _massCutHigh    = "(AM<(5619+%s)*MeV)"               % config['Lb_2bodyMhigh']
        _aptCut         = "(APT>%s*MeV)"                     % config['Lb_APTmin']
        _daugMedPtCut   = "(ANUM(PT>%s*MeV)>=2)"             % config['LbDaug_MedPT_PT']
        _daugMaxPtIPCut = "(AVAL_MAX(MIPDV(PRIMARY),PT)>%s)" % config['LbDaug_MaxPT_IP']
        _maxDocaChi2Cut = "(ACUTDOCACHI2(%s,''))"            % config['LbDaug_DD_maxDocaChi2']
        _daugPtSumCut   = "(APT1>%s*MeV)"                    % config['Lbh_DD_PTMin']

        _combCuts = _aptCut
        _combCuts += '&'+_daugPtSumCut
        _combCuts += '&'+_daugMedPtCut
        _combCuts += '&'+_massCutLow
        _combCuts += '&'+_massCutHigh
        #_combCuts += '&'+_daugMaxPtIPCut
        _combCuts += '&'+_maxDocaChi2Cut

        _ptCut      = "(PT>%s*MeV)"                    % config['Lb_PTmin']
        _vtxChi2Cut = "(VFASPF(VCHI2)<%s)"             % config['Lb_VtxChi2']
        _diraCut    = "(BPVDIRA>%s)"                   % config['Lb_DD_Dira']
        _ipChi2Cut  = "(MIPCHI2DV(PRIMARY)<%s)"        % config['Lb_DD_IPCHI2wrtPV']
        _fdCut      = "(VFASPF(VMINVDDV(PRIMARY))>%s)" % config['Lb_FDwrtPV']
        _fdChi2Cut  = "(BPVVDCHI2>%s)"                 % config['Lb_DD_FDChi2']

        _motherCuts = _ptCut
        _motherCuts += '&'+_vtxChi2Cut
        _motherCuts += '&'+_diraCut
        _motherCuts += '&'+_ipChi2Cut
        _motherCuts += '&'+_fdCut
        _motherCuts += '&'+_fdChi2Cut

        _Lb = CombineParticles()
        _Lb.DecayDescriptors = [ "Lambda_b0 -> p~- Lambda0", "Lambda_b~0 -> p+ Lambda~0"]
        _Lb.DaughtersCuts = { "p+" : "TRCHI2DOF<%s"% config['Trk_Chi2'] }
        _Lb.CombinationCut = _combCuts
        _Lb.MotherCut = _motherCuts
        _Lb.ReFitPVs = True
        
        self.selLb2V0DDp = Selection (name, Algorithm = _Lb, RequiredSelections = [self.selLambda2DD, self.protons ])

        return self.selLb2V0DDp

    def makeLb2V0DDpp( self, name, config ) :
        """
        Create and store a Lb ->Lambda0(DD) p+ h- Selection object.
        Arguments:
        name             : name of the Selection.
        config           : config dictionary
        """

        _massCutLow     = "(AM>(5619-%s)*MeV)"               % config['Lb_Mlow']
        _massCutHigh    = "(AM<(5619+%s)*MeV)"               % config['Lb_Mhigh']
        _aptCut         = "(APT>%s*MeV)"                     % config['Lb_APTmin']
        _daugMedPtCut   = "(ANUM(PT>%s*MeV)>=2)"             % config['LbDaug_MedPT_PT']
        _daugMaxPtIPCut = "(AVAL_MAX(MIPDV(PRIMARY),PT)>%s)" % config['LbDaug_MaxPT_IP']
        _maxDocaChi2Cut = "(ACUTDOCACHI2(%s,''))"            % config['LbDaug_DD_maxDocaChi2']
        _daugPtSumCut   = "((APT1+APT2+APT3)>%s*MeV)"        % config['LbDaug_DD_PTsum']

        _combCuts = _aptCut
        _combCuts += '&'+_daugPtSumCut    
        _combCuts += '&'+_daugMedPtCut    
        _combCuts += '&'+_massCutLow     
        _combCuts += '&'+_massCutHigh     
        #_combCuts += '&'+_daugMaxPtIPCut  # does not work properly 
        _combCuts += '&'+_maxDocaChi2Cut  

        _ptCut      = "(PT>%s*MeV)"                    % config['Lb_PTmin']
        _vtxChi2Cut = "(VFASPF(VCHI2)<%s)"             % config['Lb_VtxChi2']
        _diraCut    = "(BPVDIRA>%s)"                   % config['Lb_DD_Dira']
        _ipChi2Cut  = "(MIPCHI2DV(PRIMARY)<%s)"        % config['Lb_DD_IPCHI2wrtPV']
        _fdCut      = "(VFASPF(VMINVDDV(PRIMARY))>%s)" % config['Lb_FDwrtPV']
        _fdChi2Cut  = "(BPVVDCHI2>%s)"                 % config['Lb_DD_FDChi2']

        _motherCuts = _ptCut
        _motherCuts += '&'+_vtxChi2Cut 
        _motherCuts += '&'+_diraCut  
        _motherCuts += '&'+_ipChi2Cut 
        _motherCuts += '&'+_fdCut #lookhere
        _motherCuts += '&'+_fdChi2Cut 

        _Lb = CombineParticles()

        if 'SS' in name: # Same sign

            _Lb.DecayDescriptors = [ "Lambda_b0 -> p+ p+ Lambda0", "Lambda_b~0 -> p+ p+ Lambda~0",
                                     "Lambda_b0 -> p~- p~- Lambda0", "Lambda_b~0 -> p~- p~- Lambda~0"]

        else:

            _Lb.DecayDescriptors = [ "Lambda_b0 -> p+ p~- Lambda0", "Lambda_b~0 -> p+ p~- Lambda~0"]

        _trkGhostProbCut  = "(TRGHOSTPROB<%s)" % config['Trk_GhostProb']
        _trkChi2Cut       = "(TRCHI2DOF<%s)"   % config['Trk_Chi2']

        _daughtersCuts = _trkChi2Cut + '&' + _trkGhostProbCut

        _Lb.DaughtersCuts = { "p+" : _daughtersCuts }
        _Lb.CombinationCut =  _combCuts
        _Lb.MotherCut = _motherCuts
        _Lb.ReFitPVs = True
        
        _LbConf = _Lb.configurable(name + '_combined')

        if 'SS' in name: # Same sign
            self.selLb2V0DDppSS = Selection (name, Algorithm = _LbConf, RequiredSelections = [self.selLambda2DD, self.protons ])
            return self.selLb2V0DDppSS
        else:
            self.selLb2V0DDpp = Selection (name, Algorithm = _LbConf, RequiredSelections = [self.selLambda2DD, self.protons ])
            return self.selLb2V0DDpp

    def makeLb2V0LLp( self, name, config ) :
        """
        Create and store a Lb -> Lambda0(LL) p+ h- Selection object.
        Arguments:
        name             : name of the Selection.
        config           : config dictionary
        """

        _massCutLow     = "(AM>(5619-%s)*MeV)"               % config['Lb_2bodyMlow']
        _massCutHigh    = "(AM<(5619+%s)*MeV)"               % config['Lb_2bodyMhigh']
        _aptCut         = "(APT>%s*MeV)"                     % config['Lb_APTmin']
        _daugMedPtCut   = "(ANUM(PT>%s*MeV)>=2)"             % config['LbDaug_MedPT_PT']
        _maxDocaChi2Cut = "(ACUTDOCACHI2(%s,''))"            % config['LbDaug_LL_maxDocaChi2']
        _daugPtSumCut   = "(APT1>%s*MeV)"                    % config['Lbh_LL_PTMin']

        _combCuts = _aptCut
        _combCuts += '&'+_daugPtSumCut
        _combCuts += '&'+_daugMedPtCut
        _combCuts += '&'+_massCutLow
        _combCuts += '&'+_massCutHigh
        _combCuts += '&'+_maxDocaChi2Cut

        _ptCut      = "(PT>%s*MeV)"                    % config['Lb_PTmin']
        _vtxChi2Cut = "(VFASPF(VCHI2)<%s)"             % config['Lb_VtxChi2']
        _diraCut    = "(BPVDIRA>%s)"                   % config['Lb_LL_Dira']
        _ipChi2Cut  = "(MIPCHI2DV(PRIMARY)<%s)"        % config['Lb_LL_IPCHI2wrtPV']
        _fdCut      = "(VFASPF(VMINVDDV(PRIMARY))>%s)" % config['Lb_FDwrtPV']
        _fdChi2Cut  = "(BPVVDCHI2>%s)"                 % config['Lb_LL_FDChi2']

        _motherCuts = _ptCut
        _motherCuts += '&'+_vtxChi2Cut
        _motherCuts += '&'+_diraCut
        _motherCuts += '&'+_ipChi2Cut
        _motherCuts += '&'+_fdCut
        _motherCuts += '&'+_fdChi2Cut

        _Lb = CombineParticles()
        _Lb.DecayDescriptors = [ "Lambda_b0 -> p~- Lambda0", "Lambda_b~0 -> p+ Lambda~0" ]
        _Lb.DaughtersCuts = { "p+" : "TRCHI2DOF<%s"% config['Trk_Chi2'] }
        _Lb.CombinationCut = _combCuts
        _Lb.MotherCut = _motherCuts
        _Lb.ReFitPVs = True
        
        self.selLb2V0LLp = Selection (name, Algorithm = _Lb, RequiredSelections = [self.selLambda2LL, self.protons  ])

        return self.selLb2V0LLp

    def makeLb2V0LLpp( self, name, config ) :
        """
        Create and store a Lb -> Lambda0(LL) p+ h- Selection object.
        Arguments:
        name             : name of the Selection.
        config           : config dictionary
        """

        _massCutLow     = "(AM>(5619-%s)*MeV)"               % config['Lb_Mlow']
        _massCutHigh    = "(AM<(5619+%s)*MeV)"               % config['Lb_Mhigh']
        _aptCut         = "(APT>%s*MeV)"                     % config['Lb_APTmin']
        _daugMedPtCut   = "(ANUM(PT>%s*MeV)>=2)"             % config['LbDaug_MedPT_PT']
        _maxDocaChi2Cut = "(ACUTDOCACHI2(%s,''))"            % config['LbDaug_LL_maxDocaChi2']
        _daugPtSumCut   = "((APT1+APT2+APT3)>%s*MeV)"        % config['LbDaug_LL_PTsum']

        _combCuts = _aptCut
        _combCuts += '&'+_daugPtSumCut
        _combCuts += '&'+_daugMedPtCut
        _combCuts += '&'+_massCutLow
        _combCuts += '&'+_massCutHigh
        _combCuts += '&'+_maxDocaChi2Cut
        
        _ptCut      = "(PT>%s*MeV)"                    % config['Lb_PTmin']
        _vtxChi2Cut = "(VFASPF(VCHI2)<%s)"             % config['Lb_VtxChi2']
        _diraCut    = "(BPVDIRA>%s)"                   % config['Lb_LL_Dira']
        _ipChi2Cut  = "(MIPCHI2DV(PRIMARY)<%s)"        % config['Lb_LL_IPCHI2wrtPV']
        _fdCut      = "(VFASPF(VMINVDDV(PRIMARY))>%s)" % config['Lb_FDwrtPV']
        _fdChi2Cut  = "(BPVVDCHI2>%s)"                 % config['Lb_LL_FDChi2']

        _motherCuts = _ptCut
        _motherCuts += '&'+_vtxChi2Cut
        _motherCuts += '&'+_diraCut
        _motherCuts += '&'+_ipChi2Cut
        _motherCuts += '&'+_fdChi2Cut
        _motherCuts += '&'+_fdCut

        _Lb = CombineParticles()

        if 'SS' in name: # Same sign

            _Lb.DecayDescriptors = [ "Lambda_b0 -> p+ p+ Lambda0", "Lambda_b~0 -> p+ p+ Lambda~0",
                                     "Lambda_b0 -> p~- p~- Lambda0", "Lambda_b~0 -> p~- p~- Lambda~0"]

        else:

            _Lb.DecayDescriptors = [ "Lambda_b0 -> p+ p~- Lambda0", "Lambda_b~0 -> p+ p~- Lambda~0"]

        _trkGhostProbCut  = "(TRGHOSTPROB<%s)" % config['Trk_GhostProb']
        _trkChi2Cut       = "(TRCHI2DOF<%s)"   % config['Trk_Chi2']

        _daughtersCuts = _trkChi2Cut + '&' + _trkGhostProbCut

        _Lb.DaughtersCuts = { "p+" : _daughtersCuts }
        _Lb.CombinationCut = _combCuts
        _Lb.MotherCut = _motherCuts
        _Lb.ReFitPVs = True
        
        _LbConf = _Lb.configurable(name + '_combined')

        if 'SS' in name: # Same sign
            self.selLb2V0LLppSS = Selection (name, Algorithm = _LbConf, RequiredSelections = [self.selLambda2LL, self.protons  ])
            return self.selLb2V0LLppSS
        else:
            self.selLb2V0LLpp = Selection (name, Algorithm = _LbConf, RequiredSelections = [self.selLambda2LL, self.protons  ])
            return self.selLb2V0LLpp

    def makeLb2V0LDp( self, name, config ) :
        """
        Create and store a Lb ->Lambda0(LD) p+ h- Selection object.
        Arguments:
        name             : name of the Selection.
        config           : config dictionary
        """

        _massCutLow     = "(AM>(5619-%s)*MeV)"               % config['Lb_2bodyMlow']
        _massCutHigh    = "(AM<(5619+%s)*MeV)"               % config['Lb_2bodyMhigh']
        _aptCut         = "(APT>%s*MeV)"                     % config['Lb_APTmin']
        _daugMedPtCut   = "(ANUM(PT>%s*MeV)>=2)"             % config['LbDaug_MedPT_PT']
        _daugMaxPtIPCut = "(AVAL_MAX(MIPDV(PRIMARY),PT)>%s)" % config['LbDaug_MaxPT_IP']
        _maxDocaChi2Cut = "(ACUTDOCACHI2(%s,''))"            % config['LbDaug_LD_maxDocaChi2']
        _daugPtSumCut   = "(APT1>%s*MeV)"                    % config['Lbh_LD_PTMin']

        _combCuts = _aptCut
        _combCuts += '&'+_daugPtSumCut
        _combCuts += '&'+_daugMedPtCut
        _combCuts += '&'+_massCutLow
        _combCuts += '&'+_massCutHigh
        #_combCuts += '&'+_daugMaxPtIPCut
        _combCuts += '&'+_maxDocaChi2Cut

        _ptCut      = "(PT>%s*MeV)"                    % config['Lb_PTmin']
        _vtxChi2Cut = "(VFASPF(VCHI2)<%s)"             % config['Lb_VtxChi2']
        _diraCut    = "(BPVDIRA>%s)"                   % config['Lb_LD_Dira']
        _ipChi2Cut  = "(MIPCHI2DV(PRIMARY)<%s)"        % config['Lb_LD_IPCHI2wrtPV']
        _fdCut      = "(VFASPF(VMINVDDV(PRIMARY))>%s)" % config['Lb_FDwrtPV']
        _fdChi2Cut  = "(BPVVDCHI2>%s)"                 % config['Lb_LD_FDChi2']

        _motherCuts = _ptCut
        _motherCuts += '&'+_vtxChi2Cut
        _motherCuts += '&'+_diraCut
        _motherCuts += '&'+_ipChi2Cut
        _motherCuts += '&'+_fdCut
        _motherCuts += '&'+_fdChi2Cut

        _Lb = CombineParticles()
        _Lb.DecayDescriptors = [ "Lambda_b0 -> p~- Lambda0", "Lambda_b~0 -> p+ Lambda~0"]
        _Lb.DaughtersCuts = { "p+" : "TRCHI2DOF<%s"% config['Trk_Chi2'] }
        _Lb.CombinationCut = _combCuts
        _Lb.MotherCut = _motherCuts
        _Lb.ReFitPVs = True

        self.selLb2V0LDp = Selection (name, Algorithm = _Lb, RequiredSelections = [self.selLambda2LD, self.protons ])

        return self.selLb2V0LDp

    def makeLb2V0LDpp( self, name, config ) :
        """
        Create and store a Lb ->Lambda0(LD) p+ h- Selection object.
        Arguments:
        name             : name of the Selection.
        config           : config dictionary
        """

        _massCutLow     = "(AM>(5619-%s)*MeV)"               % config['Lb_Mlow']
        _massCutHigh    = "(AM<(5619+%s)*MeV)"               % config['Lb_Mhigh']
        _aptCut         = "(APT>%s*MeV)"                     % config['Lb_APTmin']
        _daugMedPtCut   = "(ANUM(PT>%s*MeV)>=2)"             % config['LbDaug_MedPT_PT']
        _daugMaxPtIPCut = "(AVAL_MAX(MIPDV(PRIMARY),PT)>%s)" % config['LbDaug_MaxPT_IP']
        _maxDocaChi2Cut = "(ACUTDOCACHI2(%s,''))"            % config['LbDaug_LD_maxDocaChi2']
        _daugPtSumCut   = "((APT1+APT2+APT3)>%s*MeV)"        % config['LbDaug_LD_PTsum']

        _combCuts = _aptCut
        _combCuts += '&'+_daugPtSumCut
        _combCuts += '&'+_daugMedPtCut
        _combCuts += '&'+_massCutLow
        _combCuts += '&'+_massCutHigh
        #_combCuts += '&'+_daugMaxPtIPCut
        _combCuts += '&'+_maxDocaChi2Cut

        _ptCut      = "(PT>%s*MeV)"                    % config['Lb_PTmin']
        _vtxChi2Cut = "(VFASPF(VCHI2)<%s)"             % config['Lb_VtxChi2']
        _diraCut    = "(BPVDIRA>%s)"                   % config['Lb_LD_Dira']
        _ipChi2Cut  = "(MIPCHI2DV(PRIMARY)<%s)"        % config['Lb_LD_IPCHI2wrtPV']
        _fdCut      = "(VFASPF(VMINVDDV(PRIMARY))>%s)" % config['Lb_FDwrtPV']
        _fdChi2Cut  = "(BPVVDCHI2>%s)"                 % config['Lb_LD_FDChi2']

        _motherCuts = _ptCut
        _motherCuts += '&'+_vtxChi2Cut
        _motherCuts += '&'+_diraCut
        _motherCuts += '&'+_ipChi2Cut
        _motherCuts += '&'+_fdChi2Cut

        _Lb = CombineParticles()

        if 'SS' in name: # Same sign

            _Lb.DecayDescriptors = [ "Lambda_b0 -> p+ p+ Lambda0", "Lambda_b~0 -> p+ p+ Lambda~0",
                                     "Lambda_b0 -> p~- p~- Lambda0", "Lambda_b~0 -> p~- p~- Lambda~0"]

        else:

            _Lb.DecayDescriptors = [ "Lambda_b0 -> p+ p~- Lambda0", "Lambda_b~0 -> p+ pi- Lambda~0"]

        _trkGhostProbCut  = "(TRGHOSTPROB<%s)" % config['Trk_GhostProb']
        _trkChi2Cut       = "(TRCHI2DOF<%s)"   % config['Trk_Chi2']

        _daughtersCuts = _trkChi2Cut + '&' + _trkGhostProbCut

        _Lb.DaughtersCuts = { "p+" : _daughtersCuts }
        _Lb.CombinationCut = _combCuts
        _Lb.MotherCut = _motherCuts
        _Lb.ReFitPVs = True
        
        _LbConf = _Lb.configurable(name + '_combined')

        if 'SS' in name: # Same sign
            self.selLb2V0LDppSS = Selection (name, Algorithm = _LbConf, RequiredSelections = [self.selLambda2LD, self.protons ])
            return self.selLb2V0LDppSS
        else:
            self.selLb2V0LDpp = Selection (name, Algorithm = _LbConf, RequiredSelections = [self.selLambda2LD, self.protons ])
            return self.selLb2V0LDpp
