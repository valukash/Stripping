###############################################################################
# (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
# Stripping Lines for Z->MuMu and studies of Mu efficiencies
# Electroweak Group (Conveners: S.Bifani, J.Anderson; Stripping contact: W.Barter)
#
# S.Bifani
#
# Z02MuMu/High Mass DY signal:     StdAllLooseMuons,  pT>3GeV & MM>40GeV

__author__ = ['S. Bifani']

__all__ = (
  'Z02MuMuConf',
  'default_config',
)

from GaudiConfUtils.ConfigurableGenerators import CombineParticles
from PhysSelPython.Wrappers import SimpleSelection
from StrippingConf.StrippingLine import StrippingLine
from StrippingUtils.Utils import LineBuilder
from StandardParticles import StdAllLooseMuons
from GaudiKernel.SystemOfUnits import GeV

default_config = {
  'NAME'             : 'Z02MuMu',
  'BUILDERTYPE'      : 'Z02MuMuConf',
  'WGs'              : [ 'QEE'],
  'STREAMS'          : [ 'EW' ],
  'CONFIG'           : { 
    'Prescale'       : 1.0,
    'Postscale'      : 1.0,
    'pT'             : 3.  * GeV,
    'MMmin'          : 40. * GeV,
    'RawEvents'      : ["Muon", "Calo", "Rich", "Velo", "Tracker", "HC"],
  },
}

class Z02MuMuConf( LineBuilder ) :

  __configuration_keys__ = default_config['CONFIG'].keys()

  def __init__( self, name, config ) :

    LineBuilder.__init__( self, name, config )

    self.registerLine(StrippingLine( name + 'Line',
      prescale          = config[ 'Prescale'  ],
      postscale         = config[ 'Postscale' ],
      RequiredRawEvents = config[ 'RawEvents' ],
      checkPV           = False,
      selection         = makeCombination(name+'Z02MuMu', config),
    ))

def makeCombination( name, config ):
  # Define the cuts
  dcut = '(PT>%(pT)s)'%config
  mcut = '(MM>%(MMmin)s)'%config

  return SimpleSelection(name, CombineParticles, [StdAllLooseMuons],
    DecayDescriptor    = 'Z0 -> mu+ mu-',
    DaughtersCuts      = { 'mu+' : dcut, 'mu-' : dcut },
    MotherCut          = mcut,
    WriteP2PVRelations = False
  )

