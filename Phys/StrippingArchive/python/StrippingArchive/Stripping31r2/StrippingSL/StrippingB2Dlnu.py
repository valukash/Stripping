###############################################################################
# (c) Copyright 2020 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""
Selection for B -> D l nu electron/muon universality measurements
"""
__author__ = ['Greg Ciezarek']
__date__ = '30/11/2016'
__version__ = '$Revision: 1.0 $'

from Gaudi.Configuration import *
from GaudiConfUtils.ConfigurableGenerators import FilterDesktop, CombineParticles
from PhysSelPython.Wrappers import Selection, DataOnDemand
from StrippingConf.StrippingLine import StrippingLine
from StrippingUtils.Utils import LineBuilder
from StandardParticles import StdLoosePions, StdLooseMuons, StdLooseKaons, StdLooseProtons, StdAllLoosePions, StdLooseMergedPi0,StdLooseResolvedPi0,StdAllNoPIDsMuons, StdLooseANNUpPions, StdLooseElectrons
from Configurables import ConjugateNeutralPID
from PhysSelPython.Wrappers import MergedSelection

__all__ = ('B2DMuForLNuconf',
           #'makeb2DMuX',
           'confdict')
default_config = {
    'B2DMuForLNu' : {
        'WGs'         : ['Semileptonic'],
	'BUILDERTYPE' : 'B2DMuForLNuconf',
        'CONFIG'      : {
 
	 "MINIPCHI2"     : 16.0    # adimensional 
	,"GhostProb"     : 0.5    # adimensional  
	,"KaonPIDK"      : 4.0    # adimensional
	,"PionPIDKTight" : 2.0    # adimensional
	,"LeptonIPCHI2"  : 9.00  # adimensional
	,"LeptonPT"      : 300    # MeV
	,"KPiPT"         : 500.0  # MeV
	,"DSumPT"        : 2500.0 # MeV
	,"DsDIRA"        : 0.999  # adimensional
	,"DsFDCHI2"      : 50.0   # adimensional
	,"DsMassWin"     : 80.0   # MeV
	,"DsAMassWin"    : 100.0  # MeV
    ,"M_MIN"         : 1920.0 # MeV
    ,"M_MAX"         : 2010.0 # MeV
	,"DsVCHI2DOF"    : 4.0    # adimensional
	,"PIDmu"         : 3. # adimensional
	,"PIDmuK"        : 0. # adimensional
	,"PIDmuP"        : 0. # adimensional
	,"PIDe"          : 0.  # adimensional
    ,"BDIRA"         : 0.999  # adimensional
	,"BVCHI2DOF"     : 6.0    # adimensional
	,"SPDmax"        : 600    # adimensional
	,"FakePrescale"  : 0.1     # adimensional
	,"Hlt2Line"      : "HLT_PASS_RE('Hlt2Topo.*Decision')"    # adimensiional
	  },
      'STREAMS'     : ['Semileptonic']	  
      }
    }

class B2DMuForLNuconf(LineBuilder) :
    """
    """
    
    __configuration_keys__ = (

        "MINIPCHI2"
        ,"GhostProb"    
        ,"KaonPIDK"    
        ,"PionPIDKTight"      
        ,"LeptonIPCHI2"      
        ,"LeptonPT"
        ,"KPiPT"
        ,"DSumPT"                
        ,"DsDIRA"        
        ,"DsFDCHI2"      
        ,"DsMassWin"     
        ,"DsAMassWin"
        ,"M_MIN"
        ,"M_MAX"
        ,"DsVCHI2DOF"    
        ,"PIDmu"            
        ,"PIDmuK"         
        ,"PIDmuP"      
        ,"PIDe"         
        ,"BDIRA"         
        ,"BVCHI2DOF"
        ,"SPDmax"
        ,"FakePrescale"
        ,"Hlt2Line"
        )
    
    __confdict__={}
        
    def __init__(self, name, config) :

        LineBuilder.__init__(self, name, config)
        self.config=config

        

        ############### MUON SELECTIONS ###################
        self.selmuon = Selection( "Mufor" + name,
                                  Algorithm = self._muonFilter(),
                                  RequiredSelections = [StdLooseMuons])
                                  
                                  
        self.selelectron = Selection( "Efor" + name,
                                  Algorithm = self._electronFilter(),
                                  RequiredSelections = [StdLooseElectrons])
                                  
                                  
        self.selFakemuon = Selection( "FakeMufor" + name,
                                  Algorithm = self._FakemuonFilter(),
                                  RequiredSelections = [StdAllNoPIDsMuons])                                  


        ############### KAON AND PION SELECTIONS ################
        
        self.selKaon = Selection( "Kfor" + name,
                                  Algorithm = self._kaonFilter(),
                                  RequiredSelections = [StdLooseKaons])
        
        self.selPion = Selection( "Pifor" + name,
                                  Algorithm = self._pionFilter(),
                                  RequiredSelections = [StdLoosePions])
                                  
                                                                          
        ################ D0 -> HH SELECTION ##########################
        
        self.seld02kpi = Selection( "D02KPifor" + name,
                                    Algorithm = self._D02KPiFilter(),
                                    RequiredSelections = [self.selKaon, self.selPion] )

        self.selb2D0MuX = makeb2DMuX('b2D0MuX' + name,
                                     DecayDescriptors = [ '[B- -> D0 mu-]cc','[B+ -> D0 mu+]cc'],
                                     MuSel = self.selmuon, 
                                     DSel = self.seld02kpi,
                                     BVCHI2DOF = config['BVCHI2DOF'],
                                     BDIRA = config['BDIRA']
                                     )
                                     
        self.selb2D0EX = makeb2DMuX('b2D0EX' + name,
                                     DecayDescriptors = [ '[B- -> D0 e-]cc','[B+ -> D0 e+]cc'],
                                     MuSel = self.selelectron, 
                                     DSel = self.seld02kpi,
                                     BVCHI2DOF = config['BVCHI2DOF'],
                                     BDIRA = config['BDIRA']
                                     )
                                                                          
        self.selFakeb2D0MuX = makeFakeb2DMuX('b2D0MuXFake' + name,
                                     DecayDescriptors = [ '[B- -> D0 mu-]cc','[B+ -> D0 mu+]cc'],
                                     FakeMuSel = self.selFakemuon, 
                                     DSel = self.seld02kpi,
                                     BVCHI2DOF = config['BVCHI2DOF'],
                                     BDIRA = config['BDIRA']
                                     )        
        
        ################# DECLARE THE STRIPPING LINES #################################

        GECs = { "Code":" ( recSummary(LHCb.RecSummary.nSPDhits,'Raw/Spd/Digits') < %(SPDmax)s )" % config,
                 "Preambulo": ["from LoKiTracks.decorators import *"]}

        ########## D0 -> HH ###########
        self.B2DMuForLNu = StrippingLine('b2D0MuX' + name+ 'Line', prescale = 1, selection = self.selb2D0MuX, FILTER  = GECs,HLT2 = config["Hlt2Line"])
        self.registerLine(self.B2DMuForLNu)
        
        self.B2DEForLNu = StrippingLine('b2D0EX' + name+ 'Line', prescale = 1, selection = self.selb2D0EX, FILTER  = GECs,HLT2 = config["Hlt2Line"])
        self.registerLine(self.B2DEForLNu)

        self.FakeB2DLForLNu = StrippingLine('b2D0LXFake' + name+ 'Line', prescale = config["FakePrescale"], selection = self.selFakeb2D0MuX, FILTER  = GECs,HLT2 = config["Hlt2Line"])
        self.registerLine(self.FakeB2DLForLNu)   

    def _muonFilter( self ):
        _code = "(MIPCHI2DV(PRIMARY)> %(LeptonIPCHI2)s) &(TRGHOSTPROB < %(GhostProb)s) & (PIDmu> %(PIDmu)s )& (PIDmu-PIDp>  %(PIDmuP)s  )& (PIDmu-PIDK>  %(PIDmuK)s  ) & (P> 3.0*GeV) & (PT > %(LeptonPT)s *MeV)" % self.config
        _mu = FilterDesktop( Code = _code )
        return _mu 
        
    def _electronFilter( self ):
        _code = "(MIPCHI2DV(PRIMARY)> %(LeptonIPCHI2)s) &(TRGHOSTPROB < %(GhostProb)s) & (PIDe > %(PIDe)s) & (P> 3.0*GeV) & (PT > %(LeptonPT)s *MeV)" % self.config
        _e = FilterDesktop( Code = _code )
        return _e 
        
    def _FakemuonFilter( self ):
        _code = "(MIPCHI2DV(PRIMARY)> %(LeptonIPCHI2)s) &(TRGHOSTPROB < %(GhostProb)s) & (P> 3.0*GeV) & (~ISMUON) & (PIDe < %(PIDe)s)" % self.config
        _Fakemu = FilterDesktop( Code = _code )
        return _Fakemu               

    def _pionFilter( self ):
        _code = "(P>2.0*GeV) & (PT > %(KPiPT)s *MeV)"\
                   "& (MIPCHI2DV(PRIMARY)> %(MINIPCHI2)s) & (PIDK< %(PionPIDKTight)s) &(TRGHOSTPROB < %(GhostProb)s) " % self.config
        _pi = FilterDesktop( Code = _code )
        return _pi

    def _kaonFilter( self ):
        _code = "(PIDK>%(KaonPIDK)s) & (MIPCHI2DV(PRIMARY)> %(MINIPCHI2)s) & (P>2.0*GeV) & (PT > %(KPiPT)s *MeV)"\
                   "    & (TRGHOSTPROB < %(GhostProb)s)" % self.config
        _ka = FilterDesktop( Code = _code )
        return _ka     

    def _D02KPiFilter( self ):
        _decayDescriptors = [ '[D0 -> K- pi+]cc' ]
        _combinationCut = "(ADAMASS('D0') < %(DsAMassWin)s *MeV) & (ACHILD(PT,1)+ACHILD(PT,2) > %(DSumPT)s *MeV) " % self.config
        _motherCut = "(SUMTREE( PT,  ISBASIC )> %(DSumPT)s * MeV) &(ADMASS('D0') < %(DsMassWin)s *MeV) & (VFASPF(VCHI2/VDOF) < %(DsVCHI2DOF)s) " \
                            "& (BPVVDCHI2 > %(DsFDCHI2)s) &  (BPVDIRA> %(DsDIRA)s)"  % self.config
        _d02kpi = CombineParticles( DecayDescriptors = _decayDescriptors,
                                    CombinationCut = _combinationCut,
                                    MotherCut = _motherCut)                            
        return _d02kpi
  
   
def makeb2DMuX(name,
               DecayDescriptors,
               MuSel,
               DSel,
               BVCHI2DOF,
               BDIRA):
               #due to tight cuts, B sideband adds virtually zero events, free
    _combinationCut = "(AM<10.2*GeV)"
    _motherCut = "  (MM<10.0*GeV) & (MM>0.0*GeV) & (VFASPF(VCHI2/VDOF)< %(BVCHI2DOF)s) & (BPVDIRA> %(BDIRA)s)  " \
                   " "  % locals()
    _B = CombineParticles(DecayDescriptors = DecayDescriptors,
                          CombinationCut = _combinationCut,
                          MotherCut = _motherCut)                          
    return Selection (name,
                      Algorithm = _B,
                      RequiredSelections = [MuSel, DSel])




def makeFakeb2DMuX(name,
               DecayDescriptors,
               FakeMuSel,
               DSel,
               BVCHI2DOF,
               BDIRA):
               #due to tight cuts, B sideband adds virtually zero events, free
    _combinationCut = "(AM<10.2*GeV)"
    _motherCut = "  (MM<10.0*GeV) & (MM>0.0*GeV) & (VFASPF(VCHI2/VDOF)< %(BVCHI2DOF)s) & (BPVDIRA> %(BDIRA)s)  " \
                   " "  % locals()
    _FakeB = CombineParticles(DecayDescriptors = DecayDescriptors,
                          CombinationCut = _combinationCut,
                          MotherCut = _motherCut)
                          
    return Selection (name,
                      Algorithm = _FakeB,
                      RequiredSelections = [FakeMuSel, DSel])

