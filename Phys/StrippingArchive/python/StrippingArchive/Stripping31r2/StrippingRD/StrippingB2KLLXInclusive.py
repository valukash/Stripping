###############################################################################
# (c) Copyright 2020 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################

__author__ = 'P. Owen'
__date__ = '21/11/2016'
__version__ = '$Revision: 1.0 $'

__all__ = ( 'B2KLLXInclusiveConf' ,'default_config')

"""
Stripping selection for inclusively selected B->KmumuX decays.
"""

from Gaudi.Configuration import *
from GaudiConfUtils.ConfigurableGenerators import  CombineParticles, FilterDesktop

from PhysSelPython.Wrappers import Selection, DataOnDemand, MergedSelection
from StrippingConf.StrippingLine import StrippingLine
from StrippingUtils.Utils import LineBuilder
from LHCbKernel.Configuration import *  #check if needed
from Configurables import SubstitutePID
from Configurables import SubPIDMMFilter


#################
#
#  Define Cuts here
#
#################


default_config = {
    'NAME'       : 'B2KLLXInclusive',
    'BUILDERTYPE' : 'B2KLLXInclusiveConf',
    'CONFIG'     :
    {
    # Incl (dimu) cuts
    'IPCHI2'     :   9.0,
    'FlightChi2'     :   100.0,
    'DIRA'           :   0.995,
    'VertexCHI2'     :     6.0,
    'LOWERMASS'       :  0.040, # MeV
    'UPPERMASS'       :  5000.0, # MeV
    'CORRM_MIN'       :  3000.0, # MeV
    'CORRM_MAX'       : 15000.0, # MeV
    # Track cuts
    'Track_CHI2nDOF'      :    3.0,
    'Track_GhostProb'     :    0.5,  

    # Muon cuts
    'Muon_MinIPCHI2'   :    16.0,
    'Muon_PIDmu'       :    0.0,
    'Muon_PIDmuK'       :   0.0,
    'Muon_PT'          :    500,

    # Muon cuts
    'Electron_MinIPCHI2'   :    16.0,
    'Electron_PIDe'       :    1.0,
    'Electron_PIDeK'       :    1.0,
    'Electron_PT'          :    500,
    # Muon cuts
    'Kaon_MinIPCHI2'   :    9.0,
    'Kaon_ProbNNK'       :    0.2,
    'Kaon_ProbNNKpi'       :    0.1,
    'Kaon_PT'          :    500,
    # Wrong sign combinations
    'WS'            :   False,

    # GEC
    'SpdMult'             :  450,
    'HLT_FILTER'             :  "HLT_PASS_RE('Hlt2DiMuonDetachedDecision')|HLT_PASS_RE('Hlt2DiMuonDetachedHeavyDecision')|HLT_PASS_RE('Hlt2SingleMuonDecision')",
     },
     'WGs'      : ['RD'],
     'STREAMS'  : ['Leptonic']
   }


#################
#
#  Make line here
#
#################

defaultName = "B2KLLXInclusive"


class B2KLLXInclusiveConf(LineBuilder) :

    __configuration_keys__ = (
       # Incl (dimu) cuts
        'IPCHI2',
        'FlightChi2',
        'VertexCHI2',
        'DIRA',
        'LOWERMASS',
        'UPPERMASS',
        'CORRM_MIN',
        'CORRM_MAX',

        # Track cuts
        'Track_CHI2nDOF',
        'Track_GhostProb',
        
        # Muon cuts
        'Muon_MinIPCHI2',
        'Muon_PIDmu',
        'Muon_PIDmuK',
        'Muon_PT',

        # Muon cuts
        'Electron_MinIPCHI2',
        'Electron_PIDe',
        'Electron_PIDeK',
        'Electron_PT',

        # Muon cuts
        'Kaon_MinIPCHI2',
        'Kaon_ProbNNK',
        'Kaon_ProbNNKpi',
        'Kaon_PT',

        # Choose WS combinations
        'WS',

        #GEC
        'SpdMult',
        'HLT_FILTER',

        )
    
    def __init__(self, name, config) :


        LineBuilder.__init__(self, name, config)

        self.name = name
        

        self.InclDiMuCombCut = "(AM > %(LOWERMASS)s *MeV) & " \
                                     "(AM < %(UPPERMASS)s *MeV)" %config
        self.InclDiMuCut = "(M > %(LOWERMASS)s) & " \
                           "(M < %(UPPERMASS)s)" %config
        self.InclDiMuCutNoPID = self.InclDiMuCut+ "& (INTREE((ABSID == 'mu+')&(switch(ISMUON,1,0) > 0)&(PIDmu > %(Muon_PIDmu)s)))" %config#&(PIDmu-PIDK > %(Muon_PIDmuK)s))" %config

        self.InclKLLCut = "(BPVCORRM > %(CORRM_MIN)s *MeV) & " \
                           "(BPVCORRM < %(CORRM_MAX)s *MeV) &" \
                           "(BPVDIRA > %(DIRA)s) & " \
                           "(BPVVDCHI2 > %(FlightChi2)s) & " \
                           "(VFASPF(VCHI2/VDOF) < %(VertexCHI2)s)" %config

        self.TrackCuts = "(TRCHI2DOF < %(Track_CHI2nDOF)s) & (TRGHP < %(Track_GhostProb)s)" %config
        #self.TrackCuts = "(TRCHI2DOF < %(Track_CHI2nDOF)s)" %config
        
        self.MuonCut = self.TrackCuts + " & (MIPCHI2DV(PRIMARY) > %(Muon_MinIPCHI2)s) & " \
                                             " (PIDmu> %(Muon_PIDmu)s) & " \
                                             " (PIDmu-PIDK> %(Muon_PIDmuK)s) & "\
                                             " (PT > %(Muon_PT)s)" %config
        self.MuonCutNoPID = self.TrackCuts + " & (MIPCHI2DV(PRIMARY) > %(Muon_MinIPCHI2)s) & " \
                                             " (PT > %(Muon_PT)s)" %config
        self.ElectronCut = "(PT > %(Electron_PT)s *MeV) & "\
                           "(PIDe > %(Electron_PIDe)s ) & "\
                           "(PIDe-PIDK > %(Electron_PIDeK)s ) & "\
                           "(MIPCHI2DV(PRIMARY) > %(Electron_MinIPCHI2)s)" % config
        self.ElectronCutNoPID = "(PT > %(Electron_PT)s *MeV) & "\
                           "(MIPCHI2DV(PRIMARY) > %(Electron_MinIPCHI2)s)" % config
        self.DiElectronCut = "(ID=='J/psi(1S)') & "\
                           "(MINTREE(ABSID<14,PT) > %(Electron_PT)s *MeV) & "\
                           "(MINTREE(ABSID<14,PIDe) > %(Electron_PIDe)s) & "\
                           "(MINTREE(ABSID<14,PIDe-PIDK) > %(Electron_PIDeK)s) & "\
                           "(MIPCHI2DV(PRIMARY) > %(Electron_MinIPCHI2)s)" % config
        self.DiElectronCutNoPID = "(ID=='J/psi(1S)') & "\
                           "(MINTREE(ABSID<14,PT) > %(Electron_PT)s *MeV) & "\
                           "(MIPCHI2DV(PRIMARY) > %(Electron_MinIPCHI2)s)" % config
        self.KaonCut = self.TrackCuts + " & (MIPCHI2DV(PRIMARY) > %(Kaon_MinIPCHI2)s) & " \
                                             " (PPINFO(PROBNNK) > %(Kaon_ProbNNK)s) & " \
                                             " (PPINFO(PROBNNK)-PPINFO(PROBNNpi) > %(Kaon_ProbNNKpi)s) & "\
                                             " (PT > %(Kaon_PT)s)" %config
        self.KaonCutNoPID = self.TrackCuts + " & (MIPCHI2DV(PRIMARY) > %(Kaon_MinIPCHI2)s) & " \
                                             " (PT > %(Kaon_PT)s)" %config

        #self.KstarFilterCut  = self.KstarCut + " & (INTREE(ABSID=='K+') & " + self.KaonCut + ") & (INTREE(ABSID=='pi+') & " + self.PionCut + ")"

        self.Electrons = self.__Electrons__(config)
        self.ElectronsNoPID = self.__Electrons__(config,NoPID=True)
        self.Muons = self.__Muons__(config)
        self.MuonsNoPID = self.__Muons__(config,NoPID=True)
        self.Kaons = self.__Kaons__(config)
        self.KaonsNoPID = self.__Kaons__(config,NoPID=True)
        self.InclDimu = self.__InclDimu__(config,doWS=False)
        self.InclDimuNoPID = self.__InclDimu__(config,NoPID=True,doWS=False)
        self.InclDielectron = self.__InclDielectron__(config,doWS=False)
        self.InclDielectronNoPID = self.__InclDielectron__(config,NoPID=True,doWS=False)
        self.InclLeptons = self.__DiLeptons__(config)
        self.InclLeptonsNoPID = self.__DiLeptons__(config,NoPID=True)
        self.InclKLL = self.__InclKLL__(config)
        self.InclKLLNoLeptonPID = self.__InclKLL__(config,NoMuonPID=True)
        self.InclKLLNoHadronPID = self.__InclKLL__(config,NoHadronPID=True)

        


        # inclusive dimuon line
        self.inclusive_KMuMu_line =  StrippingLine(
            self.name+"_InclKLLLine",
            prescale = 1,
            FILTER = {
            'Code' : " ( recSummary(LHCb.RecSummary.nSPDhits,'Raw/Spd/Digits') < %(SpdMult)s )" %config ,
            'Preambulo' : [
            "from LoKiNumbers.decorators import *", "from LoKiCore.basic import LHCb"
            ]
            },
           # HLT=config["HLT_FILTER"],
            algos=[self.InclKLL]
            )

        self.registerLine( self.inclusive_KMuMu_line )

        self.inclusive_KMuMuNoLeptonPID_line =  StrippingLine(
            self.name+"_InclKLLLine_NoLeptonPID",
            prescale = 0.02,
            FILTER = {
            'Code' : " ( recSummary(LHCb.RecSummary.nSPDhits,'Raw/Spd/Digits') < %(SpdMult)s )" %config ,
            'Preambulo' : [
            "from LoKiNumbers.decorators import *", "from LoKiCore.basic import LHCb"
            ]
            },
           # HLT=config["HLT_FILTER"],
            algos=[self.InclKLLNoLeptonPID]
            )

        self.inclusive_KMuMuNoHadronPID_line =  StrippingLine(
            self.name+"_InclKLLLine_NoHadronPID",
            prescale = 0.05,
            FILTER = {
            'Code' : " ( recSummary(LHCb.RecSummary.nSPDhits,'Raw/Spd/Digits') < %(SpdMult)s )" %config ,
            'Preambulo' : [
            "from LoKiNumbers.decorators import *", "from LoKiCore.basic import LHCb"
            ]
            },
           # HLT=config["HLT_FILTER"],
            algos=[self.InclKLLNoHadronPID]
            )
        self.registerLine( self.inclusive_KMuMuNoLeptonPID_line )
        self.registerLine( self.inclusive_KMuMuNoHadronPID_line )
        # inclusive dimuon line around jpsi and psi2s

    def __Muons__(self, conf,NoPID = False):
        """
        Filter muons from StdLooseMuons
        """ 
        _code = self.MuonCut
        _muons = DataOnDemand(Location = 'Phys/StdLooseMuons/Particles')
        _name = "Selection_"+self.name+"_Muons"
        if NoPID:
          _code = self.MuonCutNoPID 
          _muons = DataOnDemand(Location = 'Phys/StdNoPIDsMuons/Particles')
          _name+="NoPID"
        _filter = FilterDesktop(Code = _code)
        _sel = Selection(_name,
                         RequiredSelections = [ _muons ] ,
                         Algorithm = _filter)
        return _sel

    def __Electrons__(self, conf,NoPID = False):
        """
        Filter electrons from StdLooseElectrons
        """  
        _code = self.ElectronCut
        _electrons = DataOnDemand(Location = 'Phys/StdLooseElectrons/Particles')
        _name = "Selection_"+self.name+"_Electrons"
        if NoPID:
          _code = self.ElectronCutNoPID 
          _electrons = DataOnDemand(Location = 'Phys/StdNoPIDsElectrons/Particles')
          _name+="NoPID"
        _filter = FilterDesktop(Code = _code)
        _sel = Selection(_name,
                         RequiredSelections = [ _electrons ] ,
                         Algorithm = _filter)
        return _sel

    def __DiLeptons__(self, conf,NoPID = False):
        """
        Merge electrons and muons
        """ 
        electrons = self.InclDielectron
        muons = self.InclDimu
        _name = "Selection_"+self.name+"_Leptons"
        if NoPID:
          electrons = self.InclDielectronNoPID 
          muons = self.InclDimuNoPID 
          _name+="NoPID"
        _sel = MergedSelection(_name,
                         RequiredSelections = [ muons,electrons ])
        return _sel
 

    def __Kaons__(self, conf,NoPID = False):
        """
        Filter kaons from StdLooseMuons
        """
        _code = self.KaonCut
        _name = "Selection_"+self.name+"_Kaons"
        if NoPID: 
          _code = self.KaonCutNoPID
          _name = "Selection_"+self.name+"_KaonsNoPID"

        _kaons = DataOnDemand(Location = 'Phys/StdNoPIDsKaons/Particles')
        _filter = FilterDesktop(Code = _code)
        _sel = Selection(_name,
                         RequiredSelections = [ _kaons ] ,
                         Algorithm = _filter)
        return _sel
        
    def __InclDielectron__(self, conf,NoPID=False, doWS=False):
        '''
        Create a new dimuon for high q2 inclusive B->Xmumu
        '''
        from StandardParticles import StdDiElectronFromTracks as DiElectrons
        #from StandardParticles import StdLooseDiElectron as DiElectrons
        
        _name = "Sel_"+self.name+"_electronfilter"
        _Code = self.DiElectronCut
        if NoPID:
          _Code = self.DiElectronCutNoPID
          _name+="NoPID"
        _Filter = FilterDesktop( Code = _Code )
        from PhysSelPython.Wrappers import Selection
        SelDiElectron = Selection(_name, Algorithm = _Filter,
                              RequiredSelections = [ DiElectrons ] )
        return SelDiElectron


    def __InclDimu__(self, conf,NoPID = False, doWS=False):
        '''
        Create a new dimuon for high q2 inclusive B->Xmumu
        '''
        
        from  GaudiConfUtils.ConfigurableGenerators import CombineParticles
        CombineDiMuon = CombineParticles()
        CombineDiMuon.DecayDescriptors = ["J/psi(1S) -> mu- mu+","[J/psi(1S) -> mu- e+]cc"]
        CombineDiMuon.MotherCut     = self.InclDiMuCut
        if NoPID:
          CombineDiMuon.MotherCut = self.InclDiMuCutNoPID 
        # choose
        if doWS == True:
            CombineDiMuon.DecayDescriptors = ["J/psi(1S) -> mu- mu+", "J/psi(1S) -> mu- mu-", "J/psi(1S) -> mu+ mu+","J/psi(1S) -> e+ e-","J/psi(1S) -> e+ e+","J/psi(1S) -> e- e-"]
        muons = self.Muons
        electrons = self.Electrons
        _name = "Sel_"+self.name+"DiMu"
        if NoPID:
          muons = self.MuonsNoPID
          electrons = self.ElectronsNoPID
          _name+="_NoPID"

        _sel = MergedSelection(_name+"_Merged",
                         RequiredSelections = [ muons,electrons ])
        from PhysSelPython.Wrappers import Selection
        SelDiMuon = Selection(_name, Algorithm = CombineDiMuon,
                              RequiredSelections = [_sel ] )
        return SelDiMuon
   


    def __InclKLL__(self, conf,NoMuonPID = False,NoHadronPID=False):
        '''
        Create a new dimuon for high q2 inclusive B->Xmumu
        '''
        from  GaudiConfUtils.ConfigurableGenerators import CombineParticles
        CombineDiMuon = CombineParticles()
        CombineDiMuon.DecayDescriptors = ["[B+ -> J/psi(1S) K+]cc"]
        sel_name="InclKLL"
        CombineDiMuon.MotherCut     = self.InclKLLCut
        # choose
        leptons = self.InclLeptons
        kaons = self.Kaons
        _name = "Sel_"+self.name+"KMuMu"
        if NoMuonPID:
          leptons = self.InclLeptonsNoPID
          _name+="_NoLeptonPID"
        if NoHadronPID:
          kaons = self.KaonsNoPID
          _name+="_NoHadronPID"
        from PhysSelPython.Wrappers import Selection
        SelDiMuon = Selection(_name, Algorithm = CombineDiMuon,
                              RequiredSelections = [ leptons,kaons ] )
        return SelDiMuon

