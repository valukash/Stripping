###############################################################################
# (c) Copyright 2020 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""
Stripping Lines for W->eNu
Electroweak Group (Conveners: S.Bifani, J.Anderson; Stripping contact: W.Barter)

S.Bifani and D.Ward

We signal:  StdAllNoPIDsElectrons, PRS>50Mev & E_ECal/P>0.1 & E_HCal/P<0.05 & pT>20GeV & TTHits
We control: StdAllNoPIDsElectrons, PRS>50Mev & E_ECal/P>0.1 & E_HCal/P<0.05 & pT>15GeV          (10% PRESCALE)
"""

__all__ = 'WeConf', 'default_config'
__author__ = 'S. Bifani', 'D. Ward'

from Gaudi.Configuration import *
from GaudiConfUtils.ConfigurableGenerators import FilterDesktop
from PhysSelPython.Wrappers import SimpleSelection
from StrippingConf.StrippingLine import StrippingLine
from StrippingUtils.Utils import LineBuilder
from StandardParticles import StdAllNoPIDsElectrons
from GaudiKernel.SystemOfUnits import GeV


default_config = {
  'NAME'        : 'We',
  'BUILDERTYPE' : 'WeConf',
  'STREAMS'     : [ 'EW'  ],
  'WGs'         : [ 'QEE' ],
  'CONFIG'      : { 
    'We_Prescale'   : 1.0,
    'WeLow_Prescale': 0.1,
    'We_Postscale'  : 1.0,
    'PrsCalMin'     : 50.,
    'ECalMin'       :  0.10,
    'HCalMax'       :  0.05,
    'pT'            : 20. * GeV,
    'pTlow'         : 15. * GeV,
    'RawEvents'     : ["Muon", "Calo", "Rich", "Velo", "Tracker", "HC"],
  },
}

class WeConf( LineBuilder ) :

  __configuration_keys__ = default_config['CONFIG'].keys()
    
  def __init__( self, name, config ) :

    LineBuilder.__init__( self, name, config )

    # Define the cuts
    _cut    = "(PPINFO(LHCb.ProtoParticle.CaloPrsE,0)>%(PrsCalMin)s) & (PPINFO(LHCb.ProtoParticle.CaloEcalE,0)>P*%(ECalMin)s) & (PPINFO(LHCb.ProtoParticle.CaloHcalE,99999)<P*%(HCalMax)s) & (PT>%(pT)s)"%config
    _cutLow = '(PPINFO(LHCb.ProtoParticle.CaloPrsE,0)>%(PrsCalMin)s) & (PPINFO(LHCb.ProtoParticle.CaloEcalE,0)>P*%(ECalMin)s) & (PPINFO(LHCb.ProtoParticle.CaloHcalE,99999)<P*%(HCalMax)s) & (PT>%(pTlow)s)'%config

    # We signal
    self.registerLine(StrippingLine( name + 'Line',
      selection         = makeFilter( name+'We', _cut),
      prescale          = config[ 'We_Prescale'  ],
      postscale         = config[ 'We_Postscale' ],
      RequiredRawEvents = config[ 'RawEvents'    ],
    ))

    # We control
    self.registerLine(StrippingLine( name + 'LowLine',
      selection         = makeFilter( name+'WeLow', _cutLow ),
      prescale          = config[ 'WeLow_Prescale' ],
      postscale         = config[ 'We_Postscale'   ],
      RequiredRawEvents = config[ 'RawEvents'      ],
    ))


def makeFilter( name, code ):
  preambulo = ["from LoKiTracks.decorators import *"]
  return SimpleSelection(name, FilterDesktop, [StdAllNoPIDsElectrons], 
    Preambulo=preambulo, Code=code)
