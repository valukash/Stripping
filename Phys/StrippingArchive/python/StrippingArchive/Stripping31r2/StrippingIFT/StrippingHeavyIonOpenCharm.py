###############################################################################
# (c) Copyright 2020 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""
Stripping lines for selection of
    [D0 -> K- pi+]cc
    [D*(2010)+ -> (D0 -> K- pi+) pi+]cc

    [D+ -> K- pi+ pi+]cc
    [D+ -> K- K+ pi+]cc
    [D_s+ -> pi+ (phi(1020) -> K- K+)]cc
    [Lambda_c+ -> p+ K- pi+]cc
for open charm cross section measurement.
!!! based on those implemented by [Alex Pearce]
Done by Yanxi Zhang
Added : D0 -> pi pi, D0 -> K K, Lc -> pKK
"""

__author__ = ['Emilie MAURICE, Yanxi ZHANG']

__all__ = (
    'default_config',
    'HeavyIonOpenCharmConf'
)


from GaudiKernel.SystemOfUnits import MeV, GeV, mrad, mm
from GaudiConfUtils.ConfigurableGenerators import CombineParticles, FilterDesktop
#from Configurables import CombineParticles, FilterDesktop
from StandardParticles import (
    StdAllNoPIDsKaons,
    StdAllNoPIDsPions,
    StdAllNoPIDsProtons,
)
from PhysSelPython.Wrappers import Selection
from StrippingUtils.Utils import LineBuilder
from StrippingConf.StrippingLine import StrippingLine

default_config = {
    'NAME': 'HeavyIonOpenCharm',
    'WGs': ['IFT'],
    'BUILDERTYPE': 'HeavyIonOpenCharmConf',
    'STREAMS': ['IFT'],
    'CONFIG': {
        "odin": ["NoBeam","Beam1","Beam2","BeamCrossing"], 
        'Track_CHI2'         : 3,         # Minimum track chi2
        #
        #####################
        ###  D0 -> K- pi+   #
        #####################
        'D0_Daug_All_PT_MIN'         : 250.0*MeV,         # Minimum transverse momentum all D0 daughters must satisfy before fit
        'D0_Daug_AllA_PT_MIN'         : 500.0*MeV,         # Minimum transverse momentum at least one D0 daughters must satisfy after fit
        'D0_Daug_P_MIN'              : 3.0*GeV,           # Minimum D0 daughter momentum
        'D0_Daug_P_MAX'              : 1.E6*GeV,          # Maximum D0 daughter momentum,       'do it offline"
        'D0_Daug_ETA_MIN'            : 0.0,               # Minimum D0 daughter pseudorapidity, 'do it offline'
        'D0_Daug_ETA_MAX'            : 10.0,              # Maximum D0 daughter pseudorapidity, 'do it offline'
        'D0_K_PIDK_MIN'              : 5.0,               # Minimum Dpm daughter kaon DLLK       'in trigger of pAr hlt2'
        'D0_Pi_PIDK_MAX'             : 0.0,              # Maximum Dpm daughter pion DLLK,       'in trigger of pAr hlt2'
        'D0_Comb_ADOCAMAX_MAX'       : 0.5,               # DOCA
        'D0_Daug_All_BPVIPCHI2_MIN'  : 16.0,               # Minimum best primary vertex IP chi^2 all D0 daughters must satisfy
        'D0_VCHI2VDOF_MAX'           : 10.0,              # Maximum Dpm vertex chi^2 per vertex fit DoF 
        'D0_acosBPVDIRA_MAX'         : 35.0*mrad,         # Maximum angle between D0 momentum and D0 direction of flight, '10 at HLT2 of pAr'

        'D0_PVDispCut'               : '((BPVLTIME() > 0.500*picosecond))', # Primary vertex displacement requirement
        #        'D0_PVDispCut'               : '((BPVVDCHI2 > 9.0)|(BPVLTIME() > 0.100*picosecond))', # Primary vertex displacement requirement
        'D0_ADAMASS_WIN'             : 100.0*MeV,         # D0 mass window around the nominal D0 mass before the vertex fit, '90 MeV at HLT2 of pAr'

        'D0_Prescale':  1.0,
        'D0_Postscale': 1.0, 

        'D0_Hlt1Filter': None, #"HLT_PASS_RE('Hlt1SMOG.*Decision')",   #Hlt1SMOGD0KPiDecision ?
        'D0_Hlt2Filter': None, #"HLT_PASS_RE('Hlt2SMOGD02KPiDecision')",


        ##################################
        ###  D*+ -> (D0 -> K- pi+) pi+   #
        ##################################
        'Dst_AMDiff_MAX'        : 160.0*MeV,         # Maximum delta mass value m(D*{0,+}) - m(D0)
        'Dst_VCHI2VDOF_MAX'     : 10.0,              # Maximum D*+ vertex chi^2 per vertex DoF (_not_ applied to D*0)

        'Dst_Prescale'     : 1.0, 
        'Dst_Postscale'    : 1.0,

        'Dst_Hlt1Filter': None, #"HLT_PASS_RE('Hlt1SMOG.*Decision')",   #Hlt1SMOGD0KPiDecision ?
        'Dst_Hlt2Filter': None, #"HLT_PASS_RE('Hlt2SMOGD02KPiDecision')",


        ########################################
        ### D+ -> K- pi+ pi+, Ds+ -> K- K+ pi+, Lambda_c+ -> p+ K- pi+ #
        ########################################
        'D_Daug_All_PT_MIN'             : 200.0*MeV,         # Minimum transverse momentum all D daughters must satisfy
        'D_Daug_2of3_PT_MIN'            : 400.0*MeV, # Minimum transverse momentum at least 2 D daughters must satisfy
        'D_Daug_1of3_PT_MIN'            : 1000.0*MeV, # Minimum transverse momentum at least 1 D daughter must satisfy
        'D_Daug_All_BPVIPCHI2_MIN'      : 16.0,               # Minimum best primary vertex IP chi^2 all D daughters must satisfy
        'D_Daug_2of3_BPVIPCHI2_MIN'     : 4.0, # Minimum best PV IP chi^2 at least 2 D daughters must satisfy
        'D_Daug_1of3_BPVIPCHI2_MIN'     : 9.0, # Minimum best PV IP chi^2 at least 1 D daughter must satisfy
        'D_Daug_P_MIN'                  : 3.0*GeV,           # Minimum D daughter momentum
        'D_Daug_P_MAX'                  : 1.E6*GeV,          # Maximum D daughter momentum,       'do it offline"
        'D_Daug_ETA_MIN'                : 0.0,               # Minimum D daughter pseudorapidity, 'do it offline'
        'D_Daug_ETA_MAX'                : 10.0,              # Maximum D daughter pseudorapidity, 'do it offline'
        'D_K_PIDK_MIN'                  : 5.0,               # Minimum D daughter kaon DLLK       'in trigger of pAr hlt2'
        'D_Pi_PIDK_MAX'                 : 0.0,              # Maximum D daughter pion DLLK,       'in trigger of pAr hlt2'
        'D_P_PIDp_MIN'                  : 5.0,              # Maximum D (Lc) daughter proton DLLp,       'in trigger of pAr hlt2'
        'D_P_PIDpPIDK_MIN'              : 5.0,              # Maximum D (Lc) daughter proton DLLp-DLLK,       'in trigger of pAr hlt2'
        'D_Comb_ADOCAMAX_MAX'           : 0.5,         # Maximum distance of closest approach of D daughters
        #'D_Comb_ADOCAMAX_MAX'          : 0.5*mm,         # Maximum distance of closest approach of D daughters
        'D_VCHI2VDOF_MAX'               : 25.0,              # Maximum Dpm vertex chi^2 per vertex fit DoF
        'D_acosBPVDIRA_MAX'             : 35.0*mrad,         # Maximum angle between Dpm momentum and Dpm direction of flight

        'Dp_PVDispCut'               : '((BPVLTIME() > 0.500*picosecond))', # Primary vertex displacement requirement
        #        'Dp_PVDispCut'               : '((BPVVDCHI2 > 9.0)|(BPVLTIME() > 0.100*picosecond))', # Primary vertex displacement requirement

        'Ds_PVDispCut'               : '((BPVLTIME() > 0.500*picosecond))', # Primary vertex displacement requirement
        #        'Ds_PVDispCut'               : '((BPVVDCHI2 > 9.0)|(BPVLTIME() > 0.100*picosecond))', # Primary vertex displacement requirement

        'Lc_PVDispCut'               : '((BPVVDCHI2 > 9.0)|(BPVLTIME() > 0.100*picosecond))', # Primary vertex displacement requirement

        'Dp_ADAMASS_WIN'          : 100.0*MeV,         # D+ mass window around the nominal D+ mass before the vertex fit, '90 MeV at HLT2 of pAr'

        'Dp_Prescale': 1.0,
        'Dp_Postscale': 1.0, 

        'Dp_Hlt1Filter': None, #"HLT_PASS_RE('Hlt1SMOG.*Decision')",   #Hlt1SMOGDpmKKPiDecision ?
        'Dp_Hlt2Filter': None, #"HLT_PASS_RE('Hlt2SMOGDpm2KPiPiDecision')",
       

        'Ds_ADAMASS_WIN'          : 100.0*MeV,         # Ds+ mass window around the nominal Ds+ mass before the vertex fit, '90 MeV at HLT2 of pAr'

        'Ds_Prescale': 1.0,
        'Ds_Postscale': 1.0, 

        # HLT filters, only process events firing triggers matching the RegEx
        'Ds_Hlt1Filter': None, #"HLT_PASS_RE('Hlt1SMOG.*Decision')",   #Hlt1SMOGDpmKPiDecision ?
        'Ds_Hlt2Filter': None, #"HLT_PASS_RE('Hlt2SMOGDs2KKPiDecision')",

        'Lc_ADAMASS_WIN'          : 100.0*MeV,         # Lc+ mass window around the nominal Lc+ mass before the vertex fit, '90 MeV at HLT2 of pAr'

        'Lc_Prescale': 1.0,
        'Lc_Postscale': 1.0, 

        # HLT filters, only process events firing triggers matching the RegEx
        'Lc_Hlt1Filter': None, #"HLT_PASS_RE('Hlt1SMOG.*Decision')",   #Hlt1SMOGDpmKPiDecision ?
        'Lc_Hlt2Filter': None #"HLT_PASS_RE('Hlt2SMOGLc2KPPiDecision')",
        

    }
}


class HeavyIonOpenCharmConf(LineBuilder):
    """Creates LineBuilder object containing the stripping lines."""
    # Allowed configuration keys
    __configuration_keys__ = default_config['CONFIG'].keys()

    # Decay descriptors
    decays = {
          "D0kpi"      :["[D0 -> K- pi+]cc"],
          "Dst"        :["[D*(2010)+ -> D0 pi+]cc"],
          "Dp"         :["[D+ -> K- pi+ pi+]cc"],
          "Ds"         :["[D_s+ -> K+ K- pi+]cc"],
          "LcpKpi"     :["[Lambda_c+ -> p+ K- pi+]cc"],
          "D0kk"       :["[D0 -> K- K+]cc"],
          "D0pipi"     :["[D0 -> pi- pi+]cc"],
          "LcpKK"      :["[Lambda_c+ -> p+ K- K+]cc"],
          }

    def __init__(self, name, config):
        """Initialise this LineBuilder instance."""
        self.name = name
        self.config = config
        LineBuilder.__init__(self, name, config)


        # P: p+/p-
        # K: K+/K-
        # H: pi+/pi-, (hadron)
        D02kpi_name = '{0}D02kpi'.format(name)
        Dst2D0Pi_name = '{0}Dst2D0Pi'.format(name)
        Dp2KHH_name = '{0}Dp2KHH'.format(name)
        Ds2KKH_name = '{0}Ds2KKH'.format(name)
        Lc2PKpi_name = '{0}Lc2PKpi'.format(name)
        D02kk_name = '{0}D02kk'.format(name)
        D02pipi_name = '{0}D02pipi'.format(name)
        Lc2PKK_name = '{0}Lc2PKK'.format(name)

        self.inPions = StdAllNoPIDsPions
        self.inKaons = StdAllNoPIDsKaons
        self.inProtons = StdAllNoPIDsProtons

        self.selD02kpi = self.makeD02kpi(
            D02kpi_name,
            inputSel=[self.inKaons, self.inPions],
            decDescriptors=self.decays["D0kpi"]
        )

        self.selD02kk = self.makeD02kk(
            D02kk_name,
            inputSel=[self.inKaons],
            decDescriptors=self.decays["D0kk"]
        )

        self.selD02pipi = self.makeD02pipi(
            D02pipi_name,
            inputSel=[self.inPions],
            decDescriptors=self.decays["D0pipi"]
        )

        self.selDst2D0Pi = self.makeDst2D0Pi(
            name            = Dst2D0Pi_name,
            inputSel        =[self.selD02kpi, self.inPions],
            decDescriptors  =self.decays["Dst"]
        )

        self.selDp2KHH = self.makeThreeProng(
            name            = Dp2KHH_name,
            inputSel        =[self.inKaons, self.inPions],
            decDescriptors  =self.decays["Dp"],
            particle        = "D+"
        )
        self.selDs2KKH = self.makeThreeProng(
            name            =Ds2KKH_name,
            inputSel        =[self.inKaons, self.inPions],
            decDescriptors  =self.decays["Ds"],
            particle        = "D_s+"
        )
        self.selLc2PKpi = self.makeThreeProng(
            name            =Lc2PKpi_name,
            inputSel        =[self.inKaons, self.inPions, self.inProtons],
            decDescriptors  =self.decays["LcpKpi"],
            particle        = "Lambda_c+"
        )
        self.selLc2PKK = self.makeThreeProng(
            name            =Lc2PKK_name,
            inputSel        =[self.inKaons, self.inProtons],
            decDescriptors  =self.decays["LcpKK"],
            particle        = "Lambda_c+"
        )



        #odin = []
        #for odin_type in config['odin']:
        #    if odin_type not in ["NoBeam","Beam1","Beam2","BeamCrossing"]:
        #        continue
        #    odin.append( "(ODIN_BXTYP == LHCb.ODIN.%s)"%(odin_type,) )
        #odin = "|".join(odin)
        odin = "|".join( ["(ODIN_BXTYP == LHCb.ODIN.%s)"%(odin_type,) for odin_type in self.config['odin'] if odin_type in ["NoBeam","Beam1","Beam2","BeamCrossing"]])
        #"( ODIN_BXTYP == LHCb.ODIN.Beam1) | (ODIN_BXTYP == LHCb.ODIN.Beam2) | (ODIN_BXTYP == LHCb.ODIN.BeamCrossing) | (ODIN_BXTYP == LHCb.ODIN.NoBeam)",        ##[ 'NoBeam', 'BeamCrossing','Beam1','Beam2']

        self.line_D02kpi = self.make_line(
            name='{0}Line'.format(D02kpi_name),
            prescale  =self.config['D0_Prescale'],
            postscale =self.config['D0_Postscale'],
            selection =self.selD02kpi,
            HLT1      =self.config['D0_Hlt1Filter'],
            HLT2      =self.config['D0_Hlt2Filter'],
            ODIN      =odin,
            RequiredRawEvents = ["Trigger", "Muon", "Calo", "Rich", "Velo", "Tracker", "HC"]
        )

        self.line_D02pipi = self.make_line(
            name='{0}Line'.format(D02pipi_name),
            prescale  =self.config['D0_Prescale'],
            postscale =self.config['D0_Postscale'],
            selection =self.selD02pipi,
            HLT1      =self.config['D0_Hlt1Filter'],
            HLT2      =self.config['D0_Hlt2Filter'],
            ODIN      =odin,
            RequiredRawEvents = ["Trigger", "Muon", "Calo", "Rich", "Velo", "Tracker", "HC"]
        )

        self.line_D02kk = self.make_line(
            name='{0}Line'.format(D02kk_name),
            prescale  =self.config['D0_Prescale'],
            postscale =self.config['D0_Postscale'],
            selection =self.selD02kk,
            HLT1      =self.config['D0_Hlt1Filter'],
            HLT2      =self.config['D0_Hlt2Filter'],
            ODIN      =odin,
            RequiredRawEvents = ["Trigger", "Muon", "Calo", "Rich", "Velo", "Tracker", "HC"]
        )


        self.line_Dst2D0Pi= self.make_line(
            name='{0}Line'.format(Dst2D0Pi_name),
            prescale   =self.config['Dst_Prescale'],
            postscale  =self.config['Dst_Postscale'],
            selection  =self.selDst2D0Pi,
            HLT1       =self.config['Dst_Hlt1Filter'],
            HLT2       =self.config['Dst_Hlt2Filter'],
            ODIN       =odin,
            RequiredRawEvents = ["Trigger", "Muon", "Calo", "Rich", "Velo", "Tracker", "HC"]
        )

        self.line_Dp2KHH = self.make_line(
            name='{0}Line'.format(Dp2KHH_name),
            prescale   =self.config['Dp_Prescale'],
            postscale  =self.config['Dp_Postscale'],
            selection  =self.selDp2KHH,
            HLT1       =self.config['Dp_Hlt1Filter'],
            HLT2       =self.config['Dp_Hlt2Filter'],
            ODIN       =odin,
            RequiredRawEvents = ["Trigger", "Muon", "Calo", "Rich", "Velo", "Tracker", "HC"]
        )

        self.line_Ds2KKH = self.make_line(
            name='{0}Line'.format(Ds2KKH_name),
            prescale   =self.config['Ds_Prescale'],
            postscale  =self.config['Ds_Postscale'],
            selection  =self.selDs2KKH,
            HLT1       =self.config['Ds_Hlt1Filter'],
            HLT2       =self.config['Ds_Hlt2Filter'],
            ODIN       =odin,
            RequiredRawEvents = ["Trigger", "Muon", "Calo", "Rich", "Velo", "Tracker", "HC"]
        )

        self.line_Lc2PKpi = self.make_line(
            name='{0}Line'.format(Lc2PKpi_name),
            prescale   =self.config['Lc_Prescale'],
            postscale  =self.config['Lc_Postscale'],
            selection  =self.selLc2PKpi,
            HLT1       =self.config['Lc_Hlt1Filter'],
            HLT2       =self.config['Lc_Hlt2Filter'],
            ODIN       =odin,
            RequiredRawEvents = ["Trigger", "Muon", "Calo", "Rich", "Velo", "Tracker", "HC"]
        )


        self.line_Lc2PKK = self.make_line(
            name='{0}Line'.format(Lc2PKK_name),
            prescale   =self.config['Lc_Prescale'],
            postscale  =self.config['Lc_Postscale'],
            selection  =self.selLc2PKK,
            HLT1       =self.config['Lc_Hlt1Filter'],
            HLT2       =self.config['Lc_Hlt2Filter'],
            ODIN       =odin,
            RequiredRawEvents = ["Trigger", "Muon", "Calo", "Rich", "Velo", "Tracker", "HC"]
        )


    def make_line(self, name, selection, prescale, postscale, **kwargs):
        """Create the stripping line defined by the selection.

        Keyword arguments:
        name -- Base name for the Line
        selection -- Selection instance
        prescale -- Fraction of candidates to randomly drop before stripping
        postscale -- Fraction of candidates to randomly drop after stripping
        **kwargs -- Keyword arguments passed to StrippingLine constructor
        """
        # Only create the line with positive pre- and postscales
        # You can disable each line by setting either to a negative value
        if prescale > 0 and postscale > 0:
            line = StrippingLine(
                name,
                selection=selection,
                prescale=prescale,
                postscale=postscale,
                **kwargs
            )
            self.registerLine(line)
            return line
        else:
            return False

    def makeD02kpi(self, name, inputSel, decDescriptors):
        """Return a Selection instance for a D0 -> h- h+ decay.

        Keyword arguments:
        name -- Name to give the Selection instance
        inputSel -- List of inputs passed to Selection.RequiredSelections
        decDescriptors -- List of decay descriptors for CombineParticles
        """
        lclPreambulo = [
            'from math import cos'
        ]

        daugCuts = (
            '(PT > {0[D0_Daug_All_PT_MIN]})'
            '&(TRCHI2DOF<{0[Track_CHI2]})'
            '& (BPVIPCHI2() > {0[D0_Daug_All_BPVIPCHI2_MIN]})'
        ).format(self.config)

        pidFiducialCuts = (
            '(in_range({0[D0_Daug_P_MIN]}, P, {0[D0_Daug_P_MAX]}))'
            '& (in_range({0[D0_Daug_ETA_MIN]}, ETA, {0[D0_Daug_ETA_MAX]}))'
        ).format(self.config)

        kaonPIDCuts = (
            pidFiducialCuts +
            '& (PIDK-PIDpi > {0[D0_K_PIDK_MIN]})'
        ).format(self.config)

        pionPIDCuts = (
            pidFiducialCuts +
            '& (PIDK-PIDpi < {0[D0_Pi_PIDK_MAX]})'
        ).format(self.config)

        combCuts = "(ADAMASS('D0') < {0[D0_ADAMASS_WIN]}) & (AMINDOCA('')<{0[D0_Comb_ADOCAMAX_MAX]}) & (((APT1 > {0[D0_Daug_AllA_PT_MIN]}))|((APT2 > {0[D0_Daug_AllA_PT_MIN]})))".format(self.config)

        d0Cuts = (
            '(VFASPF(VCHI2/VDOF) < {0[D0_VCHI2VDOF_MAX]})'
            '& ({0[D0_PVDispCut]})'
            #            '& (BPVDIRA > cos({0[D0_acosBPVDIRA_MAX]}))'
        ).format(self.config)

        print decDescriptors

        _D0 = CombineParticles(
            DecayDescriptors=decDescriptors,
            Preambulo=lclPreambulo,
            DaughtersCuts={
                'pi+': '{0} & {1}'.format(daugCuts, pionPIDCuts),
                'K+': '{0} & {1}'.format(daugCuts, kaonPIDCuts)
                },
            CombinationCut=combCuts,
            MotherCut=d0Cuts
            )

        return Selection(name, Algorithm=_D0, RequiredSelections=inputSel)


    def makeD02kk(self, name, inputSel, decDescriptors):
        lclPreambulo = ['from math import cos' ]
        daugCuts = ('(PT > {0[D0_Daug_All_PT_MIN]})'
                    '&(TRCHI2DOF<{0[Track_CHI2]})'
                    '& (BPVIPCHI2() > {0[D0_Daug_All_BPVIPCHI2_MIN]})'
                    ).format(self.config)
        pidFiducialCuts = (
            '(in_range({0[D0_Daug_P_MIN]}, P, {0[D0_Daug_P_MAX]}))'
            '& (in_range({0[D0_Daug_ETA_MIN]}, ETA, {0[D0_Daug_ETA_MAX]}))'
            ).format(self.config)

        kaonPIDCuts = (pidFiducialCuts +
                       '& (PIDK-PIDpi > {0[D0_K_PIDK_MIN]})'
                       ).format(self.config)
        
        combCuts = "(ADAMASS('D0') < {0[D0_ADAMASS_WIN]}) & (AMINDOCA('')<{0[D0_Comb_ADOCAMAX_MAX]}) & (((APT1 > {0[D0_Daug_AllA_PT_MIN]}))|((APT2 > {0[D0_Daug_AllA_PT_MIN]})))".format(self.config)

        d0Cuts = ( '(VFASPF(VCHI2/VDOF) < {0[D0_VCHI2VDOF_MAX]})'
                   '& ({0[D0_PVDispCut]})'
                   #                   '& (BPVDIRA > cos({0[D0_acosBPVDIRA_MAX]}))'
                   ).format(self.config)
        
        _D0 = CombineParticles(
            DecayDescriptors=decDescriptors,
            Preambulo=lclPreambulo,
            DaughtersCuts={
                'K+': '{0} & {1}'.format(daugCuts, kaonPIDCuts)
                },
            CombinationCut=combCuts,
            MotherCut=d0Cuts
            )

        return Selection(name, Algorithm=_D0, RequiredSelections=inputSel)


    def makeD02pipi(self, name, inputSel, decDescriptors):
        lclPreambulo = ['from math import cos' ]
        daugCuts = ('(PT > {0[D0_Daug_All_PT_MIN]})'
                    '&(TRCHI2DOF<{0[Track_CHI2]})'
                    '& (BPVIPCHI2() > {0[D0_Daug_All_BPVIPCHI2_MIN]})'
                    ).format(self.config)
        pidFiducialCuts = (
            '(in_range({0[D0_Daug_P_MIN]}, P, {0[D0_Daug_P_MAX]}))'
            '& (in_range({0[D0_Daug_ETA_MIN]}, ETA, {0[D0_Daug_ETA_MAX]}))'
            ).format(self.config)

        pionPIDCuts = (
            pidFiducialCuts +
            '& (PIDK-PIDpi < {0[D0_Pi_PIDK_MAX]})'
        ).format(self.config)
        
        combCuts = "(ADAMASS('D0') < {0[D0_ADAMASS_WIN]}) & (AMINDOCA('')<{0[D0_Comb_ADOCAMAX_MAX]}) & (((APT1 > {0[D0_Daug_AllA_PT_MIN]}))|((APT2 > {0[D0_Daug_AllA_PT_MIN]})))".format(self.config)

        d0Cuts = ( '(VFASPF(VCHI2/VDOF) < {0[D0_VCHI2VDOF_MAX]})'
                   '& ({0[D0_PVDispCut]})'
                   #                   '& (BPVDIRA > cos({0[D0_acosBPVDIRA_MAX]}))'
                   ).format(self.config)
        
        _D0 = CombineParticles(
            DecayDescriptors=decDescriptors,
            Preambulo=lclPreambulo,
            DaughtersCuts={
                'pi+': '{0} & {1}'.format(daugCuts, pionPIDCuts),
                },
            CombinationCut=combCuts,
            MotherCut=d0Cuts
            )
        
        return Selection(name, Algorithm=_D0, RequiredSelections=inputSel)
    

    def makeDst2D0Pi(self, name, inputSel, decDescriptors):
        """Return a Selection instance for a D*+ -> D0 pi+ decay.

        Keyword arguments:
        name -- Name to give the Selection instance
        inputSel -- List of inputs passed to Selection.RequiredSelections
        decDescriptors -- List of decay descriptors for CombineParticles
        """
        combCuts = '((AM - AM1) < {0[Dst_AMDiff_MAX]})'.format(self.config)

        daugCuts = ( '(TRCHI2DOF<{0[Track_CHI2]})').format(self.config)
        dstarCuts = '(VFASPF(VCHI2/VDOF) < {0[Dst_VCHI2VDOF_MAX]})'.format( self.config)

        _Dst = CombineParticles(
            #name='Combine{0}'.format(name),
            DecayDescriptors=decDescriptors,
            CombinationCut=combCuts,
            MotherCut=dstarCuts,
            DaughtersCuts={ 'pi+': daugCuts }
        )

        return Selection(name, Algorithm=_Dst, RequiredSelections=inputSel)
    
    def makeThreeProng(self, name, inputSel, decDescriptors,particle):
        """Return a Selection instance for a D+/Ds+/Lc+ -> h+ h+ h- decay.

        Keyword arguments:
        name -- Name to give the Selection instance
        inputSel -- List of inputs passed to Selection.RequiredSelections
        decDescriptors -- List of decay descriptors for CombineParticles
        """
        #from the string to guess the particle nick name
        _part = {
              "D+":"Dp",
              "D-":"Dp",
              "D_s+":"Ds",
              "D_s-":"Ds",
              "Lambda_c+":"Lc",
              "Lambda_c~-":"Lc",
              }
        nickname =  _part[particle]
        #if particle not in _part.keys:
        #   print "particle name required!!"

        lclPreambulo = [
            'from math import cos'
        ]

        daugCuts = (
            '(PT > {0[D_Daug_All_PT_MIN]})'
            '&(TRCHI2DOF<{0[Track_CHI2]})'
            '& (BPVIPCHI2() > {0[D_Daug_All_BPVIPCHI2_MIN]})'
        ).format(self.config)
        pidFiducialCuts = (
            '(in_range({0[D_Daug_P_MIN]}, P, {0[D_Daug_P_MAX]}))'
            '& (in_range({0[D_Daug_ETA_MIN]}, ETA, {0[D_Daug_ETA_MAX]}))'
        ).format(self.config)
        pionPIDCuts = (
            pidFiducialCuts +
            '& (PIDK-PIDpi < {0[D_Pi_PIDK_MAX]})'
        ).format(self.config)
        kaonPIDCuts = (
            pidFiducialCuts +
            '& (PIDK-PIDpi > {0[D_K_PIDK_MIN]})'
        ).format(self.config)
        protonPIDCuts = (
            pidFiducialCuts +
            '& (PIDp-PIDpi > {0[D_P_PIDp_MIN]})'
            '& (PIDp-PIDK > {0[D_P_PIDpPIDK_MIN]})'
        ).format(self.config)

        _dauCuts = {
              "Dp":{
                 "pi+": '{0} & {1}'.format(daugCuts, pionPIDCuts),
                 "K+" : '{0} & {1}'.format(daugCuts, kaonPIDCuts)
              },
              "Ds":{
                 "pi+": '{0} & {1}'.format(daugCuts, pionPIDCuts),
                 "K+" : '{0} & {1}'.format(daugCuts, kaonPIDCuts)
              },
              "Lc":{
                 "pi+": '{0} & {1}'.format(daugCuts, pionPIDCuts),
                 "K+" : '{0} & {1}'.format(daugCuts, kaonPIDCuts),
                 "p+" : '{0} & {1}'.format(daugCuts, protonPIDCuts)
              }
        }
       

        # Add 10 MeV either side of the ADAMASS checks to account for the
        # poorer mass resolution before the vertex fit
        combCuts = (
            '(AMAXCHILD(PT) > {0[D_Daug_1of3_PT_MIN]})'
            '& (AMAXCHILD(BPVIPCHI2()) > {0[D_Daug_1of3_BPVIPCHI2_MIN]})'
            '& (ANUM(PT > {0[D_Daug_2of3_PT_MIN]}) >= 2)'
            '& (ANUM(BPVIPCHI2() > {0[D_Daug_2of3_BPVIPCHI2_MIN]}) >= 2)'
            "& (ACUTDOCA({0[D_Comb_ADOCAMAX_MAX]}, ''))"
        ).format(self.config)

        combCuts = combCuts +  ("&(ADAMASS('{0}') < {1})").format(particle,self.config[ nickname+"_ADAMASS_WIN" ])
        dCuts = (
            '(VFASPF(VCHI2/VDOF) < {0[D_VCHI2VDOF_MAX]})'
            #            '& (BPVDIRA > cos({0[D_acosBPVDIRA_MAX]}))'
        ).format(self.config)
        dCuts = dCuts + ('& ({0})').format(self.config[ nickname+"_PVDispCut" ])

        _dplus = CombineParticles(
            #name='Combine{0}'.format(name),
            DecayDescriptors=decDescriptors,
            Preambulo=lclPreambulo,
            CombinationCut=combCuts,
            MotherCut=dCuts,
            DaughtersCuts=_dauCuts[nickname]
        )

        return Selection(name, Algorithm=_dplus, RequiredSelections=inputSel)

