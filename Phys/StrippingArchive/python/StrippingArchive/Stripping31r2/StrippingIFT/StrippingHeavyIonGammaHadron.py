###############################################################################
# (c) Copyright 2020 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
'''
Stripping selections or Gamma+Hadron physics.
Based on those by Patrick K.
'''

__author__ = 'Cesar da Silva'
__date__ = '30/04/2017'
__version__ = '$Revision: 0 $'


__all__ = (
    'HeavyIonGammaHadronConf',
    'default_config'
    )

default_config =  {
    'NAME'            : 'HeavyIonGammaHadron',
    'WGs'             : ['IFT'],
#    'STREAMS'         : ['GammaHadronLine'],
    'STREAMS'         : ['IFT'],
    'BUILDERTYPE'     : 'HeavyIonGammaHadronConf',
    'CONFIG'          : {
        "odin": ["NoBeam","Beam1","Beam2","BeamCrossing"],
        'CheckPV'    :  True,
        'MicroBiasPrescale'            :  1.0,
        'MicroBiasPostscale'           :  1.0, 
        "MicroBiasHlt1Filter"         : None, #"(HLT_PASS('Hlt1MBMicroBiasVeloDecision'))", #(HLT_PASS('Hlt1MB.*Decision')) #Hlt1MBMicroBiasVelo
        "MicroBiasHlt2Filter"         : None, #"(HLT_PASS('Hlt2PassThroughDecision'))|(HLT_PASS('Hlt2SMOGPhysicsDecision'))",

        'MicroBiasLowMultPrescale'            :  1.0,
        'MicroBiasLowMultPostscale'           :  1.0, 
        "MicroBiasLowMultHlt1Filter"         : None, #"(HLT_PASS('Hlt1MBMicroBiasLowMultVeloDecision'))", #(HLT_PASS('Hlt1MB.*Decision')) #Hlt1MBMicroBiasVelo
        "MicroBiasLowMultHlt2Filter"         : None, #"(HLT_PASS('Hlt2PassThroughDecision'))",
        'NoBiasPrescale'            :  1.0, #0.2,
        'NoBiasPostscale'           :  1.0, 
        "NoBiasHlt1Filter"         : None, #"(HLT_PASS('Hlt1MBNoBiasLeadingCrossingDecision'))", #(HLT_PASS('Hlt1MB.*Decision')) #Hlt1MBMicroBiasVelo
        "NoBiasHlt2Filter"         : None, #"(HLT_PASS('Hlt2PassThroughDecision'))",
        'gammaPT1'              : 1500    # MeV/c
        ,'gammaPT2'             : 5000    # MeV/c
        ,'gammaCL'             : 0.9     # adimensional
        ,'gammaConvPT'         : 1500    # MeV/c
        ,'gammaConvMDD'        : 60     # MeV/cc
        ,'gammaConvIPCHI'      : 0     # adimensional
        ,'NoConvHCAL2ECAL'     : 0.05   # adimensional
        ,'LLProbNNe'           : 0.5    # adimensional
        ,'DDProbNNe'           : 0.3    # adimensional
        ,'ConvGhostLL'         : 0.3    # adimensional
        ,'ConvGhostDD'         : 0.3    # adimensional
        }
    }

from Gaudi.Configuration import *
#from GaudiConfUtils.ConfigurableGenerators import FilterDesktop, CombineParticles
from StandardParticles                     import StdLoosePhotons, StdAllLooseGammaLL, StdAllLooseGammaDD, StandardBasic
from Configurables               import FilterDesktop, CombineParticles
from StrippingConf.StrippingLine import StrippingLine
from StrippingUtils.Utils import LineBuilder
from PhysSelPython.Wrappers import Selection, DataOnDemand

class HeavyIonGammaHadronConf(LineBuilder):
    
    __configuration_keys__ = default_config['CONFIG'].keys()

    def __init__(self, name, config ): 
        
        LineBuilder.__init__(self, name, config)
        self.name = name 
        self.config = config
        print "inside GammaHadron",config
        odin = "|".join( ["(ODIN_BXTYP == LHCb.ODIN.%s)"%(odin_type,) for odin_type in config['odin'] if odin_type in ["NoBeam","Beam1","Beam2","BeamCrossing"]])

    ## Filter events with jets
#        filterJets = " ( CONTAINS('Phys/StdJets/Particles') > 0 ) "

        fltrCode_LL = "(MAXTREE(ISBASIC,TRGHOSTPROB)<%(ConvGhostLL)s) & (PT>(%(gammaConvPT)s-200.0)*MeV) & (MIPCHI2DV(PRIMARY)>%(gammaConvIPCHI)s) & (MAXTREE(ISBASIC,PPINFO( LHCb.P\
rotoParticle.ProbNNe,-1 ))>%(LLProbNNe)s)" % config
        self._trkFilter_LL = FilterDesktop(name = "LLFilterFor"+name, Code = fltrCode_LL )
        fltrCode_DD = "(MAXTREE(ISBASIC,TRGHOSTPROB)<%(ConvGhostDD)s) & (M<%(gammaConvMDD)s*MeV) & (PT>%(gammaConvPT)s*MeV) & (MIPCHI2DV(PRIMARY)>(2.0/3.0)*%(gammaConvIPCHI)s) & (M\
AXTREE(ISBASIC,PPINFO( LHCb.ProtoParticle.ProbNNe,-1 ))>%(DDProbNNe)s)" % config
        self._trkFilter_DD = FilterDesktop(name = "DDFilterFor"+name, Code = fltrCode_DD )
        fltrCode_nonConvLowPt = "(PT>%(gammaPT1)s*MeV) & (CL>%(gammaCL)s) & (MAXTREE(ISBASIC,PPINFO( LHCb.ProtoParticle.CaloNeutralHcal2Ecal,1000 ))<%(NoConvHCAL2ECAL)s)" % config
        self._trkFilterNonConvLowPt = FilterDesktop(name = "NoConvLowPtGammaFilterFor"+name, Code = fltrCode_nonConvLowPt )

        fltrCode_nonConv = "(PT>%(gammaPT2)s*MeV) & (CL>%(gammaCL)s) & (MAXTREE(ISBASIC,PPINFO( LHCb.ProtoParticle.CaloNeutralHcal2Ecal,1000 ))<%(NoConvHCAL2ECAL)s)" % config
        self._trkFilterNonConv = FilterDesktop(name = "NoConvGammaFilterFor"+name, Code = fltrCode_nonConv )

        self.convPhotons_LL = DataOnDemand(Location='Phys/StdAllLooseGammaLL/Particles')
        self.convPhotons_DD = DataOnDemand(Location='Phys/StdAllLooseGammaDD/Particles')
        stdPhotons     = DataOnDemand(Location='Phys/StdLoosePhotons/Particles')
        #
        # Clean up the converted photons to reduce the timing
        self.convPhotons_LL_clean = Selection( 'PhotonConvFilterLL' + name, Algorithm = self._trkFilter_LL, RequiredSelections = [self.convPhotons_LL])
        self.convPhotons_DD_clean = Selection( 'PhotonConvFilterDD' + name, Algorithm = self._trkFilter_DD, RequiredSelections = [self.convPhotons_DD])
    # Clean up the non-converted photons to reduce the timing
        self.stdLowPtPhotons_clean = Selection( 'LowPtPhotonFilter' + name, Algorithm = self._trkFilterNonConvLowPt, RequiredSelections = [stdPhotons])
        self.stdPhotons_clean = Selection( 'PhotonFilter' + name, Algorithm = self._trkFilterNonConv, RequiredSelections = [stdPhotons])

        self.NoConvLowPtGammaLine = StrippingLine( 
            name = 'NoConvLowPtGamma',
            prescale  = 0.05,
            postscale  = self.config['MicroBiasPostscale'],
            HLT1       =self.config['MicroBiasHlt1Filter'],
            HLT2       =self.config['MicroBiasHlt2Filter'],
            #              FILTER     = filterJets,
            checkPV   = self.config['CheckPV'],
            RequiredRawEvents = ["Trigger", "Muon", "Calo", "Rich", "Velo", "Tracker"],
            ODIN      = odin,
            selection = self.stdLowPtPhotons_clean  
            )
        self.registerLine( self.NoConvLowPtGammaLine )

        self.NoConvGammaLine = StrippingLine(
            name = 'NoConvGamma',
            prescale  = 1.0,
            postscale  = self.config['MicroBiasPostscale'],
            HLT1       =self.config['MicroBiasHlt1Filter'],
            HLT2       =self.config['MicroBiasHlt2Filter'],
            #              FILTER     = filterJets,
            checkPV   = self.config['CheckPV'],
            RequiredRawEvents = ["Trigger", "Muon", "Calo", "Rich", "Velo", "Tracker"],
            ODIN      = odin,
            selection = self.stdPhotons_clean
            )
        self.registerLine( self.NoConvGammaLine )


        self.LLGammaLine = StrippingLine(
            name = 'LLGamma',
            prescale  = 0.1,
            postscale  = self.config['MicroBiasPostscale'],
            HLT1       =self.config['MicroBiasHlt1Filter'],
            HLT2       =self.config['MicroBiasHlt2Filter'],
            #              FILTER     = filterJets,
            checkPV   = self.config['CheckPV'],
            RequiredRawEvents = ["Trigger", "Muon", "Calo", "Rich", "Velo", "Tracker"],
            ODIN      = odin,
            selection = self.convPhotons_LL_clean
            )
        self.registerLine( self.LLGammaLine )

        self.DDGammaLine = StrippingLine(
            name = 'DDGamma',
            prescale  = 0.1,
            postscale  = self.config['MicroBiasPostscale'],
            HLT1       =self.config['MicroBiasHlt1Filter'],
                HLT2       =self.config['MicroBiasHlt2Filter'],
            #              FILTER     = filterJets,
            checkPV   = self.config['CheckPV'],
            RequiredRawEvents = ["Trigger", "Muon", "Calo", "Rich", "Velo", "Tracker"],
            ODIN      = odin,
            selection =self.convPhotons_DD_clean
            )
        self.registerLine( self.DDGammaLine )
