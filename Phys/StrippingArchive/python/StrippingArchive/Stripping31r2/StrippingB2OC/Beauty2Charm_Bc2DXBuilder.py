###############################################################################
# (c) Copyright 2020 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from copy import deepcopy
from Gaudi.Configuration import *
from GaudiConfUtils.ConfigurableGenerators import CombineParticles
from PhysSelPython.Wrappers import Selection, MergedSelection
from Beauty2Charm_LoKiCuts import LoKiCuts
from Beauty2Charm_Utils import *

class Bc2DXBuilder(object):
     
    def __init__(self,d,dst,hh,pions,kaons,config):
   
        self.config = config
        self.d = d
        self.dst = dst
        self.hh = hh
        self.pions   = [pions]
        self.kaons   = [kaons]
        self.lines  = []

        d_hh_pid = self.d.hh_pid
        d_hhhh_pid = self.d.hhhh_pid
        d_hhh_pid_tight =  [filterSelection('Bc2DXD2HHH',"(MM > 1810*MeV) & (MM < 2030*MeV)",self.d.hhh_pid_tight)]
        dst_hh_pid = self.dst.d0pi
        dst_hhhh_pid = self.dst.d0pi_hhhh_pid

        self._makeBc2D0X("D02HHPID",d_hh_pid,self.config)
        self._makeBc2D0X("D02HHHHPID",d_hhhh_pid,self.config)
        self._makeBc2DX("D2HHHPID",d_hhh_pid_tight,self.config)
        self._makeBc2DstX("Dstar2D0PiD02HHPID",dst_hh_pid,self.config)
        self._makeBc2DstX("Dstar2D0PiD02HHHHPID",dst_hhhh_pid,self.config)

    def _makeBc2D0X(self,bname,d,config):
        decays = {'Bc2D0Pi':          ["[B_c+ -> D0 pi+]cc"],
                  'Bc2D0K':           ["[B_c+ -> D0 K+]cc"]
                  }
        inputs = {'Bc2D0Pi':          d+self.pions,
                  'Bc2D0K':           d+self.kaons
                  }
        bc2d0x = makeBc2D0XSels(decays,bname,inputs,config)
        self.lines.append(ProtoLine(bc2d0x,1.0))

    def _makeBc2DX(self,bname,d,config):
        config = deepcopy(self.config)
        config['BPVIPCHI2_MAX'] = '20'
        config['AM_MIN'] = '5850*MeV'
        config['SUMPT_MIN'] = '5850*MeV'
        decays = {'Bc2DPiPi':         ["B_c+ -> D+ rho(770)0","B_c- -> D- rho(770)0"],
                  'Bc2DHHWS':         ["B_c+ -> D- rho(770)+","B_c- -> D+ rho(770)-"], 
                  'Bc2DHHNP':         ["B_c+ -> D+ rho(770)+","B_c- -> D- rho(770)-"],
                  'Bc2DKPi':          ["[B_c+ -> D+ K*(892)0]cc","[B_c- -> D- K*(892)0]cc"],
                  'Bc2DKK':           ["B_c+ -> D+ phi(1020)","B_c- -> D- phi(1020)"],
                  'Bc2DPiK':          ["[B_c+ -> D+ K*(892)~0]cc","[B_c- -> D- K*(892)~0]cc"]
                  }
        inputs = {'Bc2DPiPi':         d+self.hh.pipi_pid,
                  'Bc2DHHWS':         d+self.hh.hh_ws_pid,
                  'Bc2DHHNP':         d+self.hh.hh_ws_pid,
                  'Bc2DKPi':          d+self.hh.kpi_pid,
                  'Bc2DKK':           d+self.hh.kk_pid,
                  'Bc2DPiK':          d+self.hh.kpi_pid 
                  }
        bc2dx = makeBc2DXSels(decays,bname,inputs,config)
        self.lines.append(ProtoLine(bc2dx,1.0))

    def _makeBc2DstX(self,bname,dst,config):
        decays = {'Bc2DstarPiPi':         ["B_c+ -> D*(2010)+ rho(770)0","B_c- -> D*(2010)- rho(770)0"],
                  'Bc2DstarHHWS':         ["B_c+ -> D*(2010)- rho(770)+","B_c- -> D*(2010)+ rho(770)-"], 
                  'Bc2DstarHHNP':         ["B_c+ -> D*(2010)+ rho(770)+","B_c- -> D*(2010)- rho(770)-"],
                  'Bc2DstarKPi':          ["[B_c+ -> D*(2010)+ K*(892)0]cc","[B_c- -> D*(2010)- K*(892)0]cc"],
                  'Bc2DstarKK':           ["B_c+ -> D*(2010)+ phi(1020)","B_c- -> D*(2010)- phi(1020)"],
                  'Bc2DstarPiK':          ["[B_c+ -> D*(2010)+ K*(892)~0]cc","[B_c- -> D*(2010)- K*(892)~0]cc"]
                  }
        inputs = {'Bc2DstarPiPi':         dst+self.hh.pipi_pid,
                  'Bc2DstarHHWS':         dst+self.hh.hh_ws_pid,
                  'Bc2DstarHHNP':         dst+self.hh.hh_ws_pid,
                  'Bc2DstarKPi':          dst+self.hh.kpi_pid,
                  'Bc2DstarKK':           dst+self.hh.kk_pid,
                  'Bc2DstarPiK':          dst+self.hh.kpi_pid
                  }
        bc2dstx = makeBc2DstXSels(decays,bname,inputs,config)
        self.lines.append(ProtoLine(bc2dstx,1.0))

