###############################################################################
# (c) Copyright 2020 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
#\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\#
'''
B -> Dhhh lifetime unbiased lines
'''
__author__ = ['Evelina Mihova Gersabeck, Manuel Schiller']

__all__ = ('B2DhhhLTUBConf',
           'default_config')

import re
from copy import deepcopy
from Gaudi.Configuration import *
from GaudiConfUtils.ConfigurableGenerators import (FilterDesktop, \
        CombineParticles)
from PhysSelPython.Wrappers import Selection, MergedSelection
from StrippingConf.StrippingLine import StrippingLine
from StrippingUtils.Utils import LineBuilder
from StandardParticles import (StdAllNoPIDsPions, StdAllNoPIDsKaons, \
        StdTightKaons, StdTightPions)
from Beauty2Charm_LoKiCuts import LoKiCuts
from Beauty2Charm_Utils import *
from GaudiConfUtils.ConfigurableGenerators import DaVinci__N3BodyDecays
from Configurables import SubPIDMMFilter

# Default configuration dictionary
default_config ={
  'NAME'   : 'B2DhhhLTUB',
  'BUILDERTYPE' : 'B2DhhhLTUBConf',
  'CONFIG' : {
    "ALL" : { # Cuts made on all charged input particles in all lines (expt. upstream)
    'TRCHI2DOF_MAX' : 4.,
    'PT_MIN'        : '100*MeV',
    'P_MIN'         : '1000*MeV',
    'MIPCHI2DV_MIN' : 4.,
    'TRGHP_MAX'     : 0.4
    },
    "PIDPION" : {
    'TRCHI2DOF_MAX' : 4.,
    'PT_MIN'        : '100*MeV',
    'P_MIN'         : '1000*MeV',
    'MIPCHI2DV_MIN' : 4.,
    'PIDK_MAX'      : 20.,
    'TRGHP_MAX'     : 0.4
    },
    "PIDKAON" : {
    'TRCHI2DOF_MAX' : 4.,
    'PT_MIN'        : '100*MeV',
    'P_MIN'         : '1000*MeV',
    'MIPCHI2DV_MIN' : 4.,
    'PIDK_MIN'      : -10.,
    'TRGHP_MAX'     : 0.4
    },
    "D2X" : { # Cuts made on all D's and Lc's used in all lines
    'ASUMPT_MIN'    : '1800*MeV',
    'ADOCA12_MAX'   : '0.5*mm',
    'ADOCA13_MAX'   : '0.5*mm',
    'ADOCA23_MAX'   : '0.5*mm',
    'ADOCA14_MAX'   : '0.5*mm',
    'ADOCA24_MAX'   : '0.5*mm',
    'ADOCA34_MAX'   : '0.5*mm',
    'ADOCA15_MAX'   : '0.5*mm',
    'ADOCA25_MAX'   : '0.5*mm',
    'ADOCA35_MAX'   : '0.5*mm',
    'ADOCA45_MAX'   : '0.5*mm',
    'VCHI2DOF_MAX'  : 10,
    'BPVVDCHI2_MIN' : 36,
    'BPVDIRA_MIN'   : 0,
    'MASS_WINDOW'   : '100*MeV'
    },
    "B2X" : { # Cuts made on all B's and Lb's used in all lines
    'SUMPT_MIN'     : '5000*MeV',
    'VCHI2DOF_MAX'  : 10,
    'BPVIPCHI2_MAX' : 25,
    'BPVLTIME_MIN'  : '0.2*ps',
    'BPVDIRA_MIN'   : 0.999,
    'AM_MIN'        : '4750*MeV', # Lb->X sets this to 5200*MeV
    'AM_MAX'        : '7000*MeV', # B->Dh+-h0 sets this to 5800*MeV
    'B2CBBDT_MIN'   : 0.05
    },
    "HHH": { # Cuts for PiPiPi, KPiPi analyese, etc.
    'MASS_WINDOW'   : {'A1':'3500*MeV', 'K1':'4000*MeV', 'PPH':'3600*MeV', 'PHH':'4000*MeV'},
    'KDAUGHTERS'    : {'PT_MIN':'100*MeV', 'P_MIN':'2000*MeV', 'PIDK_MIN':'-2'},
    'PiDAUGHTERS'   : {'PT_MIN':'100*MeV', 'P_MIN':'2000*MeV', 'PIDK_MAX':'10'},
    'pDAUGHTERS'    : {'PT_MIN':'100*MeV', 'P_MIN':'2000*MeV', 'PIDp_MIN':'-2'},
    'ADOCA12_MAX'   : '0.40*mm',
    'ADOCA13_MAX'   : '0.40*mm',
    'ADOCA23_MAX'   : '0.40*mm',
    'VCHI2DOF_MAX'  : 8,
    'BPVVDCHI2_MIN' : 16,
    'BPVDIRA_MIN'   : 0.98,
    'ASUMPT_MIN'    : '1250*MeV',
    'MIPCHI2DV_MIN' : 0.0,
    'BPVVDRHO_MIN'  : '0.1*mm',
    'BPVVDZ_MIN'    : '2.0*mm',
    'PTMIN1'       : '300*MeV',
    'PID'           : {'TIGHTERPI' : { 'P' : {'PIDp_MIN' : -10},
                                       'PI': {'PIDK_MAX' : 8},
                                       'K' : {'PIDK_MIN' : -10}},
                       'REALTIGHTK' : { 'P' : {'PIDp_MIN' : -10},
                                        'PI': {'PIDK_MAX' : 10},
                                        'K' : {'PIDK_MIN' : 4}}}
    },
    'PID' : {
    'P'  : {'PIDp_MIN' : -10},
    'PI' : {'PIDK_MAX' : 20},
    'K'  : {'PIDK_MIN' : -10},
    'TIGHT' : {    'P'  : {'PIDp_MIN' : -5},
                   'PI' : {'PIDK_MAX' : 0},
                   'K'  : {'PIDK_MIN' : 5}},
    },
    'FlavourTagging':[
    'B02DsKPiPiLTUBD2HHHB2DhhhLTUBLine',
    ],
    'RawEvents' : [
    ],
    'MDSTChannels':[
    ],
    'RelatedInfoTools' : [
      { "Type" : "RelInfoConeVariables",
        "ConeAngle" : 1.5,
        "Variables" : ['CONEANGLE', 'CONEMULT', 'CONEPTASYM'],
        "Location"  : 'P2ConeVar1'
      },
      { "Type" : "RelInfoConeVariables",
        "ConeAngle" : 1.7,
        "Variables" : ['CONEANGLE', 'CONEMULT', 'CONEPTASYM'],
        "Location"  : 'P2ConeVar2'
      },
      { "Type" : "RelInfoConeVariables",
        "ConeAngle" : 1.0,
        "Variables" : ['CONEANGLE', 'CONEMULT', 'CONEPTASYM'],
        "Location"  : 'P2ConeVar3'
      },
    ],
    'D0INC' : {'PT_MIN' : 1000, 'IPCHI2_MIN': 100},
    "Prescales" : { # Prescales for individual lines
    'RUN_BY_DEFAULT' : True, # False = lines off by default
    'RUN_RE'         : ['.*'],
    # Defaults are defined in, eg, B2DhhhLTUB_B2DXBuilder.py.  Put the full
    # line name here to override. E.g. 'B2D0HD2HHB2DhhhLTUBTOSLine':0.5.
    'B02DsKPiPiLTUBD2HHHB2DhhhLTUBLine' : 0.004,
    },
    'GECNTrkMax'   : 500
  },
  'STREAMS' : {
    'Bhadron' : [
    'StrippingB02DsKPiPiLTUBD2HHHB2DhhhLTUBLine',
   ]
  },
  'WGs' : [ 'B2OC' ]
}

class B2DhhhLTUBConf(LineBuilder):

    __configuration_keys__ = ('ALL', 'PIDPION', 'PIDKAON',
                              'D2X', 'B2X',
                              'HHH', 'PID', 'FlavourTagging', 'RelatedInfoTools',
                              'RawEvents', 'MDSTChannels', 'D0INC', 'Prescales', 'GECNTrkMax')

    class DBuilder(object):
        def awmFunctor(self, decays, min, max):
            wm = []
            for daughters in decays:
                 pids = ', '.join(["'%s'"%d for d in daughters])
                 wm.append("in_range(%s, AWM(%s), %s)" % (min, pids, max))
            return '('+('|'.join(wm))+')'

        def getCCs(self, decays):
            def getCC(decay):
                cc = []
                for d in decay:
                     if d.find('+') >=0: cc.append(d.replace('+', '-'))
                     elif d.find('-') >=0: cc.append(d.replace('-', '+'))
                     else: cc.append(d)
                return cc
            return [getCC(d) for d in decays]

        def makeSelName(self, pre, dec, post=''):
            name = pre+'2'
            for d in dec :
                 if   d == 'K*(892)+' : name += 'Kst+'
                 elif d == 'K*(892)-' : name += 'Kst-'
                 else                 : name += d
            name += post
            #print name
            return name

        def makeSel(self, parent, dec):
            sel = parent + ' ->'
            for d in dec : sel += ' ' + d
            #print sel
            return sel

        '''Produces all D mesons for the B2DhhhLTUB module.'''

        def __init__(self, pions, kaons, config, config_pid):
            self.pions = pions
            self.kaons = kaons
            self.config = deepcopy(config)
            self.config_pid = config_pid
            self.hhh  = self._makeD2hhh()

            self.hhh_pid = [filterPID('D2HHHPIDforBs2Dshhh', self.hhh, config_pid)]
            self.hhh_pid_tight = [filterPID('D2HHHPIDTIGHTforBs2Dshhh', self.hhh_pid,
                                            config_pid['TIGHT'])]

        def _makeD2ThreeBody(self, name, decays, wm, up, config, inputs):
            ''' Makes all D -> HHH selections.'''
            if up:
                 wm += '& (ANUM(ISUP)==1)'
                 name += 'UP'
            comboCuts = [LoKiCuts(['ASUMPT'], config).code(), wm, hasTopoChild()]
            comboCuts12 = [LoKiCuts(['ADOCA12'], config).code()]
            comboCuts.append(LoKiCuts(['ADOCA13'], config).code())
            comboCuts.append(LoKiCuts(['ADOCA23'], config).code())
            comboCuts = LoKiCuts.combine(comboCuts)
            comboCuts12 = LoKiCuts.combine(comboCuts12)
            momCuts = LoKiCuts(['VCHI2DOF', 'BPVVDCHI2', 'BPVDIRA'], config).code()
            cp = DaVinci__N3BodyDecays(Combination12Cut=comboCuts12,
                                       CombinationCut=comboCuts, MotherCut=momCuts,
                                       DecayDescriptors=decays)
            return  Selection('Proto'+name+'B2DhhhLTUB', Algorithm=cp,
                              RequiredSelections=inputs)

        def _massWindow(self, which):
            dm, units = LoKiCuts.cutValue(self.config['MASS_WINDOW'])
            if which == 'D0':
                 min = 1864.84 - dm # D0 - dm
                 max = 1864.84 + dm # D0 + dm
            elif which == 'D0wide' :
                 min = 1864.84 - dm - 150.
                 max = 1864.84 + dm + 150.
            elif which == 'D0narrow' :
                 min = 1864.84 - dm + 60.
                 max = 1864.84 + dm - 60.
            else:
                 min = 1869.62 - dm # D+ - dm
                 max = 1968.49 + dm # Ds+ + dm
            return ('%s*%s'%(min, units), '%s*%s'%(max, units))

        def getInputs(self, dec, up, extras) :
            inputs = [ ]
            inputs += extras

            # Pions ?
            if 'pi-' in dec or 'pi+' in dec :
                 inputs += [self.pions]
                 if up : inputs += [self.uppions]

            # kaons ?
            if 'K-' in dec or 'K+' in dec :
                 inputs += [self.kaons]
                 if up : inputs += [self.upkaons]

            return inputs

        # Implementation not using SubPID at all
        def _makeD2hhh(self, up=False):
            '''Makes D->hhh'''

            tag = ''
            if up: tag = 'UP'

            min, max = self._massWindow('D+')

            decaysp = [
                 ['pi+', 'pi+', 'pi-'],
                 ['pi+', 'pi+', 'K-'], ['K+', 'pi+', 'pi-'],
                 ['K+', 'pi+', 'K-'], ['K+', 'K+', 'pi-'],
                 ['K+', 'K+', 'K-']
                 ]
            decaysm = self.getCCs(decaysp)

            sels = []

            for dec in decaysp :
                 name = self.makeSelName('D+', dec, tag)
                 sel  = self.makeSel('D+', dec)
                 inputs = self.getInputs(dec, up, [])
                 sels += [ self._makeD2ThreeBody(name, [sel], self.awmFunctor([dec], min, max),
                                                 up, self.config, inputs) ]
            for dec in decaysm :
                 name = self.makeSelName('D-', dec, tag)
                 sel  = self.makeSel('D-', dec)
                 inputs = self.getInputs(dec, up, [])
                 sels += [ self._makeD2ThreeBody(name, [sel], self.awmFunctor([dec], min, max),
                                                 up, self.config, inputs) ]

            return [ MergedSelection( 'D2HHH'+tag+'B2DhhhLTUB', RequiredSelections = sels ) ]

    class LTUB_B2Dhhh_Builder(object):
        '''Makes all lifetime unbiased decays for the B2DhhhLTUB module.'''

        def __init__(self, d, config):
            self.config = deepcopy(config)
            self.pions = [topoInputs('PromptPionsForBs2Dshhh', [StdAllNoPIDsPions])]
            self.pions_loose = [topoInputs('PromptPionsL', [StdTightPions])]
#           self.pions_tight_pid = [filterSelection('B2OCLTUTIGHTPIforBs2DsKpipi', "(PIDK < 0)&(~ISMUON)", self.pions_loose)]
            self.pions_tight_pid = [filterSelection('B2OCLTUTIGHTPIforBs2DsKpipi', "(PROBNNpi>0.5)&(~ISMUON)", self.pions_loose)]
            self.kaons = [topoInputs('PromptKaonsForBs2Dshhh', [StdAllNoPIDsKaons])]
            self.kaons_loose = [topoInputs('PromptKaonsL', [StdTightKaons])]
            self.kaons_pid = [topoInputs('PromptKaonsPIDForBs2Dshhh', [StdTightKaons])]
#           self.kaons_tight_pid = [filterSelection('B2OCLTUTIGHTKforBs2DsKpipi', "(PIDK > 5) & (~ISMUON)", self.kaons_loose)]
            self.kaons_tight_pid = [filterSelection('B2OCLTUTIGHTKforBs2DsKpipi', "(PROBNNk > 0.5)&(~ISMUON)", self.kaons_loose)]
            self.d = d
            self.lines = []
            ds_hhh_pid_tight = [filterSelection('B2OCLTUDs2HHH', "(MM > 1920*MeV) & (MM < 2020*MeV)", self.d.hhh_pid_tight)]
            self._makeB02DHHH('D2HHH', ds_hhh_pid_tight)

        def _makeSels(self, decays, xtag, inputs):
            sels = []
            for tag, decay in decays.iteritems():
                comboCuts = LoKiCuts(['SUMPT', 'AM'], self.config).code()
                momCuts = [LoKiCuts(['VCHI2DOF'], self.config).code(),
                           hasTopoChildren()]
                momCuts = LoKiCuts.combine(momCuts)
                b2x = CombineParticles(DecayDescriptors=decay,
                                       CombinationCut=comboCuts,
                                       MotherCut=momCuts)
                sel = Selection(tag+xtag+'B2DhhhLTUB', Algorithm=b2x,
                                RequiredSelections=inputs[tag])
                sels.append(sel)
            return sels

        def _makeB02DHHH(self, dname, d2x):
            decays = {'B02DsKPiPiLTUB' : ["[B0 -> D- K+ pi+ pi-]cc"]}
            inputs = {'B02DsKPiPiLTUB' : d2x+self.kaons_tight_pid+self.pions_tight_pid}
            sel = self._makeSels(decays, dname, inputs)
            self.lines.append(ProtoLine(sel, 1.0))


    def __init__(self, moduleName, config) :

        LineBuilder.__init__(self, moduleName, config)

        # pre-filter inputs
        pions = filterInputs('PiForB2DhhhLTU', [StdAllNoPIDsPions], config['ALL'])
        kaons = filterInputs('KForB2DhhhLTU', [StdAllNoPIDsKaons], config['ALL'])

        # make D->X, etc. inputs
        d = self.DBuilder(pions, kaons, config['D2X'], config['PID'])

        # Unbiased lines
        ltub = self.LTUB_B2Dhhh_Builder(d, config['B2X'])
        self._makeLines(ltub.lines, config)

    def _makeLine(self, protoLine, config):
        tag = 'B2CBBDTB2DhhhLTUBFilter'
        default = config['Prescales']['RUN_BY_DEFAULT']
        run_re = config['Prescales']['RUN_RE']
        prescale_keys = config['Prescales'].keys()
        for line in protoLine.selections:
            name = line.name().replace(tag, '')+'Line'
            match = False
            for r in run_re:
                if re.match(r, name):
                    match = True
                    break
            if (not default) and (not match) and (not (name in prescale_keys)):
                continue
            tmpSel = Selection(line.name()+'FilterALL',
                               Algorithm=FilterDesktop(Code='ALL'),
                               RequiredSelections=[line])
            filter = {'Code' :
                      "(recSummaryTrack(LHCb.RecSummary.nLongTracks, TrLONG)"\
                      " < %s )" \
                      % config['GECNTrkMax'],
                      'Preambulo' : [ "from LoKiTracks.decorators import *",
                                      'from LoKiCore.functions    import *' ]
                      }
            #hlt = "HLT_PASS('Hlt1TrackAllL0Decision') & "\
            hlt = "(HLT_PASS_RE('Hlt2Topo.*Decision') | "\
                  "HLT_PASS_RE('Hlt2(Phi)?IncPhi.*Decision'))"

            if ((name in config['RawEvents'])):
                rawevent = ["Trigger", "Muon", "Calo", "Rich", "Velo", "Tracker"]
            else:
                rawevent = []
            sline = StrippingLine(name, protoLine.prescale(line, name, config),
                                  selection=tmpSel, checkPV=True, FILTER=filter,
                                  HLT2=hlt,
                                  EnableFlavourTagging = (name in config['FlavourTagging']),
                                  RequiredRawEvents = rawevent,
                                  MDSTFlag = (name in config['MDSTChannels']),
                                  RelatedInfoTools = config['RelatedInfoTools'] )

            self.registerLine(sline)

    def _makeLines(self, lines, config):
        for line in lines: self._makeLine(line, config)

# vim: sw=4:tw=78:ft=python:et
