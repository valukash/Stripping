###############################################################################
# (c) Copyright 2020 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
'''
Bs->quasi 2-body selection
Note: This script borrows heavily from that constructed by
Fred Blanc in the B2CharmlessQuasi2Body selection

Designed to look for KKhh final states with KK around the
phi mass

2016-11-21:
Updated for Run2 HLT line names, adding the HLT2 inclusive phi line.
2016-11-22:
Loosened kinematic cuts and track IP chi2 back to S20 values.
'''

__author__ = ['Sean Benson','Adam Morris','Laurence Carson']
__date__ = '22/11/2016'
__version__ = '3.2'

__all__ = ( 'BsPhiRhoConf',
            'mkDiTrackList',
            'mkKKTrackList',
            'mkBs2PRKKhh' )

default_config = {
    'NAME'        : 'BsPhiRho',
    'WGs'         : ['BnoC'],
    'BUILDERTYPE' : 'BsPhiRhoConf',
    'CONFIG'      : {'PRPrescale'     : 1.,
                     'PRResMinPT'     : 900.,
                     'PRResMinP'      : 1.,
                     'PRResMinMass'   : 0.,
                     'PRResMaxMass'   : 4000.,
                     'PRResVtxChiDOF' : 9.,
                     'PRBMinM'        : 4800.,
                     'PRBMaxM'        : 5600.,
                     'PRPhiWindow'    : 25.,
                     'PRBVtxChi2DOF'  : 9.,
                     'PRIPCHI2'       : 20,
                     },
    'STREAMS'     : ['Bhadron']
    }

from Gaudi.Configuration import *
from GaudiConfUtils.ConfigurableGenerators import CombineParticles, FilterDesktop
from PhysSelPython.Wrappers import Selection, DataOnDemand
from StrippingConf.StrippingLine import StrippingLine
from StrippingUtils.Utils import LineBuilder
from StandardParticles import StdNoPIDsPions

name = "BsPhiRho"

class BsPhiRhoConf(LineBuilder) :
    """
    Builder for BsPhiRho
    """

    __configuration_keys__ = default_config['CONFIG'].keys()

    def __init__(self, name, config) :
	self.name = name
        LineBuilder.__init__(self, name, config)

        self.hlt1Filter = "( HLT_PASS_RE('Hlt1(Two)?TrackMVADecision') )"
        self.hlt2Filter = "( HLT_PASS_RE('Hlt2Topo[234]BodyDecision') | HLT_PASS_RE('Hlt2(Phi)?IncPhiDecision') )"
        _trkFilter = FilterDesktop(Code = "(TRGHOSTPROB < 0.45) & (PT>500.*MeV) & (TRCHI2DOF < 3.5) & (MIPCHI2DV(PRIMARY) > 4.5)")
        
	self.TrackListhh = Selection( 'TrackList' + self.name,
                                    Algorithm = _trkFilter,
                                    RequiredSelections = [StdNoPIDsPions])

        self.DiTrackList = mkDiTrackList( name="DiTracksForCharmlessB" + self.name,
                                            trkList=self.TrackListhh,
                                            MinPTCut = config['PRResMinPT'],
                                            MinPCut = config['PRResMinP'],
                                            MinMassCut = config['PRResMinMass'],
                                            MaxMassCut = config['PRResMaxMass'],
                                            VtxChi2DOFCut = config['PRResVtxChiDOF'] )

        self.KKTrackList = mkKKTrackList( name="KKTracksForCharmlessB"+ self.name,
                                            MinPTCut = config['PRResMinPT'],
                                            MinPCut = config['PRResMinP'],
                                            PhiWindowMassCut = config['PRPhiWindow'] )

	Bs2PRName = self.name
        self.B2CharmlessPRKKhh = mkBs2PRKKhh( Bs2PRName,
                                               diTrkList=self.DiTrackList,
                                               diTrkListKK=self.KKTrackList,
                                               MinMassCut = config['PRBMinM'],
                                               MaxMassCut = config['PRBMaxM'],
                                               VtxChi2DOFCut = config['PRBVtxChi2DOF'],
                                               BIPchi2Cut = config['PRIPCHI2'])

        self.PRKKhhLine = StrippingLine( Bs2PRName+"Line",
                                         prescale = config['PRPrescale'],
                                         HLT1 = self.hlt1Filter,
                                         HLT2 = self.hlt2Filter,
                                         selection = self.B2CharmlessPRKKhh, EnableFlavourTagging = True,
                                         RelatedInfoTools = [ { "Type"         : "RelInfoConeVariables",
                                                                "ConeAngle"    : 0.8,
                                                                "Variables"    : ['CONEANGLE', 'CONEMULT', 'CONEPTASYM'],
                                                                "TopSelection" : self.TrackListhh,
                                                                "Location"     : 'P2ConeVar08' },
                                                              { "Type"         : "RelInfoConeVariables",
                                                                "ConeAngle"    : 1.0,
                                                                "Variables"    : ['CONEANGLE', 'CONEMULT', 'CONEPTASYM'],
                                                                "TopSelection" : self.TrackListhh,
                                                                "Location"     : 'P2ConeVar10' },
                                                              { "Type"         : "RelInfoConeVariables",
                                                                "ConeAngle"    : 1.3,
                                                                "Variables"    : ['CONEANGLE', 'CONEMULT', 'CONEPTASYM'],
                                                                "TopSelection" : self.TrackListhh,
                                                                "Location"     : 'P2ConeVar13' },
                                                              { "Type"         : "RelInfoConeVariables",
                                                                "ConeAngle"    : 1.7,
                                                                "Variables"    : ['CONEANGLE', 'CONEMULT', 'CONEPTASYM'],
                                                                "TopSelection" : self.TrackListhh,
                                                                "Location"     : 'P2ConeVar17' },
                                                              { "Type"         : "RelInfoVertexIsolation",
                                                                "Location"     : "VertexIsoInfo" } ] )

        self.registerLine(self.PRKKhhLine)

def mkDiTrackList( name,
                     trkList,
                     MinPTCut,
                     MinPCut,
                     MinMassCut,
                     MaxMassCut,
                     VtxChi2DOFCut ) :
    """
    Di-track selection
    """
    _diTrackPreVertexCuts = "(APT> %(MinPTCut)s *MeV) & (AP> %(MinPCut)s *GeV) & in_range( %(MinMassCut)s ,AM, %(MaxMassCut)s )" % locals()
    _diTrackPostVertexCuts = "(VFASPF(VCHI2/VDOF) < %(VtxChi2DOFCut)s )"% locals()
    
    _combineDiTrack = CombineParticles( DecayDescriptor="rho(770)0 -> pi+ pi-",
					CombinationCut = _diTrackPreVertexCuts,
                                         MotherCut = _diTrackPostVertexCuts )

    return Selection(name,
                     Algorithm = _combineDiTrack,
                     RequiredSelections = [ trkList ] )

def mkKKTrackList( name,
                     MinPTCut,
                     MinPCut,
                     PhiWindowMassCut ) :
    """
    KK selection
    """

    _code = "(ADMASS('phi(1020)')< %(PhiWindowMassCut)s *MeV) & (PT> %(MinPTCut)s *MeV) & (P> %(MinPCut)s *GeV)" % locals()
    _stdPhi = DataOnDemand(Location="Phys/StdTightPhi2KK/Particles")
    _phiFilter = FilterDesktop(Code = _code)

    return Selection (name,
                      Algorithm = _phiFilter,
                      RequiredSelections = [_stdPhi])

def mkBs2PRKKhh( name,
                  diTrkList,
                  diTrkListKK,
                  MinMassCut,
                  MaxMassCut,
                  VtxChi2DOFCut,
                 BIPchi2Cut) :
    """
    Bs to KKhh selection
    """
    _B2PRPreVertexCuts = "in_range( %(MinMassCut)s ,AM, %(MaxMassCut)s )" % locals()
    _B2PRPostVertexCuts = "(BPVDIRA > 0.9999) & (MIPCHI2DV(PRIMARY) < %(BIPchi2Cut)s) & (VFASPF(VCHI2/VDOF) < %(VtxChi2DOFCut)s )" % locals()

    _combineB2PR = CombineParticles( DecayDescriptor="B0 -> phi(1020) rho(770)0",
                                      MotherCut = _B2PRPostVertexCuts,
                                      CombinationCut = _B2PRPreVertexCuts )


    return Selection(name,
                     Algorithm = _combineB2PR,
                     RequiredSelections = [ diTrkList, diTrkListKK ])
