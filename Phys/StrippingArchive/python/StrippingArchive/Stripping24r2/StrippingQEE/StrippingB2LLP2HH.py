###############################################################################
# (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
'''
Stripping lines for dark pions into light hadrons analysis (produced from B, D or Ks).
'''

__author__ = [
    'Xabier Cid Vidal', 'Carlos Vazquez Sierra', 'Igor Kostiuk',
    'Wouter Hulsbergen'
]
__date__ = '25/01/2019'

__all__ = ('B2LLPV0_sConf', 'default_config')

from Gaudi.Configuration import *
from StandardParticles import StdAllNoPIDsPions
from PhysSelPython.Wrappers import CombineSelection
from StrippingConf.StrippingLine import StrippingLine
from StrippingUtils.Utils import LineBuilder
from GaudiKernel.SystemOfUnits import GeV
from LoKiPhys.functions import SOURCE, SIZE

default_config = {
    'NAME': 'B2LLPV0',
    'WGs': ['QEE'],
    'BUILDERTYPE': 'B2LLPV0_sConf',
    'CONFIG': {
        '2MixedMuPrescale': 1.0,
        '2MixedKPrescale': 1.0,
        '2MixedElPrescale': 1.0,
        '2MixedPPrescale': 1.0,
        'Multi_daugsP': 2. * GeV,
        'Multi_daugsChi2dv': 50.,
        'Multi_combChi2': 15.,
        'Multi_motherChi2dv': 25.,
        'Multi_motherChi2vx': 10.,
        'Multi_SUMPT': "1000*MeV",
        'Multi_DELTAM': "100*MeV",
        'Multi_VCHI2DOF': 25,
        'Multi_FDCHI2': 50,
        'Multi_DaughIPCHI2': 25,
        'Multi_ProbNNmu': 0.,
        'Multi_ProbNNK': 0.2,
        'Multi_ProbNNe': 0.2,
        'Multi_ProbNNp': 0.2
    },
    'STREAMS': ['BhadronCompleteEvent']
}

### Lines stored in this file:
# StrippingB2LLPV02MixedMuLine
# StrippingB2LLPV02MixedElLine
# StrippingB2LLPV02MixedKLine
# StrippingB2LLPV02MixedPLine


class B2LLPV0_sConf(LineBuilder):
    __configuration_keys__ = default_config['CONFIG'].keys()

    def __init__(self, name, config):
        LineBuilder.__init__(self, name, config)
        self.name = name
        self.config = config

        self.makeV02HH()

    ### Stripping lines:

    def makeV02HH(self):

        V02HHNOIP = {}
        for nn, pid, cutn in [["Mu", "PROBNNmu", "Multi_ProbNNmu"],
                              ["El", "PROBNNe", "Multi_ProbNNe"],
                              ["P", "PROBNNp", "Multi_ProbNNp"],
                              ["K", "PROBNNK", "Multi_ProbNNK"]]:

            V02HHNOIP[nn] = CombineSelection(
                "V02HHNOIP" + nn + self.name,
                StdAllNoPIDsPions,
                DecayDescriptor="KS0 -> pi+ pi-",
                DaughtersCuts={
                    "pi+":
                    "(P > %(Multi_daugsP)s) & (MIPCHI2DV(PRIMARY) > %(Multi_daugsChi2dv)s) & "
                    % self.config + "(" + pid + ">" + str(
                        self.config[cutn]) + ")",
                    "pi-":
                    "(P > %(Multi_daugsP)s) & (MIPCHI2DV(PRIMARY) > %(Multi_daugsChi2dv)s) & "
                    % self.config + "(" + pid + ">" + str(
                        self.config[cutn]) + ")"
                },
                CombinationCut="( ADOCACHI2CUT(%(Multi_combChi2)s, '') )" %
                self.config,
                MotherCut="( CHI2VX < %(Multi_motherChi2vx)s )" % self.config)

        V02Mixed, V02MixedLine = {}, {}
        for key in V02HHNOIP:
            V02Mixed[key] = self.V0CombineSelection(
                "Mixed" + key + "Selection", V02HHNOIP[key])

            V02MixedLine[key] = StrippingLine(
                self.name + "2Mixed" + key + "Line",
                algos=[V02Mixed[key]],
                prescale=self.config['2Mixed' + key + 'Prescale'])

        for key in V02MixedLine:
            self.registerLine(V02MixedLine[key])

    def V0CombineSelection(self, name, inputs):

        Preambulo = ["MS1 = ACHILD(M,1)", "MS2 = ACHILD(M,2)"]

        ## Generic cuts for detached selection
        comboCuts = "(ASUM(PT)>%(Multi_SUMPT)s) " \
                    "& ( abs(MS1-MS2)<%(Multi_DELTAM)s )"%self.config

        momCuts = "(VFASPF(VCHI2/VDOF)< %(Multi_VCHI2DOF)s)"\
                  "& (BPVVDCHI2> %(Multi_FDCHI2)s)" %self.config

        daughterCuts = {
            "KS0": "(BPVIPCHI2()> %(Multi_DaughIPCHI2)s)" % self.config
        }

        ## first half of decay depends on the mother, inputs depend also on the daughter
        decay = "B0 -> KS0 KS0"

        ## build B candidate
        return CombineSelection(
            name, [inputs],
            DecayDescriptor=decay,
            DaughtersCuts=daughterCuts,
            CombinationCut=comboCuts,
            MotherCut=momCuts,
            Preambulo=Preambulo)
