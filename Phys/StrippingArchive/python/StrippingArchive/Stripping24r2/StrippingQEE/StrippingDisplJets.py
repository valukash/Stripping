###############################################################################
# (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################

from Gaudi.Configuration import *
from CommonParticles.Utils import *
#from Configurables import FilterDesktop
#from PhysSelPython.Wrappers import Selection, DataOnDemand, FilterSelection
from StrippingConf.StrippingLine import StrippingLine
from StrippingUtils.Utils import LineBuilder
#from StandardParticles import StdHltJets
#from GaudiKernel.SystemOfUnits import GeV

__author__ = "Carlos Vazquez Sierra", "Igor Kostiuk", "Wouter Hulsbergen"
__all__ = "default_config", "DisplJetsConf"

default_config = {
    'NAME': 'DisplJets',
    'BUILDERTYPE': 'DisplJetsConf',
    'WGs': ['QEE'],
    'STREAMS': ['BhadronCompleteEvent'],
    'CONFIG': {
        'HLTCuts':
        "HLT_PASS('Hlt2JetsJetDisplacedSVDecision')|HLT_PASS('Hlt2JetsDiJetDisplacedSVSVLowPtDecision')",
        'HltJetsLine_Prescale':
        0.015,
        'RequiredRawEvents': ['Calo', 'Trigger', 'Velo', 'Tracker']
    }
}

# StrippingDisplJetsHltJetsLine


class DisplJetsConf(LineBuilder):
    __configuration_keys__ = default_config['CONFIG'].keys()

    def __init__(self, name, config):
        LineBuilder.__init__(self, name, config)
        #_selHltJets = FilterSelection(name + "HltJetsFilteredList", [ StdHltJets ], Code = "(PT > %(min_jet_pT)s)" %config)
        self.registerLine(
            StrippingLine(
                name + 'HltJetsLine',
                prescale=config['HltJetsLine_Prescale'],
                checkPV=False,
                HLT2=config['HLTCuts'],
                RequiredRawEvents=config['RequiredRawEvents']))
