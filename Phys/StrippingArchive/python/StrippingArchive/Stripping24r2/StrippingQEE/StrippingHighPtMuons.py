###############################################################################
# (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
## Stripping lines for high pT muons calibration:
# Tag and probe lines for Z->MuMu and Y->MuMu modes:
# |!StrippingHighPtMuonsZ02MuMuNoPIDsLine                       |  0.0190|        19|  1.105|  15.969|
# |!StrippingHighPtMuonsY2MuMuNoPIDsLine                        |  0.4520|       452|  1.069|   0.230|
# |!StrippingHighPtMuonsZ02MuMuNoPIDsLine_TIMING                |  0.0190|        19|  1.105|   0.209|
# |!StrippingHighPtMuonsY2MuMuNoPIDsLine_TIMING                 |  0.4520|       452|  1.069|   0.213|

__author__ = ['E. Dall Occo', 'C. Vazquez Sierra', 'M. Borsato']

__all__ = (
    'HighPtMuonsConf',
    'default_config',
)

from GaudiConfUtils.ConfigurableGenerators import CombineParticles
from PhysSelPython.Wrappers import SimpleSelection
from StrippingConf.StrippingLine import StrippingLine
from StrippingUtils.Utils import LineBuilder
from StandardParticles import StdAllNoPIDsMuons
from GaudiKernel.SystemOfUnits import GeV

default_config = {
    'NAME': 'HighPtMuons',
    'BUILDERTYPE': 'HighPtMuonsConf',
    'WGs': ['QEE'],
    'STREAMS': {
        'EW': ['StrippingHighPtMuonsZ02MuMuNoPIDsLine'],
        'Leptonic': ['StrippingHighPtMuonsY2MuMuNoPIDsLine']
    },
    'CONFIG': {
        'Prescale': 1.0,
        'Postscale': 1.0,
        'pT': 3. * GeV,
        'MZ0': 40. * GeV,
        'MminY': 8.5 * GeV,
        'MmaxY': 11 * GeV,
        'TrackChi2Mu': 5,
        'VtxChi2MuMu': 12,
        'YPIDcut': 0.5,
        'RawEvents': ["Muon", "Calo", "Rich", "Velo", "Tracker", "HC"],
    },
}


class HighPtMuonsConf(LineBuilder):
    __configuration_keys__ = default_config['CONFIG'].keys()

    def __init__(self, name, config):
        LineBuilder.__init__(self, name, config)

        self.registerLine(
            StrippingLine(
                name + 'Z02MuMuNoPIDsLine',
                prescale=config['Prescale'],
                postscale=config['Postscale'],
                RequiredRawEvents=config['RawEvents'],
                checkPV=False,
                selection=makeCombinationZ0(name + 'Z02MuMuNoPIDs', config),
            ))

        self.registerLine(
            StrippingLine(
                name + 'Y2MuMuNoPIDsLine',
                prescale=config['Prescale'],
                postscale=config['Postscale'],
                checkPV=False,
                selection=makeCombinationY(name + 'Y2MuMuNoPIDs', config),
            ))


def makeCombinationZ0(name, config):
    dcut = '(PT>%(pT)s)' % config
    mcut = '(MM>%(MZ0)s)' % config

    return SimpleSelection(
        name,
        CombineParticles, [StdAllNoPIDsMuons],
        DecayDescriptor='Z0 -> mu+ mu-',
        DaughtersCuts={
            'mu+': dcut,
            'mu-': dcut
        },
        MotherCut=mcut + ' & ((CHILDCUT(ISMUON,1))|(CHILDCUT(ISMUON,2)))',
        WriteP2PVRelations=False)


def makeCombinationY(name, config):
    dcut = '(TRCHI2DOF<%(TrackChi2Mu)s) & (PT>%(pT)s)' % config
    mMincut = '(MM>%(MminY)s)' % config
    mMaxcut = '(MM<%(MmaxY)s)' % config
    vtxChi2 = '(CHI2VXNDF<%(VtxChi2MuMu)s)' % config
    mcut = vtxChi2 + ' & ' + mMincut + ' & ' + mMaxcut
    pidcut = '(PROBNNmu>%(YPIDcut)s)' % config

    return SimpleSelection(
        name,
        CombineParticles, [StdAllNoPIDsMuons],
        DecayDescriptor='Upsilon(1S) -> mu+ mu-',
        DaughtersCuts={
            'mu+': dcut,
            'mu-': dcut
        },
        MotherCut=mcut + ' & ((CHILDCUT(' + pidcut + ',1))|(CHILDCUT(' + pidcut
        + ',2)))',
        WriteP2PVRelations=False)
