###############################################################################
# (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
# Stripping Lines for Z -> nH, n = 2,4,6,8,10
#
# Donal Hill
#
# Z02nH :     StdAllNoPIDsPions, pT>1GeV & MM>60GeV

__all__ = (
    'Z02nHConf',
    'default_config',
)

from GaudiConfUtils.ConfigurableGenerators import CombineParticles
from PhysSelPython.Wrappers import Selection
from StrippingConf.StrippingLine import StrippingLine
from StrippingUtils.Utils import LineBuilder
from StandardParticles import StdAllNoPIDsPions
from GaudiKernel.SystemOfUnits import GeV
from GaudiKernel.SystemOfUnits import MeV

default_config = {
    'NAME': 'Z02nH',
    'BUILDERTYPE': 'Z02nHConf',
    'WGs': ['QEE'],
    'STREAMS': ['EW'],
    'CONFIG': {
        'Z02nH_Prescale': 1.0,
        'Z02nH_Postscale': 1.0,
        'pT': 3. * GeV,
        'TrChi2': 5.,
        'pErr': 0.01,
        'vChi2': 10,
        'GhostProb': 0.4,
        'pTmin': 80. * GeV,  #Minimun pT of the array
        'MMmin': 60. * GeV,
        'MMmax': 120. * GeV,
    },
}


class Z02nHConf(LineBuilder):

    __configuration_keys__ = default_config['CONFIG'].keys()

    def __init__(self, name, config):

        LineBuilder.__init__(self, name, config)

        # Z02HH signal
        decay = 'Z0 -> pi+ pi-'
        self.registerLine(make_line(name + '2H', config, decay))

        # Z02HH WS
        decay = '[Z0 -> pi+ pi+]cc'
        self.registerLine(make_line(name + '2HWS', config, decay))

        # Z024H signal
        decay = 'Z0 -> pi+ pi- pi+ pi-'
        self.registerLine(make_line(name + '4H', config, decay))

        # Z024H WS
        decay = '[Z0 -> pi+ pi+ pi+ pi-]cc'
        self.registerLine(make_line(name + '4HWS', config, decay))

        # Z026H signal
        decay = 'Z0 -> pi+ pi- pi+ pi- pi+ pi-'
        self.registerLine(make_line(name + '6H', config, decay))

        # Z026H WS
        decay = '[Z0 -> pi+ pi+ pi+ pi- pi+ pi-]cc'
        self.registerLine(make_line(name + '6HWS', config, decay))

        # Z028H signal
        decay = 'Z0 -> pi+ pi- pi+ pi- pi+ pi- pi+ pi-'
        self.registerLine(make_line(name + '8H', config, decay))

        # Z028H WS
        decay = '[Z0 -> pi+ pi+ pi+ pi- pi+ pi- pi+ pi-]cc'
        self.registerLine(make_line(name + '8HWS', config, decay))


#===============================================================================


def make_combination(name, config, decay):
    # Define the cuts
    dcut = '(PT>%(pT)s) & (TRCHI2DOF<%(TrChi2)s) & ((PERR2)/(P*P)<%(pErr)s) & (TRGHOSTPROB<%(GhostProb)s)' % config
    ccut = 'APT>%(pTmin)s' % config
    mcut = '(MM>%(MMmin)s) & (MM<%(MMmax)s) & (VFASPF(VCHI2/VDOF)<%(vChi2)s)' % config

    algo = CombineParticles(
        DecayDescriptor=decay,
        DaughtersCuts={
            'pi+': dcut,
            'pi-': dcut
        },
        CombinationCut=ccut,
        MotherCut=mcut,
        WriteP2PVRelations=False)

    return Selection(
        name + 'Sel', Algorithm=algo, RequiredSelections=[StdAllNoPIDsPions])


def make_line(name, config, decay):
    return StrippingLine(
        name + 'Line',
        prescale=config['Z02nH_Prescale'],
        postscale=config['Z02nH_Postscale'],
        checkPV=False,
        selection=make_combination(name, config, decay))
