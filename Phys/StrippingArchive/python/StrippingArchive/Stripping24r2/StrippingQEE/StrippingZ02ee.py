###############################################################################
# (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
# Stripping Lines for Z->ee and studies of their background
# Electroweak Group (Conveners: S.Bifani, J.Anderson; Stripping contact: W.Barter)
#
# S.Bifani and D.Ward
#
# Z02ee signal:    StdAllNoPIDsElectrons, PRS>50Mev & E_ECal/P>0.1 & E_HCal/P<0.05 & pT>10GeV & MM>40GeV
# Z02ee same sign: StdAllNoPIDsElectrons, PRS>50Mev & E_ECal/P>0.1 & E_HCal/P<0.05 & pT>10GeV & MM>40GeV

__author__ = ['S.Bifani', 'D.Ward']

__all__ = (
    'Z02eeConf',
    'default_config',
)

from Gaudi.Configuration import *
from GaudiKernel.SystemOfUnits import GeV, MeV
from GaudiConfUtils.ConfigurableGenerators import CombineParticles
from PhysSelPython.Wrappers import SimpleSelection
from StandardParticles import StdAllNoPIDsElectrons
from StrippingConf.StrippingLine import StrippingLine
from StrippingUtils.Utils import LineBuilder

default_config = {
    'NAME': 'Z02ee',
    'BUILDERTYPE': 'Z02eeConf',
    'STREAMS': ['EW'],
    'WGs': ['QEE'],
    'CONFIG': {
        'Prescale': 1.0,
        'Postscale': 1.0,
        'ECalMin': 0.1,
        'HCalMax': 0.05,
        'PrsCalMin': 50. * MeV,
        'pT': 10. * GeV,
        'MMmin': 40. * GeV,
        'RawEvents': ["HC"],
    },
}


class Z02eeConf(LineBuilder):

    __configuration_keys__ = default_config['CONFIG'].keys()

    def __init__(self, name, config):

        LineBuilder.__init__(self, name, config)

        # Z02ee signal
        desc = 'Z0 -> e+ e-'
        sel = make_combination(name + 'Z02ee', config, desc)
        self.registerLine(
            StrippingLine(
                name + 'Line',
                selection=sel,
                prescale=config['Prescale'],
                postscale=config['Postscale'],
                RequiredRawEvents=config['RawEvents'],
            ))

        # Z02ee same sign
        desc = '[Z0 -> e- e-]cc'
        sel = make_combination(name + 'Z02eeSS', config, desc)
        self.registerLine(
            StrippingLine(
                name + 'SSLine',
                selection=sel,
                prescale=config['Prescale'],
                postscale=config['Postscale'],
                RequiredRawEvents=config['RawEvents'],
            ))


def make_combination(name, config, desc):
    # Define the cuts
    dcut = '((PT>%(pT)s) & (PPINFO(LHCb.ProtoParticle.CaloPrsE,0)>%(PrsCalMin)s) & (PPINFO(LHCb.ProtoParticle.CaloEcalE,0)>P*%(ECalMin)s) & (PPINFO(LHCb.ProtoParticle.CaloHcalE,99999)<P*%(HCalMax)s))' % config
    mcut = '(MM>%(MMmin)s)' % config

    return SimpleSelection(
        name,
        CombineParticles,
        [StdAllNoPIDsElectrons],
        DecayDescriptor=desc,
        DaughtersCuts={
            'e+': dcut,
            'e-': dcut
        },
        MotherCut=mcut,
        WriteP2PVRelations=False,
    )
