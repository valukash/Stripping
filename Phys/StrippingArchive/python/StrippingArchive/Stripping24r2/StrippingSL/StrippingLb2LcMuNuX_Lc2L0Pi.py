###############################################################################
# (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################

"""
Selection for [Lambda_b0 -> (Lambda_c+ -> Lambda0 pi+) mu+ nu_mu X]cc using StdVeryLooseLambdaLL
"""
__author__ = ['Scott Ely']
__date__ = '23/01/2019'
__version__ = '$Revision: 1.0 $'

from Gaudi.Configuration import *
from Configurables import FilterDesktop, CombineParticles
from PhysSelPython.Wrappers import Selection, DataOnDemand
from StrippingConf.StrippingLine import StrippingLine
from StrippingUtils.Utils import LineBuilder
from StandardParticles import StdLoosePions, StdLooseMuons, StdLooseKaons, StdLooseProtons, StdLooseElectrons, StdVeryLooseLambdaLL
from StandardParticles import StdNoPIDsPions, StdNoPIDsKaons,StdNoPIDsProtons,StdNoPIDsMuons
from GaudiKernel.SystemOfUnits import MeV, GeV, cm, mm

__all__ = ('Lb2LcMuNuX_Lc2L0PiConf'
           , 'default_config')

default_config = {
    'NAME'        : 'Lb2LcMuNuX_Lc2L0Pi',
    'WGs'         : ['Semileptonic'],
    'BUILDERTYPE' : 'Lb2LcMuNuX_Lc2L0PiConf',
    'CONFIG'      : {"GEC_nLongTrk"        : 250
                     ,"TTSpecs"            : {}
                     ,"HLT1"               : "HLT_PASS_RE('Hlt1.*Decision')"
                     ,"HLT2"               : "HLT_PASS_RE('Hlt2.*Decision')"
                     ,"Monitor"            : False
                     ,"UseNoPIDsInputs"    : False
                     ,"GhostProb"          : 0.35
                     ,"TRCHI2"             : 3.0
                     ,"MuonPIDmu"          : 0.0
                     ,"MuonPT"             : 1000*MeV
                     ,"MuonP"              : 6.0*GeV
                     ,"MuonIPCHI2"         : 9.0
                     ,"PionPT"             : 250.0*MeV
                     ,"PionP"              : 2.0*GeV
                     ,"PionIPCHI2"         : 4.0
                     ,"PionPIDK"           : 10.0
                     ,"LambdaLL_P"         : 2.0*GeV
                     ,"LambdaLL_PT"        : 500.0*MeV
                     ,"LambdaLL_ADMASS"    : 30.0*MeV
                     ,"LambdaLL_BPVVDCHI2" : 100.0
                     ,"LambdaLL_VCHI2DOF"  : 6.0
                     ,"Lc_VCHI2DOF"        : 6.0
                     ,"Lc_BPVDIRA"         : 0.99
                     ,"Lc_FDCHI2"          : 25.0
                     ,"Lc_AMassWin"        : 90.0*MeV
                     ,"Lc_MassWin"         : 80.0*MeV
                     ,"Lc_DocaChi2Max"     : 20
                     ,"Lb_DIRA"            : 0.999
                     ,"Lb_VCHI2DOF"        : 9.0
                     ,"Lb_Lc_DZ"           : -2.0*mm
                     ,"Lb_MassMin"         : 2.2*GeV
                     ,"Lb_MassMax"         : 8.0*GeV
                     ,"Lb_DocaChi2Max"     : 10
                     ,"FakePrescale"       : 0.1
                     },
    'STREAMS' : ['Semileptonic']
}

class Lb2LcMuNuX_Lc2L0PiConf(LineBuilder) :

    __configuration_keys__ = default_config['CONFIG'].keys()

    def __init__(self, name, config) :

        LineBuilder.__init__(self, name, config)
        self.config=config



        ############### MUON, PION, Lambda0 SELECTIONS ###################

        self.selmuon = Selection( "Mufor" + name,
                                  Algorithm = self._muonFilter(name),
                                  RequiredSelections = [StdLooseMuons])


        self.selFakeMuon = Selection( "FakeMufor" + name,
                                      Algorithm = self._FakeMuonFilter(name),
                                      RequiredSelections = [StdNoPIDsMuons])

        self.selPion = Selection( "Pifor" + name,
                                  Algorithm = self._pionFilter(name),
                                  RequiredSelections = [StdLoosePions])

        self.selLambdaLL = Selection( "LambdaLLfor" + name,
                                      Algorithm = self._LambdaLLFilter(name),
                                      RequiredSelections = [StdVeryLooseLambdaLL])

        ############### Lambda_c -> Lambda0 Pi SELECTION ###############

        ### Lb Combination and mother cuts:
        _LbComboCuts = "(AM > %(Lb_MassMin)s ) & (AM < %(Lb_MassMax)s) & (ADOCACHI2CUT( %(Lb_DocaChi2Max)s, ''))" % self.config
        _LbMotherCut = "(MM > %(Lb_MassMin)s ) & (MM < %(Lb_MassMax)s )"\
            "& (VFASPF(VCHI2/VDOF) < %(Lb_VCHI2DOF)s ) & (BPVDIRA > %(Lb_DIRA)s )"\
            "& ((MINTREE((ABSID=='Lambda_c+'), VFASPF(VZ)) - VFASPF(VZ)) > %(Lb_Lc_DZ)s )" % self.config

        self.selLc2L0Pi = Selection( "Lc2L0Pifor" + name,
                                     Algorithm = self._Lc2L0PiFilter(name),
                                     RequiredSelections = [self.selPion, self.selLambdaLL])

        self.selb2LcMuX = makeb2LcMuX('b2LcMuX' + name,
                                      decayDescriptors = [ '[Lambda_b0 -> Lambda_c+ mu-]cc' , '[Lambda_b0 -> Lambda_c+ mu+]cc'],
                                      MuSel = self.selmuon,
                                      DSel = self.selLc2L0Pi,
                                      LbCombinationCuts = _LbComboCuts,
                                      LbMotherCut = _LbMotherCut
                                     )

        self.selFakeb2LcMuX = makeFakeb2LcMuX('b2LcMuXFake' + name,
                                              decayDescriptors = [ '[Lambda_b0 -> Lambda_c+ mu-]cc' , '[Lambda_b0 -> Lambda_c+ mu+]cc'],
                                              FakeMuSel = self.selFakeMuon,
                                              DSel = self.selLc2L0Pi,
                                              LbCombinationCuts = _LbComboCuts,
                                              LbMotherCut = _LbMotherCut
                                              )

        ############### DECLARE THE STRIPPING LINES ###############


        GECs = { "Code":"( recSummaryTrack(LHCb.RecSummary.nLongTracks, TrLONG) < %(GEC_nLongTrk)s )" % self.config,
                 "Preambulo": ["from LoKiTracks.decorators import *"]}

        ####### Lambda_c -> Lambda0 Pi #######

        self.Lb2LcMuNuX_Lc2L0Pi = StrippingLine('b2LcMuX' + name + 'Line',
                                                prescale = 1,
                                                selection = self.selb2LcMuX,
                                                FILTER = GECs,
                                                HLT1 = config["HLT1"],
                                                HLT2 = config["HLT2"])

        self.registerLine(self.Lb2LcMuNuX_Lc2L0Pi)

        self.FakeLb2LcMuNuX_Lc2L0Pi = StrippingLine('b2LcMuXFake' + name + 'Line',
                                                    prescale = config["FakePrescale"],
                                                    selection = self.selFakeb2LcMuX,
                                                    FILTER = GECs,
                                                    HLT1 = config["HLT1"],
                                                    HLT2 = config["HLT2"])

        self.registerLine(self.FakeLb2LcMuNuX_Lc2L0Pi)

    def _muonFilter(self,name):
        _code = "(MIPCHI2DV(PRIMARY) > %(MuonIPCHI2)s ) & (TRGHOSTPROB < %(GhostProb)s ) & (PIDmu > %(MuonPIDmu)s ) & (P > %(MuonP)s ) & (PT > %(MuonPT)s ) & (TRCHI2DOF < %(TRCHI2)s)" % self.config
        _mu = FilterDesktop( '_mu'+name, Code = _code)
        return _mu

    def _FakeMuonFilter(self,name):
        _code = "(MIPCHI2DV(PRIMARY) > %(MuonIPCHI2)s ) & (TRGHOSTPROB < %(GhostProb)s ) & (PIDmu < %(MuonPIDmu)s ) & (P > %(MuonP)s ) & (PT > %(MuonPT)s ) & (TRCHI2DOF < %(TRCHI2)s)" % self.config
        _FakeMu = FilterDesktop( '_fakeMu'+name, Code = _code)
        return _FakeMu

    def _pionFilter(self,name):
        _code = "(MIPCHI2DV(PRIMARY) > %(PionIPCHI2)s ) & (TRGHOSTPROB < %(GhostProb)s ) & (P > %(PionP)s ) & (PT > %(PionPT)s ) & (TRCHI2DOF < %(TRCHI2)s ) & (PIDK < %(PionPIDK)s )" % self.config
        _pi = FilterDesktop( '_pi'+name, Code = _code)
        return _pi

    def _LambdaLLFilter(self,name):
        _code = "(P > %(LambdaLL_P)s ) & (PT > %(LambdaLL_PT)s ) & (BPVVDCHI2 > %(LambdaLL_BPVVDCHI2)s) & (VFASPF(VCHI2/VDOF) < %(LambdaLL_VCHI2DOF)s)"\
            "& (CHILDCUT((TRGHOSTPROB < %(GhostProb)s),1)) & (CHILDCUT((TRGHOSTPROB < %(GhostProb)s),2))"\
            "& (CHILDCUT((TRCHI2DOF < %(TRCHI2)s),1)) & (CHILDCUT((TRCHI2DOF < %(TRCHI2)s),2))"\
            "& (ADMASS('Lambda0') < %(LambdaLL_ADMASS)s )" % self.config
        _LambdaLL = FilterDesktop( '_LambdaLL'+name, Code = _code)
        return _LambdaLL

    def _Lc2L0PiFilter(self,name):
        _decayDescriptors = [ '[Lambda_c+ -> Lambda0 pi+]cc']
        _combinationCut = "(ADAMASS('Lambda_c+') < %(Lc_AMassWin)s )" % self.config
        _motherCut = "(ADMASS('Lambda_c+') < %(Lc_MassWin)s ) & (VFASPF(VCHI2/VDOF) < %(Lc_VCHI2DOF)s )" % self.config
        _Lc2L0Pi = CombineParticles( '_Lc2L0Pi'+name,
                                     DecayDescriptors = _decayDescriptors,
                                     CombinationCut = _combinationCut,
                                     MotherCut = _motherCut)
        return _Lc2L0Pi

def makeb2LcMuX(name,decayDescriptors,MuSel,DSel,LbCombinationCuts,LbMotherCut):
    _Lb = CombineParticles('_Lb'+name,
                           DecayDescriptors = decayDescriptors,
                           CombinationCut = LbCombinationCuts,
                           MotherCut = LbMotherCut)
    return Selection(name,
                     Algorithm = _Lb,
                     RequiredSelections = [MuSel,DSel])

def makeFakeb2LcMuX(name,decayDescriptors,FakeMuSel,DSel,LbCombinationCuts,LbMotherCut):
    _FakeLb = CombineParticles('_FakeLb'+name,
                               DecayDescriptors = decayDescriptors,
                               CombinationCut = LbCombinationCuts,
                               MotherCut = LbMotherCut)
    return Selection(name,
                     Algorithm = _FakeLb,
                     RequiredSelections = [FakeMuSel,DSel])
