###############################################################################
# (c) Copyright 2019 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################

__author__ = ['Liang Sun']
__date__ = '07/05/2019'
__version__ = '$Revision: 0.01 $'

'''
D* -> D0 pis, D0 -> K- pi+ pi- mu+ nu exclusive reconstruction 
'''
# =============================================================================
##
#  D0 -> K- pi+ pi- mu+(e+) nu exclusive reconstruction
#

"""
Stripping lines for semileptonic D0 decays
to final states with 3 charged mesons and a lepton.
The two hadronic signal modes for D0 from D*+ -> D0 pi+ are:

D0 -> K- pi+ pi- mu+ nu, 
D0 -> K- pi+ pi- e+ nu, 

Last modification $Date: 07/05/2019 $
               by $Author: lsun$
"""
__all__ = ('D2KPiPiMuNuBuilder',
           'TOSFilter',
           'default_config')

default_config = {
    'NAME'        : 'D2KPiPiMuNu',
    'WGs'         : ['Charm'],
    'BUILDERTYPE' : 'D2KPiPiMuNuBuilder',
    'CONFIG'      :  {
        "KPiPiMuMassLow"     : 800  , #MeV
        "KPiPiMuMassHigh"    : 2000 , #MeV
        "DELTA_MASS_MAX"  : 200  , #MeV
        "GEC_nLongTrk"    : 160. , #adimensional
        "TRGHOSTPROB"     : 0.35 ,#adimensional
        "DAUMINIPCHI2"   : 3    ,#adimensional
        #Muons
        "MuonGHOSTPROB"   : 0.35 ,#adimensional
        "MuonP"          : 3000. ,#MeV
        "MuonPT"          : 300. ,#MeV
        "MuonPIDK"        : 0.   ,#adimensional
        "MuonPIDmu"       : 3.   ,#adimensional
        "MuonPIDp"        : 0.   ,#adimensional
        "ElectronPIDe"    : 1.  ,
        "ElectronP"           : 3000.,#MeV
        "ElectronPT"      : 300  ,#MeV
        #Xu
        #K channel
        "KaonTRCHI2"           : 3.,#MeV
        "KaonP"           : 3000.,#MeV
        "KaonPT"          : 300. ,#MeV
        "KaonPIDK"        : 5.   ,#adimensional 
        "KaonPIDmu"       : 5.   ,#adimensional
        "KaonPIDp"        : 5.   ,#adimensional
        "PionPIDK"        : 0.   ,#adimensional 
        "PionPIDmu"       : 0.   ,#adimensional
        "PionPIDp"        : 0.   ,#adimensional
        "BVCHI2DOF"       : 20   ,#adminensional
        "BDIRA"           : 0.999,#adminensional
        "BFDCHI2HIGH"     : 30. ,#adimensional
        "D0IPCHI2Max"     : 36. ,#adimensional
        "BPVVDZcut"       : 0.0  , #mm
        #slow pion
        "Slowpion_PT"     : 120 #MeV
        ,"Slowpion_P"     : 1000 #MeV
        ,"Slowpion_TRGHOSTPROB" : 0.35 #adimensional
        ,"Slowpion_PIDe" : 5 #adimensional
        ,"useTOS" : False #adimensional
        ,"TOSFilter" : { 'Hlt2CharmHad.*HHX.*Decision%TOS' : 0}  #adimensional
        ,"SSonly" : 2 # swith: 0==signal only (h-l+), 1==same sign hl, 2==both, 3 == 2016 EOY restripping
        , "NoPIDmu"           : True
        , "NoPIDK"           : True
        , "NoPIDpi"           : True
        , "NoPIDpis"           : True
        },
    'STREAMS'     : ['CharmCompleteEvent']    
}

from Gaudi.Configuration import *
from StrippingUtils.Utils import LineBuilder

import logging
#default_name="D2KPiPiMuNu"

class D2KPiPiMuNuBuilder(LineBuilder):
    """
    Definition of D* tagged D0 -> K- pi+ pi- mu+ (e+) nu stripping
    """
    
    __configuration_keys__ = default_config['CONFIG'].keys()
    
    def __init__(self,name,config):
        LineBuilder.__init__(self, name, config)
        self._config=config
        from PhysSelPython.Wrappers import Selection, DataOnDemand
        
        self.GECs = { "Code":"( recSummaryTrack(LHCb.RecSummary.nLongTracks, TrLONG) < %(GEC_nLongTrk)s )" % config,
                      "Preambulo": ["from LoKiTracks.decorators import *"]}
        
        self._muonSel=None
        self._muonFilter()
        
        self._electronSel=None
        self._electronFilter()
        
        self._slowpionSel =None
        self._pionSel=None
        self._kaonSel=None
        self._kaonFilter()
        self._pionFilter()
        self._slowpionFilter()
        
        ## OPPOSITE SIGN hl (BACKGROUND)
        if self._config["SSonly"]==2 or self._config["SSonly"]==0:
            self.registerLine(self._D2KPiPiMuNuLine())
            self.registerLine(self._D2KPiPiENuLine())
        if self._config["SSonly"]>=1:
            ## SAME SIGN hl (BACKGROUND)
            self.registerLine(self._D2KPiPiMuNuKMuSSLine())
            self.registerLine(self._D2KPiPiMuNuSSLine())
            self.registerLine(self._D2KPiPiENuKESSLine())
            self.registerLine(self._D2KPiPiENuSSLine())

    def _NominalMuSelection( self ):
        #        return "(TRCHI2DOF < %(MuonTRCHI2)s ) &  (P> %(MuonP)s *MeV) &  (PT> %(MuonPT)s* MeV)"\
        return " (PT> %(MuonPT)s* MeV)"\
               "& (P> %(MuonP)s)"\
               "& (TRCHI2DOF < %(KaonTRCHI2)s )"\
               "& (TRGHOSTPROB < %(MuonGHOSTPROB)s)"\
               "& (PIDmu-PIDpi > %(MuonPIDmu)s )"\
               "& (PIDmu-PIDp  > %(MuonPIDp)s )"\
               "& (PIDmu-PIDK  > %(MuonPIDK)s )"\
               "& (MIPCHI2DV(PRIMARY)> %(DAUMINIPCHI2)s )"

    def _NominalKSelection( self ):
        #        return "(TRCHI2DOF < %(KaonTRCHI2)s )&  (P> %(KaonPTight)s *MeV) &  (PT> %(KaonPT)s *MeV)"\
        return "  (PT> %(KaonPT)s *MeV)"\
               "& (P> %(KaonP)s)"\
               "& (TRCHI2DOF < %(KaonTRCHI2)s )"\
               "& (TRGHOSTPROB < %(TRGHOSTPROB)s)"\
               "& (PIDK-PIDpi  > %(KaonPIDK)s ) & (PIDK-PIDp > %(KaonPIDp)s ) & (PIDK-PIDmu > %(KaonPIDmu)s ) "\
               "& (MIPCHI2DV(PRIMARY)> %(DAUMINIPCHI2)s )"

    def _NominalPiSelection( self ):
        #        return "(TRCHI2DOF < %(KaonTRCHI2)s )&  (P> %(KaonP)s *MeV) &  (PT> %(KaonPT)s *MeV)"\
        return " (PT> %(KaonPT)s *MeV)"\
               "& (P> %(KaonP)s)"\
               "& (TRCHI2DOF < %(KaonTRCHI2)s )"\
               "& (TRGHOSTPROB < %(TRGHOSTPROB)s)"\
               "& (PIDK-PIDpi < -%(PionPIDK)s )& (PIDp < -%(PionPIDp)s )& (PIDmu < -%(PionPIDmu)s ) "\
               "& (PIDe < 0)"\
               "& (MIPCHI2DV(PRIMARY)> %(DAUMINIPCHI2)s )"

    def _NominalSlowPiSelection( self ):
        #        return "(TRCHI2DOF < %(KaonTRCHI2)s )&  (P> %(KaonP)s *MeV) &  (PT> %(KaonPT)s *MeV)"\
        return " (PT> %(Slowpion_PT)s *MeV)"\
               "& (P > %(Slowpion_P)s)"\
               "& (TRGHOSTPROB < %(Slowpion_TRGHOSTPROB)s)"\
               "& (PIDe < %(Slowpion_PIDe)s) & (MIPCHI2DV(PRIMARY)< 9.0) "

    def _NominalElectronSelection( self ):
        return " (PT> %(ElectronPT)s *MeV) & (P> %(ElectronP)s)"\
               "& (TRCHI2DOF < %(KaonTRCHI2)s )"\
                "& (TRGHOSTPROB < %(TRGHOSTPROB)s)"\
                "& (PIDe > %(ElectronPIDe)s)"\
               "& (MIPCHI2DV(PRIMARY)> %(DAUMINIPCHI2)s )"

    def _electronFilter( self ):
        if self._electronSel is not None:
            return self._electronSel
        
        from GaudiConfUtils.ConfigurableGenerators import FilterDesktop
        from PhysSelPython.Wrappers import Selection
        from StandardParticles import StdLooseElectrons
        _el = FilterDesktop( Code = self._NominalElectronSelection() % self._config )
        _electronSel=Selection("Electron_for"+self._name,
                         Algorithm=_el,
                         RequiredSelections = [StdLooseElectrons])
        
        self._electronSel=_electronSel
        
        return _electronSel

    ######--######
    def _muonFilter( self ):
        if self._muonSel is not None:
            return self._muonSel
        
        from GaudiConfUtils.ConfigurableGenerators import FilterDesktop
        from PhysSelPython.Wrappers import Selection
        from StandardParticles import StdLooseMuons, StdAllLooseMuons, StdAllNoPIDsMuons
        _mu = FilterDesktop( Code = self._NominalMuSelection() % self._config )
        _mupid = StdAllLooseMuons
        if self._config["NoPIDmu"]: _mupid = StdAllNoPIDsMuons
        _muSel=Selection("Mu_for"+self._name,
                         Algorithm=_mu,
                         RequiredSelections = [_mupid])
        
        self._muonSel=_muSel
        
        return _muSel

    def _pionFilter( self ):
        if self._pionSel is not None:
            return self._pionSel
        
        from GaudiConfUtils.ConfigurableGenerators import FilterDesktop
        from PhysSelPython.Wrappers import Selection
        from StandardParticles import StdAllLoosePions, StdAllNoPIDsPions
        
        _ka = FilterDesktop( Code = self._NominalPiSelection() % self._config )
        _pipid = StdAllLoosePions
        if self._config["NoPIDpi"]: _pipid = StdAllNoPIDsPions
        _kaSel=Selection("Pi_for"+self._name,
                         Algorithm=_ka,
                         RequiredSelections = [_pipid])
        self._pionSel=_kaSel
        return _kaSel

    ######Kaon Filter######
    def _kaonFilter( self ):
        if self._kaonSel is not None:
            return self._kaonSel
        
        from GaudiConfUtils.ConfigurableGenerators import FilterDesktop
        from PhysSelPython.Wrappers import Selection
        from StandardParticles import StdAllLooseKaons, StdAllNoPIDsKaons
        
        _ka = FilterDesktop( Code = self._NominalKSelection() % self._config )
        _kpid = StdAllLooseKaons
        if self._config["NoPIDK"]: _kpid = StdAllNoPIDsKaons
        _kaSel=Selection("K_for"+self._name,
                         Algorithm=_ka,
                         RequiredSelections = [_kpid])
        self._kaonSel=_kaSel
        return _kaSel

    ######Slow Pion Filter######
    def _slowpionFilter( self ):
        if self._slowpionSel is not None:
            return self._slowpionSel
        
        from GaudiConfUtils.ConfigurableGenerators import FilterDesktop
        from PhysSelPython.Wrappers import Selection
        from StandardParticles import StdAllLoosePions, StdAllNoPIDsPions
        
        _pipid = StdAllLoosePions
        if self._config["NoPIDpis"]: _pipid = StdAllNoPIDsPions
        _ka = FilterDesktop( Code = self._NominalSlowPiSelection() % self._config )
        _kaSel=Selection("pis_for"+self._name,
                         Algorithm=_ka,
                         RequiredSelections = [_pipid])
        self._slowpionSel=_kaSel
        return _kaSel

    def _D2KPiPiMuNuLine( self ):
        return self.DstarMaker("D2Kpipimu",["[D0 -> K- pi+ pi- mu+]cc"], self._kaonFilter(), self._pionFilter(), self._muonFilter())

    ## SAME SIGN hl (BACKGROUND)
    def _D2KPiPiMuNuKMuSSLine( self ):
        return self.DstarMaker("D2Kpipimu_KLepSS",["[D0 -> K- pi+ pi+ mu-]cc"], self._kaonFilter(), self._pionFilter(), self._muonFilter())
    
    def _D2KPiPiMuNuSSLine( self ):
        return self.DstarMaker("D2Kpipimu_SS",["[D0 -> K- pi+ pi- mu-]cc"], self._kaonFilter(), self._pionFilter(), self._muonFilter())

    def _D2KPiPiENuLine( self ):
        return self.DstarMaker("D2KpipiE",["[D0 -> K- pi+ pi- e+]cc"], self._kaonFilter(), self._pionFilter(), self._electronFilter())

    ## SAME SIGN hl (BACKGROUND)
    def _D2KPiPiENuKESSLine( self ):
        return self.DstarMaker("D2KpipiE_KLepSS",["[D0 -> K- pi+ pi+ e-]cc"], self._kaonFilter(), self._pionFilter(), self._electronFilter())
    
    def _D2KPiPiENuSSLine( self ):
        return self.DstarMaker("D2KpipiE_SS",["[D0 -> K- pi+ pi- e-]cc"], self._kaonFilter(), self._pionFilter(), self._electronFilter())

    def DstarMaker(self, _name, _D0Decays, kmfiler, pifiler, lfilter):
        from GaudiConfUtils.ConfigurableGenerators import CombineParticles
        from PhysSelPython.Wrappers import Selection
        from StrippingConf.StrippingLine import StrippingLine
        
        _KPiPiMu = CombineParticles(
            DecayDescriptors = _D0Decays,
            CombinationCut = "(AM>%(KPiPiMuMassLow)s*MeV) & (AM<%(KPiPiMuMassHigh)s*MeV)" % self._config,
            MotherCut = "(VFASPF(VCHI2/VDOF)< %(BVCHI2DOF)s )"\
            "& (BPVVDCHI2 >%(BFDCHI2HIGH)s)"\
            "& (BPVIPCHI2()<%(D0IPCHI2Max)s)"\
            "& (BPVDIRA > %(BDIRA)s)"\
            "& (BPVVDZ > %(BPVVDZcut)s)"
            #            "& (BPVVD > %(VDCut)s)"
            #" & (ratio > 0.0)"
            % self._config,
            ReFitPVs = True
            )
        # _KPiPiMu.Preambulo = [ "from LoKiPhys.decorators import *",
        #                    "dx = (VFASPF(VX)-BPV(VX))",
        #                    "dy = (VFASPF(VY)-BPV(VY))",
        #                    "dz = (VFASPF(VZ)-BPV(VZ))",
        #                    "norm = (max(sqrt(dx*dx+dy*dy+dz*dz),0))",
        #                    "D_xdir  = ((dx)/norm)",
        #                    "D_ydir  = ((dy)/norm)",
        #                    "D_zdir  = ((dz)/norm)",
        #                    "Pxkmu   = (CHILD(PX,1)+CHILD(PX,2))",
        #                    "Pykmu   = (CHILD(PY,1)+CHILD(PY,2))",
        #                    "Pzkmu   = (CHILD(PZ,1)+CHILD(PZ,2))",
        #                    "Dflight = (D_xdir*Pxkmu + D_ydir*Pykmu+ D_zdir*Pzkmu)",
        #                    "mD  = 1864.84",
        #                    "M_2 = (mD*mD + M12*M12)",
        #                    "kmu_E  = (CHILD(E,1)+CHILD(E,2))",
        #                    "quada = (Dflight*Dflight - kmu_E*kmu_E)",
        #                    "quadb = (M_2*Dflight)",
        #                    "quadc = (((M_2*M_2)/4) - (kmu_E*kmu_E*mD*mD))",
        #                    "Discriminant = ((quadb*quadb)-(4*quada*quadc))",
        #                    "solution_a = ((-quadb + sqrt(max(Discriminant,0)))/(2*quada))",
        #                    "solution_b = ((-quadb - sqrt(max(Discriminant,0)))/(2*quada))",
        #                    "ratio = (solution_a/solution_b)"
        #                    ]
        
        _D0Sel=Selection("SelD0_for"+_name,
                         Algorithm=_KPiPiMu,
                         RequiredSelections = [lfilter, pifiler, kmfiler])


        DstComb = CombineParticles( #name = "CombDst"+_name,
                DecayDescriptors = ['[D*(2010)+ -> D0 pi+]cc'],
                CombinationCut = "(AM - ACHILD(M,1) < %(DELTA_MASS_MAX)s+5 *MeV) & (ADOCACHI2CUT(20,''))" %self._config,
                MotherCut = "(M - CHILD(M,1) < %(DELTA_MASS_MAX)s *MeV) & (VFASPF(VCHI2/VDOF) < %(BVCHI2DOF)s)" %self._config
                )
        DstSel = Selection("SelDst"+_name,
                Algorithm = DstComb,
                RequiredSelections = [_D0Sel,self._slowpionFilter()])

        _tosFilter = self._config['TOSFilter']
        DstSelTOS = TOSFilter( "SelDstKPiPiMu_Hlt2TOS"+_name
                ,DstSel
                ,_tosFilter)
    
        hlt2 = ""        
        the_prescale = 1.0
        if self._config["useTOS"] == True: # and _name.find('E') < 0:
            if "K" in _name:
                the_prescale = 1
            if 'SS' in _name:
                the_prescale = 1
            Line = StrippingLine(_name+'Line',
                    prescale = the_prescale,
                    FILTER=self.GECs,
                    HLT2 = hlt2,
                    selection = DstSelTOS) 
        else:
            if "K" in _name:
                the_prescale = 1
            Line = StrippingLine(_name+'Line',
                    prescale = the_prescale,
                    FILTER=self.GECs,
                    HLT2 = hlt2,
                    selection = DstSel) 
        
        return Line
                                                    
def TOSFilter( name, _input, _specs ) :
    from Configurables import TisTosParticleTagger
    from PhysSelPython.Wrappers import Selection 
    _tisTosFilter = TisTosParticleTagger( name + "Tagger" )
    _tisTosFilter.TisTosSpecs = _specs 
    return Selection( name
                      , Algorithm = _tisTosFilter
                      , RequiredSelections = [ _input ]
                      )        
