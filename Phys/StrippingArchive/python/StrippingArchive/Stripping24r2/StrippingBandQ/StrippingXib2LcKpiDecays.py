###############################################################################
# (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""
This module supersedes StrippingXibDecays.py
Module for construction of Xib Decays Stripping Selections and StrippingLines.
Provides functions to build Lc, Xic, Xib selections.
Provides class XibDecaysConf, which constructs the Selections and
StrippingLines given a configuration dictionary.
Exported selection makers: 'makeLc', 'makeXic0', 'makeXib2LcKpi', 'makeXib2Xicpi'
"""

__author__ = ['Marco Pappagallo']
__date__ = '16/03/2019'
__version__ = '$Revision: 1.0 $'

__all__ = ('XibDecaysConf', 'makeLc', 'makeXic0', 'makeXib2LcKpi',
           'makeXib2Xicpi', 'default_config')

from Gaudi.Configuration import *
from GaudiConfUtils.ConfigurableGenerators import FilterDesktop, CombineParticles
from PhysSelPython.Wrappers import Selection, DataOnDemand
from StrippingConf.StrippingLine import StrippingLine
from StrippingUtils.Utils import LineBuilder

#default_name = ''
default_config = {
    'NAME': 'XibDecays',
    'BUILDERTYPE': 'XibDecaysConf',
    'CONFIG': {
        # Xib
        'MinXibMass': 5300.  # MeV
        ,
        'MaxXibMass': 6500.  # MeV
        ,
        'Xib_PT': 3500.  # MeV
        ,
        'XibVertChi2DOF': 10  # Dimensionless
        ,
        'XibLT': 0.2  # ps
        ,
        'XibIPCHI2': 25  # Dimensionless
        ,
        'XibDIRA': 0.999  # Dimensionless
        ,
        'KaonProbNN': 0.1  # Dimensionless
        ,
        'PionProbNN': 0.1  # Dimensionless
        ,
        'ProtProbNN': 0.1  # Dimensionless
        ,
        'TrackMinIPCHI2': 4  # Dimensionless
        ,
        'LcPT': 1800  # MeV
        ,
        'LcBPVVDCHI2': 36,
        'LcDIRA': 0.  # Dimensionless
        ,
        'LcDeltaMassWin': 100  # MeV
        ,
        'MaxLcVertChi2DOF': 10  # Dimensionless
        # Pre- and postscales
        ,
        'XibDecaysPreScale': 1.0,
        'XibDecaysPostScale': 1.0
    },
    'STREAMS': ['Bhadron'],
    'WGs': ['BandQ']
}


class XibDecaysConf(LineBuilder):
    """
    Definition of Xib -> Lc K pi stripping

    Constructs Xib -> Lc K pi Selections and StrippingLines from
    a configuration dictionary.
    Usage:
    >>> config = { .... }
    >>> gammaConf = StrippingB2XGammaConf('StrippingB2XGammaTest',config)
    >>> gammaLines = gammaConf.lines
    >>> for line in line :
    >>>  print line.name(), line.outputLocation()
    The lines can be used directly to build a StrippingStream object.

    Exports as instance data members:
    selLc                    : nominal Lc+  -> p K- pi+ Selection object
    selXic0                  : nominal Xic0 -> p K- K- pi+ Selection object
    selXib2LcKpi             : nominal Xib- -> Lc+ K- pi- Selection object
    selXib2LcKpiWS           : nominal Xib- -> Lc+ K- pi+ Selection object
    selXib2Xicpi             : nominal Xib- -> Xic0 pi- Selection object
    selXib2XicpiWS           : nominal Xib- -> Xic0 pi+ Selection object
    lines                    : List of lines

    Exports as class data member:
    StrippingB2XGammaConf.__configuration_keys__ : List of required configuration parameters.
    """

    __configuration_keys__ = default_config['CONFIG'].keys()

    def __init__(self, name, config):
        LineBuilder.__init__(self, name, config)

        # if name not set outside, set it to empty
        #if name == None:
        #    name = ""

        # Selection of Xib daughters: Lc

        self.selLc = makeLc('LcFor%s' % name, config['KaonProbNN'],
                            config['PionProbNN'], config['ProtProbNN'],
                            config['LcPT'], config['LcDeltaMassWin'],
                            config['MaxLcVertChi2DOF'], config['LcBPVVDCHI2'],
                            config['LcDIRA'], config['TrackMinIPCHI2'])

        self.selXic0 = makeXic0(
            'Xic0For%s' % name, config['KaonProbNN'], config['PionProbNN'],
            config['ProtProbNN'], config['LcPT'], config['LcDeltaMassWin'],
            config['MaxLcVertChi2DOF'], config['LcBPVVDCHI2'],
            config['LcDIRA'], config['TrackMinIPCHI2'])
        # Xib -> Lc K pi selections
        self.selXib2LcKpi = makeXib2LcKpi(
            'Xib2LcKpiFor%s' % name, self.selLc,
            '[Xi_b- -> Lambda_c+ K- pi-]cc', config['KaonProbNN'],
            config['PionProbNN'], config['MaxXibMass'], config['MinXibMass'],
            config['XibVertChi2DOF'], config['XibIPCHI2'], config['XibLT'],
            config['Xib_PT'], config['XibDIRA'], config['TrackMinIPCHI2'])

        self.selXib2LcKpiWS = makeXib2LcKpi(
            'Xib2LcKpiWSFor%s' % name, self.selLc,
            '[Xi_b- -> Lambda_c+ K- pi+]cc', config['KaonProbNN'],
            config['PionProbNN'], config['MaxXibMass'], config['MinXibMass'],
            config['XibVertChi2DOF'], config['XibIPCHI2'], config['XibLT'],
            config['Xib_PT'], config['XibDIRA'], config['TrackMinIPCHI2'])

        # Xib -> Xic pi selections
        self.selXib2Xicpi = makeXib2Xicpi(
            'Xib2Xic0piFor%s' % name, self.selXic0, '[Xi_b- -> Xi_c0 pi-]cc',
            config['PionProbNN'], config['MaxXibMass'], config['MinXibMass'],
            config['XibVertChi2DOF'], config['XibIPCHI2'], config['XibLT'],
            config['Xib_PT'], config['XibDIRA'], config['TrackMinIPCHI2'])

        self.selXib2XicpiWS = makeXib2Xicpi(
            'Xib2Xic0piWSFor%s' % name, self.selXic0, '[Xi_b- -> Xi_c0 pi+]cc',
            config['PionProbNN'], config['MaxXibMass'], config['MinXibMass'],
            config['XibVertChi2DOF'], config['XibIPCHI2'], config['XibLT'],
            config['Xib_PT'], config['XibDIRA'], config['TrackMinIPCHI2'])

        # Create and register stripping lines
        self.Xib2LcKpiLine = StrippingLine(
            "%sLcKpiLine" % name,
            prescale=config['XibDecaysPreScale'],
            postscale=config['XibDecaysPostScale'],
            selection=self.selXib2LcKpi)
        #                                               selection=self.selLc)
        self.registerLine(self.Xib2LcKpiLine)

        self.Xib2LcKpiLineWS = StrippingLine(
            "%sLcKpiWSLine" % name,
            prescale=config['XibDecaysPreScale'],
            postscale=config['XibDecaysPostScale'],
            selection=self.selXib2LcKpiWS)
        self.registerLine(self.Xib2LcKpiLineWS)

        self.Xib2XicpiLine = StrippingLine(
            "%sXic0piLine" % name,
            prescale=config['XibDecaysPreScale'],
            postscale=config['XibDecaysPostScale'],
            selection=self.selXib2Xicpi)
        self.registerLine(self.Xib2XicpiLine)

        self.Xib2XicpiLineWS = StrippingLine(
            "%sXic0piWSLine" % name,
            prescale=config['XibDecaysPreScale'],
            postscale=config['XibDecaysPostScale'],
            selection=self.selXib2XicpiWS)
        self.registerLine(self.Xib2XicpiLineWS)


def makeLc(name, KaonProbNN, PionProbNN, ProtProbNN, LcPT, LcDeltaMassWin,
           MaxLcVertChi2DOF, LcBPVVDCHI2, LcDIRA, TrackMinIPCHI2):
    """
    Create and return a Lc+ -> p K- pi+ Selection object, starting with the daughters' selections.

    @arg name: name of the Selection.
    @arg KaonProbNN: PID of Kaon
    @arg PionProbNN: PID of pion
    @arg ProtProbNN: PID of proton
    @arg LcPT:      Min PT of Lc
    @arg LcDeltaMassWin: Mass difference m(pKpi)-m(Lc)
    @arg MaxLcVertChi2DOF: chi2/NDOF
    @arg LcBPVVDCHI2: Flight distance chi2
    @arg LcDIRA:    DIRA of Lc
    @arg TrackMinIPCHI2: Min IPCHI2 for kaons and pions
    @return: Selection object
    """
    _daughterKCut = "(PROBNNk  > %(KaonProbNN)s) & (MIPCHI2DV(PRIMARY)>%(TrackMinIPCHI2)s)" % locals(
    )
    _daughterpiCut = "(PROBNNpi > %(PionProbNN)s) & (MIPCHI2DV(PRIMARY)>%(TrackMinIPCHI2)s)" % locals(
    )
    _daughterpCut = "(PROBNNp  > %(ProtProbNN)s) & (MIPCHI2DV(PRIMARY)>%(TrackMinIPCHI2)s)" % locals(
    )

    _combinationCut = "(ASUM(PT)> %(LcPT)s*MeV) & (ADAMASS('Lambda_c+') < 1.25*%(LcDeltaMassWin)s*MeV) & (ACUTDOCA(0.5*mm,'LoKi::DistanceCalculator'))" % locals(
    )
    _motherCut = "(ADMASS('Lambda_c+') < %(LcDeltaMassWin)s*MeV) & (VFASPF(VCHI2/VDOF)<%(MaxLcVertChi2DOF)s) & (BPVVDCHI2>%(LcBPVVDCHI2)s) & (BPVDIRA>%(LcDIRA)s)" % locals(
    )
    _Lc = CombineParticles(
        DecayDescriptor="[Lambda_c+ -> p+ K- pi+]cc",
        DaughtersCuts={
            "K+": _daughterKCut,
            "pi+": _daughterpiCut,
            "p+": _daughterpCut
        },
        CombinationCut=_combinationCut,
        MotherCut=_motherCut)

    _stdANNKaon = DataOnDemand(Location="Phys/StdLooseANNKaons/Particles")
    _stdANNPion = DataOnDemand(Location="Phys/StdLooseANNPions/Particles")
    _stdANNProtons = DataOnDemand(Location="Phys/StdLooseANNProtons/Particles")

    return Selection(
        name,
        Algorithm=_Lc,
        RequiredSelections=[_stdANNKaon, _stdANNPion, _stdANNProtons])


def makeXic0(name, KaonProbNN, PionProbNN, ProtProbNN, LcPT, LcDeltaMassWin,
             MaxLcVertChi2DOF, LcBPVVDCHI2, LcDIRA, TrackMinIPCHI2):
    """
    Create and return a Xic0 -> p K- K- pi+ Selection object, starting with the daughters' selections.

    @arg name: name of the Selection.
    @arg KaonProbNN: PID of Kaon
    @arg PionProbNN: PID of pion
    @arg ProtProbNN: PID of proton
    @arg LcPT:      Min PT of Xic
    @arg LcDeltaMassWin: Mass difference m(pKkpi)-m(Xic0)
    @arg MaxLcVertChi2DOF: chi2/NDOF
    @arg LcBPVVDCHI2: Flight distance chi2
    @arg LcDIRA:    DIRA of Xic
    @arg TrackMinIPCHI2: Min IPCHI2 for kaons and pions
    @return: Selection object
    """
    _daughterKCut = "(PROBNNk  > %(KaonProbNN)s) & (MIPCHI2DV(PRIMARY)>%(TrackMinIPCHI2)s)" % locals(
    )
    _daughterpiCut = "(PROBNNpi > %(PionProbNN)s) & (MIPCHI2DV(PRIMARY)>%(TrackMinIPCHI2)s)" % locals(
    )
    _daughterpCut = "(PROBNNp  > %(ProtProbNN)s) & (MIPCHI2DV(PRIMARY)>%(TrackMinIPCHI2)s)" % locals(
    )

    _combinationCut = "(ASUM(PT)> %(LcPT)s*MeV) & (ADAMASS('Xi_c0') < 1.25*%(LcDeltaMassWin)s*MeV) & (ACUTDOCA(0.5*mm,'LoKi::DistanceCalculator'))" % locals(
    )
    _motherCut = "(ADMASS('Xi_c0') < %(LcDeltaMassWin)s*MeV) & (VFASPF(VCHI2/VDOF)<%(MaxLcVertChi2DOF)s) & (BPVVDCHI2>%(LcBPVVDCHI2)s) & (BPVDIRA>%(LcDIRA)s)" % locals(
    )
    _Xic0 = CombineParticles(
        DecayDescriptor="[Xi_c0 -> p+ K- K- pi+]cc",
        DaughtersCuts={
            "K+": _daughterKCut,
            "pi+": _daughterpiCut,
            "p+": _daughterpCut
        },
        CombinationCut=_combinationCut,
        MotherCut=_motherCut)

    _stdANNKaon = DataOnDemand(Location="Phys/StdLooseANNKaons/Particles")
    _stdANNPion = DataOnDemand(Location="Phys/StdLooseANNPions/Particles")
    _stdANNProtons = DataOnDemand(Location="Phys/StdLooseANNProtons/Particles")

    return Selection(
        name,
        Algorithm=_Xic0,
        RequiredSelections=[_stdANNKaon, _stdANNPion, _stdANNProtons])


def makeXib2LcKpi(name, LcSel, DecDescr, KaonProbNN, PionProbNN, MaxXibMass,
                  MinXibMass, XibVertChi2DOF, XibIPCHI2, XibLT, Xib_PT,
                  XibDIRA, TrackMinIPCHI2):
    """
    Create and return a Xib- -> Lc+ K- pi- Selection object, starting with the daughters' selections.

    @arg name: name of the Selection.
    @arg LcSel: Lc selection.
    @arg DecDescr: Decay descriptor
    @arg KaonProbNN: PID of Kaon
    @arg PionProbNN: PID of pion
    @arg MaxXibMass: Max value of Xib mass window
    @arg MinXibMass: Max value of Xib mass window
    @arg XibVertChi2DOF: chi2/NDOF
    @arg XibIPCHI2:  Xib IP chi2
    @arg XibLT:      Xib lifetime
    @arg Xib_PT:     Xib PT
    @arg XibDIRA:    Xib DIRA
    @arg TrackMinIPCHI2: Min IPCHI2 for kaons and pions
    @return: Selection object
    """

    _stdANNKaonAll = DataOnDemand(
        Location="Phys/StdAllLooseANNKaons/Particles")
    _stdANNPionAll = DataOnDemand(
        Location="Phys/StdAllLooseANNPions/Particles")

    _daughterKCut = "(PROBNNk  > %(KaonProbNN)s) & (MIPCHI2DV(PRIMARY)>%(TrackMinIPCHI2)s)" % locals(
    )
    _daughterpiCut = "(PROBNNpi > %(PionProbNN)s) & (MIPCHI2DV(PRIMARY)>%(TrackMinIPCHI2)s)" % locals(
    )

    _combinationCut = "(AM < %(MaxXibMass)s*MeV+500*MeV) & (AM > %(MinXibMass)s*MeV-500*MeV)" % locals(
    )
    #_combinationCut = "(AM < %(MaxXibMass)s*MeV) & (AM > %(MinXibMass)s*MeV)" % locals()
    _motherCut = "(M < %(MaxXibMass)s*MeV) & (M > %(MinXibMass)s*MeV) & (VFASPF(VCHI2/VDOF)<%(XibVertChi2DOF)s) & (BPVIPCHI2() < %(XibIPCHI2)s) & (BPVLTIME()>%(XibLT)s*ps) & (PT>%(Xib_PT)s*MeV) & (BPVDIRA>%(XibDIRA)s)" % locals(
    )

    _Xib = CombineParticles(
        DecayDescriptor=DecDescr,
        DaughtersCuts={
            "K+": _daughterKCut,
            "pi+": _daughterpiCut
        },
        CombinationCut=_combinationCut,
        MotherCut=_motherCut)

    return Selection(
        name,
        Algorithm=_Xib,
        RequiredSelections=[LcSel, _stdANNKaonAll, _stdANNPionAll])


def makeXib2Xicpi(name, XicSel, DecDescr, PionProbNN, MaxXibMass, MinXibMass,
                  XibVertChi2DOF, XibIPCHI2, XibLT, Xib_PT, XibDIRA,
                  TrackMinIPCHI2):
    """
    Create and return a Xib- -> Xic0 pi-  Selection object, starting with the daughters' selections.

    @arg name: name of the Selection.
    @arg XicSel: Xic selection.
    @arg DecDescr: Decay descriptor
    @arg PionProbNN: PID of pion
    @arg MaxXibMass: Max value of Xib mass window
    @arg MinXibMass: Max value of Xib mass window
    @arg XibVertChi2DOF: chi2/NDOF
    @arg XibIPCHI2:  Xib IP chi2
    @arg XibLT:      Xib lifetime
    @arg Xib_PT:     Xib PT
    @arg XibDIRA:    Xib DIRA
    @arg TrackMinIPCHI2: Min IPCHI2 for kaons and pions
    @return: Selection object
    """

    _stdANNPionAll = DataOnDemand(
        Location="Phys/StdAllLooseANNPions/Particles")

    _daughterpiCut = "(PROBNNpi > %(PionProbNN)s) & (MIPCHI2DV(PRIMARY)>%(TrackMinIPCHI2)s)" % locals(
    )

    _combinationCut = "(AM < %(MaxXibMass)s*MeV+500*MeV) & (AM > %(MinXibMass)s*MeV-500*MeV)" % locals(
    )

    _motherCut = "(M < %(MaxXibMass)s*MeV) & (M > %(MinXibMass)s*MeV) & (VFASPF(VCHI2/VDOF)<%(XibVertChi2DOF)s) & (BPVIPCHI2() < %(XibIPCHI2)s) & (BPVLTIME()>%(XibLT)s*ps) & (PT>%(Xib_PT)s*MeV) & (BPVDIRA>%(XibDIRA)s)" % locals(
    )

    _Xib = CombineParticles(
        DecayDescriptor=DecDescr,
        DaughtersCuts={"pi+": _daughterpiCut},
        CombinationCut=_combinationCut,
        MotherCut=_motherCut)

    return Selection(
        name, Algorithm=_Xib, RequiredSelections=[XicSel, _stdANNPionAll])


# EOF
