#!/usr/bin/env python # ============================================================================= 
###############################################################################
# (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
# @file   StrippintrangeHadrons.py 
# @author Alexandre Brea (alexandre.brea.rodriguez@cern.ch) 
# @date   29.11.2018 # =============================================================================


# Some extra Strange Hadron decays 
#
#Xi-2Sigma0munu
#Xi-2ppipi-
#Xi02p+pi-
#Omega-2Lambda0pi-


'''
Module for construction of stripping selections for some rare strange decays
Exported symbols (use python help!):
'''

__author__ = ['Alexandre Brea']
__date__ = '29/11/2018'
__version__ = '1.0'

__all__ = ('default_config',
           'StrangeHadronsConf',
           )

import os

from Gaudi.Configuration import *

from GaudiConfUtils.ConfigurableGenerators import CombineParticles

from GaudiConfUtils.ConfigurableGenerators import DaVinci__N3BodyDecays as Combine3Particles
from GaudiConfUtils.ConfigurableGenerators import DaVinci__N4BodyDecays as Combine4Particles
from CommonParticles import StdNoPIDsDownElectrons
from PhysSelPython.Wrappers import Selection, DataOnDemand, MergedSelection
from StrippingConf.StrippingLine import StrippingLine
from StrippingUtils.Utils import LineBuilder
from GaudiKernel.PhysicalConstants import c_light
from GaudiKernel.SystemOfUnits import MeV

from StandardParticles import StdAllNoPIDsPions, StdLoosePions, StdLooseProtons, StdAllLoosePions, StdAllLooseMuons
from StandardParticles import StdLooseAllPhotons
##
from PhysConf.Selections import CombineSelection
                                
default_config = {
  'NAME': 'StrangeHadrons',
  'WGs' : ['RD'],
  'BUILDERTYPE' : 'StrangeHadronsConf',
  'STREAMS' : [ 'Leptonic' ],
  'CONFIG' : {
        'Postscale'           :1,
 
        'Xi-2Sigma0munuPrescale' : 1,
        'Xi-2ppipiPrescale' :1,
        'Xi02ppiPrescale' :1,
        'Omega-2Lambda0piPrescale' :1,
        
        #General
        'trchi2dof' : 3,
        'trghostprob' : 0.2,

        #Muons
        'muonProbNNmu' : 0.3,
        'muonMinIpChi2' : 60.,
        'muonMinIp' : 1,
        
        #Protons
        'protonProbNNp' : 0.3,
        'protonMinIpChi2' : 16.,

        #Pions
        'pionProbNNpi' : 0.4,
        'pionMinIpChi2' : 60.,
        'pi0MinPt' : 500 * MeV,

        #Electrons
        'electronProbNNe' : 0.3,
        'electronMinIpChi2' : 60.,
        'electronMinIp' : 1,
        

        #GAMMAS
        

        #Lambda
        'LambdaMassMax': (1116+400.) *MeV,
        'LambdaMinTauPs' : 9.,
        'LambdaMinPt' : 500.,
        'LambdaMaxDOCA' : 1.,  #That's a lot!
        'LambdaMassWin' : 500.,
        'LambdaMassWinTight' : 30.,
        'LambdaMinDIRA' : 0.9,
        'LambdaMaxIpChi2' : 36.,
        'LambdaMinIpChi2' : 16.,
        'LambdaVtxChi2' : 9.,
        'LambdaSecMinVDChi2' : 100,
        'LambdaPrimMinVDChi2' : 50,
        'LambdaMinPt' : 500,
        'LambdaIPMin' : 0.2,

        #Sigma     #
        'SigmaMinTauPs' : 6.,
        'SigmaMinPt' : 500.,
        'SigmaMaxDOCA' : 2.,
        'SigmaMassWin' : 150.,
        'SigmaMinDIRA' : 0.9,
        'SigmaMaxIpChi2' : 36.,
        'SigmaVtxChi2' : 36.,
        'SigmaIPMin' : 0.05,
        #Xi
        'XiMassMax': (1320.+300.) *MeV,  #1315-1320
        'XiMassMin' : 0.,
        'XiMassWin': 300.,
        'XiMassWinTight': 50., #from Heavy B&Q 
        'XiMinTauPs' :6.,  #lifetime Xi+:1.6e-10, Xi0:2.9e-10
        'XiVtxChi2' : 25.,
        'XiMinPt' : 500 * MeV,
        'XiMinVDChi2' : 30,
        'XiIPMin' : 0.05,
        'XiIpChi2' : 36.,
        'XiMaxDOCA' : 1,



        #Omega
        'OmegaMassMax' : (1672.+300) * MeV,
        'OmegaMassMin' : 0 * MeV,
        'OmegaMassWin' : 300 * MeV,
        'OmegaMinTauPs' : 4.,
        'OmegaVtxChi2' : 25.,
        'OmegaMinPt' : 500 * MeV,
        #Combination
        'DiDOCA' : 2, #0.5
        'TriDOCA': 3,
        }
  }
  
#============================================================
# Line builder 
class StrangeHadronsConf(LineBuilder) :
    """
    Builder 
 
    """

    __configuration_keys__ = default_config['CONFIG'].keys() 
    
    #### This is the dictionary of all tunable cuts ########
    
    
    def __init__(self, name, config):         
        self.name = name    
        LineBuilder.__init__(self, name, config)

        Xi2Sigma0munu_name=name+'Xi-2Sigma0munu'
        Xi2ppipi_name=name+'Xi-2ppipi'
        Xi02ppi_name=name+'Xi02ppi'
        Omega2Lambda0pi_name=name+'Omega-2Lambda0pi'
        

        Muons = DataOnDemand(Location='Phys/StdAllLooseMuons/Particles')
        Protons = DataOnDemand(Location = "Phys/StdLooseProtons/Particles")         
        Pions = DataOnDemand(Location = "Phys/StdAllLoosePions/Particles")
        Gammas = DataOnDemand(Location = "Phys/StdLooseAllPhotons/Particles")       # StdLooseAllPhotons is used in Bs->Phi Gamma
        Electrons = DataOnDemand(Location = "Phys/StdAllLooseElectrons/Particles")
       
        track_cuts = ''' (TRCHI2DOF< %(trchi2dof)s) & (TRGHOSTPROB<%(trghostprob)s)'''
        pion_cuts = '''(PROBNNpi> %(pionProbNNpi)s) & (MIPCHI2DV(PRIMARY)>%(pionMinIpChi2)s)  &  (~ISMUON) & (PROBNNmu<0.7)  & (PROBNNk<0.7) '''
        proton_cuts = ''' (PROBNNp> %(protonProbNNp)s) & (MIPCHI2DV(PRIMARY)>%(protonMinIpChi2)s) &  (PROBNNmu<0.7) & (PROBNNk<0.7) '''
        muon_cuts = ''' (PROBNNmu> %(muonProbNNmu)s) & (MIPCHI2DV(PRIMARY)> %(muonMinIpChi2)s) & (MIPDV(PRIMARY) > %(muonMinIp)s) &  (ISMUON)  & (PROBNNpi<0.7) & (PROBNNk<0.7)'''
        electron_cuts = ''' (PROBNNe> %(electronProbNNe)s) & (MIPCHI2DV(PRIMARY)> %(electronMinIpChi2)s) & (PROBNNmu<0.7) & (PROBNNk<0.7) & (PROBNNpi<0.7)'''   # (HASRICH) 
        

        
        
##################### build L02PPi ###############################
        L02PPi = CombineSelection(
            'L02PPi_sel_for_StrangeHadrons',
            [Protons, Pions],
            DecayDescriptor="[Lambda0 -> p+ pi-]cc",
            DaughtersCuts={ "pi-" : (track_cuts + "&" + pion_cuts ) %config,
                            "p+" : (track_cuts + "&" + proton_cuts ) %config},
            CombinationCut= "(ADAMASS('Lambda0')<%(LambdaMassWinTight)s *MeV) & (AMAXDOCA('')< %(LambdaMaxDOCA)s *mm)"%config,
            MotherCut = "(VFASPF(VCHI2/VDOF)< %(LambdaVtxChi2)s)  & (PT> %(LambdaMinPt)s *MeV) & (ADMASS('Lambda0') < %(LambdaMassWinTight)s *MeV )  & (BPVIPCHI2()> %(LambdaMinIpChi2)s) & (BPVLTIME()> %(LambdaMinTauPs)s * ps) &(BPVVDCHI2 > %(LambdaSecMinVDChi2)s) & (MIPDV(PRIMARY)>%(LambdaIPMin)s*mm)" %config
            )
        


###################build Sigma02LambdaGamma#######################
        S02L0Gamma = CombineSelection(
            'Sigma2LambdaGamma_sel',
            [L02PPi, Gammas], #?
            DecayDescriptor='[Sigma0 -> Lambda0 gamma]cc', #?
            DaughtersCuts={ "gamma" : 'ALL',
                            "Lambda0" : 'ALL'}, #
            CombinationCut= "(ADAMASS('Sigma0')<%(SigmaMassWin)s *MeV) & (AMAXDOCA('')< %(SigmaMaxDOCA)s *mm)"%config,
            MotherCut="(VFASPF(VCHI2/VDOF)< %(SigmaVtxChi2)s)  & (PT> %(SigmaMinPt)s *MeV) & (ADMASS('Sigma+') < %(SigmaMassWin)s *MeV ) & (MIPDV(PRIMARY)>%(SigmaIPMin)s*mm) & (BPVLTIME()> %(SigmaMinTauPs)s * ps)"%config
                                    )  #

                


#################### build Xi-2Sigma0munu ##############################  
        
        Xi2Sigma0munu = CombineSelection(
            'Xi-2Sigma0munu_sel',
            [S02L0Gamma,Muons],
            DecayDescriptor='[Xi- -> Sigma0 mu- ]cc', #nu_mu~
            DaughtersCuts={"Sigma0": 'ALL',#(track_cuts + "&" + pion_cuts) %config,
                           "mu-": (track_cuts + "&" + muon_cuts) %config,
                },
            
            CombinationCut= "(AM< %(XiMassMax)s) & (AMAXDOCA('')< %(XiMaxDOCA)s *mm)"%config,
            MotherCut="(M< %(XiMassMax)s) & (BPVLTIME()> %(XiMinTauPs)s * ps) &  (VFASPF(VCHI2/VDOF)< %(XiVtxChi2)s)  & (BPVVDCHI2 > %(XiMinVDChi2)s) & (MIPDV(PRIMARY)>%(XiIPMin)s*mm) "%config

            #CombinationCut= "(ADAMASS('Xi-') < %(XiMassWin)s*MeV) & (AMAXDOCA('')< %(XiMaxDOCA)s *mm)"%config,
            #MotherCut="(ADMASS('Xi-') < %(XiMassWin)s*MeV) & (VFASPF(VCHI2/VDOF)<%(XiVtxChi2)s) & (BPVLTIME()> %(XiMinTauPs)s * ps) & (PT>%(XiMinPt)s) &  (BPVVDCHI2 > %(XiMinVDChi2)s) "%config
            #Xi2LambdaPiControl = Selection('Xi2LambdaPiControl_sel', Algorithm = FilterDesktop('Xi2LambdaPiControlFD', Code='( (MIPCHI2DV(PRIMARY)<%(XiIpChi2)s))'%config) ,RequiredSelections=[Xi2LambdaPi])
             )
                                
#################### build Xi-2PPiPi ##############################
        Xi2ppipi = CombineSelection(
            'Xi-2ppipi_sel',
            [Protons, Pions],
            DecayDescriptor='[Xi- -> p~-  pi+ pi-]cc',
            DaughtersCuts={ "p~-" : (track_cuts + "&" + proton_cuts + " & (TRGHOSTPROB<0.1)" ) %config,  # Should I introduce " & (TRGHOSTPROB<0.1)"  ????
                            "pi+" : (track_cuts + "&" + pion_cuts + " & (TRGHOSTPROB<0.1)" ) %config,
                            "pi-" : (track_cuts + "&" + pion_cuts + " & (TRGHOSTPROB<0.1)" ) %config},
            #CombinationCut= "(AM< %(XiMassMax)s) & (AMAXDOCA('')< %(XiMaxDOCA)s *mm)"%config,
            #MotherCut="(M< %(XiMassMax)s) & (BPVLTIME()> %(XiMinTauPs)s * ps) &  (VFASPF(VCHI2/VDOF)< %(XiVtxChi2)s)  & (BPVVDCHI2 > %(XiMinVDChi2)s) & (MIPDV(PRIMARY)>%(XiIPMin)s*mm) "%config

            CombinationCut= "(ADAMASS('Xi-') < %(XiMassWin)s*MeV) & (AMAXDOCA('')< %(XiMaxDOCA)s *mm)"%config,
            MotherCut="(ADMASS('Xi-') < %(XiMassWin)s*MeV) & (VFASPF(VCHI2/VDOF)<%(XiVtxChi2)s) & (BPVLTIME()> %(XiMinTauPs)s * ps) & (PT>%(XiMinPt)s) &  (BPVVDCHI2 > %(XiMinVDChi2)s) "%config          
                                            )   
                                


#################### build Xi02ppi ##############################  
        Xi02ppi = CombineSelection(
            'Xi02ppi_sel',
            [Protons, Pions],
            DecayDescriptor='[Xi0 -> p+ pi-]cc',
            DaughtersCuts={ "pi-" : (track_cuts + "&" + pion_cuts ) %config,
                            "p+" : (track_cuts + "&" + proton_cuts ) %config},
           # CombinationCut= "(AM< %(XiMassMax)s) & (AMAXDOCA('')< %(XiMaxDOCA)s *mm)"%config,
           # MotherCut="(M< %(XiMassMax)s) & (BPVLTIME()> %(XiMinTauPs)s * ps) &  (VFASPF(VCHI2/VDOF)< %(XiVtxChi2)s) & (MIPDV(PRIMARY)>%(XiIPMin)s*mm)"%config
            
            CombinationCut= "(ADAMASS('Xi0') < %(XiMassWin)s*MeV) & (AMAXDOCA('')< %(XiMaxDOCA)s *mm)"%config,
            MotherCut="(ADMASS('Xi0') < %(XiMassWinTight)s*MeV) & (BPVLTIME()> %(XiMinTauPs)s * ps) &  (VFASPF(VCHI2/VDOF)< %(XiVtxChi2)s) & (PT>%(XiMinPt)s) & (BPVIPCHI2()< %(XiIpChi2)s)"%config
            # Is this solution correct??
            
            )


###################build Omega2Lambda0pi################################ 

        Omega2Lambdapi = CombineSelection(
            'Omega2Lambdapi_sel',
            [L02PPi, Pions],
            DecayDescriptor='[Omega- -> Lambda0  pi-]cc',
            DaughtersCuts={ "Lambda0" : "ALL",
                            "pi-" : (track_cuts + "&" + pion_cuts) %config},
            CombinationCut= "(AM< %(OmegaMassMax)s)"%config,
            MotherCut="(M< %(OmegaMassMax)s) & (BPVLTIME()> %(OmegaMinTauPs)s * ps) &  (VFASPF(VCHI2/VDOF)< %(OmegaVtxChi2)s)"%config
            )



#################### build L02penu ##############################
        #L02penu = CombineSelection(
         #               'L02penu_sel',
          #              [Protons, Electrons],
           #             DecayDescriptor='[Lambda0 -> p+ e-]cc',
            #            DaughtersCuts={ "e+" : (track_cuts + "&" + electron_cuts ) %config,
             #                           "p+" : (track_cuts + "&" + proton_cuts ) %config},
              #          CombinationCut= "(AM< %(LambdaMassMax)s) & (AMAXDOCA('')< %(LambdaMaxDOCA)s *mm)"%config,
               #         MotherCut="(M< %(LambdaMassMax)s )  & (BPVLTIME()> %(LambdaMinTauPs)s * ps) & (VFASPF(VCHI2/VDOF)< %(LambdaVtxChi2)s) &  (BPVVDCHI2 > %(LambdaPrimMinVDChi2)s) & (MIPDV(PRIMARY)>%(LambdaIPMin)s*mm)"%config
#                        )

        





#################### build Lines ############################## 

        self.Xi2Sigma0MuNuLine = StrippingLine(Xi2Sigma0munu_name+"Line",
                                            prescale = config['Xi-2Sigma0munuPrescale'],
                                            postscale = config['Postscale'],
                                            selection=Xi2Sigma0munu,
                                            RequiredRawEvents = [],
                                            MDSTFlag=False
                                           )


        self.Xi2ppipiLine = StrippingLine(Xi2ppipi_name+"Line",
                                            prescale = config['Xi-2ppipiPrescale'],
                                            postscale = config['Postscale'],
                                            selection=Xi2ppipi,
                                            RequiredRawEvents = [],
                                            MDSTFlag=False
                                           )


        self.Xi02ppiLine = StrippingLine(Xi02ppi_name+"Line",
                                            prescale = config['Xi02ppiPrescale'],
                                            postscale = config['Postscale'],
                                            selection=Xi02ppi,
                                            RequiredRawEvents = [],
                                            MDSTFlag=False
                                           )


        self.Omega2Lambda0piLine = StrippingLine(Omega2Lambda0pi_name+"Line",
                                            prescale = config['Omega-2Lambda0piPrescale'],
                                            postscale = config['Postscale'],
                                            selection=Omega2Lambdapi,
                                            RequiredRawEvents = [],
                                            MDSTFlag=False
                                           )


    
 #       self.L02penuLine = StrippingLine(L02penu_name+"Line",
  #                                        prescale = config['L02penuPrescale'],
 #                                         postscale = config['Postscale'],
  #                                        selection=L02penu,
   #                                       RequiredRawEvents = [],
    #                                      MDSTFlag=False
     #                                     )


        

        
        self.registerLine(self.Xi2Sigma0MuNuLine)
        self.registerLine(self.Xi2ppipiLine)
        self.registerLine(self.Xi02ppiLine)
        self.registerLine(self.Omega2Lambda0piLine)
      #  self.registerLine(self.L02penuLine)
        



