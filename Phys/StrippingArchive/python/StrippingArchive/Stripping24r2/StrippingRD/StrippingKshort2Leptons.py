###############################################################################
# (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
__author__ = ['Adrian Casais Vidal', 'Carla Marin', 'Xabier Cid Vidal']
__date__ = '23/02/2018'
__version__ = '$2.0 $'
'''
stripping code for Kshort ---> Leptons
'''

default_config = {
    'NAME': 'Kshort2Leptons',
    'WGs': ['RD'],
    'BUILDERTYPE': 'Kshort2LeptonsConf',
    'CONFIG': {
        "e": {
            "L": {
                'PT': 100.,  #MeV
                'MINIPCHI2': 50,  #adimensional
                'PIDe': -1,  #adimensional
                'GhostProb': 0.35  #adimensional
            },
            "U": {
                'PT': 50,  #MeV
                'MINIPCHI2': 10,  #adimensional
                'PIDe': -3.5,  #adimensional
                'GhostProb': 0.35  #adimensional
            },
            "V": {
                'PT': 0,  #MeV
                'MINIPCHI2': 40,  #adimensional
                'PIDe': -1000,  #adimensional
                'GhostProb': 0.5  #adimensional
            },
            "Di": {
                'PT': 100.,  #MeV
                'MINIPCHI2': 16,  #adimensional
                'PIDe': -4,  #adimensional
                'GhostProb': 0.5  #adimensional
            },
        },
        "mu": {
            "L": {
                'PT': 50.,  #MeV
                'MINIPCHI2': 20,  #adimensional
                'PIDmu': -5,  #adimensional
                'GhostProb': 0.5  #adimensional
            },
            "U": {
                'PT': 50,  #MeV
                'MINIPCHI2': 10,  #adimensional
                'PIDmu': -3.5,  #adimensional
                'GhostProb': 0.35  #adimensional
            },
            "V": {
                'PT': 50,  #MeV
                'MINIPCHI2': 10,  #adimensional
                'PIDmu': -3.5,  #adimensional
                'GhostProb': 0.35  #adimensional
            },
        },
        "pi": {
            "L": {
                'PT': 100.,  #MeV
                'MINIPCHI2': 16,  #adimensional
                'PIDK': 5,  #adimensional
                'GhostProb': 0.5  #adimensional
            },
            "U": {
                'PT': 50,  #MeV
                'MINIPCHI2': 10,  #adimensional
                'PIDK': -3.5,  #adimensional
                'GhostProb': 0.35  #adimensional
            },
            "V": {
                'PT': 50,  #MeV
                'MINIPCHI2': 10,  #adimensional
                'PIDK': -3.5,  #adimensional
                'GhostProb': 0.35  #adimensional
            },
        },
        "2pi2e": {
            'LL': {
                'KsMAXDOCA': 1.,  #mm
                'KsLifetime': 0.01 * 89.53,  #0.01*10^-12s
                'KsIP': 1,  #mm
                'MaxKsMass': 800.,  #MeV, comb mass high limit
                'KsVtxChi2': 50,
                'Prescale': 1,
                'Postscale': 1
            },
            'UU': {
                'KsMAXDOCA': 1.,  #mm
                'KsDisChi2': 2500,  #adimensional
                'KsIP': 1,  #mm
                'MaxKsMass': 800.,  #MeV, comb mass high limit
                'KsVtxChi2': 35,  #adimensional
                'Prescale': 1,
                'Postscale': 1
            },
            'VV': {
                'KsMAXDOCA': 1.,  #mm
                'KsDisChi2': 2500,  #adimensional
                'KsIP': 8,  #mm
                'MaxKsMass': 900.,  #MeV, comb mass high limit
                'KsVtxChi2': 40,  #adimensional
                'Prescale': 1,
                'Postscale': 1
            },
            'LU': {
                'KsMAXDOCA': 1.,  #mm
                'KsDisChi2': 2500,  #adimensional
                'KsIP': 5,  #mm
                'MaxKsMass': 800.,  #MeV, comb mass high limit
                'KsVtxChi2': 25,  #adimensional
                'Prescale': 1,
                'Postscale': 1
            },
            'LV': {
                'KsMAXDOCA': .5,  #mm
                'KsDisChi2': 2500,  #adimensional
                'KsIP': 10,  #mm
                'MaxKsMass': 900.,  #MeV, comb mass high limit
                'KsVtxChi2': 37,  #adimensional
                'Prescale': 1,
                'Postscale': 1
            },
            'UV': {
                'KsMAXDOCA': .8,  #mm
                'KsDisChi2': 2000,  #adimensional
                'KsIP': 15,  #mm
                'MaxKsMass': 878.,  #MeV, comb mass high limit
                'KsVtxChi2': 19,  #adimensional
                'Prescale': 1,
                'Postscale': 1
            }
        },
        "2mu2e": {
            'LL': {
                'KsMAXDOCA': 1.,  #mm
                'KsLifetime': 0.01 * 89.53,  #0.01*10^-12s
                'KsIP': 1,  #mm
                'MaxKsMass': 800.,  #MeV, comb mass high limit
                'KsVtxChi2': 50,
                'Prescale': 1,
                'Postscale': 1
            },
            'UU': {
                'KsMAXDOCA': 1.,  #mm
                'KsDisChi2': 1500,  #adimensional
                'KsIP': 1,  #mm
                'MaxKsMass': 800.,  #MeV, comb mass high limit
                'KsVtxChi2': 40,  #adimensional
                'Prescale': 1,
                'Postscale': 1
            },
            'VV': {
                'KsMAXDOCA': 1.,  #mm
                'KsDisChi2': 1500,  #adimensional
                'KsIP': 8,  #mm
                'MaxKsMass': 900.,  #MeV, comb mass high limit
                'KsVtxChi2': 40,  #adimensional
                'Prescale': 1,
                'Postscale': 1
            },
            'LU': {
                'KsMAXDOCA': 1.,  #mm
                'KsDisChi2': 1500,  #adimensional
                'KsIP': 5,  #mm
                'MaxKsMass': 800.,  #MeV, comb mass high limit
                'KsVtxChi2': 35,  #adimensional
                'Prescale': 1,
                'Postscale': 1
            },
            'LV': {
                'KsMAXDOCA': .5,  #mm
                'KsDisChi2': 1500,  #adimensional
                'KsIP': 10,  #mm
                'MaxKsMass': 900.,  #MeV, comb mass high limit
                'KsVtxChi2': 40,  #adimensional
                'Prescale': 1,
                'Postscale': 1
            },
            'UV': {
                'KsMAXDOCA': .8,  #mm
                'KsDisChi2': 1000,  #adimensional
                'KsIP': 15,  #mm
                'MaxKsMass': 878.,  #MeV, comb mass high limit
                'KsVtxChi2': 25,  #adimensional
                'Prescale': 1,
                'Postscale': 1
            }
        },
        "3mue": {
            'LL': {
                'KsMAXDOCA': 1.,  #mm
                'KsDisChi2': 50,  #0.01*10^-12s
                'KsIP': 1,  #mm
                'MaxKsMass': 800.,  #MeV, comb mass high limit
                'KsVtxChi2': 50,
                'Prescale': 1,
                'Postscale': 1
            },
            'UU': {
                'KsMAXDOCA': 1.,  #mm
                'KsDisChi2': 2500,  #adimensional
                'KsIP': 1,  #mm
                'MaxKsMass': 800.,  #MeV, comb mass high limit
                'KsVtxChi2': 35,  #adimensional
                'Prescale': 1,
                'Postscale': 1
            },
            'VV': {
                'KsMAXDOCA': 1.,  #mm
                'KsDisChi2': 2500,  #adimensional
                'KsIP': 1,  #mm
                'MaxKsMass': 800.,  #MeV, comb mass high limit
                'KsVtxChi2': 35,  #adimensional
                'Prescale': 1,
                'Postscale': 1
            },
            'LU': {
                'KsMAXDOCA': 1.,  #mm
                'KsDisChi2': 2500,  #adimensional
                'KsIP': 1,  #mm
                'MaxKsMass': 800.,  #MeV, comb mass high limit
                'KsVtxChi2': 35,  #adimensional
                'Prescale': 1,
                'Postscale': 1
            },
            'LV': {
                'KsMAXDOCA': 1.,  #mm
                'KsDisChi2': 2500,  #adimensional
                'KsIP': 1,  #mm
                'MaxKsMass': 800.,  #MeV, comb mass high limit
                'KsVtxChi2': 35,  #adimensional
                'Prescale': 1,
                'Postscale': 1
            },
            'UV': {
                'KsMAXDOCA': 1.,  #mm
                'KsDisChi2': 2500,  #adimensional
                'KsIP': 1,  #mm
                'MaxKsMass': 800.,  #MeV, comb mass high limit
                'KsVtxChi2': 35,  #adimensional
                'Prescale': 1,
                'Postscale': 1
            }
        },
        "3emu": {
            'LL': {
                'KsMAXDOCA': 1.,  #mm
                'KsDisChi2': 2500,  #0.01*10^-12s
                'KsIP': 1,  #mm
                'MaxKsMass': 800.,  #MeV, comb mass high limit
                'KsVtxChi2': 50,
                'Prescale': 1,
                'Postscale': 1
            },
            'UU': {
                'KsMAXDOCA': 1.,  #mm
                'KsDisChi2': 2500,  #adimensional
                'KsIP': 1,  #mm
                'MaxKsMass': 800.,  #MeV, comb mass high limit
                'KsVtxChi2': 35,  #adimensional
                'Prescale': 1,
                'Postscale': 1
            },
            'VV': {
                'KsMAXDOCA': 1.,  #mm
                'KsDisChi2': 2500,  #adimensional
                'KsIP': 1,  #mm
                'MaxKsMass': 800.,  #MeV, comb mass high limit
                'KsVtxChi2': 35,  #adimensional
                'Prescale': 1,
                'Postscale': 1
            },
            'LU': {
                'KsMAXDOCA': 1.,  #mm
                'KsDisChi2': 2500,  #adimensional
                'KsIP': 1,  #mm
                'MaxKsMass': 800.,  #MeV, comb mass high limit
                'KsVtxChi2': 35,  #adimensional
                'Prescale': 1,
                'Postscale': 1
            },
            'LV': {
                'KsMAXDOCA': 1.,  #mm
                'KsDisChi2': 2500,  #adimensional
                'KsIP': 1,  #mm
                'MaxKsMass': 800.,  #MeV, comb mass high limit
                'KsVtxChi2': 35,  #adimensional
                'Prescale': 1,
                'Postscale': 1
            },
            'UV': {
                'KsMAXDOCA': 1.,  #mm
                'KsDisChi2': 2500,  #adimensional
                'KsIP': 1,  #mm
                'MaxKsMass': 800.,  #MeV, comb mass high limit
                'KsVtxChi2': 35,  #adimensional
                'Prescale': 1,
                'Postscale': 1
            }
        },
        "2mu2pi": {
            'LL': {
                'KsMAXDOCA': 3.,  #mm
                'KsDisChi2': 1500,  #0.01*10^-12s
                'KsIP': 5.,  #mm
                'MaxKsMass': 800.,  #MeV, comb mass high limit
                'KsVtxChi2': 100,
                'Prescale': 1,
                'Postscale': 1
            },
            'UU': {
                'KsMAXDOCA': 1.,  #mm
                'KsDisChi2': 2500,  #adimensional
                'KsIP': 1,  #mm
                'MaxKsMass': 800.,  #MeV, comb mass high limit
                'KsVtxChi2': 35,  #adimensional
                'Prescale': 1,
                'Postscale': 1
            },
            'VV': {
                'KsMAXDOCA': 1.,  #mm
                'KsDisChi2': 2500,  #adimensional
                'KsIP': 8,  #mm
                'MaxKsMass': 900.,  #MeV, comb mass high limit
                'KsVtxChi2': 40,  #adimensional
                'Prescale': 1,
                'Postscale': 1
            },
            'LU': {
                'KsMAXDOCA': 1.,  #mm
                'KsDisChi2': 2500,  #adimensional
                'KsIP': 5,  #mm
                'MaxKsMass': 800.,  #MeV, comb mass high limit
                'KsVtxChi2': 25,  #adimensional
                'Prescale': 1,
                'Postscale': 1
            },
            'LV': {
                'KsMAXDOCA': .5,  #mm
                'KsDisChi2': 2500,  #adimensional
                'KsIP': 10,  #mm
                'MaxKsMass': 900.,  #MeV, comb mass high limit
                'KsVtxChi2': 37,  #adimensional
                'Prescale': 1,
                'Postscale': 1
            },
            'UV': {
                'KsMAXDOCA': .8,  #mm
                'KsDisChi2': 2000,  #adimensional
                'KsIP': 15,  #mm
                'MaxKsMass': 878.,  #MeV, comb mass high limit
                'KsVtxChi2': 19,  #adimensional
                'Prescale': 1,
                'Postscale': 1
            }
        },
        "4mu": {
            'LL': {
                'KsMAXDOCA': 1.,  #mm
                'KsDisChi2': 1500.,  #0.01*10^-12s
                'KsIP': 1,  #mm
                'MaxKsMass': 800.,  #MeV, comb mass high limit
                'KsVtxChi2': 50,
                'Prescale': 1,
                'Postscale': 1
            },
            'UU': {
                'KsMAXDOCA': 1.,  #mm
                'KsDisChi2': 1500,  #adimensional
                'KsIP': 1,  #mm
                'MaxKsMass': 800.,  #MeV, comb mass high limit
                'KsVtxChi2': 35,  #adimensional
                'Prescale': 1,
                'Postscale': 1
            },
            'VV': {
                'KsMAXDOCA': 1.,  #mm
                'KsDisChi2': 1500,  #adimensional
                'KsIP': 1,  #mm
                'MaxKsMass': 800.,  #MeV, comb mass high limit
                'KsVtxChi2': 35,  #adimensional
                'Prescale': 1,
                'Postscale': 1
            },
            'LU': {
                'KsMAXDOCA': 1.,  #mm
                'KsDisChi2': 1500,  #adimensional
                'KsIP': 1,  #mm
                'MaxKsMass': 800.,  #MeV, comb mass high limit
                'KsVtxChi2': 25,  #adimensional
                'Prescale': 1,
                'Postscale': 1
            },
            'LV': {
                'KsMAXDOCA': .5,  #mm
                'KsDisChi2': 1500,  #adimensional
                'KsIP': 10,  #mm
                'MaxKsMass': 800.,  #MeV, comb mass high limit
                'KsVtxChi2': 35,  #adimensional
                'Prescale': 1,
                'Postscale': 1
            },
            'UV': {
                'KsMAXDOCA': 1.,  #mm
                'KsDisChi2': 1500,  #adimensional
                'KsIP': 1,  #mm
                'MaxKsMass': 800.,  #MeV, comb mass high limit
                'KsVtxChi2': 19,  #adimensional
                'Prescale': 1,
                'Postscale': 1
            },
        },
        "4e": {
            'LL': {
                'KsMAXDOCA': 2.,  #mm
                'KsLifetime': 0.01 * 89.53,  #0.01*10^-12s
                'KsIP': 2,  #mm
                'MaxKsMass': 800.,  #MeV, comb mass high limit
                'KsVtxChi2': 50,
                'Prescale': 1,
                'Postscale': 1
            },
            'UU': {
                'KsMAXDOCA': 1.,  #mm
                'KsDisChi2': 2500,  #adimensional
                'KsIP': 1,  #mm
                'MaxKsMass': 800.,  #MeV, comb mass high limit
                'KsVtxChi2': 35,  #adimensional
                'Prescale': 1,
                'Postscale': 1
            },
            'VV': {
                'KsMAXDOCA': 1.,  #mm
                'KsDisChi2': 2500,  #adimensional
                'KsIP': 1,  #mm
                'MaxKsMass': 900.,  #MeV, comb mass high limit
                'KsVtxChi2': 40,  #adimensional
                'Prescale': 1,
                'Postscale': 1
            },
            'LU': {
                'KsMAXDOCA': 1.,  #mm
                'KsDisChi2': 2500,  #adimensional
                'KsIP': 5,  #mm
                'MaxKsMass': 800.,  #MeV, comb mass high limit
                'KsVtxChi2': 25,  #adimensional
                'Prescale': 1,
                'Postscale': 1
            },
            'LV': {
                'KsMAXDOCA': .5,  #mm
                'KsDisChi2': 2500,  #adimensional
                'KsIP': 10,  #mm
                'MaxKsMass': 900.,  #MeV, comb mass high limit
                'KsVtxChi2': 37,  #adimensional
                'Prescale': 1,
                'Postscale': 1
            },
            'UV': {
                'KsMAXDOCA': .8,  #mm
                'KsDisChi2': 2000,  #adimensional
                'KsIP': 15,  #mm
                'MaxKsMass': 880.,  #MeV, comb mass high limit
                'KsVtxChi2': 20,  #adimensional
                'Prescale': 1,
                'Postscale': 1
            },
        },
        'TISTOSDict': {
            'Hlt2RareStrangeKsLeptonsTOSDecision%TOS': 0,
        },
    },
    'STREAMS': ['Leptonic']
}

__all__ = ('Kshort2Leptons', 'default_config')

from Gaudi.Configuration import *
from GaudiConfUtils.ConfigurableGenerators import FilterDesktop
from GaudiConfUtils.ConfigurableGenerators import DaVinci__N3BodyDecays as Combine3Particles
from PhysSelPython.Wrappers import Selection
from StrippingConf.StrippingLine import StrippingLine
from StrippingUtils.Utils import LineBuilder
from StandardParticles import StdAllNoPIDsMuons
from StandardParticles import StdAllLooseMuons
from StandardParticles import StdAllNoPIDsPions
from StandardParticles import StdNoPIDsUpPions
from StandardParticles import StdAllNoPIDsElectrons
from StandardParticles import StdDiElectronFromTracks
from StandardParticles import StdNoPIDsUpElectrons
from CommonParticles.Utils import *
from Configurables import ChargedProtoParticleMaker, NoPIDsParticleMaker, DelegatingTrackSelector, CombineParticles, LoKi__VertexFitter
from SelPy.utils import (UniquelyNamedObject, ClonableObject, SelectionBase)
from itertools import product


## VELO PARTICLES SETUP COPIED FROM StrippingTrackEffD0ToK3Pi
class GSWrapper(UniquelyNamedObject, ClonableObject, SelectionBase):
    def __init__(self, name, sequencer, output, requiredSelections):
        UniquelyNamedObject.__init__(self, name)
        ClonableObject.__init__(self, locals())
        SelectionBase.__init__(
            self,
            algorithm=sequencer,
            outputLocation=output,
            requiredSelections=requiredSelections)


##############################


class Kshort2LeptonsConf(LineBuilder):
    """
    Builder for Kshort --> Leptons
    """

    __configuration_keys__ = default_config['CONFIG'].keys()

    def __init__(self, name, config):

        self.name = name
        LineBuilder.__init__(self, name, config)

        ########################
        ## 0: make missing VELO/Upstream particles by hand

        self.makeparts_velo = ChargedProtoParticleMaker(
            name="MyProtoParticlesVelo" + self.name,
            Inputs=["Rec/Track/Best"],
            Output="Rec/ProtoP/MyProtosVelo" + self.name)
        self.makeparts_up = ChargedProtoParticleMaker(
            name="MyProtoParticlesUp" + self.name,
            Inputs=["Rec/Track/Best"],
            Output="Rec/ProtoP/MyProtosUp" + self.name)

        self.makeparts_velo.addTool(
            DelegatingTrackSelector, name="TrackSelector")
        self.makeparts_velo.TrackSelector.TrackTypes = ["Velo"]

        self.makeparts_up.addTool(
            DelegatingTrackSelector, name="TrackSelector")
        self.makeparts_up.TrackSelector.TrackTypes = ["Upstream"]

        self.myprotos_velo = GSWrapper(
            name="MyProtosVelo" + self.name,
            sequencer=self.makeparts_velo,
            output="Rec/ProtoP/MyProtosVelo" + self.name,
            requiredSelections=[])

        self.myprotos_up = GSWrapper(
            name="MyProtosUp" + self.name,
            sequencer=self.makeparts_up,
            output="Rec/ProtoP/MyProtosUp" + self.name,
            requiredSelections=[])

        #Making NoPIDs containers for the now available particles
        self.veloElectrons = NoPIDsParticleMaker(
            name="StdNoPIDsVeloElectronsLocal" + self.name,
            DecayDescriptor='Electron',
            Particle='electron',
            AddBremPhotonTo=[],
            Input=self.myprotos_velo.outputLocation())
        self.upMuons = NoPIDsParticleMaker(
            name="StdNoPIDsUpMuonsLocal" + self.name,
            DecayDescriptor='Muon',
            Particle='muon',
            AddBremPhotonTo=[],
            Input=self.myprotos_up.outputLocation())

        self.veloMuons = NoPIDsParticleMaker(
            name="StdNoPIDsVeloMuonsLocal" + self.name,
            DecayDescriptor='Muon',
            Particle='muon',
            AddBremPhotonTo=[],
            Input=self.myprotos_velo.outputLocation())
        self.veloPions = NoPIDsParticleMaker(
            name="StdNoPIDsVeloPionsLocal" + self.name,
            DecayDescriptor='Pion',
            Particle='pion',
            AddBremPhotonTo=[],
            Input=self.myprotos_velo.outputLocation())

        updateDoD(self.veloElectrons)
        updateDoD(self.veloMuons)
        updateDoD(self.upMuons)
        updateDoD(self.veloPions)

        self.StdNoPIDsVeloElectrons = Selection(
            'StdNoPIDsVeloElectrons' + self.name,
            Algorithm=self.veloElectrons,
            RequiredSelections=[self.myprotos_velo],
            InputDataSetter=None)

        self.StdNoPIDsUpMuons = Selection(
            'StdNoPIDsUpMuons' + self.name,
            Algorithm=self.upMuons,
            RequiredSelections=[self.myprotos_up],
            InputDataSetter=None)

        self.StdNoPIDsVeloMuons = Selection(
            'StdNoPIDsVeloMuons' + self.name,
            Algorithm=self.veloMuons,
            RequiredSelections=[self.myprotos_velo],
            InputDataSetter=None)
        self.StdNoPIDsVeloPions = Selection(
            'StdNoPIDsVeloPions' + self.name,
            Algorithm=self.veloPions,
            RequiredSelections=[self.myprotos_velo],
            InputDataSetter=None)

        #Handy dictionaries to access the different categories of particles.

        StandardParticles = {
            "mu": {
                "L": StdAllLooseMuons,
                "U": self.StdNoPIDsUpMuons,
                "V": self.StdNoPIDsVeloMuons
            },
            "e": {
                "L": StdAllNoPIDsElectrons,
                "V": self.StdNoPIDsVeloElectrons,
                "U": StdNoPIDsUpElectrons,
                "Di": StdDiElectronFromTracks
            },
            "pi": {
                "L": StdAllNoPIDsPions,
                "U": StdNoPIDsUpPions,
                "V": self.StdNoPIDsVeloPions
            }
        }

        # Applying cuts to particles

        particles = {"mu": {}, "pi": {}, "e": {}}

        for cat in ["L", "U", "V"]:

            particles["mu"][cat] = self.makeMuons(
                self.name + "Muons" + cat, StandardParticles["mu"][cat],
                default_config["CONFIG"]["mu"][cat])
            particles["pi"][cat] = self.makePions(
                self.name + "Pions" + cat, StandardParticles["pi"][cat],
                default_config["CONFIG"]["pi"][cat])
            particles["e"][cat] = self.makeElecs(
                self.name + "Electrons" + cat, StandardParticles["e"][cat],
                default_config["CONFIG"]["e"][cat])

        particles["e"]["Di"] = self.makeElecsFromTracks(
            self.name + "DiElectrons" + cat, StandardParticles["e"]["Di"],
            default_config["CONFIG"]["e"]["Di"])

        ########################
        ## 1: Make combinations for the different decays and different tracks combinations and building the Lines

        #KS0 -> mumuee
        diLongmumuee = particles["mu"]["L"]

        parSelsmumuee = {
            "LL": [diLongmumuee, particles["e"]["Di"]],
            "LU": [diLongmumuee, particles["e"]["L"], particles["e"]["U"]],
            "LV": [diLongmumuee, particles["e"]["L"], particles["e"]["V"]],
            "UU": [diLongmumuee, particles["e"]["U"]],
            "UV": [diLongmumuee, particles["e"]["U"], particles["e"]["V"]],
            "VV": [diLongmumuee, particles["e"]["V"]],
        }

        KS0mumuee = self.buildCategories(["mu+", "mu-", "e+", "e-"], True,
                                         parSelsmumuee,
                                         default_config["CONFIG"]["2mu2e"])
        KS0mumueeLFV1 = self.buildCategories(["mu+", "mu+", "e-", "e-"],
                                             True,
                                             parSelsmumuee,
                                             default_config["CONFIG"]["2mu2e"],
                                             cc=True)

        self.buildLines(self.name, "2mu2e", KS0mumuee)
        self.buildLines(self.name, "2mu2e", KS0mumueeLFV1, LFVLabel="LFV1")

        #KS0 -> 3mue
        diLong3mue = particles["mu"]["L"]

        parSels3mue = {
            "LL": [diLong3mue, particles["e"]["L"]],
            "LU": [diLong3mue, particles["e"]["U"]],
            "LV": [diLong3mue, particles["e"]["V"]],
            "UU": [diLong3mue, particles["mu"]["U"], particles["e"]["U"]],
            "UV": [diLong3mue, particles["mu"]["U"], particles["e"]["V"]],
            "VV": [diLong3mue, particles["e"]["V"], particles["mu"]["V"]]
        }

        KS03mue = self.buildCategories(["mu+", "mu-", "mu+", "e-"],
                                       False,
                                       parSels3mue,
                                       default_config["CONFIG"]["3mue"],
                                       cc=True)

        self.buildLines(self.name, "3mue", KS03mue)

        #KS0 -> 3emu
        diLong3emu = particles["e"]["L"]

        parSels3emu = {
            "LL": [diLong3emu, particles["mu"]["L"]],
            "LU": [diLong3emu, particles["mu"]["U"]],
            "LV": [diLong3emu, particles["mu"]["L"], particles["e"]["V"]],
            "UU": [diLong3emu, particles["mu"]["U"], particles["e"]["U"]],
            "UV": [diLong3emu, particles["mu"]["U"], particles["e"]["V"]],
            "VV": [diLong3emu, particles["e"]["V"], particles["mu"]["V"]]
        }

        KS03emu = self.buildCategories(["e+", "e-", "mu+", "e-"],
                                       False,
                                       parSels3emu,
                                       default_config["CONFIG"]["3emu"],
                                       cc=True)

        self.buildLines(self.name, "3emu", KS03emu)

        #KS0 -> pipiee
        diLongpipiee = particles["pi"]["L"]

        parSelspipiee = {
            "LL": [diLongpipiee, particles["e"]["Di"]],
            "LU": [diLongpipiee, particles["e"]["L"], particles["e"]["U"]],
            "LV": [diLongpipiee, particles["e"]["L"], particles["e"]["V"]],
            "UU": [diLongpipiee, particles["e"]["U"]],
            "UV": [diLongpipiee, particles["e"]["U"], particles["e"]["V"]],
            "VV": [diLongpipiee, particles["e"]["V"]],
        }

        KS0pipiee = self.buildCategories(["pi+", "pi-", "e+", "e-"], True,
                                         parSelspipiee,
                                         default_config["CONFIG"]["2pi2e"])

        self.buildLines(self.name, "2pi2e", KS0pipiee)

        #KS0 -> mumupipi
        diLongmumupipi = particles["mu"]["L"]

        parSelsmumupipi = {
            "LL": [diLongmumupipi, particles["pi"]["L"]],
            "LU": [diLongmumupipi, particles["pi"]["L"], particles["pi"]["U"]],
            "LV": [diLongmumupipi, particles["pi"]["L"], particles["pi"]["V"]],
            "UU": [diLongmumupipi, particles["pi"]["U"]],
            "UV": [diLongmumupipi, particles["pi"]["U"], particles["pi"]["V"]],
            "VV": [diLongmumupipi, particles["pi"]["V"]],
        }

        KS0mumupipi = self.buildCategories(["mu+", "mu-", "pi+", "pi-"], False,
                                           parSelsmumupipi,
                                           default_config["CONFIG"]["2mu2pi"])
        KS0mumupipiLFV1 = self.buildCategories(
            ["mu+", "mu+", "pi-", "pi-"],
            False,
            parSelsmumupipi,
            default_config["CONFIG"]["2mu2pi"],
            cc=True)

        self.buildLines(self.name, "2mu2pi", KS0mumupipi)
        self.buildLines(self.name, "2mu2pi", KS0mumupipiLFV1, LFVLabel="LFV1")
        #KS0 -> 4mu
        diLong4mu = particles["mu"]["L"]

        parSels4mu = {
            "LL": [diLong4mu],
            "LU": [diLong4mu, particles["mu"]["U"]],
            "LV": [diLong4mu, particles["mu"]["V"]],
            "UU": [diLong4mu, particles["mu"]["U"]],
            "UV": [diLong4mu, particles["mu"]["U"], particles["mu"]["V"]],
            "VV": [diLong4mu, particles["mu"]["V"]],
        }

        KS04mu = self.buildCategories(["mu+", "mu-", "mu+", "mu-"], False,
                                      parSels4mu,
                                      default_config["CONFIG"]["4mu"])

        self.buildLines(self.name, "4mu", KS04mu)

        #KS0 -> 4e
        diLong4e = particles["e"]["Di"]

        parSels4e = {
            "LL": [diLong4e],
            "LU": [diLong4e, particles["e"]["U"], particles["e"]["L"]],
            "LV": [diLong4e, particles["e"]["V"], particles["e"]["L"]],
            "UU": [diLong4e, particles["e"]["U"]],
            "UV": [diLong4e, particles["e"]["U"], particles["e"]["V"]],
            "VV": [diLong4e, particles["e"]["V"]],
        }

        KS04e = self.buildCategories(["e+", "e-", "e+", "e-"],
                                     True,
                                     parSels4e,
                                     default_config["CONFIG"]["4e"],
                                     onlyLL=False,
                                     is4e=True)

        self.buildLines(self.name, "4e", KS04e)

    def buildLines(self, name, dec, categories, LFVLabel=""):
        '''
        Handy function to build and register the lines.
        '''
        lines = {}

        for cat in categories:
            if ((dec == "4e") and cat in ["VV", "LV", "UV"]):
                lines[cat] = StrippingLine(
                    name + dec + cat + LFVLabel + "Line",
                    prescale=default_config["CONFIG"][dec][cat]["Prescale"],
                    postscale=default_config["CONFIG"][dec][cat]["Postscale"],
                    selection=categories[cat],
                    MaxCombinations=200000,
                    MDSTFlag=False)
            else:
                lines[cat] = StrippingLine(
                    name + dec + cat + LFVLabel + "Line",
                    prescale=default_config["CONFIG"][dec][cat]["Prescale"],
                    postscale=default_config["CONFIG"][dec][cat]["Postscale"],
                    selection=categories[cat],
                    MDSTFlag=False)

        for line in lines:
            self.registerLine(lines[line])

    def makePions(self, name, inputPions, pionCuts):
        """
        Pion selection.
        """
        #adding variables to function scope

        _code = "(PT > %(PT)s) &"\
                "(MIPCHI2DV(PRIMARY) > %(MINIPCHI2)s) &"\
                "(TRGHOSTPROB < %(GhostProb)s) &"\
                "(PIDK < %(PIDK)s)" % pionCuts

        _Filter = FilterDesktop(Code=_code)

        return Selection(
            name, Algorithm=_Filter, RequiredSelections=[inputPions])

    def makeMuons(self, name, inputMuons, muonCuts):
        """
        Muon selection.
        """

        _code = "(PT > %(PT)s) &"\
                "(MIPCHI2DV(PRIMARY) > %(MINIPCHI2)s) &"\
                "(TRGHOSTPROB < %(GhostProb)s) &"\
                "(PIDmu > %(PIDmu)s)" % muonCuts

        _Filter = FilterDesktop(Code=_code)

        return Selection(
            name, Algorithm=_Filter, RequiredSelections=[inputMuons])

    def makeElecs(self, name, inputElecs, electronCuts):
        """
        Electron.
        """



        _code = "( MIPCHI2DV(PRIMARY) > %(MINIPCHI2)s ) &"\
                "( PT > %(PT)s ) &"\
                "( TRGHOSTPROB < %(GhostProb)s ) & "\
                "( PIDe > %(PIDe)s )"  % electronCuts

        _Filter = FilterDesktop(Code=_code)

        return Selection(
            name, Algorithm=_Filter, RequiredSelections=[inputElecs])

    def makeElecsFromTracks(self, name, inputElecs, electronCuts):
        """
        Electron selection from StdDiElectronFromTracks
        """



        _code = "(MINTREE(ABSID<14,PT) > %(PT)s) &"\
                "(MINTREE(ABSID<14,MIPCHI2DV(PRIMARY)) > %(MINIPCHI2)s) &"\
                "(MAXTREE(ABSID<14,TRGHOSTPROB) < %(GhostProb)s) &"\
                "(MINTREE(ABSID<14,PIDe) > %(PIDe)s)"  % electronCuts

        _Filter = FilterDesktop(Code=_code)

        return Selection(
            name, Algorithm=_Filter, RequiredSelections=[inputElecs])

    def buildCategories(self,
                        pars,
                        hasDielectron,
                        parSels,
                        motherCuts,
                        cc=False,
                        onlyLL=False,
                        is4e=False):
        '''
        Builds the different cobinations of tracks using the class KshortTo4bodies.
        '''
        combCuts = self.makeCombCuts(pars)

        dec = "".join(pars)

        LL = KshortTo4Bodies(
            self.name + dec + "LL",
            pars,
            parSels["LL"],
            motherCuts["LL"],
            combCuts["LL"],
            hasDielectronLong=hasDielectron,
            hasVELO=False,
            CC=cc,
            is4e=is4e)  #0: LL
        if not onlyLL:
            LV = KshortTo4Bodies(
                self.name + dec + "LV",
                pars,
                parSels["LV"],
                motherCuts["LV"],
                combCuts["LV"],
                hasDielectronLong=False,
                hasVELO=True,
                CC=cc,
                is4e=is4e)  #1: LV
            LU = KshortTo4Bodies(
                self.name + dec + "LU",
                pars,
                parSels["LU"],
                motherCuts["LU"],
                combCuts["LU"],
                hasDielectronLong=False,
                hasVELO=False,
                CC=cc,
                is4e=is4e)  #2: LU
            UU = KshortTo4Bodies(
                self.name + dec + "UU",
                pars,
                parSels["UU"],
                motherCuts["UU"],
                combCuts["UU"],
                hasDielectronLong=False,
                hasVELO=False,
                CC=cc,
                is4e=is4e)  #3: UU
            UV = KshortTo4Bodies(
                self.name + dec + "UV",
                pars,
                parSels["UV"],
                motherCuts["UV"],
                combCuts["UV"],
                hasDielectronLong=False,
                hasVELO=True,
                CC=cc,
                is4e=is4e)  #4: UV
            VV = KshortTo4Bodies(
                self.name + dec + "VV",
                pars,
                parSels["VV"],
                motherCuts["VV"],
                combCuts["VV"],
                hasDielectronLong=False,
                hasVELO=True,
                CC=cc,
                is4e=is4e)  #5: VV

            return {
                "LL": LL.KS0,
                "LV": LV.KS0,
                "LU": LU.KS0,
                "UU": UU.KS0,
                "UV": UV.KS0,
                "VV": VV.KS0
            }
        else:

            return {"LL": LL.KS0}

    def makeCombCuts(self, pars):
        '''
        Function aimed no to write by hand every single combination cut needed when using Upstream/VELO tracks.
        '''
        cuts = {}
        pars_ = [par.replace('+', '').replace('-', '') for par in pars]

        cut = " ( ( ANUM( ( TRTYPE == 3 ) &  ( ABSID == '{0}' ) ) == 2 ) ) &".format(
            *pars)
        cuts[
            "LV"] = cut + " ( ( ANUM( ( TRTYPE == 3 ) &  ( ABSID == '{2}' ) ) == 1 ) )  & ( ( ANUM( ( TRTYPE == 1 ) &  ( ABSID == '{3}' ) ) == 1 ) )".format(
                *pars)
        cuts[
            "LU"] = cut + " ( ( ANUM( ( TRTYPE == 3 ) &  ( ABSID == '{2}' ) ) == 1 ) )  & ( ( ANUM( ( TRTYPE == 4 ) &  ( ABSID == '{3}' ) ) == 1 ) )".format(
                *pars)
        cuts[
            "UV"] = cut + " ( ( ANUM( ( TRTYPE == 4 ) &  ( ABSID == '{2}' ) ) == 1 ) )  & ( ( ANUM( ( TRTYPE == 1 ) &  ( ABSID == '{3}' ) ) == 1 ) )".format(
                *pars)

        if pars_[2] == pars_[3]:
            cuts[
                "UU"] = cut + " ( ( ANUM( ( TRTYPE == 4 ) &  ( ABSID == '{2}' ) ) == 2 ) )".format(
                    *pars)
            cuts[
                "VV"] = cut + " ( ( ANUM( ( TRTYPE == 1 ) &  ( ABSID == '{2}' ) ) == 2 ) )".format(
                    *pars)
            cuts[
                "LL"] = cut + " ( ( ANUM( ( TRTYPE == 3 ) &  ( ABSID == '{2}' ) ) == 2 ) )".format(
                    *pars)
        else:
            cuts[
                "UU"] = cut + " ( ( ANUM( ( TRTYPE == 4 ) &  ( ABSID == '{2}' ) ) == 1 ) )  & ( ( ANUM( ( TRTYPE == 4 ) &  ( ABSID == '{3}' ) ) == 1 ) )".format(
                    *pars)
            cuts[
                "VV"] = cut + " ( ( ANUM( ( TRTYPE == 1 ) &  ( ABSID == '{2}' ) ) == 1 ) )  & ( ( ANUM( ( TRTYPE == 1 ) &  ( ABSID == '{3}' ) ) == 1 ) )".format(
                    *pars)
            cuts[
                "LL"] = cut + " ( ( ANUM( ( TRTYPE == 4 ) &  ( ABSID == '{2}' ) ) == 3 ) )  & ( ( ANUM( ( TRTYPE == 3 ) &  ( ABSID == '{3}' ) ) == 1 ) )".format(
                    *pars)

        if pars_[0] + pars_[1] + pars_[2] + pars_[3] == "mumumue":

            cuts[
                "LL"] = "( ( ANUM( ( TRTYPE == 3 ) &  ( ABSID == 'mu-' ) ) == 3 ) ) &  ( ( ANUM( ( TRTYPE == 3 ) &  ( ABSID == 'e-' ) ) == 1 ) ) "
            cuts[
                "LV"] = "( ( ANUM( ( TRTYPE == 3 ) &  ( ABSID == 'mu-' ) ) == 3 ) ) &  ( ( ANUM( ( TRTYPE == 1 ) &  ( ABSID == 'e-' ) ) == 1 ) ) "
            cuts[
                "LU"] = "( ( ANUM( ( TRTYPE == 3 ) &  ( ABSID == 'mu-' ) ) == 3 ) ) &  ( ( ANUM( ( TRTYPE == 4 ) &  ( ABSID == 'e-' ) ) == 1 ) ) "

        if pars_[0] + pars_[1] + pars_[2] + pars_[3] == "eemue":

            cuts[
                "LL"] = "( ( ANUM( ( TRTYPE == 3 ) &  ( ABSID == 'e-' ) ) == 3 ) ) &  ( ( ANUM( ( TRTYPE == 3 ) &  ( ABSID == 'mu-' ) ) == 1 ) ) "

        if pars_[0] + pars_[1] + pars_[2] + pars_[3] == "mumumumu":

            cuts[
                "LL"] = "( ( ANUM( ( TRTYPE == 3 ) &  ( ABSID == 'mu-' ) ) == 4 ) ) "
            cuts[
                "LV"] = "( ( ANUM( ( TRTYPE == 3 ) &  ( ABSID == 'mu-' ) ) == 3 ) ) & ( ( ANUM( ( TRTYPE == 1 ) &  ( ABSID == 'mu-' ) ) == 1 ) ) "
            cuts[
                "LU"] = "( ( ANUM( ( TRTYPE == 3 ) &  ( ABSID == 'mu-' ) ) == 4 ) ) & ( ( ANUM( ( TRTYPE == 1 ) &  ( ABSID == 'mu-' ) ) == 1 ) ) "

        if pars_[0] + pars_[1] + pars_[2] + pars_[3] == "eeee":
            cuts[
                "LL"] = "( ( ANUM( ( TRTYPE == 3 ) &  ( ABSID == 'e-' ) ) == 4 ) ) "
            cuts[
                "LV"] = "( ( ANUM( ( TRTYPE == 3 ) &  ( ABSID == 'e-' ) ) == 1 ) ) & ( ( ANUM( ( TRTYPE == 1 ) &  ( ABSID == 'e-' ) ) == 1 ) ) "
            cuts[
                "LU"] = "( ( ANUM( ( TRTYPE == 3 ) &  ( ABSID == 'e-' ) ) == 1 ) ) & ( ( ANUM( ( TRTYPE == 4 ) &  ( ABSID == 'e-' ) ) == 1 ) ) "
            cuts[
                "UU"] = "  ( ( ANUM( ( TRTYPE == 4 ) &  ( ABSID == 'e-' ) ) == 2 ) )"
            cuts[
                "VV"] = "  ( ( ANUM( ( TRTYPE == 1 ) &  ( ABSID == 'e-' ) ) == 2 ) )"
            cuts[
                "UV"] = " ( ( ANUM( ( TRTYPE == 4 ) &  ( ABSID == 'e-' ) ) == 1 ) )  & ( ( ANUM( ( TRTYPE == 1 ) &  ( ABSID == 'e-' ) ) == 1 ) )".format(
                    *pars)

        #for cut in cuts:
        #print cut+ "  "+cuts[cut]
        return cuts


#####################################################


class KshortTo4Bodies():
    '''
    Class to aimed to output the selection using the different track combinations for Kshort -> 4 bodies (as the name suggests).
    '''

    def __init__(self,
                 name,
                 pars,
                 parSels,
                 motherCuts,
                 combCuts,
                 hasDielectronLong,
                 hasVELO,
                 CC=False,
                 is4e=False):

        decay = self.makeDecayDescriptor(pars, CC, is4e)

        #We apply different cuts deppending on the configuration of tracks. We also have a special case when having 4 electrons in the final state.

        if hasDielectronLong:
            decay = self.makeDecayDescriptorDiElectron(pars)

            self.KS0 = self.combineWithDiElectron(
                name, decay, parSels, motherCuts, is4e=is4e)

        elif hasVELO:

            decay = self.makeDecayDescriptorVELO(pars, CC, is4e)

            self.KS0 = self.combineWithVELO(name, decay, parSels, motherCuts,
                                            combCuts)

        else:

            self.KS0 = self.combine(name, decay, parSels, motherCuts, combCuts)

    def makeDecayDescriptor(self, pars, CC, is4e=False):

        descriptor = "KS0 -> {0} {1} {2} {3}".format(*pars)
        if is4e:
            descriptor = "KS0 -> J/psi(1S) e+ e-".format(*pars)
        if CC:
            descriptor = "[" + descriptor + "]cc"
        #print descriptor
        return descriptor

    def makeDecayDescriptorDiElectron(self, pars):

        descriptor = "KS0 -> {0} {1} J/psi(1S)".format(*pars)
        #print descriptor
        return descriptor

    def makeDecayDescriptorVELO(self, pars, CC, is4e=False):
        '''
        VELO tracks cannot distinguish the charge of particles so we neeed to take into account all combinations
        of charges.
        '''
        descriptor = "KS0 -> {0} {1} ".format(*pars)
        if is4e:
            descriptor = "KS0 -> J/psi(1S) "
        descriptors = []
        par3 = pars[2].replace("+", "")
        par3 = par3.replace("-", "")
        par4 = pars[3].replace("+", "")
        par4 = par4.replace("-", "")

        for iter in product("+-", repeat=2):
            descriptor_ = descriptor + par3 + iter[0] + " " + par4 + iter[1]
            if CC:
                descriptor_ = "[" + descriptor_ + "]cc"
            #print descriptor_
            descriptors.append(descriptor_)

        return descriptors

    def combineWithDiElectron(self,
                              name,
                              decay,
                              parSels,
                              motherCuts,
                              is4e=False):
        """
        Makes the KS0 -> X (J/psi(1S) -> e+ e-)
        """

        _comb12cut = "(AMAXDOCA('') < %(KsMAXDOCA)s *mm)" % motherCuts

        _combcut =      "(AM < %(MaxKsMass)s *MeV) & "\
                        "(AMAXDOCA('') < %(KsMAXDOCA)s *mm)" % motherCuts


        _mothercut =    "(M < %(MaxKsMass)s *MeV) &"\
                        "(MIPDV(PRIMARY) < %(KsIP)s *mm) & "\
                        "((BPVVDSIGN*M/P) > %(KsLifetime)s*2.9979e-01) & "\
                        "(VFASPF(VCHI2/VDOF) < %(KsVtxChi2)s) " % motherCuts

        _Combine = Combine3Particles(
            DecayDescriptor=decay,
            Combination12Cut=_comb12cut,
            CombinationCut=_combcut,
            MotherCut=_mothercut)
        if is4e:
            _Combine = CombineParticles(
                name="Combine" + name,
                DecayDescriptor="KS0 -> J/psi(1S) J/psi(1S)",
                CombinationCut=_combcut,
                MotherCut=_mothercut)
            #print name, is4e
        else:
            _Combine = Combine3Particles(
                DecayDescriptor=decay,
                Combination12Cut=_comb12cut,
                CombinationCut=_combcut,
                MotherCut=_mothercut)
            #print name,is4e

        return Selection(
            name + decay, Algorithm=_Combine, RequiredSelections=parSels)

    def combineWithVELO(self, name, decay, parSels, motherCuts, combCuts):
        """
        Makes the KS0 -> Leptons applying LoKi VertexFitter.
        """

        _combcut =      "(AM < %(MaxKsMass)s *MeV) & "\
                        "(AMAXDOCA('') < %(KsMAXDOCA)s *mm) &" % motherCuts
        _combcut += combCuts

        _mothercut =    "( M < %(MaxKsMass)s *MeV) &"\
                        "( MIPDV(PRIMARY) < %(KsIP)s *mm) & "\
                        "( BPVVDCHI2 > %(KsDisChi2)s) & "\
                        "( VFASPF(VCHI2/VDOF) < %(KsVtxChi2)s) " % motherCuts

        _Combine = CombineParticles(
            name="Combine" + name,
            DecayDescriptors=decay,
            CombinationCut=_combcut,
            MotherCut=_mothercut)
        _Combine.ParticleCombiners = {"": "LoKi::VertexFitter"}
        _Combine.addTool(LoKi__VertexFitter, name="LoKi::VertexFitter")

        return Selection(name, Algorithm=_Combine, RequiredSelections=parSels)

    def combine(self, name, decay, parSels, motherCuts, combCuts):
        """
        Makes the KS0 -> Leptons
        """

        _combcut =      "(AM < %(MaxKsMass)s *MeV) & "\
                        "(AMAXDOCA('') < %(KsMAXDOCA)s *mm) &" % motherCuts
        _combcut += combCuts

        _mothercut =    "( M < %(MaxKsMass)s *MeV) &"\
                        "( MIPDV(PRIMARY) < %(KsIP)s *mm) & "\
                        "( BPVVDCHI2 > %(KsDisChi2)s) & "\
                        "( VFASPF(VCHI2/VDOF) < %(KsVtxChi2)s) " % motherCuts

        _Combine = CombineParticles(
            name="Combine" + name,
            DecayDescriptor=decay,
            CombinationCut=_combcut,
            MotherCut=_mothercut)

        return Selection(name, Algorithm=_Combine, RequiredSelections=parSels)

    def getKS0(self):
        return self.KS0


# EOF
