###############################################################################
# (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
#############################################################
#      Stripping lines for LFV direct searches              #
# Author: X/ Cid Vidal xabier.cid.vidal@cern.ch (2016)      #
#############################################################

"""

LFV searches, based on functionality from StrippingA1MuMu by C. Elsasser

"""

from Gaudi.Configuration import *
from LHCbKernel.Configuration import *

from GaudiConfUtils.ConfigurableGenerators import CombineParticles
from PhysSelPython.Wrappers import Selection
from StrippingConf.StrippingLine import StrippingLine
from StrippingUtils.Utils import LineBuilder
from GaudiKernel.SystemOfUnits import MeV,picosecond
from StandardParticles import StdAllLooseMuons, StdAllLooseElectrons
from CommonParticles.Utils import *

__author__  = 'Xabier Cid Vidal'
__date__    = '22/09/2016'
__version__ = '$Revision: 1.0$'
__all__     = 'LFVExoticaConf', 'default_config'

default_config = {
  'NAME'        : 'LFVExotica',
  'BUILDERTYPE' : 'LFVExoticaConf',
  'WGs'         : [ 'QEE' ],
  'STREAMS'     : [ 'Leptonic' ], 
  'CONFIG'      : {'checkPV'        : False,
                   'Detached_LinePrescale'   : 1.0,
                   'Detached_LinePostscale'  : 1.0,
                   'Detached_SS_LinePrescale'   : 1.0,
                   'Detached_SS_LinePostscale'  : 1.0,
                   
                   'Detached_PT' : 500*MeV,
                   'Detached_P' : 10000*MeV,                                                  
                   'Detached_ProbNNmu' : 0.5,
                   'Detached_ProbNNe' : 0.,
                   'Detached_GhostProb': 0.3,
                   'Detached_IPChi2' : 16,
                   'Detached_VChi2' : 10,
                   'Detached_XIPChi2' : 16,
                   'Detached_M' : 0,
                   'Detached_TAU' : 1*picosecond,
                   'Detached_FDChi2' : 45,
                   
                   'Prompt_LinePrescale'   : 1.0,
                   'Prompt_LinePostscale'  : 1.0,
                   'Prompt_SS_LinePrescale'   : 0.5,
                   'Prompt_SS_LinePostscale'  : 1.0,

                   'Prompt_PT' : 500*MeV,
                   'Prompt_P' : 10000*MeV,                                                  
                   'Prompt_ProbNNmu' : 0.5,
                   'Prompt_ProbNNe' : 0.25,
                   'Prompt_GhostProb': 0.3,
                   'Prompt_IPChi2' : 1,
                   'Prompt_VChi2' : 5,
                   'Prompt_XIPChi2' : 1,
                   'Prompt_M' : 0,
                   'Prompt_FDChi2' : 1,
                   },
  }


## Jet isolation stuff
##########################################
## standard jetID
from JetAccessories.JetMaker_Config import JetMakerConf
stdjets_name_noban = "StdJetsNoJetIDNoBan"
StdJetsNoJetIDNoBan = JetMakerConf(stdjets_name_noban,
                                   R = 0.7 ,
                                   PtMin = 500.,
                                   JetIDCut = False).algorithms[0]

## configure Data-On-Demand service                                                        
locations = updateDoD ( StdJetsNoJetIDNoBan )


# the daughters banning is line dependent (need to provide the location of the daughters!)
def create_stdjets(strob,line_location,stdjets_name_ban):
    
    myconf1 = JetMakerConf(stdjets_name_ban,
                           R = 0.7 ,
                           PtMin = 500.,
                           listOfParticlesToBan = [line_location],
                           JetIDCut = False).algorithms[0]
    
    if "StdJetsNoJetIDBan" in dir(strob): strob.StdJetsNoJetIDBan.append(myconf1)
    else: strob.StdJetsNoJetIDBan= [myconf1]
                                      
    ## configure Data-On-Demand service                                            
    locations = updateDoD ( myconf1 )


relInfoJetsVars = ["JETNOMU1PX","JETNOMU1PY", "JETNOMU1PZ", "JETNOMU1PT", "JETNOMU1JETWIDTH", "JETNOMU1NNTAG", "JETNOMU1MNF", "JETNOMU2PX", "JETNOMU2PY", "JETNOMU2PZ", "JETNOMU2PT", "JETNOMU2JETWIDTH", "JETNOMU2NNTAG", "JETNOMU2MNF", "JETNOMU3PX", "JETNOMU3PY", "JETNOMU3PZ", "JETNOMU3PT", "JETNOMU3JETWIDTH", "JETNOMU3NNTAG", "JETNOMU3MNF", "JETMU1PX", "JETMU1PY", "JETMU1PZ", "JETMU1PT", "JETMU1JETWIDTH", "JETMU1NNTAG", "JETMU1MNF", "JETMU2PX", "JETMU2PY", "JETMU2PZ", "JETMU2PT", "JETMU2JETWIDTH", "JETMU2NNTAG", "JETMU2MNF"]

relInfoConeVars = ["CONEANGLE","CONEMULT","CONEP","CONEPT","CONEPASYM","CONEPTASYM","CONEDELTAETA","CONEDELTAPHI"]
    
class LFVExoticaConf(LineBuilder) :
  __configuration_keys__ = default_config['CONFIG'].keys()

  def __init__(self, name, config):
    LineBuilder.__init__(self, name, config)
    
    
    stdjets_name_ban_prompt = "StdJetsNoJetIDBanDaughtersPrompt"
    stdjets_name_ban_ssprompt = "StdJetsNoJetIDBanDaughtersSameSignPrompt"
    stdjets_name_ban_detached = "StdJetsNoJetIDBanDaughtersDetached"
    stdjets_name_ban_ssdetached = "StdJetsNoJetIDBanDaughtersSameSignDetached"

    RelatedInfoToolsPrompt = [{ 'Type' : 'RelInfoJetsVariables',
                                'Location': 'RelatedInfoJets',
                                'Variables': relInfoJetsVars,
                                'UseVarsJetsWithB' : False,
                                'LocationJetsNoMu' : "Phys/"+stdjets_name_ban_prompt+"/Particles",
                                'LocationJetsNoRemove' : "Phys/"+stdjets_name_noban+"/Particles"
                                }]
    
    RelatedInfoToolsSSPrompt = [{ 'Type' : 'RelInfoJetsVariables',
                                  'Location': 'RelatedInfoJets',
                                  'Variables': relInfoJetsVars,
                                  'UseVarsJetsWithB' : False,
                                  'LocationJetsNoMu' : "Phys/"+stdjets_name_ban_ssprompt+"/Particles",
                                  'LocationJetsNoRemove' : "Phys/"+stdjets_name_noban+"/Particles"
                                  }]

    RelatedInfoToolsDetached = [{ 'Type' : 'RelInfoJetsVariables',
                                  'Location': 'RelatedInfoJets',
                                  'Variables': relInfoJetsVars,
                                  'UseVarsJetsWithB' : False,
                                  'LocationJetsNoMu' : "Phys/"+stdjets_name_ban_detached+"/Particles",
                                  'LocationJetsNoRemove' : "Phys/"+stdjets_name_noban+"/Particles"
                                  }]
    
    RelatedInfoToolsSSDetached = [{ 'Type' : 'RelInfoJetsVariables',
                                    'Location': 'RelatedInfoJets',
                                    'Variables': relInfoJetsVars,
                                    'UseVarsJetsWithB' : False,
                                    'LocationJetsNoMu' : "Phys/"+stdjets_name_ban_ssdetached+"/Particles",
                                    'LocationJetsNoRemove' : "Phys/"+stdjets_name_noban+"/Particles"
                                    }]
    
    
    ## cone related infos
    otherRelatedInfos = [
        # cone variables
        {'Type' : 'RelInfoConeVariables',
         'Variables':relInfoConeVars,
         'Location': 'ConeVariables1',
         'ConeAngle' : 1.0,
         'DaughterLocations':{'[H_30 -> ^mu+ e-]CC' : 'MuonConeInfo1', '[H_30 -> mu+ ^e-]CC' : 'ElectronConeInfo1'}},
        {'Type' : 'RelInfoConeVariables',
         'Variables':relInfoConeVars,
         'Location': 'ConeVariables2',
         'ConeAngle' : 2.0,
         'DaughterLocations':{'[H_30 -> ^mu+ e-]CC' : 'MuonConeInfo2', '[H_30 -> mu+ ^e-]CC' : 'ElectronConeInfo2'}},
        {'Type': 'RelInfoVertexIsolation',
         'Location':'RelInfoVtxIso' }
        ]

    otherRelatedInfosSS = [
        # cone variables
        {'Type' : 'RelInfoConeVariables',
         'Variables':relInfoConeVars,
         'Location': 'ConeVariables1',
         'ConeAngle' : 1.0,
         'DaughterLocations':{'[H_30 -> ^mu+ e+]CC' : 'MuonConeInfo1', '[H_30 -> mu+ ^e+]CC' : 'ElectronConeInfo1'}},
        {'Type' : 'RelInfoConeVariables',
         'Variables':relInfoConeVars,
         'Location': 'ConeVariables2',
         'ConeAngle' : 2.0,
         'DaughterLocations':{'[H_30 -> ^mu+ e+]CC' : 'MuonConeInfo2', '[H_30 -> mu+ ^e+]CC' : 'ElectronConeInfo2'}},
        {'Type': 'RelInfoVertexIsolation',
         'Location':'RelInfoVtxIso' }
        ]
    
    for ri in [RelatedInfoToolsPrompt, RelatedInfoToolsDetached]: ri.extend(otherRelatedInfos)
    for ri in [RelatedInfoToolsSSPrompt, RelatedInfoToolsSSDetached]: ri.extend(otherRelatedInfosSS)
    
    ## [A1 -> mu+ e-]cc prompt line:
    sel_prompt = combinePrompt(name,config)
    
    promptline = StrippingLine(name + 'PromptLine',
                               prescale  = config['Prompt_LinePrescale'],
                               postscale = config['Prompt_LinePrescale'],
                               checkPV   = config['checkPV'],
                               selection = sel_prompt,
                               RelatedInfoTools = RelatedInfoToolsPrompt)
    self.registerLine(promptline)
    
    ### [A1 -> mu- e-]cc prompt line:
    sel_prompt_ss = combinePrompt(name,config,ss=True)
            
    sspromptline = StrippingLine(name + 'SameSignPromptLine',
                                 prescale  = config['Prompt_SS_LinePrescale'],
                                 postscale = config['Prompt_SS_LinePrescale'],
                                 checkPV   = config['checkPV'],
                                 selection = sel_prompt_ss,
                                 RelatedInfoTools = RelatedInfoToolsSSPrompt)
    self.registerLine(sspromptline)
    
    ### [A1 -> mu+ e-]cc detached line:
    sel_detached = combineDetached(name,config)

    detachedline = StrippingLine(name + 'DetachedLine',
                                 prescale  = config['Detached_LinePrescale'],
                                 postscale = config['Detached_LinePrescale'],
                                 checkPV   = config['checkPV'],
                                 selection = sel_detached,
                                 RelatedInfoTools = RelatedInfoToolsDetached)
    self.registerLine(detachedline)

    ### [A1 -> mu- e-]cc detached line:
    sel_detached_ss = combineDetached(name,config,ss=True)

    ssdetachedline = StrippingLine(name + 'SameSignDetachedLine',
                                   prescale  = config['Detached_SS_LinePrescale'],
                                   postscale = config['Detached_SS_LinePrescale'],
                                   checkPV   = config['checkPV'],
                                   selection = sel_detached_ss,
                                   RelatedInfoTools = RelatedInfoToolsSSDetached)
    self.registerLine(ssdetachedline)

    create_stdjets(self,promptline.outputLocation(),stdjets_name_ban_prompt)
    create_stdjets(self,sspromptline.outputLocation(),stdjets_name_ban_ssprompt)
    create_stdjets(self,detachedline.outputLocation(),stdjets_name_ban_detached)
    create_stdjets(self,ssdetachedline.outputLocation(),stdjets_name_ban_ssdetached)
    

def combinePrompt(name,config,ss=False):

  daugh_cut={'mu': ("(PT > %(Prompt_PT)s) " \
                    "& (P > %(Prompt_P)s) " \
                    "& (BPVIPCHI2() < %(Prompt_IPChi2)s) " \
                    "& (TRGHOSTPROB < %(Prompt_GhostProb)s) " \
                    "& (PROBNNmu > %(Prompt_ProbNNmu)s) ") %config,
             'e': ("(PT > %(Prompt_PT)s) " \
                   "& (P > %(Prompt_P)s) " \
                   "& (PROBNNe > %(Prompt_ProbNNe)s) " \
                   "& (BPVIPCHI2() < %(Prompt_IPChi2)s) " \
                   "& (TRGHOSTPROB < %(Prompt_GhostProb)s)") %config }
    
  comb_cut = "(APT > 2*%(Prompt_PT)s) &  (AM > %(Prompt_M)s) &" \
             "(ACUTDOCACHI2(%(Prompt_VChi2)s,''))" %config
  
  mother_cut = ("(PT > 2*%(Prompt_PT)s)" \
                "& (HASVERTEX)" \
                "& (VFASPF(VCHI2PDOF) < %(Prompt_VChi2)s) " \
                "& (BPVIPCHI2() < %(Prompt_XIPChi2)s)" \
                "& (BPVVDCHI2 < %(Prompt_FDChi2)s)" %config )
  
  decay = "[H_30 -> mu+ e-]cc"
  ename = "e-"
  namesel = name+"PromptSel"
  if ss:
    decay = "[H_30 -> mu+ e+]cc"
    ename = "e+" 
    namesel = name+"SSPromptSel"
  
  _combination = CombineParticles( DecayDescriptor    = decay,
                                   CombinationCut     = comb_cut,
                                   DaughtersCuts      = { 'mu+' : daugh_cut["mu"],
                                                          ename : daugh_cut["e"] },
                                   MotherCut          = mother_cut,
                                   )

  return Selection ( namesel,
                     Algorithm          = _combination,
                     RequiredSelections = [ StdAllLooseMuons, StdAllLooseElectrons]
                     )


def combineDetached(name,config,ss=False):

  daugh_cut={'mu': ("(PT > %(Detached_PT)s) " \
                    "& (P > %(Detached_P)s) " \
                    "& (BPVIPCHI2() > %(Detached_IPChi2)s) " \
                    "& (TRGHOSTPROB < %(Detached_GhostProb)s) " \
                    "& (PROBNNmu > %(Detached_ProbNNmu)s) ") %config,
             'e': ("(PT > %(Detached_PT)s) " \
                   "& (P > %(Detached_P)s) " \
                   "& (PROBNNe > %(Detached_ProbNNe)s) " \
                   "& (BPVIPCHI2() > %(Detached_IPChi2)s) " \
                   "& (TRGHOSTPROB < %(Detached_GhostProb)s)") %config }
    
  comb_cut = "(APT > 2*%(Detached_PT)s) &  (AM > %(Detached_M)s) &" \
             "(ACUTDOCACHI2(%(Detached_VChi2)s,''))" %config
  
  mother_cut = ("(PT > 2*%(Detached_PT)s)" \
                "& (HASVERTEX)" \
                "& (VFASPF(VCHI2PDOF) < %(Detached_VChi2)s) " \
                "& (BPVIPCHI2() < %(Detached_XIPChi2)s)" \
                "& (BPVVDCHI2 > %(Detached_FDChi2)s)" \
                "& (BPVLTIME() > %(Detached_TAU)s)" %config)
  
  decay = "[H_30 -> mu+ e-]cc"
  ename = "e-"
  namesel = name+"DetachedSel"
  if ss:
    decay = "[H_30 -> mu+ e+]cc"
    ename = "e+" 
    namesel = name+"SSDetachedSel"

  _combination = CombineParticles( DecayDescriptor    = decay,
                                   CombinationCut     = comb_cut,
                                   DaughtersCuts      = { 'mu+' : daugh_cut["mu"],
                                                          ename : daugh_cut["e"] },
                                   MotherCut          = mother_cut,
                                   )

  return Selection ( namesel,
                     Algorithm          = _combination,
                     RequiredSelections = [ StdAllLooseMuons, StdAllLooseElectrons]
                     )

