###############################################################################
# (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
#\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\#

from copy import deepcopy
from Gaudi.Configuration import *
from GaudiConfUtils.ConfigurableGenerators import CombineParticles
from PhysSelPython.Wrappers import Selection
from Beauty2Charm_LoKiCuts import LoKiCuts
from Beauty2Charm_Utils import *
from StandardParticles import ( StdAllNoPIDsKaons, StdTightKaons, StdTightPions)

#\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\#

class LTUnbiasedBuilder(object):
    '''Makes all lifetime unbiased decays for the Beauty2Charm module.'''
    
    def __init__(self,d,config):
        self.config = deepcopy(config)
        self.kaons = [topoInputs('PromptKaons',[StdAllNoPIDsKaons])]
        self.kaons_pid = [topoInputs('PromptKaonsPid',[StdTightKaons])]
        self.kaons_tight_pid = [filterSelection('B2OCLTUTIGHTK',"(PROBNNk > 0.5)",self.kaons_pid)]
        self.pions_pid = [topoInputs('PromptPionsPid',[StdTightPions])]
        self.pions_tight_pid = [filterSelection('B2OCLTUTIGHTPI',"(PROBNNpi>0.5)",self.pions_pid)]
        self.d = d
        self.lines = []  
        self._makeB02DH('D2HHH',self.d.hhh_cf_pid)
        ds_hhh_pid_tight = [filterSelection('B2OCLTUDs2HHH',"(MM > 1920*MeV) & (MM < 2020*MeV)",self.d.hhh_pid_tight)]
        self._makeB02DHHH('D2HHH',ds_hhh_pid_tight)

    def _makeSels(self,decays,xtag,inputs):
        sels = []
        for tag, decay in decays.iteritems():
            comboCuts = LoKiCuts(['SUMPT','AM'],self.config).code()
            momCuts = [LoKiCuts(['VCHI2DOF'],self.config).code(),
                       hasTopoChildren()]
            momCuts = LoKiCuts.combine(momCuts)
            b2x = CombineParticles(DecayDescriptors=decay,
                                   CombinationCut=comboCuts,
                                   MotherCut=momCuts)
            sel = Selection(tag+xtag+'Beauty2Charm',Algorithm=b2x,
                            RequiredSelections=inputs[tag])
            sels.append(sel)
        return sels

    def _makeB02DH(self,dname,d2x):
        decays = {'B02DKLTUB' : ["[B0 -> D- K+]cc"]}
        inputs = {'B02DKLTUB' : d2x+self.kaons}
        sel = self._makeSels(decays,dname,inputs)
        self.lines.append(ProtoLine(sel,1.0))

    def _makeB02DHHH(self,dname,d2x):
        decays = {'B02DsKPiPiLTUB' : ["[B0 -> D- K+ pi+ pi-]cc"]}
        inputs = {'B02DsKPiPiLTUB' : d2x+self.kaons_tight_pid+self.pions_tight_pid}
        sel = self._makeSels(decays,dname,inputs)
        self.lines.append(ProtoLine(sel,1.0))

#\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\#
