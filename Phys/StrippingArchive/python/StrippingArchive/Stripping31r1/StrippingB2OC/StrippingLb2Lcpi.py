###############################################################################
# (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
'''
Module for selecting Lb->Lambda_c+ pi
'''

__author__=['Jiesheng Yu', 'Yuehong Xie']
__date__ = '28/11/2016'
__version__= '$Revision: 2.0$'


__all__ = (
    'Lb2LambdacPiConf',
    'default_config'
    )

default_config = {
    'NAME'              :  'Lb2LambdacPi',
    'BUILDERTYPE'       :  'Lb2LambdacPiConf',
    'CONFIG'    : {
        'KaonCuts'      : "(TRCHI2DOF < 4.) & (PT > 100*MeV) & (P > 1000*MeV) & (MIPCHI2DV(PRIMARY) > 4.) & (TRGHOSTPROB < 0.4) & (PIDK > -10.)",
        'ProtonCuts'      : "(TRCHI2DOF < 4.) & (PT > 100*MeV) & (P > 1000*MeV) & (MIPCHI2DV(PRIMARY) > 4.) & (TRGHOSTPROB < 0.4) & (PIDp > -10.)",
        'PionCuts'      : "(TRCHI2DOF < 4.) & (PT > 100*MeV) & (P > 1000*MeV) & (MIPCHI2DV(PRIMARY) > 4.) & (TRGHOSTPROB < 0.4) & (PIDK < 20.)",
        #'KaonCuts'      : "(PROBNNk > 0.1) & (PT > 300*MeV) & (TRGHOSTPROB<0.4)",
        #'ProtonCuts'    : "(PROBNNp > 0.1) & (PT > 300*MeV) & (P > 10*GeV) & (TRGHOSTPROB<0.4)",        
        #'PionCuts'      : "(PROBNNpi > 0.1) & (PT > 250*MeV) & (TRGHOSTPROB<0.4)",
        'LambdacComAMCuts' : "(AM<2.375*GeV)",
        'LambdacComN4Cuts' : """
                          (in_range(2.195*GeV, AM, 2.375*GeV))
                          & ( (ACHILD(PT,1)+ACHILD(PT,2)+ACHILD(PT,3)+ACHILD(PT,4) ) > 2.5 *GeV)
                          & ( (ACHILD(MIPCHI2DV(), 1) + ACHILD(MIPCHI2DV(), 2) + ACHILD(MIPCHI2DV(), 3) + ACHILD(MIPCHI2DV(), 4))>30)
                          """,
        'LambdacMomN4Cuts' : """
                           (VFASPF(VCHI2/VDOF) < 10.) 
                           & (BPVDIRA> 0.) 
                           & (in_range(2.185*GeV, MM, 2.385*GeV)) 
                           & (BPVVDCHI2>36) 
                           """,
        'LambdacComCuts'   : "(in_range(2.195*GeV, AM, 2.375*GeV))",
        'Lambda0Cuts'        : "(ADMASS('Lambda0') < 50.*MeV) & (PT > 250*MeV) & (BPVVDCHI2 >36)",
        'LbComCuts'     : "(ADAMASS('Lambda_b0') < 500 *MeV)",
        'LbMomCuts'     : """
                          (VFASPF(VCHI2/VDOF) < 10.) 
                          & (BPVDIRA> 0.999) 
                          & (BPVIPCHI2()<25) 
                          & (BPVVDCHI2>250)
                          & (BPVVDRHO>0.1*mm) 
                          & (BPVVDZ>2.0*mm)
                          """,
        'Prescale'      : 1.,
        'RelatedInfoTools': [{
                      'Type'              : 'RelInfoVertexIsolation',
                      'Location'          : 'RelInfoVertexIsolation'
                  }, {
                      'Type'              : 'RelInfoVertexIsolationBDT',
                      'Location'          : 'RelInfoVertexIsolationBDT'
                  }, {
                      'Type'              : 'RelInfoConeVariables',
                      'ConeAngle'         : 1.0,
                      "Variables" : ['CONEANGLE', 'CONEMULT', 'CONEPTASYM'], 
                      'Location'          : 'RelInfoConeVariables_1.0'
                  }, {
                      'Type'              : 'RelInfoConeVariables',
                      'ConeAngle'         : 1.5,
                      "Variables" : ['CONEANGLE', 'CONEMULT', 'CONEPTASYM'], 
                      'Location'          : 'RelInfoConeVariables_1.5'
                  }, {
                      'Type'              : 'RelInfoConeVariables',
                      'ConeAngle'         : 2.0,
                      "Variables" : ['CONEANGLE', 'CONEMULT', 'CONEPTASYM'], 
                      'Location'          : 'RelInfoConeVariables_2.0'
                  }]        
        },
    'STREAMS'           : ['Bhadron' ],
    'WGs'               : ['B2OC'],
    }


from Gaudi.Configuration import *
from GaudiConfUtils.ConfigurableGenerators import FilterDesktop, CombineParticles 
from PhysSelPython.Wrappers import Selection, DataOnDemand
from StrippingConf.StrippingLine import StrippingLine
from StrippingUtils.Utils import LineBuilder
#from Configurables import DaVinci__N4BodyDecays
from GaudiConfUtils.ConfigurableGenerators import DaVinci__N4BodyDecays
from PhysSelPython.Wrappers import MergedSelection
from Configurables import LoKi__VoidFilter
from StandardParticles import ( StdAllNoPIDsPions, StdAllNoPIDsKaons,
                                StdAllNoPIDsProtons, StdNoPIDsUpPions,
                                StdLooseMuons, StdNoPIDsUpKaons,
                                StdLooseResolvedPi0, StdLooseMergedPi0 )
from StandardParticles import StdLooseAllPhotons, StdVeryLooseAllPhotons

class Lb2LambdacPiConf(LineBuilder):
    
    __configuration_keys__ = default_config['CONFIG'].keys()
    
    def __init__(self, name, config ): 
        
        LineBuilder.__init__(self, name, config)
        self.name = name 
        self.config = config


        self.SelKaons = self.createSubSel( OutputList = self.name + "SelKaons",
                                           InputList =  DataOnDemand(Location = 'Phys/StdAllNoPIDsKaons/Particles' ), 
                                           Cuts = config['KaonCuts']
                                           )

        self.SelPions = self.createSubSel( OutputList = self.name + "SelPions",
                                           InputList =  DataOnDemand(Location = 'Phys/StdAllNoPIDsPions/Particles' ), 
                                           Cuts = config['PionCuts']
                                           )

        self.SelProtons = self.createSubSel( OutputList = self.name + "SelProtons",
                                           InputList =  DataOnDemand(Location = 'Phys/StdALLProtons/Particles' ), 
                                           Cuts = config['ProtonCuts']
                                           )

        #"""
        #Eta_c
        #"""
        #from PhysSelPython.Wrappers import MergedSelection

        #self.SelLambdac = MergedSelection( self.name + "SelLambdac",
        #                                RequiredSelections =  [ self.SelLambdac2KKPiPi, 
        #                                                        self.SelLambdac2KKKK,
        #                                                        self.SelLambdac2PiPiPiPi,
        #                                                        self.SelLambdac2PPbarPiPi,
        #                                                        ])
        #
        
        """
        Lambda0
        """
        # Both LL and DD 
        self.InputLambda0 = MergedSelection( self.name + "InputLambda0",
                                        RequiredSelections = [DataOnDemand(Location = "Phys/StdLooseLambdaDD/Particles"),
                                                              DataOnDemand(Location = "Phys/StdLooseLambdaLL/Particles")] )

        self.SelLambda0 = self.createSubSel( OutputList = self.name + "SelLambda0",
                                        InputList = self.InputLambda0,
                                        Cuts = config['Lambda0Cuts'] ) 

                                                     

        """
        Lambdac-> Lambda0 Pi
        """
        self.SelLambdac2Lambda0Pi = self.createCombinationSel( OutputList = self.name + "SelLambdac2Lambda0Pi",
                                                        DaughterLists = [ self.SelPions, self.SelLambda0],
                                                        DecayDescriptor = "[Lambda_c+ -> Lambda0 pi+]cc",
                                                        PreVertexCuts  = config['LambdacComCuts'], 
                                                        PostVertexCuts = config['LambdacMomN4Cuts']
                                                        )
        """
        Lb-> Lambdac Pi 
        """
        self.SelLb2LambdacPi_Lambdac2Lambda0Pi = self.createCombinationSel( OutputList = self.name + "SelLb2LambdacPi_Lambdac2Lambda0Pi",
                                                              DecayDescriptor = "[Lambda_b0 -> Lambda_c+ pi-]cc",
                                                              DaughterLists = [ self.SelLambdac2Lambda0Pi, self.SelPions],                    
                                                              PreVertexCuts  = config['LbComCuts'],
                                                              PostVertexCuts = config['LbMomCuts'] )

        self.Lb2LambdacPiLambdac2Lambda0PiLine = StrippingLine( self.name + 'Lambdac2Lambda0PiLine',                                                
                                                   prescale  = config['Prescale'],
                                                 #  HLT       = config['HLTCuts'],
                                                   algos     = [ self.SelLb2LambdacPi_Lambdac2Lambda0Pi ],
                                                   EnableFlavourTagging = False,
                                                   MDSTFlag = False,
                                                   RelatedInfoTools = self.config['RelatedInfoTools']                                                   
                                                   )

        self.registerLine( self.Lb2LambdacPiLambdac2Lambda0PiLine )
        
        


    def createSubSel( self, OutputList, InputList, Cuts ) :
        '''create a selection using a FilterDesktop'''
        filter = FilterDesktop(Code = Cuts)
        return Selection( OutputList,
                          Algorithm = filter,
                          RequiredSelections = [ InputList ] )

    def createCombinationSel( self, OutputList,
                              DecayDescriptor,
                              DaughterLists,
                              DaughterCuts = {} ,
                              PreVertexCuts = "ALL",
                              PostVertexCuts = "ALL",
                              ReFitPVs = True ) :
        '''create a selection using a ParticleCombiner with a single decay descriptor'''
        combiner = CombineParticles( DecayDescriptor = DecayDescriptor,
                                     DaughtersCuts = DaughterCuts,
                                     MotherCut = PostVertexCuts,
                                     CombinationCut = PreVertexCuts,
                                     ReFitPVs = False)
        return Selection ( OutputList,
                           Algorithm = combiner,
                           RequiredSelections = DaughterLists)


    def createN4BodySel( self, OutputList,
                         DecayDescriptor,
                         DaughterLists,
                         DaughterCuts = {} ,
                         ComAMCuts      = "AALL",
                         PreVertexCuts  = "AALL",
                         PostVertexCuts = "ALL" ) :
        '''create a selection using a ParticleCombiner with a single decay descriptor'''
        combiner = DaVinci__N4BodyDecays ( DecayDescriptor = DecayDescriptor,      
                                           DaughtersCuts = DaughterCuts,
                                           Combination12Cut  = ComAMCuts + "&" + "( ACHI2DOCA(1,2)<20 )",
                                           Combination123Cut = ComAMCuts + "&" + "( ACHI2DOCA(1,3)<20 ) & ( ACHI2DOCA(2,3)<20 )",
                                           CombinationCut = "( ACHI2DOCA(1,4)<20 ) & ( ACHI2DOCA(2,4)<20 ) & ( ACHI2DOCA(3,4)<20 )" + " & " + PreVertexCuts,
                                           MotherCut = PostVertexCuts,
                                           ReFitPVs = False )
        return Selection ( OutputList,
                           Algorithm = combiner,
                           RequiredSelections = DaughterLists)
    

    def applyMVA( self, name, 
                  SelB,
                  MVAVars,
                  MVAxmlFile,
                  MVACutValue
                  ):
        from MVADictHelpers import addTMVAclassifierValue
        from Configurables import FilterDesktop as MVAFilterDesktop

        _FilterB = MVAFilterDesktop( name + "Filter",
                                     Code = "VALUE('LoKi::Hybrid::DictValue/" + name + "')>" + MVACutValue  )

        addTMVAclassifierValue( Component = _FilterB,
                                XMLFile   = MVAxmlFile,
                                Variables = MVAVars,
                                ToolName  = name )
        
        return Selection( name,
                          Algorithm =  _FilterB,
                          RequiredSelections = [ SelB ] )
