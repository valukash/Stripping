###############################################################################
# (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""
Module importing stripping selection line builder modules
for BnoC WG.
"""

_selections = [ 'StrippingHb2Charged2Body',
                'StrippingB2HHBDT',
                'StrippingD2HHBDT',
                'StrippingBc2hhh_BnoC',
                'StrippingBu2hhh',
                'StrippingB2pphh',
                'StrippingB24pLines',
                'StrippingBs2Kst_0Kst_0',
                'StrippingB2HHPi0',
                'StrippingB2KShh',
                'StrippingLb2V0hh',
                'StrippingBs2PhiPhi',
                'StrippingB2Kpi0',
                'StrippingB2KShhh',
                'StrippingXb23ph', 
                'StrippingHb2V0V0h',
                'StrippingB2Ksthh',
                'StrippingBu2Ksthh',
                'StrippingBs2PhiKst0',
                'StrippingXb2p3h',
                'StrippingBs2KKhh',
                'StrippingBs2KSKS_Run2',
                'StrippingBu2rho0rhoPlus',
                'StrippingXb2phh',
                'StrippingB2XEta',
                'StrippingB2a1Pi',
                'StrippingB2CharmlessInclusive',
                'StrippingButo5h',
                'StrippingB2TwoBaryons',
                ]

for _sel in _selections :
    try :
        __import__( '%s.%s'  % ( __name__, _sel ) )
    except Exception, x:
        print '[WARNING] Submodule %s.%s raises the exception "%s" and will be skipped !' % ( __name__,_sel,x )

from sys import modules as _modules
_this = _modules[__name__]

_strippingKeys = filter ( lambda x : x[:9]=='Stripping',
                          locals().keys())

_strippingModules = [getattr(_this, _k) for _k in _strippingKeys]
