###############################################################################
# (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""
Module importing stripping selection line builder modules
for RD WG.
"""

_selections = [
    'StrippingB2Lambda0MuLines',
    'StrippingB2XMuMu',
    'StrippingB2XTauMu',
    'StrippingB2KLLXInclusive',
    'StrippingB2KstTauTau',
    'StrippingBu2LLK',
    'StrippingBd2KSLLX',
    'StrippingRareStrange',
    'StrippingRareNStrange',
    'StrippingBeauty2XGamma',
    'StrippingBeauty2XGammaExclusive',
    'StrippingBeauty2XGammaNoBias',
    'StrippingLb2L0Gamma',
    'StrippingBs2gammagamma',
    'StrippingBd2eeKstarBDT',
    'StrippingDarkBoson',
    'StrippingB2XTau',
    'StrippingZVTOP',
    'StrippingBs2MuMuLines',
    'StrippingBu2MuNu',
    'StrippingD23MuLines',
    'StrippingB23MuLines',
    'StrippingBLVLines',
    'StrippingK0s2XXMuMu',
    'StrippingKshort2MuMuMuMu',
    'StrippingKshort2PiPiMuMu',
    'StrippingKshort2eePiPi',
    'StrippingLc23MuLines',
    'StrippingLFVLines',
    'StrippingRareStrange',
    'StrippingRareNStrange',
    'StrippingTau2LambdaMuLines',
    'StrippingTau23MuLines',
    'StrippingBs2st2KKMuX',
    'StrippingPhiToKSKS',
    'StrippingB2XLL',
    'StrippingB2LLXBDT',
    'StrippingB2MuMuMuMuLines',
    'StrippingHypb2L0HGamma',
    'StrippingB02Lcmu_pKpi',
    'StrippingDoubleSLForRX',
    'StrippingBu2MajoLep',
    'StrippingStrangeSL',
    'StrippingBs2MuMuGamma'
    ]

for _sel in _selections :
    try :
        __import__( '%s.%s'  % ( __name__, _sel ) )
    except Exception, x:
        print '[WARNING] Submodule %s.%s raises the exception "%s" and will be skipped !' % ( __name__,_sel,x )

from sys import modules as _modules
_this = _modules[__name__]

_strippingKeys = filter ( lambda x : x[:9]=='Stripping',
                          locals().keys())

_strippingModules = [getattr(_this, _k) for _k in _strippingKeys]

