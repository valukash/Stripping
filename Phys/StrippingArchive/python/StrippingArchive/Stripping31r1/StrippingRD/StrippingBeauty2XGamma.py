###############################################################################
# (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""
B-> X Gamma selections
"""

__author__ = ['Violaine Bellee', 'Albert Puig']
__date__ = '08/04/2016'

__all__ = ('Beauty2XGammaConf',
           'makeDiTrack',
           'makeTriTrack',
           'makeFourTrack',
           'makeB',
           'default_config',)

default_config = {
    'NAME' : 'Beauty2XGamma',
    'WGs' : ['RD'],
    'BUILDERTYPE' : 'Beauty2XGammaConf',
    'CONFIG'      : {
          'B2XG2piPrescale'            : 1.,
          'B2XG2piCNVLLPrescale'       : 1.,
          'B2XG2piCNVDDPrescale'       : 1.,
          'B2XGpiKsPrescale'           : 1.,
          'B2XG2piGGPrescale'          : 1.,
          'B2XG3piGGPrescale'          : 1.,
          'B2XG3piPrescale'            : 1.,
          'B2XG3piCNVLLPrescale'       : 1.,
          'B2XG3piCNVDDPrescale'       : 1.,
          'B2XG2pipi0MPrescale'        : 1.,
          'B2XG2pipi0RPrescale'        : 1.,
          'B2XG2piKsPrescale'          : 1.,
          'B2PhiOmega2pipipi0MPrescale': 1.,
          'B2PhiOmega2pipipi0RPrescale': 1.,
          'B2XG4piPrescale'            : 1.,
          'B2XG3piKsPrescale'          : 1.,
          'B2XG3pipi0MPrescale'        : 1.,
          'B2XG3pipi0RPrescale'        : 1.,
          'B2XG2pi2KsPrescale'         : 1.,
          'B2XGLambdapiPrescale'       : 1.,
          'B2XGLambda2piPrescale'      : 1.,
          'B2XGLambda3piPrescale'      : 1.,
          ### WARNING ###
          ## Additional selection present in hard-coded cuts, starting somewhere below!!!
          ### WARNING ###
          # TRACK CUTS
          'B2XGTrkChi2DOF'   : 3., # was nothing
          'TrackGhostProb'   : 0.4, # was 0.5
          'B2XGTrkMinIPChi2' : 20., # was 5
          'B2XGTrkMinPT'     : 300., # was 100
          'B2XGTrkMinP'      : 1000, # P was 2000, old stripping has 1 GeV...
          # PHOTON CUTS
          'B2XGGammaPTMin'   : 2000.,
          'B2XGGammaCL'      : 0.,# was 0.25
          'B2XGGammaCNVPTMin': 1000.,
          # Neutral pion cuts
          'Pi0MPMin'   : 4000.,
          #'Pi0MPTMin' : 700., # was 1200
          'Pi0RPMin'   : 4000.,
          'Pi0MPTMin'  : 700.,
          'Pi0RPTMin'  : 700., # basic cut for phi/omega -> pi pi pi0 lines, recut harder for rest with below
          'Pi0RPTReCut': 1200.,
          'Pi0MPTReCut' : 1200.,
          # TRACK COMBINATION CUTS
          'B2XGResMinPT'       : 150.,
          'B2XGResMinMass'     : 0.,
          'B2XGResMaxMass'     : 5000.,
          'B2XGResVtxChi2DOF'  : 9.,
          'B2XGResSumPtMin'    : 1000.,
          'B2XGResBPVVDCHI2Min': 0.,
          'B2XGPhiOmegaMinMass': 700.,
          'B2XGPhiOmegaMaxMass': 1300.,
          # B HADRON CUTS
          'B2XGBMinPT'       : 1000.,
          'B2XGBMinMpi'      : 3200.,
          'B2XGBMinM2pi'     : 2400.,
          'B2XGBMinM3pi'     : 2400.,  # We cannot afford to go lower
          'B2XGBMinM4pi'     : 2000.,  # We cannot afford to go lower
          'B2XGBMaxM'        : 6500.,
          'B2XGBSumPtMin'    : 3000,
          'B2XGBMinBPVDIRA'  : 0.0,  #BPVDIRA avoid if possible
          'B2XGBVtxChi2DOF'  : 9.,
          'B2XGBVtxMaxIPChi2': 9., # was 25
          'B2XGBMinBPVFDCHI2': 0.,
          # Trigger TOS
          'Hlt1TISTOSLinesDict': {'Hlt1(Two)?TrackMVA(Loose)?Decision%TOS':0,
                                  'Hlt1(Phi)?IncPhi.*Decision%TOS':0,
                                  'Hlt1B2GammaGamma.*Decision%TOS':0,
                                  'Hlt1B2PhiGamma_LTUNB.*Decision%TOS':0
                                  },
          'Hlt2TISTOSLinesDict': {'Hlt2Topo(2|3|4)Body.*Decision%TOS':0,
                                  # 'Hlt2Topo(2|3|4)Body.*Decision%TIS':0,
                                  'Hlt2(Phi)?IncPhi.*Decision%TOS':0,
#                                  'Hlt2IncPhi.*Decision%TOS':0,
                                  # 'Hlt2IncPhi.*Decision%TIS':0,
                                  #'Hlt2RadiativeTopo.*Decision%TOS':0, ## Cut based raditive topological
                                  #'Hlt2RadiativeTopo.*Decision%TIS':0, ## Cut based raditive topological
                                  #'Hlt2TopoRad.*Decision%TOS':0, ## BBDT based radiative topological
                                  #'Hlt2TopoRad.*Decision%TIS':0, ## BBDT based radiative topological
                                  #'Hlt2Bs2PhiGamma.*Decision%TOS':0,
                                  #'Hlt2Bs2PhiGamma.*Decision%TIS':0,
                                  #'Hlt2Bd2KstGamma.*Decision%TOS':0,
                                  #'Hlt2Bd2KstGamma.*Decision%TIS':0
                                  'Hlt2Radiative.*Decision%TOS':0,
                                  # 'Hlt2Radiative.*Decision%TIS':0,
                                  # """This is equivalent to:
                                      # 'Hlt2RadiativeInc.*Decision%TOS':0, ## Run II inclusive radiative lines (BBDT based)
                                  # 'Hlt2RadiativeInc.*Decision%TIS':0,
                                  # 'Hlt2Radiatives2PhiGamma.*Decision%TOS':0, ## Run II exclusive radiative lines
                                  # 'Hlt2Radiatives2PhiGamma.*Decision%TIS':0,
                                  # 'Hlt2Radiatived2KstGamma.*Decision%TOS':0,
                                  # 'Hlt2Radiatived2KstGamma.*Decision%TIS':0,
                                  # 'Hlt2Radiative2GammaGamma.*Decision%TOS':0,
                                  # 'Hlt2Radiative2GammaGamma.*Decision%TIS':0,
                                  # 'Hlt2RadiativeLb2L0Gamma.*Decision%TOS':0,
                                  # 'Hlt2RadiativeLb2L0Gamma.*Decision%TIS':0,"""
                                  }

          },
    'STREAMS' : ['Leptonic'],
}

from Gaudi.Configuration import *
from GaudiConfUtils.ConfigurableGenerators import FilterDesktop, CombineParticles
from PhysSelPython.Wrappers import Selection, DataOnDemand, MergedSelection
from StrippingConf.StrippingLine import StrippingLine
from StrippingUtils.Utils import LineBuilder
from StandardParticles import StdAllNoPIDsKaons, StdAllNoPIDsPions, StdLooseAllPhotons, StdLooseMergedPi0, StdLooseResolvedPi0, StdAllLooseGammaDD,StdAllLooseGammaLL, StdAllNoPIDsProtons
from GaudiConfUtils.ConfigurableGenerators import DaVinci__N3BodyDecays as Combine3Particles
from GaudiConfUtils.ConfigurableGenerators import DaVinci__N4BodyDecays as Combine4Particles
from Configurables import TisTosParticleTagger


name = "Beauty2XGamma"

class Beauty2XGammaConf(LineBuilder):
    """Builder for Beauty2XGamma."""

    __configuration_keys__ = default_config['CONFIG'].keys()

    __confdict__ = {}

    def __init__(self, name, config):
        self.name = name
        self.__confdict__ = config
        LineBuilder.__init__(self, name, config)

        # Prepare related info
        rel_info = lambda top, children: [get_cone_relinfo(1.7, top, children),
                                          get_cone_relinfo(1.35, top, children),
                                          get_cone_relinfo(1.0, top, children),
                                          get_vtxisol_relinfo(top),
                                          get_vtxisol_relinfo_radiative(top)]

        # Generic track (pi) = h
        trackList = Selection('TrackList%s' % self.name,
                              Algorithm=FilterDesktop(Code="(HASTRACK) "
                                                           "& (TRCHI2DOF < %(B2XGTrkChi2DOF)s) "
                                                           "& (MIPCHI2DV(PRIMARY) > %(B2XGTrkMinIPChi2)s) "
                                                           "& (TRGHOSTPROB < %(TrackGhostProb)s) "
                                                           "& (PT > %(B2XGTrkMinPT)s) "
                                                           "& (P > %(B2XGTrkMinP)s)" % config),
                              RequiredSelections=[StdAllNoPIDsPions])

        # Neutral Pions, both merged and resolved
        mergedPi0 = Selection('MergedPi0Sel%s' % self.name,
                              Algorithm=FilterDesktop(Code="(P > %(Pi0MPMin)s) & (PT > %(Pi0MPTMin)s)" % config),
                              RequiredSelections=[StdLooseMergedPi0])
        resolvedPi0 = Selection('ResolvedPi0Sel%s' % self.name,
                                Algorithm=FilterDesktop(Code="(P > %(Pi0RPMin)s) & (PT > %(Pi0RPTMin)s)" % config),
                                RequiredSelections=[StdLooseResolvedPi0])
        # Ks0s
        mergedKshorts = MergedSelection('MergedKshorts',
                                        RequiredSelections=[DataOnDemand("Phys/StdLooseKsDD/Particles"),
                                                            DataOnDemand("Phys/StdLooseKsLL/Particles")])
        ks = Selection('KS0Sel%s' % self.name,
                                Algorithm=FilterDesktop(Code="(PT > 1000) "
                                                             "& (MM > 480*MeV) "
                                                             "& (MM < 515*MeV) "
                                                             "& (HASVERTEX) "
                                                             "& (VFASPF(VCHI2/VDOF)<9)" % config),
                                RequiredSelections=[mergedKshorts])

        # Lambdas
        mergedLambdas = MergedSelection('MergedLambdas',
                                        RequiredSelections=[DataOnDemand("Phys/StdLooseLambdaLL/Particles"),
                                                            DataOnDemand("Phys/StdLooseLambdaDD/Particles")])
        lambdas = Selection('LambdaSel%s' % self.name,
                            Algorithm=FilterDesktop(Code="(PT > %(B2XGResMinPT)s)" % config),
                            RequiredSelections=[mergedLambdas])
        ######################################
        # Hadronic part
        ######################################
        ##########
        # Di-track
        ##########
        diTrackCuts = dict(MinPTCut=config['B2XGResMinPT'],
                           MinSumPTCut=config['B2XGResSumPtMin'],
                           MaxMassCut=config['B2XGResMaxMass'],
                           MinMassCut=config['B2XGResMinMass'],
                           VtxChi2DOFCut=config['B2XGResVtxChi2DOF'])
        # hh
        diTrackList = makeDiTrack("DiTracksForRadiative%s" % self.name,
                                  [trackList],
                                  "rho(770)0 -> pi+ pi-",
                                  **diTrackCuts)
        # h + Ks0
        diTrackList_wKS0 = makeDiTrack("DiTracks_wKS0_ForRadiative%s" % self.name,
                                       [trackList, ks],
                                       "[rho(770)+ -> pi+ KS0]cc",
                                       **diTrackCuts)

        # h + Lambda
        diTrackList_wLambda = makeDiTrack("DiTracks_wLambda_ForRadiative%s" % self.name,
                                          [trackList, lambdas],
                                          "[rho(770)+ -> pi+ Lambda0]cc",
                                          **diTrackCuts)

        ###########
        # Tri-track
        ###########
        triTrackCuts = dict(MinPTCut=config['B2XGResMinPT'],
                            MinSumPTCut=config['B2XGResSumPtMin'],
                            MaxMassCut=config['B2XGResMaxMass'],
                            MinMassCut=config['B2XGResMinMass'],
                            VtxChi2DOFCut=config['B2XGResVtxChi2DOF'],
                            BPVVDCHI2MinCut=config['B2XGResBPVVDCHI2Min'])
        # hhh built as h+h+h
        triTrackList = makeTriTrack("TriTracks_ForRadiative%s" % self.name,
                                    trackList,
                                    "[K_1(1270)+ -> pi+ pi- pi+]cc",
                                    **triTrackCuts)


        # hhKs0 built as h+h+Ks0
        triTrackList_wKS0 = makeTriTrack("TriTracks_wKS0_ForRadiative%s" % self.name,
                                    [trackList, ks],
                                    "K_1(1270)0 -> pi+ pi- KS0",
                                    **triTrackCuts)

        # h+h+merged pi0
        triTrackCuts['Pi0PTCut'] = config['Pi0MPTReCut']
        triTrackList_wpi0M = makeTriTrack("TriTracks_wPi0M_ForRadiative%s" % self.name,
                                          [trackList, mergedPi0],
                                          "K_1(1270)0 -> pi+ pi- pi0",
                                          **triTrackCuts)

        # h+h+resolved pi0
        triTrackCuts['Pi0PTCut'] = config['Pi0RPTReCut']
        triTrackList_wpi0R = makeTriTrack("TriTracks_wPi0R_ForRadiative%s" % self.name,
                                          [trackList, resolvedPi0],
                                          "K_1(1270)0 -> pi+ pi- pi0",
                                          **triTrackCuts)

        # h+h+Lambda0
        triTrackList_wLambda = makeTriTrack("TriTracks_wLambda_ForRadiative%s" % self.name,
                                            [trackList, lambdas],
                                            "K_1(1270)0 -> pi+ pi- Lambda0",
                                            **triTrackCuts)

        # Omegas
        triTrackCutsForOmega = dict(MinPTCut=config['B2XGResMinPT'],
                                    MinSumPTCut=config['B2XGResSumPtMin'],
                                    MaxMassCut=config['B2XGPhiOmegaMaxMass'],
                                    MinMassCut=config['B2XGPhiOmegaMinMass'],
                                    VtxChi2DOFCut=config['B2XGResVtxChi2DOF'],
                                    BPVVDCHI2MinCut=config['B2XGResBPVVDCHI2Min'])
        # hh + merged pi0 for omega / phi analysis
        triTrackCutsForOmega['Pi0PTCut'] = config['Pi0MPTMin']
        triTrackList_wpi0M_phiomega = makeTriTrack("TriTracks_wPi0M_phiomega_ForRadiative%s" % self.name,
                                                   [trackList, mergedPi0],
                                                   "K_1(1270)0 -> pi+ pi- pi0",
                                                   **triTrackCutsForOmega)

        # hh + resolved pi0 for omega / phi analysis
        triTrackCutsForOmega['Pi0PTCut'] = config['Pi0RPTMin']
        triTrackList_wpi0R_phiomega = makeTriTrack("TriTracks_wPi0R_phiomega_ForRadiative%s" % self.name,
                                                   [trackList, resolvedPi0],
                                                   "K_1(1270)0 -> pi+ pi- pi0",
                                                   **triTrackCutsForOmega)

        ############
        # Four-track
        ############
        fourTrackCuts = dict(MinPTCut=config['B2XGResMinPT'],
                             MinSumPTCut=config['B2XGResSumPtMin'],
                             MaxMassCut=config['B2XGResMaxMass'],
                             MinMassCut=config['B2XGResMinMass'],
                             VtxChi2DOFCut=config['B2XGResVtxChi2DOF'],
                             BPVVDCHI2MinCut=config['B2XGResBPVVDCHI2Min'])
        # hhhh built as h+h+h+h
        fourTrackList = makeFourTrack("FourTracks_ForRadiative%s" % self.name,
                                      trackList,
                                      "a_1(1260)0 -> pi+ pi+ pi- pi-",
                                      **fourTrackCuts)

        # hhhKs0 built as h+h+h+Ks0
        fourTrackList_wKS0 = makeFourTrack("FourTracks_wKS0_ForRadiative%s" % self.name,
                                           [trackList, ks],
                                           "[a_1(1260)+ -> pi+ pi+ pi- KS0]cc",
                                           **fourTrackCuts)

        # hhKs0Ks0 built as h+Ks0+h+Ks0
        fourTrackList_w2KS0 = makeFourTrack("FourTracks_w2KS0_ForRadiative%s" % self.name,
                                            [trackList, ks],
                                            "a_1(1260)0 -> pi+ pi+ KS0 KS0",
                                            **fourTrackCuts)

        # h+h+h+Lambda
        fourTrackList_wLambda = makeFourTrack("FourTracks_wLambda_ForRadiative%s" % self.name,
                                              [trackList, lambdas],
                                              "[a_1(1260)+ -> pi+ pi+ pi- Lambda0]cc",
                                              **fourTrackCuts)

        # h+h+h+merged pi0
        fourTrackCuts['Pi0PTCut'] = config['Pi0MPTReCut']
        fourTrackList_wpi0M = makeFourTrack("FourTracks_wPi0M_ForRadiative%s" % self.name,
                                            [trackList, mergedPi0],
                                            "[a_1(1260)+ -> pi+ pi+ pi- pi0]cc",
                                            **fourTrackCuts)

        # h+h+h+resolved pi0
        fourTrackCuts['Pi0PTCut'] = config['Pi0RPTReCut']
        fourTrackList_wpi0R = makeFourTrack("FourTracks_wPi0R_ForRadiative%s" % self.name,
                                            [trackList, resolvedPi0],
                                            "[a_1(1260)+ -> pi+ pi+ pi- pi0]cc",
                                            **fourTrackCuts)

        ######################################
        # Photons
        ######################################
        photons = Selection('PhotonSel%s' % self.name,
                            Algorithm=FilterDesktop(Code="(PT > %(B2XGGammaPTMin)s)"
                                                         "& (CL > %(B2XGGammaCL)s)" % config),
                            RequiredSelections=[StdLooseAllPhotons])

        convCuts = ("(MM < 100*MeV) "
                    "& (HASVERTEX) "
                    "& (VFASPF(VCHI2/VDOF)<9) "
                    "& (PT > %(B2XGGammaCNVPTMin)s)" % config)
        convLLPhotons = Selection('ConvLLPhoton%s' % self.name,
                                  Algorithm=FilterDesktop(Code=convCuts),
                                  RequiredSelections=[StdAllLooseGammaLL])
        convDDPhotons = Selection('ConvDDPhoton%s' % self.name,
                                  Algorithm=FilterDesktop(Code=convCuts),
                                  RequiredSelections=[StdAllLooseGammaDD])

        ######################################
        # Stripping lines
        ######################################
        # B -> hh gammma
        b2XG2piName = self.name + "2pi_"
        b2XG2piCuts = dict(MinPTCut=config['B2XGBMinPT'],
                           MinMassCut=config['B2XGBMinM2pi'],
                           MaxMassCut=config['B2XGBMaxM'],
                           VtxChi2DOFCut=config['B2XGBVtxChi2DOF'],
                           MinSumPTCut=config['B2XGBSumPtMin'],
                           MinBPVDIRACut=config['B2XGBMinBPVDIRA'],
                           VtxMaxIPChi2Cut=config['B2XGBVtxMaxIPChi2'],
                           MinBPVFDCHI2Cut=config['B2XGBMinBPVFDCHI2'])
        b2XG2piSel = fullTisTosSelection(makeB(b2XG2piName,
                                               [diTrackList, photons],
                                               'B0 -> rho(770)0 gamma',
                                               **b2XG2piCuts),
                                         config)
        self.registerLine(StrippingLine(b2XG2piName+"Line",
                                        prescale=config['B2XG2piPrescale'],
                                        selection=b2XG2piSel,
                                        EnableFlavourTagging=True,
                                        MDSTFlag=False,
                                        RelatedInfoTools=rel_info(b2XG2piSel,
                                                                  {'Gamma': 'B0 -> rho(770)0 ^gamma',
                                                                   'Res': 'B0 -> ^rho(770)0 gamma'})))
        # B -> hh gamma gamma
        b2XG2piGGName = self.name + "2pi_gammagamma"
        b2XG2piGGSel = fullTisTosSelection(makeB(b2XG2piGGName,
                                                 [diTrackList, photons],
                                                 'B0 -> rho(770)0 gamma gamma',
                                                 **b2XG2piCuts),
                                           config)
        self.registerLine(StrippingLine(b2XG2piGGName+"Line",
                                        prescale=config['B2XG2piGGPrescale'],
                                        selection=b2XG2piGGSel,
                                        EnableFlavourTagging=True,
                                        MDSTFlag=False,
                                        RelatedInfoTools=rel_info(b2XG2piGGSel,
                                                                  {'Gamma0': 'B0 -> rho(770)0 ^gamma gamma',
                                                                   'Gamma1': 'B0 -> rho(770)0 gamma ^gamma',
                                                                   'Res': 'B0 -> ^rho(770)0 gamma gamma'})))

        # B -> hh gamma(->ee)
        b2XG2piCNVLLName = self.name + "2pi_wCNVLL_"
        b2XG2piCNVLLSel = fullTisTosSelection(makeB(b2XG2piCNVLLName,
                                                    [diTrackList, convLLPhotons],
                                                    'B0 -> rho(770)0 gamma',
                                                    **b2XG2piCuts),
                                              config)
        self.registerLine(StrippingLine(b2XG2piCNVLLName+"Line",
                                        prescale=config['B2XG2piCNVLLPrescale'],
                                        selection=b2XG2piCNVLLSel,
                                        EnableFlavourTagging=True,
                                        MDSTFlag=False,
                                        RelatedInfoTools=rel_info(b2XG2piCNVLLSel,
                                                                  {'Gamma': 'B0 -> rho(770)0 ^gamma',
                                                                   'Res': 'B0 -> ^rho(770)0 gamma'})))

        b2XG2piCNVDDName = self.name + "2pi_wCNVDD_"
        b2XG2piCNVDDSel = fullTisTosSelection(makeB(b2XG2piCNVDDName,
                                                    [diTrackList, convDDPhotons],
                                                    'B0 -> rho(770)0 gamma',
                                                    **b2XG2piCuts),
                                              config)
        self.registerLine(StrippingLine(b2XG2piCNVDDName+"Line",
                                        prescale=config['B2XG2piCNVDDPrescale'],
                                        selection=b2XG2piCNVDDSel,
                                        EnableFlavourTagging=True,
                                        MDSTFlag=False,
                                        RelatedInfoTools=rel_info(b2XG2piCNVDDSel,
                                                                  {'Gamma': 'B0 -> rho(770)0 ^gamma',
                                                                   'Res': 'B0 -> ^rho(770)0 gamma'})))

        # B -> h Ks0 gamma
        b2XGpiKsName = self.name + "pi_Ks0_"
        b2XGpiKsSel = fullTisTosSelection(makeB(b2XGpiKsName,
                                                [diTrackList_wKS0, photons],
                                                '[B+ -> rho(770)+ gamma]cc',
                                                **b2XG2piCuts),
                                          config)
        self.registerLine(StrippingLine(b2XGpiKsName+"Line",
                                        prescale=config['B2XGpiKsPrescale'],
                                        selection=b2XGpiKsSel,
                                        EnableFlavourTagging=True,
                                        MDSTFlag=False,
                                        RelatedInfoTools=rel_info(b2XGpiKsSel,
                                                                  {'Gamma': '[B+ -> rho(770)+ ^gamma]CC',
                                                                   'Res': '[B+ -> ^rho(770)+ gamma]CC'})))

        # B -> h Lambda0 gamma
        b2XG2piCuts['MinMassCut'] = config['B2XGBMinMpi']
        b2XGpiLambdaName = self.name + "pi_Lambda0_"
        b2XGpiLambdaSel = fullTisTosSelection(makeB(b2XGpiLambdaName,
                                                    [diTrackList_wLambda, photons],
                                                    '[B+ -> rho(770)+ gamma]cc',
                                                    **b2XG2piCuts),
                                              config)
        self.registerLine(StrippingLine(b2XGpiLambdaName+"Line",
                                        prescale=config['B2XGLambdapiPrescale'],
                                        selection=b2XGpiLambdaSel,
                                        EnableFlavourTagging=True,
                                        MDSTFlag=False,
                                        RelatedInfoTools=rel_info(b2XGpiLambdaSel,
                                                                  {'Gamma': '[B+ -> rho(770)+ ^gamma]CC',
                                                                   'Res': '[B+ -> ^rho(770)+ gamma]CC'})))


        # B -> hhh gamma
        b2XG3piCuts = dict(MinPTCut=config['B2XGBMinPT'],
                           MinMassCut=config['B2XGBMinM3pi'],
                           MaxMassCut=config['B2XGBMaxM'],
                           VtxChi2DOFCut=config['B2XGBVtxChi2DOF'],
                           MinSumPTCut=config['B2XGBSumPtMin'],
                           MinBPVDIRACut=config['B2XGBMinBPVDIRA'],
                           VtxMaxIPChi2Cut=config['B2XGBVtxMaxIPChi2'],
                           MinBPVFDCHI2Cut=config['B2XGBMinBPVFDCHI2'])
        b2XG3piName = self.name + "3pi_"
        b2XG3piSel = fullTisTosSelection(makeB(b2XG3piName,
                                               [triTrackList, photons],
                                               '[B+ -> K_1(1270)+ gamma]cc',
                                               **b2XG3piCuts),
                                         config)
        self.registerLine(StrippingLine(b2XG3piName+"Line",
                                        prescale=config['B2XG3piPrescale'],
                                        selection=b2XG3piSel,
                                        EnableFlavourTagging=True,
                                        MDSTFlag=False,
                                        RelatedInfoTools=rel_info(b2XG3piSel,
                                                                  {'Gamma': '[B+ -> K_1(1270)+ ^gamma]CC',
                                                                   'Res': '[B+ -> ^K_1(1270)+ gamma]CC'})))

        # B -> hhh gamma gamma
        b2XG3piGGName = self.name + "3pi_gammagamma"
        b2XG3piGGSel = fullTisTosSelection(makeB(b2XG3piGGName,
                                                 [triTrackList, photons],
                                                 '[B+ -> K_1(1270)+ gamma gamma]cc',
                                                 **b2XG3piCuts),
                                           config)
        self.registerLine(StrippingLine(b2XG3piGGName+"Line",
                                        prescale=config['B2XG3piGGPrescale'],
                                        selection=b2XG3piGGSel,
                                        EnableFlavourTagging=True,
                                        MDSTFlag=False,
                                        RelatedInfoTools=rel_info(b2XG3piGGSel,
                                                                  {'Gamma0': '[B+ -> K_1(1270)+ ^gamma gamma]CC',
                                                                   'Gamma1': '[B+ -> K_1(1270)+ gamma ^gamma]CC',
                                                                   'Res': '[B+ -> ^K_1(1270)+ gamma gamma]CC'})))

        # B -> hhh gamma(->ee)
        b2XG3piCNVLLName = self.name + "3pi_wCNVLL_"
        b2XG3piCNVLLSel = fullTisTosSelection(makeB(b2XG3piCNVLLName,
                                                    [triTrackList, convLLPhotons],
                                                    '[B+ -> K_1(1270)+ gamma]cc',
                                                    **b2XG3piCuts),
                                              config)
        self.registerLine(StrippingLine(b2XG3piCNVLLName+"Line",
                                        prescale=config['B2XG3piCNVLLPrescale'],
                                        selection=b2XG3piCNVLLSel,
                                        EnableFlavourTagging=True,
                                        MDSTFlag=False,
                                        RelatedInfoTools=rel_info(b2XG3piCNVLLSel,
                                                                  {'Gamma': '[B+ -> K_1(1270)+ ^gamma]CC',
                                                                   'Res': '[B+ -> ^K_1(1270)+ gamma]CC'})))

        b2XG3piCNVDDName = self.name + "3pi_wCNVDD_"
        b2XG3piCNVDDSel = fullTisTosSelection(makeB(b2XG3piCNVDDName,
                                                    [triTrackList, convDDPhotons],
                                                    '[B+ -> K_1(1270)+ gamma]cc',
                                                    **b2XG3piCuts),
                                              config)
        self.registerLine(StrippingLine(b2XG3piCNVDDName+"Line",
                                        prescale=config['B2XG3piCNVDDPrescale'],
                                        selection=b2XG3piCNVDDSel,
                                        EnableFlavourTagging=True,
                                        MDSTFlag=False,
                                        RelatedInfoTools=rel_info(b2XG3piCNVDDSel,
                                                                  {'Gamma': '[B+ -> K_1(1270)+ ^gamma]CC',
                                                                   'Res': '[B+ -> ^K_1(1270)+ gamma]CC'})))

        # B -> pipi KS0 gamma
        b2XG2piKsName = self.name + "2pi_Ks0_"
        b2XG2piKsSel = fullTisTosSelection(makeB(b2XG2piKsName,
                                                 [triTrackList_wKS0, photons],
                                                 'B0 -> K_1(1270)0 gamma',
                                                 **b2XG3piCuts),
                                           config)
        self.registerLine(StrippingLine(b2XG2piKsName+"Line",
                                        prescale=config['B2XG2piKsPrescale'],
                                        selection=b2XG2piKsSel,
                                        EnableFlavourTagging=True,
                                        MDSTFlag=False,
                                        RelatedInfoTools=rel_info(b2XG2piKsSel,
                                                                  {'Gamma': 'B0 -> K_1(1270)0 ^gamma',
                                                                   'Res': 'B0 -> ^K_1(1270)0 gamma'})))

        # B -> pipipi0 gamma
        b2XG2pipi0MName = self.name + "2pi_pi0M_"
        b2XG2pipi0MSel = fullTisTosSelection(makeB(b2XG2pipi0MName,
                                                   [triTrackList_wpi0M, photons],
                                                   'B0 -> K_1(1270)0 gamma',
                                                   **b2XG3piCuts),
                                             config)
        self.registerLine(StrippingLine(b2XG2pipi0MName+"Line",
                                        prescale=config['B2XG2pipi0MPrescale'],
                                        selection=b2XG2pipi0MSel,
                                        EnableFlavourTagging=True,
                                        MDSTFlag=False,
                                        RelatedInfoTools=rel_info(b2XG2pipi0MSel,
                                                                  {'Gamma': 'B0 -> K_1(1270)0 ^gamma',
                                                                   'Res': 'B0 -> ^K_1(1270)0 gamma'})))

        # B -> omega(->pipipi0) gamma
        b2PhiOmega2pipipi0MName = self.name + "phiOmega_2pipi0M_"
        b2PhiOmega2pipipi0MSel = fullTisTosSelection(makeB(b2PhiOmega2pipipi0MName,
                                                           [triTrackList_wpi0M_phiomega, photons],
                                                           'B0 -> K_1(1270)0 gamma',
                                                           **b2XG3piCuts),
                                                     config)
        self.registerLine(StrippingLine(b2PhiOmega2pipipi0MName+"Line",
                                        prescale=config['B2PhiOmega2pipipi0MPrescale'],
                                        selection=b2PhiOmega2pipipi0MSel,
                                        EnableFlavourTagging=True,
                                        MDSTFlag=False,
                                        RelatedInfoTools=rel_info(b2PhiOmega2pipipi0MSel,
                                                                  {'Gamma': 'B0 -> K_1(1270)0 ^gamma',
                                                                   'Res': 'B0 -> ^K_1(1270)0 gamma'})))

        # B -> pipipi0 gamma
        b2XG2pipi0RName = self.name + "2pi_pi0R_"
        b2XG2pipi0RSel = fullTisTosSelection(makeB(b2XG2pipi0RName,
                                                   [triTrackList_wpi0R, photons],
                                                   'B0 -> K_1(1270)0 gamma',
                                                   **b2XG3piCuts),
                                             config)
        self.registerLine(StrippingLine(b2XG2pipi0RName+"Line",
                                        prescale=config['B2XG2pipi0RPrescale'],
                                        selection=b2XG2pipi0RSel,
                                        EnableFlavourTagging=True,
                                        MDSTFlag=False,
                                        RelatedInfoTools=rel_info(b2XG2pipi0RSel,
                                                                  {'Gamma': 'B0 -> K_1(1270)0 ^gamma',
                                                                   'Res': 'B0 -> ^K_1(1270)0 gamma'})))

        # B -> omega(->pipipi0) gamma
        b2PhiOmega2pipipi0RName = self.name + "phiOmega_2pipi0R_"
        b2PhiOmega2pipipi0RSel = fullTisTosSelection(makeB(b2PhiOmega2pipipi0RName,
                                                           [triTrackList_wpi0R_phiomega, photons],
                                                           'B0 -> K_1(1270)0 gamma',
                                                           **b2XG3piCuts),
                                                     config)
        self.registerLine(StrippingLine(b2PhiOmega2pipipi0RName+"Line",
                                        prescale=config['B2PhiOmega2pipipi0RPrescale'],
                                        selection=b2PhiOmega2pipipi0RSel,
                                        EnableFlavourTagging=True,
                                        MDSTFlag=False,
                                        RelatedInfoTools=rel_info(b2PhiOmega2pipipi0RSel,
                                                                  {'Gamma': 'B0 -> K_1(1270)0 ^gamma',
                                                                   'Res': 'B0 -> ^K_1(1270)0 gamma'})))

        # B -> hhLambda0 gamma
        b2XG3piCuts['MinMassCut'] = config['B2XGBMinM2pi']
        b2XGLambda2piName = self.name + "2pi_Lambda0_"
        b2XGLambda2piSel = fullTisTosSelection(makeB(b2XGLambda2piName,
                                                     [triTrackList_wLambda, photons],
                                                     'B0 -> K_1(1270)0 gamma',
                                                     **b2XG3piCuts),
                                               config)
        self.registerLine(StrippingLine(b2XGLambda2piName+"Line",
                                        prescale=config['B2XGLambdapiPrescale'],
                                        selection=b2XGLambda2piSel,
                                        EnableFlavourTagging=True,
                                        MDSTFlag=False,
                                        RelatedInfoTools=rel_info(b2XGLambda2piSel,
                                                                  {'Gamma': 'B0 -> K_1(1270)0 ^gamma',
                                                                   'Res': 'B0 -> ^K_1(1270)0 gamma'})))


        # B -> hhhh gamma
        b2XG4piCuts = dict(MinPTCut=config['B2XGBMinPT'],
                           MinMassCut=config['B2XGBMinM4pi'],
                           MaxMassCut=config['B2XGBMaxM'],
                           VtxChi2DOFCut=config['B2XGBVtxChi2DOF'],
                           MinSumPTCut=config['B2XGBSumPtMin'],
                           MinBPVDIRACut=config['B2XGBMinBPVDIRA'],
                           VtxMaxIPChi2Cut=config['B2XGBVtxMaxIPChi2'],
                           MinBPVFDCHI2Cut=config['B2XGBMinBPVFDCHI2'])
        b2XG4piName = self.name + "4pi_"
        b2XG4piSel = fullTisTosSelection(makeB(b2XG4piName,
                                               [fourTrackList, photons],
                                               'B0 -> a_1(1260)0 gamma',
                                               **b2XG4piCuts),
                                         config)
        self.registerLine(StrippingLine(b2XG4piName+"Line",
                                        prescale=config['B2XG4piPrescale'],
                                        selection=b2XG4piSel,
                                        EnableFlavourTagging=True,
                                        MDSTFlag=False,
                                        RelatedInfoTools=rel_info(b2XG4piSel,
                                                                  {'Gamma': 'B0 -> a_1(1260)0 ^gamma',
                                                                   'Res': 'B0 -> ^a_1(1260)0 gamma'})))

        # B -> hhhKS0 gamma
        b2XG3piKsName = self.name + "3pi_Ks0_"
        b2XG3piKsSel = fullTisTosSelection(makeB(b2XG3piKsName,
                                                 [fourTrackList_wKS0, photons],
                                                 '[B+ -> a_1(1260)+ gamma]cc',
                                                 **b2XG4piCuts),
                                           config)
        self.registerLine(StrippingLine(b2XG3piKsName+"Line",
                                        prescale=config['B2XG3piKsPrescale'],
                                        selection=b2XG3piKsSel,
                                        EnableFlavourTagging=True,
                                        MDSTFlag=False,
                                        RelatedInfoTools=rel_info(b2XG3piKsSel,
                                                                  {'Gamma': '[B+ -> a_1(1260)+ ^gamma]CC',
                                                                   'Res': '[B+ -> ^a_1(1260)+ gamma]CC'})))

        # B -> hKS0hKS0 gamma
        b2XG2pi2KsName = self.name + "2pi_2Ks_"
        b2XG2pi2KsSel = fullTisTosSelection(makeB(b2XG2pi2KsName,
                                                  [fourTrackList_w2KS0, photons],
                                                  'B0 -> a_1(1260)0 gamma',
                                                  **b2XG4piCuts),
                                            config)
        self.registerLine(StrippingLine(b2XG2pi2KsName+"Line",
                                        prescale=config['B2XG2pi2KsPrescale'],
                                        selection=b2XG2pi2KsSel,
                                        EnableFlavourTagging=True,
                                        MDSTFlag=False,
                                        RelatedInfoTools=rel_info(b2XG2pi2KsSel,
                                                                  {'Gamma': 'B0 -> a_1(1260)0 ^gamma',
                                                                   'Res': 'B0 -> ^a_1(1260)0 gamma'})))

        # B -> hhhpi0 gamma
        b2XG3pipi0MName = self.name + "3pi_pi0M_"
        b2XG3pipi0MSel = fullTisTosSelection(makeB(b2XG3pipi0MName,
                                                   [fourTrackList_wpi0M, photons],
                                                   '[B+ -> a_1(1260)+ gamma]cc',
                                                   **b2XG4piCuts),
                                             config)
        self.registerLine(StrippingLine(b2XG3pipi0MName+"Line",
                                        prescale=config['B2XG3pipi0MPrescale'],
                                        selection=b2XG3pipi0MSel,
                                        EnableFlavourTagging=True,
                                        MDSTFlag=False,
                                        RelatedInfoTools=rel_info(b2XG3pipi0MSel,
                                                                  {'Gamma': '[B+ -> a_1(1260)+ ^gamma]CC',
                                                                   'Res': '[B+ -> ^a_1(1260)+ gamma]CC'})))

        b2XG3pipi0RName = self.name + "3pi_pi0R_"
        b2XG3pipi0RSel = fullTisTosSelection(makeB(b2XG3pipi0RName,
                                                   [fourTrackList_wpi0R, photons],
                                                   '[B+ -> a_1(1260)+ gamma]cc',
                                                   **b2XG4piCuts),
                                             config)
        self.registerLine(StrippingLine(b2XG3pipi0RName+"Line",
                                        prescale=config['B2XG3pipi0RPrescale'],
                                        selection=b2XG3pipi0RSel,
                                        EnableFlavourTagging=True,
                                        MDSTFlag=False,
                                        RelatedInfoTools=rel_info(b2XG3pipi0RSel,
                                                                  {'Gamma': '[B+ -> a_1(1260)+ ^gamma]CC',
                                                                   'Res': '[B+ -> ^a_1(1260)+ gamma]CC'})))

        # B -> hhhLambda0 gamma
        b2XG3piCuts['MinMassCut'] = config['B2XGBMinM3pi'] # We put the 3pi because the lambda is not going to have PID change
        b2XGLambda3piName = self.name + "3pi_Lambda0_"
        b2XGLambda3piSel = fullTisTosSelection(makeB(b2XGLambda3piName,
                                                     [fourTrackList_wLambda, photons],
                                                     '[B+ -> a_1(1260)+ gamma]cc',
                                                     **b2XG3piCuts),
                                               config)
        self.registerLine(StrippingLine(b2XGLambda3piName+"Line",
                                        prescale=config['B2XGLambda3piPrescale'],
                                        selection=b2XGLambda3piSel,
                                        EnableFlavourTagging=True,
                                        MDSTFlag=False,
                                        RelatedInfoTools=rel_info(b2XGLambda3piSel,
                                                                  {'Gamma': '[B+ -> a_1(1260)+ ^gamma]CC',
                                                                   'Res': '[B+ -> ^a_1(1260)+ gamma]CC'})))


# Hadronic system makers
def makeDiTrack(name,
                inputs,
                decayDescriptor,
                **cuts):
    """Di-Particle maker."""
    # A quick check
    if not isinstance(inputs, (list, tuple)):
        inputs = [inputs]
    # Prepare selection
    preambulo = ["AMMassMin = 0.75*%(MinMassCut)s" % cuts,
                 "AMMassMax = 1.5*%(MaxMassCut)s" % cuts]
    combCut = ("(ASUM(PT) > %(MinSumPTCut)s) "
               "& in_range(AMMassMin, AM , AMMassMax)")
    motherCut = ("(HASVERTEX) "
                 "& (VFASPF(VCHI2/VDOF) < %(VtxChi2DOFCut)s)"
                 "& (PT > %(MinPTCut)s) "
                 "& in_range(%(MinMassCut)s, M, %(MaxMassCut)s)" % cuts)
    # Build selection
    cpConfig = {'DecayDescriptor': decayDescriptor,
                'Preambulo': preambulo,
                'CombinationCut': combCut % cuts,
                'MotherCut': motherCut % cuts}
    return Selection(name,
                     Algorithm=CombineParticles(**cpConfig),
                     RequiredSelections=inputs)


def makeTriTrack(name,
                 inputs,
                 decayDescriptor,
                 **cuts):
    """Tri-particle maker."""
    # A quick check
    if not isinstance(inputs, (list, tuple)):
        inputs = [inputs]
    # Prepare selection
    preambulo = ["AMMassMin = 0.75*%(MinMassCut)s" % cuts,
                 "AMMassMax = 1.5*%(MaxMassCut)s" % cuts,
                 "ACHI2Max = 5*%(VtxChi2DOFCut)s" % cuts]
    combCut = ("(ASUM(PT) > %(MinSumPTCut)s) "
               "& in_range(AMMassMin, AM, AMMassMax) "
               "& (ACHI2DOCA(1,3) < ACHI2Max) "
               "& (ACHI2DOCA(2,3) < ACHI2Max)" % cuts)
    comb12Cut = ("in_range(AMMassMin, AM, AMMassMax) "
                 "& (ACHI2DOCA(1,2) < ACHI2Max)" % cuts)
    motherCut = ("(HASVERTEX) "
                 "& (VFASPF(VCHI2/VDOF) < %(VtxChi2DOFCut)s) "
                 "& (PT > %(MinPTCut)s) "
                 "& in_range(%(MinMassCut)s, M, %(MaxMassCut)s)" % cuts)
    cpCuts = {'DecayDescriptor': decayDescriptor,
              'Preambulo': preambulo,
              'CombinationCut': combCut,
              'MotherCut': motherCut,
              'Combination12Cut': comb12Cut}
    if 'pi0' in decayDescriptor:
        cpCuts['DaughtersCuts'] = {"pi0" : "PT > %(Pi0PTCut)s" % cuts}
    # Build combine particles
    return Selection(name,
                     Algorithm=Combine3Particles(**cpCuts),
                     RequiredSelections=inputs)


def makeFourTrack(name,
                  inputs,
                  decayDescriptor,
                  **cuts):
    """Four-particle maker."""
    # A quick check
    if not isinstance(inputs, (list, tuple)):
        inputs = [inputs]
    # Prepare selection
    preambulo = ["AMMassMin = 0.75*%(MinMassCut)s" % cuts,
                 "AMMassMax = 1.5*%(MaxMassCut)s" % cuts,
                 "ACHI2Max = 5*%(VtxChi2DOFCut)s" % cuts]
    combCut = ("(ASUM(PT) > %(MinSumPTCut)s) "
               "& in_range(AMMassMin, AM, AMMassMax) "
               "& (ACHI2DOCA(1,4) < ACHI2Max) "
               "& (ACHI2DOCA(2,4) < ACHI2Max) "
               "& (ACHI2DOCA(3,4) < ACHI2Max) " % cuts)
    comb12Cut = ("in_range(AMMassMin, AM, AMMassMax) "
                 " & ( ACHI2DOCA(1,2) < ACHI2Max)" % cuts)
    comb123Cut = ("in_range(AMMassMin, AM, %(MaxMassCut)s) "
                  " & (ACHI2DOCA(1,3) < ACHI2Max) "
                  " & (ACHI2DOCA(2,3) < ACHI2Max)" % cuts)
    motherCut = ("(HASVERTEX) & "
                 "(VFASPF(VCHI2/VDOF) < %(VtxChi2DOFCut)s) "
                 "& (PT > %(MinPTCut)s) "
                 "& in_range(%(MinMassCut)s, M, %(MaxMassCut)s)" % cuts)
    cpCuts = {'DecayDescriptor': decayDescriptor,
              'Preambulo': preambulo,
              'CombinationCut': combCut,
              'MotherCut': motherCut,
              'Combination12Cut': comb12Cut,
              'Combination123Cut': comb123Cut}
    if 'pi0' in decayDescriptor:
        cpCuts['DaughtersCuts'] = {"pi0": "PT > %(Pi0PTCut)s" % cuts}
    # Build selection
    return Selection(name,
                     Algorithm=Combine4Particles(**cpCuts),
                     RequiredSelections=inputs)


def makeB(name,
          inputs,
          decayDescriptor,
          **cuts):
    """B meson maker."""
    # A quick check
    if not isinstance(inputs, (list, tuple)):
        inputs = [inputs]
    # Prepare selection
    preambulo = ["AMMassMin = 0.75*%(MinMassCut)s" % cuts,
                 "AMMassMax = 1.5*%(MaxMassCut)s" % cuts]
    combCut = ("(ASUM(PT) > %(MinSumPTCut)s) "
               "& in_range(AMMassMin, AM , AMMassMax)" % cuts)
    motherCut = ("(HASVERTEX) "
                 "& (VFASPF(VCHI2/VDOF) < %(VtxChi2DOFCut)s) "
                 "& (PT > %(MinPTCut)s) "
                 "& in_range(%(MinMassCut)s, M , %(MaxMassCut)s) "
                 "& (BPVIPCHI2() < %(VtxMaxIPChi2Cut)s) "
                 "& (BPVDIRA > %(MinBPVDIRACut)s)"
                 "& (BPVVDCHI2 > %(MinBPVFDCHI2Cut)s)" % cuts)
    # Build selection
    cpConfig = {'DecayDescriptor': decayDescriptor,
                'Preambulo': preambulo,
                'CombinationCut': combCut,
                'MotherCut': motherCut}
    return Selection(name,
                     Algorithm=CombineParticles(**cpConfig),
                     RequiredSelections=inputs)

# TISTOS
def fullTisTosSelection(sel, config,
                        hlt1ConfigKey='Hlt1TISTOSLinesDict',
                        hlt2ConfigKey='Hlt2TISTOSLinesDict'):
    def tisTosSelection(sel, specs, taggerName):
        """Filters Selection sel to be TOS OR TIS."""

        hltTisTosFilter = TisTosParticleTagger(name+'TISTOSFilter')
        hltTisTosFilter.TisTosSpecs = specs
        hltSel = Selection(sel.name() + taggerName,
                           Algorithm=hltTisTosFilter,
                           RequiredSelections=[sel])
        return hltSel
    return tisTosSelection(tisTosSelection(sel,
                                           config[hlt1ConfigKey],
                                           'Hlt1TISTOS'),
                           config[hlt2ConfigKey],
                           'Hlt2TISTOS')


# Related info
def get_cone_relinfo(angle, head=None, children=None):
    tool = {'Type'     : 'RelInfoConeVariables',
            'ConeAngle': angle,
            'Variables': ['CONEANGLE', 'CONEMULT', 'CONEP', 'CONEPASYM', 'CONEPT', 'CONEPTASYM']}
    # Some shortcuts
    base_location = 'ConeVarsInfo/%%s/%s' % angle
    # Head
    if head:
        tool.update({'Location'    : base_location % 'B',
                     'TopSelection': head})
        if children:
            tool.update({'DaughterLocations': dict([(sel_string, base_location % name)
                                                    for name, sel_string in children.items()])})
            return tool


def get_vtxisol_relinfo(selection):
    return {'Type'        : 'RelInfoVertexIsolation',
            'Variables'   : ['VTXISONUMVTX', 'VTXISODCHI2ONETRACK', 'VTXISODCHI2MASSONETRACK', 'VTXISODCHI2TWOTRACK', 'VTXISODCHI2MASSTWOTRACK'],
            'Location'    : 'VertexIsolInfo',
            'TopSelection': selection}

def get_vtxisol_relinfo_radiative(selection):
    return {'Type'        : 'RelInfoVertexIsolationRadiative',
            'Variables'   : ['NEWVTXISONUMVTX', 'NEWVTXISOTRKRELD0', 'NEWVTXISOTRKDCHI2', 'NEWVTXISODCHI2MASS'],
            'Location'    : 'VertexIsolInfoRadiative',
            'TopSelection': selection}

