###############################################################################
# (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
'''
Stripping selections for single scattered electrons
'''

__author__ = 'Giacomo Graziani'
__date__ = '25/11/2016'
__version__ = '$Revision: 0 $'


__all__ = (
    'HeavyIonSingleElectronConf',
    'default_config'
    )

from GaudiKernel.SystemOfUnits import *

default_config =  {
    'NAME'            : 'HeavyIonSingleElectron',
    'WGs'             : ['IFT'],
    'STREAMS'         : ['MiniBias'],
    'BUILDERTYPE'     : 'HeavyIonSingleElectronConf',
    'CONFIG'          : {
        'odin': ["NoBeam","Beam1","Beam2","BeamCrossing"],
        'Prescale'            :  1.0,
        'Postscale'           :  1.0, 
        'Hlt1Filter'         : "(HLT_PASS('Hlt1BEMicroBiasLowMultVeloDecision'))", 
        'Hlt2Filter'         : None,
        "MaxNvelo"           : 5,
        "MaxNTT"             : 3,
        "MaxNDown"           : 4,
        "MaxNBack"           : 2,
        "MaxNUp"             : 4,
        "MaxNSpd"            : 50,
        "MaxP"               : 25*GeV,
        "MaxPt"              : 180*MeV,
        "MinEta"           : 3.5,
        "MaxEta"           : 7.0
      }
    }

from StrippingConf.StrippingLine import StrippingLine
from StrippingUtils.Utils import LineBuilder
from PhysSelPython.Wrappers import Selection, DataOnDemand
from StandardParticles import StdAllNoPIDsElectrons
from GaudiConfUtils.ConfigurableGenerators import FilterDesktop

class HeavyIonSingleElectronConf(LineBuilder):
    
    __configuration_keys__ = default_config['CONFIG'].keys()

    
    def __init__(self, name, config ): 
        
        LineBuilder.__init__(self, name, config)
        self.name = name 
        self.config = config
        print "inside SingleElectron",config
        odin = "|".join( ["(ODIN_BXTYP == LHCb.ODIN.%s)"%(odin_type,) for odin_type in config['odin'] if odin_type in ["NoBeam","Beam1","Beam2","BeamCrossing"]])

        # filter electrons in the relevant kinematic range
        myFilter = FilterDesktop().configurable("myFilter")
        myFilter.Code = "(PT < %(MaxPt)s) & (P < %(MaxP)s) & ( ETA < %(MaxEta)s) & ( ETA > %(MinEta)s)" % config
        self.makeElectron = Selection("SingleElectronSel",
                                      Algorithm = myFilter,
                                      RequiredSelections = [  StdAllNoPIDsElectrons ])

        # require event to have very little other activity
        myEvfilter  =  "( RECSUMMARY(LHCb.RecSummary.nVeloTracks,-1) < %d )" %self.config['MaxNvelo']
        myEvfilter  += "& ( RECSUMMARY(LHCb.RecSummary.nTTracks,-1) < %d )" %self.config['MaxNTT']
        myEvfilter  += "& ( RECSUMMARY(LHCb.RecSummary.nDownstreamTracks,-1) < %d )" %self.config['MaxNDown']
        myEvfilter  += "& ( RECSUMMARY(LHCb.RecSummary.nBackTracks,-1) < %d )" %self.config['MaxNBack']
        myEvfilter  += "& ( RECSUMMARY(LHCb.RecSummary.nUpstreamTracks,-1) < %d )" %self.config['MaxNUp']
        myEvfilter  += "& ( RECSUMMARY(LHCb.RecSummary.nSPDhits,-1) < %d )" %self.config['MaxNSpd']
        self.EventFilter = {'Code' : myEvfilter,
                            'Preambulo' : ["from LoKiNumbers.functions import RECSUMMARY"]
                            }
        
        self.SingleElectronLine = StrippingLine( 
	      name = 'SingleElectron',
              prescale  = self.config['Prescale'],
              postscale = self.config['Postscale'],                                                                      
              HLT1      = self.config['Hlt1Filter'],
              HLT2      = self.config['Hlt2Filter'],
              checkPV   = False,
	      RequiredRawEvents = [],
              algos     = [ self.makeElectron ],
              ODIN      = odin,
              FILTER= self.EventFilter
              )
        self.registerLine( self.SingleElectronLine )


