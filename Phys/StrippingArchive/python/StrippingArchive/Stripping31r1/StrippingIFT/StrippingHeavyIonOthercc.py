###############################################################################
# (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
'''
Module for selecting dielectron (J/psi, ...), pp (eta_c, ...)
and W->MuNu 
'''

__author__=['Emilie MAURICE']
__date__ = ''
__version__= '$Revision: 1.0 $'


__all__ = (
    'HeavyIonOtherccConf',
    'default_config'
    )

default_config =  {
    'NAME'            : 'HeavyIonOthercc',
    'WGs'             : ['IFT'],
    'STREAMS'         : ['IFT'],
    'BUILDERTYPE'     : 'HeavyIonOtherccConf',
    'CONFIG'          : {
        "odin": ["NoBeam","Beam1","Beam2","BeamCrossing"],
        'CheckPV'    :  False,
        
              
        "VCHI2VDOF_max"    : 16,
        "Track_CHI2"       : 5,
        "DOCA_max"         : 10,

        "PT_min"         : 500,   # MeV
        "AM"               : 2900,   # MeV
        "AMmax"               : 4000,   # MeV
        "Electron_PIDe"    : 1,

        'PrescaleDiElectron'            :  1.0,
        'PostscaleDiElectron'           : 1.0, 

        'Hlt1FilterDiElectron'          : None, #"HLT_PASS_RE('Hlt1SingleElectronNoIP')",
        'Hlt2FilterDiElectron'          : None,

        # etac -> ppbar
        'ProtonPIDppi'     :    20.  , # CombDLL(p-pi)                                                                                                                         
        'ProtonPIDpK'      :    15.  , # CombDLL(p-K)                                                                                                                          
        'CombMinMass'      :  2750.  , # MeV, before Vtx fit                                                                                                                   
        'MaxMass'          :  4000.  , # MeV, after Vtx fit                                                                                                                    

        'PrescaleDiProton'            :  1.0,
        'PostscaleDiProton'           : 1.0, 

        'Hlt1FilterDiProton'          : None,
        'Hlt2FilterDiProton'          : None,
        }
    }

from Gaudi.Configuration import *
from PhysSelPython.Wrappers import Selection, DataOnDemand
from StrippingConf.StrippingLine import StrippingLine
from StrippingUtils.Utils import LineBuilder
from StandardParticles import StdNoPIDsElectrons, StdLooseProtons, StdLooseMuons
from GaudiConfUtils.ConfigurableGenerators import CombineParticles

class HeavyIonOtherccConf(LineBuilder):
    
    __configuration_keys__ = default_config['CONFIG'].keys()

    
    def __init__(self, name, config ): 
        
        LineBuilder.__init__(self, name, config)
        self.name = name 
        self.config = config
        self.inElectrons = StdNoPIDsElectrons
        self.inProtons = StdLooseProtons
        self.inMuons = StdLooseMuons
        
        _monCuts = ( "(VFASPF(VCHI2/VDOF)<{0[VCHI2VDOF_max]})").format(self.config)
        
        _dauCuts = {"e+":("(PT>{0[PT_min]}) & (TRCHI2DOF<{0[Track_CHI2]}) &  (PIDe >  {0[Electron_PIDe]})").format(self.config)}
        
        _comCutsHighMass = ( 
            "(AM > {0[AM]}) & (AM < {0[AMmax]} ) "
            #            "&(ACUTDOCA({0[DOCA_max]}, ''))"
            ).format(self.config)
        
        self.makeJpsi = self.makeDiElectron(
            name = "Jpsi2ee",
            daughterCuts = _dauCuts,
            motherCuts = _monCuts,
            combCuts = _comCutsHighMass
            )
        
        _dauCuts_proton = {"p+":("(TRCHI2DOF<{0[Track_CHI2]}) & ((PIDp-PIDpi) > {0[ProtonPIDppi]}) & ((PIDp-PIDK) > {0[ProtonPIDpK]})").format(self.config)}
        
        _comCuts_pp = (
            "(AM > {0[CombMinMass]}) & (AM < {0[MaxMass]})"
#            "&(ACUTDOCA({0[DOCA_max]}, ''))"
            ).format(self.config)
        self.makeEtac = self.makeDiProton(
            name = "etac2ppbar",
            daughterCuts = _dauCuts_proton,
            motherCuts = _monCuts,
            combCuts = _comCuts_pp
            )

        
        odin = "|".join( ["(ODIN_BXTYP == LHCb.ODIN.%s)"%(odin_type,) for odin_type in self.config['odin'] if odin_type in ["NoBeam","Beam1","Beam2","BeamCrossing"]])
        
        self.JpsiLine = StrippingLine( name+'Jpsi2eeLine',
                                       prescale  = self.config['PrescaleDiElectron'],
                                       postscale  = self.config['PostscaleDiElectron'],                                                                      
                                       checkPV   = self.config['CheckPV'],
                                       HLT1       =self.config['Hlt1FilterDiElectron'],
                                       HLT2       =self.config['Hlt2FilterDiElectron'],
                                       algos     = [ self.makeJpsi],
                                       ODIN      = odin,
                                       RequiredRawEvents = ["Trigger", "Muon", "Calo", "Rich", "Velo", "Tracker", "HC"]
                                       )
        self.registerLine( self.JpsiLine )
        
        self.EtacLine = StrippingLine( name+'Etac2ppLine',
                                       prescale  = self.config['PrescaleDiProton'],
                                       postscale  = self.config['PostscaleDiProton'],                                                                      
                                       checkPV   = self.config['CheckPV'],
                                       HLT1       =self.config['Hlt1FilterDiProton'],
                                       HLT2       =self.config['Hlt2FilterDiProton'],
                                       algos     = [ self.makeEtac],
                                       ODIN      = odin,
                                       RequiredRawEvents = ["Trigger", "Muon", "Calo", "Rich", "Velo", "Tracker", "HC"]
                                       )
        self.registerLine( self.EtacLine )
        

        
        
    def makeDiElectron( self,name, daughterCuts, motherCuts, combCuts) :
        diElectron = CombineParticles(
            #name               = 'Combine{0}'.format(name),
            DecayDescriptors   = ["J/psi(1S) -> e+ e-"],
            CombinationCut     = combCuts,
            MotherCut          = motherCuts,
            DaughtersCuts       = daughterCuts,
            )
        return Selection( name,
                          Algorithm = diElectron,
                          RequiredSelections = [ self.inElectrons] )
        
        

    def makeDiProton( self,name, daughterCuts, motherCuts, combCuts) :
        '''create a selection using a CombineParticles'''
        diProton = CombineParticles(
            #name               = 'Combine{0}'.format(name),
            DecayDescriptors   = ["J/psi(1S) -> p+ p~-"],
            CombinationCut     = combCuts,
            MotherCut          = motherCuts,
            DaughtersCuts       = daughterCuts,
            )
        return Selection( name,
                          Algorithm = diProton,
                          RequiredSelections = [ self.inProtons] )
    
        
