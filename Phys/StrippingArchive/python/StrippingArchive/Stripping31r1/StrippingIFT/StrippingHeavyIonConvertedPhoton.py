###############################################################################
# (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
'''
Module for selecting converted photons
'''

__author__ = ['Tom Boettcher']
__date__ = ''
__version__ = '$Revision: 1.0 $'

__all__ = (
    'HeavyIonConvertedPhotonConf',
    'default_config'
    )

from Gaudi.Configuration import *
from CommonParticles.Utils import *
from GaudiKernel.SystemOfUnits import MeV, GeV, mm
from Configurables import ( DiElectronMaker,
                            ProtoParticleCALOFilter,
                            ParticleTransporter,
                            LoKi__VertexFitter,
                            )
from GaudiConfUtils.ConfigurableGenerators import FilterDesktop
from PhysSelPython.Wrappers import Selection
from StrippingUtils.Utils import LineBuilder
from StrippingConf.StrippingLine import StrippingLine

default_config = {
    'NAME' : 'HeavyIonConvertedPhoton',
    'WGs' : ['IFT'],
    'STREAMS' : ['IFT'],
    'BUILDERTYPE' : 'HeavyIonConvertedPhotonConf',
    'CONFIG' : {
        'odin' : ['NoBeam', 'Beam1', 'Beam2', 'BeamCrossing'],

        #=================
        # Hlt2 passthrough
        'Hlt2Pass' : {
            'Hlt1Filter' : None,
            'Hlt2Filter' : None,
            #'Hlt2Filter' : ("HLT_PASS('Hlt2EWConvPhotonLLHighPtDecision') | "
            #               "HLT_PASS('Hlt2EWConvPhotonDDHighPtDecision')"),
            'Prescale' : 1.0
            },

        #====================
        # Analysis level cuts
        'LL' : {
            'Hlt1Filter' : None,
            # 'Hlt2Filter' : "HLT_PASS('Hlt2EWConvPhotonLLHighPtDecision')",
            'Hlt2Filter' : None,
            'Prescale' : 1.0,
            'TrackType' : 'Long',
            'AddBrem' : True,
            'MinPt' : 5000.0*MeV,
            'MaxPt' : 100000.0*MeV,
            'MaxMass' : 80.0*MeV,
            'MinPIDe' : 0.0,
            'MaxIPChi2' : 200.0,
            'MaxPointing' : 0.03
            },

        'DD' : {
            'Hlt1Filter' : None,
            #'Hlt2Filter' : "HLT_PASS('Hlt2EWConvPhotonDDHighPtDecision')",
            'Hlt2Filter' : None,
            'TrackType' : 'Downstream',
            'Prescale' : 1.0,
            'AddBrem' : True,
            'MinPt' : 5000.0*MeV,
            'MaxPt' : 100000.0*MeV,
            'MaxMass' : 100.0*MeV,
            'MinPIDe' : 0.0,
            'MaxIPChi2' : 200.0,
            'MaxPointing' : 0.03
            },

        'LLNoBrem' : {
            'Hlt1Filter' : None,
            #'Hlt2Filter' : "HLT_PASS('Hlt2EWConvPhotonLLHighPtDecision')",
            'Hlt2Filter' : None,
            'Prescale' : 1.0,
            'TrackType' : 'Long',
            'AddBrem' : False,
            'MinPt' : 5000.0*MeV,
            'MaxPt' : 100000.0*MeV,
            'MaxMass' : 80.0*MeV,
            'MinPIDe' : 0.0,
            'MaxIPChi2' : 200.0,
            'MaxPointing' : 0.03
            },

        'DDNoBrem' : {
            'Hlt1Filter' : None,
            #'Hlt2Filter' : "HLT_PASS('Hlt2EWConvPhotonDDHighPtDecision')",
            'Hlt2Filter' : None,
            'Prescale' : 1.0,
            'TrackType' : 'Downstream',
            'AddBrem' : False,
            'MinPt' : 5000.0*MeV,
            'MaxPt' : 100000.0*MeV,
            'MaxMass' : 100.0*MeV,
            'MinPIDe' : 0.0,
            'MaxIPChi2' : 200.0,
            'MaxPointing' : 0.03
            },

        #=================
        # Low-pt prescaled
        'LowPtLL' : {
            'Hlt1Filter' : None,
            'Hlt2Filter' : None,
            #            'Hlt2Filter' : "HLT_PASS('Hlt2EWConvPhotonLLHighPtDecision')",
            'Prescale' : 0.2,
            'TrackType' : 'Long',
            'AddBrem' : True,
            'MinPt' : 5000.0*MeV,
            'MaxPt' : 10000.0*MeV,
            'MaxMass' : 80.0*MeV,
            'MinPIDe' : -2.0,
            'MaxIPChi2' : 1000.0,
            'MaxPointing' : 1.0
            },

        'LowPtDD' : {
            'Hlt1Filter' : None,
            'Hlt2Filter' : None,
            #            'Hlt2Filter' : "HLT_PASS('Hlt2EWConvPhotonDDHighPtDecision')",
            'Prescale' : 0.2,
            'TrackType' : 'Downstream',
            'AddBrem' : True,
            'MinPt' : 5000.0*MeV,
            'MaxPt' : 10000.0*MeV,
            'MaxMass' : 100.0*MeV,
            'MinPIDe' : -2.0,
            'MaxIPChi2' : 1000.0,
            'MaxPointing' : 1.0
            },

        'LowPtLLNoBrem' : {
            'Hlt1Filter' : None,
            'Hlt2Filter' : None,
            #            'Hlt2Filter' : "HLT_PASS('Hlt2EWConvPhotonLLHighPtDecision')",
            'Prescale' : 0.2,
            'TrackType' : 'Long',
            'AddBrem' : False,
            'MinPt' : 5000.0*MeV,
            'MaxPt' : 10000.0*MeV,
            'MaxMass' : 80.0*MeV,
            'MinPIDe' : -2.0,
            'MaxIPChi2' : 1000.0,
            'MaxPointing' : 1.0
            },

        'LowPtDDNoBrem' : {
            'Hlt1Filter' : None,
            'Hlt2Filter' : None,
            #            'Hlt2Filter' : "HLT_PASS('Hlt2EWConvPhotonDDHighPtDecision')",
            'Prescale' : 0.2,
            'TrackType' : 'Downstream',
            'AddBrem' : True,
            'MinPt' : 5000.0*MeV,
            'MaxPt' : 10000.0*MeV,
            'MaxMass' : 100.0*MeV,
            'MinPIDe' : -2.0,
            'MaxIPChi2' : 1000.0,
            'MaxPointing' : 1.0
            },

        #=========
        # High-pt
        'HighPtLL' : {
            'Hlt1Filter' : None,
            'Hlt2Filter' : None,
            #            'Hlt2Filter' : "HLT_PASS('Hlt2EWConvPhotonLLHighPtDecision')",
            'Prescale' : 0.2,
            'TrackType' : 'Long',
            'AddBrem' : True,
            'MinPt' : 10000.0*MeV,
            'MaxPt' : 100000.0*MeV,
            'MaxMass' : 80.0*MeV,
            'MinPIDe' : -2.0,
            'MaxIPChi2' : 1000.0,
            'MaxPointing' : 1.0
            },

        'HighPtDD' : {
            'Hlt1Filter' : None,
            'Hlt2Filter' : None,
            #            'Hlt2Filter' : "HLT_PASS('Hlt2EWConvPhotonDDHighPtDecision')",
            'Prescale' : 0.2,
            'TrackType' : 'Downstream',
            'AddBrem' : True,
            'MinPt' : 10000.0*MeV,
            'MaxPt' : 100000.0*MeV,
            'MaxMass' : 100.0*MeV,
            'MinPIDe' : -2.0,
            'MaxIPChi2' : 1000.0,
            'MaxPointing' : 1.0
            },

        'HighPtLLNoBrem' : {
            'Hlt1Filter' : None,
            'Hlt2Filter' : None,
            #            'Hlt2Filter' : "HLT_PASS('Hlt2EWConvPhotonLLHighPtDecision')",
            'Prescale' : 0.2,
            'TrackType' : 'Long',
            'AddBrem' : False,
            'MinPt' : 10000.0*MeV,
            'MaxPt' : 100000.0*MeV,
            'MaxMass' : 80.0*MeV,
            'MinPIDe' : -2.0,
            'MaxIPChi2' : 1000.0,
            'MaxPointing' : 1.0
            },

        'HighPtDDNoBrem' : {
            'Hlt1Filter' : None,
            'Hlt2Filter' : None,
            #            'Hlt2Filter' : "HLT_PASS('Hlt2EWConvPhotonDDHighPtDecision')",
            'Prescale' : 0.2,
            'TrackType' : 'Downstream',
            'AddBrem' : True,
            'MinPt' : 10000.0*MeV,
            'MaxPt' : 100000.0*MeV,
            'MaxMass' : 100.0*MeV,
            'MinPIDe' : -2.0,
            'MaxIPChi2' : 1000.0,
            'MaxPointing' : 1.0
            }   
        }
    }

class HeavyIonConvertedPhotonConf(LineBuilder):

    __configuration_keys__ = default_config['CONFIG'].keys()

    def __init__(self, name, config):

        LineBuilder.__init__(self, name, config)
        self.name = name
        self.config = config
        odin = "|".join( ["(ODIN_BXTYP == LHCb.ODIN.%s)"%(odin_type,) for odin_type in config['odin'] if odin_type in ["NoBeam","Beam1","Beam2","BeamCrossing"]])

        #=======================
        # Hlt2 passthrough line
        self.Hlt2PassLine = StrippingLine(
            name = 'ConvPhotonHlt2Pass',
            prescale = self.config['Hlt2Pass']['Prescale'],
            HLT1 = self.config['Hlt2Pass']['Hlt1Filter'],
            HLT2 = self.config['Hlt2Pass']['Hlt2Filter'],
            ODIN = odin,
            RequiredRawEvents = ["Trigger", "Muon", "Calo", "Rich", "Velo", "Tracker", "HC"]
            )
        self.registerLine(self.Hlt2PassLine)

        #================================
        # Lines with analysis level cuts
        # Brem
        self.LLLine = StrippingLine(
            name = 'ConvPhotonLL',
            prescale = self.config['LL']['Prescale'],
            HLT1 = self.config['LL']['Hlt1Filter'],
            HLT2 = self.config['LL']['Hlt2Filter'],
            selection = self.makeConvertedPhoton('SelConvPhotonLL',config['LL']),
            ODIN = odin,
            RequiredRawEvents = ["Trigger", "Muon", "Calo", "Rich", "Velo", "Tracker", "HC"]
            )
        self.registerLine(self.LLLine)
        
        self.DDLine = StrippingLine(
            name = 'ConvPhotonDD',
            prescale = self.config['DD']['Prescale'],
            HLT1 = self.config['DD']['Hlt1Filter'],
            HLT2 = self.config['DD']['Hlt2Filter'],
            selection = self.makeConvertedPhoton('SelConvPhotonDD',config['DD']),
            ODIN = odin,
            RequiredRawEvents = ["Trigger", "Muon", "Calo", "Rich", "Velo", "Tracker", "HC"]
            )
        self.registerLine(self.DDLine)

        # No Brem
        self.LLNoBremLine = StrippingLine(
            name = 'ConvPhotonLLNoBrem',
            prescale = self.config['LLNoBrem']['Prescale'],
            HLT1 = self.config['LLNoBrem']['Hlt1Filter'],
            HLT2 = self.config['LLNoBrem']['Hlt2Filter'],
            selection = self.makeConvertedPhoton('SelConvPhotonLLNoBrem',config['LLNoBrem']),
            ODIN = odin,
            RequiredRawEvents = ["Trigger", "Muon", "Calo", "Rich", "Velo", "Tracker", "HC"]
            )
        self.registerLine(self.LLNoBremLine)

        self.DDNoBremLine = StrippingLine(
            name = 'ConvPhotonDDNoBrem',
            prescale = self.config['DDNoBrem']['Prescale'],
            HLT1 = self.config['DDNoBrem']['Hlt1Filter'],
            HLT2 = self.config['DDNoBrem']['Hlt2Filter'],
            selection = self.makeConvertedPhoton('SelConvPhotonDDNoBrem',config['DDNoBrem']),
            ODIN = odin,
            RequiredRawEvents = ["Trigger", "Muon", "Calo", "Rich", "Velo", "Tracker", "HC"]
            )
        self.registerLine(self.DDNoBremLine)            

        #========================
        # Prescaled Low Pt lines
        # Brem
        self.LowPtLLLine = StrippingLine(
            name = 'ConvPhotonLowPtLL',
            prescale = self.config['LowPtLL']['Prescale'],
            HLT1 = self.config['LowPtLL']['Hlt1Filter'],
            HLT2 = self.config['LowPtLL']['Hlt2Filter'],
            selection = self.makeConvertedPhoton('SelConvPhotonLowPtLL',config['LowPtLL']),
            ODIN = odin,
            RequiredRawEvents = ["Trigger", "Muon", "Calo", "Rich", "Velo", "Tracker", "HC"]
            )
        self.registerLine(self.LowPtLLLine)
        
        self.LowPtDDLine = StrippingLine(
            name = 'ConvPhotonLowPtDD',
            prescale = self.config['LowPtDD']['Prescale'],
            HLT1 = self.config['LowPtDD']['Hlt1Filter'],
            HLT2 = self.config['LowPtDD']['Hlt2Filter'],
            selection = self.makeConvertedPhoton('SelConvPhotonLowPtDD',config['LowPtDD']),
            ODIN = odin,
            RequiredRawEvents = ["Trigger", "Muon", "Calo", "Rich", "Velo", "Tracker", "HC"]
            )
        self.registerLine(self.LowPtDDLine)

        # No Brem
        self.LowPtLLNoBremLine = StrippingLine(
            name = 'ConvPhotonLowPtLLNoBrem',
            prescale = self.config['LowPtLLNoBrem']['Prescale'],
            HLT1 = self.config['LowPtLLNoBrem']['Hlt1Filter'],
            HLT2 = self.config['LowPtLLNoBrem']['Hlt2Filter'],
            selection = self.makeConvertedPhoton('SelConvPhotonLowPtLLNoBrem',config['LowPtLLNoBrem']),
            ODIN = odin,
            RequiredRawEvents = ["Trigger", "Muon", "Calo", "Rich", "Velo", "Tracker", "HC"]
            )
        self.registerLine(self.LowPtLLNoBremLine)

        self.LowPtDDNoBremLine = StrippingLine(
            name = 'ConvPhotonLowPtDDNoBrem',
            prescale = self.config['LowPtDDNoBrem']['Prescale'],
            HLT1 = self.config['LowPtDDNoBrem']['Hlt1Filter'],
            HLT2 = self.config['LowPtDDNoBrem']['Hlt2Filter'],
            selection = self.makeConvertedPhoton('SelConvPhotonLowPtDDNoBrem',config['LowPtDDNoBrem']),
            ODIN = odin,
            RequiredRawEvents = ["Trigger", "Muon", "Calo", "Rich", "Velo", "Tracker", "HC"]
            )
        self.registerLine(self.LowPtDDNoBremLine)            

        #===============
        # High-pt lines
        # Brem
        self.HighPtLLLine = StrippingLine(
            name = 'ConvPhotonHighPtLL',
            prescale = self.config['HighPtLL']['Prescale'],
            HLT1 = self.config['HighPtLL']['Hlt1Filter'],
            HLT2 = self.config['HighPtLL']['Hlt2Filter'],
            selection = self.makeConvertedPhoton('SelConvPhotonHighPtLL',config['HighPtLL']),
            ODIN = odin,
            RequiredRawEvents = ["Trigger", "Muon", "Calo", "Rich", "Velo", "Tracker", "HC"]
            )
        self.registerLine(self.HighPtLLLine)
        
        self.HighPtDDLine = StrippingLine(
            name = 'ConvPhotonHighPtDD',
            prescale = self.config['HighPtDD']['Prescale'],
            HLT1 = self.config['HighPtDD']['Hlt1Filter'],
            HLT2 = self.config['HighPtDD']['Hlt2Filter'],
            selection = self.makeConvertedPhoton('SelConvPhotonHighPtDD',config['HighPtDD']),
            ODIN = odin,
            RequiredRawEvents = ["Trigger", "Muon", "Calo", "Rich", "Velo", "Tracker", "HC"]
            )
        self.registerLine(self.HighPtDDLine)

        # No Brem
        self.HighPtLLNoBremLine = StrippingLine(
            name = 'ConvPhotonHighPtLLNoBrem',
            prescale = self.config['HighPtLLNoBrem']['Prescale'],
            HLT1 = self.config['HighPtLLNoBrem']['Hlt1Filter'],
            HLT2 = self.config['HighPtLLNoBrem']['Hlt2Filter'],
            selection = self.makeConvertedPhoton('SelConvPhotonHighPtLLNoBrem',config['HighPtLLNoBrem']),
            ODIN = odin,
            RequiredRawEvents = ["Trigger", "Muon", "Calo", "Rich", "Velo", "Tracker", "HC"]
            )
        self.registerLine(self.HighPtLLNoBremLine)

        self.HighPtDDNoBremLine = StrippingLine(
            name = 'ConvPhotonHighPtDDNoBrem',
            prescale = self.config['HighPtDDNoBrem']['Prescale'],
            HLT1 = self.config['HighPtDDNoBrem']['Hlt1Filter'],
            HLT2 = self.config['HighPtDDNoBrem']['Hlt2Filter'],
            selection = self.makeConvertedPhoton('SelConvPhotonHighPtDDNoBrem',config['HighPtDDNoBrem']),
            ODIN = odin,
            RequiredRawEvents = ["Trigger", "Muon", "Calo", "Rich", "Velo", "Tracker", "HC"]
            )
        self.registerLine(self.HighPtDDNoBremLine)            

    #==============================
    # Make selections for each line
    def makeConvertedPhoton(self, name, config):
        alg = DiElectronMaker(name+'Maker')
        alg.DecayDescriptor = 'gamma -> e+ e-'
        selector = trackSelector(alg, trackTypes=[config['TrackType']])
        alg.addTool(ProtoParticleCALOFilter, name='Electron')
        alg.Electron.Selection = ["RequiresDet='CALO' CombDLL(e-pi)>'%s'"%config['MinPIDe']]
        alg.DeltaY = 3.0
        alg.DeltaYmax = 200.0*mm
        alg.DiElectronMassMax = config['MaxMass']
        alg.DiElectronPtMin = 200.0*MeV
        alg.AddBrem = config['AddBrem']
        
        # Extra setup for DD pairs
        if config['TrackType'] == 'Downstream':
            alg.ParticleCombiners.update({"" : "LoKi::VertexFitter"})
            alg.addTool(LoKi__VertexFitter)
            alg.LoKi__VertexFitter.addTool(ParticleTransporter, name='Transporter')
            alg.LoKi__VertexFitter.Transporter.TrackExtrapolator = "TrackRungeKuttaExtrapolator"
            alg.LoKi__VertexFitter.DeltaDistance = 100*mm

        code = ("(BPVVDZ > 0)"
                " & (BPVIPCHI2() < %s)"
                " & (BPVDIRA > 1 - %s*PT/P)"
                " & (PT > %s)"
                " & (PT < %s)"
                %(config['MaxIPChi2'], config['MaxPointing'], config['MinPt'], config['MaxPt']))
        print code
        preSel = Selection('Pre'+name, Algorithm=alg)
        sel = Selection(name,
                        Algorithm=FilterDesktop(Code=code),
                        RequiredSelections=[preSel])
        
        return sel
