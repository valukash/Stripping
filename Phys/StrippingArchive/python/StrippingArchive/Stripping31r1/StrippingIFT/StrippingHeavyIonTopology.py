###############################################################################
# (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
'''
Module for selecting specific topology
'''

__author__=['Emilie MAURICE']
__date__ = ''
__version__= '$Revision: 1.0 $'


__all__ = (
    'HeavyIonTopologyConf',
    'default_config'
    )

default_config =  {
    'NAME'            : 'HeavyIonTopology',
    'BUILDERTYPE'     : 'HeavyIonTopologyConf',
    'WGs'             : ['IFT'],
    'STREAMS'         : ['IFT'],
    'CONFIG'          : {
        "odin": ["NoBeam","Beam1","Beam2","BeamCrossing"],

        # For pPb/Pbp
        # "GEC"       : "( recSummaryTrack(LHCb.RecSummary.nLongTracks, TrLONG) < 20)",
        # For PbPb / PbAr
        "GEC"       : "( recSummaryTrack(LHCb.RecSummary.nLongTracks, TrLONG) < 20) & ( recSummary(LHCb.RecSummary.nSPDhits, 'Raw/Spd/Digits') < 2000) & ( recSummaryTrack(LHCb.RecSummary.nVeloTracks, TrVELO) < 11) & ( recSummaryTrack(LHCb.RecSummary.nLongTracks, TrLONG)  > 0) ",
        "GECGamma"       : "( recSummaryTrack(LHCb.RecSummary.nLongTracks, TrLONG) < 20) & ( recSummary(LHCb.RecSummary.nSPDhits, 'Raw/Spd/Digits') < 2000) & ( recSummaryTrack(LHCb.RecSummary.nVeloTracks, TrVELO) < 11)  ",
        
        'CheckPVfalse'    :  False,

        'PrescaleLowMult'            :  1.0,
        'PostscaleLowMult'           : 1.0, 

        'PrescaleGammaLowMult'            :  1.0,
        'PostscaleGammaLowMult'           : 1.0, 

        'PrescaleEE'            :  1.0,
        'PostscaleEE'           : 1.0, 

        'Hlt1FilterLowMult'          : None,
        'Hlt2FilterLowMult'          : None,

        'Hlt1FilterE'          : None, 
        'Hlt2FilterE'          : None,

        'gammaPT' : 200
        }
    }

from Gaudi.Configuration import *
from PhysSelPython.Wrappers import Selection, DataOnDemand
from StrippingConf.StrippingLine import StrippingLine
from StrippingUtils.Utils import LineBuilder
from StandardParticles import StdAllLooseMuons, StdAllLoosePions, StdAllNoPIDsPions, StdLoosePhotons
from GaudiConfUtils.ConfigurableGenerators import CombineParticles
from LoKiPhys.functions import SOURCE
from Configurables               import FilterDesktop, CombineParticles


class HeavyIonTopologyConf(LineBuilder):
    
    __configuration_keys__ = default_config['CONFIG'].keys()

    
    def __init__(self, name, config ): 
        
        LineBuilder.__init__(self, name, config)

        from PhysSelPython.Wrappers import Selection, DataOnDemand

        self.name = name 
        self.config = config


        odin = "|".join( ["(ODIN_BXTYP == LHCb.ODIN.%s)"%(odin_type,) for odin_type in self.config['odin'] if odin_type in ["NoBeam","Beam1","Beam2","BeamCrossing"]])
        
        _filterGEC = {'Code': config['GEC'], 'Preambulo' : ["from LoKiTracks.decorators import *" ,
                                                            "from LoKiCore.functions    import * ",
                                                            "from GaudiKernel.SystemOfUnits import *"]}

        _filterGECGamma = {'Code': config['GECGamma'], 'Preambulo' : ["from LoKiTracks.decorators import *" ,
                                                                      "from LoKiCore.functions    import * ",
                                                                      "from GaudiKernel.SystemOfUnits import *"]}
        
        
        _filterGamma = "(PT>%(gammaPT)s*MeV)" % config
        self._FilterGamma = FilterDesktop(name = "GammaFilterFor"+name, Code = _filterGamma )

        stdPhotons     = DataOnDemand(Location='Phys/StdLoosePhotons/Particles')
        self.stdPhotons_lowmult = Selection( 'PhotonFilter' + name, Algorithm = self._FilterGamma, RequiredSelections = [stdPhotons])
        
        
        
        self.LowActivityLine = StrippingLine( name+'LowActivityLine',
                                              prescale  = self.config['PrescaleLowMult'],
                                              postscale  = self.config['PostscaleLowMult'],
                                              FILTER = _filterGEC,
                                              checkPV   = self.config['CheckPVfalse'],
                                              ODIN      = odin,
                                              RequiredRawEvents = ["Trigger", "Muon", "Calo", "Rich", "Velo", "Tracker", "HC"]
                                              )
        self.registerLine( self.LowActivityLine )


        # Gamma line with low activity
        self.GammaLowActivityLine = StrippingLine( name+'GammaLowActivityLine',
                                                   prescale  = self.config['PrescaleGammaLowMult'],
                                                   postscale  = self.config['PostscaleGammaLowMult'],
                                                   FILTER = _filterGECGamma,
                                                   checkPV   = self.config['CheckPVfalse'],
                                                   ODIN      = odin,
                                                   RequiredRawEvents = ["Trigger", "Muon", "Calo", "Rich", "Velo", "Tracker", "HC"],
                                                   selection = self.stdPhotons_lowmult
                                                   )
        self.registerLine( self.GammaLowActivityLine )
   
        
        
        odinEE = "(ODIN_BXTYP == LHCb.ODIN.NoBeam)"
   
        self.EmptyEmptyLine = StrippingLine( name+'EmptyEmptyLine',
                                             prescale  = self.config['PrescaleEE'],
                                             postscale  = self.config['PostscaleEE'],
                                             checkPV   = self.config['CheckPVfalse'],
                                             ODIN      = odinEE,
                                             RequiredRawEvents = ["Trigger", "Muon", "Calo", "Rich", "Velo", "Tracker", "HC"]
                                             )
        self.registerLine( self.EmptyEmptyLine )


