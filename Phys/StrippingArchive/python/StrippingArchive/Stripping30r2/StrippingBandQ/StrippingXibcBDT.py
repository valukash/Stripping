###############################################################################
# (c) Copyright 2020 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
'''
Module for the Doublely Heavy Flavour Baryons, Xibc,  
Note:
*. CPU-intensive cuts like IPCHI2 are not re-applied
   if being identical to those in the common particles

Include the following lines:
Xibc+  -> p+ K- pi+ 
Xibc+  -> D0 p+ K- pi+
Xibc+  -> D+ p+ K-
Xibc+  -> Lc+ K- pi+
Xibc+  -> Lambda pi+
-----------------------
Xibc0  -> p+ K-
Xibc0  -> D0 p+ K-
Xibc0  -> Lambda phi
Xibc0  -> Lc+ pi-
Xibc0  -> Lc+ K-
Xibc0  -> Lb+ K- pi+
'''

__author__=['Jibo He']
__date__ = '20/11/2016'
__version__= '$Revision: 1.0 $'


__all__ = (
    'XibcBDTConf',
    'default_config'
    )

default_config = {
    'NAME'              :  'XibcBDT',
    'BUILDERTYPE'       :  'XibcBDTConf',
    'CONFIG'    : {
        'PionCuts'      : "(PROBNNpi> 0.2) & (PT>200*MeV) & (TRGHOSTPROB<0.4) & (MIPCHI2DV(PRIMARY)>1.)",
        'KaonCuts'      : "(PROBNNk > 0.1) & (PT>250*MeV) & (TRGHOSTPROB<0.4) & (MIPCHI2DV(PRIMARY)>1.)",
        'ProtonCuts'    : "(PROBNNp> 0.05) & (PT>300*MeV) & (TRGHOSTPROB<0.4) & (MIPCHI2DV(PRIMARY)>1.)",

        'DplusCuts'     : "(ADMASS('D+')<75*MeV) & (VFASPF(VCHI2/VDOF) < 25.) & (PT>1.0*GeV) & (BPVVDCHI2>100)",
        'D0Cuts'        : "(ADMASS('D0')<75*MeV) & (VFASPF(VCHI2/VDOF) < 25.) & (PT>1.0*GeV) & (BPVVDCHI2>64)",
    
        'LcComCuts'     : "(APT>1.0*GeV) & (ADAMASS('Lambda_c+')<50*MeV) & (ADOCACHI2CUT(30, ''))",
        'LcMomCuts'     : "(ADMASS('Lambda_c+')<30*MeV) & (VFASPF(VCHI2/VDOF) < 25.) & (BPVVDCHI2>16)",

        'LambdaDDCuts'   : "(ADMASS('Lambda0') < 30.*MeV) & (BPVVDCHI2>25)",
        'LambdaLLComCuts': "(ADAMASS('Lambda0')<50*MeV) & (ADOCACHI2CUT(30, ''))",
        'LambdaLLCuts'   : "(ADMASS('Lambda0') < 30.*MeV) & (BPVVDCHI2>25) & (VFASPF(VCHI2) < 25.)",

        'PhiCuts'       : """
                          (HASVERTEX) & (VFASPF(VCHI2)<16) & (MM<1.05*GeV) & (MIPCHI2DV(PRIMARY)>2.)
                          & (INTREE( (ID=='K+') & (PT>200*MeV) & (TRGHOSTPROB<0.4) ))
                          & (INTREE( (ID=='K-') & (PT>200*MeV) & (TRGHOSTPROB<0.4) ))
                          """ ,

        'UnPionCuts'    : "(PROBNNpi> 0.2) & (PT>200*MeV) & (TRGHOSTPROB<0.4)",  
        'UnKaonCuts'    : "(PROBNNk > 0.1) & (PT>250*MeV) & (TRGHOSTPROB<0.4)",
        'UnProtonCuts'  : "(PROBNNp> 0.05) & (PT>300*MeV) & (TRGHOSTPROB<0.4)",

        'UnPTPionCuts'    : "(PROBNNpi> 0.2) & (PT>500*MeV) & (TRGHOSTPROB<0.4)",  
        'UnPTKaonCuts'    : "(PROBNNk > 0.1) & (PT>600*MeV) & (TRGHOSTPROB<0.4)",
        'UnPTProtonCuts'  : "(PROBNNp> 0.05) & (PT>750*MeV) & (TRGHOSTPROB<0.4)",    

        'TightPionCuts'   : "(PROBNNpi> 0.2) & (PT>200*MeV) & (TRGHOSTPROB<0.4) & (MIPCHI2DV(PRIMARY)>2.)",
        'TightKaonCuts'   : "(PROBNNk > 0.1) & (PT>250*MeV) & (TRGHOSTPROB<0.4) & (MIPCHI2DV(PRIMARY)>2.)",
        'TightProtonCuts' : "(PROBNNp> 0.05) & (PT>300*MeV) & (TRGHOSTPROB<0.4) & (MIPCHI2DV(PRIMARY)>2.)",

        'HighPTPionCuts'   : "(PROBNNpi> 0.2) & (PT>0.5*GeV) & (TRGHOSTPROB<0.4) & (MIPCHI2DV(PRIMARY)>1.)",
        'HighPTKaonCuts'   : "(PROBNNk > 0.1) & (PT>1.0*GeV) & (TRGHOSTPROB<0.4) & (MIPCHI2DV(PRIMARY)>1.)",
        'HighPTProtonCuts' : "(PROBNNp> 0.05) & (PT>1.0*GeV) & (TRGHOSTPROB<0.4) & (MIPCHI2DV(PRIMARY)>1.)",

        'Pion4LPCuts'   : "(PROBNNpi> 0.2) & (PT>100*MeV) & (TRGHOSTPROB<0.4) & (MIPCHI2DV(PRIMARY)>9.)",
        
        'XibcComCuts'   : "(AM>4.8*GeV)",
        'XibcMomCuts'   : "(M>5.0*GeV) & (VFASPF(VCHI2/VDOF) < 25.) & (BPVIPCHI2()<25) & (BPVDIRA> 0.99)",
        'XibcLPMomCuts' : "(M>5.0*GeV) & (VFASPF(VCHI2/VDOF) < 25.) & (BPVIPCHI2()<25)",

        'LbComCuts'     : "(APT>1.0*GeV) & (ADAMASS('Lambda_b0')<80*MeV) & (ADOCACHI2CUT(30, ''))",
        'LbMomCuts'     : "(ADMASS('Lambda_b0')<60*MeV) & (VFASPF(VCHI2/VDOF) < 25.) & (BPVVDCHI2>64)",

        'XibcP2pKpiMVACut'   :  "0.14",
        'XibcP2pKpiXmlFile'  :  '$TMVAWEIGHTSROOT/data/XibcP2pKpi_BDT_v1r0.xml',

        'XibcP2D0pKpiMVACut'   :  "-0.05",
        'XibcP2D0pKpiXmlFile'  :  '$TMVAWEIGHTSROOT/data/XiccP2D0pKpi_BDT_v1r0.xml', # use Xicc one

        'XibcP2DpKMVACut'   :  "0.02",
        'XibcP2DpKXmlFile'  :  '$TMVAWEIGHTSROOT/data/XiccP2DpK_BDT_v1r0.xml',  #use Xicc one

        'XibcP2LcKpiMVACut'   :  "0.10",
        'XibcP2LcKpiXmlFile'  :  '$TMVAWEIGHTSROOT/data/XibcP2LcKpi_BDT_v1r0.xml',

        'XibcP2LambdaPiMVACut'   :  "0.1",
        'XibcP2LambdaPiXmlFile'  :  '$TMVAWEIGHTSROOT/data/XibcP2LambdaPi_BDT_v1r0.xml',

        'Xibc2pKMVACut'   :  "0.",
        'Xibc2pKXmlFile'  :  '$TMVAWEIGHTSROOT/data/Xibc2pK_BDT_v1r0.xml',

        'Xibc2D0pKMVACut'   :  "0.05",
        'Xibc2D0pKXmlFile'  :  '$TMVAWEIGHTSROOT/data/Xibc2D0pK_BDT_v1r0.xml',

        'Xibc2LambdaPhiMVACut'   :  "-0.1",
        'Xibc2LambdaPhiXmlFile'  :  '$TMVAWEIGHTSROOT/data/Xibc2LambdaPhi_BDT_v1r0.xml',

        'Xibc2LcPiMVACut'   :  "0.",
        'Xibc2LcKMVACut'    :  "-0.01",
        'Xibc2LcPiXmlFile'  :  '$TMVAWEIGHTSROOT/data/Xibc2LcPi_BDT_v1r0.xml',

        'Xibc2LbKpiMVACut'   :  "-0.3",
        'Xibc2LbKpiXmlFile'  :  '$TMVAWEIGHTSROOT/data/Xibc2LbKpi_BDT_v1r0.xml'
        },
    'STREAMS'           : ['Bhadron' ],
    'WGs'               : ['BandQ']
    }


from Gaudi.Configuration import *
from GaudiConfUtils.ConfigurableGenerators import FilterDesktop, CombineParticles 
from GaudiConfUtils.ConfigurableGenerators import DaVinci__N4BodyDecays
from PhysSelPython.Wrappers import Selection, MergedSelection, DataOnDemand
from StrippingConf.StrippingLine import StrippingLine
from StrippingUtils.Utils import LineBuilder

class XibcBDTConf(LineBuilder):
    
    __configuration_keys__ = default_config['CONFIG'].keys()
    
    def __init__(self, name, config ): 
        
        LineBuilder.__init__(self, name, config)
        self.name = name 
        self.config = config
                
        """
        Basic particles, long tracks
        """
        from StandardParticles import StdAllLooseANNPions, StdAllLooseANNKaons, StdAllLooseANNProtons
        
        self.SelPions = self.createSubSel( OutputList = self.name + "SelPions",
                                           InputList =  StdAllLooseANNPions , 
                                           Cuts = config['PionCuts']
                                           )
        
        self.SelKaons = self.createSubSel( OutputList = self.name + "SelKaons",
                                           InputList = StdAllLooseANNKaons, 
                                           Cuts = config['KaonCuts']
                                           )
                                                   
        self.SelProtons = self.createSubSel( OutputList = self.name + "SelProtons",
                                             InputList = StdAllLooseANNProtons, 
                                             Cuts = config['ProtonCuts']
                                             )

        #from StandardParticles import StdAllLooseANNPions
        self.SelUnPions = self.createSubSel( OutputList = self.name + "SelUnPions",
                                               InputList =  StdAllLooseANNPions , 
                                               Cuts = config['UnPionCuts']
                                               )
        
        self.SelUnKaons = self.createSubSel( OutputList = self.name + "SelUnKaons",
                                               InputList =  StdAllLooseANNKaons , 
                                               Cuts = config['UnKaonCuts']
                                               )
        self.SelUnProtons = self.createSubSel( OutputList = self.name + "SelUnProtons",
                                                 InputList =  StdAllLooseANNProtons , 
                                                 Cuts = config['UnProtonCuts']
                                                 )

        """
        Unbiased but with moderate PT
        """
        self.SelUnPTPions = self.createSubSel( OutputList = self.name + "SelUnPTPions",
                                               InputList =  StdAllLooseANNPions , 
                                               Cuts = config['UnPTPionCuts']
                                               )
        
        self.SelUnPTKaons = self.createSubSel( OutputList = self.name + "SelUnPTKaons",
                                               InputList =  StdAllLooseANNKaons , 
                                               Cuts = config['UnPTKaonCuts']
                                               )
        self.SelUnPTProtons = self.createSubSel( OutputList = self.name + "SelUnPTProtons",
                                                 InputList =  StdAllLooseANNProtons , 
                                                 Cuts = config['UnPTProtonCuts']
                                                 )
        
        """
        With IPCHI2
        """
        self.SelTightPions = self.createSubSel( OutputList = self.name + "SelTightPions",
                                                    InputList =  StdAllLooseANNPions , 
                                                    Cuts = config['TightPionCuts']
                                                    )
        
        self.SelTightKaons = self.createSubSel( OutputList = self.name + "SelTightKaons",
                                                    InputList =  StdAllLooseANNKaons , 
                                                    Cuts = config['TightKaonCuts']
                                                    )

        """
        With high PT cut, also IPCHI2
        """
        self.SelHighPTPions = self.createSubSel( OutputList = self.name + "SelHighPTPions",
                                                 InputList =  StdAllLooseANNPions , 
                                                 Cuts = config['HighPTPionCuts']
                                                 )
        
        self.SelHighPTKaons = self.createSubSel( OutputList = self.name + "SelHighPTKaons",
                                                 InputList =  StdAllLooseANNKaons , 
                                                 Cuts = config['HighPTKaonCuts']
                                                 )

        self.SelHighPTProtons = self.createSubSel( OutputList = self.name + "SelHighPTProtons",
                                                 InputList =  StdAllLooseANNProtons , 
                                                 Cuts = config['HighPTProtonCuts']
                                                 )

        """
        Pion for very long lived particle 
        """
        self.SelPions4LP = self.createSubSel( OutputList = self.name + "SelPions4LP",
                                              InputList =  StdAllLooseANNPions , 
                                              Cuts = config['Pion4LPCuts']
                                              )
        
        """
        Dplus->K pi pi 
        """
        self.SelDplus = self.createSubSel( OutputList = self.name + "SelDplus",
                                           InputList =  DataOnDemand(Location = 'Phys/StdLooseDplus2KPiPi/Particles' ), 
                                           Cuts = config['DplusCuts']
                                           )

        """
        D0->K pi
        """
        self.SelD0 = self.createSubSel( OutputList = self.name + "SelD0",
                                        InputList =  DataOnDemand(Location = 'Phys/StdLooseD02KPi/Particles' ), 
                                        Cuts = config['D0Cuts']
                                        )
        
        """
        LambdaC->p K pi
        """
        self.SelLambdaC = self.createCombinationSel( OutputList = self.name + "SelLambdaC",
                                                     DecayDescriptor = "[ Lambda_c+ -> p+  K-  pi+ ]cc",
                                                     DaughterLists = [ self.SelProtons,
                                                                       self.SelKaons,
                                                                       self.SelPions ],                    
                                                     PreVertexCuts  = config['LcComCuts'],
                                                     PostVertexCuts = config['LcMomCuts'] )

        
        """
        Lambda_b -> LambdaC pi
        """
        self.SelLambdaB = self.createCombinationSel( OutputList = self.name + "SelLambdaB",
                                                     DecayDescriptor = "[ Lambda_b0 -> Lambda_c+  pi- ]cc",
                                                     DaughterLists = [ self.SelLambdaC,
                                                                       self.SelHighPTPions ],                    
                                                     PreVertexCuts  = config['LbComCuts'],
                                                     PostVertexCuts = config['LbMomCuts'] )

        
        """
        Lambda, both LL and DD
        """
        from StandardParticles import StdLooseLambdaDD

        self.SelLambdaDD = self.createSubSel( OutputList = self.name + "SelLambdaDD",
                                              InputList = StdLooseLambdaDD,
                                              Cuts = config['LambdaDDCuts'] )

        self.SelLambdaLL = self.createCombinationSel( OutputList = self.name + "SelLambdaLL",
                                                      DecayDescriptor = "[Lambda0 -> p+ pi-]cc",
                                                      DaughterLists = [ self.SelProtons, self.SelPions4LP ],   
                                                      PreVertexCuts  = config['LambdaLLComCuts'],
                                                      PostVertexCuts = config['LambdaLLCuts'] )
        
        self.SelLambda = MergedSelection( self.name + "SelLambda",
                                          RequiredSelections = [ self.SelLambdaDD,
                                                                 self.SelLambdaLL ] )


        """
        Phi 
        """
        from StandardParticles import StdLoosePhi2KK
        
        self.SelPhi = self.createSubSel( OutputList = self.name + "SelPhi",
                                         InputList =  StdLoosePhi2KK, 
                                         Cuts = config['PhiCuts']
                                         )


        #
        # Stripping lines 
        # 
        """
        -------------------------------------------
        Xibc+
        -------------------------------------------

        """

        """
        XibcP2pKpi 
        """
        self.XibcP2pKpiVars = {
            "sqrt(P_IPCHI2_OWNPV)"        : "sqrt(CHILD(MIPCHI2DV(), 1 ))",
            "sqrt(Kaon_IPCHI2_OWNPV)"     : "sqrt(CHILD(MIPCHI2DV(), 2 ))",
            "sqrt(Pion_IPCHI2_OWNPV)"     : "sqrt(CHILD(MIPCHI2DV(), 3 ))",
            "sqrt(P_IPCHI2_OWNPV+Kaon_IPCHI2_OWNPV+Pion_IPCHI2_OWNPV)" : "sqrt( CHILD(MIPCHI2DV(),1) + CHILD(MIPCHI2DV(),2) + CHILD(MIPCHI2DV(),3) )",
            "sqrt(C_IPCHI2_OWNPV)"        : "sqrt(BPVIPCHI2())", 
            "log(P_PT)"                   : "log(CHILD(PT, 1))",
            "log(Kaon_PT)"                : "log(CHILD(PT, 2))",
            "log(Pion_PT)"                : "log(CHILD(PT, 3))",
            "log(P_PT+Kaon_PT+Pion_PT)"   : "log( CHILD(PT, 1) + CHILD(PT, 2) + CHILD(PT, 3) )",
            "log(C_PT)"                   : "log(PT)",
            "sqrt(C_FDCHI2_OWNPV)"        : "sqrt(BPVVDCHI2)", 
            "C_DIRA_OWNPV"                : "BPVDIRA"
            }
        
        self.SelXibcP2pKpi = self.createCombinationSel( OutputList = self.name + "SelXibcP2pKpi",
                                                        DecayDescriptor = "[ Xi_bc+ -> p+ K- pi+ ]cc",
                                                        DaughterLists = [ self.SelHighPTProtons,
                                                                          self.SelHighPTKaons,
                                                                          self.SelPions ],
                                                        PreVertexCuts  = config['XibcComCuts'],
                                                        PostVertexCuts = config['XibcMomCuts'] )
        
        
        self.MvaXibcP2pKpi = self.applyMVA( self.name + "MvaXibcP2pKpi",
                                           SelB        = self.SelXibcP2pKpi,
                                           MVAVars     = self.XibcP2pKpiVars,
                                           MVACutValue = config['XibcP2pKpiMVACut'], 
                                           MVAxmlFile  = config['XibcP2pKpiXmlFile']
                                           )
        
        self.XibcP2pKpiLine = StrippingLine( self.name + '_XibcP2pKpiLine',                                               
                                            algos     = [ self.MvaXibcP2pKpi ],
                                            MDSTFlag  = False
                                            )
        
        self.registerLine( self.XibcP2pKpiLine )


        """
        XibcP2D0pKpi
        """
        # No MC, use Xicc one
        self.XibcP2D0pKpiVars = {
            "sqrt(DK_IPCHI2_OWNPV)"      : "sqrt(CHILD(MIPCHI2DV(), 1, 1))",
            "sqrt(DPi_IPCHI2_OWNPV)"     : "sqrt(CHILD(MIPCHI2DV(), 1, 2))",
            "sqrt(D_IPCHI2_OWNPV)"       : "sqrt(CHILD(MIPCHI2DV(), 1 ))",
            "sqrt(XiccP_IPCHI2_OWNPV)"   : "sqrt(CHILD(MIPCHI2DV(), 2 ))",
            "sqrt(XiccK_IPCHI2_OWNPV)"   : "sqrt(CHILD(MIPCHI2DV(), 3 ))",
            "sqrt(XiccPi_IPCHI2_OWNPV)"  : "sqrt(CHILD(MIPCHI2DV(), 4 ))",
            "sqrt(C_IPCHI2_OWNPV)"       : "sqrt(BPVIPCHI2())" ,
            "log(DK_PT)"                 : "log(CHILD(PT, 1, 1))", 
            "log(DPi_PT)"                : "log(CHILD(PT, 1, 2))", 
            "log(D_PT)"                  : "log(CHILD(PT, 1))",   
            "log(XiccP_PT)"              : "log(CHILD(PT, 2))",
            "log(XiccK_PT)"              : "log(CHILD(PT, 3))",
            "log(XiccPi_PT)"             : "log(CHILD(PT, 4))",
            "log(C_PT)"                  : "log(PT)",
            "sqrt(D_FDCHI2_OWNPV)"       : "sqrt(CHILD(BPVVDCHI2,1))",
            "sqrt(C_FDCHI2_OWNPV)"       : "sqrt(BPVVDCHI2)" ,
            "C_DIRA_OWNPV"               : "BPVDIRA"
            }

        self.SelXibcP2D0pKpi = self.createCombinationSel( OutputList = self.name + "SelXibcP2D0pKpi",
                                                          DecayDescriptor = "[ Xi_bc+ -> D0 p+ K- pi+ ]cc",
                                                          DaughterLists = [ self.SelD0,
                                                                            self.SelUnProtons,
                                                                            self.SelUnKaons,
                                                                            self.SelHighPTPions ],  # w/ IPchi2 cut to speed up
                                                          PreVertexCuts  = config['XibcComCuts'],
                                                          PostVertexCuts = config['XibcMomCuts'] )
              
        self.MvaXibcP2D0pKpi = self.applyMVA( self.name + "MvaXibcP2D0pKpi",
                                           SelB        = self.SelXibcP2D0pKpi,
                                           MVAVars     = self.XibcP2D0pKpiVars,
                                           MVACutValue = config['XibcP2D0pKpiMVACut'], 
                                           MVAxmlFile  = config['XibcP2D0pKpiXmlFile']
                                           )
        
        self.XibcP2D0pKpiLine = StrippingLine( self.name + '_XibcP2D0pKpiLine',                                               
                                                 algos     = [ self.MvaXibcP2D0pKpi ],
                                                 MDSTFlag  = False
                                               )
        
        self.registerLine( self.XibcP2D0pKpiLine )


        """
        XibcP2DpK
        """
        # No MC, use Xicc one
        self.XibcP2DpKVars = {
            "sqrt(DK_IPCHI2_OWNPV)"      : "sqrt(CHILD(MIPCHI2DV(), 1, 1))",
            "sqrt(DPi1_IPCHI2_OWNPV)"    : "sqrt(CHILD(MIPCHI2DV(), 1, 2))",
            "sqrt(DPi2_IPCHI2_OWNPV)"    : "sqrt(CHILD(MIPCHI2DV(), 1, 3))",
            "sqrt(D_IPCHI2_OWNPV)"       : "sqrt(CHILD(MIPCHI2DV(), 1 ))",
            "sqrt(XiccP_IPCHI2_OWNPV)"   : "sqrt(CHILD(MIPCHI2DV(), 2 ))",
            "sqrt(XiccK_IPCHI2_OWNPV)"   : "sqrt(CHILD(MIPCHI2DV(), 3 ))",
            "sqrt(C_IPCHI2_OWNPV)"       : "sqrt(BPVIPCHI2())" ,
            "log(DK_PT)"                 : "log(CHILD(PT, 1, 1))", 
            "log(DPi1_PT)"               : "log(CHILD(PT, 1, 2))", 
            "log(DPi2_PT)"               : "log(CHILD(PT, 1, 3))", 
            "log(D_PT)"                  : "log(CHILD(PT, 1))",   
            "log(XiccP_PT)"              : "log(CHILD(PT, 2))",
            "log(XiccK_PT)"              : "log(CHILD(PT, 3))",
            "log(C_PT)"                  : "log(PT)",
            "sqrt(D_FDCHI2_OWNPV)"       : "sqrt(CHILD(BPVVDCHI2,1))",
            "sqrt(C_FDCHI2_OWNPV)"       : "sqrt(BPVVDCHI2)" ,
            "C_DIRA_OWNPV"               : "BPVDIRA"
            }

        self.SelXibcP2DpK = self.createCombinationSel( OutputList = self.name + "SelXibcP2DpK",
                                                          DecayDescriptor = "[ Xi_bc+ -> D+ p+ K- ]cc",
                                                          DaughterLists = [ self.SelDplus,
                                                                            self.SelUnProtons,
                                                                            self.SelUnKaons ], 
                                                          PreVertexCuts  = config['XibcComCuts'],
                                                          PostVertexCuts = config['XibcMomCuts'] )
        
        self.MvaXibcP2DpK = self.applyMVA( self.name + "MvaXibcP2DpK",
                                           SelB        = self.SelXibcP2DpK,
                                           MVAVars     = self.XibcP2DpKVars,
                                           MVACutValue = config['XibcP2DpKMVACut'], 
                                           MVAxmlFile  = config['XibcP2DpKXmlFile']
                                           )
        
        self.XibcP2DpKLine = StrippingLine( self.name + '_XibcP2DpKLine',                                               
                                                 algos     = [ self.MvaXibcP2DpK ],
                                                 MDSTFlag  = False
                                            )
        
        self.registerLine( self.XibcP2DpKLine )


        """
        XibcP2LcKpi
        """
        self.XibcP2LcKpiVars = {
            "sqrt(LcP_IPCHI2_OWNPV)"     : "sqrt(CHILD(MIPCHI2DV(), 1, 1))",
            "sqrt(LcPi_IPCHI2_OWNPV)"    : "sqrt(CHILD(MIPCHI2DV(), 1, 3))",
            "sqrt(LcK_IPCHI2_OWNPV)"     : "sqrt(CHILD(MIPCHI2DV(), 1, 2))",
            "sqrt(Lc_IPCHI2_OWNPV)"      : "sqrt(CHILD(MIPCHI2DV(), 1 ))",
            "sqrt(XibcPi_IPCHI2_OWNPV)"  : "sqrt(CHILD(MIPCHI2DV(), 3 ))",
            "sqrt(XibcK_IPCHI2_OWNPV)"   : "sqrt(CHILD(MIPCHI2DV(), 2 ))",
            "sqrt(C_IPCHI2_OWNPV)"       : "sqrt(BPVIPCHI2())" ,
            "log(LcP_PT)"                : "log(CHILD(PT, 1, 1))", 
            "log(LcPi_PT)"               : "log(CHILD(PT, 1, 3))", 
            "log(LcK_PT)"                : "log(CHILD(PT, 1, 2))", 
            "log(Lc_PT)"                 : "log(CHILD(PT, 1))",   
            "log(XibcPi_PT)"             : "log(CHILD(PT, 3))",
            "log(XibcK_PT)"              : "log(CHILD(PT, 2))",
            "log(C_PT)"                  : "log(PT)",
            "sqrt(Lc_FDCHI2_OWNPV)"      : "sqrt(CHILD(BPVVDCHI2,1))",
            "sqrt(C_FDCHI2_OWNPV)"       : "sqrt(BPVVDCHI2)" ,
            "C_DIRA_OWNPV"               : "BPVDIRA"
            }

        self.SelXibcP2LcKpi = self.createCombinationSel( OutputList = self.name + "SelXibcP2LcKpi",
                                                         DecayDescriptor = "[ Xi_bc+ -> Lambda_c+ K- pi+ ]cc",
                                                         DaughterLists = [ self.SelLambdaC,
                                                                           self.SelUnKaons,
                                                                           self.SelUnPions ],
                                                         PreVertexCuts  = config['XibcComCuts'],
                                                         PostVertexCuts = config['XibcMomCuts'] )
        
        self.MvaXibcP2LcKpi = self.applyMVA( self.name + "MvaXibcP2LcKpi",
                                           SelB        = self.SelXibcP2LcKpi,
                                           MVAVars     = self.XibcP2LcKpiVars,
                                           MVACutValue = config['XibcP2LcKpiMVACut'], 
                                           MVAxmlFile  = config['XibcP2LcKpiXmlFile']
                                           )
        
        self.XibcP2LcKpiLine = StrippingLine( self.name + '_XibcP2LcKpiLine',                                               
                                                 algos     = [ self.MvaXibcP2LcKpi ],
                                                 MDSTFlag  = False
                                                 )
        
        self.registerLine( self.XibcP2LcKpiLine )



        """
        XibcP2LambdaPi
        """
        self.XibcP2LambdaPiVars = {
            "sqrt(LP_IPCHI2_OWNPV)"    : "sqrt(CHILD(MIPCHI2DV(), 1, 1))",
            "sqrt(LPi_IPCHI2_OWNPV)"   : "sqrt(CHILD(MIPCHI2DV(), 1, 2))",
            "sqrt(Pi_IPCHI2_OWNPV)"    : "sqrt(CHILD(MIPCHI2DV(), 2 ))",
            "log(LP_PT)"               : "log(CHILD(PT, 1, 1))",
            "log(LPi_PT)"              : "log(CHILD(PT, 1, 2))", 
            "log(Lambda_PT)"           : "log(CHILD(PT, 1))",
            "log(Pi_PT)"               : "log(CHILD(PT, 2))",
            "sqrt(Lambda_FDCHI2_OWNPV)": "sqrt(CHILD(BPVVDCHI2,1))",
            "sqrt(C_FDCHI2_OWNPV)"     : "sqrt(BPVVDCHI2)",
            "sqrt(C_IPCHI2_OWNPV)"     : "sqrt(BPVIPCHI2())" 
            }
        
        self.SelXibcP2LambdaPi = self.createCombinationSel( OutputList = self.name + "SelXibcP2LambdaPi",
                                                         DecayDescriptor = "[ Xi_bc+ -> Lambda0 pi+ ]cc",
                                                         DaughterLists = [ self.SelLambda,
                                                                           self.SelUnPions ],
                                                         PreVertexCuts  = config['XibcComCuts'],
                                                         PostVertexCuts = config['XibcLPMomCuts'] )
        
        self.MvaXibcP2LambdaPi = self.applyMVA( self.name + "MvaXibcP2LambdaPi",
                                           SelB        = self.SelXibcP2LambdaPi,
                                           MVAVars     = self.XibcP2LambdaPiVars,
                                           MVACutValue = config['XibcP2LambdaPiMVACut'], 
                                           MVAxmlFile  = config['XibcP2LambdaPiXmlFile']
                                           )
        
        self.XibcP2LambdaPiLine = StrippingLine( self.name + '_XibcP2LambdaPiLine',                                               
                                                 algos     = [ self.MvaXibcP2LambdaPi ],
                                                 MDSTFlag  = False
                                                 )
        
        self.registerLine( self.XibcP2LambdaPiLine )



        """
        -------------------------------------------
        Xibc0
        -------------------------------------------

        """

        """
        Xibc2pK 
        """
        self.Xibc2pKVars = {
            "sqrt(P_IPCHI2_OWNPV)"        : "sqrt(CHILD(MIPCHI2DV(), 1 ))",
            "sqrt(K_IPCHI2_OWNPV)"        : "sqrt(CHILD(MIPCHI2DV(), 2 ))",
            "sqrt(P_IPCHI2_OWNPV+K_IPCHI2_OWNPV)" : "sqrt( CHILD(MIPCHI2DV(),1) + CHILD(MIPCHI2DV(),2) )",
            "sqrt(C_IPCHI2_OWNPV)"        : "sqrt(BPVIPCHI2())", 
            "log(P_PT)"                   : "log(CHILD(PT, 1))",
            "log(K_PT)"                   : "log(CHILD(PT, 2))",
            "log(P_PT+K_PT)"              : "log( CHILD(PT, 1) + CHILD(PT, 2) )",
            "log(C_PT)"                   : "log(PT)",
            "sqrt(C_FDCHI2_OWNPV)"        : "sqrt(BPVVDCHI2)", 
            "C_DIRA_OWNPV"                : "BPVDIRA"
            }
        self.SelXibc2pK = self.createCombinationSel( OutputList = self.name + "SelXibc2pK",
                                                     DecayDescriptor = "[ Xi_bc0 -> p+ K- ]cc",
                                                     DaughterLists = [ self.SelHighPTProtons,
                                                                       self.SelHighPTKaons
                                                                       ],
                                                     PreVertexCuts  = config['XibcComCuts'],
                                                     PostVertexCuts = config['XibcMomCuts'] )
        
        
        self.MvaXibc2pK = self.applyMVA( self.name + "MvaXibc2pK",
                                           SelB        = self.SelXibc2pK,
                                           MVAVars     = self.Xibc2pKVars,
                                           MVACutValue = config['Xibc2pKMVACut'], 
                                           MVAxmlFile  = config['Xibc2pKXmlFile']
                                           )
        
        self.Xibc2pKLine = StrippingLine( self.name + '_Xibc2pKLine',                                               
                                            algos     = [ self.MvaXibc2pK ],
                                            MDSTFlag  = False
                                            )
        
        self.registerLine( self.Xibc2pKLine )


        """
        Xibc2D0pK
        """
        self.Xibc2D0pKVars = {
            "sqrt(DK_IPCHI2_OWNPV)"      : "sqrt(CHILD(MIPCHI2DV(), 1, 1))",
            "sqrt(DPi_IPCHI2_OWNPV)"     : "sqrt(CHILD(MIPCHI2DV(), 1, 2))",
            "sqrt(D_IPCHI2_OWNPV)"       : "sqrt(CHILD(MIPCHI2DV(), 1 ))",
            "sqrt(XibcP_IPCHI2_OWNPV)"   : "sqrt(CHILD(MIPCHI2DV(), 2 ))",
            "sqrt(XibcK_IPCHI2_OWNPV)"   : "sqrt(CHILD(MIPCHI2DV(), 3 ))",
            "sqrt(C_IPCHI2_OWNPV)"       : "sqrt(BPVIPCHI2())" ,
            "log(DK_PT)"                 : "log(CHILD(PT, 1, 1))", 
            "log(DPi_PT)"                : "log(CHILD(PT, 1, 2))", 
            "log(D_PT)"                  : "log(CHILD(PT, 1))",   
            "log(XibcP_PT)"              : "log(CHILD(PT, 2))",
            "log(XibcK_PT)"              : "log(CHILD(PT, 3))",
            "log(C_PT)"                  : "log(PT)",
            "sqrt(D_FDCHI2_OWNPV)"       : "sqrt(CHILD(BPVVDCHI2,1))",
            "sqrt(C_FDCHI2_OWNPV)"       : "sqrt(BPVVDCHI2)" ,
            "C_DIRA_OWNPV"               : "BPVDIRA"
            }

        self.SelXibc2D0pK = self.createCombinationSel( OutputList = self.name + "SelXibc2D0pK",
                                                       DecayDescriptor = "[ Xi_bc0 -> D0 p+ K- ]cc",
                                                       DaughterLists = [ self.SelD0,
                                                                         self.SelUnProtons,
                                                                         self.SelUnKaons ], 
                                                       PreVertexCuts  = config['XibcComCuts'],
                                                       PostVertexCuts = config['XibcMomCuts'] )
        
        self.MvaXibc2D0pK = self.applyMVA( self.name + "MvaXibc2D0pK",
                                           SelB        = self.SelXibc2D0pK,
                                           MVAVars     = self.Xibc2D0pKVars,
                                           MVACutValue = config['Xibc2D0pKMVACut'], 
                                           MVAxmlFile  = config['Xibc2D0pKXmlFile']
                                           )
 
        self.Xibc2D0pKLine = StrippingLine( self.name + '_Xibc2D0pKLine',                                               
                                            algos     = [ self.MvaXibc2D0pK ],
                                            MDSTFlag  = False
                                            )
        
        self.registerLine( self.Xibc2D0pKLine )


        """
        Xibc2LambdaPhi
        """
        self.Xibc2LambdaPhiVars = {
            "sqrt(LP_IPCHI2_OWNPV)"    : "sqrt(CHILD(MIPCHI2DV(), 1, 1))",
            "sqrt(LPi_IPCHI2_OWNPV)"   : "sqrt(CHILD(MIPCHI2DV(), 1, 2))",
            "sqrt(KaonP_IPCHI2_OWNPV)" : "sqrt(CHILD(MIPCHI2DV(), 2, 1))",
            "sqrt(KaonM_IPCHI2_OWNPV)" : "sqrt(CHILD(MIPCHI2DV(), 2, 2))",
            "sqrt(Phi_IPCHI2_OWNPV)"   : "sqrt(CHILD(MIPCHI2DV(), 2 ))",
            "log(LP_PT)"               : "log(CHILD(PT, 1, 1))",
            "log(LPi_PT)"              : "log(CHILD(PT, 1, 2))",
            "log(KaonP_PT)"            : "log(CHILD(PT, 2, 1))",
            "log(KaonM_PT)"            : "log(CHILD(PT, 2, 2))",
            "log(Lambda_PT)"           : "log(CHILD(PT, 1))",
            "log(Phi_PT)"              : "log(CHILD(PT, 2))",
            "sqrt(Lambda_FDCHI2_OWNPV)": "sqrt(CHILD(BPVVDCHI2,1))",
            "sqrt(Phi_FDCHI2_OWNPV)"   : "sqrt(CHILD(BPVVDCHI2,2))",
            "sqrt(C_FDCHI2_OWNPV)"     : "sqrt(BPVVDCHI2)",
            "sqrt(C_IPCHI2_OWNPV)"     : "sqrt(BPVIPCHI2())" 
            }
        self.SelXibc2LambdaPhi = self.createCombinationSel( OutputList = self.name + "SelXibc2LambdaPhi",
                                                            DecayDescriptor = "[ Xi_bc0 -> Lambda0 phi(1020) ]cc",
                                                            DaughterLists = [ self.SelLambda,
                                                                              self.SelPhi ],
                                                            PreVertexCuts  = config['XibcComCuts'],
                                                            PostVertexCuts = config['XibcLPMomCuts'] )
        
        self.MvaXibc2LambdaPhi = self.applyMVA( self.name + "MvaXibc2LambdaPhi",
                                           SelB        = self.SelXibc2LambdaPhi,
                                           MVAVars     = self.Xibc2LambdaPhiVars,
                                           MVACutValue = config['Xibc2LambdaPhiMVACut'], 
                                           MVAxmlFile  = config['Xibc2LambdaPhiXmlFile']
                                           )
        
        self.Xibc2LambdaPhiLine = StrippingLine( self.name + '_Xibc2LambdaPhiLine',                                               
                                                 algos     = [ self.MvaXibc2LambdaPhi ],
                                                 MDSTFlag  = False
                                                 )
        
        self.registerLine( self.Xibc2LambdaPhiLine )


        """
        Xibc2LcPi
        """
        self.Xibc2LcPiVars = {
            "sqrt(LcP_IPCHI2_OWNPV)"     : "sqrt(CHILD(MIPCHI2DV(), 1, 1))",
            "sqrt(LcPi_IPCHI2_OWNPV)"    : "sqrt(CHILD(MIPCHI2DV(), 1, 3))",
            "sqrt(LcK_IPCHI2_OWNPV)"     : "sqrt(CHILD(MIPCHI2DV(), 1, 2))",
            "sqrt(Lc_IPCHI2_OWNPV)"      : "sqrt(CHILD(MIPCHI2DV(), 1 ))",
            "sqrt(XibcPi_IPCHI2_OWNPV)"  : "sqrt(CHILD(MIPCHI2DV(), 2 ))",
            "sqrt(C_IPCHI2_OWNPV)"       : "sqrt(BPVIPCHI2())" ,
            "log(LcP_PT)"                : "log(CHILD(PT, 1, 1))", 
            "log(LcPi_PT)"               : "log(CHILD(PT, 1, 3))", 
            "log(LcK_PT)"                : "log(CHILD(PT, 1, 2))", 
            "log(Lc_PT)"                 : "log(CHILD(PT, 1))",
            "log(XibcPi_PT)"             : "log(CHILD(PT, 2))",
            "log(C_PT)"                  : "log(PT)",
            "sqrt(Lc_FDCHI2_OWNPV)"      : "sqrt(CHILD(BPVVDCHI2,1))",
            "sqrt(C_FDCHI2_OWNPV)"       : "sqrt(BPVVDCHI2)" ,
            "C_DIRA_OWNPV"               : "BPVDIRA"
            }

        self.SelXibc2LcPi = self.createCombinationSel( OutputList = self.name + "SelXibc2LcPi",
                                                         DecayDescriptor = "[ Xi_bc0 -> Lambda_c+ pi- ]cc",
                                                         DaughterLists = [ self.SelLambdaC,
                                                                           self.SelUnPions ],
                                                         PreVertexCuts  = config['XibcComCuts'],
                                                         PostVertexCuts = config['XibcMomCuts'] )

        self.MvaXibc2LcPi = self.applyMVA( self.name + "MvaXibc2LcPi",
                                           SelB        = self.SelXibc2LcPi,
                                           MVAVars     = self.Xibc2LcPiVars,
                                           MVACutValue = config['Xibc2LcPiMVACut'], 
                                           MVAxmlFile  = config['Xibc2LcPiXmlFile']
                                           )
        
        self.Xibc2LcPiLine = StrippingLine( self.name + '_Xibc2LcPiLine',                                               
                                            algos     = [ self.MvaXibc2LcPi ],
                                            MDSTFlag  = False
                                            )
        
        self.registerLine( self.Xibc2LcPiLine )


        """
        Xibc2LcK
        """
        # Use the same as LcPi
        self.SelXibc2LcK = self.createCombinationSel( OutputList = self.name + "SelXibc2LcK",
                                                      DecayDescriptor = "[ Xi_bc0 -> Lambda_c+ K- ]cc",
                                                      DaughterLists = [ self.SelLambdaC,
                                                                        self.SelUnKaons ],
                                                      PreVertexCuts  = config['XibcComCuts'],
                                                      PostVertexCuts = config['XibcMomCuts'] )
              
        self.MvaXibc2LcK = self.applyMVA( self.name + "MvaXibc2LcK",
                                           SelB        = self.SelXibc2LcK,
                                           MVAVars     = self.Xibc2LcPiVars,
                                           MVACutValue = config['Xibc2LcKMVACut'], 
                                           MVAxmlFile  = config['Xibc2LcPiXmlFile']
                                           )
        
        self.Xibc2LcKLine = StrippingLine( self.name + '_Xibc2LcKLine',                                               
                                            algos     = [ self.MvaXibc2LcK ],
                                            MDSTFlag  = False
                                            )
        
        self.registerLine( self.Xibc2LcKLine )
        


        """
        Xibc2LbKpi
        """
        self.Xibc2LbKpiVars = {
            "sqrt(LcP_IPCHI2_OWNPV)"     : "sqrt(CHILD(MIPCHI2DV(), 1, 1, 1))",
            "sqrt(LcPi_IPCHI2_OWNPV)"    : "sqrt(CHILD(MIPCHI2DV(), 1, 1, 3))",
            "sqrt(LcK_IPCHI2_OWNPV)"     : "sqrt(CHILD(MIPCHI2DV(), 1, 1, 2))",
            "sqrt(Lc_IPCHI2_OWNPV)"      : "sqrt(CHILD(MIPCHI2DV(), 1, 1))",
            "sqrt(LbPi_IPCHI2_OWNPV)"    : "sqrt(CHILD(MIPCHI2DV(), 1, 2))",
            "sqrt(Lb_IPCHI2_OWNPV)"      : "sqrt(CHILD(MIPCHI2DV(), 1 ))",
            "sqrt(XibcPi_IPCHI2_OWNPV)"  : "sqrt(CHILD(MIPCHI2DV(), 3 ))",
            "sqrt(XibcK_IPCHI2_OWNPV)"   : "sqrt(CHILD(MIPCHI2DV(), 2 ))",
            "sqrt(C_IPCHI2_OWNPV)"       : "sqrt(BPVIPCHI2())" ,
            "log(LcP_PT)"                : "log(CHILD(PT, 1, 1, 1))", 
            "log(LcPi_PT)"               : "log(CHILD(PT, 1, 1, 3))", 
            "log(LcK_PT)"                : "log(CHILD(PT, 1, 1, 2))",            
            "log(Lc_PT)"                 : "log(CHILD(PT, 1, 1))",
            "log(LbPi_PT)"               : "log(CHILD(PT, 1, 2))",
            "log(Lb_PT)"                 : "log(CHILD(PT, 1))",  
            "log(XibcPi_PT)"             : "log(CHILD(PT, 3))",
            "log(XibcK_PT)"              : "log(CHILD(PT, 2))",
            "log(C_PT)"                  : "log(PT)",
            "sqrt(Lc_FDCHI2_OWNPV)"      : "sqrt(CHILD(BPVVDCHI2, 1, 1))",
            "sqrt(Lb_FDCHI2_OWNPV)"      : "sqrt(CHILD(BPVVDCHI2, 1))",
            "sqrt(C_FDCHI2_OWNPV)"       : "sqrt(BPVVDCHI2)" ,
            "C_DIRA_OWNPV"               : "BPVDIRA"
            }

        self.SelXibc2LbKpi = self.createCombinationSel( OutputList = self.name + "SelXibc2LbKpi",
                                                        DecayDescriptor = "[ Xi_bc0 -> Lambda_b0 K- pi+ ]cc",
                                                        DaughterLists = [ self.SelLambdaB,
                                                                          self.SelUnKaons,
                                                                          self.SelUnPions ],
                                                        PreVertexCuts  = config['XibcComCuts'],
                                                        PostVertexCuts = config['XibcMomCuts'] )
        
        self.MvaXibc2LbKpi = self.applyMVA( self.name + "MvaXibc2LbKpi",
                                           SelB        = self.SelXibc2LbKpi,
                                           MVAVars     = self.Xibc2LbKpiVars,
                                           MVACutValue = config['Xibc2LbKpiMVACut'], 
                                           MVAxmlFile  = config['Xibc2LbKpiXmlFile']
                                           )
    
        self.Xibc2LbKpiLine = StrippingLine( self.name + '_Xibc2LbKpiLine',                                               
                                                 algos     = [ self.MvaXibc2LbKpi ],
                                                 MDSTFlag  = False
                                                 )
        
        self.registerLine( self.Xibc2LbKpiLine )
        



    def createSubSel( self, OutputList, InputList, Cuts ) :
        '''create a selection using a FilterDesktop'''
        filter = FilterDesktop(Code = Cuts)
        return Selection( OutputList,
                          Algorithm = filter,
                          RequiredSelections = [ InputList ] )

    def createCombinationSel( self, OutputList,
                              DecayDescriptor,
                              DaughterLists,
                              DaughterCuts = {} ,
                              PreVertexCuts = "ALL",
                              PostVertexCuts = "ALL",
                              ReFitPVs = True ) :
        '''create a selection using a ParticleCombiner with a single decay descriptor'''
        combiner = CombineParticles( DecayDescriptor = DecayDescriptor,
                                     DaughtersCuts = DaughterCuts,
                                     MotherCut = PostVertexCuts,
                                     CombinationCut = PreVertexCuts,
                                     ReFitPVs = False)
        return Selection ( OutputList,
                           Algorithm = combiner,
                           RequiredSelections = DaughterLists)

    def applyMVA( self, name, 
                  SelB,
                  MVAVars,
                  MVAxmlFile,
                  MVACutValue
                  ):
        from MVADictHelpers import addTMVAclassifierValue
        from Configurables import FilterDesktop as MVAFilterDesktop

        _FilterB = MVAFilterDesktop( name + "Filter",
                                     Code = "VALUE('LoKi::Hybrid::DictValue/" + name + "')>" + MVACutValue  )

        addTMVAclassifierValue( Component = _FilterB,
                                XMLFile   = MVAxmlFile,
                                Variables = MVAVars,
                                ToolName  = name )
        
        return Selection( name,
                          Algorithm =  _FilterB,
                          RequiredSelections = [ SelB ] )
