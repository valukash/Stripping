###############################################################################
# (c) Copyright 2020 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""
Module for construction of Bu->Majorana(KS0) mu+ stripping Selections and StrippingLines.
Provides functions to build KS->DD, KS->LL, Bu selections.
"""

__author__ = ['Mariusz Witek', 'Marcin Chrzaszcz']
__date__ = '31/11/2016'
__version__ = 'V0.9'
__all__ = ('Bu2MajoLepConf',
	   'default_config')

from Gaudi.Configuration import *
from GaudiConfUtils.ConfigurableGenerators import FilterDesktop, CombineParticles
from PhysSelPython.Wrappers import Selection, DataOnDemand, MergedSelection
from StrippingConf.StrippingLine import StrippingLine
from StrippingUtils.Utils import LineBuilder
from GaudiKernel.SystemOfUnits import *

from StandardParticles import StdLooseMuons as Muons
from StandardParticles import StdLoosePions as Pions
from StandardParticles import StdLooseElectrons as Electrons

from StandardParticles import StdNoPIDsDownPions as DownPions
from StandardParticles import StdNoPIDsDownElectrons as DownElectrons

default_config = {
    'NAME' : 'Bu2MajoLep',
    'WGs' : ['RD'],
    'STREAMS' : ['Leptonic'],
    'BUILDERTYPE' : 'Bu2MajoLepConf',
    'CONFIG'      : {'MajoDaug_LTrk_GhostProb'   : 0.5,
                     'MajoDaug_IPChi2min'        : 9.0,
                     'MajoDaug_Pmin'             : 2000.0,
                     'MajoDaug_PTmin'            : 300.0,
                     'Trk_GhostProb'             : 0.5,
                     'Trk_Chi2'                  : 3.0,
                     'Majo_Mhigh'                : 5600.0,
                     'Majo_Pmin'                 : 5000.0,
                     'Majo_PTmin'                : 500.0,
                     'Majo_DD_DocaChi2'          : 25.0,
                     'Majo_LL_DocaChi2'          : 25.0,
                     'Majo_WrongMass'            : 35.0,
                     'Majo_DD_VtxChi2'           : 16.0,
                     'Majo_LL_VtxChi2'           : 16.0,
                     'Majo_DD_FDChi2'            : 25.0,
                     'Majo_LL_FDChi2'            : 25.0,
                     'Majo_FDwrtPV'              : 1.0,
                     'Bach_PTmin'                : 500.0,
                     'Bach_Pmin'                 : 3000.0,
                     'Bach_IPChi2min'            : 16.0,
#                     'BDaug_DD_PTsum'            : 3000.0,
#                     'BDaug_LL_PTsum'            : 3000.0,
                     'B_Mlow'                    : 500.0,
                     'B_Mhigh'                   : 700.0,
                     
                     'B_Pmin'                    : 5000.0,
                     'B_VtxChi2'                 : 10.0,
                     'B_Dira'                    : 0.9999,
                     'B_DD_IPCHI2wrtPV'          : 16.0,
                     'B_LL_IPCHI2wrtPV'          : 16.0,
                     'B_FDwrtPV'                 : 1.0,
                     'B_DD_FDChi2'               : 30.0,
                     'B_LL_FDChi2'               : 30.0,
                     'GEC_MaxTracks'             : 800,
                     'Prescale'                  : 1.0,
                     'Postscale'                 : 1.0,
    "RelatedInfoTools": [
     {
     "Location": "KSTARMUMUVARIABLES",
     "Type": "RelInfoBKstarMuMuBDT",
     "Variables": ['MU_SLL_ISO_1', 'MU_SLL_ISO_2']
     },
    {
    "Location": "ConeIsoInfo", 
    "Type": "RelInfoConeVariables", 
    "Variables": [
    "CONEANGLE", 
    "CONEMULT", 
    "CONEPTASYM", 
    "CONEPT", 
    "CONEP", 
    "CONEPASYM", 
    "CONEDELTAETA", 
    "CONEDELTAPHI"
    ]
    }, 
    {
    "Location": "VtxIsoInfo", 
    "Type": "RelInfoVertexIsolation", 
    "Variables": [
    "VTXISONUMVTX", 
    "VTXISODCHI2ONETRACK", 
    "VTXISODCHI2MASSONETRACK", 
    "VTXISODCHI2TWOTRACK", 
    "VTXISODCHI2MASSTWOTRACK"
    ]
    },
    {
    "Location": "VtxIsoBDTInfo", 
    "Type": "RelInfoVertexIsolationBDT"
    },
     ], 
                     }, 
    }


name="Bu2MajoLep"

class Bu2MajoLepConf(LineBuilder) :
    """
    Builder of Bu->KS mu+ stripping Selection and StrippingLine.
    Constructs B+ -> KS mu+ Selections and StrippingLines from a configuration dictionary.
    """

    __configuration_keys__ = default_config['CONFIG'].keys()

    def __init__(self, name, config) :

        LineBuilder.__init__(self, name, config)

        dd_name = name+'DD'
        ll_name = name+'LL'
        Bu2MajoMuDDName = dd_name.replace("Bu2MajoLep","Bu2MajoMu")
        Bu2MajoMuLLName = ll_name.replace("Bu2MajoLep","Bu2MajoMu")

        GECCode = {'Code' : "(recSummaryTrack(LHCb.RecSummary.nLongTracks, TrLONG) < %s)" % config['GEC_MaxTracks'],
                   'Preambulo' : ["from LoKiTracks.decorators import *"]}

        self.muons = Muons
        self.makeMu( 'Mufor'+name, config )

        self.pions = Pions
        self.makePi( 'Pifor'+name, config )

        self.electrons = Electrons
        self.makeE( 'Efor'+name, config )

        self.downpions = DownPions
        self.makeDownPi( 'DownPifor'+name, config )

        self.downelectrons = DownElectrons
        self.makeDownE( 'DownEfor'+name, config )

        self.selMajo2DD = self.makeMajo2DD( 'DownMajofor'+dd_name, config )
        self.selMajo2LL = self.makeMajo2LL( 'Majofor'+ll_name, config )


        self.Bu2MajoMuDD = self.makeBu2Majo2DD( Bu2MajoMuDDName, config )
        self.Bu2MajoMuLL = self.makeBu2Majo2LL( Bu2MajoMuLLName, config )

        self.Bu2MajoMuDDLine = StrippingLine(Bu2MajoMuDDName+"Line",
                                           prescale = config['Prescale'],
                                           postscale = config['Postscale'],
                                           selection = self.Bu2MajoMuDD,
                                           RelatedInfoTools = config["RelatedInfoTools"],
                                           FILTER = GECCode
                                           )

        self.Bu2MajoMuLLLine = StrippingLine(Bu2MajoMuLLName+"Line",
                                           prescale = config['Prescale'],
                                           postscale = config['Postscale'],
                                           selection =  self.Bu2MajoMuLL,
                                           RelatedInfoTools = config["RelatedInfoTools"],
                                           FILTER = GECCode
                                           )

        self.registerLine(self.Bu2MajoMuDDLine)
        self.registerLine(self.Bu2MajoMuLLLine)


# For LL
    def makeBu2Majo2LL( self, name, config ) :
        """
        Create and store a Bu -> KS(LL) mu+ Selection object.
        Arguments:
        name             : name of the Selection.
        config           : config dictionary
        """

        _massCutLow     = "(AM>(5100-%s)*MeV)" % config['B_Mlow']
        _massCutHigh    = "(AM<(5300+%s)*MeV)" % config['B_Mhigh']

        _combCuts = _massCutLow+'&'+_massCutHigh

        _pCut       = "(P>%s*MeV)"                     % config['B_Pmin']
        _vtxChi2Cut = "(VFASPF(VCHI2)<%s)"             % config['B_VtxChi2']
        _diraCut    = "(BPVDIRA>%s)"                   % config['B_Dira']
        _ipChi2Cut  = "(MIPCHI2DV(PRIMARY)<%s)"        % config['B_LL_IPCHI2wrtPV']
        _fdCut      = "(VFASPF(VMINVDDV(PRIMARY))>%s)" % config['B_FDwrtPV']
        _fdChi2Cut  = "(BPVVDCHI2>%s)"                 % config['B_LL_FDChi2']
        _massCutLow2     = "(M>(5279-%s)*MeV)"               % config['B_Mlow']
        _massCutHigh2    = "(M<(5279+%s)*MeV)"               % config['B_Mhigh']
        


        _motherCuts = _pCut+'&'+_vtxChi2Cut+'&'+_diraCut+'&'+_ipChi2Cut+'&'+_fdCut+'&'+_fdChi2Cut+'&'+_massCutLow2+'&'+_massCutHigh2

        _B = CombineParticles()

        _B.DecayDescriptors = [ "B+ -> mu+ KS0", "B- -> mu- KS0" ]

        _B.DaughtersCuts = { "mu+" : "(P>%s)"% config['Bach_Pmin'] }

        _B.CombinationCut = _combCuts
        _B.MotherCut = _motherCuts

        return Selection (name, Algorithm = _B, RequiredSelections = [ self.selMu, self.selMajo2LL ])


    def makeE( self, name, config ) :
        # define all the cuts
        _bachPtCut      = "(PT>%s*MeV)"                  % config['MajoDaug_PTmin']
        _bachIPChi2Cut  = "(MIPCHI2DV(PRIMARY) > %s)"    % config['MajoDaug_IPChi2min']
        _trkChi2Cut     = "(TRCHI2DOF<%s)"               % config['Trk_Chi2']
        _trkGPCut       = "(TRGHOSTPROB<%s)"             % config['Trk_GhostProb']

        _allCuts = _trkGPCut + '&' + _trkChi2Cut + '&' + _bachPtCut + '&' + _bachIPChi2Cut

        # make the filter
        _filterH = FilterDesktop( Code = _allCuts )

        # make and store the selection object
        self.selE = Selection( name, Algorithm = _filterH, RequiredSelections = [self.electrons] )


    def makePi( self, name, config ) :
        # define all the cuts
        _bachPtCut      = "(PT>%s*MeV)"                  % config['MajoDaug_PTmin']
        _bachIPChi2Cut  = "(MIPCHI2DV(PRIMARY) > %s)"    % config['MajoDaug_IPChi2min']
        _trkChi2Cut     = "(TRCHI2DOF<%s)"               % config['Trk_Chi2']
        _trkGPCut       = "(TRGHOSTPROB<%s)"             % config['Trk_GhostProb']

        _allCuts = _trkGPCut + '&' + _trkChi2Cut + '&' + _bachPtCut + '&' + _bachIPChi2Cut

        # make the filter
        _filterH = FilterDesktop( Code = _allCuts )

        # make and store the selection object
        self.selPi = Selection( name, Algorithm = _filterH, RequiredSelections = [self.pions] )


    def makeMu( self, name, config ) :
        # define all the cuts
        _bachPtCut      = "(PT>%s*MeV)"                  % config['Bach_PTmin']
        _bachIPChi2Cut  = "(MIPCHI2DV(PRIMARY) > %s)"    % config['Bach_IPChi2min']
        _trkChi2Cut     = "(TRCHI2DOF<%s)"               % config['Trk_Chi2']
        _trkGPCut       = "(TRGHOSTPROB<%s)"             % config['Trk_GhostProb']

        _allCuts = _trkGPCut + '&' + _trkChi2Cut + '&' + _bachPtCut + '&' + _bachIPChi2Cut

        # make the filter
        _filterH = FilterDesktop( Code = _allCuts )

        # make and store the selection object
        self.selMu = Selection( name, Algorithm = _filterH, RequiredSelections = [self.muons] )


    def makeMajo2LL( self, name, config ) :
        """
        Create and store a Bu -> KS(LL) h+ Selection object.
        Arguments:
        name             : name of the Selection.
        config           : config dictionary
        """

        _massCutHigh    = "(AM<(%s*MeV))"                     % config['Majo_Mhigh']
        _docaCut        = "(ADOCACHI2CUT(%s, ''))"           % config['Majo_LL_DocaChi2']

        _combCuts = _massCutHigh+'&'+_docaCut

        _pCut       = "(P>%s*MeV)"                     % config['Majo_Pmin']
        _ptCut      = "(PT>%s*MeV)"                    % config['Majo_PTmin']
        _vtxChi2Cut = "(VFASPF(VCHI2)<%s)"             % config['Majo_LL_VtxChi2']
        _fdCut      = "(VFASPF(VMINVDDV(PRIMARY))>%s)" % config['Majo_FDwrtPV']
        _fdChi2Cut  = "(BPVVDCHI2>%s)"                 % config['Majo_LL_FDChi2']
        _vetoKsCut      = "( ADWM( 'KS0' , WM('pi+','pi-') )>%s)"    % config['Majo_WrongMass']
        _vetoLambda1Cut = "( ADWM('Lambda0', WM('p+','pi-') )>%s)"   % config['Majo_WrongMass']
        _vetoLambda2Cut = "( ADWM('Lambda0', WM('pi+','p~-') )>%s)"  % config['Majo_WrongMass']

        _motherCuts = _pCut+'&'+_ptCut+'&'+_vtxChi2Cut+'&'+_fdCut+'&'+_fdChi2Cut+'&'+_vetoKsCut+'&'+_vetoLambda1Cut+'&'+_vetoLambda2Cut

        _Majo = CombineParticles()

        _Majo.DecayDescriptors = [ "KS0 -> pi+ e-", "KS0 -> pi- e+" ]

        _MajoDaug_pCut = "(P>%s)"                         % config['MajoDaug_Pmin']

        _daughtersCuts = _MajoDaug_pCut

        _Majo.DaughtersCuts = { "pi+" : _daughtersCuts, "e-" : _daughtersCuts }
        _Majo.CombinationCut = _combCuts
        _Majo.MotherCut = _motherCuts

        return Selection (name, Algorithm = _Majo, RequiredSelections = [ self.selPi, self.selE ])

# For DD
    def makeBu2Majo2DD( self, name, config ) :
        """
        Create and store a Bu -> KS(DD) mu+ Selection object.
        Arguments:
        name             : name of the Selection.
        config           : config dictionary
        """


        _massCutLow     = "(AM>(5100-%s)*MeV)"               % config['B_Mlow']
        _massCutHigh    = "(AM<(5300+%s)*MeV)"               % config['B_Mhigh']

        _combCuts = _massCutLow+'&'+_massCutHigh

        _pCut       = "(P>%s*MeV)"                     % config['B_Pmin']
        _vtxChi2Cut = "(VFASPF(VCHI2)<%s)"             % config['B_VtxChi2']
        _diraCut    = "(BPVDIRA>%s)"                   % config['B_Dira']
        _ipChi2Cut  = "(MIPCHI2DV(PRIMARY)<%s)"        % config['B_DD_IPCHI2wrtPV']
        _fdCut      = "(VFASPF(VMINVDDV(PRIMARY))>%s)" % config['B_FDwrtPV']
        _fdChi2Cut  = "(BPVVDCHI2>%s)"                 % config['B_DD_FDChi2']
        _massCutLow2     = "(M>(5279-%s)*MeV)"               % config['B_Mlow']
        _massCutHigh2    = "(M<(5279+%s)*MeV)"               % config['B_Mhigh']
        



        _motherCuts = _pCut+'&'+_vtxChi2Cut+'&'+_diraCut+'&'+_ipChi2Cut+'&'+_fdCut+'&'+_fdChi2Cut+'&'+_massCutLow2+'&'+_massCutHigh2
        
        
        _B = CombineParticles()

        _B.DecayDescriptors = [ "B+ -> mu+ KS0", "B- -> mu- KS0" ]

        _B.DaughtersCuts = { "mu+" : "(P>%s)"% config['Bach_Pmin'] }

        _B.CombinationCut = _combCuts
        _B.MotherCut = _motherCuts

        return Selection (name, Algorithm = _B, RequiredSelections = [ self.selMu, self.selMajo2DD ])


    def makeDownE( self, name, config ) :
        # define all the cuts
        _bachPtCut      = "(PT>%s*MeV)"                  % config['MajoDaug_PTmin']
        _bachIPChi2Cut  = "(MIPCHI2DV(PRIMARY) > %s)"    % config['MajoDaug_IPChi2min']
        _trkChi2Cut     = "(TRCHI2DOF<%s)"               % config['Trk_Chi2']
        _trkGPCut       = "(TRGHOSTPROB<%s)"             % config['Trk_GhostProb']

        _allCuts = _trkGPCut + '&' + _trkChi2Cut + '&' + _bachPtCut + '&' + _bachIPChi2Cut

        # make the filter
        _filterH = FilterDesktop( Code = _allCuts )

        # make and store the selection object
        self.selDownE = Selection( name, Algorithm = _filterH, RequiredSelections = [self.downelectrons] )


    def makeDownPi( self, name, config ) :
        # define all the cuts
        _bachPtCut      = "(PT>%s*MeV)"                  % config['MajoDaug_PTmin']
        _bachIPChi2Cut  = "(MIPCHI2DV(PRIMARY) > %s)"    % config['MajoDaug_IPChi2min']
        _trkChi2Cut     = "(TRCHI2DOF<%s)"               % config['Trk_Chi2']
        _trkGPCut       = "(TRGHOSTPROB<%s)"             % config['Trk_GhostProb']

        _allCuts = _trkGPCut + '&' + _trkChi2Cut + '&' + _bachPtCut + '&' + _bachIPChi2Cut

        # make the filter
        _filterH = FilterDesktop( Code = _allCuts )

        # make and store the selection object
        self.selDownPi = Selection( name, Algorithm = _filterH, RequiredSelections = [self.downpions] )


    def makeMajo2DD( self, name, config ) :
        """
        Create and store a Bu -> KS(LL) h+ Selection object.
        Arguments:
        name             : name of the Selection.
        config           : config dictionary
        """

        _massCutHigh    = "(AM<(%s*MeV))"                     % config['Majo_Mhigh']
        _docaCut        = "(ADOCACHI2CUT(%s, ''))"           % config['Majo_DD_DocaChi2']

        _combCuts = _massCutHigh+'&'+_docaCut

        _pCut       = "(P>%s*MeV)"                     % config['Majo_Pmin']
        _ptCut      = "(PT>%s*MeV)"                    % config['Majo_PTmin']
        _vtxChi2Cut = "(VFASPF(VCHI2)<%s)"             % config['Majo_DD_VtxChi2']
        _fdCut      = "(VFASPF(VMINVDDV(PRIMARY))>%s)" % config['Majo_FDwrtPV']
        _fdChi2Cut  = "(BPVVDCHI2>%s)"                 % config['Majo_DD_FDChi2']
        _vetoKsCut      = "( ADWM( 'KS0' , WM('pi+','pi-') )>%s)"    % config['Majo_WrongMass']
        _vetoLambda1Cut = "( ADWM('Lambda0', WM('p+','pi-') )>%s)"   % config['Majo_WrongMass']
        _vetoLambda2Cut = "( ADWM('Lambda0', WM('pi+','p~-') )>%s)"  % config['Majo_WrongMass']

        _motherCuts = _pCut+'&'+_ptCut+'&'+_vtxChi2Cut+'&'+_fdCut+'&'+_fdChi2Cut+'&'+_vetoKsCut+'&'+_vetoLambda1Cut+'&'+_vetoLambda2Cut

        _Majo = CombineParticles()

        _Majo.DecayDescriptors = [ "KS0 -> pi+ e-", "KS0 -> pi- e+" ]

        _MajoDaug_pCut = "(P>%s)"                         % config['MajoDaug_Pmin']

        _daughtersCuts = _MajoDaug_pCut

        _Majo.DaughtersCuts = { "pi+" : _daughtersCuts, "e-" : _daughtersCuts }
        _Majo.CombinationCut = _combCuts
        _Majo.MotherCut = _motherCuts

        return Selection (name, Algorithm = _Majo, RequiredSelections = [ self.selDownPi, self.selDownE ])
