###############################################################################
# (c) Copyright 2020 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
'''
B->charmless quasi 2-body selection
'''

__author__ = ['Fred Blanc', 'Luca Pescatore']
__date__ = '30/11/2016'
__version__ = '$Revision: 3.0 $'

__all__ = ( 'B2CharmlessInclusive',
            'makeLightResList',
            'makeB2Q2B4pi',
            'makeB2Q2B3piAllX0',
            'makeB2Q2BKSX',
            'default_config')

name = 'B2CharmlessInclusive'

default_config = {
    'NAME'        : 'B2CharmlessInclusive',
    'WGs'         : ['BnoC'],
    'BUILDERTYPE' : 'B2CharmlessInclusive',
    'CONFIG'      : { 'Q2BPrescale'     : 1.0,
                      'Q2BTrkGhostProb' : 0.5,

                      # New cuts
                      'Photon_PT_Min'     : 1300.0,
                      'Photon_Res_PT_Min' : 400.0,
                      'Photon_CL_Min'     : 0.2,

                      'Pi0_B_PT_Min'    : 1300., # Pt cut on a pi0 which is direct daughter of the B
                      'Pi0_Res_PT_Min'  : 500.,  # Pt cut on a pi0 with is grandaughter of the B

                       ###KS cuts                    
                      'KS0_LL_MassWindow'       : 40.0,
                      'KS0_DD_MassWindow'       : 60.0,
                      'KS0_LL_FDChi2'           : 80.,
                      'KS0_DD_FDChi2'           : 50.,
                      'KS0_Dira'                : 0.999,
                      'KS0_Child_Trk_Chi2'       : 4.,
                      ###end KS cuts
                      # End new cuts

                      'Q2BTrkMinIPChi2' : 16.,
                      'Q2BTrkMinPT'     : 400.,
                      'Q2BTrkMinHiPT'   : 1000.,
                      'Q2BResMinPT'     : 600.,
                      'Q2BResMinHiPT'   : 1000.,
                      'Q2BResMaxMass'   : 1100.,
                      'Q2BResVtxChi2DOF': 6.,
                      'Q2BBMinPT'       : 1500.,
                      'Q2BBMinM3pi'     : 4400., #4200.,
                      'Q2BBMinM4pi'     : 3900., #3500.,
                      'Q2BBMaxM3pi'     : 6700.,
                      'Q2BBMaxM4pi'     : 5700.,
                      'Q2BBMaxCorrM3pi' : 7000.,
                      'Q2BBMaxCorrM4pi' : 7000.,
                      'Q2BBVtxChi2DOF'  : 6.            
                      },
    'STREAMS'     : ['Bhadron']     ## This stream puts it into mDST
    }


from Gaudi.Configuration import *
from GaudiConfUtils.ConfigurableGenerators import FilterDesktop, CombineParticles
from PhysSelPython.Wrappers import Selection, DataOnDemand, SimpleSelection
from StrippingConf.StrippingLine import StrippingLine
from StrippingUtils.Utils import LineBuilder
from StandardParticles import StdNoPIDsPions, StdLooseAllPhotons, StdLooseMergedPi0, StdLooseResolvedPi0
from StandardParticles import StdLooseKsDD, StdLooseKsLL

class B2CharmlessInclusive(LineBuilder) :
    """
    Builder for B2CharmlessInclusive
    """

    __configuration_keys__ = default_config['CONFIG'].keys()

    def __init__(self, name, config) :
        self.name = name
        LineBuilder.__init__(self, name, config)

        cuts = default_config['CONFIG']

        ## Single particle cuts definition
        trackCuts = "(MIPCHI2DV(PRIMARY) > %(Q2BTrkMinIPChi2)s) & (TRGHOSTPROB < %(Q2BTrkGhostProb)s) & (PT > %(Q2BTrkMinPT)s)" % cuts
        gamma_for_B_Cuts = "(PT > %(Photon_PT_Min)s*MeV) & (CL > %(Photon_CL_Min)s)" % cuts
        gamma_for_Res_Cuts = "(PT > %(Photon_Res_PT_Min)s*MeV) & (CL > %(Photon_CL_Min)s)" % cuts
        pi0_for_B_Cuts = "(PT > %(Pi0_B_PT_Min)s*MeV)" % cuts
        pi0_for_Res_Cuts = "(PT > %(Pi0_Res_PT_Min)s*MeV)" % cuts
        KS_DDCuts="(ADMASS('KS0')<%(KS0_DD_MassWindow)s*MeV) & (BPVVDCHI2>%(KS0_DD_FDChi2)s)" % cuts
        KS_LLCuts="(ADMASS('KS0')<%(KS0_LL_MassWindow)s*MeV) & (BPVVDCHI2>%(KS0_LL_FDChi2)s)" % cuts
        KS_ChildCuts= "(CHILDCUT((TRCHI2DOF<%(KS0_Child_Trk_Chi2)s),1)) & (CHILDCUT((TRCHI2DOF<%(KS0_Child_Trk_Chi2)s),2))"% cuts
        KS_LL_ChildCuts= "(CHILDCUT((TRGHOSTPROB<%(Q2BTrkGhostProb)s),1)) & (CHILDCUT((TRGHOSTPROB<%(Q2BTrkGhostProb)s),2))"% cuts
        
        KS_DDCuts = '&'.join([KS_DDCuts, KS_ChildCuts])
        KS_LLCuts = '&'.join([KS_LLCuts, KS_ChildCuts, KS_LL_ChildCuts])

        self.TrackList = SimpleSelection(
                            'TrackList' + self.name,
                            FilterDesktop,
                            [StdNoPIDsPions],
                            Code = trackCuts
                                       )

        self.Gamma_for_B_List = SimpleSelection( 'Gamma_for_B_List' + self.name,
                                    FilterDesktop,
                                    [StdLooseAllPhotons],
                                    Code = gamma_for_B_Cuts
                                    )

        self.Gamma_for_Res_List = SimpleSelection( 'Gamma_for_Res_List' + self.name,
                                    FilterDesktop,
                                    [StdLooseAllPhotons],
                                    Code = gamma_for_Res_Cuts
                                    )

        self.Pi0_for_B_List = SimpleSelection( 'Pi0_for_B_List' + self.name,
                                    FilterDesktop,
                                    [StdLooseResolvedPi0, StdLooseMergedPi0],
                                    Code = pi0_for_B_Cuts
                                    )

        self.Pi0_for_Res_List = SimpleSelection( 'Pi0_for_Res_List' + self.name,
                                    FilterDesktop,
                                    [StdLooseResolvedPi0, StdLooseMergedPi0],
                                    Code = pi0_for_Res_Cuts
                                    )

        self.TrackList_HiPt = SimpleSelection( 'TrackList_HiPt' + self.name,
                                         FilterDesktop,
                                         [self.TrackList],
                                         Code="(PT > %(Q2BTrkMinHiPT)s)" % config
                                         )

        ## Modified to add neutrals to the "pipi"
        self.LightResList = makeLightResList( name="DiTracksForCharmlessB" + self.name,
                                            trkList=self.TrackList,
                                            gammaList=self.Gamma_for_Res_List,
                                            pi0List=self.Pi0_for_Res_List,
                                            MinPTCut = config['Q2BResMinPT'],
                                            MaxMassCut = config['Q2BResMaxMass'],
                                            VtxChi2DOFCut = config['Q2BResVtxChi2DOF'] )

        self.LightResList_HiPt = SimpleSelection( 'DiTracksHiPtForCharmlessB' + self.name,
                                           FilterDesktop,
                                           [self.LightResList],
                                           Code="(PT > %(Q2BResMinHiPT)s)" % config )
                                           
                                           
        ##MOdified to add KS
        self.KSList_DD=SimpleSelection( 'KSList_DD'+ self.name,
                                         FilterDesktop,
                                         [StdLooseKsDD],
                                         Code=KS_DDCuts
                                       )
        self.KSList_LL=SimpleSelection( 'KSList_LL'+ self.name,
                                         FilterDesktop,
                                         [StdLooseKsLL],
                                         Code=KS_LLCuts
                                       )


        ## 4pi line modified to add neutrals
        B2Q2B4piX0Name = self.name + "4piAllX0Selection"
        self.B2CharmlessQ2B4piX0 = makeB2Q2B4pi( B2Q2B4piX0Name,
                                               diTrkList     = self.LightResList,
                                               MinPTCut      = config['Q2BBMinPT'],
                                               MinMassCut    = config['Q2BBMinM4pi'],
                                               MaxMassCut    = config['Q2BBMaxM4pi'],
                                               MaxCorrMCut   = config['Q2BBMaxCorrM4pi'],
                                               VtxChi2DOFCut = config['Q2BBVtxChi2DOF'] )

        self.Q2B4piX0Line = StrippingLine( B2Q2B4piX0Name+"Line",
                                         prescale = config['Q2BPrescale'],
                                         selection = tisTosSelection(self.B2CharmlessQ2B4piX0),
                                         RelatedInfoTools = getRelInfo4pi()
                                         )

        ## 3pi line modified to add neutrals
        B2Q2B3piX0Name = self.name + "3piAllX0Selection"
        self.B2CharmlessQ2B3piX0 = makeB2Q2B3piAllX0( B2Q2B3piX0Name,
                                               trkList       = self.TrackList,
                                               diTrkList     = self.LightResList,
                                               MinPTCut      = config['Q2BBMinPT'],
                                               MinMassCut    = config['Q2BBMinM3pi'],
                                               MaxMassCut    = config['Q2BBMaxM3pi'],
                                               MaxCorrMCut   = config['Q2BBMaxCorrM3pi'],
                                               VtxChi2DOFCut = config['Q2BBVtxChi2DOF'] )

        self.Q2B3piX0Line = StrippingLine( B2Q2B3piX0Name+"Line",
                                         prescale = config['Q2BPrescale'],
                                         selection = tisTosSelection(self.B2CharmlessQ2B3piX0),
                                         RelatedInfoTools = getRelInfo3pi()
                                         )
                                         
        B2Q2BKSX_LLName = self.name + "KSX_LLSelection"
        self.B2CharmlessQ2KSX_LL=makeB2Q2BKSX(B2Q2BKSX_LLName,
                                                      kSList       = self.KSList_LL,
                                                      diTrkList     = self.LightResList,
                                                      MinPTCut      = config['Q2BBMinPT'],
                                                      MinMassCut    = config['Q2BBMinM3pi'],
                                                      MaxMassCut    = config['Q2BBMaxM3pi'],
                                                      MaxCorrMCut   = config['Q2BBMaxCorrM3pi'],
                                                      VtxChi2DOFCut = config['Q2BBVtxChi2DOF'] )
                                                      
        self.Q2KSX_LL_Line=StrippingLine( B2Q2BKSX_LLName+"Line",
                                         prescale = config['Q2BPrescale'],
                                         selection = tisTosSelection(self.B2CharmlessQ2KSX_LL),
                                         RelatedInfoTools = getRelInfoKSX()
                                         )
                                         
        B2Q2BKSX_DDName = self.name + "KSX_DDSelection"
        self.B2CharmlessQ2KSX_DD=makeB2Q2BKSX(B2Q2BKSX_DDName,
                                                      kSList       = self.KSList_DD,
                                                      diTrkList     = self.LightResList,
                                                      MinPTCut      = config['Q2BBMinPT'],
                                                      MinMassCut    = config['Q2BBMinM3pi'],
                                                      MaxMassCut    = config['Q2BBMaxM3pi'],
                                                      MaxCorrMCut   = config['Q2BBMaxCorrM3pi'],
                                                      VtxChi2DOFCut = config['Q2BBVtxChi2DOF'] )
                                                      
        self.Q2KSX_DD_Line=StrippingLine( B2Q2BKSX_DDName+"Line",
                                         prescale = config['Q2BPrescale'],
                                         selection = tisTosSelection(self.B2CharmlessQ2KSX_DD),
                                         RelatedInfoTools = getRelInfoKSX()
                                         )
                                         
        

        self.registerLine(self.Q2B4piX0Line)
        self.registerLine(self.Q2B3piX0Line)
        self.registerLine(self.Q2KSX_LL_Line)
        self.registerLine(self.Q2KSX_DD_Line)

def makeLightResList( name,
                     trkList,
                     gammaList,
                     pi0List,
                     MinPTCut,
                     MaxMassCut,
                     VtxChi2DOFCut ) :
    """
    Di-track or di-track + neutral selection
    """
    _diTrackPreVertexCuts = "(APT> %(MinPTCut)s) & (AM< %(MaxMassCut)s)" %locals()
    _diTrackPostVertexCuts = "(VFASPF(VCHI2/VDOF) < %(VtxChi2DOFCut)s)" %locals()

    return SimpleSelection(name, CombineParticles,
                           [ trkList, gammaList, pi0List ],
                           DecayDescriptors=["rho(770)0 -> pi+ pi-", "rho(770)0 -> pi+ pi- pi0", "rho(770)0 -> pi+ pi- gamma"],
                           CombinationCut = _diTrackPreVertexCuts,
                           MotherCut = _diTrackPostVertexCuts
                           )

def makeB2Q2B4pi( name,
                  diTrkList,
                  MinPTCut,
                  MinMassCut,
                  MaxMassCut,
                  MaxCorrMCut,
                  VtxChi2DOFCut ) :
    """
    Charmless to 4pi + neutrals selection with missing mass
    """

    _B2Q2B4piPreVertexCuts = "in_range( %(MinMassCut)s ,AM, %(MaxMassCut)s )" %locals()
    _B2Q2B4piPreVertexCuts += " & ( APT > %(MinPTCut)s )" %locals()

    _B2Q2B4piPostVertexCuts = "( BPVCORRM < %(MaxCorrMCut)s )" %locals()
    _B2Q2B4piPostVertexCuts += " & (VFASPF(VCHI2/VDOF) < %(VtxChi2DOFCut)s )" %locals()

    return SimpleSelection(name, CombineParticles, [ diTrkList ]
                           , DecayDescriptor="B0 -> rho(770)0 rho(770)0"
                           , MotherCut = _B2Q2B4piPostVertexCuts
                           , CombinationCut = _B2Q2B4piPreVertexCuts
                           )


def makeB2Q2B3piAllX0( name,
                       trkList,
                       diTrkList,
                       MinPTCut,
                       MinMassCut,
                       MaxMassCut,
                       MaxCorrMCut,
                       VtxChi2DOFCut ) :
    """
    Charmless to 3pi + neutrals selection with missing mass
    """

    _B2Q2B3piPreVertexCuts = "in_range( %(MinMassCut)s ,AM, %(MaxMassCut)s )" %locals()
    _B2Q2B3piPreVertexCuts += " & ( APT > %(MinPTCut)s )" %locals()

    _B2Q2B3piPostVertexCuts = "( BPVCORRM < %(MaxCorrMCut)s )" %locals()
    _B2Q2B3piPostVertexCuts += " & (VFASPF(VCHI2/VDOF) < %(VtxChi2DOFCut)s ) " %locals()

    return SimpleSelection(name, CombineParticles, [ diTrkList, trkList ]
                           , DecayDescriptors = [ "[B+ -> rho(770)0 pi+]cc" ]
                           , MotherCut = _B2Q2B3piPostVertexCuts
                           , CombinationCut = _B2Q2B3piPreVertexCuts
                           )



def makeB2Q2BKSX( name,
                  kSList,
                  diTrkList,
                  MinPTCut,
                  MinMassCut,
                  MaxMassCut,
                  MaxCorrMCut,
                  VtxChi2DOFCut ) :
    """
    Charmless to KSX, where X is a light resonance reconstructed as rho
    """

    _B2Q2BKSXPreVertexCuts = "in_range( %(MinMassCut)s ,AM, %(MaxMassCut)s )" %locals()
    _B2Q2BKSXPreVertexCuts += " & ( APT > %(MinPTCut)s )" %locals()

    _B2Q2BKSXPostVertexCuts = "( BPVCORRM < %(MaxCorrMCut)s )" %locals()
    _B2Q2BKSXPostVertexCuts += " & (VFASPF(VCHI2/VDOF) < %(VtxChi2DOFCut)s )" %locals()

    return SimpleSelection(name, CombineParticles, [ diTrkList, kSList]
                           , DecayDescriptor="B0 -> rho(770)0 KS0"
                           , MotherCut = _B2Q2BKSXPostVertexCuts
                           , CombinationCut = _B2Q2BKSXPreVertexCuts
                           )

def getRelInfo4pi():
    relInfo = []
    for coneAngle in [0.8,1.0,1.3,1.7]:
        conestr = str(coneAngle).replace('.','')
        relInfo += [
        { "Type"         : "RelInfoConeVariables",
             "IgnoreUnmatchedDescriptors" : True,
             "ConeAngle"    : coneAngle,
             "Variables"    : ['CONEANGLE', 'CONEMULT', 'CONEPASYM', 'CONEPTASYM', 'CONEDELTAETA'],
             "DaughterLocations" : {
                "^[B0 -> (rho(770)0 -> pi+ pi- {X0}) (rho(770)0 -> pi+ pi- {X0}) ]CC" : 'P2ConeVar%s_B' % conestr,
                "[B0 -> ^(rho(770)0 -> pi+ pi- {X0}) (rho(770)0 -> pi+ pi- {X0}) ]CC" : 'P2ConeVar%s_X1' % conestr,
                "[B0 -> (rho(770)0 -> pi+ pi- {X0}) ^(rho(770)0 -> pi+ pi- {X0}) ]CC" : 'P2ConeVar%s_X2' % conestr,
                "[B0 -> (rho(770)0 -> ^pi+ pi- {X0}) (rho(770)0 -> pi+ pi- {X0}) ]CC" : 'P2ConeVar%s_X11' % conestr,
                "[B0 -> (rho(770)0 -> pi+ ^pi- {X0}) (rho(770)0 -> pi+ pi- {X0}) ]CC" : 'P2ConeVar%s_X12' % conestr,
                "[B0 -> (rho(770)0 -> pi+ pi- {X0}) (rho(770)0 -> ^pi+ pi- {X0}) ]CC" : 'P2ConeVar%s_X21' % conestr,
                "[B0 -> (rho(770)0 -> pi+ pi- {X0}) (rho(770)0 -> pi+ ^pi- {X0}) ]CC" : 'P2ConeVar%s_X22' % conestr
         } }       ]
    relInfo += [ { "Type" : "RelInfoVertexIsolation", "Location": "VertexIsoInfo" } ]
    return relInfo

def getRelInfo3pi():
    relInfo = []
    for coneAngle in [0.8,1.0,1.3,1.7]:
        conestr = str(coneAngle).replace('.','')
        relInfo += [
        { "Type"         : "RelInfoConeVariables",
             "IgnoreUnmatchedDescriptors" : True,
             "ConeAngle"    : coneAngle,
             "Variables"    : ['CONEANGLE', 'CONEMULT', 'CONEPASYM', 'CONEPTASYM', 'CONEDELTAETA'],
             "DaughterLocations" : {
                "^[B+ -> (rho(770)0 -> pi+ pi- {X0}) pi+ ]CC" : 'P2ConeVar%s_B' % conestr,
                "[B+ -> ^(rho(770)0 -> pi+ pi- {X0}) pi+ ]CC" : 'P2ConeVar%s_X1' % conestr,
                "[B+ -> (rho(770)0 -> pi+ pi- {X0}) ^pi+ ]CC" : 'P2ConeVar%s_X2' % conestr,
                "[B+ -> (rho(770)0 -> ^pi+ pi- {X0}) pi+ ]CC" : 'P2ConeVar%s_X11' % conestr,
                "[B+ -> (rho(770)0 -> pi+ ^pi- {X0}) pi+ ]CC" : 'P2ConeVar%s_X12' % conestr
        } }       ]
        relInfo += [ { "Type" : "RelInfoVertexIsolation", "Location": "VertexIsoInfo" } ]
        return relInfo

def getRelInfoKSX() :
    """
    Create a RelInfoConeVariables dictionary for the given angle
    """
    relInfo = []
    for coneAngle in [0.8,1.0,1.3,1.7]:
        conestr = str(coneAngle).replace('.','')
        relInfo += [
        { "Type"         : "RelInfoConeVariables",
             "IgnoreUnmatchedDescriptors" : True
             ,"ConeAngle"    : coneAngle
             ,"Variables"         : ['CONEANGLE', 'CONEMULT', 'CONEPASYM', 'CONEPTASYM', 'CONEDELTAETA']
             ,'Location'          : 'ConeVar%s_B' % conestr
             , "DaughterLocations" : { 
                 "^[B0 -> (rho(770)0 -> pi+ pi- {X0}) KS0]CC" : 'ConeVar%s_B' % conestr,
                 "[B0 -> ^(rho(770)0 -> pi+ pi- {X0}) KS0]CC" : 'ConeVar%s_X' % conestr,                      
                 "[B0 -> (rho(770)0 -> pi+ pi- {X0}) ^KS0]CC" : 'ConeVar%s_KS' % conestr,
                 "[B0 -> (rho(770)0 -> ^pi+ pi- {X0}) KS0]CC" : 'ConeVar%s_X1' % conestr,
                 "[B0 -> (rho(770)0 -> pi+ ^pi- {X0}) KS0]CC" : 'ConeVar%s_X2' % conestr
        } }]
        relInfo += [ { "Type" : "RelInfoVertexIsolation", "Location": "VertexIsoInfo" } ]
        return relInfo

def makeTISTOSFilter(name):
    specs = {'Hlt1(Two)?Track.*Decision%TOS':0}
    from Configurables import TisTosParticleTagger
    tisTosFilter = TisTosParticleTagger(name+'TISTOSFilter')
    tisTosFilter.TisTosSpecs = specs
    tisTosFilter.ProjectTracksToCalo = False
    tisTosFilter.CaloClustForCharged = False
    tisTosFilter.CaloClustForNeutral = False
    tisTosFilter.TOSFrac = {4:0.0, 5:0.0}
    return tisTosFilter

def tisTosSelection(sel):
    '''Filters Selection sel to be TOS OR TIS.'''
    tisTosFilter = makeTISTOSFilter(sel.name())
    return Selection(sel.name()+'TISTOS', Algorithm=tisTosFilter, RequiredSelections=[sel])

