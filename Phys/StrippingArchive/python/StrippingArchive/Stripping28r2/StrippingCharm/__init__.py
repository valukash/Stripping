###############################################################################
# (c) Copyright 2019 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################

_selections = ('StrippingB2ppipiSigmacmm_Lcpi', 'StrippingChargedHyperons', 'StrippingCharm2PPX', 'StrippingCharmFromBSemi', 'StrippingCharmWeakDecays', 'StrippingD2HHHGamma', 'StrippingD2HHHKs', 'StrippingD2HHHPi0', 'StrippingD2HMuNu', 'StrippingD2KPiPiMuNu', 'StrippingD2PiPi0', 'StrippingD2XMuMuSS', 'StrippingD2hh', 'StrippingD2hhh_FTcalib', 'StrippingDstarD02xx', 'StrippingDstarD0ToHHPi0', 'StrippingDstarD2HHHH', 'StrippingDstarD2KSHHPi0', 'StrippingDstarD2KShh', 'StrippingDstarD2XGamma', 'StrippingDstarPromptWithD02HHHH', 'StrippingDstarPromptWithD02HHMuMu', 'StrippingExcitedDsSpectroscopy', 'StrippingHc2V02H', 'StrippingHc2V03H', 'StrippingHc2V2H', 'StrippingHc2V3H', 'StrippingKKPiPi', 'StrippingLambdac2V0H', 'StrippingLc2L0DDpi', 'StrippingLc2L0LLpi', 'StrippingNeutralCBaryons', 'StrippingPromptCharm', 'StrippingXic2HHH', 'StrippingXic2LambdaKPi')

for _sel in _selections :
    try :
        __import__( '%s.%s'  % ( __name__, _sel ) )
    except Exception, x:
        print '[WARNING] Submodule %s.%s raises the exception "%s" and will be skipped !' % ( __name__,_sel,x )

from sys import modules as _modules
_this = _modules[__name__]

_strippingKeys = filter ( lambda x : x[:9]=='Stripping',
                          locals().keys())

_strippingModules = [getattr(_this, _k) for _k in _strippingKeys]
