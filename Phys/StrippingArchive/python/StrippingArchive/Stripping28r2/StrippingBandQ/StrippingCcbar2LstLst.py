###############################################################################
# (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
'''
Module for selecting Ccbar->LstLst detached line, with loose PT, PID cuts. 
'''

__author__=['Andrii Usachov']
__date__ = '12/01/2017'

__all__ = (
    'Ccbar2LstLstConf'
    )

default_config = {
    'NAME'        : 'Ccbar2LstLst', 
    'BUILDERTYPE' : 'Ccbar2LstLstConf',
    'CONFIG' : {
        'TRCHI2DOF'        :  5.   ,
        'KaonProbNNk'      :  0.1  ,
        'KaonPTSec'        :  100. , # MeV
        'ProtonProbNNp'    :  0.1  ,
        'ProtonP'          :  5000 ,
        'ProtonPTSec'      :  100. , # MeV
        'LstVtxChi2'       :  16.  ,
        'LstMinMass'       :  1400 ,
        'LstMaxMass'       :  1600 ,
        'CombMaxMass'      :  6100., # MeV, before Vtx fit
        'CombMinMass'      :  2800., # MeV, before Vtx fit
        'MaxMass'          :  6000., # MeV, after Vtx fit
        'MinMass'          :  2850. # MeV, after Vtx fit
        },
    'STREAMS' : [ 'Charm'],
    'WGs'     : [ 'BandQ']
    }

from Gaudi.Configuration import *
from GaudiConfUtils.ConfigurableGenerators import FilterDesktop, CombineParticles
from PhysSelPython.Wrappers import Selection, DataOnDemand, MergedSelection
from StrippingConf.StrippingLine import StrippingLine
from StrippingUtils.Utils import LineBuilder


from StandardParticles import StdNoPIDsProtons, StdNoPIDsKaons


class Ccbar2LstLstConf(LineBuilder):
    
    __configuration_keys__ = default_config['CONFIG'].keys()
    
    def __init__(self, name, config ): 
        
        LineBuilder.__init__(self, name, config)
        self.name = name 
        self.config = config

        TRCHI2DOF       =   config['TRCHI2DOF']
        KaonProbNNk     =   config['KaonProbNNk']
        KaonPTSec       =   config['KaonPTSec']
        ProtonProbNNp   =   config['ProtonProbNNp']
        ProtonP         =   config['ProtonP']
        ProtonPTSec     =   config['ProtonPTSec']
        LstVtxChi2      =   config['LstVtxChi2']
        LstMinMass      =   config['LstMinMass']
        LstMaxMass      =   config['LstMaxMass']
        CombMaxMass     =   config['CombMaxMass']
        CombMinMass     =   config['CombMinMass']
        MaxMass         =   config['MaxMass']
        MinMass         =   config['MinMass']



        self.DetachedProtonForLst = self.createSubSel( OutputList = "DetachedProtonForLst" + self.name,
                                              InputList =  DataOnDemand( Location = 'Phys/StdNoPIDsProtons/Particles' ),
                                              Cuts = "(P>%(ProtonP)s*MeV) & (PT> %(ProtonPTSec)s*MeV) & (TRCHI2DOF < %(TRCHI2DOF)s) & (PROBNNp> %(ProtonProbNNp)s)" % self.config
                                              )
        self.DetachedKaonForLst = self.createSubSel( OutputList = "DetachedKaonForLst" + self.name,
                                              InputList =  DataOnDemand( Location = 'Phys/StdNoPIDsKaons/Particles' ),
                                              Cuts = "(PT> %(KaonPTSec)s*MeV) & (TRCHI2DOF < %(TRCHI2DOF)s) & (PROBNNk> %(KaonProbNNk)s)" % self.config
                                              )
        
        self.DetachedLstForJpsiList = self.createCombinationSel( OutputList = "DetachedLstFor" + self.name,
                                                        DaughterLists = [ self.DetachedProtonForLst, self.DetachedKaonForLst],
                                                        DecayDescriptor = "[Lambda(1520)0 -> p+ K-]cc",
                                                        PreVertexCuts = "(in_range( %(LstMinMass)s *MeV, AM, %(LstMaxMass)s *MeV))" % self.config,
                                                        PostVertexCuts = "(in_range( %(LstMinMass)s *MeV, MM, %(LstMaxMass)s *MeV)) & (VFASPF(VCHI2PDOF)<%(LstVtxChi2)s)" % self.config,
                                                        reFitPVs = False
                                                        )
        self.makeDetachedJpsi2LstLst()

        
    def createSubSel( self, OutputList, InputList, Cuts ) :
        '''create a selection using a FilterDesktop'''
        filter = FilterDesktop(Code = Cuts)
        return Selection( OutputList,
                          Algorithm = filter,
                          RequiredSelections = [ InputList ] )
    
    def createCombinationSel( self, OutputList,
                              DecayDescriptor,
                              DaughterLists,
                              DaughterCuts = {} ,
                              PreVertexCuts = "AALL",
                              PostVertexCuts = "ALL",
                              reFitPVs = True) :
        '''create a selection using a ParticleCombiner with a single decay descriptor'''
        combiner = CombineParticles( DecayDescriptor = DecayDescriptor,
                                     DaughtersCuts = DaughterCuts,
                                     MotherCut = PostVertexCuts,
                                     CombinationCut = PreVertexCuts,
                                     ReFitPVs = reFitPVs)
        return Selection ( OutputList,
                           Algorithm = combiner,
                           RequiredSelections = DaughterLists)
        
    def makeDetachedJpsi2LstLst(self):
        DetachedJpsi2LstLst = self.createCombinationSel( OutputList = "DetachedJpsi2LstLst" + self.name,
                                                         DecayDescriptor = " J/psi(1S) -> Lambda(1520)0 Lambda(1520)~0",
                                                         DaughterLists = [ self.DetachedLstForJpsiList ],
                                                         PreVertexCuts = "(in_range( %(CombMinMass)s *MeV, AM, %(CombMaxMass)s *MeV))" % self.config,
                                                         PostVertexCuts = "(in_range( %(MinMass)s *MeV, MM, %(MaxMass)s *MeV)) & (VFASPF(VCHI2PDOF) < 16 ) & (BPVDLS>3)" %self.config )
                                                        
        DetachedJpsi2LstLstLine = StrippingLine( self.name + "Line",
                                                 algos = [ DetachedJpsi2LstLst ] )
        self.registerLine(DetachedJpsi2LstLstLine)




