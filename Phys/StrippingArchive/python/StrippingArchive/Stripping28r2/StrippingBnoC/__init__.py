###############################################################################
# (c) Copyright 2019 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################

_selections = ('StrippingB24pLines', 'StrippingB2CharmlessInclusive', 'StrippingB2HHBDT', 'StrippingB2HHPi0', 'StrippingB2KShh', 'StrippingB2KShhh', 'StrippingB2Kpi0', 'StrippingB2Ksthh', 'StrippingB2TwoBaryons', 'StrippingB2XEta', 'StrippingB2hhpipiPhsSpcCut', 'StrippingB2pphh', 'StrippingBc2hhh_BnoC', 'StrippingBs2KKhh', 'StrippingBs2KSKS_Run2', 'StrippingBs2Kst_0Kst_0', 'StrippingBs2PhiKst0', 'StrippingBs2PhiPhi', 'StrippingBu2Ksthh', 'StrippingBu2hhh', 'StrippingBu2rho0rhoPlus', 'StrippingButo5h', 'StrippingD2HHBDT', 'StrippingHb2Charged2Body', 'StrippingHb2V0V0h', 'StrippingLb2V04h', 'StrippingLb2V0hh', 'StrippingLb2V0pp', 'StrippingXb23ph', 'StrippingXb2p3h', 'StrippingXb2phh')

for _sel in _selections :
    try :
        __import__( '%s.%s'  % ( __name__, _sel ) )
    except Exception, x:
        print '[WARNING] Submodule %s.%s raises the exception "%s" and will be skipped !' % ( __name__,_sel,x )

from sys import modules as _modules
_this = _modules[__name__]

_strippingKeys = filter ( lambda x : x[:9]=='Stripping',
                          locals().keys())

_strippingModules = [getattr(_this, _k) for _k in _strippingKeys]
