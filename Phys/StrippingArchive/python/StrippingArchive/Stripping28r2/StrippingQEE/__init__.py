###############################################################################
# (c) Copyright 2019 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################

_selections = ('StrippingA1MuMu', 'StrippingA2MuMu', 'StrippingA2MuMuSameSign', 'StrippingB2LLP2HH', 'StrippingB2SS', 'StrippingConvertedPhoton', 'StrippingDY2MuMu', 'StrippingDY2ee', 'StrippingDisplJets', 'StrippingDisplVertices', 'StrippingDitau', 'StrippingExotica', 'StrippingFullDiJets', 'StrippingGluinos', 'StrippingH24Mu', 'StrippingHighPtMuons', 'StrippingHltQEE', 'StrippingInclQQ', 'StrippingLFVExotica', 'StrippingLLP2HH', 'StrippingLLP2Jets', 'StrippingLLP2MuX', 'StrippingLb2dp', 'StrippingLowMultINC', 'StrippingMicroDiJets', 'StrippingMuMuSS', 'StrippingSbarSCorrelations', 'StrippingSingleTrackTIS', 'StrippingStrangeBaryons', 'StrippingStrangeBaryonsNoPID', 'StrippingTaggedJets', 'StrippingWJets', 'StrippingWMu', 'StrippingWRareDecay', 'StrippingWe', 'StrippingZ02MuMu', 'StrippingZ02MuMuCalibration', 'StrippingZ02ee', 'StrippingZ0RareDecay')

for _sel in _selections :
    try :
        __import__( '%s.%s'  % ( __name__, _sel ) )
    except Exception, x:
        print '[WARNING] Submodule %s.%s raises the exception "%s" and will be skipped !' % ( __name__,_sel,x )

from sys import modules as _modules
_this = _modules[__name__]

_strippingKeys = filter ( lambda x : x[:9]=='Stripping',
                          locals().keys())

_strippingModules = [getattr(_this, _k) for _k in _strippingKeys]
