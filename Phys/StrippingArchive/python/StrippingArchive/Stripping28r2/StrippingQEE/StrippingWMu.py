###############################################################################
# (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""
Stripping Lines for W->MuNu and studies of their background
Electroweak Group (Conveners: S.Bifani, J.Anderson; Stripping contact: W.Barter)

S.Bifani

WMu signal:           StdAllLooseMuons,  pT>20GeV
WMu control:          StdAllLooseMuons,  pT>15GeV                    (10% PRESCALE)
WMu background:       StdAllNoPIDsMuons, pT>5GeV  &  Hlt1MBNoBias Dec (RATE LIMITED, 20% PRESCALE)
SingleMuon control:   StdAllLooseMuons,  pT>10GeV &  Hlt2SingleMuonHighPT Dec     (1% PRESCALE)
SingleMuon control:   StdAllLooseMuons,  pT>4.8GeV & Hlt2SingleMuonLowPT Dec      (20% PRESCALE)
"""

__all__ = 'WMuConf', 'default_config'
__author__ = 'S. Bifani'

from GaudiConfUtils.ConfigurableGenerators import FilterDesktop
from Configurables import FilterDesktop as Filter
from PhysSelPython.Wrappers import SimpleSelection
from StrippingConf.StrippingLine import StrippingLine
from StrippingUtils.Utils import LineBuilder
from StandardParticles import StdAllLooseMuons, StdAllNoPIDsMuons
from GaudiKernel.SystemOfUnits import GeV, mm

default_config = {
    'NAME': 'WMu',
    'BUILDERTYPE': 'WMuConf',
    'WGs': ['QEE'],
    'STREAMS': ['EW'],
    'CONFIG': {
        'WMu_Prescale': 1.0,
        'WMuLow_Prescale': 0.1,
        'WMuHighIP_Prescale': 1.0,
        'WMuNoMuID_Prescale': 1.0,
        'WMuIso_Prescale': 1.0,
        'STNB_Prescale': 0.2,
        'WMu_Postscale': 1.0,
        'WMuHighIP_Postscale': 1.0,
        'WMuIso_Postscale': 1.0,
        'WMuNoMuID_Postcale': 1.0,
        'SingMuon10_Prescale': 0.01,
        'SingMuon48_Prescale': 0.4,
        'pT': 20. * GeV,
        'MinpT_HighIP': 15. * GeV,
        'MinpTiso': 15. * GeV,
        'pTlow': 15. * GeV,
        'pTvlow': 5. * GeV,
        'SingMuon10_pT': 10. * GeV,
        'SingMuon48_pT': 4.8 * GeV,
        'MinIP': 0.12 * mm,
        'IsoMax': 4 * GeV,
        #
        'HLT2_Control10': "HLT_PASS_RE('Hlt2(EW)?SingleMuon(V)?High.*')",
        'HLT2_Control4800': "HLT_PASS_RE('Hlt2(EW)?SingleMuonLow.*')",
        'HLT1_SingleTrackNoBias': "HLT_PASS( 'Hlt1MBNoBiasDecision' )",
        'HLT2_NoMuID': "HLT_PASS( 'Hlt2EWSingleMuonHighPtNoMUIDDecision' )",
        #
        'RawEvents': ["Muon", "Calo", "Rich", "Velo", "Tracker", "HC"],
    },
}


class WMuConf(LineBuilder):

    __configuration_keys__ = default_config['CONFIG'].keys()

    def __init__(self, name, config):

        LineBuilder.__init__(self, name, config)

        #---------------------------#
        # Single Muon Control Lines #
        #---------------------------#

        sel = makeFilter(
            name + 'Mu10', StdAllLooseMuons, min_PT=config['SingMuon10_pT'])

        self.registerLine(
            StrippingLine(
                name + 'Control10Line',
                prescale=config['SingMuon10_Prescale'],
                postscale=config['WMu_Postscale'],
                HLT2=config['HLT2_Control10'],
                checkPV=False,
                selection=sel,
            ))

        #-----------------------------------------------------------------

        sel = makeFilter(
            name + 'Mu48', StdAllLooseMuons, min_PT=config['SingMuon48_pT'])

        self.registerLine(
            StrippingLine(
                name + 'Control4800Line',
                prescale=config['SingMuon48_Prescale'],
                postscale=config['WMu_Postscale'],
                HLT2=config['HLT2_Control4800'],
                checkPV=False,
                selection=sel,
            ))

        #------------#
        # WMu signal #
        #------------#

        sel = makeFilter(name + 'Wmu', StdAllLooseMuons, min_PT=config['pT'])

        self.registerLine(
            StrippingLine(
                name + 'Line',
                prescale=config['WMu_Prescale'],
                postscale=config['WMu_Postscale'],
                checkPV=False,
                selection=sel,
                RequiredRawEvents=config['RawEvents'],
            ))

        #-------------#
        # WMu control #
        #-------------#

        sel = makeFilter(
            name + 'WMuLow', StdAllLooseMuons, min_PT=config['pTlow'])

        self.registerLine(
            StrippingLine(
                name + 'LowLine',
                prescale=config['WMuLow_Prescale'],
                postscale=config['WMu_Postscale'],
                checkPV=False,
                selection=sel,
                RequiredRawEvents=config['RawEvents'],
            ))

        #----------------#
        # WMu background #
        #----------------#

        sel = makeFilter(
            name + 'SingleTrackNoBias',
            StdAllNoPIDsMuons,
            min_PT=config['pTlow'])

        self.registerLine(
            StrippingLine(
                name + 'SingleTrackNoBiasLine',
                prescale=config['WMu_Prescale'],
                postscale=config['WMu_Postscale'],
                HLT1=config['HLT1_SingleTrackNoBias'],
                checkPV=False,
                selection=sel,
            ))

        #---------------------------

        sel = makeFilter(
            name + 'SingleTrackNoBiasPS',
            StdAllNoPIDsMuons,
            min_PT=config['pTvlow'])

        self.registerLine(
            StrippingLine(
                name + 'SingleTrackNoBiasLinePS',
                prescale=config['STNB_Prescale'],
                postscale=config['WMu_Postscale'],
                HLT1=config['HLT1_SingleTrackNoBias'],
                checkPV=False,
                selection=sel,
            ))

        #-----------------#
        # WMu No muons ID #
        #-----------------#

        sel = makeFilter(
            name + '_NoMuID', StdAllNoPIDsMuons, min_PT=config['pTlow'])

        self.registerLine(
            StrippingLine(
                name + 'NoMuID',
                RequiredRawEvents=config['RawEvents'],
                prescale=config['WMuNoMuID_Prescale'],
                postscale=config['WMuNoMuID_Postcale'],
                HLT2=config['HLT2_NoMuID'],
                checkPV=False,
                selection=sel,
            ))

        #-------------#
        # WMu IP Line #
        #-------------#

        sel = makeFilter(
            name + '_HighIP',
            StdAllLooseMuons,
            min_PT=config['MinpT_HighIP'],
            min_IP=config['MinIP'])

        self.registerLine(
            StrippingLine(
                name + 'HighIPLine',
                RequiredRawEvents=config['RawEvents'],
                prescale=config['WMuHighIP_Prescale'],
                postscale=config['WMuHighIP_Postscale'],
                checkPV=True,
                selection=sel))

        #--------------#
        # WMu Iso Line #
        #--------------#

        # WMu Isolation variables
        relinfo = [{
            "Type":
            "RelInfoConeVariables",
            "ConeAngle":
            0.5,
            "Variables": ['CONEPT', 'CONEANGLE', 'CONEMULT', 'CONEPTASYM'],
            "Location":
            "Iso"
        }]

        sel = makeFilter(
            name + '_Iso', StdAllLooseMuons, min_PT=config['MinpTiso'])
        relinfofilter = Filter(
            name + "IsoFilter",
            Code="RELINFO('/Event/Phys/" + name +
            "IsoLine/Iso', 'CONEPT', 100000.) < " + str(config["IsoMax"]),
            Inputs=['Phys/' + name + 'IsoLine/Particles'])

        self.registerLine(
            StrippingLine(
                name + 'IsoLine',
                RequiredRawEvents=config['RawEvents'],
                prescale=config['WMuIso_Prescale'],
                postscale=config['WMuIso_Postscale'],
                checkPV=False,
                selection=sel,
                RelatedInfoTools=relinfo,
                RelatedInfoFilter=relinfofilter))


def makeFilter(name, single_input, min_PT=None, max_PT=None, min_IP=None):
    """
    Simple FilterDesktop selection having min_PT/max_PT/min_IP cuts.
    """

    cuts = []

    if min_PT:
        cuts.append("PT > {}".format(min_PT))

    if max_PT:
        cuts.append("PT < {}".format(max_PT))

    if min_IP:
        cuts.append("MIPDV(PRIMARY) > {}".format(min_IP))

    cut_str = ""
    for i, c in enumerate(cuts):
        if i == 0:
            cut_str += "({})".format(c)
        else:
            cut_str += " & ({})".format(c)

    selection = SimpleSelection(
        name,
        FilterDesktop, [single_input],
        Preambulo=["from LoKiTracks.decorators import *"],
        Code=cut_str)

    return selection
