###############################################################################
# (c) Copyright 2020 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""
Stripping Lines for W->MuNu and studies of their background
Electroweak Group (Conveners: S.Bifani, J.Anderson; Stripping contact: W.Barter)

S.Bifani

WMu signal:           StdAllLooseMuons,  pT>20GeV
WMu control:          StdAllLooseMuons,  pT>15GeV                    (10% PRESCALE)
WMu background:       StdAllNoPIDsMuons, pT>5GeV  &  Hlt1MBNoBias Dec (RATE LIMITED, 20% PRESCALE)
SingleMuon control:   StdAllLooseMuons,  pT>10GeV &  Hlt2SingleMuonHighPT Dec     (1% PRESCALE)
SingleMuon control:   StdAllLooseMuons,  pT>4.8GeV & Hlt2SingleMuonLowPT Dec      (20% PRESCALE)
"""

__all__ = 'HeavyIonWMuNuConf', 'default_config'
__author__ = 'S. Bifani'

from GaudiConfUtils.ConfigurableGenerators import FilterDesktop
from PhysSelPython.Wrappers import SimpleSelection
from StrippingConf.StrippingLine import StrippingLine
from StrippingUtils.Utils import LineBuilder
from StandardParticles import StdAllLooseMuons, StdAllNoPIDsMuons
from GaudiKernel.SystemOfUnits import GeV


default_config = {
  'NAME'        : 'HeavyIonWMuNu',
  'BUILDERTYPE' : 'HeavyIonWMuNuConf',
  'WGs'         : [ 'IFT'],
  'STREAMS'     : [ 'IFT' ],
  'CONFIG'      : { 
    'WMu_Prescale'          : 1.0,
    'WMuLow_Prescale'       : 0.1,
    'STNB_Prescale'         : 0.2,
    'WMu_Postscale'         : 1.0,
    'SingMuon10_Prescale'   : 0.01,
    'SingMuon48_Prescale'   : 0.4,
    'pT'                    : 20. * GeV,
    'pTlow'                 : 15. * GeV,
    'pTvlow'                :  5. * GeV,
    'SingMuon10_pT'         : 10. * GeV,
    'SingMuon48_pT'         : 4.8 * GeV,
    #
    'HLT2_Control10'        : "HLT_PASS_RE('Hlt2(EW)?SingleMuon(V)?High.*')",
    'HLT2_Control4800'      : "HLT_PASS_RE('Hlt2(EW)?SingleMuonLow.*')",
    'HLT1_SingleTrackNoBias': "HLT_PASS( 'Hlt1MBNoBiasDecision' )",
    #
    'RawEvents' : ["Muon", "Calo", "Rich", "Velo", "Tracker", "HC"],

    
    "ProbNNmu_min"     : 0.5,

  },
}

class HeavyIonWMuNuConf( LineBuilder ) :

  __configuration_keys__ = default_config['CONFIG'].keys()

  def __init__( self, name, config ) :

    LineBuilder.__init__( self, name, config )

    #---------------------------#
    # Single Muon Control Lines #
    #---------------------------#

    sel = makeFilter(name+'Mu10', StdAllLooseMuons, config['SingMuon10_pT'])

    self.registerLine(StrippingLine( name + 'Control10Line',
      prescale  = config[ 'SingMuon10_Prescale' ],
      postscale = config[ 'WMu_Postscale' ],
      HLT2      = config[ 'HLT2_Control10'],
      checkPV   = False,
      selection = sel,
    ))

    #-----------------------------------------------------------------        

    sel = makeFilter(name+'Mu48', StdAllLooseMuons, config['SingMuon48_pT'])

    self.registerLine(StrippingLine( name + 'Control4800Line',
      prescale  = config[ 'SingMuon48_Prescale' ],
      postscale = config[ 'WMu_Postscale' ],
      HLT2      = config[ 'HLT2_Control4800'],
      checkPV   = False,
      selection = sel,
    ))


    #------------#
    # WMu signal #
    #------------#

    sel = makeFilter(name+'Wmu', StdAllLooseMuons, config['pT'])

    self.registerLine(StrippingLine( name + 'Line',
      prescale  = config[ 'WMu_Prescale'  ],
      postscale = config[ 'WMu_Postscale' ],
      checkPV   = False,
      selection = sel,
      RequiredRawEvents = config['RawEvents'],
    ))


    #-------------#
    # WMu control #
    #-------------#

    sel = makeFilter(name+'WMuLow', StdAllLooseMuons, config['pTlow'])

    self.registerLine(StrippingLine( name + 'LowLine',
      prescale  = config[ 'WMuLow_Prescale' ],
      postscale = config[ 'WMu_Postscale'   ],
      checkPV   = False,
      selection = sel,
      RequiredRawEvents = config['RawEvents'],
    ))

    #----------------#
    # WMu background #
    #----------------#

    sel = makeFilter(name+'SingleTrackNoBias', StdAllNoPIDsMuons, config['pTlow'])

    self.registerLine(StrippingLine( name + 'SingleTrackNoBiasLine',
        prescale  = config[ 'WMu_Prescale'  ],
        postscale = config[ 'WMu_Postscale' ],
        HLT1      = config[ 'HLT1_SingleTrackNoBias'],
        checkPV   = False,
        selection = sel,
    ))


    #---------------------------

    sel = makeFilter(name+'SingleTrackNoBiasPS', StdAllNoPIDsMuons, config['pTvlow'])

    self.registerLine(StrippingLine( name + 'SingleTrackNoBiasLinePS',
        prescale  = config[ 'STNB_Prescale' ],
        postscale = config[ 'WMu_Postscale' ],
        HLT1      = config[ 'HLT1_SingleTrackNoBias'],
        checkPV   = False,
        selection = sel,
    ))


def makeFilter(name, single_input, min_PT):
  """
  Simple FilterDesktop selection having only min PT cut.
  """
  return SimpleSelection(name, FilterDesktop, [single_input],
                         Preambulo = [ "from LoKiTracks.decorators import *" ],    
                         Code      = 'PT > {}'.format(min_PT)
                         )

