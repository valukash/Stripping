###############################################################################
# (c) Copyright 2020 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
# Stripping Lines for W -> nH, n = 3,5,7,9,11
#
# Donal Hill
#
# W2nH :     StdAllNoPIDsPions, pT>1GeV & MM>60GeV

__all__ = (
  'W2nHConf',
  'default_config',
)


from GaudiConfUtils.ConfigurableGenerators import CombineParticles
from PhysSelPython.Wrappers import Selection
from StrippingConf.StrippingLine import StrippingLine
from StrippingUtils.Utils import LineBuilder
from StandardParticles import StdAllNoPIDsPions
from GaudiKernel.SystemOfUnits import GeV
from GaudiKernel.SystemOfUnits import MeV


default_config = {
    'NAME'        : 'W2nH',
    'BUILDERTYPE' : 'W2nHConf',
    'WGs'         : [ 'QEE'],
    'STREAMS'     : [ 'EW' ],
    'CONFIG'      : { 
        'W2nH_Prescale'  : 1.0,
        'W2nH_Postscale' : 1.0,
        'pT'        : 3. * GeV,
        'TrChi2'    : 5.,
        'pErr'      : 0.01,
        'vChi2'     : 10,
        'GhostProb' : 0.4,
        'pTmin'     : 80. * GeV,    #Minimun pT of the array
        'MMmin'     : 50. * GeV,
        'MMmax'     : 110. * GeV,
    },
}


class W2nHConf( LineBuilder ) :

    __configuration_keys__ = default_config['CONFIG'].keys()

    def __init__( self, name, config ) :

        LineBuilder.__init__( self, name, config )

        # W23H signal
        decay = '[W+ -> pi+ pi- pi+]cc'
        self.registerLine(make_line( name+'3H', config, decay ))

        # W23H WS
        decay = '[W+ -> pi+ pi+ pi+]cc'
        self.registerLine(make_line( name+'3HWS', config, decay ))

        # W25H signal
        decay = '[W+ -> pi+ pi- pi+ pi- pi+]cc'
        self.registerLine(make_line( name+'5H', config, decay ))

        # W25H WS
        decay = '[W+ -> pi+ pi+ pi+ pi- pi+]cc'
        self.registerLine(make_line( name+'5HWS', config, decay ))

        # W27H signal
        decay = '[W+ -> pi+ pi- pi+ pi- pi+ pi- pi+]cc'
        self.registerLine(make_line( name+'7H', config, decay ))

        # W27H WS
        decay = '[W+ -> pi+ pi+ pi+ pi- pi+ pi- pi+]cc'
        self.registerLine(make_line( name+'7HWS', config, decay ))

#===============================================================================

def make_combination(name, config, decay):
  dcut = '(PT>%(pT)s) & (TRCHI2DOF<%(TrChi2)s) & ((PERR2)/(P*P)<%(pErr)s) & (TRGHOSTPROB<%(GhostProb)s)'%config
  ccut = 'APT>%(pTmin)s'%config
  mcut = '(MM>%(MMmin)s) & (MM<%(MMmax)s) & (VFASPF(VCHI2/VDOF)<%(vChi2)s)'%config

  algo = CombineParticles(
    DecayDescriptor     = decay,
    DaughtersCuts       = {'pi+':dcut, 'pi-':dcut },
    CombinationCut      = ccut,
    MotherCut           = mcut,
    WriteP2PVRelations  = False
  )

  return Selection(name+'Sel', Algorithm=algo, RequiredSelections=[StdAllNoPIDsPions])


def make_line(name, config, decay):
  return StrippingLine( name + 'Line',
    prescale  = config[ 'W2nH_Prescale' ],
    postscale = config[ 'W2nH_Postscale' ],
    checkPV   = False,
    selection = make_combination(name, config, decay)
  )

