###############################################################################
# (c) Copyright 2020 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
#Module for selecting B0->(Lambda_c+ -> p+ K- pi+) mu-

__author__=['Nikita Beliy', 'Jibo He']
__date__ = '28/11/2016'
__version__= '$Revision: 1.0 $'

__all__ = (
    'B02Lcmu_pKpiConf'
    )

from Gaudi.Configuration import *
from GaudiConfUtils.ConfigurableGenerators import FilterDesktop, CombineParticles
from PhysSelPython.Wrappers import Selection, DataOnDemand
from StrippingConf.StrippingLine import StrippingLine
from StrippingUtils.Utils import LineBuilder

default_config = {
    'NAME'              :   'B02Lcmu_pKpi',
    'BUILDERTYPE'       :   'B02Lcmu_pKpiConf',
    'CONFIG'    : {
        'ProtonCuts'    :   '(PROBNNp  > 0.10) & (PT > 400*MeV) & (TRGHOSTPROB<0.4)',
        'KaonCuts'      :   '(PROBNNk  > 0.10) & (PT > 300*MeV) & (TRGHOSTPROB<0.4)',
        'PionCuts'      :   '(PROBNNpi > 0.18) & (PT > 200*MeV) & (TRGHOSTPROB<0.4)',
        'MuonCuts'      :   '(PROBNNmu > 0.10) & (PT > 600*MeV) & (TRGHOSTPROB<0.4)',
        'LambdacMothCut':  "(ADAMASS('Lambda_c+')<24*MeV)",
        'LambdacVxCut'  :   '(VFASPF(VCHI2/VDOF) < 14.) & (PT > 1*GeV) & (P > 15*GeV)',
        'B0MotheCut'    :  "(ADAMASS('B0')<500*MeV)",
        'B0VxCut'       :   "(VFASPF(VCHI2/VDOF) < 14.) & (BPVDIRA>0.988)",
        'Prescale'      :  1.
        },
    'STREAMS'           : ['Bhadron' ],
    'WGs'               : ['RD']
    }


class B02Lcmu_pKpiConf(LineBuilder):

    __configuration_keys__ = default_config['CONFIG'].keys() 

    def __init__(self, name, config ):

        LineBuilder.__init__(self, name, config)
        self.name = name
        self.config = config

        #Lambda_c
        self.SelProtons = self.createSubSel( OutputList = name + 'SelProtons',
                                             InputList =  DataOnDemand(Location = 'Phys/StdLooseProtons/Particles' ),
                                             Cuts = config['ProtonCuts']
                                            )

        self.SelKaons = self.createSubSel( OutputList = name + 'SelKaons',
                                           InputList =  DataOnDemand(Location = 'Phys/StdLooseKaons/Particles' ),
                                           Cuts = config['KaonCuts']
                                           )
        
        self.SelPions = self.createSubSel( OutputList = name + 'SelPions',
                                           InputList =  DataOnDemand(Location = 'Phys/StdLoosePions/Particles' ),
                                           Cuts = config['PionCuts']
                                           )

        self.SelLc2pKpi = self.createCombinationSel(OutputList = name + 'SelLc2pKpi',
                                            DecayDescriptor = '[Lambda_c+ -> p+ K- pi+]cc ',
                                            DaughterLists = [ self.SelProtons, self.SelKaons, self.SelPions ],
                                            PreVertexCuts  = config['LambdacMothCut'],
                                            PostVertexCuts = config['LambdacVxCut']
                                            )

        #B0->Lambda_c+ mu-
        self.SelMuons = self.createSubSel( OutputList = name + 'SelMuons',
                                            InputList =  DataOnDemand(Location = 'Phys/StdLooseMuons/Particles' ),
                                            Cuts = config['MuonCuts']
                                        )

        self.SelB02Lcmu = self.createCombinationSel ( OutputList = name + 'SelB02Lcmu',
                                                    DecayDescriptor = '[B0 -> Lambda_c+ mu-]cc',
                                                    DaughterLists = [self.SelMuons, self.SelLc2pKpi],
                                                    PreVertexCuts  = config['B0MotheCut'],
                                                    PostVertexCuts = config['B0VxCut'] 
                                                    )
        self.B02Lcmu_pKpiBDTLine = StrippingLine( self.name + 'Line',
                                                prescale  = config['Prescale'],
                                                algos     = [self.SelB02Lcmu]
                                                )
        self.registerLine( self.B02Lcmu_pKpiBDTLine )

    def createSubSel( self, OutputList, InputList, Cuts ) :
        '''create a selection using a FilterDesktop'''
        filter = FilterDesktop(Code = Cuts)
        return Selection( OutputList,
                      Algorithm = filter,
                      RequiredSelections = [ InputList ] )

    def createCombinationSel( self, OutputList,
                          DecayDescriptor,
                          DaughterLists,
                          DaughterCuts = {} ,
                          PreVertexCuts = "ALL",
                          PostVertexCuts = "ALL",
                          ReFitPVs = True ) :
        '''create a selection using a ParticleCombiner with a single decay descriptor'''
        combiner = CombineParticles( DecayDescriptor = DecayDescriptor,
                                 DaughtersCuts = DaughterCuts,
                                 MotherCut = PostVertexCuts,
                                 CombinationCut = PreVertexCuts,
                                 ReFitPVs = False)
        return Selection ( OutputList,
                       Algorithm = combiner,
                       RequiredSelections = DaughterLists)


