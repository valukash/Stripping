###############################################################################
# (c) Copyright 2020 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""
    Exotic six-quarks (hexa-quarks) with charm or strange: [c(s)u(d)][ud][ud] decaying to two same-sign protons
    - Hc++ -> pp (exch , Cabibbo-suppr.)
    - Hc++ -> ppK-pi+
    - Hc+ -> ppK- (exch)
    - Hc+ -> ppK-pi+pi-
    - Hs+ -> pppi- : LLL and DDD
"""


__author__ = ['Ivan Polyakov']
__date__ = ['17/11/2016']

__all__ = (
    'default_config',
    'StrippingCharm2PPXConf' ##???
)

from GaudiKernel.SystemOfUnits import MeV, GeV, mm, mrad
from Configurables import CombineParticles, FilterDesktop
#from GaudiConfUtils.ConfigurableGenerators import CombineParticles, FilterDesktop, DaVinci__N3BodyDecays
from StandardParticles import (
    StdAllNoPIDsProtons,
    StdAllNoPIDsKaons,
    StdAllNoPIDsPions,
    StdNoPIDsDownProtons,
    StdNoPIDsDownPions,
)
from PhysSelPython.Wrappers import Selection
from StrippingUtils.Utils import LineBuilder
from StrippingConf.StrippingLine import StrippingLine

default_config = {
    'NAME': 'Charm2PPX',
    'WGs': ['Charm'],
    'BUILDERTYPE': 'StrippingCharm2PPXConf',
    'STREAMS': ['Charm'],
    'CONFIG': {
        'Daug_All_CloneDist_MIN': 5000,
        'Daug_All_TrChostProb_MAX': 0.5,
        # Minimum transverse momentum all daughters must satisfy
        'Daug_All_PT_MIN': 200.0*MeV,
        # Minimum transverse momentum at least 1 daughter must satisfy
        'Daug_1ofAll_PT_MIN': 700*MeV, # 500.0*MeV, # 1000
        'HsDaug_1ofAll_PT_MIN': -500.0*MeV, # 1000
        # Minimum best primary vertex IP chi^2 all daughters must satisfy
        'Daug_All_BPVIPCHI2_MIN': 4.0,
        'Daug_All_MIPCHI2_MIN': 4.0,
        # Minimum best PV IP chi^2 at least 1 daughter must satisfy
        'Daug_1ofAll_BPVIPCHI2_MIN': 9.0, # 9.0
        'HsDaug_1ofAll_BPVIPCHI2_MIN': 9.0, # 9.0
        'HsDownDaug_1ofAll_BPVIPCHI2_MIN': -9.0, #
        # Minimum daughter momentum
        'Daug_P_MIN': 3.2*GeV,
        'Proton_P_MIN': 10.0*GeV,
        'Proton_PT_MIN': 300.0*MeV, # -250*MeV,
        # Maximum daughter momentum
        'Daug_P_MAX': 100.0*GeV,
        # Minimum daughter pseudorapidity
        'Daug_ETA_MIN': 2.0,
        # Maximum daughter pseudorapidity
        'Daug_ETA_MAX': 4.9,
        # Minimum daughter proton ProbNN
        'Proton_ProbNN_MIN': 0.1,
        'Proton_ProbNNpi_MAX': 0.55, #0.5
        'Proton_ProbNNk_MAX': 0.8, #0.8
        'Proton_1ofAll_ProbNN_MIN': 0.5,
        'HsProton_1ofAll_ProbNN_MIN': 0.5,
        # Minimum daughter kaon ProbNN
        'Kaon_ProbNN_MIN': 0.1,
        # Maximum daughter pion ProbNN
        'Pion_ProbNN_MIN': 0.1,

        # Hc maximum mass
        'Comb_MASS_MAX': 3500*MeV , # 3350*MeV,
        'HsComb_MASS_MAX': 2125*MeV,
        # Maximum distance of closest approach of daughters
        'Comb_ADOCACHI2_MAX': 16,
        'HsComb_ADOCACHI2_MAX': 16,
        # Maximum Hc vertex chi^2 per vertex fit DoF
        'Hc_VCHI2VDOF_MAX': 25.0,
        'Hs_VCHI2VDOF_MAX': 25.0,
        # Maximum angle between Hc momentum and Hc direction of flight
        'Hc_BPVIPCHI2_MAX': 16,
        'Hs_BPVIPCHI2_MAX': 16,
        # Primary vertex displacement requirement, that the Hc is some sigma
        # away from the PV and it has a minimum flight time
        'Hc_BPVVDCHI2_MIN': 9.0,
        'Hc_BPVCTAU_MIN': 0.*mm, # 0.03*mm,
        'Hs_BPVVDCHI2_MIN': 9.0,
        'HsDown_BPVVDCHI2_MIN': 4.0,
        'Hs_BPVCTAU_MIN': 0.1*mm,
        # Hc minimum transverse momenta
        'Hc_PT_MIN': -1000*MeV,
        'Hs_PT_MIN': -1000*MeV,
        # HLT filters, only process events firing triggers matching the RegEx
        'Hlt1Filter': None,
        'Hlt2Filter': None,

        # Fraction of candidates to randomly throw away before stripping
        'PrescaleHc2PP': 1.0,
        'PrescaleHc2PPKPi': 1.0,
        'PrescaleHc2PPK': 1.0,
        'PrescaleHc2PPKPiPi': 1.0,
        'PrescaleHs2PPPi': 1.0,
        # Fraction of candidates to randomly throw after before stripping
        'PostscaleHc2PP': 1.0,
        'PostscaleHc2PPKPi': 1.0,
        'PostscaleHc2PPK': 1.0,
        'PostscaleHc2PPKPiPi': 1.0,
        'PostscaleHs2PPPi': 1.0,
    }
}

class StrippingCharm2PPXConf(LineBuilder):
    """Creates LineBuilder object containing the stripping lines."""
    # Allowed configuration keys
    __configuration_keys__ = default_config['CONFIG'].keys()

    # Decay descriptors
    Hc2pp = ['[Sigma_c++ -> p+ p+]cc']
    Hc2ppkpi = ['[Sigma_c++ -> p+ p+ K- pi+]cc']
    Hc2ppk = ['[Sigma_c+ -> p+ p+ K-]cc']
    Hc2ppkpipi = ['[Sigma_c+ -> p+ p+ K- pi+ pi-]cc']

    Hs2pppi = ['[Sigma_c+ -> p+ p+ pi-]cc']

    def __init__(self, name, config):
        """Initialise this LineBuilder instance."""
        self.name = name
        self.config = config
        LineBuilder.__init__(self, name, config)

        # Line names
        hc2pp_name = '{0}Hc2PP'.format(name)
        hc2ppkpi_name = '{0}Hc2PPKPi'.format(name)
        hc2ppk_name = '{0}Hc2PPK'.format(name)
        hc2ppkpipi_name = '{0}Hc2PPKPiPi'.format(name)
        
        hs2pppi_name = '{0}Hs2PPPi'.format(name)
        hs2pppiDown_name = '{0}Hs2PPPiDown'.format(name)

        # Build pion, kaon, and proton cut strings
        daugCuts = (
            '(PT > {0[Daug_All_PT_MIN]})'
            ' & (CLONEDIST > {0[Daug_All_CloneDist_MIN]} )'
            ' & (TRGHOSTPROB < {0[Daug_All_TrChostProb_MAX]})'
            ' & (MIPCHI2DV() > {0[Daug_All_MIPCHI2_MIN]})'
        ).format(self.config)
        pidFiducialCuts = (
            '(in_range({0[Daug_P_MIN]}, P, {0[Daug_P_MAX]}))'
            '& (in_range({0[Daug_ETA_MIN]}, ETA, {0[Daug_ETA_MAX]}))'
            ' & HASRICH'
        ).format(self.config)
        protonPIDCuts = (
            pidFiducialCuts +
            '& (PROBNNp > {0[Proton_ProbNN_MIN]})'
            '& (PROBNNpi < {0[Proton_ProbNNpi_MAX]})'
            '& (PROBNNk < {0[Proton_ProbNNk_MAX]})'
            '& (P > {0[Proton_P_MIN]})'
            '& (PT > {0[Proton_PT_MIN]})'
        ).format(self.config)
        kaonPIDCuts = (
            pidFiducialCuts +
            '& (PROBNNk > {0[Kaon_ProbNN_MIN]})'
        ).format(self.config)
        pionPIDCuts = (
            pidFiducialCuts +
            '& (PROBNNpi > {0[Pion_ProbNN_MIN]})'
        ).format(self.config)

        pionCuts = '{0} & {1}'.format(daugCuts, pionPIDCuts)
        kaonCuts = '{0} & {1}'.format(daugCuts, kaonPIDCuts)
        protonCuts = '{0} & {1}'.format(daugCuts, protonPIDCuts)

        # Filter StdAllNoPIDs particles
        self.inPions = Selection(
            'PionsFor{0}'.format(name),
            Algorithm=FilterDesktop(
                'FilterPionsFor{0}'.format(name),
                Code=pionCuts
            ),
            RequiredSelections=[StdAllNoPIDsPions]
        )
        self.inKaons = Selection(
            'KaonsFor{0}'.format(name),
            Algorithm=FilterDesktop(
                'FilterKaonsFor{0}'.format(name),
                Code=kaonCuts
            ),
            RequiredSelections=[StdAllNoPIDsKaons]
        )
        self.inProtons = Selection(
            'ProtonsFor{0}'.format(name),
            Algorithm=FilterDesktop(
                'FilterProtonsFor{0}'.format(name),
                Code=protonCuts
            ),
            RequiredSelections=[StdAllNoPIDsProtons]
        )
        self.inDownPions = Selection(
            'DownPionsFor{0}'.format(name),
            Algorithm=FilterDesktop(
                'FilterDownPionsFor{0}'.format(name),
                Code=pionCuts
            ),
            RequiredSelections=[StdNoPIDsDownPions]
        )
        self.inDownProtons = Selection(
            'DownProtonsFor{0}'.format(name),
            Algorithm=FilterDesktop(
                'FilterDownProtonsFor{0}'.format(name),
                Code=protonCuts
            ),
            RequiredSelections=[StdNoPIDsDownProtons]
        )

        self.selHc2PP = self.makeHc2PPX(
            name=hc2pp_name,
            inputSel=[self.inProtons],
            decDescriptors=self.Hc2pp
        )

        self.selHc2PPKPi = self.makeHc2PPX(
            name=hc2ppkpi_name,
            inputSel=[self.inProtons, self.inKaons, self.inPions ],
            decDescriptors=self.Hc2ppkpi
        )

        self.selHc2PPK = self.makeHc2PPX(
            name=hc2ppk_name,
            inputSel=[self.inProtons, self.inKaons ],
            decDescriptors=self.Hc2ppk
        )

        self.selHc2PPKPiPi = self.makeHc2PPX(
            name=hc2ppkpipi_name,
            inputSel=[self.inProtons, self.inKaons, self.inPions ],
            decDescriptors=self.Hc2ppkpipi
        )

        self.selHs2PPPi = self.makeHs2PPX(
            name=hs2pppi_name,
            inputSel=[self.inProtons, self.inPions ],
            decDescriptors=self.Hs2pppi,
            mipchi2_cut = config['HsDaug_1ofAll_BPVIPCHI2_MIN'],
            vdchi2_cut = config['Hs_BPVVDCHI2_MIN'],
        )

        self.selHs2PPPiDown = self.makeHs2PPX(
            name=hs2pppiDown_name,
            inputSel=[self.inDownProtons, self.inDownPions ],
            decDescriptors=self.Hs2pppi,
            mipchi2_cut = config['HsDownDaug_1ofAll_BPVIPCHI2_MIN'],
            vdchi2_cut = config['HsDown_BPVVDCHI2_MIN'],
        )

        ### lines

        self.line_Hc2PP = self.make_line(
            name='{0}Line'.format(hc2pp_name),
            selection=self.selHc2PP,
            prescale=config['PrescaleHc2PP'],
            postscale=config['PostscaleHc2PP'],
            HLT1=config['Hlt1Filter'],
            HLT2=config['Hlt2Filter']
        )

        self.line_Hc2PPKPi = self.make_line(
            name='{0}Line'.format(hc2ppkpi_name),
            selection=self.selHc2PPKPi,
            prescale=config['PrescaleHc2PPKPi'],
            postscale=config['PostscaleHc2PPKPi'],
            HLT1=config['Hlt1Filter'],
            HLT2=config['Hlt2Filter']
        )

        self.line_Hc2PPK = self.make_line(
            name='{0}Line'.format(hc2ppk_name),
            selection=self.selHc2PPK,
            prescale=config['PrescaleHc2PPK'],
            postscale=config['PostscaleHc2PPK'],
            HLT1=config['Hlt1Filter'],
            HLT2=config['Hlt2Filter']
        )

        self.line_Hc2PPKPiPi = self.make_line(
            name='{0}Line'.format(hc2ppkpipi_name),
            selection=self.selHc2PPKPiPi,
            prescale=config['PrescaleHc2PPKPiPi'],
            postscale=config['PostscaleHc2PPKPiPi'],
            HLT1=config['Hlt1Filter'],
            HLT2=config['Hlt2Filter']
        )

        self.line_Hs2PPPi = self.make_line(
            name='{0}Line'.format(hs2pppi_name),
            selection=self.selHs2PPPi,
            prescale=config['PrescaleHs2PPPi'],
            postscale=config['PostscaleHs2PPPi'],
            HLT1=config['Hlt1Filter'],
            HLT2=config['Hlt2Filter']
        )

        self.line_Hs2PPPiDown = self.make_line(
            name='{0}Line'.format(hs2pppiDown_name),
            selection=self.selHs2PPPiDown,
            prescale=config['PrescaleHs2PPPi'],
            postscale=config['PostscaleHs2PPPi'],
            HLT1=config['Hlt1Filter'],
            HLT2=config['Hlt2Filter']
        )

    def make_line(self, name, selection, prescale, postscale, **kwargs):
        """Create the stripping line defined by the selection.

        Keyword arguments:
        name -- Base name for the Line
        selection -- Selection instance
        prescale -- Fraction of candidates to randomly drop before stripping
        postscale -- Fraction of candidates to randomly drop after stripping
        **kwargs -- Keyword arguments passed to StrippingLine constructor
        """
        # Only create the line with positive pre- and postscales
        # You can disable each line by setting either to a negative value
        if prescale > 0 and postscale > 0:
            line = StrippingLine(
                name,
                selection=selection,
                prescale=prescale,
                postscale=postscale,
                **kwargs
            )
            self.registerLine(line)
            return line
        else:
            return False

    def makeHc2PPX(self, name, inputSel, decDescriptors):
        """Return a Selection instance for a Hc -> p+ p+ X decay.

        Keyword arguments:
        name -- Name to give the Selection instance
        inputSel -- List of inputs passed to Selection.RequiredSelections
        decDescriptors -- List of decay descriptors for CombineParticles
        """

        combCuts = (
            '(AMAXCHILD(PT) > {0[Daug_1ofAll_PT_MIN]})'
            ' & (AMAXCHILD(PROBNNp) > {0[Proton_1ofAll_ProbNN_MIN]})'
            ' & (AMAXCHILD(BPVIPCHI2()) > {0[Daug_1ofAll_BPVIPCHI2_MIN]})'
            " & (ACUTDOCACHI2({0[Comb_ADOCACHI2_MAX]}, ''))"
        ).format(self.config)

        HcCuts = (
            '(VFASPF(VCHI2/VDOF) < {0[Hc_VCHI2VDOF_MAX]})'
            ' & (BPVVDCHI2 > {0[Hc_BPVVDCHI2_MIN]})'
            ' & (BPVLTIME(9)*c_light > {0[Hc_BPVCTAU_MIN]})'
            ' & (M < {0[Comb_MASS_MAX]})'
#            ' & (BPVIPCHI2() < {0[Hc_BPVIPCHI2_MAX]})'
#            ' & (PT > {0[Hc_PT_MIN]})'
        ).format(self.config)

        _Hc = CombineParticles(
            name='Combine{0}'.format(name),
            DecayDescriptors=decDescriptors,
            CombinationCut=combCuts,
            MotherCut=HcCuts
        )

        return Selection(name, Algorithm=_Hc, RequiredSelections=inputSel)

    def makeHs2PPX(self, name, inputSel, decDescriptors, mipchi2_cut, vdchi2_cut):
        """Return a Selection instance for a Hs -> p+ p+ X decay.

        Keyword arguments:
        name -- Name to give the Selection instance
        inputSel -- List of inputs passed to Selection.RequiredSelections
        decDescriptors -- List of decay descriptors for CombineParticles
        """

        combCuts = (
            '(AMAXCHILD(PT) > {0[HsDaug_1ofAll_PT_MIN]})'
            ' & (AMAXCHILD(PROBNNp) > {0[HsProton_1ofAll_ProbNN_MIN]})'
            ' & (AMAXCHILD(BPVIPCHI2()) > {1})'
            " & (ACUTDOCACHI2({0[HsComb_ADOCACHI2_MAX]}, ''))"
        ).format(self.config,mipchi2_cut)

        HsCuts = (
            '(VFASPF(VCHI2/VDOF) < {0[Hs_VCHI2VDOF_MAX]})'
            ' & (BPVVDCHI2 > {1})'
            ' & (BPVLTIME(9)*c_light > {0[Hs_BPVCTAU_MIN]})'
            ' & (M < {0[HsComb_MASS_MAX]})'
#            ' & (BPVIPCHI2() < {0[Hs_BPVIPCHI2_MAX]})'
#            ' & (PT > {0[Hs_PT_MIN]})'
        ).format(self.config,vdchi2_cut)

        _Hs = CombineParticles(
            name='Combine{0}'.format(name),
            DecayDescriptors=decDescriptors,
            CombinationCut=combCuts,
            MotherCut=HsCuts
        )

        return Selection(name, Algorithm=_Hs, RequiredSelections=inputSel)
