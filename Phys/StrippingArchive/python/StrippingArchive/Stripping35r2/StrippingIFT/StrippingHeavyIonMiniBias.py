###############################################################################
# (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
'''
Stripping selections or Minimum Bias physics.
Based on those by Patrick K.
'''

__author__ = 'Yanxi ZHANG, Emilie MAURICE'
__date__ = '06/09/2017'
__version__ = '$Revision: 0 $'


__all__ = (
    'HeavyIonMiniBiasConf',
    'default_config'
    )

default_config =  {
    'NAME'            : 'HeavyIonMiniBias',
    'WGs'             : ['IFT'],
    "STREAMS": {
    "MiniBias" : ['StrippingMBHighMult'],
    'IFT'      : ['StrippingMBMicroBias']
                  },
    'BUILDERTYPE'     : 'HeavyIonMiniBiasConf',
    'CONFIG'          : {
        "odin": ["NoBeam","Beam1","Beam2","BeamCrossing"],
        "odinSMOG": ["Beam1","Beam2"],
        'CheckPV'    :  False,
        

        'MicroBiasPrescale'            :  1.0,
        'MicroBiasPostscale'           :  0.2, 
        # Do PbPb or PbAr 
        "MicroBiasHlt1Filter"         : "(HLT_PASS('Hlt1BBMicroBiasVeloDecision'))|(HLT_PASS('Hlt1BBHighMultDecision'))", 
        "MicroBiasHlt2Filter"         : None,
                                                                                                   
        "GEC_LowMult"       : "( recSummaryTrack(LHCb.RecSummary.nVeloTracks, TrVELO) > 1) & ( recSummary(LHCb.RecSummary.nVeloClusters, 'Raw/Velo/Clusters') < 20000)",
        "GEC_HighMult"       : "( recSummaryTrack(LHCb.RecSummary.nVeloClusters, 'Raw/Velo/Clusters') >= 20000) ",

        #Only PbAr trigger available for LowMult MicroBias
        #        'MicroBiasLowMultPrescale'            :  1.0,
        # 'MicroBiasLowMultPostscale'           :  1.0, 
        #"MicroBiasLowMultHlt1Filter"         : "(HLT_PASS('Hlt1BBMicroBiasLowMultVeloDecision'))", #(HLT_PASS('Hlt1MB.*Decision')) #Hlt1MBMicroBiasVelo
        #"MicroBiasLowMultHlt2Filter"         : None,

        'MicroBiasHighMultPrescale'            :  1.0,
        'MicroBiasHighMultPostscale'           :  1.0, 
        "MicroBiasHighMultHlt1Filter"         : "(HLT_PASS('Hlt1BBVeryHighMultDecision'))"

        }
    }

from StrippingConf.StrippingLine import StrippingLine
from StrippingUtils.Utils import LineBuilder
from PhysSelPython.Wrappers import Selection, DataOnDemand

class HeavyIonMiniBiasConf(LineBuilder):
    
    __configuration_keys__ = default_config['CONFIG'].keys()

    
    def __init__(self, name, config ): 
        
        LineBuilder.__init__(self, name, config)
        self.name = name 
        self.config = config
        #print "inside MiniBias",config
        odin = "|".join( ["(ODIN_BXTYP == LHCb.ODIN.%s)"%(odin_type,) for odin_type in config['odin'] if odin_type in ["NoBeam","Beam1","Beam2","BeamCrossing"]])      

        _filter1 = {'Code': config['GEC_LowMult'], 'Preambulo' : ["from LoKiTracks.decorators import *" ,
                                                                  "from LoKiCore.functions    import * ",
                                                                  "from GaudiKernel.SystemOfUnits import *"]}
        
        self.MicroBiasLine = StrippingLine( 
	      name = 'MBMicroBias',
              prescale  = self.config['MicroBiasPrescale'],
              postscale  = self.config['MicroBiasPostscale'],                 
              FILTER = _filter1,
              HLT1       =self.config['MicroBiasHlt1Filter'],
              HLT2       =self.config['MicroBiasHlt2Filter'],
              checkPV   = self.config['CheckPV'],
              RequiredRawEvents = ["Trigger", "Muon", "Calo", "Rich", "Velo", "Tracker", "HC"],
              ODIN      = odin
              )
        self.registerLine( self.MicroBiasLine )

        self.MicroBiasHighMultLine = StrippingLine(
            name = 'MBHighMult',
            prescale  = self.config['MicroBiasHighMultPrescale'],
            postscale  = self.config['MicroBiasHighMultPostscale'],
            HLT1       =self.config['MicroBiasHighMultHlt1Filter'],
            HLT2       =self.config['MicroBiasHlt2Filter'],
            checkPV   = self.config['CheckPV'],
            RequiredRawEvents = ["Trigger", "Muon", "Calo", "Rich", "Velo", "Tracker", "HC"],
            ODIN      = odin
            )
        self.registerLine( self.MicroBiasHighMultLine )


        """
        self.MicroBiasLowMultLine = StrippingLine( 
	      name = 'MicroBiasLowMult',
              prescale  = self.config['MicroBiasLowMultPrescale'],
              postscale  = self.config['MicroBiasLowMultPostscale'],                             
              HLT1       =self.config['MicroBiasLowMultHlt1Filter'],
              HLT2       =self.config['MicroBiasLowMultHlt2Filter'],
              checkPV   = self.config['CheckPV'],
              RequiredRawEvents = ["Trigger", "Muon", "Calo", "Rich", "Velo", "Tracker", "HC"],
              ODIN      = odin
              )
        self.registerLine( self.MicroBiasLowMultLine )
        """
