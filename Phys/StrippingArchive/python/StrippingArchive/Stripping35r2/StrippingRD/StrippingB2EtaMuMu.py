###############################################################################
# (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
'''
B->etamumuselection
'''

__author__ = ['Fred Blanc']
__date__ = '26/01/2019'
__version__ = '$Revision: 1.0 $'

__all__ = ( 'B2EtaMuMu',
            'makeLightResList',
            'makeDiMuonList',
            'makeB2EtaMuMuAllX0',
            'default_config')

name = 'B2EtaMuMu'

default_config = {
    'NAME'        : 'B2EtaMuMu',
    'WGs'         : ['RD'],
    'BUILDERTYPE' : 'B2EtaMuMu',
    'CONFIG'      : { 'B2EMMPrescale'     : 1.0,

                      # Photon cuts
                      'Photon_Res_PT_Min' : 400.0,
                      'Photon_CL_Min'     : 0.2,

                      # Pi0 cuts
                      'Pi0_Res_PT_Min'  : 600.,  # Pt cut on a pi0 with is grandaughter of the B

                      # Track cuts
                      'EMMTrkGhostProb' : 0.5,
                      'EMMTrkMinIPChi2' : 9.,
                      'EMMTrkMinPT'     : 500,

                      # Resonance cuts
                      'EMMResMinPT'     : 600,
                      'EMMResMaxMass'   : 1100.,
                      'EMMResVtxChi2DOF': 9.,

                      # Muon cuts
                      'MuonPID': -3.0,

                      # Dimuon cuts
                      'EMMDimuon_MaxMass': 6100., 

                      # B cuts
                      'EMMBMinPT'       : 2500.,
                      'EMMBMinMass'     : 4900.,
                      'EMMBMaxMass'     : 6000.,
                      'EMMBDIRA'        : 0.995,
                      'EMMBVtxChi2DOF'  : 9.
                      },
    'STREAMS'     : ['Bhadron']     ## This stream puts it into mDST
    }


from Gaudi.Configuration import *
from GaudiConfUtils.ConfigurableGenerators import FilterDesktop, CombineParticles
from PhysSelPython.Wrappers import Selection, DataOnDemand, SimpleSelection, MergedSelection
from StrippingConf.StrippingLine import StrippingLine
from StrippingUtils.Utils import LineBuilder
from StandardParticles import StdNoPIDsPions, StdLooseAllPhotons, StdLooseMergedPi0, StdLooseResolvedPi0
from StandardParticles import StdAllNoPIDsMuons, StdAllLooseMuons

class B2EtaMuMu(LineBuilder) :
    """
    Builder for B2EtaMuMu
    """

    __configuration_keys__ = default_config['CONFIG'].keys()

    def __init__(self, name, config) :
        self.name = name
        LineBuilder.__init__(self, name, config)

        cuts = default_config['CONFIG']

        ## Single particle cuts definition
        trackCuts = "(MIPCHI2DV(PRIMARY) > %(EMMTrkMinIPChi2)s) & (TRGHOSTPROB < %(EMMTrkGhostProb)s) & (PT > %(EMMTrkMinPT)s)" % cuts
        gamma_for_Res_Cuts = "(PT > %(Photon_Res_PT_Min)s*MeV) & (CL > %(Photon_CL_Min)s)" % cuts
        pi0_for_Res_Cuts = "(PT > %(Pi0_Res_PT_Min)s*MeV)" % cuts
        muonCuts = trackCuts + " & (PIDmu> %(MuonPID)s)" % cuts

        self.TrackList = SimpleSelection( 'TrackList' + self.name,
                                          FilterDesktop,
                                          [StdNoPIDsPions],
                                          Code = trackCuts
                                          )

        self.Gamma_for_Res_List = SimpleSelection( 'Gamma_for_Res_List' + self.name,
                                                   FilterDesktop,
                                                   [StdLooseAllPhotons],
                                                   Code = gamma_for_Res_Cuts
                                                   )

        self.Pi0_for_Res_List = SimpleSelection( 'Pi0_for_Res_List' + self.name,
                                                 FilterDesktop,
                                                 [StdLooseResolvedPi0, StdLooseMergedPi0],
                                                 Code = pi0_for_Res_Cuts
                                                 )

        self.LightResList = makeLightResList( name="DiTracksForB2EtaMuMu" + self.name,
                                              trkList=self.TrackList,
                                              gammaList=self.Gamma_for_Res_List,
                                              pi0List=self.Pi0_for_Res_List,
                                              MinPTCut = config['EMMResMinPT'],
                                              MaxMassCut = config['EMMResMaxMass'],
                                              VtxChi2DOFCut = config['EMMResVtxChi2DOF'] )

        self.MuonList = SimpleSelection( 'Muon_for_DiMuon_List' + self.name,
                                         FilterDesktop,
                                         [StdAllNoPIDsMuons],
                                         #[StdAllLooseMuons],
                                         Code = muonCuts
                                         )

        self.DiMuonList = makeDiMuonList( name="DiMuonForB2EtaMuMu" + self.name,
                                          muList = self.MuonList,
                                          MinPTCut = config['EMMResMinPT'],
                                          MaxMassCut = config['EMMDimuon_MaxMass'],
                                          VtxChi2DOFCut = config['EMMResVtxChi2DOF'] )

        self.B2EtaMuMuAllX0 = makeB2EtaMuMuAllX0( self.name + "AllX0Selection",
                                                  diTrkList     = self.LightResList,
                                                  diMuList      = self.DiMuonList,
                                                  MinPTCut      = config['EMMBMinPT'],
                                                  MinMassCut    = config['EMMBMinMass'],
                                                  MaxMassCut    = config['EMMBMaxMass'],
                                                  DIRACut       = config['EMMBDIRA'],
                                                  VtxChi2DOFCut = config['EMMBVtxChi2DOF']
                                                  )

        self.B2EtaMuMuAllX0Line = StrippingLine( self.name + "AllX0SelectionLine",
                                                 prescale = config['B2EMMPrescale'],
                                                 selection = tisTosSelection(self.B2EtaMuMuAllX0),
                                                 RelatedInfoTools = getRelInfoB2EtaMuMu()
                                                 )


        self.registerLine(self.B2EtaMuMuAllX0Line)

def makeLightResList( name,
                     trkList,
                     gammaList,
                     pi0List,
                     MinPTCut,
                     MaxMassCut,
                     VtxChi2DOFCut ) :
    """
    Di-track or di-track + neutral selection
    """
    _diTrackPreVertexCuts = "(APT> %(MinPTCut)s) & (AM< %(MaxMassCut)s)" %locals()
    _diTrackPostVertexCuts = "(VFASPF(VCHI2/VDOF) < %(VtxChi2DOFCut)s)" %locals()

#    PiPi = SimpleSelection(name+"_2body", CombineParticles,
#                           [ trkList ],
#                           DecayDescriptors=["rho(770)0 -> pi+ pi-"],
#                           CombinationCut = _diTrackPreVertexCuts,
#                           MotherCut = _diTrackPostVertexCuts
#                           )

    PiPiGamma = SimpleSelection(name+"_gamma", CombineParticles,
                           [ trkList, gammaList ],
                           DecayDescriptors=["rho(770)0 -> pi+ pi- gamma"],
                           CombinationCut = _diTrackPreVertexCuts,
                           MotherCut = _diTrackPostVertexCuts
                           )
    
    PiPiPi0 = SimpleSelection(name+"_pi0", CombineParticles,
                           [ trkList, pi0List ],
                           DecayDescriptors=["rho(770)0 -> pi+ pi- pi0"],
                           CombinationCut = _diTrackPreVertexCuts,
                           MotherCut = _diTrackPostVertexCuts
                           )

#    return MergedSelection( "Merge" + name, RequiredSelections =  [ PiPi, PiPiGamma, PiPiPi0 ] )
    return MergedSelection( "Merge" + name, RequiredSelections =  [ PiPiGamma, PiPiPi0 ] )


def makeDiMuonList(name,
                   muList,
                   MinPTCut,
                   MaxMassCut,
                   VtxChi2DOFCut) :

        _DiMuonPreVertexCuts = "(APT> %(MinPTCut)s) & (AM< %(MaxMassCut)s)" %locals()
        _DiMuonPostVertexCuts = "(VFASPF(VCHI2/VDOF) < %(VtxChi2DOFCut)s)" %locals()

        return SimpleSelection(name,
                               CombineParticles,
                               [ muList ],
                               DecayDescriptors = ["J/psi(1S) -> mu+ mu-"],
                               MotherCut = _DiMuonPostVertexCuts,
                               CombinationCut = _DiMuonPreVertexCuts
                               )


def makeB2EtaMuMuAllX0( name,
                        diTrkList,
                        diMuList,
                        MinPTCut,
                        MinMassCut,
                        MaxMassCut,
                        DIRACut,
                        VtxChi2DOFCut ) :
    """
    B to eta(')mumu selection
    """

    _B2EtaMuMuPreVertexCuts = "in_range( %(MinMassCut)s ,AM, %(MaxMassCut)s )" %locals()
    _B2EtaMuMuPreVertexCuts += " & ( APT > %(MinPTCut)s )" %locals()

    _B2EtaMuMuPostVertexCuts = "(VFASPF(VCHI2/VDOF) < %(VtxChi2DOFCut)s) & (BPVDIRA > %(DIRACut)s)" %locals()

    return SimpleSelection(name, CombineParticles, [ diTrkList, diMuList ]
                           , DecayDescriptor = "B0 -> rho(770)0 J/psi(1S)"
                           , MotherCut = _B2EtaMuMuPostVertexCuts
                           , CombinationCut = _B2EtaMuMuPreVertexCuts
                           )



def getRelInfoB2EtaMuMu():
    relInfo = []
    for coneAngle in [0.8,1.0,1.3,1.7]:
        conestr = str(coneAngle).replace('.','')
        relInfo += [
        { "Type"         : "RelInfoConeVariables",
             "IgnoreUnmatchedDescriptors" : True,
             "ConeAngle"    : coneAngle,
             "Variables"    : ['CONEANGLE', 'CONEMULT', 'CONEPASYM', 'CONEPTASYM', 'CONEDELTAETA'],
             "DaughterLocations" : {
                "^[B0 -> (rho(770)0 -> pi+ pi- {X0}) (J/psi(1S) -> mu- mu+) ]CC" : 'P2ConeVar%s_B' % conestr,
                "[B0 -> ^(rho(770)0 -> pi+ pi- {X0}) (J/psi(1S) -> mu- mu+) ]CC" : 'P2ConeVar%s_X1' % conestr,
                "[B0 -> (rho(770)0 -> pi+ pi- {X0}) ^(J/psi(1S) -> mu- mu+) ]CC" : 'P2ConeVar%s_X2' % conestr,
                "[B0 -> (rho(770)0 -> ^pi+ pi- {X0}) (J/psi(1S) -> mu- mu+) ]CC" : 'P2ConeVar%s_X11' % conestr,
                "[B0 -> (rho(770)0 -> pi+ ^pi- {X0}) (J/psi(1S) -> mu- mu+) ]CC" : 'P2ConeVar%s_X12' % conestr,
                "[B0 -> (rho(770)0 -> pi+ pi- {X0}) (J/psi(1S) -> ^mu- mu+) ]CC" : 'P2ConeVar%s_X21' % conestr,
                "[B0 -> (rho(770)0 -> pi+ pi- {X0}) (J/psi(1S) -> mu- ^mu+) ]CC" : 'P2ConeVar%s_X22' % conestr
         } }       ]
    relInfo += [ { "Type" : "RelInfoVertexIsolation", "Location": "VertexIsoInfo" } ]
    return relInfo


def makeTISTOSFilter(name):
    specs = {'Hlt1(Two)?Track.*Decision%TOS':0}
    from Configurables import TisTosParticleTagger
    tisTosFilter = TisTosParticleTagger(name+'TISTOSFilter')
    tisTosFilter.TisTosSpecs = specs
    tisTosFilter.ProjectTracksToCalo = False
    tisTosFilter.CaloClustForCharged = False
    tisTosFilter.CaloClustForNeutral = False
    tisTosFilter.TOSFrac = {4:0.0, 5:0.0}
    return tisTosFilter

def tisTosSelection(sel):
    '''Filters Selection sel to be TOS OR TIS.'''
    tisTosFilter = makeTISTOSFilter(sel.name())
    return Selection(sel.name()+'TISTOS', Algorithm=tisTosFilter, RequiredSelections=[sel])


