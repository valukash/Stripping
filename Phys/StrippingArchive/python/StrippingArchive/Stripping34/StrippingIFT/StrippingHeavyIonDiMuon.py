###############################################################################
# (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
'''
Module for selecting J/psi, Psi(2S), Upsilon, Drell-Yan
Based on those by Jibo, developped by Yanxi ZHANG (version 1.0)
'''

__author__=['Emilie MAURICE']
__date__ = ''
__version__= '$Revision: 2.0 $'


__all__ = (
    'HeavyIonDiMuonConf',
    'default_config'
    )

default_config =  {
    'NAME'            : 'HeavyIonDiMuon',
    'BUILDERTYPE'     : 'HeavyIonDiMuonConf',
    'WGs'             : ['IFT'],
    'STREAMS'         : ['IFT'],
    'CONFIG'          : {
        "odin": ["NoBeam","Beam1","Beam2","BeamCrossing"],
        'CheckPV'    :  False,
        "VCHI2VDOF_max"    : 25,
        #       "Track_CHI2"       : 3,
        "Track_CHI2"       : 10,
        #        "DOCA_max"         : 0.5,
        "DOCA_max"         : 2.,
        #        "PTmu_min"         : 700,   # MeV
        "PTmu_min"         : 500,   # MeV
        "AM"               : 2900,   # MeV
        "AM_4000MeV"       : 4000,   # MeV
        "AM_8500MeV"       : 8500,   # MeV
        "AMDY"             : 2500,   # MeV
        "ProbNNmu_min"     : 0.5, 
        'PrescaleHighMass'            :  1.0,
        'PostscaleHighMass'           : 1.0, 
        'PrescaleLowMass'            :  1.0,
        'PostscaleLowMass'           : 1.0, 
        'PrescaleSameSign'            :  1.0,
        'PostscaleSameSign'           : 1.0, 
        'PrescaleDY'            :  1.0,
        'PostscaleDY'           : 1.0, 
        'Hlt1FilterHighMass'          : None,
        'Hlt2FilterHighMass'          : None,
        'Hlt1FilterLowMass'           : None,     
        'Hlt2FilterLowMass'           : None,
        'Hlt1FilterSameSign'          : None, 
        'Hlt2FilterSameSign'          : None,
        'Hlt1FilterDY'           : None,
        #        'Hlt2FilterDY1'          : "HLT_PASS_RE('Hlt2EWDiMuonDY1Decision')",
        'Hlt2FilterDY1'          : None,
        #        'Hlt2FilterDY2'          : "HLT_PASS_RE('Hlt2EWDiMuonDY2Decision')",
        'Hlt2FilterDY2'          : None,
        #        'Hlt2FilterDY3'          : "HLT_PASS_RE('Hlt2EWDiMuonDY3Decision')",
        'Hlt2FilterDY3'          : None,
        #        'Hlt2FilterDY4'          : "HLT_PASS_RE('Hlt2EWDiMuonDY4Decision')",
        'Hlt2FilterDY4'          : None,
        #        'Hlt2FilterDYSS'         : "HLT_PASS_RE('Hlt2EWDiMuonDYSSDecision')"
        'Hlt2FilterDYSS'         : None
        }
    }

from Gaudi.Configuration import *
from PhysSelPython.Wrappers import Selection, DataOnDemand
from StrippingConf.StrippingLine import StrippingLine
from StrippingUtils.Utils import LineBuilder
from StandardParticles import StdAllLooseMuons
from GaudiConfUtils.ConfigurableGenerators import CombineParticles

class HeavyIonDiMuonConf(LineBuilder):
    
    __configuration_keys__ = default_config['CONFIG'].keys()

    
    def __init__(self, name, config ): 
        
        LineBuilder.__init__(self, name, config)
        self.name = name 
        self.config = config
        self.inMuons = StdAllLooseMuons

        _monCuts = ( "(VFASPF(VCHI2/VDOF)<{0[VCHI2VDOF_max]})").format(self.config)

        #       _dauCuts = {"mu+":("(PT>{0[PTmu_min]}) & (TRCHI2DOF<{0[Track_CHI2]})").format(self.config)}
        # _dauCuts = {"mu+":("(PT>{0[PTmu_min]}) & (TRCHI2DOF<{0[Track_CHI2]}) & (PROBNNmu > {0[ProbNNmu_min]})").format(self.config)}
        _dauCuts = {"mu+":("(PT>{0[PTmu_min]}) & (TRCHI2DOF<{0[Track_CHI2]})").format(self.config)}

        _comCutsHighMass = ( 
              "( AM > {0[AM]})"
              "&( (AM < {0[AM_4000MeV]}) | (AM > {0[AM_8500MeV]}))"
              # "&(ACUTDOCA({0[DOCA_max]}, ''))"
              ).format(self.config)
        self.makeJpsi = self.makeDiMuon(
              name = "Jpsi2MuMu",
              daughterCuts = _dauCuts,
              motherCuts = _monCuts,
              combCuts = _comCutsHighMass
              )

        _comCutsHighMass_SameSign = ( 
              "(ACUTDOCA({0[DOCA_max]}, ''))"
              ).format(self.config)
        self.makeDiMuonSameSign = self.makeDiMuonSameSign(
              name = "DiMuMuSameSign",
              daughterCuts = _dauCuts,
              motherCuts = _monCuts,
              combCuts = _comCutsHighMass
              )

        _comCutsLowMass = ( 
              "(AM < {0[AM]})"
              #              "&(PROBNNmu > {0[ProbNNmu_min]}, ''))"
              ).format(self.config)
        self.makeDiMuonLow = self.makeDiMuon(
              name = "LowDiMuon",
              daughterCuts = _dauCuts,
              motherCuts = _monCuts,
              combCuts = _comCutsLowMass
              )


        _comCutsDY = ( 
            "(AM > {0[AMDY]})"
            ).format(self.config)

        self.makeDY = self.makeDYMuMu(
            name = "DY",
            daughterCuts = _dauCuts,
            motherCuts = _monCuts,
            combCuts = _comCutsDY
            )

        self.makeDYSS = self.makeDYMuMuSS(
            name = "DYSS",
            daughterCuts = _dauCuts,
            motherCuts = _monCuts,
            combCuts = _comCutsDY
            )
        
        odin = "|".join( ["(ODIN_BXTYP == LHCb.ODIN.%s)"%(odin_type,) for odin_type in self.config['odin'] if odin_type in ["NoBeam","Beam1","Beam2","BeamCrossing"]])

        self.JpsiLine = StrippingLine( name+'Jpsi2MuMuLine',
              prescale  = self.config['PrescaleHighMass'],
              postscale  = self.config['PostscaleHighMass'],                                                                      
              checkPV   = self.config['CheckPV'],
              HLT1       =self.config['Hlt1FilterHighMass'],
              HLT2       =self.config['Hlt2FilterHighMass'],
              algos     = [ self.makeJpsi],
              ODIN      = odin,
              RequiredRawEvents = ["Trigger", "Muon", "Calo", "Rich", "Velo", "Tracker", "HC"]
              )
        self.registerLine( self.JpsiLine )

        self.SameSignDiMuonLine = StrippingLine( name+'SameSignDiMuonLine',
              prescale  = self.config['PrescaleSameSign'],
              postscale  = self.config['PostscaleSameSign'],                                                                      
              checkPV   = self.config['CheckPV'],
              HLT1       =self.config['Hlt1FilterSameSign'],
              HLT2       =self.config['Hlt2FilterSameSign'],
              algos     = [ self.makeDiMuonSameSign],
              ODIN      = odin,
              RequiredRawEvents = ["Trigger", "Muon", "Calo", "Rich", "Velo", "Tracker", "HC"]
              )
        self.registerLine( self.SameSignDiMuonLine )

        self.LowDiMuonLine = StrippingLine( name+'LowDiMuonLine',
              prescale  = self.config['PrescaleLowMass'],
              postscale  = self.config['PostscaleLowMass'],                                                                      
              checkPV   = self.config['CheckPV'],
              HLT1       =self.config['Hlt1FilterLowMass'],
              HLT2       =self.config['Hlt2FilterLowMass'],
              algos     = [ self.makeDiMuonLow],
              ODIN      = odin,           
              RequiredRawEvents = ["Trigger", "Muon", "Calo", "Rich", "Velo", "Tracker", "HC"]
              )
        self.registerLine( self.LowDiMuonLine )

        self.DY1Line = StrippingLine( name+'DY1Line',
              prescale  = self.config['PrescaleDY'],
              postscale  = self.config['PostscaleDY'],                                                                      
              checkPV   = self.config['CheckPV'],
              HLT1       =self.config['Hlt1FilterDY'],
              HLT2       =self.config['Hlt2FilterDY1'],
              algos     = [ self.makeDY],
              ODIN      = odin,
              RequiredRawEvents = ["Trigger", "Muon", "Calo", "Rich", "Velo", "Tracker", "HC"]
              )
        self.registerLine( self.DY1Line )


        self.DY2Line = StrippingLine( name+'DY2Line',
              prescale  = self.config['PrescaleDY'],
              postscale  = self.config['PostscaleDY'],                                                                      
              checkPV   = self.config['CheckPV'],
              HLT1       =self.config['Hlt1FilterDY'],
              HLT2       =self.config['Hlt2FilterDY2'],
              algos     = [ self.makeDY],
              ODIN      = odin,
              RequiredRawEvents = ["Trigger", "Muon", "Calo", "Rich", "Velo", "Tracker", "HC"]
              )
        self.registerLine( self.DY2Line )


        self.DY3Line = StrippingLine( name+'DY3Line',
              prescale  = self.config['PrescaleDY'],
              postscale  = self.config['PostscaleDY'],                                                                      
              checkPV   = self.config['CheckPV'],
              HLT1       =self.config['Hlt1FilterDY'],
              HLT2       =self.config['Hlt2FilterDY3'],
              algos     = [ self.makeDY],
              ODIN      = odin,
              RequiredRawEvents = ["Trigger", "Muon", "Calo", "Rich", "Velo", "Tracker", "HC"]
              )
        self.registerLine( self.DY3Line )

        self.DY4Line = StrippingLine( name+'DY4Line',
              prescale  = self.config['PrescaleDY'],
              postscale  = self.config['PostscaleDY'],                                                                      
              checkPV   = self.config['CheckPV'],
              HLT1       =self.config['Hlt1FilterDY'],
              HLT2       =self.config['Hlt2FilterDY4'],
              algos     = [ self.makeDY],
              ODIN      = odin,
              RequiredRawEvents = ["Trigger", "Muon", "Calo", "Rich", "Velo", "Tracker", "HC"]
              )
        self.registerLine( self.DY4Line )


        self.DYSSLine = StrippingLine( name+'DYSSLine',
              prescale  = self.config['PrescaleDY'],
              postscale  = self.config['PostscaleDY'],                                                                      
              checkPV   = self.config['CheckPV'],
              HLT1       =self.config['Hlt1FilterDY'],
              HLT2       =self.config['Hlt2FilterDYSS'],
              algos     = [ self.makeDYSS],
              ODIN      = odin,
              RequiredRawEvents = ["Trigger", "Muon", "Calo", "Rich", "Velo", "Tracker", "HC"]
              )
        self.registerLine( self.DYSSLine )

    def makeDiMuon( self,name, daughterCuts, motherCuts, combCuts) :
        '''create a selection using a CombineParticles'''
        diMuon = CombineParticles(
              #name               = 'Combine{0}'.format(name),
              DecayDescriptors   = ["J/psi(1S) -> mu+ mu-"],
              CombinationCut     = combCuts,
              MotherCut          = motherCuts,
              DaughtersCuts       = daughterCuts,
              )
        return Selection( name,
                          Algorithm = diMuon,
                          RequiredSelections = [ self.inMuons] )


    def makeDiMuonSameSign( self,name, daughterCuts, motherCuts, combCuts) :
        '''create a selection using a CombineParticles'''
        diMuonSS = CombineParticles(
            #name               = 'Combine{0}'.format(name),
            DecayDescriptors   = ["J/psi(1S) -> mu+ mu+"],
            CombinationCut     = combCuts,
            MotherCut          = motherCuts,
            DaughtersCuts       = daughterCuts    
        )
        return Selection( name,
                          Algorithm = diMuonSS,
                          RequiredSelections = [ self.inMuons] )
                          
    
    def makeDYMuMu( self,name, daughterCuts, motherCuts, combCuts) :
        '''create a selection using a CombineParticles'''
        DYmumu = CombineParticles(
            #name               = 'Combine{0}'.format(name),
            DecayDescriptors   = ["Z0 -> mu+ mu-"],
            CombinationCut     = combCuts,
            MotherCut          = motherCuts,
            DaughtersCuts       = daughterCuts,
            )
        return Selection( name,
                          Algorithm = DYmumu,
                          RequiredSelections = [ self.inMuons] )


    def makeDYMuMuSS( self,name, daughterCuts, motherCuts, combCuts) :
        '''create a selection using a CombineParticles'''
        DYmumu = CombineParticles(
            #name               = 'Combine{0}'.format(name),
            DecayDescriptors   = ["Z0 -> mu+ mu+"],
            CombinationCut     = combCuts,
            MotherCut          = motherCuts,
            DaughtersCuts       = daughterCuts,
            )
        return Selection( name,
                          Algorithm = DYmumu,
                          RequiredSelections = [ self.inMuons] )
