###############################################################################
# (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
'''
Module for selecting charmonium -> phi phi pi pi 
'''

__author__=['Andrii Usachov']
__date__ = '10/04/2017'
__version__= '$Revision: 1.1 $'



__all__ = (
    'Ccbar2PhiPhiPiPiConf',
    'default_config'
    )

default_config = {
    'NAME'              :  'Ccbar2PhiPhiPiPi',
    'BUILDERTYPE'       :  'Ccbar2PhiPhiPiPiConf',
    'CONFIG'    : {
        'PionCuts'      : "(PROBNNpi > 0.1) & (PT > 100*MeV) & (MIPCHI2DV(PRIMARY)>4.)",
        'EtacComN4Cuts' : """
                          (AM > 2.7 *GeV)
                          """,  
        'EtacMomN4Cuts' : "(VFASPF(VCHI2/VDOF) < 16.) & (MM>2.7*GeV) & (BPVDLS>3)",
        'Prescale'         :     1.,     
        'TRCHI2DOF'        :     5.,
        'KaonProbNNk'      :    0.1,
        'PhiMassW'         :    30.,
        'PhiVtxChi2'       :    25.,
        'KaonPT'           :    100,
        'KaonIPCHI2'       :      4.
        },
    'STREAMS'           : ['Charm' ],
    'WGs'               : ['BandQ'],
    }



from Gaudi.Configuration import *
from GaudiConfUtils.ConfigurableGenerators import FilterDesktop, CombineParticles 
from PhysSelPython.Wrappers import Selection, DataOnDemand
from StrippingConf.StrippingLine import StrippingLine
from StrippingUtils.Utils import LineBuilder
from GaudiConfUtils.ConfigurableGenerators import DaVinci__N4BodyDecays

class Ccbar2PhiPhiPiPiConf(LineBuilder):
    
    __configuration_keys__ = default_config['CONFIG'].keys()

    def __init__(self, name, config ): 
        
        LineBuilder.__init__(self, name, config)
        self.name = name 
        self.config = config

        PhiVtxChi2 =config['PhiVtxChi2']
        PhiMassW   =config['PhiMassW']
        KaonPT     =config['KaonPT']
        TRCHI2DOF  =config['TRCHI2DOF']
        KaonProbNNk=config['KaonProbNNk']
        KaonIPCHI2 =config['KaonIPCHI2'] 

        self.SelPions = self.createSubSel( OutputList = self.name + "SelPions",
                                           InputList =  DataOnDemand(Location = 'Phys/StdLoosePions/Particles' ), 
                                           Cuts = config['PionCuts']
                                           )


        self.PhiForJpsiList = self.createSubSel( OutputList = "PhiFor" + self.name,
                                                 InputList =  DataOnDemand( Location = 'Phys/StdLooseDetachedPhi2KK/Particles' ), 
                                                 Cuts = "(VFASPF(VCHI2/VDOF)<%(PhiVtxChi2)s)"\
                                                         " & (ADMASS('phi(1020)')<%(PhiMassW)s*MeV )"\
                                                         " & (MAXTREE('K+'==ABSID, TRCHI2DOF) < %(TRCHI2DOF)s )" \
                                                         " & (MINTREE('K+'==ABSID, PROBNNk) > %(KaonProbNNk)s )"\
                                                         " & (MINTREE('K+'==ABSID, MIPCHI2DV(PRIMARY)) > %(KaonIPCHI2)s )"\
                                                         % self.config)


        """
        J/psi -> Phi Phi Pi Pi
        """
        self.SelCcbar2PhiPhiPiPi = self.createN4BodySel( OutputList = self.name + "SelCcbar2PhiPhiPiPi",
                                                     DaughterLists = [ self.PhiForJpsiList, self.SelPions ],
                                                     DecayDescriptor = "J/psi(1S) -> phi(1020) phi(1020) pi+ pi-",
                                                     PreVertexCuts  = config['EtacComN4Cuts'], 
                                                     PostVertexCuts = config['EtacMomN4Cuts']
                                                     )
        
        self.Ccbar2PhiPhiPiPiLine = StrippingLine( self.name + 'Line',                                                
                                               prescale  = config['Prescale'],
                                               algos     = [ self.SelCcbar2PhiPhiPiPi ],
                                               MDSTFlag  = False
                                               )
        self.registerLine( self.Ccbar2PhiPhiPiPiLine )
        
    def createSubSel( self, OutputList, InputList, Cuts ) :
        '''create a selection using a FilterDesktop'''
        filter = FilterDesktop(Code = Cuts)
        return Selection( OutputList,
                          Algorithm = filter,
                          RequiredSelections = [ InputList ] )
    
    def createN4BodySel( self, OutputList,
                         DecayDescriptor,
                         DaughterLists,
                         DaughterCuts = {} ,
                         ComAMCuts      = "AALL",
                         PreVertexCuts  = "AALL",
                         PostVertexCuts = "ALL" ) :
        '''create a selection using a ParticleCombiner with a single decay descriptor'''
        combiner = DaVinci__N4BodyDecays ( DecayDescriptor = DecayDescriptor,      
                                           DaughtersCuts = DaughterCuts,
                                           Combination12Cut  = ComAMCuts + "&" + "( ACHI2DOCA(1,2)<20 )",
                                           Combination123Cut = ComAMCuts + "&" + "( ACHI2DOCA(1,3)<20 ) & ( ACHI2DOCA(2,3)<20 )",
                                           CombinationCut = "( ACHI2DOCA(1,4)<20 ) & ( ACHI2DOCA(2,4)<20 ) & ( ACHI2DOCA(3,4)<20 )" + " & " + PreVertexCuts,
                                           MotherCut = PostVertexCuts,
                                           ReFitPVs = False )
        return Selection ( OutputList,
                           Algorithm = combiner,
                           RequiredSelections = DaughterLists)
