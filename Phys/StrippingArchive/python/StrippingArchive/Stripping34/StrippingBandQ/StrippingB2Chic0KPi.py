###############################################################################
# (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
'''
Module for selecting B0->Chic0 K pi, B+->Chic0 K phi #yanxi
'''

__author__=['Andrii Usachov']
__date__ = '13/03/2018'
__version__= '$Revision: 1.2$'


__all__ = (
    'B2Chic0KPiConf',
    'default_config'
    )

default_config = {
    'NAME'              :  'B2Chic0KPi',
    'BUILDERTYPE'       :  'B2Chic0KPiConf',
    'CONFIG'    : {
        'KaonCuts'      : "(PROBNNk  > 0.1) & (PT > 300*MeV) & (TRGHOSTPROB<0.4)",
        'ProtonCuts'    : "(PROBNNp  > 0.1) & (PT > 300*MeV) & (P > 10*GeV) & (TRGHOSTPROB<0.4)",        
        'PionCuts'      : "(PROBNNpi > 0.1) & (PT > 300*MeV) & (TRGHOSTPROB<0.4)",
        'KaonCuts4h'    : "(PROBNNk  > 0.1) & (PROBNNpi < 0.9) & (PROBNNp < 0.9) & (PT > 300*MeV) & (TRGHOSTPROB<0.4)",
        'PionCuts4h'    : "(PROBNNpi > 0.1) & (PROBNNk < 0.9)  & (PROBNNp < 0.9) & (PT > 300*MeV) & (TRGHOSTPROB<0.4)",
        'Chic0ComAMCuts' : "(AM<4.1*GeV)", 
        'Chic0ComN4Cuts' : """
                           (in_range(3.1*GeV, AM, 3.9*GeV))
                           & ( (ACHILD(PT,1)+ACHILD(PT,2)+ACHILD(PT,3)+ACHILD(PT,4) ) > 2.0 *GeV)
                           & ( (ACHILD(MIPCHI2DV(), 1) + ACHILD(MIPCHI2DV(), 2) + ACHILD(MIPCHI2DV(), 3) + ACHILD(MIPCHI2DV(), 4))>50)
                           """,
        'Chic0MomN4Cuts' : """
                           (VFASPF(VCHI2/VDOF) < 9.) 
                           & (in_range(3.3*GeV, MM, 3.7*GeV)) 
                           & (MIPCHI2DV(PRIMARY) > 0.) 
                           & (BPVVDCHI2>10) 
                           & (BPVDIRA>0.9)
                           """,
        'Chic0ComCuts'   : "(in_range(2.7*GeV, AM, 4.2*GeV))",
        'Chic0MomN2Cuts' : """
                           (VFASPF(VCHI2/VDOF) < 9.)
                           & (in_range(2.8*GeV, MM, 4.1*GeV))
                           & (MIPCHI2DV(PRIMARY) > 0.)
                           & (BPVVDCHI2>10)
                           & (BPVDIRA>0.9)
                           """,
        'KstarComCuts': """
                        (ACHILD(PT,1)+ACHILD(PT,2) > 900.*MeV) & (AM < 4.0 *GeV) & (ADOCACHI2CUT(20., ''))
                        & ( (ACHILD(MIPCHI2DV(), 1) + ACHILD(MIPCHI2DV(), 2))>20)
                        """,
        'KstarMomCuts': """
                          (MIPCHI2DV(PRIMARY) > 0.)
                          & (BPVVDCHI2 > 10.)
                          & (VFASPF(VCHI2) < 9.)
                          & (BPVDIRA>0.9)
                        """,
        'PhiComCuts': "(ACHILD(PT,1)+ACHILD(PT,2) > 500.*MeV) & (AM>970.*MeV) &(AM < 1070 *MeV) & (ADOCACHI2CUT(20., ''))",
        'PhiMomCuts': """
                          (MIPCHI2DV(PRIMARY) > 0.)
                          & (BPVVDCHI2 > 10.)
                          & (VFASPF(VCHI2) < 9.)
                          & (BPVDIRA>0.9)
                        """,
        'BComCuts'     : "(ADAMASS('B0') < 500 *MeV)",
        'BMomCuts'     : """
                          (VFASPF(VCHI2/VDOF) < 10.)
                          & (BPVDIRA> 0.9999) 
                          & (BPVIPCHI2()<25)
                          & (BPVVDCHI2>100)
                          #& (BPVVDRHO>0.1*mm) 
                          #& (BPVVDZ>2.0*mm)
                         """,
        'Prescale'      : 1.,
        'RelatedInfoTools': [{
                      'Type'              : 'RelInfoVertexIsolation',
                      'Location'          : 'RelInfoVertexIsolation'
                  }, {
                      'Type'              : 'RelInfoConeVariables',
                      'ConeAngle'         : 1.0,
                      "Variables" : ['CONEANGLE', 'CONEMULT', 'CONEPTASYM'], 
                      'Location'          : 'RelInfoConeVariables_1.0'
                  }, {
                      'Type'              : 'RelInfoConeVariables',
                      'ConeAngle'         : 1.5,
                      "Variables" : ['CONEANGLE', 'CONEMULT', 'CONEPTASYM'], 
                      'Location'          : 'RelInfoConeVariables_1.5'
                  }, {
                      'Type'              : 'RelInfoConeVariables',
                      'ConeAngle'         : 2.0,
                      "Variables" : ['CONEANGLE', 'CONEMULT', 'CONEPTASYM'], 
                      'Location'          : 'RelInfoConeVariables_2.0'
                  }]        
        },
    'STREAMS'           : ['Bhadron'],
    'WGs'               : ['BandQ'],
    }


from Gaudi.Configuration import *
from GaudiConfUtils.ConfigurableGenerators import FilterDesktop, CombineParticles 
from PhysSelPython.Wrappers import Selection, DataOnDemand
from StrippingConf.StrippingLine import StrippingLine
from StrippingUtils.Utils import LineBuilder
#from Configurables import DaVinci__N4BodyDecays
from GaudiConfUtils.ConfigurableGenerators import DaVinci__N4BodyDecays

class B2Chic0KPiConf(LineBuilder):
    
    __configuration_keys__ = default_config['CONFIG'].keys()

    def __init__(self, name, config ): 
        
        LineBuilder.__init__(self, name, config)
        self.name = name 
        self.config = config


        self.SelKaons   = self.createSubSel( OutputList = self.name + "SelKaons",
                                           InputList =  DataOnDemand(Location = 'Phys/StdLooseKaons/Particles' ), 
                                           Cuts = config['KaonCuts']
                                           )

        self.SelPions   = self.createSubSel( OutputList = self.name + "SelPions",
                                           InputList =  DataOnDemand(Location = 'Phys/StdLoosePions/Particles' ), 
                                           Cuts = config['PionCuts']
                                           )

        self.SelPions4h = self.createSubSel( OutputList = self.name + "SelPions4h",
                                           InputList =  DataOnDemand(Location = 'Phys/StdLoosePions/Particles' ), 
                                           Cuts = config['PionCuts4h']
                                           )

        self.SelKaons4h = self.createSubSel( OutputList = self.name + "SelKaons4h",
                                           InputList =  DataOnDemand(Location = 'Phys/StdLooseKaons/Particles' ), 
                                           Cuts = config['KaonCuts4h']
                                           )

        self.SelProtons = self.createSubSel( OutputList = self.name + "SelProtons",
                                           InputList =  DataOnDemand(Location = 'Phys/StdLooseProtons/Particles' ), 
                                           Cuts = config['ProtonCuts']
                                           )

        """
        Chic0-> K K Pi Pi
        """
        self.SelChic02KKPiPi = self.createN4BodySel( OutputList = self.name + "SelChic02KKPiPi",
                                                    DaughterLists = [ self.SelKaons4h, self.SelPions4h ],
                                                    DecayDescriptor = "chi_c0(1P) -> K+ K- pi+ pi-",
                                                    ComAMCuts      = config['Chic0ComAMCuts'],
                                                    PreVertexCuts  = config['Chic0ComN4Cuts'], 
                                                    PostVertexCuts = config['Chic0MomN4Cuts']
                                                    )

        """
        Chic0-> K K K K
        """
        self.SelChic02KKKK = self.createN4BodySel( OutputList = self.name + "SelChic02KKKK",
                                                  DaughterLists = [ self.SelKaons4h ],
                                                  DecayDescriptor = "chi_c0(1P) -> K+ K- K+ K-",
                                                  ComAMCuts      = config['Chic0ComAMCuts'],
                                                  PreVertexCuts  = config['Chic0ComN4Cuts'], 
                                                  PostVertexCuts = config['Chic0MomN4Cuts']
                                                  )

        
        """
        Chic0-> Pi Pi Pi Pi
        """
        self.SelChic02PiPiPiPi = self.createN4BodySel( OutputList = self.name + "SelChic02PiPiPiPi",
                                                      DaughterLists = [ self.SelPions4h ],
                                                      DecayDescriptor = "chi_c0(1P) -> pi+ pi- pi+ pi-",
                                                      ComAMCuts      = config['Chic0ComAMCuts'],
                                                      PreVertexCuts  = config['Chic0ComN4Cuts'], 
                                                      PostVertexCuts = config['Chic0MomN4Cuts']
                                                      )

        """
        Chic0-> p pbar Pi Pi
        """
        self.SelChic02PPbarPiPi = self.createN4BodySel( OutputList = self.name + "SelChic02PPbarPiPi",
                                                      DaughterLists = [ self.SelPions4h, self.SelProtons ],
                                                      DecayDescriptor = "chi_c0(1P) -> p+ p~- pi+ pi-",
                                                      ComAMCuts      = config['Chic0ComAMCuts'],
                                                      PreVertexCuts  = config['Chic0ComN4Cuts'], 
                                                      PostVertexCuts = config['Chic0MomN4Cuts']
                                                      )
        """
	Chic0-> p pbar K K
        """
	self.SelChic02PPbarKK = self.createN4BodySel( OutputList = self.name + "SelChic02PPbarKK",
                                                      DaughterLists = [ self.SelKaons4h, self.SelProtons ],
                                                      DecayDescriptor = "chi_c0(1P) -> p+ p~- K+ K-",
                                                      ComAMCuts      = config['Chic0ComAMCuts'],
                                                      PreVertexCuts  = config['Chic0ComN4Cuts'],
                                                      PostVertexCuts = config['Chic0MomN4Cuts']
                                                      )

        """
        Chic0-> p pbar
        """
        self.SelChic02PPbar = self.createCombinationSel( OutputList = self.name + "SelChic02PPbar",
                                                        DecayDescriptor = "chi_c0(1P) -> p+ p~-",
                                                        DaughterLists = [ self.SelProtons ],          
                                                        PreVertexCuts  = config['Chic0ComCuts'], 
                                                        PostVertexCuts = config['Chic0MomN2Cuts']
                                                        )

        """
	Chic0-> K K
        """
	self.SelChic02KK    = self.createCombinationSel( OutputList = self.name + "SelChic02KK",
                                                        DecayDescriptor = "chi_c0(1P) -> K+ K-",
                                                        DaughterLists = [ self.SelKaons ],
                                                        PreVertexCuts  = config['Chic0ComCuts'],
                                                        PostVertexCuts = config['Chic0MomN2Cuts']
                                                        )

        """
	Chic0-> Pi Pi
        """
	self.SelChic02PiPi  = self.createCombinationSel( OutputList = self.name + "SelChic02PiPi",
                                                        DecayDescriptor = "chi_c0(1P) -> pi+ pi-",
                                                        DaughterLists = [ self.SelPions ],
                                                        PreVertexCuts  = config['Chic0ComCuts'],
                                                        PostVertexCuts = config['Chic0MomN2Cuts']
                                                        )

        """
        Chi_c0 -> 4h, Chi_c0 -> 2h
        """
        from PhysSelPython.Wrappers import MergedSelection
        self.SelChic0_4h = MergedSelection( self.name + "SelChic0_4h",
                                        RequiredSelections =  [ self.SelChic02KKPiPi, 
                                                                self.SelChic02KKKK,
                                                                self.SelChic02PiPiPiPi,
                                                                self.SelChic02PPbarPiPi,
                                                                self.SelChic02PPbarKK
                                                               ])

        self.SelChic0_2h = MergedSelection( self.name + "SelChic0_2h",
                                        RequiredSelections =  [ self.SelChic02KK,
                                                                self.SelChic02PiPi,
                                                                self.SelChic02PPbar
                                                                ])

        """
        K*
        """
        self.SelKstar       = self.createCombinationSel( OutputList = self.name + "SelKstar",
                                         DecayDescriptor = "[K*(892)0 -> K+ pi-]cc", 
                                         DaughterLists = [self.SelKaons, self.SelPions],
                                         PreVertexCuts  = config['KstarComCuts'],     
                                         PostVertexCuts = config['KstarMomCuts']
                                         )

	self.SelKstar4h     = self.createCombinationSel( OutputList = self.name + "SelKstar4h",
                                         DecayDescriptor = "[K*(892)0 -> K+ pi-]cc",
                                         DaughterLists = [self.SelKaons4h, self.SelPions4h],
                                         PreVertexCuts  = config['KstarComCuts'],
                                         PostVertexCuts = config['KstarMomCuts']
                                         )


        """
        phi(1020)
        """
        self.SelPhi = self.createCombinationSel( OutputList = self.name + "SelPhi",
                                         DecayDescriptor = "phi(1020) -> K+ K-", 
                                         DaughterLists = [self.SelKaons],
                                         PreVertexCuts  = config['PhiComCuts'],     
                                         PostVertexCuts = config['PhiMomCuts']
                                         )
         
        """
        B->Chi_c0(->2h) K* 
        """
        self.SelB2Chic0Kpi_2h = self.createCombinationSel( OutputList = self.name + "SelB2Chic0Kpi_2h",
                                                              DecayDescriptor = "[B0 -> chi_c0(1P) K*(892)0]cc",
                                                              DaughterLists = [ self.SelKstar, self.SelChic0_2h ],                    
                                                              PreVertexCuts  = config['BComCuts'],
                                                              PostVertexCuts = config['BMomCuts'] )
         
        self.B2Chic0Kpi_2hLine = StrippingLine( self.name + '_2hLine',                                                
                                                   prescale  = config['Prescale'],
                                                  # HLT       = config['HLTCuts'],
                                                   algos     = [ self.SelB2Chic0Kpi_2h ],
                                                   EnableFlavourTagging = False,
#                                                   MDSTFlag = False,
                                                   RelatedInfoTools = self.config['RelatedInfoTools']
                                                   )
        
        self.registerLine( self.B2Chic0Kpi_2hLine )
        
        
        
        """
        B->(Chi_c0->4h) K* 
        """
        self.SelB2Chic0Kpi_KKKK   = self.createCombinationSel( OutputList = self.name + "SelB2Chic0Kpi_KKKK",
                                                        DecayDescriptor = "[B0 -> chi_c0(1P) K*(892)0]cc",
                                                        DaughterLists = [ self.SelKstar4h, self.SelChic02KKKK ],
                                                        PreVertexCuts  = config['BComCuts'],
                                                        PostVertexCuts = config['BMomCuts'] )
        
        self.B2Chic0Kpi_KKKKLine  = StrippingLine( self.name + '_KKKKLine',
                                                 prescale  = config['Prescale'],
                                                 #  HLT       = config['HLTCuts'],
                                                 algos     = [ self.SelB2Chic0Kpi_KKKK ],
                                                 EnableFlavourTagging = False,
#                                                MDSTFlag = False,
                                                 RelatedInfoTools = self.config['RelatedInfoTools'])
        self.registerLine( self.B2Chic0Kpi_KKKKLine )



	self.SelB2Chic0Kpi_PiPiPiPi   = self.createCombinationSel( OutputList = self.name + "SelB2Chic0Kpi_PiPiPiPi",
                                                        DecayDescriptor = "[B0 -> chi_c0(1P) K*(892)0]cc",
                                                        DaughterLists = [ self.SelKstar4h, self.SelChic02PiPiPiPi ],
                                                        PreVertexCuts  = config['BComCuts'],
                                                        PostVertexCuts = config['BMomCuts'] )

        self.B2Chic0Kpi_PiPiPiPiLine  = StrippingLine( self.name + '_PiPiPiPiLine',
                                                 prescale  = config['Prescale'],
                                                 #  HLT       = config['HLTCuts'],
                                                 algos     = [ self.SelB2Chic0Kpi_PiPiPiPi ],
                                                 EnableFlavourTagging = False,
#                                                MDSTFlag = False,
                                                 RelatedInfoTools = self.config['RelatedInfoTools'])
        self.registerLine( self.B2Chic0Kpi_PiPiPiPiLine )



	self.SelB2Chic0Kpi_KKPiPi   = self.createCombinationSel( OutputList = self.name + "SelB2Chic0Kpi_KKPiPi",
                                                        DecayDescriptor = "[B0 -> chi_c0(1P) K*(892)0]cc",
                                                        DaughterLists = [ self.SelKstar4h, self.SelChic02KKPiPi ],
                                                        PreVertexCuts  = config['BComCuts'],
                                                        PostVertexCuts = config['BMomCuts'] )

        self.B2Chic0Kpi_KKPiPiLine  = StrippingLine( self.name + '_KKPiPiLine',
                                                 prescale  = config['Prescale'],
                                                 #  HLT       = config['HLTCuts'],
                                                 algos     = [ self.SelB2Chic0Kpi_KKPiPi ],
                                                 EnableFlavourTagging = False,
#                                                MDSTFlag = False,
                                                 RelatedInfoTools = self.config['RelatedInfoTools'])
        self.registerLine( self.B2Chic0Kpi_KKPiPiLine )



	self.SelB2Chic0Kpi_PPbarKK   = self.createCombinationSel( OutputList = self.name + "SelB2Chic0Kpi_PPbarKK",
                                                        DecayDescriptor = "[B0 -> chi_c0(1P) K*(892)0]cc",
                                                        DaughterLists = [ self.SelKstar4h, self.SelChic02PPbarKK ],
                                                        PreVertexCuts  = config['BComCuts'],
                                                        PostVertexCuts = config['BMomCuts'] )

        self.B2Chic0Kpi_PPbarKKLine  = StrippingLine( self.name + '_PPbarKKLine',
                                                 prescale  = config['Prescale'],
                                                 #  HLT       = config['HLTCuts'],
                                                 algos     = [ self.SelB2Chic0Kpi_PPbarKK ],
                                                 EnableFlavourTagging = False,
#                                                MDSTFlag = False,
                                                 RelatedInfoTools = self.config['RelatedInfoTools'])
        self.registerLine( self.B2Chic0Kpi_PPbarKKLine )



	self.SelB2Chic0Kpi_PPbarPiPi   = self.createCombinationSel( OutputList = self.name + "SelB2Chic0Kpi_PPbarPiPi",
                                                        DecayDescriptor = "[B0 -> chi_c0(1P) K*(892)0]cc",
                                                        DaughterLists = [ self.SelKstar4h, self.SelChic02PPbarPiPi ],
                                                        PreVertexCuts  = config['BComCuts'],
                                                        PostVertexCuts = config['BMomCuts'] )

        self.B2Chic0Kpi_PPbarPiPiLine  = StrippingLine( self.name + '_PPbarPiPiLine',
                                                 prescale  = config['Prescale'],
                                                 #  HLT       = config['HLTCuts'],
                                                 algos     = [ self.SelB2Chic0Kpi_PPbarPiPi ],
                                                 EnableFlavourTagging = False,
#                                                MDSTFlag = False,
                                                 RelatedInfoTools = self.config['RelatedInfoTools'])
        self.registerLine( self.B2Chic0Kpi_PPbarPiPiLine )









        """
        B+->Chi_c0(->2h) phi K+ 
        """
        self.SelB2Chic0phiK_2h = self.createCombinationSel( OutputList = self.name + "SelB2Chic0phiK_2h",
                                                              DecayDescriptor = "[B+ -> chi_c0(1P) phi(1020) K+]cc",
                                                              DaughterLists = [ self.SelPhi, self.SelKaons, self.SelChic0_2h ],                    
                                                              PreVertexCuts  = config['BComCuts'],
                                                              PostVertexCuts = config['BMomCuts'] )
         
        self.B2Chic0phiK_2hLine = StrippingLine( 'B2Chic0phiK_2hLine',                                                
                                                   prescale  = config['Prescale'],
                                                  # HLT       = config['HLTCuts'],
                                                   algos     = [ self.SelB2Chic0phiK_2h ],
                                                   EnableFlavourTagging = False,
#                                                   MDSTFlag = False,
                                                   RelatedInfoTools = self.config['RelatedInfoTools']
                                                   )
        
        self.registerLine( self.B2Chic0phiK_2hLine )
        
        
        
        """
        B+->Chi_c0(->4h) phi K+ 
        """
        self.SelB2Chic0phiK_4h = self.createCombinationSel( OutputList = self.name + "SelB2Chic0phiK_4h",
                                                        DecayDescriptor = "[B+ -> chi_c0(1P) phi(1020) K+]cc",
                                                        DaughterLists = [ self.SelPhi, self.SelKaons, self.SelChic0_4h ],                    
                                                        PreVertexCuts  = config['BComCuts'],
                                                        PostVertexCuts = config['BMomCuts'] )
        
        self.B2Chic0phiK_4hLine            = StrippingLine( 'B2Chic0phiK_4hLine',                                                
                                                   prescale  = config['Prescale'],
                                                 #  HLT       = config['HLTCuts'],
                                                   algos     = [ self.SelB2Chic0phiK_4h ],
                                                   EnableFlavourTagging = False,
#                                                   MDSTFlag = False,
                                                   RelatedInfoTools = self.config['RelatedInfoTools']                                                   
                                                   )
        self.registerLine( self.B2Chic0phiK_4hLine )

    def createSubSel( self, OutputList, InputList, Cuts ) :
        '''create a selection using a FilterDesktop'''
        filter = FilterDesktop(Code = Cuts)
        return Selection( OutputList,
                          Algorithm = filter,
                          RequiredSelections = [ InputList ] )

    def createCombinationSel( self, OutputList,
                              DecayDescriptor,
                              DaughterLists,
                              DaughterCuts = {} ,
                              PreVertexCuts = "ALL",
                              PostVertexCuts = "ALL",
                              ReFitPVs = True ) :
        '''create a selection using a ParticleCombiner with a single decay descriptor'''
        combiner = CombineParticles( DecayDescriptor = DecayDescriptor,
                                     DaughtersCuts = DaughterCuts,
                                     MotherCut = PostVertexCuts,
                                     CombinationCut = PreVertexCuts,
                                     ReFitPVs = False)
        return Selection ( OutputList,
                           Algorithm = combiner,
                           RequiredSelections = DaughterLists)


    def createN4BodySel( self, OutputList,
                         DecayDescriptor,
                         DaughterLists,
                         DaughterCuts = {} ,
                         ComAMCuts      = "AALL",
                         PreVertexCuts  = "AALL",
                         PostVertexCuts = "ALL" ) :
        '''create a selection using a ParticleCombiner with a single decay descriptor'''
        combiner = DaVinci__N4BodyDecays ( DecayDescriptor = DecayDescriptor,      
                                           DaughtersCuts = DaughterCuts,
                                           Combination12Cut  = ComAMCuts + "&" + "( ACHI2DOCA(1,2)<20 )",
                                           Combination123Cut = ComAMCuts + "&" + "( ACHI2DOCA(1,3)<20 ) & ( ACHI2DOCA(2,3)<20 )",
                                           CombinationCut = "( ACHI2DOCA(1,4)<20 ) & ( ACHI2DOCA(2,4)<20 ) & ( ACHI2DOCA(3,4)<20 )" + " & " + PreVertexCuts,
                                           MotherCut = PostVertexCuts,
                                           ReFitPVs = False )
        return Selection ( OutputList,
                           Algorithm = combiner,
                           RequiredSelections = DaughterLists)
    

    #def applyMVA( self, name, 
    #              SelB,
    #              MVAVars,
    #              MVAxmlFile,
    #              MVACutValue
    #              ):
    #    from MVADictHelpers import addTMVAclassifierValue
    #    from Configurables import FilterDesktop as MVAFilterDesktop
    #
    #    _FilterB = MVAFilterDesktop( name + "Filter",
    #                                 Code = "VALUE('LoKi::Hybrid::DictValue/" + name + "')>" + MVACutValue  )
    #
    #    addTMVAclassifierValue( Component = _FilterB,
    #                            XMLFile   = MVAxmlFile,
    #                            Variables = MVAVars,
    #                            ToolName  = name )
    #    
    #    return Selection( name,
    #                      Algorithm =  _FilterB,
    #                      RequiredSelections = [ SelB ] )
