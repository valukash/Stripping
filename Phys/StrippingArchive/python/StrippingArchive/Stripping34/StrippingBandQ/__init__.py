###############################################################################
# (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################

_selections = ('StrippingB2Chic0KPi', 'StrippingBbbar2PhiPhi', 'StrippingBc2Ds1Gamma', 'StrippingBc2JpsiHBDT', 'StrippingBc2JpsiMuXNew', 'StrippingBc3h', 'StrippingCC2DD', 'StrippingCcbar2LstLambda', 'StrippingCcbar2LstLst', 'StrippingCcbar2PPPiPi', 'StrippingCcbar2PhiPhi', 'StrippingCcbar2PhiPhiDetached', 'StrippingCcbar2PhiPhiPiPi', 'StrippingCcbar2PpbarNew', 'StrippingCharmAssociative', 'StrippingChiCJPsiGammaConv', 'StrippingDiMuonInherit', 'StrippingHeavyBaryons', 'StrippingLb2EtacKp', 'StrippingOmegab2XicKpi', 'StrippingPPMuMu', 'StrippingXB2DPiP', 'StrippingXibc', 'StrippingXibcBDT', 'StrippingXiccBDT')

for _sel in _selections :
    try :
        __import__( '%s.%s'  % ( __name__, _sel ) )
    except Exception, x:
        print '[WARNING] Submodule %s.%s raises the exception "%s" and will be skipped !' % ( __name__,_sel,x )

from sys import modules as _modules
_this = _modules[__name__]

_strippingKeys = filter ( lambda x : x[:9]=='Stripping',
                          locals().keys())

_strippingModules = [getattr(_this, _k) for _k in _strippingKeys]
