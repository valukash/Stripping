###############################################################################
# (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""
Module with all former stripping selection line builder modules.
All line builders available via function lineBuilders(stripping) in StrippingArchive.Utils.
"""

__author__ = 'Rob Lambert'
__all__ = ['strippingArchive', 'strippingDescription']

# List of known strippings IN THIS BRANCH
_known_strippings = [    
		     'Stripping24r2',
                     'Stripping25',                     
                     'Stripping27',                     
                     'Stripping28r2',                     
                     'Stripping30r2',
                     'Stripping30r3',
                     'Stripping31r1',
                     'Stripping31r2',
                     'Stripping33r2',
                     'Stripping34',
                     'Stripping34r0p1',                     
                     'Stripping35r2',
                     'Stripping35r3']

# List of obsolete strippings (ie, which don't run with the current stack).
# This only affects which Strippings are tested in the nightlies.
_relinfo_obsolete_strippings = []


#give a dictionary of strippings which use the same line builders
_duplicate_strippings={}


#give a dictionary to describe what each stripping was for IN THIS BRANCH
_stripping_help = {
    'Stripping24r2': 'Full restripping of 2015 pp data',
    'Stripping25': 'Stripping of 2015 pAr modified to run on 2018-patches',         'Stripping27': 'Stripping of 2016 pHe modified to run on 2018-patches',         'Stripping28r2': 'Full restripping of 2016 pp data',                            'Stripping30r2': 'Full restripping of 2016 Pbp data',
    'Stripping30r3': 'Full restripping of 2016 pPb data',
    'Stripping31r1': 'Stripping of 2015 PbAr data',
    'Stripping31r2': 'Full restripping of 2015 PbPb data',
    'Stripping33r2': 'Full restripping of 2017 pNe data',
    'Stripping34': '2018 pp stripping',
    'Stripping34r0p1': 'Incremental re-stripping of 2018 pp data as part of the full Run 1 and Run 2 re-stripping campaigns',
    'Stripping35r2': 'Full restripping of 2018 PbPb data',
    'Stripping35r3': 'Full restripping of 2018 PbNe'}


_strippingKeys = {}

#known strippings
for x in _known_strippings :
    _strippingKeys[x] = x

#add the duplicated strippings
for _k in _duplicate_strippings:
    if _k in _strippingKeys:
        raise KeyError, _k+' already defined as a StrippingArchive. check _duplicate_strippings'
    if _duplicate_strippings[_k] not in _known_strippings:
        raise KeyError, _duplicate_strippings[_k]+' is not defined as a StrippingArchive. check _duplicate_strippings'
    if _duplicate_strippings[_k] in _strippingKeys : 
        _strippingKeys[_k]=_strippingKeys[_duplicate_strippings[_k]]


def _listofStrippings():
    _l=[]
    for _k in _known_strippings:
        module_name = __name__ + "." + _k
        print "Trying to import module", module_name
        try : 
            __import__(module_name)
        except Exception, _k: 
            print "  -> Cannot be loaded with this version of DaVinci (%s)" % str(_k)
        else:
            _l.append(_k)
    for _d in _strippingKeys:
        if _strippingKeys[_d] in _l and _d not in _l:
            _l.append(_d)
    return _l



def _importArchive(stripping):
    if type(stripping) is not str:
        raise TypeError, "Strippings must be strings, like Stripping15 for example"
        
    print "Requested "+stripping
    _tmpstrip=str(stripping)
    
    if stripping not in _strippingKeys.keys():
        raise KeyError, stripping + ' is not known, call strippingArchive() with no argument to get the full dictionary'
    if  _strippingKeys[stripping]!=stripping:
        print stripping+" is a duplicate of "+_strippingKeys[stripping]
        _tmpstrip=str(_strippingKeys[stripping])


    module_name = __name__ + "." + _tmpstrip
    print "Trying to import module", module_name
    try : 
        __import__(module_name)
    except Exception, _tmpstrip: 
        print " -> Cannot be loaded with this version of DaVinci (%s)" % str(_tmpstrip)
    
    from sys import modules as _modules
    _this = _modules[__name__]
    
    _stripping = getattr(_this, _tmpstrip)
    return _stripping


#check the descriptions
for _k in _strippingKeys:
    if _k not in _stripping_help:
        raise KeyError, _k+' has not been provided a description. check _stripping_help'

#the only two functions to be exported to the end user
def strippingArchive(stripping=None):
    '''Return the archived stripping line builders.
    strippingArchive(): return all line builder modules in a dictionary {stripping:module}.
    strippingArchive(stripping): return the line builder module for that stripping'''
    if stripping is None:
        return list(_listofStrippings())
    
    strippingname=""
    
    for _k in _strippingKeys:
        if _k.lower()==stripping.lower():
            strippingname=_k
            break
    if strippingname=="":
        raise KeyError, stripping + ' is not known, call strippingArchive() with no argument to get the full dictionary'
    
    
    
    strip = _importArchive(strippingname)
    return strip
    

def strippingDescription(stripping=None):
    '''Return the description of the stripping pass
    strippingDescription(): return all descriptions in a dictionary {stripping:module}.
    strippingDescription(stripping): return the description for that stripping'''
    if stripping is None:
        return list(_listofStrippings())
 
    strippingname=""

    for _k in _strippingKeys:
        if _k.lower()==stripping.lower():
            strippingname=_k
            break
    if strippingname=="":
        raise KeyError, stripping + ' is not known, call strippingArchive() with no argument to get the full dictionary'
    
    strip = _importArchive(strippingname)
    
    return _stripping_help[strippingname]
