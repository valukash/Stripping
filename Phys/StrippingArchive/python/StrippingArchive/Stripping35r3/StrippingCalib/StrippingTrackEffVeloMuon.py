###############################################################################
# (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
#
# A stripping selection for VeloMuon from J/Psi->mu+mu-,
# Upsilon->mu+mu-, Z->mu+mu- decays
# To be used for tracking studies
#
# @authors G. Krocker, P. Seyfert, S. Wandernoth
# @date 2010-Aug-17
#
# @authors P. Seyfert, A. Jaeger
# @date 2011-Mar-17
# 
# @author M. Kolpin
# @date 2015-Mar-23
#
# @author R. Kopecna
# @date 2019-Apr-15
#
###############################################################################
__author__ = ['Flavio Archilli']
__date__ = '30/10/2019'
__version__ = '$Revision: 0.1 $'

__all__ = ('StrippingTrackEffVeloMuonConf',
           'default_config'
           )

default_config = {
    'NAME'        : 'TrackEffVeloMuon',
    'WGs'         : ['ALL'],
    'BUILDERTYPE' : 'StrippingTrackEffVeloMuonConf',
    'CONFIG'      : {
         'Jpsi': {"ResonanceName":   "J/psi(1S)"
              ,   "DecayDescriptor": "J/psi(1S) -> mu+ mu-"
              ,   "CombPT" :             0.5 # GeV
              ,   "TrP" :            5. # GeV
              ,   "TrPT":            0.5 # GeV
              ,   "TrChi2":          5.0 # adimensional
              ,   "LongP":           7. # GeV
              ,   "LongPT":          0.5 # GeV
              ,   "LongChi2":        3.0 
              ,   "LongMinIP":       0.2 # mm
              ,   "VertChi2":        2.0
              ,   "MuDLL"   :        -1.
              ,   "MassPreComb":     1000. # MeV
              ,   "MassPostComb":    500. # MeV
              ,   "Prescale":        1
              ,   "Postscale":       1
              ,   "Hlt1Filter":      'Hlt1.*Decision'
              ,   "Hlt2Filter":      'Hlt2.*Decision'
              ,   "HLT1TisTosSpecs": { "Hlt1TrackMuonDecision%TOS" : 0, "Hlt1SingleMuonNoIPDecision%TOS" : 0 }
              ,   "HLT1PassOnAll":   True
              ,   "HLT2TisTosSpecs": { "Hlt2SingleMuon.*Decision%TOS" : 0, "Hlt2TrackEffDiMuonVeloMuon.*Decision%TOS" : 0 }
              ,   "HLT2PassOnAll":   False
              ,   "vemucut"      :   "(TRCHI2DOF < %(TrChi2)s) & (PT > %(TrPT)s*GeV) & (P > %(TrP)s*GeV)"
              ,   "longcut"      :   "(TRCHI2DOF < %(LongChi2)s) & (P > %(LongP)s*GeV) & (PIDmu > %(MuDLL)s) & (MIPDV(PRIMARY)>%(LongMinIP)s*mm)"
              ,   "resonancecut" :   "(VFASPF(VCHI2/VDOF) < %(VertChi2)s) & (PT > %(CombPT)s*GeV)"
              ,   "linename"     :   "Line"
              },
         "Upsilon": { "ResonanceName":   "Upsilon(1S)"
                  ,   "DecayDescriptor": "Upsilon(1S) -> mu+ mu-"
                  ,   "CombPT" :             0.5 # GeV
                  ,   "TrP" :            0. # GeV
                  ,   "TrPT":            0.5 # GeV
                  ,   "TrChi2":          5.
                  ,   "LongP":           7. # GeV
                  ,   "LongPT":          0.5 # GeV
                  ,   "LongChi2":        3.
                  ,   "VertChi2":        10000.
                  ,   "MuDLL"   :        1.
                  ,   "MassPreComb":     100000. # MeV
                  ,   "MassPostComb":    1500. # MeV
                  ,   "Prescale":        1
                  ,   "Postscale":       1
                  ,   "Hlt1Filter":      'Hlt1.*Decision'
                  ,   "Hlt2Filter":      'Hlt2.*Decision'
                  ,   "HLT1TisTosSpecs": { "Hlt1SingleMuonHighPTDecision%TOS" : 0 }
                  ,   "HLT1PassOnAll":   True
                  ,   "HLT2TisTosSpecs": { "Hlt2SingleMuonLowPTDecision%TOS" : 0 }
                  ,   "HLT2PassOnAll":   False
                  ,   "vemucut"      :   "((PT > %(TrPT)s*GeV))"
                  ,   "longcut"      :   "((PT > %(LongPT)s*GeV))"
                  ,   "resonancecut" :   "(PT > %(CombPT)s*GeV)"
                  ,   "linename"     :   "UpsilonLine"
                      },
         "Z": {    "ResonanceName":   "Z0"
               ,   "DecayDescriptor": "Z0 -> mu+ mu-"
               ,   "CombPT" :             0.5 # GeV
               ,   "TrP" :            0. # GeV
               ,   "TrPT":            20. # GeV
               ,   "TrChi2":          5.
               ,   "LongP":           7. # GeV
               ,   "LongPT":          0.5 # GeV
               ,   "LongChi2":        3.
               ,   "TrMinEta":        2.0
               ,   "TrMaxEta":        4.5
               ,   "LongMinEta":        2.0
               ,   "LongMaxEta":        4.5
               ,   "VertChi2":        10000. 
               ,   "MuDLL"   :        1.
               ,   "MassPreComb":     100000. # MeV
               ,   "MassPostComb":    40000. # MeV
               ,   "Prescale":        1
               ,   "Postscale":       1
               ,   "Hlt1Filter":      'Hlt1.*Decision'
               ,   "Hlt2Filter":      'Hlt2.*Decision'
               ,   "HLT1TisTosSpecs": { "Hlt1SingleMuonHighPTDecision%TOS" : 0 }
               ,   "HLT1PassOnAll":   True
               ,   "HLT2TisTosSpecs": { "Hlt2SingleMuonHighPTDecision%TOS" : 0 }
               ,   "HLT2PassOnAll":   False
               ,   "vemucut"      :   "((PT > %(TrPT)s*GeV) & (ETA > %(TrMinEta)s) & (ETA < %(TrMaxEta)s) )"
               ,   "longcut"      :   "((PT > %(LongPT)s*GeV) & (ETA > %(LongMinEta)s) & (ETA < %(LongMaxEta)s) )"
               ,   "resonancecut" :   "(PT > %(CombPT)s*GeV)"
               ,   "linename"     :   "ZLine"
               }                        
         },
    'STREAMS'     : { 'Dimuon' : ['StrippingTrackEffVeloMuonLine1',
                                       'StrippingTrackEffVeloMuonLine2',
                                       'StrippingTrackEffVeloMuonZLine1',
                                       'StrippingTrackEffVeloMuonZLine2',
                                       'StrippingTrackEffVeloMuonUpsilonLine1',
                                       'StrippingTrackEffVeloMuonUpsilonLine2']
                      }
}


from Gaudi.Configuration import *
from LHCbKernel.Configuration import *
from Configurables import CombineParticles	
from PhysSelPython.Wrappers import Selection, SelectionSequence, DataOnDemand
from Configurables import ChargedProtoParticleMaker, NoPIDsParticleMaker, DataOnDemandSvc, DelegatingTrackSelector, TrackSelector, CombinedParticleMaker, BestPIDParticleMaker
from Configurables import FastVeloTracking
  
from StrippingConf.StrippingLine import StrippingLine
from Configurables import TrackStateInitAlg, TrackEventFitter, TrackPrepareVelo,TrackContainerCopy, Tf__PatVeloSpaceTool, StandaloneMuonRec
from Configurables import TrackCloneFinder
from StrippingUtils.Utils import LineBuilder
from StandardParticles import StdLooseMuons

from GaudiConfUtils.ConfigurableGenerators import FilterDesktop, TisTosParticleTagger

from Configurables import GaudiSequencer
from Configurables import TrackSys
from PhysSelPython.Wrappers import AutomaticData
from TrackFitter.ConfiguredFitters import ConfiguredFit

from Configurables import TrackEventCloneKiller,VeloMuonBuilder
from Configurables import TrackEventFitter, TrackMasterFitter
from Configurables import TrackKalmanFilter, TrackMasterExtrapolator

from SelPy.utils import ( UniquelyNamedObject,
                          ClonableObject,
                          SelectionBase )



class StrippingTrackEffVeloMuonConf(LineBuilder):

    __configuration_keys__ = default_config['CONFIG'].keys()

    def __init__(self, name, config) :

        LineBuilder.__init__(self, name, config)
        self.name = name 
        self.config = config

        _lines = []
        _lines += self.buildTrackEffVeloMuonLine( "Jpsi" ) #two lines per call one for positive probes and one for negative probes 
        _lines += self.buildTrackEffVeloMuonLine( "Upsilon" )
        _lines += self.buildTrackEffVeloMuonLine( "Z" )
        for _line in _lines:
            self.registerLine(_line)

    def buildTrackEffVeloMuonLine(self, confLine):

        _config = self.config[confLine]
        _lines = []
        TrackingPreFilter = self.VeloMuonBuilder(self.name+"_"+confLine,
                                                 [ ]) 
        VeloMuProtoPFilter = self.selMuonPParts(self.name+"_"+confLine,
                                                TrackingPreFilter)
        VeloMuPFilter = self.makeMyMuons(self.name+"_"+confLine,
                                         VeloMuProtoPFilter)

        longMuons = self.longtrackFilter( self.name+"_"+confLine+"_Longs",
                                               muCut = _config["longcut"] % _config)
        

        LongMuHlt1 = self.selHlt1('TisTosFilter1'+confLine+'for'+self.name,
                                                preFilter = longMuons,
                                                HLT1TisTosSpecs = _config['HLT1TisTosSpecs'],
                                                HLT1PassOnAll = _config['HLT1PassOnAll'])

        LongMuHlt2 = self.selHlt2('TisTosFilter2'+confLine+'for'+self.name,
                                                HLT1Filter = LongMuHlt1,
                                                HLT2TisTosSpecs = _config['HLT2TisTosSpecs'],
                                                HLT2PassOnAll = _config['HLT2PassOnAll'])
        

        _lines.append(self.buildPerCharge( confLine,
                                           velomuons = VeloMuPFilter,
                                           longmuons = LongMuHlt2,
                                           charge = 1) ) 
        
        _lines.append(self.buildPerCharge( confLine,
                                  velomuons = VeloMuPFilter,
                                  longmuons = LongMuHlt2,
                                  charge = -1) )        

        return _lines
        
    def buildPerCharge(self, confLine, velomuons, longmuons, charge):

        _config = self.config[confLine]
        
        _pchargeCut = None
        _tchargeCut = None
        _tagCharge = None
        _probeCharge = None
        _chargename = None
        _linename = _config["linename"]
        if (charge > 0):
            pchargeCut = "& (Q > 0)"    
            tchargeCut = "& (Q < 0)"    
            _tagCharge = "Minus"
            _probeCharge = "Plus"
            _chargename = "1"
        else:
            pchargeCut = "& (Q < 0)"    
            tchargeCut = "& (Q > 0)"    
            _probeCharge = "Minus"
            _tagCharge = "Plus"
            _chargename = "2"
            
        _probeCut = _config["vemucut"] % _config
        _probeCut += pchargeCut

        _tagCut = _config["longcut"] % _config
        _tagCut += tchargeCut


        _pfilter = FilterDesktop().configurable(self.name+confLine+"probeFilter"+_probeCharge)
        _pfilter.Code = _probeCut
        _probe  = Selection(self.name+confLine+"FilteredVeloMuon"+_probeCharge,
                            Algorithm = _pfilter,
                            RequiredSelections = [velomuons])

        _tfilter = FilterDesktop().configurable(self.name+confLine+"tagFilter"+_probeCharge)
        _tfilter.Code = _tagCut
        _tag  = Selection(self.name+confLine+"FilteredLong"+_tagCharge,
                          Algorithm = _tfilter,
                          RequiredSelections = [longmuons])

        _algo = self.makeResonanceVeloMuTrackEff(self.name + confLine + "VeloMuJpsiSel" + _chargename,
                                                 confLine = confLine,
                                                 tagmuon = _tag,
                                                 probemuon = _probe)

        _line =  StrippingLine(self.name + _linename + _chargename
                               , prescale = _config['Prescale']
                               , postscale = _config['Postscale']
                               , algos=[_algo]
                               , RequiredRawEvents = ["Trigger","Muon","Velo","Tracker"]
                               , HLT1 = "HLT_PASS_RE('%(Hlt1Filter)s')" % _config
                               , HLT2 = "HLT_PASS_RE('%(Hlt2Filter)s')" % _config
                               , MDSTFlag = True)

        return _line
        
    ########################
    # Probe Muon 
    ########################

    def VeloMuonBuilder(self, localname, prefilter):
        
        VeloMuonBuilder1 = VeloMuonBuilder("VeloMuonBuilder"+localname)
        VeloMuonBuilder1.OutputLevel = 6
        VeloMuonBuilder1.MuonLocation = "Rec/Track/"+localname+"MuonStandalone"
        VeloMuonBuilder1.VeloLocation = "Rec/Track/"+localname+"UnFittedVelo"
        VeloMuonBuilder1.lhcbids = 4
        VeloMuonBuilder1.OutputLocation = "Rec/VeloMuon/"+localname+"Tracks"
        
        preve = TrackPrepareVelo(localname+"preve")
        preve.inputLocation = "Rec/"+localname+"_Track/Velo"
        preve.outputLocation = "Rec/Track/"+localname+"UnFittedVelo"
        preve.bestLocation = ""
        alg = GaudiSequencer("VeloMuonTrackingFor"+localname,
                             Members = [ FastVeloTracking(localname+"FastVelo",OutputTracksName="Rec/"+localname+"_Track/Velo"),
                                         preve, 
                                         StandaloneMuonRec(localname+"MuonStandalone",OutputMuonTracksName="Rec/Track/"+localname+"MuonStandalone"),
                                         VeloMuonBuilder1]
                             )


        return GSWrapper(name="WrappedVeloMuonTracking"+localname,
                         sequencer=alg,
                         output='Rec/VeloMuon/'+localname+'Tracks',
                         requiredSelections =  prefilter)

    

    def selMuonPParts(self, localname, preSequence):
        """
        Make ProtoParticles out of VeloMuon tracks
        """
        veloprotos = ChargedProtoParticleMaker(localname+"ProtoPMaker")
        veloprotos.Inputs = ["Rec/VeloMuon/"+localname+"Tracks"]
        veloprotos.Output = "Rec/ProtoP/ProtoPMaker/"+localname+"ProtoParticles"
        veloprotos.addTool( DelegatingTrackSelector, name="TrackSelector" )
        tracktypes = [ "Long" ]
        veloprotos.TrackSelector.TrackTypes = tracktypes
        selector = veloprotos.TrackSelector
        for tsname in tracktypes:
            selector.addTool(TrackSelector,name=tsname)
            ts = getattr(selector,tsname)
            ts.TrackTypes = [tsname]

        veloprotoseq = GaudiSequencer(localname+"ProtoPSeq")
        veloprotoseq.Members += [ veloprotos ]

        return GSWrapper(name="WrappedVeloMuonProtoPSeqFor" + localname,
                         sequencer=veloprotoseq,
                         output='Rec/ProtoP/ProtoPMaker/'+localname+'ProtoParticles',
                         requiredSelections = [preSequence])
    
    
    def makeMyMuons(self, localname, protoParticlesMaker):
        """
        Make Particles out of the muon ProtoParticles
        """
        particleMaker =  NoPIDsParticleMaker(localname+"ParticleMaker" , Particle = "Muon")
        particleMaker.Input = "Rec/ProtoP/ProtoPMaker/"+localname+"ProtoParticles"


        DataOnDemandSvc().AlgMap.update( {
            "/Event/Phys/" + particleMaker.name() + '/Particles' : particleMaker.getFullName(),
            "/Event/Phys/" + particleMaker.name() + '/Vertices'  : particleMaker.getFullName() 
            } )
        

        return Selection(localname+"SelVeloMuonParts",
                         Algorithm = particleMaker,
                         RequiredSelections = [protoParticlesMaker],
                         InputDataSetter=None)


    ########################
    # Tag Muon 
    ########################

    def longtrackFilter(self, localname, muCut):
        """
        Select plus or minus charge for longtrack
        """
        _filter = FilterDesktop().configurable("LongTrackFilter")
        _filter.Code = muCut

        return Selection( localname+'_longFilter'+'LongMu',
                          Algorithm = _filter,
                          RequiredSelections = [ StdLooseMuons ] )

    def selHlt1(self, localname, preFilter, HLT1TisTosSpecs, HLT1PassOnAll):
        """
        Filter the long track muon to be TOS on a HLT1 single muon trigger,
        """

        Hlt1Jpsi = TisTosParticleTagger(
            TisTosSpecs = HLT1TisTosSpecs
            ,ProjectTracksToCalo = False
            ,CaloClustForCharged = False
            ,CaloClustForNeutral = False
            ,TOSFrac = { 4:0.0, 5:0.0 }
            ,NoRegex = True
            )
        Hlt1Jpsi.PassOnAll = HLT1PassOnAll
        return Selection(localname+"_SelHlt1Jpsi",
                         Algorithm = Hlt1Jpsi,
                         RequiredSelections = [ preFilter ])

    def selHlt2(self, localname, HLT1Filter, HLT2TisTosSpecs, HLT2PassOnAll):
        """
        Filter the long track muon to be TOS on a HLT2 single muon trigger,
        """

        Hlt2Jpsi = TisTosParticleTagger(
            TisTosSpecs = HLT2TisTosSpecs
            ,ProjectTracksToCalo = False
            ,CaloClustForCharged = False
            ,CaloClustForNeutral = False
            ,TOSFrac = { 4:0.0, 5:0.0 }
            ,NoRegex = False
            )
        Hlt2Jpsi.PassOnAll = HLT2PassOnAll
        return Selection(localname+"_SelHlt2Jpsi",
                         Algorithm = Hlt2Jpsi,
                         RequiredSelections = [ HLT1Filter ])


    ########################
    # make the resonance 
    ########################


    def makeResonanceVeloMuTrackEff(self, localname, confLine, tagmuon, probemuon):                                    
        """
        Create and return a Resonance -> mu mu Selection object, with one track a long track
        and the other a MuonVelo track.
        Arguments:
        name                 : name of the selection
        ResonanceName        : name of the resonance
        DecayDescriptor      : decayDescriptor of the decay
        plusCharge           : algorithm for selection positvely charged tracks
        minusCharge          : algorithm for selection negatively charged tracks
        """    
        _config = self.config[confLine]
        
        MuonVeloResonance = CombineParticles('_'+localname)
        MuonVeloResonance.DecayDescriptor = _config["DecayDescriptor"]
        MuonVeloResonance.OutputLevel = 4 

        MuonVeloResonance.CombinationCut = "ADAMASS('%(ResonanceName)s')<%(MassPreComb)s*MeV" % _config
        _resonancecut = "(ADMASS('%(ResonanceName)s')<%(MassPostComb)s*MeV) &" % _config
        _resonancecut+= _config["resonancecut"] % _config
        MuonVeloResonance.MotherCut = _resonancecut
                    
        return Selection(localname, Algorithm = MuonVeloResonance, RequiredSelections = [tagmuon, probemuon] )
    


class GSWrapper(UniquelyNamedObject,
                ClonableObject,
                SelectionBase) :
    
    def __init__(self, name, sequencer, output, requiredSelections) :
        UniquelyNamedObject.__init__(self, name)
        ClonableObject.__init__(self, locals())
        SelectionBase.__init__(self,
                               algorithm = sequencer,
                               outputLocation = output,
                               requiredSelections = requiredSelections )
