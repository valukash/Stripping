###############################################################################
# (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""
Stripping options for (pre-)selecting D(s)+ -> h h pi gamma

Authors: Shantam Taneja
"""

########################################################################
__author__ = ['Shantam Taneja']
__date__ = '14/01/2019'
__version__ = '$Revision: 0.1 $'

__all__ = ('D2HHHGammaLines',
           'default_config')

from Gaudi.Configuration import *

from GaudiConfUtils.ConfigurableGenerators import CombineParticles, DaVinci__N4BodyDecays
from StandardParticles                     import StdNoPIDsPions, StdNoPIDsKaons
from StandardParticles                     import StdLooseAllPhotons, StdAllLooseGammaLL, StdAllLooseGammaDD

from PhysSelPython.Wrappers      import Selection
from StrippingConf.StrippingLine import StrippingLine
from StrippingUtils.Utils        import LineBuilder, checkConfig

from Configurables import FilterDesktop

from GaudiKernel.SystemOfUnits import GeV, MeV, picosecond, mm


default_config = {
    'NAME'        : 'D2HHHGamma',
    'WGs'         : ['Charm'],
    'BUILDERTYPE' : 'D2HHHGammaLines',
    'CONFIG'      : { 'PrescaleD2PiPiPiGamma'    : 1,
                      'PrescaleD2KPiPiGamma'     : 1,
                      'PrescaleD2KKPiGamma'      : 1,
                      'PrescaleD2PiPiPiGamma_CNVLL'    : 1,
                      'PrescaleD2KPiPiGamma_CNVLL'     : 1,
                      'PrescaleD2KKPiGamma_CNVLL'      : 1,
		      'PrescaleD2PiPiPiGamma_CNVDD'    : 1,
                      'PrescaleD2KPiPiGamma_CNVDD'     : 1,
                      'PrescaleD2KKPiGamma_CNVDD'      : 1,
                      # Gamma
                      'photonPT'                : 2.0 * GeV, #1.8 * GeV,
                      'MaxMass_CNV_LL'          : 100 * MeV,
                      'MaxVCHI2_CNV_LL'         : 9,
                      'MinPT_CNV_LL'            : 1000 * MeV,
                      'MaxMass_CNV_DD'          : 100 * MeV,
                      'MaxVCHI2_CNV_DD'         : 9,
                      'MinPT_CNV_DD'            : 1000 * MeV,
		      'Gamma_CL'		: 0.2,

                      # D(s)+ -> HHHGamma
                      'TrChi2'                  : 4,
                      'TrGhostProb'             : 0.5,
                      'MinTrkPT'                : 300 * MeV,
                      'MinTrkIPChi2'            : 5,
                      'HighPIDK'                : -1,
                      'LowPIDK'                 : 5,
                      'MaxADOCACHI2'            : 10.0,
                      'CombMassLow'             : 1665 * MeV,
                      'CombMassHigh'            : 2100 * MeV,
                      'MinCombPT'               : 3.0 * GeV,
                      'MaxVCHI2NDOF'            : 12.0,
                      'MinBPVDIRA'              : 0.99998,
                      'MinBPVTAU'               : 0.3 * picosecond,
                      'MassLow'                 : 1685 * MeV,
                      'MassHigh'                : 2100 * MeV,
                      # HLT filters, only process events firing triggers matching the RegEx
                      'Hlt1Filter': None,
                      'Hlt2Filter': None,
                    },
    'STREAMS'     : ['Charm']
    }

class D2HHHGammaLines( LineBuilder ) :
    """Class defining the D(s)+ -> h h pi gamma stripping lines"""

    __configuration_keys__ = default_config['CONFIG'].keys()

    def __init__( self,name,config ) :

        LineBuilder.__init__(self, name, config)

        self.D2HHHGamma, self.lineD2HHHGamma = {}, {}
	
	# Select photons
        self.selPhotons = {'': self.makePhoton('GammaFor'+name,
                                                config['photonPT'],
						config['Gamma_CL']),
                           '_CNVLL': self.makePhotonConv('CNVGammaLLFor'+name,
                                                        config['MaxMass_CNV_LL'],
                                                        config['MaxVCHI2_CNV_LL'],
                                                        config['MinPT_CNV_LL'],
                                                        [StdAllLooseGammaLL]),
                           '_CNVDD': self.makePhotonConv('CNVGammaDDFor'+name,
                                                        config['MaxMass_CNV_DD'],
                                                        config['MaxVCHI2_CNV_DD'],
                                                        config['MinPT_CNV_DD'],
                                                        [StdAllLooseGammaDD]) }


        decays = { 'PiPiPiGamma'  : ['[D+ -> pi+ pi- pi+ gamma]cc'],
                   'KPiPiGamma'   : ['[D+ -> K- pi+ pi+ gamma]cc', '[D+ -> K+ pi- pi+ gamma]cc'],
                   'KKPiGamma'    : ['[D+ -> K+ K- pi+ gamma]cc', '[D+ -> K+ K+ pi- gamma]cc'],
                     }

        

        for decay, decayDescriptors in decays.iteritems():
            # make the various stripping selections
            D2HHHGammaName   = name + "D2" + decay

            am34 = (139.57) * MeV
            #am4 = 134.9766 * MeV
            inputs = [ StdNoPIDsPions ] if decays == 'PiPiPiGamma' else [ StdNoPIDsPions, StdNoPIDsKaons ]

            # use both LL and DD KS
            for phName, photons in self.selPhotons.iteritems():

                # Create the D+ candidate
                self.D2HHHGamma[decay+phName] = self.makeD2HHHGamma(D2HHHGammaName+phName,
                                                              decayDescriptors,
                                                              config['TrChi2'],
                                                              config['TrGhostProb'],
                                                              config['MinTrkPT'],
                                                              config['MinTrkIPChi2'],
                                                              config['HighPIDK'],
                                                              config['LowPIDK'],
                                                              am34,
                                                              #am4,
                                                              config['CombMassLow'],
                                                              config['CombMassHigh'],
                                                              config['MaxADOCACHI2'],
                                                              config['MinCombPT'],
                                                              config['MassLow'],
                                                              config['MassHigh'],
                                                              config['MaxVCHI2NDOF'],
                                                              config['MinBPVDIRA'],
                                                              config['MinBPVTAU'],
                                                              inputs+[photons] # Not sure about this
                                                              )

                # Create the stripping line
                self.lineD2HHHGamma[decay+phName] = StrippingLine(D2HHHGammaName+phName+"Line",
                                                               prescale  = config['PrescaleD2'+decay+phName],
                                                               selection = self.D2HHHGamma[decay+phName],
                                                               HLT1 = config['Hlt1Filter'],
                                                               HLT2 = config['Hlt2Filter'] )

                # Register the line
                self.registerLine(self.lineD2HHHGamma[decay+phName])
            # end loop on phton types
        # end loop on decay modes

    def makePhoton(self, name, photonPT, Gamma_CL):
        """Create photon Selection object starting from DataOnDemand 'Phys/StdLooseAllPhotons'.

        @arg name: name of the Selection.
        @arg photonPT: PT of the photon

        @return: Selection object

        """
        # Prepare selection
        _code = "(PT> %(photonPT)s*MeV)" \
		"&( CL> %(Gamma_CL)s )" % locals()
        _gammaFilter = FilterDesktop(name = "GammaFilter_"+name, Code=_code)
        _stdGamma = StdLooseAllPhotons
        return Selection(name, Algorithm=_gammaFilter, RequiredSelections=[_stdGamma])

    def makePhotonConv(self, name,
                       mMax, maxVChi, minPT,
                       inputSel):
        """
            Create photon selection objects starting from converted photons list
                (StdAllLooseGammaLL, StdAllLooseGammaDD)
        """
        _code = " ( MM < %(mMax)s * MeV ) " \
                "&( HASVERTEX ) " \
                "&( VFASPF(VCHI2/VDOF)<%(maxVChi)s ) " \
                "&( PT >  %(minPT)s)" % locals()

        _filter = FilterDesktop(name = "ConvertedGammaFilter_"+name,
                                Code = _code)

        return Selection(name,
                         Algorithm = _filter,
                         RequiredSelections = inputSel )


    def makeD2HHHGamma( self, name, decayDescriptors,
                   trChi2,trGhostProb,minPT,minIPChi2,
                   highPIDK, lowPIDK,
                   am34, amMin, amMax,
                   maxDocaChi2, minCombPT,
                   vmMin, vmMax, maxVChi, minbpvDira, minLT,
                   inputSel = [ StdNoPIDsPions, StdNoPIDsKaons ]) :
        """
          Create and return a D0 -> HHHGamma (H=K,pi) Selection object.
          Arguments:
          name            : name of the Selection.
          DecayDescriptor : DecayDescriptor.
          trChi2          : maximum tracks chi2
          trGhostProb     : maximum tracks ghost probability
          minPT           : minimum tracks PT
          minIPChi2       : minimum tracks IPChi2
          highPIDK        : maximum PIDk for pions
          lowPIDK         : minimum PIDk for kaons
          am34            : phase space limit on 2-body combinations mass
          am4             : phase space limit on 3-body combinations mass
          amMin           : minimum 4-body combination mass
          amMax           : maximum 4-body combination mass
          minPT           : minimum 4-body combination PT
          maxDocaChi2     : maximum 2-tracks DocaChi2
          mMin            : minimum vertex mass
          mMax            : maximum vertex mass
          maxVChi         : maximum vertex chi2
          minbpvDira      : minimum DIRA wrt best PV
          minLT           : minimum lifetime wrt best PV
          inputSel        : input selections
        """

        _daughters_cuts = " (TRGHOSTPROB < %(trGhostProb)s)" \
                          "&(TRCHI2DOF < %(trChi2)s)" \
                          "&(PT > %(minPT)s)" \
                          "&(MIPCHI2DV(PRIMARY) > %(minIPChi2)s )" %locals()
        _pidPi = "&(PIDK < %(highPIDK)s)" %locals()
        _pidK  = "&(PIDK > %(lowPIDK)s)" %locals()


        _c12_cuts = (" ( AM < (%(amMax)s - %(am34)s) ) " \
                     "&( ACHI2DOCA(1,2) < %(maxDocaChi2)s ) " %locals() )
        _c123_cuts =(" ( ACHI2DOCA(1,3) < %(maxDocaChi2)s ) " \
                     "&( ACHI2DOCA(2,3) < %(maxDocaChi2)s ) " %locals() )
        _combination_cuts =  (" (in_range( %(amMin)s, AM, %(amMax)s )) " \
                              "&( (APT1+APT2+APT3+APT4) > %(minPT)s )" %locals() )
                              #"&( ACHI2DOCA(1,4) < %(maxDocaChi2)s ) " \ #removed in s23c since it has no effect on pi0 (ACHI2DOCA is always set to 0)
                              #"&( ACHI2DOCA(2,4) < %(maxDocaChi2)s ) " \
                              #"&( ACHI2DOCA(3,4) < %(maxDocaChi2)s ) " %locals() )
        _mother_cuts = (" (in_range( %(vmMin)s* MeV, M, %(vmMax)s )) " \
                        "&(VFASPF(VCHI2PDOF) < %(maxVChi)s)" \
                        "&(BPVDIRA > %(minbpvDira)s )" \
                        "&(BPVLTIME() > %(minLT)s )" %locals() )

        CombineD2HHHKs = DaVinci__N4BodyDecays(DecayDescriptors = decayDescriptors,
                                              DaughtersCuts = { "pi+" : _daughters_cuts+_pidPi, "K+" : _daughters_cuts+_pidK },
                                              Combination12Cut = _c12_cuts, Combination123Cut = _c123_cuts,
                                              CombinationCut = _combination_cuts,
                                              MotherCut = _mother_cuts)

        return Selection(name,
                         Algorithm = CombineD2HHHKs,
                         RequiredSelections = inputSel )


