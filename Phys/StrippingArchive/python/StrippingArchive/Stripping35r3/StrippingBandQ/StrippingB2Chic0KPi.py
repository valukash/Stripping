###############################################################################
# (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
'''
Module for selecting B0->Chic0 K pi, B+->Chic0 K phi #yanxi
'''

__author__=['Andrii Usachov']
__date__ = '13/03/2018'
__version__= '$Revision: 1.2$'


__all__ = (
    'B2Chic0KPiConf',
    'default_config'
    )

default_config = {
    'NAME'              :  'B2Chic0KPi',
    'BUILDERTYPE'       :  'B2Chic0KPiConf',
    'CONFIG'    : {
        'KaonCuts'      : "(PROBNNk  > 0.2) & (PT > 300*MeV) & (MIPCHI2DV()>9) & (TRGHOSTPROB<0.4)",
        'ProtonCuts'    : "(PROBNNp  > 0.2) & (PT > 300*MeV) & (MIPCHI2DV()>9) & (P > 10*GeV) & (TRGHOSTPROB<0.4)",
        'PionCuts'      : "(PROBNNpi > 0.2) & (PT > 300*MeV) & (MIPCHI2DV()>9) & (TRGHOSTPROB<0.4)",
        'KaonCuts4h'    : "(PROBNNk  > 0.2) & (PROBNNp < 0.9) & (PROBNNpi < 0.9) & (MIPCHI2DV()>6) & (PT > 300*MeV) & (TRGHOSTPROB<0.4)",
        'PionCuts4h'    : "(PROBNNpi > 0.2) & (PROBNNk < 0.9) & (PROBNNp  < 0.9) & (MIPCHI2DV()>6) & (PT > 300*MeV) & (TRGHOSTPROB<0.4)",
        'Chic0ComAMCuts' : "(AM<4.2*GeV)",
        'Chic0ComN4Cuts' : """
                           (in_range(2.6*GeV, AM, 4.1*GeV))
                           & ( (ACHILD(PT,1)+ACHILD(PT,2)+ACHILD(PT,3)+ACHILD(PT,4) ) > 2.0 *GeV)
                           & ( (ACHILD(MIPCHI2DV(), 1) + ACHILD(MIPCHI2DV(), 2) + ACHILD(MIPCHI2DV(), 3) + ACHILD(MIPCHI2DV(), 4))>50)
                           """,
        'Chic0MomN4Cuts' : """
                           (VFASPF(VCHI2/VDOF) < 5.)
                           & (in_range(2.7*GeV, MM, 4.1*GeV))
                           & (MIPCHI2DV(PRIMARY) > 0.)
                           & (BPVVDCHI2>16)
                           & (BPVDIRA>0.9)
                           """,
        'Chic0ComCuts'   : """
                           (in_range(2.6*GeV, AM, 4.2*GeV))
                           & ( (ACHILD(PT,1)+ACHILD(PT,2))> 1.5 *GeV)
                           """,
        'Chic0MomN2Cuts' : """
                           (VFASPF(VCHI2/VDOF) < 9.)
                           & (in_range(2.7*GeV, MM, 4.1*GeV))
                           & (MIPCHI2DV(PRIMARY) > 0.)
                           & (BPVVDCHI2>16)
                           & (BPVDIRA>0.9)
                           """,
        'KstarComCuts': """
                        (ACHILD(PT,1)+ACHILD(PT,2) > 900.*MeV) & (AM < 4.0 *GeV) & (ADOCACHI2CUT(10., ''))
                        & ( (ACHILD(MIPCHI2DV(), 1) + ACHILD(MIPCHI2DV(), 2))>20)
                        """,
        'KstarMomCuts': """
                          (MIPCHI2DV(PRIMARY) > 0.)
                          & (BPVVDCHI2 > 9.)
                          & (VFASPF(VCHI2) < 9.)
                          & (BPVDIRA>0.9)
                        """,
        'PhiComCuts': "(ACHILD(PT,1)+ACHILD(PT,2) > 400.*MeV) & (AM>970.*MeV) &(AM < 1070 *MeV) & (ADOCACHI2CUT(10., ''))",
        'PhiMomCuts': """
                          (MIPCHI2DV(PRIMARY) > 0.)
                          & (BPVVDCHI2 > 9.)
                          & (VFASPF(VCHI2) < 9.)
                          & (BPVDIRA>0.9)
                        """,
        'BComCuts'     : "(ADAMASS('B0') < 300 *MeV)",
        'BMomCuts'     : """
                          (VFASPF(VCHI2/VDOF) < 8.)
                          & (BPVDIRA> 0.9999)
                          & (BPVIPCHI2()<16)
                          & (BPVVDCHI2>49)
                         """,
        'LbComCuts'     : "(ADAMASS('Lambda_b0') < 200 *MeV)",
        'LbMomCuts'     : """
                          (VFASPF(VCHI2/VDOF) < 5.)
                          & (BPVDIRA> 0.9999)
                          & (BPVIPCHI2()<16)
                          & (BPVVDCHI2>49)
                         """,
        'Prescale'      : 1.,
        'RelatedInfoTools': [{
                      'Type'              : 'RelInfoVertexIsolation',
                      'Location'          : 'RelInfoVertexIsolation'
                  }, {
                      'Type'              : 'RelInfoConeVariables',
                      'ConeAngle'         : 1.0,
                      "Variables" : ['CONEANGLE', 'CONEMULT', 'CONEPTASYM'],
                      'Location'          : 'RelInfoConeVariables_1.0'
                  }, {
                      'Type'              : 'RelInfoConeVariables',
                      'ConeAngle'         : 1.5,
                      "Variables" : ['CONEANGLE', 'CONEMULT', 'CONEPTASYM'],
                      'Location'          : 'RelInfoConeVariables_1.5'
                  }, {
                      'Type'              : 'RelInfoConeVariables',
                      'ConeAngle'         : 2.0,
                      "Variables" : ['CONEANGLE', 'CONEMULT', 'CONEPTASYM'],
                      'Location'          : 'RelInfoConeVariables_2.0'
                  }]       
        },
    'STREAMS'           : ['Bhadron'],
    'WGs'               : ['BandQ'],
    }


from Gaudi.Configuration import *
from GaudiConfUtils.ConfigurableGenerators import FilterDesktop, CombineParticles 
from PhysSelPython.Wrappers import Selection, DataOnDemand
from StrippingConf.StrippingLine import StrippingLine
from StrippingUtils.Utils import LineBuilder
#from Configurables import DaVinci__N4BodyDecays
from GaudiConfUtils.ConfigurableGenerators import DaVinci__N4BodyDecays

class B2Chic0KPiConf(LineBuilder):
    
    __configuration_keys__ = default_config['CONFIG'].keys()

    def __init__(self, name, config ): 
        
        LineBuilder.__init__(self, name, config)
        self.name = name 
        self.config = config


        self.SelKaons   = self.createSubSel( OutputList = self.name + "SelKaons",
                                           InputList =  DataOnDemand(Location = 'Phys/StdLooseKaons/Particles' ), 
                                           Cuts = config['KaonCuts']
                                           )

        self.SelPions   = self.createSubSel( OutputList = self.name + "SelPions",
                                           InputList =  DataOnDemand(Location = 'Phys/StdLoosePions/Particles' ), 
                                           Cuts = config['PionCuts']
                                           )

        self.SelPions4h = self.createSubSel( OutputList = self.name + "SelPions4h",
                                           InputList =  DataOnDemand(Location = 'Phys/StdLoosePions/Particles' ), 
                                           Cuts = config['PionCuts4h']
                                           )

        self.SelKaons4h = self.createSubSel( OutputList = self.name + "SelKaons4h",
                                           InputList =  DataOnDemand(Location = 'Phys/StdLooseKaons/Particles' ), 
                                           Cuts = config['KaonCuts4h']
                                           )

        self.SelProtons = self.createSubSel( OutputList = self.name + "SelProtons",
                                           InputList =  DataOnDemand(Location = 'Phys/StdLooseProtons/Particles' ), 
                                           Cuts = config['ProtonCuts']
                                           )

        """
        Chic0-> K K Pi Pi
        """
        self.SelChic02KKPiPi = self.createN4BodySel( OutputList = self.name + "SelChic02KKPiPi",
                                                    DaughterLists = [ self.SelKaons4h, self.SelPions4h ],
                                                    DecayDescriptor = "chi_c0(1P) -> K+ K- pi+ pi-",
                                                    ComAMCuts      = config['Chic0ComAMCuts'],
                                                    PreVertexCuts  = config['Chic0ComN4Cuts'], 
                                                    PostVertexCuts = config['Chic0MomN4Cuts']
                                                    )

        """
        Chic0-> K K K K
        """
        self.SelChic02KKKK = self.createN4BodySel( OutputList = self.name + "SelChic02KKKK",
                                                  DaughterLists = [ self.SelKaons4h ],
                                                  DecayDescriptor = "chi_c0(1P) -> K+ K- K+ K-",
                                                  ComAMCuts      = config['Chic0ComAMCuts'],
                                                  PreVertexCuts  = config['Chic0ComN4Cuts'], 
                                                  PostVertexCuts = config['Chic0MomN4Cuts']
                                                  )

        
        """
        Chic0-> Pi Pi Pi Pi
        """
        self.SelChic02PiPiPiPi = self.createN4BodySel( OutputList = self.name + "SelChic02PiPiPiPi",
                                                      DaughterLists = [ self.SelPions4h ],
                                                      DecayDescriptor = "chi_c0(1P) -> pi+ pi- pi+ pi-",
                                                      ComAMCuts      = config['Chic0ComAMCuts'],
                                                      PreVertexCuts  = config['Chic0ComN4Cuts'], 
                                                      PostVertexCuts = config['Chic0MomN4Cuts']
                                                      )

        """
        Chic0-> p pbar Pi Pi
        """
        self.SelChic02PPbarPiPi = self.createN4BodySel( OutputList = self.name + "SelChic02PPbarPiPi",
                                                      DaughterLists = [ self.SelPions4h, self.SelProtons ],
                                                      DecayDescriptor = "chi_c0(1P) -> p+ p~- pi+ pi-",
                                                      ComAMCuts      = config['Chic0ComAMCuts'],
                                                      PreVertexCuts  = config['Chic0ComN4Cuts'], 
                                                      PostVertexCuts = config['Chic0MomN4Cuts']
                                                      )
        """
        Chic0-> p pbar K K
        """
        self.SelChic02PPbarKK = self.createN4BodySel( OutputList = self.name + "SelChic02PPbarKK",
                                                      DaughterLists = [ self.SelKaons4h, self.SelProtons ],
                                                      DecayDescriptor = "chi_c0(1P) -> p+ p~- K+ K-",
                                                      ComAMCuts      = config['Chic0ComAMCuts'],
                                                      PreVertexCuts  = config['Chic0ComN4Cuts'],
                                                      PostVertexCuts = config['Chic0MomN4Cuts']
                                                      )

        """
        Chic0-> p pbar
        """
        self.SelChic02PPbar = self.createCombinationSel( OutputList = self.name + "SelChic02PPbar",
                                                        DecayDescriptor = "chi_c0(1P) -> p+ p~-",
                                                        DaughterLists = [ self.SelProtons ],          
                                                        PreVertexCuts  = config['Chic0ComCuts'], 
                                                        PostVertexCuts = config['Chic0MomN2Cuts']
                                                        )

        """
        Chic0-> K K
        """
        self.SelChic02KK    = self.createCombinationSel( OutputList = self.name + "SelChic02KK",
                                                        DecayDescriptor = "chi_c0(1P) -> K+ K-",
                                                        DaughterLists = [ self.SelKaons ],
                                                        PreVertexCuts  = config['Chic0ComCuts'],
                                                        PostVertexCuts = config['Chic0MomN2Cuts']
                                                        )

        """
        Chic0-> Pi Pi
        """
        self.SelChic02PiPi  = self.createCombinationSel( OutputList = self.name + "SelChic02PiPi",
                                                        DecayDescriptor = "chi_c0(1P) -> pi+ pi-",
                                                        DaughterLists = [ self.SelPions ],
                                                        PreVertexCuts  = config['Chic0ComCuts'],
                                                        PostVertexCuts = config['Chic0MomN2Cuts']
                                                        )

        """
        Chi_c0 -> 4h, Chi_c0 -> 2h
        """
        from PhysSelPython.Wrappers import MergedSelection
        self.SelChic0_4h = MergedSelection( self.name + "SelChic0_4h",
                                        RequiredSelections =  [ self.SelChic02KKPiPi, 
                                                                self.SelChic02KKKK,
                                                                self.SelChic02PiPiPiPi,
                                                                self.SelChic02PPbarPiPi,
                                                                self.SelChic02PPbarKK
                                                               ])

        self.SelChic0_2h = MergedSelection( self.name + "SelChic0_2h",
                                        RequiredSelections =  [ self.SelChic02KK,
                                                                self.SelChic02PiPi,
                                                                self.SelChic02PPbar
                                                                ])

        """
        K*
        """
        self.SelKstar       = self.createCombinationSel( OutputList = self.name + "SelKstar",
                                         DecayDescriptor = "[K*(892)0 -> K+ pi-]cc", 
                                         DaughterLists = [self.SelKaons, self.SelPions],
                                         PreVertexCuts  = config['KstarComCuts'],     
                                         PostVertexCuts = config['KstarMomCuts']
                                         )

        """
        phi(1020)
        """
        self.SelPhi = self.createCombinationSel( OutputList = self.name + "SelPhi",
                                         DecayDescriptor = "phi(1020) -> K+ K-", 
                                         DaughterLists = [self.SelKaons],
                                         PreVertexCuts  = config['PhiComCuts'],     
                                         PostVertexCuts = config['PhiMomCuts']
                                         )
         
        """
        B->Chi_c0(->2h) K* 
        """
        self.SelB2Chic0Kpi_2h = self.createCombinationSel( OutputList = self.name + "SelB2Chic0Kpi_2h",
                                                              DecayDescriptor = "[B0 -> chi_c0(1P) K*(892)0]cc",
                                                              DaughterLists = [ self.SelKstar, self.SelChic0_2h ],                    
                                                              PreVertexCuts  = config['BComCuts'],
                                                              PostVertexCuts = config['BMomCuts'] )
         
        self.B2Chic0Kpi_2hLine = StrippingLine( self.name + '_2hLine',                                                
                                                   prescale  = config['Prescale'],
                                                  # HLT       = config['HLTCuts'],
                                                   algos     = [ self.SelB2Chic0Kpi_2h ],
                                                   EnableFlavourTagging = False,
#                                                   MDSTFlag = False,
                                                   RelatedInfoTools = self.config['RelatedInfoTools']
                                                   )
        
        self.registerLine( self.B2Chic0Kpi_2hLine )
        
        
        
        """
        B->(Chi_c0->4h) K* 
        """

        self.SelB2Chic0Kpi_4h = self.createCombinationSel( OutputList = self.name + "SelB2Chic0Kpi_4h",
                                                              DecayDescriptor = "[B0 -> chi_c0(1P) K*(892)0]cc",
                                                              DaughterLists = [ self.SelKstar, self.SelChic0_4h ],                    
                                                              PreVertexCuts  = config['BComCuts'],
                                                              PostVertexCuts = config['BMomCuts'] )
         
        self.B2Chic0Kpi_4hLine = StrippingLine( self.name + '_4hLine',                                                
                                                   prescale  = config['Prescale'],
                                                  # HLT       = config['HLTCuts'],
                                                   algos     = [ self.SelB2Chic0Kpi_4h ],
                                                   EnableFlavourTagging = False,
#                                                   MDSTFlag = False,
                                                   RelatedInfoTools = self.config['RelatedInfoTools']
                                                   )
        
        self.registerLine( self.B2Chic0Kpi_4hLine )
        
        




        """
        B+->Chi_c0(->2h) phi K+ 
        """
        self.SelB2Chic0phiK_2h = self.createCombinationSel( OutputList = self.name + "SelB2Chic0phiK_2h",
                                                              DecayDescriptor = "[B+ -> chi_c0(1P) phi(1020) K+]cc",
                                                              DaughterLists = [ self.SelPhi, self.SelKaons, self.SelChic0_2h ],                    
                                                              PreVertexCuts  = config['BComCuts'],
                                                              PostVertexCuts = config['BMomCuts'] )
         
        self.B2Chic0phiK_2hLine = StrippingLine( 'B2Chic0phiK_2hLine',                                                
                                                   prescale  = config['Prescale'],
                                                  # HLT       = config['HLTCuts'],
                                                   algos     = [ self.SelB2Chic0phiK_2h ],
                                                   EnableFlavourTagging = False,
#                                                   MDSTFlag = False,
                                                   RelatedInfoTools = self.config['RelatedInfoTools']
                                                   )
        
        self.registerLine( self.B2Chic0phiK_2hLine )
        
        
        
        """
        B+->Chi_c0(->4h) phi K+ 
        """
        self.SelB2Chic0phiK_4h = self.createCombinationSel( OutputList = self.name + "SelB2Chic0phiK_4h",
                                                        DecayDescriptor = "[B+ -> chi_c0(1P) phi(1020) K+]cc",
                                                        DaughterLists = [ self.SelPhi, self.SelKaons, self.SelChic0_4h ],                    
                                                        PreVertexCuts  = config['BComCuts'],
                                                        PostVertexCuts = config['BMomCuts'] )
        
        self.B2Chic0phiK_4hLine            = StrippingLine( 'B2Chic0phiK_4hLine',                                                
                                                   prescale  = config['Prescale'],
                                                 #  HLT       = config['HLTCuts'],
                                                   algos     = [ self.SelB2Chic0phiK_4h ],
                                                   EnableFlavourTagging = False,
#                                                   MDSTFlag = False,
                                                   RelatedInfoTools = self.config['RelatedInfoTools']                                                   
                                                   )
        self.registerLine( self.B2Chic0phiK_4hLine )

        """
        Lambda_b0->Chi_c0(->2h) p+ K- 
        """
        self.SelLb2Chic0pK_2h = self.createCombinationSel( OutputList = self.name + "SelLb2Chic0pK_2h",
                                                              DecayDescriptor = "[Lambda_b0 -> chi_c0(1P) p+ K-]cc",
                                                              DaughterLists = [ self.SelProtons, self.SelKaons, self.SelChic0_2h ],                    
                                                              PreVertexCuts  = config['LbComCuts'],
                                                              PostVertexCuts = config['LbMomCuts'] )
         
        self.Lb2Chic0pK_2hLine = StrippingLine( 'Lb2Chic0pK_2hLine',                                                
                                                   prescale  = config['Prescale'],
                                                  # HLT       = config['HLTCuts'],
                                                   algos     = [ self.SelLb2Chic0pK_2h ],
                                                   EnableFlavourTagging = False,
#                                                   MDSTFlag = False,
                                                   RelatedInfoTools = self.config['RelatedInfoTools']
                                                   )
        
        self.registerLine( self.Lb2Chic0pK_2hLine )

        """
        Lambda_b0->Chi_c0(->4h) p+ K- 
        """
        self.SelLb2Chic0pK_4h = self.createCombinationSel( OutputList = self.name + "SelLb2Chic0pK_4h",
                                                              DecayDescriptor = "[Lambda_b0 -> chi_c0(1P) p+ K-]cc",
                                                              DaughterLists = [ self.SelProtons, self.SelKaons, self.SelChic0_4h ],                    
                                                              PreVertexCuts  = config['LbComCuts'],
                                                              PostVertexCuts = config['LbMomCuts'] )
         
        self.Lb2Chic0pK_4hLine = StrippingLine( 'Lb2Chic0pK_4hLine',                                                
                                                   prescale  = config['Prescale'],
                                                  # HLT       = config['HLTCuts'],
                                                   algos     = [ self.SelLb2Chic0pK_4h ],
                                                   EnableFlavourTagging = False,
#                                                   MDSTFlag = False,
                                                   RelatedInfoTools = self.config['RelatedInfoTools']
                                                   )
        
        self.registerLine( self.Lb2Chic0pK_4hLine )
        

    def createSubSel( self, OutputList, InputList, Cuts ) :
        '''create a selection using a FilterDesktop'''
        filter = FilterDesktop(Code = Cuts)
        return Selection( OutputList,
                          Algorithm = filter,
                          RequiredSelections = [ InputList ] )

    def createCombinationSel( self, OutputList,
                              DecayDescriptor,
                              DaughterLists,
                              DaughterCuts = {} ,
                              PreVertexCuts = "ALL",
                              PostVertexCuts = "ALL",
                              ReFitPVs = True ) :
        '''create a selection using a ParticleCombiner with a single decay descriptor'''
        combiner = CombineParticles( DecayDescriptor = DecayDescriptor,
                                     DaughtersCuts = DaughterCuts,
                                     MotherCut = PostVertexCuts,
                                     CombinationCut = PreVertexCuts,
                                     ReFitPVs = False)
        return Selection ( OutputList,
                           Algorithm = combiner,
                           RequiredSelections = DaughterLists)


    def createN4BodySel( self, OutputList,
                         DecayDescriptor,
                         DaughterLists,
                         DaughterCuts = {} ,
                         ComAMCuts      = "AALL",
                         PreVertexCuts  = "AALL",
                         PostVertexCuts = "ALL" ) :
        '''create a selection using a ParticleCombiner with a single decay descriptor'''
        combiner = DaVinci__N4BodyDecays ( DecayDescriptor = DecayDescriptor,      
                                           DaughtersCuts = DaughterCuts,
                                           Combination12Cut  = ComAMCuts + "&" + "( ACHI2DOCA(1,2)<20 )",
                                           Combination123Cut = ComAMCuts + "&" + "( ACHI2DOCA(1,3)<20 ) & ( ACHI2DOCA(2,3)<20 )",
                                           CombinationCut = "( ACHI2DOCA(1,4)<20 ) & ( ACHI2DOCA(2,4)<20 ) & ( ACHI2DOCA(3,4)<20 )" + " & " + PreVertexCuts,
                                           MotherCut = PostVertexCuts,
                                           ReFitPVs = False )
        return Selection ( OutputList,
                           Algorithm = combiner,
                           RequiredSelections = DaughterLists)
    

    #def applyMVA( self, name, 
    #              SelB,
    #              MVAVars,
    #              MVAxmlFile,
    #              MVACutValue
    #              ):
    #    from MVADictHelpers import addTMVAclassifierValue
    #    from Configurables import FilterDesktop as MVAFilterDesktop
    #
    #    _FilterB = MVAFilterDesktop( name + "Filter",
    #                                 Code = "VALUE('LoKi::Hybrid::DictValue/" + name + "')>" + MVACutValue  )
    #
    #    addTMVAclassifierValue( Component = _FilterB,
    #                            XMLFile   = MVAxmlFile,
    #                            Variables = MVAVars,
    #                            ToolName  = name )
    #    
    #    return Selection( name,
    #                      Algorithm =  _FilterB,
    #                      RequiredSelections = [ SelB ] )
