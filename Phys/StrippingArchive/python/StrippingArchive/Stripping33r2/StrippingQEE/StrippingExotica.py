#!/usr/bin/env python
###############################################################################
# (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################

"""

StrippingLine making exotic candidates based on Hlt2Exotica.

### Exotica Lines written by Phil Ilten and Mike Williams (mwill@mit.edu)
Hlt2ExoticaDisplPhiPhi
Hlt2ExoticaRHNu
Hlt2ExoticaSharedDiMuonNoIP
Hlt2ExoticaQuadMuonNoIP
Hlt2ExoticaDisplDiMuon
Hlt2ExoticaDisplDiMuonNoPoint
Hlt2ExoticaPrmptDiMuonTurbo
Hlt2ExoticaPrmptDiMuonSSTurbo
Hlt2ExoticaPrmptDiMuonHighMass
Hlt2ExoticaDisplDiE
Hlt2ExoticaDisplDiELowMass
Hlt2ExoticaPi0ToDiEGamma
Hlt2ExoticaEtaToDiEGamma

"""

from GaudiKernel.SystemOfUnits import MeV, GeV, mm, picosecond
from GaudiConfUtils.ConfigurableGenerators import FilterDesktop
from GaudiConfUtils.ConfigurableGenerators import CombineParticles
from PhysSelPython.Wrappers import Selection, SimpleSelection, MergedSelection, AutomaticData
from StrippingUtils.Utils import LineBuilder
from StrippingConf.StrippingLine import StrippingLine


__author__  = 'Chitsanu Khurewathanakul', 'Philip Ilten', 'Michael Williams', 'Constantin Weisser'
__date__    = '2016-04-08'
__version__ = 1.3
__all__     = 'ExoticaConf', 'default_config'

default_config = {
  'NAME'        : 'Exotica',
  'BUILDERTYPE' : 'ExoticaConf',
  'WGs'         : [ 'QEE' ],
  'STREAMS'     : {
    'EW': [
      'StrippingExoticaDisplPhiPhiLine',
      'StrippingExoticaRHNuLine',
      'StrippingExoticaRHNuHighMassLine',
      'StrippingExoticaDiRHNuLine',
      'StrippingExoticaPrmptDiMuonHighMassLine',
      'StrippingExoticaQuadMuonNoIPLine',
      'StrippingExoticaDisplDiELine',
      'StrippingExoticaDisplDiEHighMassLine',
      'StrippingExoticaDisplDiELowMassFullLine',
      'StrippingExoticaPi0ToDiEGammaFullLine',
      'StrippingExoticaEtaToDiEGammaFullLine'
    ],
    'Leptonic': [
      'StrippingExoticaDisplDiELowMassMicroLine',
      'StrippingExoticaPi0ToDiEGammaMicroLine',
      'StrippingExoticaEtaToDiEGammaMicroLine'
    ],
    'Dimuon': [
      'StrippingExoticaDisplDiMuonLine',
      'StrippingExoticaDisplDiMuonHighMassLine',
      'StrippingExoticaDisplDiMuonNoPointLine',
      'StrippingExoticaDisplDiMuonNoPointRLine',
    ]
  },
  'CONFIG'      : {
    'Common': {
      'GhostProb': 0.3,
    },
    'Prescales': {
      'DisplPhiPhi'         : 1.0,
      'RHNu'                : 1.0,
      'RHNuHighMass'        : 1.0,
      'DiRHNu'              : 1.0,
      'DisplDiE'            : 1.0,
      'DisplDiEHighMass'    : 1.0,
      'DisplDiELowMassFull' : 0.1,
      'Pi0ToDiEGammaFull'   : 0.1,
      'EtaToDiEGammaFull'   : 0.1,
      'DisplDiELowMassMicro': 1.0,
      'Pi0ToDiEGammaMicro'  : 1.0,
      'EtaToDiEGammaMicro'  : 1.0,
      'DisplDiMuon'         : 1.0,
      'DisplDiMuonHighMass' : 1.0,
      'DisplDiMuonNoPoint'  : 1.0,
      'DisplDiMuonNoPointR' : 1.0,
      'PrmptDiMuonHighMass' : 1.0,
      'QuadMuonNoIP'        : 1.0,
    },
    'DisplPhiPhi': {
      # 'TOS_HLT2'      : 'Hlt2ExoticaDisplPhiPhiDecision',
      'input'         : 'Phys/StdLoosePhi2KK/Particles',
      'TisTosSpec'    : "Hlt1IncPhi.*Decision",
      'KPT'           : 500*MeV,
      'KIPChi2'       : 16,
      'KProbNNk'      : 0.1,
      'PhiPT'         : 1000*MeV,
      'PhiMassWindow' : 20*MeV,
      'VChi2'         : 10,
      'FDChi2'        : 45,
    },
    'SharedRHNu' : {
      'input'    : ['Phys/StdAllLooseMuons/Particles',
                    'Phys/StdNoPIDsPions/Particles'],
      'PT'       : 500*MeV,
      'P'        : 10000*MeV,
      'ProbNNmu' : 0.5,
      'IPChi2'   : 16,
      'VChi2'    : 10,
      'XIPChi2'  : 16,
      'M'        : 0,
      'TAU'      : 1*picosecond,
      'FDChi2'   : 45,
      },
    'RHNu' : {
      'M'   : 0,
      'TAU' : 1*picosecond,
      },
    'RHNuHighMass' : {
      'ProbNNmu' : 0.5,
      'M'        : 5000*MeV,
      'TAU'      : 1*picosecond,
      },
    'DiRHNu' : {
      'PT'    : 0,
      'VChi2' : 10,
      },
    'SharedDiENoIP': {
      'input'       : 'Phys/StdAllLooseElectrons/Particles',
      'EPT'         : 500*MeV,
      'EP'          : 3000*MeV,
      'EProbNNe'    : 0.1,
      'DOCA'        : 0.5*mm,
      'MM'          : 0*MeV,
      'VChi2'       : 25,
      },
    'SharedDiMuonNoIP': {
      'input'       : 'Phys/StdAllLooseMuons/Particles',
      'MuPT'        : 500*MeV,
      'MuP'         : 10000*MeV,
      'MuProbNNmu'  : 0.5,
      'DOCA'        : 0.5*mm,
      'VChi2'       : 10,
    },
    'DisplDiE': {
      # 'TOS_HLT2'  : 'Hlt2ExoticaDisplDiEDecision',
      'TisTosSpec': "Hlt1TrackMVA.*Decision",
      'EProbNNe'  : 0.1,
      'EIPChi2'   : 4,
      'MaxMM'     : 500*MeV,
      'PT'        : 500*MeV,
      'IPChi2'    : 32,
      'FDChi2'    : 0,
    },
    'DisplDiEHighMass': {
      # 'TOS_HLT2'  : 'Hlt2ExoticaDisplDiEDecision',
      'TisTosSpec': "Hlt1TrackMVA.*Decision",
      'EProbNNe'  : 0.1,
      'EIPChi2'   : 4,
      'MinMM'     : 500*MeV,
      'PT'        : 500*MeV,
      'IPChi2'    : 32,
      'FDChi2'    : 0,
      'TAU'       : 1*picosecond,
    },
    'DisplDiELowMassFull' : {
      'L0FILTER' : None,  #"L0_CHANNEL('Photon')|L0_CHANNEL('Electron')
      'HLT1FILTER' : "HLT_PASS_RE('Hlt1DiElectronLowMassDecision')",
      'HLT2FILTER' : None,
      'TisTosSpec' : None,
      'EProbNNe' : 0.1,
      'EIPChi2' : 4,
      'PT' : 1000*MeV,
      'MMIN' : 5*MeV,
      'MMAX' : 300*MeV
    },
    'Pi0ToDiEGammaFull' : {
      'input'       : 'Phys/StdLooseAllPhotons/Particles',
      'L0FILTER' : None,
      'HLT1FILTER' : "HLT_PASS_RE('Hlt1DiElectronLowMass.*Decision')",
      'HLT2FILTER' : None,
      'TisTosSpec' : None,
      'EProbNNe' : 0.1,
      'EIPChi2' : -1,
      'EEPT' : 1000*MeV,
      'GPT' : 500*MeV,
      'GCL' : 0.2,
      'PT' : 1000*MeV,
      'MMIN' : 0*MeV,
      'MMAX' : 300*MeV
    },
    'EtaToDiEGammaFull' : {
      'input'       : 'Phys/StdLooseAllPhotons/Particles',
      'L0FILTER' : None,
      'HLT1FILTER' : "HLT_PASS_RE('Hlt1DiElectronLowMass.*Decision')",
      'HLT2FILTER' : None,
      'TisTosSpec' : None,
      'EProbNNe' : 0.1,
      'EIPChi2' : -1,
      'EEPT' : 1000*MeV,
      'GPT' : 500*MeV,
      'GCL' : 0.2,
      'PT' : 1000*MeV,
      'MMIN' : 400*MeV,
      'MMAX' : 700*MeV
    },
    'DisplDiELowMassMicro' : {
      'L0FILTER' : None,  #"L0_CHANNEL('Photon')|L0_CHANNEL('Electron')
      'HLT1FILTER' : "HLT_PASS_RE('Hlt1DiElectronLowMassDecision')",
      'HLT2FILTER' : None,
      'TisTosSpec' : None,
      'EProbNNe' : 0.1,
      'EIPChi2' : 4,
      'PT' : 1000*MeV,
      'MMIN' : 5*MeV,
      'MMAX' : 300*MeV
    },
    'Pi0ToDiEGammaMicro' : {
      'input'       : 'Phys/StdLooseAllPhotons/Particles',
      'L0FILTER' : None,
      'HLT1FILTER' : "HLT_PASS_RE('Hlt1DiElectronLowMass.*Decision')",
      'HLT2FILTER' : None,
      'TisTosSpec' : None,
      'EProbNNe' : 0.1,
      'EIPChi2' : -1,
      'EEPT' : 1000*MeV,
      'GPT' : 500*MeV,
      'GCL' : 0.2,
      'PT' : 1000*MeV,
      'MMIN' : 0*MeV,
      'MMAX' : 300*MeV
    },
    'EtaToDiEGammaMicro' : {
      'input'       : 'Phys/StdLooseAllPhotons/Particles',
      'L0FILTER' : None,
      'HLT1FILTER' : "HLT_PASS_RE('Hlt1DiElectronLowMass.*Decision')",
      'HLT2FILTER' : None,
      'TisTosSpec' : None,
      'EProbNNe' : 0.1,
      'EIPChi2' : -1,
      'EEPT' : 1000*MeV,
      'GPT' : 500*MeV,
      'GCL' : 0.2,
      'PT' : 1000*MeV,
      'MMIN' : 400*MeV,
      'MMAX' : 700*MeV
    },
    'DisplDiMuon': {
      # 'TOS_HLT2'  : 'Hlt2ExoticaDisplDiMuonDecision',
      'MuProbNNmu': 0.5,
      'MuIPChi2'  : 4,
      'MaxMM'     : 500*MeV,
      'PT'        : 1000*MeV,
      'IPChi2'    : 16,
      'FDChi2'    : 4,
    },
    'DisplDiMuonHighMass': {
      # 'TOS_HLT2'  : 'Hlt2ExoticaDisplDiMuonDecision',
      'MuProbNNmu': 0.8,
      'MuIPChi2'  : 9,
      'MinMM'     : 500*MeV,
      'PT'        : 1000*MeV,
      'IPChi2'    : 16,
      'FDChi2'    : 4,
      'TAU'       : 1*picosecond,
    },
    'DisplDiMuonNoPoint': {
      # 'TOS_HLT2'  : 'Hlt2ExoticaDisplDiMuonNoPointDecision',
      'MSwitch'             : 500*MeV,
      'MuProbNNmu_lowmass'  : 0.5,
      'MuProbNNmu_highmass' : 0.8,
      'MuIPChi2_lowmass'    : 4,
      'MuIPChi2_highmass'   : 25,
      'PT'                  : 1000*MeV,
      'FDChi2'              : 16,
      'R'                   : 0*mm,
    },
    'DisplDiMuonNoPointR': {
      # 'TOS_HLT2'  : 'Hlt2ExoticaDisplDiMuonNoPointDecision',
      'MSwitch'             : 500*MeV,
      'MuProbNNmu_lowmass'  : 0.5,
      'MuProbNNmu_highmass' : 0.8,
      'MuIPChi2_lowmass'    : 4,
      'MuIPChi2_highmass'   : 25,
      'PT'                  : 1000*MeV,
      'FDChi2'              : 16,
      'R'                   : 2.75*mm,
    },
    'PrmptDiMuonHighMass': {
      # 'TOS_HLT2'  : 'Hlt2ExoticaPrmptDiMuonHighMassDecision',
      'MuPT'         : 500*MeV,
      'MuPTPROD'     : 1*GeV*GeV,
      'MuP'          : 10000*MeV,
      'M'            : 3200*MeV,
      'M_switch_ab'  : 740*MeV,
      'M_switch_bc'  : 1100*MeV,
      'M_switch_cd'  : 3000*MeV,
      'M_switch_de'  : 3200*MeV,
      'M_switch_ef'  : 9000*MeV,
      'MuProbNNmu_a' : 0.8,
      'MuProbNNmu_b' : 0.8,
      'MuProbNNmu_c' : 0.95,
      'MuProbNNmu_d' : 2.0,
      'MuProbNNmu_e' : 0.95,
      'MuProbNNmu_f' : 0.9,
      'MuIPChi2'     : 6,
      'PT'           : 1000*MeV,
      'FDChi2'       : 45
     },
    'QuadMuonNoIP': {
      # 'TOS_HLT2'  : 'Hlt2ExoticaQuadMuonNoIPDecision',
      'PT'        : 0,
      'VChi2'     : 10,
    },
  },
}

#===============================================================================

def DisplPhiPhi( conf ):

  inputs = [ AutomaticData(Location=conf['input'])  ]

  dc = ("TOS('%(TisTosSpec)s','Hlt1TriggerTisTos')"
        "& (PT > %(PhiPT)s) "
        "& (MINTREE('K+'==ABSID,PT) > %(KPT)s) "
        "& (MINTREE('K+'==ABSID,BPVIPCHI2()) > %(KIPChi2)s) "
        "& (MAXTREE('K+'==ABSID,TRGHOSTPROB) < %(GhostProb)s) "
        "& (MINTREE('K+'==ABSID,PROBNNK) > %(KProbNNk)s) "
        "& (VFASPF(VCHI2PDOF) < %(VChi2)s) "
        "& (in_range( PDGM('phi(1020)') - %(PhiMassWindow)s , M , PDGM('phi(1020)') + %(PhiMassWindow)s ) )")%conf

  mc =  ("(HASVERTEX)"
         "& (VFASPF(VCHI2) < %(VChi2)s) "
         "& (BPVVDCHI2 > %(FDChi2)s)")%conf

  ## TOS on HLT2 on-demand
  if 'TOS_HLT2' in conf:
    mc += '& (TOS("%(TOS_HLT2)s", "Hlt2TriggerTisTos"))'%conf

  return SimpleSelection( 'DisplPhiPhi', CombineParticles, inputs,
    DecayDescriptor = 'B0 -> phi(1020) phi(1020)',
    DaughtersCuts   = {'phi(1020)': dc},
    MotherCut       = mc,
  )


def SharedRHNu( conf ):

  inputs = [AutomaticData(Location = loc) for loc in conf['input']]

  dc = {}
  dc['mu+'] = ("(PT > %(PT)s) "
               "& (P > %(P)s) "
               "& (BPVIPCHI2() > %(IPChi2)s) "
               "& (TRGHOSTPROB < %(GhostProb)s) "
               "& (PROBNNmu > %(ProbNNmu)s) ")%conf
  dc['pi+'] = ("(PT > %(PT)s) "
               "& (P > %(P)s) "
               "& (BPVIPCHI2() > %(IPChi2)s) "
               "& (TRGHOSTPROB < %(GhostProb)s)")%conf
  cc = "(APT > 2*%(PT)s) &  (AM > %(M)s) & (ACUTDOCACHI2(%(VChi2)s,''))"%conf
  mc = ("(PT > 2*%(PT)s)"
        "& (HASVERTEX)"
        "& (VFASPF(VCHI2PDOF) < %(VChi2)s) "
        "& (BPVIPCHI2() < %(XIPChi2)s)"
        "& (BPVVDCHI2 > %(FDChi2)s)"
        "& (BPVLTIME() > %(TAU)s)")%conf

  return SimpleSelection( 'SharedRHNu', CombineParticles, inputs,
    DecayDescriptor = "[KS0 -> mu+ pi-]cc",
    CombinationCut  = cc,
    DaughtersCuts   = dc,
    MotherCut       = mc
  )

def RHNu( conf, name, sharedRHNu ):
  code = ("(M > %(M)s)"
          "& (HASVERTEX)"
          "& (BPVLTIME() > %(TAU)s)")%conf

  ## TOS on HLT2 on-demand
  if 'TOS_HLT2' in conf:
    code += '& (TOS("%(TOS_HLT2)s", "Hlt2TriggerTisTos"))'%conf

  return SimpleSelection( name, FilterDesktop, sharedRHNu, Code=code)

def DiRHNu( conf, sharedRHNu ):

  cc = "APT > %(PT)s"%conf
  mc =  ("(HASVERTEX)"
         "& (VFASPF(VCHI2) < %(VChi2)s) ")%conf

  return SimpleSelection( 'DiRHNu', CombineParticles, sharedRHNu,
    DecayDescriptor = "B0 -> KS0 KS0",
    CombinationCut  = cc,
    MotherCut       = mc,
  )

#-------------------------------------------------------------------------------

def SharedDiENoIP( conf ):

  inputs = [ AutomaticData(Location=conf['input'])  ]

  dc = ("(PT > %(EPT)s) "
        "& (P > %(EP)s) "
        "& (TRGHOSTPROB < %(GhostProb)s) "
        "& (PROBNNe > %(EProbNNe)s) ")%conf
  cc = "(AMAXDOCA('') < %(DOCA)s) & (AM > %(MM)s)"%conf
  mc = "(VFASPF(VCHI2PDOF) < %(VChi2)s) "%conf

  return SimpleSelection( 'SharedDiENoIP', CombineParticles, inputs,
    DecayDescriptor = "KS0 -> e+ e-",
    DaughtersCuts   = {'e+':dc, 'e-':dc},
    CombinationCut  = cc,
    MotherCut       = mc,
  )

def SharedDiMuonNoIP( conf ):

  inputs = [ AutomaticData(Location=conf['input'])  ]

  dc = ("(PT > %(MuPT)s) "
        "& (P > %(MuP)s) "
        "& (TRGHOSTPROB < %(GhostProb)s) "
        "& (PROBNNmu > %(MuProbNNmu)s) ")%conf
  cc = "(AMAXDOCA('') < %(DOCA)s)"%conf
  mc = "(VFASPF(VCHI2PDOF) < %(VChi2)s) "%conf

  return SimpleSelection( 'SharedDiMuonNoIP', CombineParticles, inputs,
    DecayDescriptor = "KS0 -> mu+ mu-",
    DaughtersCuts   = {'mu+':dc, 'mu-':dc},
    CombinationCut  = cc,
    MotherCut       = mc,
  )

def DisplDiE( conf, name, sharedDiE ):
  code = ("TOS('%(TisTosSpec)s','Hlt1TriggerTisTos')"
          "& (MINTREE('e+'==ABSID,BPVIPCHI2()) > %(EIPChi2)s)"
          "& (MINTREE('e+'==ABSID,PROBNNe) > %(EProbNNe)s)"
          "& (PT > %(PT)s)"
          "& (HASVERTEX)"
          "& (BPVIPCHI2() < %(IPChi2)s)"
          "& (BPVVDCHI2 > %(FDChi2)s)")%conf
  if 'TAU' in conf: code += "& (BPVLTIME() > %(TAU)s)"%conf
  if 'MinMM' in conf: code += "& (MM > %(MinMM)s)"%conf
  if 'MaxMM' in conf: code += "& (MM < %(MaxMM)s)"%conf

  ## TOS on HLT2 on-demand
  if 'TOS_HLT2' in conf:
    code += '& (TOS("%(TOS_HLT2)s", "Hlt2TriggerTisTos"))'%conf

  return SimpleSelection( name, FilterDesktop, sharedDiE, Code=code)


def DisplDiELowMass( conf, name, sharedDiE ):

  code = ("(MINTREE('e+'==ABSID,BPVIPCHI2()) > %(EIPChi2)s)"
            "& (MINTREE('e+'==ABSID,PROBNNe) > %(EProbNNe)s)"
            "& (PT > %(PT)s)"
            "& (HASVERTEX)"
            "& (M > %(MMIN)s) & (M < %(MMAX)s)")%conf

  return SimpleSelection( name, FilterDesktop, sharedDiE, Code=code)

def DiEGamma(conf, name, sharedDiE ):

  inputs = [ AutomaticData(Location=conf['input'])  ] + sharedDiE

  dc = {}
  dc['KS0'] = ("(MINTREE('e+'==ABSID,BPVIPCHI2()) > %(EIPChi2)s)"
          "& (MINTREE('e+'==ABSID,PROBNNe) > %(EProbNNe)s)"
          "& (PT > %(EEPT)s)"
          "& (HASVERTEX)")%conf

  dc['gamma'] = '(PT > %(GPT)s) & (CL > %(GCL)s)'%conf

  cc = "(APT > %(PT)s) &  (AM > %(MMIN)s) & (AM < %(MMAX)s)"%conf
  mc = "(PT > %(PT)s) &  (M > %(MMIN)s) & (M < %(MMAX)s)"%conf



  return SimpleSelection( name, CombineParticles, inputs,
    DecayDescriptor = "rho(770)0 -> KS0 gamma",
    DaughtersCuts   = dc,
    CombinationCut  = cc,
    MotherCut       = mc,
  )




def DisplDiMuon( conf, name, sharedDiMuon ):
  code = ("(MINTREE('mu+'==ABSID,BPVIPCHI2()) > %(MuIPChi2)s)"
          "& (MINTREE('mu+'==ABSID,PROBNNmu) > %(MuProbNNmu)s)"
          "& (PT > %(PT)s)"
          "& (HASVERTEX)"
          "& (BPVIPCHI2() < %(IPChi2)s)"
          "& (BPVVDCHI2 > %(FDChi2)s)")%conf
  if 'TAU' in conf: code += "& (BPVLTIME() > %(TAU)s)"%conf
  if 'MinMM' in conf: code += "& (MM > %(MinMM)s)"%conf
  if 'MaxMM' in conf: code += "& (MM < %(MaxMM)s)"%conf

  ## TOS on HLT2 on-demand
  if 'TOS_HLT2' in conf:
    code += '& (TOS("%(TOS_HLT2)s", "Hlt2TriggerTisTos"))'%conf

  return SimpleSelection( name, FilterDesktop, sharedDiMuon, Code=code)

def DisplDiMuonNoPoint( conf, name, sharedDiMuon ):
  code = ("(((MM < %(MSwitch)s) & (MINTREE('mu+'==ABSID,BPVIPCHI2()) > %(MuIPChi2_lowmass)s)) | ((MM > %(MSwitch)s) & (MINTREE('mu+'==ABSID,BPVIPCHI2()) > %(MuIPChi2_highmass)s)))"
          "&(((MM < %(MSwitch)s) & (MINTREE('mu+'==ABSID,PROBNNmu) > %(MuProbNNmu_lowmass)s)) | ((MM > %(MSwitch)s) & (MINTREE('mu+'==ABSID,PROBNNmu) > %(MuProbNNmu_highmass)s)))"
          "& (PT > %(PT)s)"
          "& (HASVERTEX)"
          "& (BPVVDCHI2 > %(FDChi2)s)"
          "& (BPVVDR > %(R)s)")%conf

  ## TOS on HLT2 on-demand
  if 'TOS_HLT2' in conf:
    code += '& (TOS("%(TOS_HLT2)s", "Hlt2TriggerTisTos"))'%conf

  return SimpleSelection( name, FilterDesktop, sharedDiMuon, Code=code)

def PrmptDiMuonHighMass( conf, sharedDiMuon ):
  nregions = 6
  region_names = ['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h']

  code = '(M > %(M)s) &'
  code += ("(MINTREE('mu+'==ABSID,PT) > %(MuPT)s) "
           "& (MINTREE('mu+'==ABSID,P) > %(MuP)s)"
           "& (MINTREE('mu+'==ABSID,BPVIPCHI2()) < %(MuIPChi2)s)")
  for n in range(nregions):
    if n==0: code += "& ( ((M < %(M_switch_{1}{2})s) & (MINTREE('mu+'==ABSID,PROBNNmu) > %(MuProbNNmu_{1})s)) ".format(None,region_names[n],region_names[n+1])
    elif n==nregions-1: code += "  | ((M > %(M_switch_{0}{1})s) & (MINTREE('mu+'==ABSID,PROBNNmu) > %(MuProbNNmu_{1})s)) )".format(region_names[n-1], region_names[n], None)
    else: code += "  | ((M > %(M_switch_{0}{1})s) & (M < %(M_switch_{1}{2})s) & (MINTREE('mu+'==ABSID,PROBNNmu) > %(MuProbNNmu_{1})s))".format(region_names[n-1], region_names[n],region_names[n+1])
  code += ("& (CHILD(PT,1)*CHILD(PT,2) > %(MuPTPROD)s)"
           "& (PT > %(PT)s)"
           "& (HASVERTEX)"
           "& (BPVVDCHI2 < %(FDChi2)s)")
  code = code%conf

  ## TOS on HLT2 on-demand
  if 'TOS_HLT2' in conf:
    code += '& (TOS("%(TOS_HLT2)s", "Hlt2TriggerTisTos"))'%conf

  return SimpleSelection( 'PrmptDiMuonHighMass', FilterDesktop, sharedDiMuon, Code=code)

def QuadMuonNoIP( conf, sharedDiMuon ):
  cc = "APT > %(PT)s"%conf
  mc = "(HASVERTEX) & (VFASPF(VCHI2) < %(VChi2)s)"%conf

  ## TOS on HLT2 on-demand
  if 'TOS_HLT2' in conf:
    mc += '& (TOS("%(TOS_HLT2)s", "Hlt2TriggerTisTos"))'%conf

  return SimpleSelection( 'QuadMuonNoIP', CombineParticles, sharedDiMuon,
    DecayDescriptor = 'B0 -> KS0 KS0',
    CombinationCut  = cc,
    MotherCut       = mc,
  )

#===============================================================================

class ExoticaConf(LineBuilder):

  __configuration_keys__ = default_config['CONFIG'].keys()  # Legacy field

  def __init__(self, name, config):
    LineBuilder.__init__(self, name, config)

    ## Inject 'Common' into all other confs
    d = config.pop('Common')
    for key in config:
      config[key].update(d)

    ## Shared.
    sharedRHNu = [SharedRHNu(config['SharedRHNu'])]
    sharedDiE = [SharedDiENoIP(config['SharedDiENoIP'])]
    sharedDiMuon = [SharedDiMuonNoIP(config['SharedDiMuonNoIP'])]

    ## Register lines
    prescales = config['Prescales']

    self.registerLine(StrippingLine( name+'DisplPhiPhiLine',
      selection = DisplPhiPhi(config['DisplPhiPhi']),
      prescale  = prescales['DisplPhiPhi'],
    ))

    self.registerLine(StrippingLine( name+'RHNuLine',
      selection = RHNu(config['RHNu'], 'RHNu', sharedRHNu),
      prescale  = prescales['RHNu'],
    ))

    self.registerLine(StrippingLine( name+'RHNuHighMassLine',
      selection = RHNu(config['RHNuHighMass'], 'RHNuHighMass', sharedRHNu),
      prescale  = prescales['RHNuHighMass'],
    ))

    self.registerLine(StrippingLine( name+'DiRHNuLine',
      selection = DiRHNu(config['DiRHNu'], sharedRHNu),
      prescale  = prescales['DiRHNu'],
    ))

    self.registerLine(StrippingLine( name+'DisplDiELine',
      selection = DisplDiE(config['DisplDiE'], 'DisplDiE', sharedDiE),
      prescale  = prescales['DisplDiE'],
    ))

    self.registerLine(StrippingLine( name+'DisplDiEHighMassLine',
      selection = DisplDiE(config['DisplDiEHighMass'],
                           'DisplDiEHighMass', sharedDiE),
      prescale  = prescales['DisplDiEHighMass'],
    ))

    self.registerLine(StrippingLine( name+'DisplDiELowMassFullLine',
      selection = DisplDiELowMass(config['DisplDiELowMassFull'], 'DisplDiELowMassFull', sharedDiE),
      prescale  = prescales['DisplDiELowMassFull'],
      L0DU= config['DisplDiELowMassFull']['L0FILTER'],
      HLT1= config['DisplDiELowMassFull']['HLT1FILTER'],
      HLT2= config['DisplDiELowMassFull']['HLT2FILTER'],
    ))

    self.registerLine(StrippingLine( name+'Pi0ToDiEGammaFullLine',
      selection = DiEGamma(config['Pi0ToDiEGammaFull'], 'Pi0ToDiEGammaFull', sharedDiE),
      prescale  = prescales['Pi0ToDiEGammaFull'],
      L0DU= config['Pi0ToDiEGammaFull']['L0FILTER'],
      HLT1= config['Pi0ToDiEGammaFull']['HLT1FILTER'],
      HLT2= config['Pi0ToDiEGammaFull']['HLT2FILTER'],
    ))

    self.registerLine(StrippingLine( name+'EtaToDiEGammaFullLine',
      selection = DiEGamma(config['EtaToDiEGammaFull'], 'EtaToDiEGammaFull', sharedDiE),
      prescale  = prescales['EtaToDiEGammaFull'],
      L0DU= config['EtaToDiEGammaFull']['L0FILTER'],
      HLT1= config['EtaToDiEGammaFull']['HLT1FILTER'],
      HLT2= config['EtaToDiEGammaFull']['HLT2FILTER'],
    ))

    self.registerLine(StrippingLine( name+'DisplDiELowMassMicroLine',
      selection = DisplDiELowMass(config['DisplDiELowMassMicro'], 'DisplDiELowMassMicro', sharedDiE),
      prescale  = prescales['DisplDiELowMassMicro'],
      L0DU= config['DisplDiELowMassMicro']['L0FILTER'],
      HLT1= config['DisplDiELowMassMicro']['HLT1FILTER'],
      HLT2= config['DisplDiELowMassMicro']['HLT2FILTER'],
    ))

    self.registerLine(StrippingLine( name+'Pi0ToDiEGammaMicroLine',
      selection = DiEGamma(config['Pi0ToDiEGammaMicro'], 'Pi0ToDiEGammaMicro', sharedDiE),
      prescale  = prescales['Pi0ToDiEGammaMicro'],
      L0DU= config['Pi0ToDiEGammaMicro']['L0FILTER'],
      HLT1= config['Pi0ToDiEGammaMicro']['HLT1FILTER'],
      HLT2= config['Pi0ToDiEGammaMicro']['HLT2FILTER'],
    ))

    self.registerLine(StrippingLine( name+'EtaToDiEGammaMicroLine',
      selection = DiEGamma(config['EtaToDiEGammaMicro'], 'EtaToDiEGammaMicro', sharedDiE),
      prescale  = prescales['EtaToDiEGammaMicro'],
      L0DU= config['EtaToDiEGammaMicro']['L0FILTER'],
      HLT1= config['EtaToDiEGammaMicro']['HLT1FILTER'],
      HLT2= config['EtaToDiEGammaMicro']['HLT2FILTER'],
    ))

    self.registerLine(StrippingLine( name+'DisplDiMuonLine',
      selection = DisplDiMuon(config['DisplDiMuon'],
                              'DisplDiMuon', sharedDiMuon),
      prescale  = prescales['DisplDiMuon'],
    ))

    self.registerLine(StrippingLine( name+'DisplDiMuonHighMassLine',
      selection = DisplDiMuon(config['DisplDiMuonHighMass'],
                              'DisplDiMuonHighMass', sharedDiMuon),
      prescale  = prescales['DisplDiMuonHighMass'],
    ))

    self.registerLine(StrippingLine( name+'DisplDiMuonNoPointLine',
      selection = DisplDiMuonNoPoint(config['DisplDiMuonNoPoint'],
                                     'DisplDiMuonNoPoint', sharedDiMuon),
      prescale  = prescales['DisplDiMuonNoPoint'],
    ))

    self.registerLine(StrippingLine( name+'DisplDiMuonNoPointRLine',
      selection = DisplDiMuonNoPoint(config['DisplDiMuonNoPointR'],
                                     'DisplDiMuonNoPointR', sharedDiMuon),
      prescale  = prescales['DisplDiMuonNoPointR'],
    ))

    self.registerLine(StrippingLine( name+'PrmptDiMuonHighMassLine',
      selection = PrmptDiMuonHighMass(config['PrmptDiMuonHighMass'],
                                      sharedDiMuon),
      prescale  = prescales['PrmptDiMuonHighMass'],
    ))

    self.registerLine(StrippingLine( name+'QuadMuonNoIPLine',
      selection = QuadMuonNoIP(config['QuadMuonNoIP'], sharedDiMuon),
      prescale  = prescales['QuadMuonNoIP'],
    ))
