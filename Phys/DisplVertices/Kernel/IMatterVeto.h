/*****************************************************************************\
* (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// $Id: IJets2Jets.h,v 1.1 2009-11-10 12:54:14 cocov Exp $
// ============================================================================
// CVS tag $Name: not supported by cvs2svn $ , version $Revision: 1.1 $
// ============================================================================
#ifndef KERNEL_IMATTERVETO_H 
#define KERNEL_IMATTERVETO_H 1

// ============================================================================
// Include files
// ============================================================================
// STD & STL 
// ============================================================================
#include <string>
#include <vector>
// ============================================================================
// GaudiKernel
// ============================================================================
#include "GaudiKernel/IAlgTool.h"
#include "GaudiKernel/Point3DTypes.h"

class GAUDI_API IMatterVeto : public virtual IAlgTool 
{
public: 
  // ==========================================================================
  /// interface machinery 
  DeclareInterfaceID(IMatterVeto, 1, 0);
  // ==========================================================================
public:
  /// the main method
  virtual bool isInMatter( const  Gaudi::XYZPoint &point ) const = 0 ;
};
// ============================================================================
// The END 
// ============================================================================
#endif // DAVINCIKERNEL_IMATTERVETO_H
// ============================================================================

