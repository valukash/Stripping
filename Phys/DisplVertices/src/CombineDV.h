/*****************************************************************************\
* (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// $Id$
#ifndef COMBINEDV_H
#define COMBINEDV_H 1

// Include files
// from Gaudi
#include "GaudiAlg/GaudiAlgorithm.h"

#include "Kernel/IParticleCombiner.h"
#include "GaudiKernel/AlgFactory.h"

/** @class CombineDV CombineDV.h
 *  Merge DV candidates into one candidate
 *
 *  @author Veerle Heijne
 *  @date   2011-10-12
 */
class CombineDV : public GaudiAlgorithm {
public:
  /// Standard constructor
  CombineDV( const std::string& name, ISvcLocator* pSvcLocator );

  virtual ~CombineDV( ); ///< Destructor

  StatusCode initialize() override;    ///< Algorithm initialization
  StatusCode execute   () override;    ///< Algorithm execution
  StatusCode finalize  () override;    ///< Algorithm finalization

protected:

private:
  std::string m_candidateLocation;  //input TES location input candidates to be merged
  std::string m_outputLocation;     //output TES location of merged candidate
  IParticleCombiner*  m_combiner;   //Combiner tool
  std::string  m_combinerName;      //MomentumCombiner



};
#endif // COMBINEDV_H
