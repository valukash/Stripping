/*****************************************************************************\
* (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// $Id: StrippingCheck.cpp,v 1.1 2009/10/13 13:12:34 poluekt Exp $
// Include files

// from Gaudi
#include "GaudiKernel/AlgFactory.h"
#include "Event/Particle.h"

// local
#include "StrippingCheck.h"

//-----------------------------------------------------------------------------
// Implementation file for class : StrippingCheck
//
// 2009-02-06 : Anton Polouektov
//-----------------------------------------------------------------------------

//=============================================================================
// Main execution
//=============================================================================
StatusCode StrippingCheck::execute()
{
  const bool selected = ( numberOfCandidates(m_inputLocation) > 0 );

  setFilterPassed( selected );

  return StatusCode::SUCCESS;
}

unsigned int 
StrippingCheck::numberOfCandidates(const std::string& selalgo) const
{
  const LHCb::Particle::Range parts = getIfExists<LHCb::Particle::Range>(selalgo);
  const unsigned int num = parts.size();
  
  if (msgLevel(MSG::VERBOSE))
    verbose() << "Selection " << selalgo << " finds " << num
              << " candidates" << endmsg ;

  return num;
}

//=============================================================================

// Declaration of the Algorithm Factory
DECLARE_ALGORITHM_FACTORY( StrippingCheck )

//=============================================================================

