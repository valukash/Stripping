/*****************************************************************************\
* (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include "GaudiKernel/AlgFactory.h" 
#include "StrippingTCK.h"

#include "Event/HltDecReports.h"

//-----------------------------------------------------------------------------
// Implementation file for class : StrippingTCK
//
// A simple algorithm that sets the TCK field in the stripping DecReports
// structure
//
// 2010-09-20 : Anton Poluektov
//-----------------------------------------------------------------------------

// Declaration of the Algorithm Factory
DECLARE_ALGORITHM_FACTORY( StrippingTCK )

//=============================================================================
// Main execution
//=============================================================================
StatusCode StrippingTCK::execute() {

  if ( msgLevel(MSG::DEBUG) ) debug() << "==> Execute" << endmsg;

  LHCb::HltDecReports* reports = get<LHCb::HltDecReports>(m_hdrLocation);

  if (reports) {
    reports->setConfiguredTCK(m_tck);
    if ( msgLevel(MSG::VERBOSE) ) verbose() << "Set stripping TCK = " << m_tck <<
      "in " << m_hdrLocation << endmsg;
  } else {
    warning() << "No stripping DecReports found at " << m_hdrLocation << endmsg;
  }

  setFilterPassed( true );
  return StatusCode::SUCCESS;
}

//=============================================================================
