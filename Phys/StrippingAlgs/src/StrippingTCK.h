/*****************************************************************************\
* (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef STRIPPINGTCK_H
#define STRIPPINGTCK_H 1

// Include files
// from Gaudi
#include "GaudiAlg/GaudiAlgorithm.h"

/** @class StrippingTCK StrippingTCK.h
 *
 * A simple algorithm that sets the TCK field in the stripping DecReports
 * structure
 *
 *  @author Anton Poluektov
 *  @date   2010-09-20
 */
class StrippingTCK : public GaudiAlgorithm
{

public:

  /// Standard constructor
  using GaudiAlgorithm::GaudiAlgorithm;

  StatusCode execute() override; ///< Algorithm execution

private:
  Gaudi::Property<std::string> m_hdrLocation {this, "HDRLocation", "Strip/Phys/DecReports"};
  Gaudi::Property<unsigned int> m_tck {this, "TCK", 0};
};

#endif // StrippingTCK_H
