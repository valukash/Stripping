/*****************************************************************************\
* (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include "Kernel/SelectionLine.h"
#include "GaudiKernel/AlgFactory.h"
#include "GaudiKernel/VectorMap.h"
#include "Event/Particle.h"

class StrippingAlg : public Selection::Line
{

public:
  /// Standard constructor
  using Selection::Line::Line;
  StatusCode initialize() override;

private:
  using Selection::Line::numberOfCandidates;
  int numberOfCandidates() const override;
  const std::pair<std::string,unsigned>& id() const override;

  std::pair<std::string,unsigned> m_id;
  Gaudi::Property<std::string> m_outputLocation {this, "OutputLocation"};

};

StatusCode StrippingAlg::initialize()
{
  StatusCode sc = Selection::Line::initialize();
  if ( sc.isFailure() ) return sc;
  m_id = std::make_pair(std::string(decisionName()), 1);
  return sc;
}

int StrippingAlg::numberOfCandidates() const
{
  const LHCb::Particle::Range parts = getIfExists<LHCb::Particle::Range>(m_outputLocation);
  const int num = parts.size();

  if ( msgLevel(MSG::VERBOSE))
    verbose() << "Selection " << m_outputLocation << " finds " << num
              << " candidates" << endmsg ;

  return num;
}


const std::pair<std::string,unsigned>& StrippingAlg::id() const
{
  return m_id;
}


// Declaration of the Algorithm Factory
DECLARE_ALGORITHM_FACTORY( StrippingAlg )

