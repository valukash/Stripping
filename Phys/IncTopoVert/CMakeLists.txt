###############################################################################
# (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
################################################################################
# Package: IncTopoVert
################################################################################
gaudi_subdir(IncTopoVert v1r10p2)

gaudi_depends_on_subdirs(GaudiAlg
                         Event/RecEvent
                         Event/HltEvent
                         Event/PhysEvent
                         Event/L0Event
                         Event/MCEvent
                         Kernel/LHCbMath
                         Phys/DaVinciInterfaces
                         Phys/DaVinciKernel)

gaudi_install_headers(IncTopoVert)

find_package(Boost)
find_package(ROOT)
include_directories(SYSTEM ${Boost_INCLUDE_DIRS} ${ROOT_INCLUDE_DIRS})

gaudi_add_module(IncTopoVert
                 src/*.cpp
                 LINK_LIBRARIES GaudiAlgLib RecEvent TrackEvent PhysEvent
                                MCEvent HltEvent LHCbMathLib
                                DaVinciInterfacesLib DaVinciKernelLib)
