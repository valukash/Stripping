/*****************************************************************************\
* (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// $Id: $
#ifndef IPSTRACKTYPEFILTERVTXTOPOTRACKSTOOL_H
#define IPSTRACKTYPEFILTERVTXTOPOTRACKSTOOL_H 1

// Include files
// from Gaudi
#include "GaudiAlg/GaudiTool.h"
#include "Kernel/IDistanceCalculator.h"
#include "IncTopoVert/IFilterVtxTopoTracksTool.h"            // Interface
#include "GaudiAlg/GaudiHistoTool.h"
//#include "GaudiKernel/NTuple.h"
#include "GaudiAlg/Tuple.h"
#include "GaudiAlg/TupleObj.h"
#include "Event/Particle.h"

using namespace std;

/** @class IpsFilterVtxTopoTracksTool IpsFilterVtxTopoTracksTool.h
 *  This class implements IFilterVtxTopoTracksTool. The selection is performed
 *  on the basis of the impact parameter significance of the track with respect
 *  to all primary vertices in the event.
 *
 *  @author Julien Cogan and Mathieu Perrin-Terrin
 *  @date   2012-11-27
 */
class IpsFilterVtxTopoTracksTool : public GaudiHistoTool, virtual public IFilterVtxTopoTracksTool {
public:
  /// Standard constructor
  IpsFilterVtxTopoTracksTool( const std::string& type,
                                       const std::string& name,
                                       const IInterface* parent);
  StatusCode initialize() override;

  std::vector<const LHCb::Track*> & filteredTracks(Tuples::Tuple* tuple = NULL) override;
  std::vector<const LHCb::Track*> & filteredTracks(std::vector<const LHCb::Track*> input_tracks, Tuples::Tuple* tuple = NULL) override;
  void SetParam(double p) override {
    m_ips = p;
  };

  virtual ~IpsFilterVtxTopoTracksTool( ); ///< Destructor

protected:

private:

  IDistanceCalculator*  m_Geom;
  std::string m_distanceCalculatorToolType; ///< Type of the distance calculator Tool


  //ips cut
  double                           m_ips;


  //container for tracks
  vector<const LHCb::Track*>  m_tracks;

};
#endif // IPSTRACKTYPEFILTERVTXTOPOTRACKSTOOL_H
