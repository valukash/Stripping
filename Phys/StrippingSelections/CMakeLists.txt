###############################################################################
# (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
################################################################################
# Package: StrippingSelections
################################################################################
gaudi_subdir(StrippingSelections v4r21p2)

gaudi_depends_on_subdirs(GaudiConfUtils
                         Kernel/LHCbAlgs
                         Phys/DaVinciKernel
                         Phys/LoKiCore
                         Phys/StrippingConf
                         Phys/StrippingSettings
                         Phys/StrippingUtils
                         Phys/TisTosTobbing
                         PhysSel/PhysSelPython)

gaudi_install_python_modules()


gaudi_add_test(QMTest QMTEST)
