###############################################################################
# (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""
Module importing stripping selection line builder modules
for QEE WG.
"""

list_S26r1 = [
  ## New lines
  'StrippingA1MuMu',  # not in 2015
  'StrippingLFVExotica',
  'StrippingW2nH',
  'StrippingZ02nH',

  ## Composite electrons bug-fix
  'StrippingDY2ee',
  'StrippingDitau',
  'StrippingWJets',
  'StrippingZ02ee',

  ## Tuned
  'StrippingWRareDecay',
  'StrippingZ0RareDecay',
  'StrippingDisplVertices',
  'StrippingWe',      # For Herschel
  'StrippingWMu',     # For Herschel
  'StrippingZ02MuMu', # For Herschel
]

## For Run-II spring-2016
list_S26 = [
  'StrippingDisplVertices',
  'StrippingDitau',
  'StrippingDY2ee',
  'StrippingDY2MuMu',
  'StrippingExotica',
  'StrippingFullDiJets',
  'StrippingH24Mu',
  'StrippingHighPtTau',
  'StrippingLb2dp',
  'StrippingLLP2MuX',
  'StrippingLowMultINC',
  'StrippingMicroDiJets',
  'StrippingMuMuSS',
  'StrippingSbarSCorrelations',
  'StrippingSingleTrackTIS',
  'StrippingStrangeBaryons',
  'StrippingStrangeBaryonsNoPID',
  'StrippingTaggedJets',
  'StrippingWe',
  'StrippingWMu',
  'StrippingZ02ee',
  'StrippingZ02MuMu',
  ## NEW
  'StrippingWJets',
  'StrippingWRareDecay',
  'StrippingZ0RareDecay',
  'StrippingA1MuMu',
  'StrippingA2MuMu',
  'StrippingA2MuMuSameSign',
  'StrippingHltQEE',
  'StrippingInclQQ', # renamed from InclbJets
]

## For autumn-2015 Run-II End-of-2015 restripping (S24)
## https://twiki.cern.ch/twiki/bin/view/LHCb/LHCbStripping24
list_S24 = (
  ## These line has explicit request from users for Run-II measurement.
  'StrippingDitau',
  'StrippingDisplVertices',
  'StrippingH24Mu',
  'StrippingHighPtTau',
  'StrippingInclbJets',
  'StrippingLb2dp',
  'StrippingLowMultINC',
  'StrippingMuMuSS',
  'StrippingSingleTrackTIS',
  'StrippingTaggedJets',          # Need Phys/JetTagging post-v1r9
  'StrippingWMu',
  'StrippingWmuAKTJets',
  'StrippingWeAKTJets',
  'StrippingZ02MuMu',

  ## These lines are recovered from S21 `just-in-case`,
  ## but there's no explicit request in S23+ yet.
  'StrippingDijets',
  'StrippingDY2ee',
  'StrippingDY2MuMu',
  'StrippingLLP2MuX',
  'StrippingSbarSCorrelations',
  'StrippingStrangeBaryons',
  'StrippingStrangeBaryonsNoPID',
  'StrippingWe',
  'StrippingZ02ee',

  ## Depreciated
  # 'StrippingHighPtTopoJets',
  # 'StrippingJets',
)

## For winter-2015 Run-I incremental stripping (S21r1p1, S21r0p1)
## https://twiki.cern.ch/twiki/bin/view/LHCb/LHCbStripping21%28r0%2Cr1%29p1
list_S21rXp1 = (
  'StrippingDitau',
  'StrippingFullDiJets',
  'StrippingLb2dp',
  'StrippingMicroDiJets',
  'StrippingTaggedJets',          # Need Phys/JetTagging post-v1r9
)

## For full restripping of 2016 data: S28 = S26 + S26r1
# This is an alternative plan after S26r1 (planned 2016 incremental) has the
# bandwidth too high.
list_S28 = list(set(list_S26 + list_S26r1))


list_S29 = list_S28 + ['StrippingConvertedPhoton']
list_S29.remove('StrippingHighPtTau')

##

list_S34 = list_S29
list_S34.append('StrippingDisplJets')
list_S34.remove('StrippingLowMultINC')
list_S34.remove('StrippingW2nH')
list_S34.remove('StrippingZ02nH')

## 

### 2018 incremental re-stripping
list_s34r0p1 = ['StrippingB2SS',
                'StrippingGluinos',
                'StrippingHighPtMuons',
                'StrippingLLP2HH',
                'StrippingLLP2Jets',
                'StrippingLFVExotica',
                'StrippingB2LLP2HH',
                ]    


#===============================================================================

## Choose the list to use here
_selections = list_s34r0p1

## Boilerplate codes
for _sel in _selections :
  try:
    __import__( '%s.%s'  % ( __name__, _sel ) )
  except Exception, x:
    print '[WARNING] Submodule %s.%s raises the exception "%s" and will be skipped !' % ( __name__,_sel,x )
    # raise # comment me out in production. Use for DEV

## Extract the successfully-imported modules
_strippingModules = [ val for key,val in dict(locals()).iteritems() if key.startswith('Stripping') ]

