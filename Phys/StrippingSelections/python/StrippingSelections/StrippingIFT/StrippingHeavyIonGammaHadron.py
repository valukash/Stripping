###############################################################################
# (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
'''
Stripping selections or Gamma+Hadron physics.
Based on those by Patrick K.
'''

__author__ = 'Cesar da Silva'
__date__ = '30/04/2017'
__version__ = '$Revision: 0 $'


__all__ = (
    'HeavyIonGammaHadronConf',
    'default_config'
    )

default_config =  {
    'NAME'            : 'HeavyIonGammaHadron',
    'WGs'             : ['IFT'],
    'STREAMS'         : ['GammaHadronLine'],
    'BUILDERTYPE'     : 'HeavyIonGammaHadronConf',
    'CONFIG'          : {
        "odin": ["NoBeam","Beam1","Beam2","BeamCrossing"],
        'CheckPV'    :  True,
        'MicroBiasPrescale'            :  1.0,
        'MicroBiasPostscale'           :  1.0, 
        "MicroBiasHlt1Filter"         : None, #"(HLT_PASS('Hlt1MBMicroBiasVeloDecision'))", #(HLT_PASS('Hlt1MB.*Decision')) #Hlt1MBMicroBiasVelo
        "MicroBiasHlt2Filter"         : None, #"(HLT_PASS('Hlt2PassThroughDecision'))|(HLT_PASS('Hlt2SMOGPhysicsDecision'))",

        'MicroBiasLowMultPrescale'            :  1.0,
        'MicroBiasLowMultPostscale'           :  1.0, 
        "MicroBiasLowMultHlt1Filter"         : None, #"(HLT_PASS('Hlt1MBMicroBiasLowMultVeloDecision'))", #(HLT_PASS('Hlt1MB.*Decision')) #Hlt1MBMicroBiasVelo
        "MicroBiasLowMultHlt2Filter"         : None, #"(HLT_PASS('Hlt2PassThroughDecision'))",
        'NoBiasPrescale'            :  1.0, #0.2,
        'NoBiasPostscale'           :  1.0, 
        "NoBiasHlt1Filter"         : None, #"(HLT_PASS('Hlt1MBNoBiasLeadingCrossingDecision'))", #(HLT_PASS('Hlt1MB.*Decision')) #Hlt1MBMicroBiasVelo
        "NoBiasHlt2Filter"         : None, #"(HLT_PASS('Hlt2PassThroughDecision'))",
        'gammaPT1'              : 1000    # MeV/c
        ,'gammaPT2'             : 1500    # MeV/c
        ,'gammaCL'             : 0.9     # adimensional
        ,'gammaConvPT1'         : 500    # MeV/c
        ,'gammaConvPT2'         : 1000    # MeV/c
        ,'gammaConvMDD'        : 200     # MeV/cc
        ,'gammaConvMLL'        : 60     # MeV/cc
        ,'gammaConvIPCHI'      : 0     # adimensional
        ,'NoConvHCAL2ECAL'     : 0.05   # adimensional
        ,'LLProbNNe'           : 0.    # adimensional
        ,'DDProbNNe'           : 0.    # adimensional
        ,'ConvGhostLL'         : 0.2    # adimensional
        ,'ConvGhostDD'         : 0.25    # adimensional
        }
    }

from Gaudi.Configuration import *
from StandardParticles           import StdLoosePhotons, StdAllLooseGammaLL, StdAllLooseGammaDD, StandardBasic
from Configurables               import FilterDesktop, CombineParticles
from StrippingConf.StrippingLine import StrippingLine
from StrippingUtils.Utils import LineBuilder
from PhysSelPython.Wrappers import Selection, DataOnDemand
from Configurables import RelInfoConeVariables

# Default config for the RelatedInfoTool framework.


class HeavyIonGammaHadronConf(LineBuilder):
    
    __configuration_keys__ = default_config['CONFIG'].keys()

    def __init__(self, name, config ): 
                
        LineBuilder.__init__(self, name, config)
        self.name = name 
        self.config = config
        print "inside GammaHadron",config
        odin = "|".join( ["(ODIN_BXTYP == LHCb.ODIN.%s)"%(odin_type,) for odin_type in config['odin'] if odin_type in ["NoBeam","Beam1","Beam2","BeamCrossing"]])

        fltrCode_LowPtLL = "(MAXTREE(ISBASIC,TRGHOSTPROB)<%(ConvGhostLL)s) & (M<%(gammaConvMLL)s*MeV) & (PT>(%(gammaConvPT1)s)*MeV) & (PT<(%(gammaConvPT2)s)*MeV) & (MAXTREE(ISBASIC,PPINFO( LHCb.ProtoParticle.ProbNNe,-1 ))>%(LLProbNNe)s) & INTREE('gamma'==ABSID)" % config
        self._trkFilter_LowPtLL = FilterDesktop(name = "LowPtLLFilterFor"+name, Code = fltrCode_LowPtLL )
        
        fltrCode_LL = "(MAXTREE(ISBASIC,TRGHOSTPROB)<%(ConvGhostLL)s) & (M<%(gammaConvMLL)s*MeV) & (PT>(%(gammaConvPT2)s)*MeV) & (MAXTREE(ISBASIC,PPINFO( LHCb.ProtoParticle.ProbNNe,-1 ))>%(LLProbNNe)s) & INTREE('gamma'==ABSID)" % config
        self._trkFilter_LL = FilterDesktop(name = "LLFilterFor"+name, Code = fltrCode_LL )

        fltrCode_LowPtDD = "(MAXTREE(ISBASIC,TRGHOSTPROB)<%(ConvGhostDD)s) & (M<%(gammaConvMDD)s*MeV) & (PT>%(gammaConvPT1)s*MeV) & (PT<%(gammaConvPT2)s*MeV) & (MAXTREE(ISBASIC,PPINFO( LHCb.ProtoParticle.ProbNNe,-1 ))>%(DDProbNNe)s) & INTREE('gamma'==ABSID)" % config
        self._trkFilter_LowPtDD = FilterDesktop(name = "LowPtDDFilterFor"+name, Code = fltrCode_LowPtDD )

        fltrCode_DD = "(MAXTREE(ISBASIC,TRGHOSTPROB)<%(ConvGhostDD)s) & (M<%(gammaConvMDD)s*MeV) & (PT>%(gammaConvPT2)s*MeV) & (MAXTREE(ISBASIC,PPINFO( LHCb.ProtoParticle.ProbNNe,-1 ))>%(DDProbNNe)s) & INTREE('gamma'==ABSID)" % config
        self._trkFilter_DD = FilterDesktop(name = "DDFilterFor"+name, Code = fltrCode_DD )

#        fltrCode_nonConvLowPt = "(PT>%(gammaPT1)s*MeV) & (CL>%(gammaCL)s) & (MAXTREE(ISBASIC,PPINFO( LHCb.ProtoParticle.CaloNeutralHcal2Ecal,1000 ))<%(NoConvHCAL2ECAL)s) & (MAXTREE(ISBASIC,PPINFO( LHCb.ProtoParticle.IsNotE,1000 ))>%(gammaCL)s) & (MAXTREE(ISBASIC,PPINFO( LHCb.ProtoParticle.IsNotH,1000 ))>%(gammaCL)s) & (MAXTREE(ISBASIC,PPINFO( LHCb.ProtoParticle.IsPhoton,1000 ))>%(gammaCL)s)" % config
#        self._trkFilterNonConvLowPt = FilterDesktop(name = "NoConvLowPtGammaFilterFor"+name, Code = fltrCode_nonConvLowPt )

        fltrCode_nonConv = "(PT>%(gammaPT2)s*MeV) & (CL>%(gammaCL)s) & (MAXTREE(ISBASIC,PPINFO( LHCb.ProtoParticle.CaloNeutralHcal2Ecal,1000 ))<%(NoConvHCAL2ECAL)s) & (MAXTREE(ISBASIC,PPINFO( LHCb.ProtoParticle.IsNotE,1000 ))>%(gammaCL)s) & (MAXTREE(ISBASIC,PPINFO( LHCb.ProtoParticle.IsNotH,1000 ))>%(gammaCL)s) & (MAXTREE(ISBASIC,PPINFO( LHCb.ProtoParticle.IsPhoton,1000 ))>%(gammaCL)s)" % config
        self._trkFilterNonConv = FilterDesktop(name = "NoConvGammaFilterFor"+name, Code = fltrCode_nonConv )

        self.convPhotons_LL = DataOnDemand(Location='Phys/StdAllLooseGammaLL/Particles')
        self.convPhotons_DD = DataOnDemand(Location='Phys/StdAllLooseGammaDD/Particles')
        stdPhotons     = DataOnDemand(Location='Phys/StdLoosePhotons/Particles')
        
        self.convPhotons_LowPtLL_clean = Selection( 'PhotonConvFilterLowPtLL' + name, Algorithm = self._trkFilter_LowPtLL, RequiredSelections = [self.convPhotons_LL])
        self.convPhotons_LowPtDD_clean = Selection( 'PhotonConvFilterLowPtDD' + name, Algorithm = self._trkFilter_LowPtDD, RequiredSelections = [self.convPhotons_DD])
        self.convPhotons_LL_clean = Selection( 'PhotonConvFilterLL' + name, Algorithm = self._trkFilter_LL, RequiredSelections = [self.convPhotons_LL])
        self.convPhotons_DD_clean = Selection( 'PhotonConvFilterDD' + name, Algorithm = self._trkFilter_DD, RequiredSelections = [self.convPhotons_DD])
 #       self.stdLowPtPhotons_clean = Selection( 'LowPtPhotonFilter' + name, Algorithm = self._trkFilterNonConvLowPt, RequiredSelections = [stdPhotons])
        self.stdPhotons_clean = Selection( 'PhotonFilter' + name, Algorithm = self._trkFilterNonConv, RequiredSelections = [stdPhotons])

        # implement isolation cuts
        relinfo = [{"Type" : "RelInfoConeIsolation",
                    "ConeSize" : 0.5,
                    "Location"  : "Iso05"}]
        
#        isocut_NoConvLowPtGamma = FilterDesktop(name+"NoConvLowPtGammaIsoLine",
#                                                Code="RELINFO('/Event/Phys/NoConvLowPtGamma/Iso05', 'NC_MULT', 1000) < 3",
#                                                Inputs=['Phys/NoConvLowPtGamma/Particles'])
        isocut_NoConvGamma = FilterDesktop(name+"NoConvGammaIsoLine",
                                                Code="RELINFO('/Event/Phys/NoConvGamma/Iso05', 'NC_MULT', 1000) < 3",
                                                Inputs=['Phys/NoConvGamma/Particles'])
        isocut_LLGamma = FilterDesktop(name+"LLIsoLine",
                                                Code="(RELINFO('/Event/Phys/LLGamma/Iso05', 'CC_MULT', 1000)+RELINFO('/Event/Phys/LLGamma/Iso05', 'NC_MULT', 1000)) < 4",
                                                Inputs=['Phys/LLGamma/Particles'])
        isocut_DDGamma = FilterDesktop(name+"DDIsoLine",
                                                Code="(RELINFO('/Event/Phys/DDGamma/Iso05', 'CC_MULT', 1000)+RELINFO('/Event/Phys/DDGamma/Iso05', 'NC_MULT', 1000)) < 4",
                                                Inputs=['Phys/DDGamma/Particles'])
        isocut_LowPtLLGamma = FilterDesktop(name+"LowPtLLIsoLine",
                                                Code="(RELINFO('/Event/Phys/LowPtLLGamma/Iso05', 'CC_MULT', 1000)+RELINFO('/Event/Phys/LowPtLLGamma/Iso05', 'NC_MULT', 1000)) < 4",
                                                Inputs=['Phys/LowPtLLGamma/Particles'])
        isocut_LowPtDDGamma = FilterDesktop(name+"LowPtDDIsoLine",
                                                Code="(RELINFO('/Event/Phys/LowPtDDGamma/Iso05', 'CC_MULT', 1000)+RELINFO('/Event/Phys/LowPtDDGamma/Iso05', 'NC_MULT', 1000)) < 4",
                                                Inputs=['Phys/LowPtDDGamma/Particles'])

#        self.NoConvLowPtGammaLine = StrippingLine( 
#            name = 'NoConvLowPtGamma',
#            prescale  = 1.0,
#            postscale  = self.config['MicroBiasPostscale'],
#            HLT1       =self.config['MicroBiasHlt1Filter'],
#            HLT2       =self.config['MicroBiasHlt2Filter'],
#            checkPV   = self.config['CheckPV'],
#            RequiredRawEvents = ["Trigger", "Muon", "Calo", "Rich", "Velo", "Tracker"],
#            ODIN      = odin,
#            RelatedInfoTools = relinfo,
#            RelatedInfoFilter = isocut_NoConvLowPtGamma,
#            selection = self.stdLowPtPhotons_clean
#            )
#        self.registerLine( self.NoConvLowPtGammaLine )

        self.NoConvGammaLine = StrippingLine(
            name = 'NoConvGamma',
            prescale  = 1.0,
            postscale  = self.config['MicroBiasPostscale'],
            HLT1       =self.config['MicroBiasHlt1Filter'],
            HLT2       =self.config['MicroBiasHlt2Filter'],
            checkPV   = self.config['CheckPV'],
            RequiredRawEvents = ["Trigger", "Muon", "Calo", "Rich", "Velo", "Tracker"],
            ODIN      = odin,
            RelatedInfoTools = relinfo,
            RelatedInfoFilter = isocut_NoConvGamma,
            selection = self.stdPhotons_clean
            )
        self.registerLine( self.NoConvGammaLine )


        self.LLGammaLine = StrippingLine(
            name = 'LLGamma',
            prescale  = 1.0,
            postscale  = self.config['MicroBiasPostscale'],
            HLT1       =self.config['MicroBiasHlt1Filter'],
            HLT2       =self.config['MicroBiasHlt2Filter'],
            checkPV   = self.config['CheckPV'],
            RequiredRawEvents = ["Trigger", "Muon", "Calo", "Rich", "Velo", "Tracker"],
            ODIN      = odin,
            RelatedInfoTools = relinfo,
            RelatedInfoFilter = isocut_LLGamma,
            selection = self.convPhotons_LL_clean
            )
        self.registerLine( self.LLGammaLine )

        self.DDGammaLine = StrippingLine(
            name = 'DDGamma',
            prescale  = 1.0,
            postscale  = self.config['MicroBiasPostscale'],
            HLT1       =self.config['MicroBiasHlt1Filter'],
            HLT2       =self.config['MicroBiasHlt2Filter'],
            checkPV   = self.config['CheckPV'],
            RequiredRawEvents = ["Trigger", "Muon", "Calo", "Rich", "Velo", "Tracker"],
            ODIN      = odin,
            RelatedInfoTools = relinfo,
            RelatedInfoFilter = isocut_DDGamma,
            selection =self.convPhotons_DD_clean
            )
        self.registerLine( self.DDGammaLine )

        self.LowPtLLGammaLine = StrippingLine(
            name = 'LowPtLLGamma',
            prescale  = 0.2,
            postscale  = self.config['MicroBiasPostscale'],
            HLT1       =self.config['MicroBiasHlt1Filter'],
            HLT2       =self.config['MicroBiasHlt2Filter'],
            checkPV   = self.config['CheckPV'],
            RequiredRawEvents = ["Trigger", "Muon", "Calo", "Rich", "Velo", "Tracker"],
            ODIN      = odin,
            RelatedInfoTools = relinfo,
            RelatedInfoFilter = isocut_LowPtLLGamma,
            selection = self.convPhotons_LowPtLL_clean
            )
        self.registerLine( self.LowPtLLGammaLine )

        self.LowPtDDGammaLine = StrippingLine(
            name = 'LowPtDDGamma',
            prescale  = 0.2,
            postscale  = self.config['MicroBiasPostscale'],
            HLT1       =self.config['MicroBiasHlt1Filter'],
            HLT2       =self.config['MicroBiasHlt2Filter'],
            checkPV   = self.config['CheckPV'],
            RequiredRawEvents = ["Trigger", "Muon", "Calo", "Rich", "Velo", "Tracker"],
            ODIN      = odin,
            RelatedInfoTools = relinfo,
            RelatedInfoFilter = isocut_LowPtDDGamma,
            selection =self.convPhotons_LowPtDD_clean
            )
        self.registerLine( self.LowPtDDGammaLine )
