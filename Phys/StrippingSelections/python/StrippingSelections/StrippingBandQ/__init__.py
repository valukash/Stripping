###############################################################################
# (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""
Module importing stripping selection line builder modules
for BandQ WG.
"""

_selections = (
    'StrippingDiMuonForXsection',
    'StrippingB2JpsiXforEM_s'   ,
    'StrippingXibc'             ,
    'StrippingCC2DD'            ,
    'StrippingBc3h'             , 
    'StrippingCharmAssociative' , 
#    'StrippingPsiX0'            , ## no need to run in production 
#    'StrippingPsiXForBandQ'     , ## no need to run in production
#    'StrippingUpsilonExotic'    , ## no need to run in production
    'StrippingDiMuonNew'        ,
    'StrippingChiCJPsiGammaConv',
#    'StrippingXic2HHH',
    'StrippingDiMuonInherit',
    'StrippingHeavyBaryons',
    'StrippingBc2Ds1Gamma',
    #
    'StrippingBc2JpsiHBDT',
    'StrippingBc2JpsiMuXNew',
    'StrippingCcbar2PpbarNew',
    'StrippingCcbar2PhiPhi',
    'StrippingCcbar2PhiPhiDetached',
    'StrippingCcbar2LstLst',
    'StrippingCcbar2LstLambda',
    'StrippingCcbar2LambdaLambda',
    'StrippingCcbar2PPPiPi',
    'StrippingCcbar2PhiPhiPiPi',
    'StrippingLb2EtacKp',
    'StrippingB2Chic0KPi',
    'StrippingBbbar2PhiPhi',
    'StrippingXB2DPiP',
#    'StrippingOmegab2XicKpi',
    'StrippingOmegabDecays',
    'StrippingXiccBDT',
    'StrippingXibcBDT',
    'StrippingPPMuMu',
    'StrippingEtap2pipimumu',
    'StrippingInclusiveCharmBaryons',
    'StrippingInclusiveDoubleD'
    )

for _sel in _selections :
    try :
        __import__( '%s.%s'  % ( __name__, _sel ) )
    except Exception, x:
        print '[WARNING] Submodule %s.%s raises the exception "%s" and will be skipped !' % ( __name__,_sel,x )

from sys import modules as _modules
_this = _modules[__name__]

_strippingKeys = filter ( lambda x : x[:9]=='Stripping',
                          locals().keys())

_strippingModules = [getattr(_this, _k) for _k in _strippingKeys]

