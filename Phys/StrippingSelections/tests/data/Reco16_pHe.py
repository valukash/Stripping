###############################################################################
# (c) Copyright 2020 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
#-- GAUDI jobOptions generated on Wed Nov 30 23:45:15 2016
#-- Contains event types : 
#--   90300000 - 244 files - 27313679 events - 126.01 GBytes


#--  Extra information about the data processing phases:

from Gaudi.Configuration import * 
from GaudiConf import IOHelper
IOHelper('ROOT').inputFiles([
'LFN:/lhcb/LHCb/Protonhelium16/RDST/00055503/0000/00055503_00000489_1.rdst',
'LFN:/lhcb/LHCb/Protonhelium16/RDST/00055503/0000/00055503_00000490_1.rdst',
'LFN:/lhcb/LHCb/Protonhelium16/RDST/00055503/0000/00055503_00000491_1.rdst',
'LFN:/lhcb/LHCb/Protonhelium16/RDST/00055503/0000/00055503_00000492_1.rdst',
'LFN:/lhcb/LHCb/Protonhelium16/RDST/00055503/0000/00055503_00000493_1.rdst',
'LFN:/lhcb/LHCb/Protonhelium16/RDST/00055503/0000/00055503_00000494_1.rdst',
], clear=True)



from Gaudi.Configuration import FileCatalog

FileCatalog().Catalogs += [ 'xmlcatalog_file:$STRIPPINGSELECTIONSROOT/tests/data/Reco16_pHe.xml' ]

