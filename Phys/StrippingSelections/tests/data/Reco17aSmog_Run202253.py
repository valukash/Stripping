###############################################################################
# (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from Gaudi.Configuration import *
from GaudiConf import IOHelper
IOHelper('ROOT').inputFiles([
    "LFN:/lhcb/LHCb/Collision17/RDST/00073870/0000/00073870_00005160_1.rdst",
    "LFN:/lhcb/LHCb/Collision17/RDST/00073870/0000/00073870_00005161_1.rdst",
    "LFN:/lhcb/LHCb/Collision17/RDST/00073870/0000/00073870_00005162_1.rdst",
    "LFN:/lhcb/LHCb/Collision17/RDST/00073870/0000/00073870_00005163_1.rdst",
    "LFN:/lhcb/LHCb/Collision17/RDST/00073870/0000/00073870_00005164_1.rdst",
    "LFN:/lhcb/LHCb/Collision17/RDST/00073870/0000/00073870_00005165_1.rdst",
    "LFN:/lhcb/LHCb/Collision17/RDST/00073870/0000/00073870_00005166_1.rdst",
    "LFN:/lhcb/LHCb/Collision17/RDST/00073870/0000/00073870_00005167_1.rdst"
    ], clear=True)

from Gaudi.Configuration import FileCatalog

FileCatalog().Catalogs += [ 'xmlcatalog_file:$STRIPPINGSELECTIONSROOT/tests/data/Reco17aSmog_Run202253.xml' ]

