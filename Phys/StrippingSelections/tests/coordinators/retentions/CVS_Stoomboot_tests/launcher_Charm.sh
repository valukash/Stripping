#!/bin/bash
###############################################################################
# (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
# Start with qsub -q generic7 -N StrippingTestCharm -t 1 < ../launcher_Charm.sh

source /cvmfs/lhcb.cern.ch/lib/LbLogin.sh
JOBNAME="StrippingTestCharm"

scripts=/data/bfys/cvazquez/STRIPPING/PREPARATION_STRIPPING_34r0p1/Stripping/Phys/StrippingSelections/tests/coordinators/retentions/CVS_Stoomboot_tests
davinci=/data/bfys/cvazquez/STRIPPING/PREPARATION_STRIPPING_34r0p1/DaVinciDev_2018-patches

output=/data/bfys/cvazquez/Stripping34r0p1/Charm/
# try to create the folder if it does not exist
mkdir $output

echo "================================================================================================================================"
echo "Starting on : $(date)"
echo "Running on node : $(hostname)"
echo "Current directory : $(pwd)"
echo "Working directory : $PBS_O_WORKDIR"
echo "Current job ID : $PBS_JOBID"
echo "Current job name : $JOBNAME"
echo "Job index number : $PBS_ARRAYID"
echo "Output directory: $output"
echo "================================================================================================================================"

cd ${output}
cp -r ${davinci} .
cd DaVinciDev_2018-patches
cp ${scripts}/TestFromSettings_Charm.py .

#LbLogin -c x86_64-centos7-gcc62-opt
### before sending the scripts, needs to:
# export X509_USER_PROXY=/user/cvazquez/.proxy
# lhcb-proxy-init
# echo $X509_USER_PROXY
export X509_USER_PROXY=/user/cvazquez/.proxy

./run gaudirun.py TestFromSettings_Charm.py

