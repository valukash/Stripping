###############################################################################
# (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################

from Configurables import DaVinci
from Gaudi.Configuration import importOptions

DaVinci().DataType = '2015'
DaVinci().Lumi = True
DaVinci().EvtMax = 100000

from Configurables import CondDB
CondDB().LatestGlobalTagByDataType = "2015"

from Configurables import LHCbApp
LHCbApp().XMLSummary = "summary.xml"

importOptions("$STRIPPINGSELECTIONSROOT/tests/data/Reco15a_Run164668.py")
