###############################################################################
# (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
#author: Guido Andreassi <guido.andreassi@cern.ch>
#This code launches on ganga (tested on v 6.6.1) the jobs necessary to produce the ntuples needed for the validation of the stripping in RD

import os 
jname = "S28_lept_validation"
j = Job(name = jname, backend=Dirac())
DaVinci_dirname = "./DaVinciDev_v42r4"
if os.path.exists(DaVinci_dirname): myApp = GaudiExec()#application='DaVinci',version='v41r4p1')
else: myApp = prepareGaudiExec('DaVinci','v42r4', myPath='.')
myApp.directory = "./DaVinciDev_v42r4"
j.application = myApp
j.application.options = ['$PWD/optsfile_leptonic.py']
j.application.readInputData('./validation_Collision17_Beam6500GeVVeloClosedMagUp_Real Data_Reco17a_90000000_LEPTONIC_MDST.py')
j.splitter = SplitByFiles(filesPerJob=15)
f = MassStorageFile('S29_validation.root')
j.outputfiles=[f]
#j.postprocessors.append(RootMerger(files = ['S29_validation.root'],ignorefailed = True,overwrite = True))
j.submit()


###############################################################

jname = "S28_bhad_validation"
j = Job(name = jname, backend=Dirac())
DaVinci_dirname = "./DaVinciDev_v42r4"
if os.path.exists(DaVinci_dirname): myApp = GaudiExec()#application='DaVinci',version='v41r4p1')
else: myApp = prepareGaudiExec('DaVinci','v42r4', myPath='.')
myApp.directory = "./DaVinciDev_v42r4"
j.application = myApp
j.application.options = ['$PWD/optsfile_bhadron.py']
j.application.readInputData('./validation_Collision17_Beam6500GeVVeloClosedMagUp_Real Data_Reco17a_90000000_BHADRONCOMPLETEEVENT_DST.py')
j.splitter = SplitByFiles(filesPerJob=15)
f = MassStorageFile('S29_validation.root')
j.outputfiles=[f]
#j.postprocessors.append(RootMerger(files = ['S29_validation.root'],ignorefailed = True,overwrite = True))
j.submit()
