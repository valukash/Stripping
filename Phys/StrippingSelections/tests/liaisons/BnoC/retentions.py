###############################################################################
# (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
################################################################################################


# Ganga job IDs for WG tests
#jobs20r1 = {
  #"All"  : 87, # the whole stripping
  #"B2CC" : 86, 
  #"B2OC" : 88, 
  #"BandQ" : 89, 
  #"Charm" : 90, 
  #"Charmless" : 96, 
  #"QEE" : 92, 
  #"RD" : 93, 
  #"SL" : 94, 
 # "Calib" : 95 
#}

#jobs24 = {
 # "All"  : 57, # the whole stripping
  #"B2CC" : 58, 
  #"B2OC" : 59, 
  #"BandQ" : 60, 
  #"Charm" : 61, 
 # "Charmless" : 57, 
  #"QEE" : 63, 
  #"RD" : 64, 
  #"SL" : 65, 
  #"Calib" : 66 
#}

#jobs21r1p1={
 # "Charmless" : 96,
  #}
jobs26r1={
  "Charmless" : 27,
  }

jobs = jobs26r1

dumpDetailedTable = True  # Dump the table with lines' retentions in twiki format
triggerRate = 5.e3        # Trigger rate for BW calculation in Hz
daVinciVersion = 'v41r3'  # DaVinci version (for log file name)
#daVinciVersion = 'v32r2p1'  # DaVinci version (for log file name)
gangadir = '/afs/cern.ch/work/t/tiwillia/private/gangadir/workspace/tiwillia/LocalXML/' # Location of ganga job logs

# Average event sizes in kB for each stream
streamEventSize = {
    'Bhadron' : 12.2,
    'BhadronCompleteEvent' : 125.5, 
    'Calibration' : (116.5*0.25), # only 1 replicas
    'Charm' : 8.0,
    'CharmCompleteEvent' : 126.8,
    'CharmToBeSwum' : (104.4*0.35), # only 1 replicas instead of 4 + 10% to DST
    'Dimuon' : 117.2,
    'EW' : 77.8,
    'Leptonic' : 6.8,
    'MiniBias' : 47.5,
    'PID' : (8.8*0.25), # only 1 replicas
    'Radiative' : 146.8,
    'Semileptonic' : 113.9
}



#################################################################################################

import os
from copy import copy
from math import sqrt

streams = sorted(streamEventSize.keys())

#['Bhadron', 'BhadronCompleteEvent', 'Calibration', 'Charm',
#           'CharmCompleteEvent', 'CharmToBeSwum', 'Dimuon', 'EW',
#           'Leptonic', 'MiniBias', 'PID', 'Radiative', 'Semileptonic']

streamRate = {}
eventSize = {}

dd = {}

for (wg, job) in jobs.iteritems() : 
  d = {}

  path = gangadir + str(job)
  files = os.listdir(path)
  nfiles = 0
  curr_stream = None
  nevents = 0
  notgoodevents = 0

  for filename in files : 

    reachedLastEvent = False
    foundStrippingReport = False

    fullname = path + '/' + filename + '/output/Step1_Ganga_DaVinci_' + daVinciVersion + '.log'
    if not os.path.isfile(fullname) : continue
    f = file(fullname)

    nfiles += 1
    
#    print fullname
    
    for l in f : 
        if l.find('Xeon') > 0 : 
#            print l
            speed = float(l.split()[8])
#            print speed
        if l.find('Application Manager Stopped successfully') > 0 : 
            reachedLastEvent = True
            continue
#        if l.find('Stripping') <= 0 : continue
        if reachedLastEvent : 
#    	    print l
	    if l.find('StrippingReport') == 0:
                print 'l.split[6] ', l.split()[6]
		nevents += int(l.split()[6])
                notgoodevents += int(l.split()[3].strip(','))
                print "file= ",fullname
                print "events= ",int(l.split()[6])
            if l.find('StrippingGlobal') > 0 : 
        	foundStrippingReport = True
                ls = l.split('|')
 #               print l
		if 'ALL' not in d.keys() :  
                    d['ALL'] = [ int(ls[3]), float(ls[5])*speed/1.5, [], {}, 0, 0 ]
                else : 
                    d['ALL'][0] += int(ls[3])
                    d['ALL'][1] += float(ls[5])*speed/1.5
	    elif foundStrippingReport and l.find('StrippingSequenceStream') > 0 : 
                ls = l.split('|')
                name = ls[1].strip().rstrip('_')[24:]
                curr_stream = name
#                    print ls
                nev = int(ls[3])
#		if name == 'MiniBias' : nev = float(nev)/100.
		if name not in d.keys() : 
                    d[name] = [ nev, float(ls[5])*speed/1.5, [], {}, 0, 0 ]
                else : 
                    d[name][0] += nev
                    d[name][1] += float(ls[5])*speed/1.5
            elif foundStrippingReport and l.find('Stripping') > 0 : 
                ls = l.split('|')
                name = ls[1].strip()[1:]
                print "Name= ",name
                nev = int(ls[3])
                print "NEV= ",nev
                if len(ls[5].strip())>1 : 
                    time = float(ls[5])*speed/1.5
                else :
                    time = 0
                    
                if name not in d[curr_stream][2] : 
                    d[curr_stream][2] += [ name ]
                    d[curr_stream][3][name] = [ nev, time, 0, 0 ]
                else : 
            	    d[curr_stream][3][name][0] += nev
            	    d[curr_stream][3][name][1] += time
            elif foundStrippingReport : 
#                print l 
                break
    f.close()

  print '%10s: job %4d, events=%10d, files=%4d, notgoodevents=%10d' % (wg, job, nevents, nfiles, notgoodevents)

  if nevents == 0 : continue

  for k in d.keys() : 
    d[k][4] = sqrt(float(d[k][0]))/float(nevents)
    d[k][5] = d[k][0]
    d[k][0] /= float(nevents)
    d[k][1] /= float(nfiles)
    for sl in d[k][2] : 
        d[k][3][sl][2] = sqrt(float(d[k][3][sl][0]))/float(nevents)
        d[k][3][sl][3] = d[k][3][sl][0]
        d[k][3][sl][0] /= float(nevents)
        d[k][3][sl][1] /= float(nfiles)

  dd[wg] = copy(d)

f = open('wg_table.tex', 'w')

s = '%16s ' % 'WG / Stream'
s2 = '%16s ' % 'Event size, kb'
for stream in streams :
    s += '& \\begin{sideways}%5s\\end{sideways}' % stream
    s2 += '& %5.1f' % streamEventSize[stream]

#    if stream not in streamRate.keys() : 
#        eventSize[stream] = [ streamEventSize[stream] ]
#        streamRate[stream] = [ d['ALL'][stream][0] ]
#    else : 
#        eventSize[stream] += [ streamEventSize[stream] ]
#        streamRate[stream] += [ d['ALL'][stream][0] ]

s += '& \\begin{sideways}%5s\\end{sideways}' % 'Total, Mb/s\n'
s2 += '&       \n'
s += '& \\begin{sideways}%5s\\end{sideways} \\\\' % 'Time, ms/event\n'
s2 += '&       \\\\\n'
f.write(s)
f.write('\\hline\n')
f.write(s2)
f.write('\\hline\n')

for wg in sorted(jobs.keys()) :
    if wg not in dd.keys() : continue

    tot_bw = 0.
    s = '%16s ' % wg

#    print dd[wg].keys()

    for stream in streams :
        if stream in dd[wg].keys() :
            retention = dd[wg][stream][0]
            bw = streamEventSize[stream]*retention*triggerRate/1e3
            tot_bw += bw
            s += '& %5.2f' % bw
        else :
            s += '&      '
    if 'ALL' in dd[wg].keys() : 
        s += '& %5.2f & %5.1f \\\\' % (tot_bw, dd[wg]['ALL'][1])
        if wg == 'All' : print '\\hline'
    else : 
        s += '&       &       \\\\'
    f.write(s + '\n')

f.close()

if dumpDetailedTable : 

    f = open('line_retentions.txt', 'w')

    for wg in sorted(jobs.keys()) : 
        if wg not in dd.keys() : continue
        if wg == 'All' : continue
        f.write('---+ ' + wg)
        f.write('\n\n')
        for stream in streams : 
            if stream in dd[wg].keys() : 
#            print ' |_%-50s|         |        |' % (stream+'_')
                f.write('---+++ ' +  stream + '\n')
                f.write(' |*%-49s*|*N evnts*|*Rate, %%*|*ms/evt*|\n' % 'Line name')
                for sl in dd[wg][stream][2] : 
                    f.write(' |!%-50s| %7d | %7.4f | %6.3f |\n' % (sl, dd[wg][stream][3][sl][0]*(nevents), dd[wg][stream][3][sl][0]*100, dd[wg][stream][3][sl][1]))
#                    print "sl= ",sl
 #                   print "dd[wg][stream][3]= ",dd[wg][stream][3]
  #                  print "dd[wg][stream][3][sl]= ",dd[wg][stream][3][sl]
   #                 print "dd[wg][stream][3][sl][0]= ",dd[wg][stream][3][sl][0]
    #                print "dd[wg][stream][3][sl][0]*(nfiles)*(nevents)= ",dd[wg][stream][3][sl][0]*(nfiles)*(nevents)
                f.write('\n')
        f.write('\n')
        
    f.close()
