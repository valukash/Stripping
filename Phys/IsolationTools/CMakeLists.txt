###############################################################################
# (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
################################################################################
# Package: IsolationTools
################################################################################
gaudi_subdir(IsolationTools v1r14p1)

gaudi_depends_on_subdirs(Phys/DaVinciTools
                         Phys/MVADictTools
                         Phys/DaVinciKernel
                         Phys/LoKiPhys
                         Phys/IncTopoVert
                         Event/HltEvent
                         Det/MuonDet
                         Muon/MuonID)

find_package(Boost COMPONENTS regex)

find_package(ROOT)

find_package(ROOT COMPONENTS Physics Matrix)

include_directories(SYSTEM ${Boost_INCLUDE_DIRS} ${ROOT_INCLUDE_DIRS})

gaudi_add_module(IsolationTools
                 src/*.cpp
                 INCLUDE_DIRS Boost # AIDA
                 LINK_LIBRARIES Boost ROOT DaVinciKernelLib MVADictToolsLib MuonDetLib LoKiPhysLib HltEvent)

gaudi_install_python_modules()


gaudi_add_test(QMTest QMTEST)
