/*****************************************************************************\
* (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// $Id: $
#ifndef RELINFOGAMMAISO_H
#define RELINFOGAMMAISO_H 1

// Include files
#include "GaudiAlg/GaudiTool.h"
#include "Kernel/RelatedInfoNamed.h"
#include "Kernel/IRelatedInfoTool.h"            // Interface
#include "Event/RelatedInfoMap.h"
#include "Event/ProtoParticle.h"
#include "CaloUtils/CaloParticle.h"

#include <memory>

//============================================================================

class RelInfoGammaIso : public GaudiTool, virtual public IRelatedInfoTool 
{
  
public:

  /// Standard constructor
  RelInfoGammaIso( const std::string& type,
                  const std::string& name,
                  const IInterface* parent );

  /// Initialise
  StatusCode initialize() override;

  /// GetInfo
  LHCb::RelatedInfoMap* getInfo(void) override; 

  /// Destructor
  virtual ~RelInfoGammaIso( ){}


public:

  StatusCode calculateRelatedInfo( const LHCb::Particle*, 
				   const LHCb::Particle* ) override;


private:
  LHCb::RelatedInfoMap m_map;
  std::vector<short int> m_keys;

  float n_pi0_f;
  float n_eta_f;
  float dm_pi0;
  float dm_eta;

  int m_PhotonID;
  std::map<std::string,std::vector<std::string> > m_veto;

  std::vector<const LHCb::Particle*> getTree(const LHCb::Particle* P);  


  inline bool isPureNeutralCalo(const LHCb::Particle* P)const
  {
    LHCb::CaloParticle caloP(  (LHCb::Particle*) P );
    return caloP.isPureNeutralCalo();
  }
  
};

#endif // _RELINFOGAMMAISO_H
