/*****************************************************************************\
* (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef RELINFOVERTEXISOLATIONRADIATIVE_H
#define RELINFOVERTEXISOLATIONRADIATIVE_H 1

// Include files
#include "Kernel/IRelatedInfoTool.h"
#include "Event/RelatedInfoMap.h"
#include "GaudiAlg/GaudiTool.h"
#include "CaloUtils/CaloParticle.h"

#include "Kernel/IDVAlgorithm.h"
#include <Kernel/GetIDVAlgorithm.h>
#include "Kernel/IVertexFit.h"

struct IDVAlgorithm;
struct IDistanceCalculator;
struct IVertexFit;

namespace LHCb {
  class Particle;
  class Vertex;
}
//class IVertexFit;
//class LHCb::CaloParticle ;

/** @class VertexIsolationRadiative VertexIsolationRadiative.h
 *
 * \brief Calculate vertex isolation using IDistanceCalculator
 *    Fix(Take vertexed particle and add other tracks of the
 *    event, one by one, building a new vertex.)
 *
 * Fix(Variables:
 *    - m_nPartChi2Win: number of particles that generate a vertex within a chi2 window
 *    - m_smallestDChi2: smallest particle impact parameter chi2-value wrt the vertex
 *    - m_smallestRelD0: impact parameter (distance) wrt vertex for the particle with the lowest DChi2 value 
 *    - m_smallestDChi2Mass: mass of the candidate with the smallest delta chi2
 *
 * Options:
 *    - InputParticles: containers for extra particles used for building extra vertices
 *    - MaxChi2: maximum chi2 for counting compatible vertices
 *
 *  @author Preema Pais (preema.rennee.pais@cern.ch)
 *  @date   24/01/2017
 *
 */

class RelInfoVertexIsolationRadiative : public GaudiTool, virtual public IRelatedInfoTool
{

public:

  /// Standard constructor
  RelInfoVertexIsolationRadiative( const std::string& type,
                          const std::string& name,
                          const IInterface* parent );

  StatusCode initialize () override;

  virtual ~RelInfoVertexIsolationRadiative( ); ///< Destructor

public:

  StatusCode calculateRelatedInfo( const LHCb::Particle*,
                                   const LHCb::Particle*) override;

  LHCb::RelatedInfoMap* getInfo(void) override;
  
private:
  
  /// Check if a pure CALO Particle
  inline bool isPureNeutralCalo(const LHCb::Particle* P)const
  {
    LHCb::CaloParticle caloP(  (LHCb::Particle*) P );
    return caloP.isPureNeutralCalo();
  }  // Helpers
  
  const IDistanceCalculator*  m_distCalc; ///< LoKi::DistanceCalculator

private:

  std::vector<std::string> m_variables;
  std::vector<short int> m_keys;

  int    m_nPartChi2Win ;
  double m_smallestRelD0 ;
  double m_smallestDChi2;
  double m_smallestDChi2Mass ;


  /// Find all the signal particles to vertex
  void findSignalFinalState( const LHCb::Particle* );
  LHCb::Particle::ConstVector m_signalFinalState ;
  LHCb::Particle::ConstVector m_particlesToVertex ;

  // Config and vertexing
  std::vector<std::string>   m_inputParticles ;
  double                     m_chi2 ;

  LHCb::RelatedInfoMap       m_map;

 public:
  
  // Structure of isolation vars
  struct IsolationResult
  {
    // Constructor with defaults
    IsolationResult() :  nCompatibleChi2(0),
                         smallestRelD0(-1), smallestDChi2(-1),
                         bestParticle(NULL) {}
    // Members
    int             nCompatibleChi2 ;
    double          smallestRelD0, smallestDChi2 ;
    LHCb::Particle *bestParticle ;
  };
  
  // Isolation calculation
  RelInfoVertexIsolationRadiative::IsolationResult getIsolation( const LHCb::Vertex*,
								 LHCb::Particle::ConstVector& ) ;
  
};

#endif // RELINFOVERTEXISOLATIONRADIATIVE_H
