/*****************************************************************************\
* (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// $Id: $
#ifndef RELINFOMUONISOLATION_H
#define RELINFOMUONISOLATION_H 1

// Include files
// from Gaudi
#include "GaudiAlg/GaudiTool.h"
#include "Kernel/IRelatedInfoTool.h"            // Interface
#include "Event/RelatedInfoMap.h"

class IMuonIDTool;

/** @class RelInfoMuonIsolation RelInfoMuonIsolation.h
 *
 *  \brief Computes isolation variables in the muon stations for muon candidate tracks
 *
 *
 *  @author Giacomo Graziani
 *  @date   2015-11-20
 */
class RelInfoMuonIsolation : public GaudiTool, virtual public IRelatedInfoTool {
public:
  /// Standard constructor
  RelInfoMuonIsolation( const std::string& type,
                        const std::string& name,
                        const IInterface* parent);
  virtual ~RelInfoMuonIsolation( ) {} ///< Destructor
  StatusCode initialize() override;
  StatusCode calculateRelatedInfo( const LHCb::Particle*,
                                   const LHCb::Particle*) override;

  LHCb::RelatedInfoMap* getInfo(void) override;


private:
  LHCb::RelatedInfoMap m_map;
  IMuonIDTool* m_muIDtool;
  int m_nStations;

};
#endif // RELINFOMUONISOLATION_H
