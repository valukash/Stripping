/*****************************************************************************\
* (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include "GaudiKernel/ToolFactory.h"
#include "RelInfoBKstarMuMuBDT.h"
#include "Kernel/RelatedInfoNamed.h"

//-----------------------------------------------------------------------------
// Implementation file for class : RelInfoBKstarMuMuBDT
// Converted from ConeVariables by A. Shires
//
// 2016-11-08 : Marcin Chrzaszcz
//-----------------------------------------------------------------------------

// Declaration of the Algorithm Factory
DECLARE_TOOL_FACTORY( RelInfoBKstarMuMuBDT )

//=============================================================================
// Standard constructor, initializes variables
//=============================================================================
RelInfoBKstarMuMuBDT::RelInfoBKstarMuMuBDT( const std::string& type,
                                                                    const std::string& name,
                                                                    const IInterface* parent) : GaudiTool ( type, name , parent )
{
  declareInterface<IRelatedInfoTool>(this);

  declareProperty( "Variables", m_variables,
                   "List of variables to store (store all if empty)");

  declareProperty("Name", m_transformName = "BDTvalue_KstarMuMu" );



  declareProperty
    ( "WeightsFile" , m_weightsName = "B2Kstarmumu_v2.xml",
      "weights parameter file");
  declareProperty(    "PVInputLocation"
                      , m_PVInputLocation = LHCb::RecVertexLocation::Primary
                      , " PV input location"
                      );


  m_keys.clear();

  m_keys.push_back( RelatedInfoNamed::MU_SLL_ISO_1);
  m_keys.push_back( RelatedInfoNamed::MU_SLL_ISO_2);

  m_ParticlePath="/Event/Phys/StdAllNoPIDsPions/Particles";
  m_tracktype=3;

  //std::cout<<"Initialized: RelInfoBKstarMuMuBDT"<<std::endl;


}

//=============================================================================
// Destructor
//=============================================================================
RelInfoBKstarMuMuBDT::~RelInfoBKstarMuMuBDT() {}


//=============================================================================
// Initialize
//=============================================================================
StatusCode RelInfoBKstarMuMuBDT::initialize() {
  StatusCode sc = GaudiTool::initialize() ;
  if ( sc.isFailure() ) return sc ;

  m_descend = tool<IParticleDescendants> ( "ParticleDescendants", this );
  if( ! m_descend ) {
    fatal() << "Unable to retrieve ParticleDescendants tool "<< endmsg;
    return StatusCode::FAILURE;
  }

  m_optmap["Name"] = m_transformName ;
  m_optmap["KeepVars"] = "0" ;
  m_optmap["XMLFile"] = System::getEnv("TMVAWEIGHTSROOT") + "/data/" + m_weightsName ;
  //std::cout<<"xml file: "<< m_optmap["XMLFile"]<<std::endl;
  //std::cout<<"m_transformName: "<<m_transformName<<std::endl;


  m_tmva.Init( m_optmap, debug().stream(), msgLevel(MSG::DEBUG) ) ; //

  m_angle  = 0.27;
  m_fc  = 0.60;
  m_doca_iso  = 0.13;
  m_ips  =  3.0;
  m_svdis  = -0.15;
  m_svdis_h  = 30.;
  m_pvdis  = 0.5;
  m_pvdis_h  = 40. ;


  return StatusCode::SUCCESS;

}

//=============================================================================
// Fill Extra Info structure
//=============================================================================
StatusCode RelInfoBKstarMuMuBDT::calculateRelatedInfo( const LHCb::Particle *part,
                                                                   const LHCb::Particle *top_bis )
{



  if ( msgLevel(MSG::DEBUG) ) debug() << "Calculating TrackIso extra info" << endmsg;
  m_bdt1 = -1000 ;
  m_bdt2 = -1000 ;

  if ( part->isBasicParticle() || part!=top_bis)
  {
    if ( msgLevel(MSG::DEBUG) )
      debug() << "top != top_bis" << endmsg;
    return StatusCode::SUCCESS ;
  }

  // -- The vector m_decayParticles contains all the particles that belong to the given decay
  // -- according to the decay descriptor.

  // -- Clear the vector with the particles in the specific decay


  //  LHCb::Particle::ConstVector decprod;

  is_kst=0;

  const SmartRefVector< LHCb::Particle > Bdaughters = part->daughters();
  int countDaughters =0;

  for( SmartRefVector< LHCb::Particle >::const_iterator idau = Bdaughters.begin() ; idau != Bdaughters.end() ; ++idau)
  {
    if(abs((*idau)->particleID().pid())==313) is_kst++;

    const SmartRefVector< LHCb::Particle > Bgddaughters = (*idau)->daughters();
    for( SmartRefVector< LHCb::Particle >::const_iterator igddau = Bgddaughters.begin() ; igddau != Bgddaughters.end() ; ++igddau)
    {
      //const LHCb::Particle* track = *(igddau);
      //decprod.push_back(track);
      if (  abs((*igddau)->particleID().pid()) == 13 ) countDaughters++;
    }

  }

  if  (countDaughters ==2 && (abs(part->particleID().pid()) >500) && (is_kst==1) )
    {
      StatusCode fillIsolation_mu = fillIsolation(part);
      //     std::cout<<"Finished filling isolations"<<std::endl;

      if (!fillIsolation_mu) return fillIsolation_mu;
      //std::cout<<"This should fucking work!"<<std::endl;

      return StatusCode::SUCCESS;

    }






  
    return StatusCode::SUCCESS;

}

StatusCode RelInfoBKstarMuMuBDT::fillIsolation(const LHCb::Particle *top)
{


  //set PV and SV of the mother
  m_dva = Gaudi::Utils::getIDVAlgorithm( contextSvc() ) ;

  if ( !m_dva )
  {
    return Error("Could not get parent DVAlgorithm");
  }

  const LHCb::VertexBase* goodPV = m_dva->bestVertex(top);

  //const SmartRefVector< LHCb::Particle > & daughters = top->daughters();

  m_dist = m_dva->distanceCalculator ();


  // now fillinf the isolations
  m_count_mum = 0;
  m_count_mup = 0;
  m_count_mum_f = 0;
  m_count_mup_f = 0;

  Gaudi::XYZVector PV_pos;
  if (NULL==goodPV)
  {
    warning() << "No best PV for " << top->particleID().pid() << endmsg ;
    return StatusCode::SUCCESS;
  }
  const LHCb::Particle::ConstVector pv = top->daughtersVector();

  int idx = 0;

  for (LHCb::Particle::ConstVector::const_iterator ipart=pv.begin();ipart!=pv.end();++ipart)
  {
    debug() << "daughter ID " << (*ipart)->particleID().pid() << endmsg ;
    //      if ( NULL==(*ipart)->proto() ) continue;
    idx++;
  }
  if(idx != 2 )
  {
    return StatusCode::SUCCESS;
  }
  //bool test =true;
  std::string prefix1, prefix2;
  debug() << "  just before calling getIso " << endmsg;

  std::vector<double> iso5 = getIso(top);

  // part =B
  //std::cout<<"Got back the isolation"<<std::endl;
  //std::cout<<"Checking :"<<iso5[0]<<"  "<<iso5[1]<<std::endl;

  debug() << "  just after calling getIso "
          << "GIAMPI  iso " << iso5[0] << "  " << iso5[1] << endmsg;

  if (iso5[0]==-9999) return StatusCode::FAILURE;
  const LHCb::Particle* dau1 = top->daughtersVector().at(0);
  const LHCb::Particle* dau2 = top->daughtersVector().at(1);
  const LHCb::Particle* jpsi = dau1;
  //  const LHCb::Particle* kstar = dau2;
  
  if (abs((dau1)->particleID().pid()) == 443 ) 
  {
    jpsi = (dau1);
    //   kstar = (dau2);                       
  }
  else                                         
  {
    jpsi = (dau2);
    //  kstar = (dau1);                        
  }
  




  /* this is an old isolations, don't remove the code as we might need them

  double mean1,mean2,min1,min2;
  // this is stupid but removes the 
  mean1=0.; mean2=mean1; min1=mean2; min2=min1; mean1=min2;
  
  
  if (abs((dau1)->particleID().pid()) == 443 )
  {
    jpsi = (dau1);
    //   kstar = (dau2);
  }
  else
  {
    jpsi = (dau2);
    //  kstar = (dau1);
  }
  const LHCb::Particle::ConstVector parts = jpsi->daughtersVector();
  prefix1 = "Muminus";
  prefix2 = "Muplus";
  if (jpsi->daughtersVector().at(0)->charge() < 0)
  {
    //    m_count_mum_Giampi = static_cast<float>( iso5[0] );
    //m_count_mup_Giampi = static_cast<float>( iso5[1] );
    mean1=static_cast<float>( iso5[2] );
    mean2=static_cast<float>( iso5[3] );
    min1=static_cast<float>( iso5[4] );
    min2=static_cast<float>( iso5[5] );
  }
  else
  {
    //    m_count_mum_Giampi = static_cast<float>( iso5[1] );
    //m_count_mup_Giampi = static_cast<float>( iso5[0] );
    mean1=static_cast<float>( iso5[3] );
    mean2=static_cast<float>( iso5[2] );
    min1=static_cast<float>( iso5[5] );
    min2=static_cast<float>( iso5[4] );
  }
  */
  // now my isolations
  double m_count_mum, m_count_mup;
  int i=15;
  //std::cout<<"Checking what is really needed: "<<iso5[i*2+6] <<"  "<<iso5[i*2+1+6]<<std::endl;



  if (jpsi->daughtersVector().at(0)->charge() < 0)
  {


    m_count_mum = static_cast<float>( iso5[i*2+6] );

    m_count_mup = static_cast<float>( iso5[i*2+1+6] );

    //      std::cout<<"Testing ISO: "<<iso5[i*2]<<std::endl;
    //      std::cout<<m_count_mum_Giampi<<std::endl;
  }

  else
  {
    m_count_mum = static_cast<float>( iso5[i*2+1+6] );
    m_count_mup = static_cast<float>( iso5[i*2+6] );
  }

  //store
  m_map.clear();

  //std::cout<<"Storing: "<<m_count_mum<<" "<<m_count_mup<<std::endl;

  std::vector<short int>::const_iterator ikey;
  for (ikey = m_keys.begin(); ikey != m_keys.end(); ikey++)
  {
    double value = 0;
    switch (*ikey)
    {
    case RelatedInfoNamed::MU_SLL_ISO_1 : value =m_count_mum;      break;
    case RelatedInfoNamed::MU_SLL_ISO_2 : value =m_count_mup;      break;
    }
    m_map.insert( std::make_pair( *ikey, value) );
    
  }


  return StatusCode::SUCCESS;

}



//=============================================================================
// method to calculate  isolation
//=============================================================================
std::vector<double>  RelInfoBKstarMuMuBDT::getIso(const LHCb::Particle* B)
{
  const LHCb::VertexBase *PV = m_dva->bestVertex(B);
  const LHCb::VertexBase *SV = B->endVertex();
  //  const LHCb::Particle::ConstVector parts = B->daughtersVector();

  std::vector<double>  iso_tmp(2,-9999);



  //  std::cout<<mclink1<<std::endl;
  //    std::vector<int> iso(2, -9999);
  // double mass= B->momentum().M();

  const LHCb::Particle* dau1 =  B->daughtersVector().at(0);
  const LHCb::Particle* dau2 = B->daughtersVector().at(1);
  const LHCb::Particle* jpsi = dau1;
  const LHCb::Particle* kstar = dau2;


  if (abs((dau1)->particleID().pid()) == 443 )
  {
    jpsi = (dau1);
    kstar = (dau2);
  }
  else
  {
    jpsi = (dau2);
    kstar = (dau1);
  }



  const LHCb::Particle::ConstVector parts = jpsi->daughtersVector();
  const LHCb::Particle::ConstVector kpi = kstar->daughtersVector();
  //Loop over kstar particles, get their simple kinematics
  LHCb::Particle::ConstVector::const_iterator ikpi_part;
  ROOT::Math::SMatrix<double, 3, 2> p_kpi;
  ROOT::Math::SVector<double, 2> ptkpi;
  int j=0;



  for ( ikpi_part = kpi.begin(); ikpi_part != kpi.end(); ikpi_part++)
  {
    const LHCb::ProtoParticle * proto =  (*ikpi_part)->proto();
    const LHCb::Track* track = proto->track();
    p_kpi(0,j) = track->momentum().x();
    p_kpi(1,j) = track->momentum().y();
    p_kpi(2,j) = track->momentum().z();
    ptkpi[j] = sqrt(pow(p_kpi(0,j),2)+pow(p_kpi(1,j),2));
    j++;
  }

  if(parts.size() !=2)
  {
    return iso_tmp;
  }




  StatusCode sc = StatusCode::SUCCESS;
  LHCb::Particles* allparts = get<LHCb::Particles>(m_ParticlePath);
  //LHCb::Particles* allparts = get<LHCb::Particles>(m_TracksPath);
  if (!allparts)
  {
    error() << " Failed to get particles container "
            <<  m_ParticlePath << endmsg;


    return iso_tmp;

  }



  Gaudi::XYZPoint PosPV = PV->position();
  Gaudi::XYZPoint PosSV = SV->position();
  int i = 0;
  ROOT::Math::SVector<int, 2> iso5;
  iso5[0]=0;
  iso5[1]=0;
  ROOT::Math::SMatrix<double, 3, 2> o_mu;
  ROOT::Math::SMatrix<double, 3, 2> p_mu;
  ROOT::Math::SVector<double, 2> ptmu;
  //Loop over input particles, get their simple kinematics
  LHCb::Particle::ConstVector::const_iterator ip_part;

  // int itrack=0;


  for ( ip_part = parts.begin(); ip_part != parts.end(); ip_part++)
  {
    const LHCb::ProtoParticle * proto =  (*ip_part)->proto();
    const LHCb::Track* track = proto->track();
    o_mu(0,i) = track->position().x();
    o_mu(1,i) = track->position().y();
    o_mu(2,i) = track->position().z();
    p_mu(0,i) = track->momentum().x();
    p_mu(1,i) = track->momentum().y();
    p_mu(2,i) = track->momentum().z();
    ptmu[i] = sqrt(pow(p_mu(0,i),2)+pow(p_mu(1,i),2));
    i++;
  }
  if (i>2)
  {
    error()<<"more than 2 daughters of the Jpsi" <<endmsg;
    return iso_tmp;

  }
  //int B_ID=0;



  //////////////////////////////////////////////////////////////////////////
  //  double BDT_response= reader->EvaluateMVA("BDT");
  //std::cout<<"BDT response ="<<BDT_response<<std::endl;
  
  double cut_BDT=0.0;
  double dBDT=0.01;
  double max_BDT=0.91;
  int n_bBDT = (max_BDT-cut_BDT)/dBDT;
  n_bBDT+=1;
  std::vector<double> BDTsmu1(n_bBDT,0);
  std::vector<double> BDTsmu2(n_bBDT,0);


  double mean_mu1=10;
  double mean_mu2=10;
  double min_mu1=10;
  double min_mu2=10;
  std::vector<double> iso(2*n_bBDT+6, -9999);

  int ntracks=0;
  for ( int i2 = 0; i2 < 2; i2++)
  {
    bool hltgood = false;
    double fc = 0.;
    Gaudi::XYZPoint omu(o_mu(0,i2),o_mu(1,i2),o_mu(2,i2));
    Gaudi::XYZVector pmu(p_mu(0,i2),p_mu(1,i2),p_mu(2,i2));

    //Loop over all particles
    LHCb::Particles::const_iterator ip;
    int j=0;
    // for counting ntracks
    for ( ip = allparts->begin(); ip != allparts->end() ; ++ip)
    {
      ntracks++;


      const LHCb::ProtoParticle * proto =  (*ip)->proto();
      const LHCb::Track* track = proto->track();
      const LHCb::Particle*  cand = (*ip);
      Gaudi::XYZPoint o(track->position());
      Gaudi::XYZVector p(track->momentum());
      //bool isInList = 0;
      double pt = p.Rho();
      //double ptot = p.R();
      //      int charge= proto->charge();
      //float clone = proto->track()->info(LHCb::Track::CloneDist,100000);
      //float ghost = proto->track()->ghostProbability();

      if (track->type()!=m_tracktype)   continue;
      j++;
      // skip if other particle is in input list
      if (ratio(pt, ptmu[0]) < 0.0001 || ratio(pt,ptmu[1]) <0.0001 || ratio(pt, ptkpi[0]) < 0.0001 || ratio(pt,ptkpi[1]) <0.0001)
      {
        continue;
      }
      IsHltGood(o, p, omu, pmu ,PosPV, hltgood, fc);

      // find doca and angle between input and other tracks
      Gaudi::XYZPoint vtx(0.,0.,0.);
      double doca(-1.);
      double angle(-1.);
      InCone(omu,pmu, o, p, vtx, doca, angle);
      // find impact parameters, distances from secondary and primary verte
      double imp = 0.;
      double impchi2 = 0.;
      double IP,ips,pvdis,svdis;
      ips=100000.;
      LHCb::RecVertex::Container::const_iterator iv;
      LHCb::RecVertex::Container* verts = NULL;
      if(exist<LHCb::RecVertex::Container>(m_PVInputLocation))
      {
        verts = get<LHCb::RecVertex::Container>(m_PVInputLocation);
      }
      else
      {
        return iso_tmp;
      }
      
      for ( iv = verts->begin(); iv != verts->end(); iv++)
      {
        m_dist->distance(&(*cand),(*iv),imp,impchi2);
        if (impchi2<ips) ips = impchi2;
      }

      ips=sqrt(ips);
      IP=imp;
      double trkchi2=cand->proto()->track()->chi2PerDoF();
      //double deltaZvtx = (vtx.z()-PosPV.z());
      pvdis = (vtx.z()-PosPV.z())/fabs(vtx.z()-PosPV.z())*(vtx-PosPV).R();
      svdis = (vtx.z()-PosSV.z())/fabs(vtx.z()-PosSV.z())*(vtx-PosSV).R();

      // non-isolating criteria #5
      if (angle <m_angle && fc<m_fc && doca<m_doca_iso && ips>m_ips &&
          svdis>m_svdis && svdis<m_svdis_h && pvdis>m_pvdis && pvdis<m_pvdis_h
          && track->type()==m_tracktype)
      {
        iso5[i2] += 1;
      }
      // if
      // variables for BDT

      tv_angle=angle;
      tv_fc=fc;
      tv_doca=doca;
      tv_svdis=svdis;
      tv_pvdis=pvdis;
      tv_pt=pt;
      tv_ips=ips;
      tv_ip=IP;
      tv_trkchi2=trkchi2;
      m_varmap.clear()    ;


      m_varmap.insert( "angle", tv_angle);
      m_varmap.insert( "fc", tv_fc);
      m_varmap.insert( "doca", tv_doca);
      m_varmap.insert( "svdis", tv_svdis);
      m_varmap.insert( "pvdis", tv_pvdis);
      m_varmap.insert( "pt", tv_pt);
      m_varmap.insert( "ips", tv_ips);
      m_varmap.insert( "ip", tv_ip);
      //      m_varmap.insert( "@#!@#@!trkchi2", tv_trkchi2);




      m_tmva(m_varmap,m_out) ;
      double BDT_response = m_out[m_transformName];

      //if(hltgood) std::cout<<"Checking BDT val: "<<BDT_response<<std::endl;
      //std::cout<<"Testing BDT:" << tv_angle<<", "<<tv_fc<<", "<<tv_doca<<", "<<tv_svdis<<", "<<tv_pvdis<<", "<<tv_pt<<", "<<tv_ips<<", "<<tv_ip<<" "<<BDT_response<<std::endl;


      //if(hltgood) std::cout<<"BDT response ="<<BDT_response<<std::endl;

      int counter=0;

      //if(i2==0) mean_mu1+=BDT_response;
      //if(i2==1) mean_mu2+=BDT_response;
      //if(i2==0 && BDT_response<min_mu1) min_mu1=BDT_response;
      //if(i2==1 && BDT_response<min_mu2) min_mu2=BDT_response;


      double cut_BDT_tmp=cut_BDT;

      while(cut_BDT_tmp<max_BDT)
      {
        //std::cout<<"Cut BDT: "<<cut_BDT_tmp<<std::endl;
        // making mean
        if(hltgood&&BDT_response>cut_BDT_tmp&&i2==0)
        {
          BDTsmu1[counter]+=1;
        }
        //std::cout<<"adding mu1"<<std::endl; }
        if(hltgood&&BDT_response>cut_BDT_tmp&&i2==1)
        {
          BDTsmu2[counter]+=1;
        }
        // std::cout<<"adding mu2"<<std::endl; }

        counter++;
        if(counter > n_bBDT)
        {
          std::cout<<"Breaking, overfloa of counter"<<std::endl;
          break;
        }
        // std::cout<<"Finished loop: "<<cut_BDT_tmp<<std::endl;

        //std::cout<<"Checking the isolation "<<BDTsmu1[counter]<<"  "<< BDTsmu2[counter]<<std::endl;


        cut_BDT_tmp+=dBDT;
      }
      //      std::cout<<"Track: "<<ntracks<<" Done the loops"<<std::endl;

    }
    // allparts

  }
  //i2
  
  if(ntracks>0) mean_mu1=mean_mu1/(ntracks/2.);
  if(ntracks==0)mean_mu1=ntracks;
  if(ntracks>0) mean_mu2=mean_mu2/(ntracks/2.);
  if(ntracks==0) mean_mu2=ntracks;

  //  old iso
  iso[0] = iso5[0] ;
  iso[1] = iso5[1] ;

  // mean+min
  iso[2] = mean_mu1;
  iso[3] = mean_mu2;
  iso[4] = min_mu1;
  iso[5] = min_mu2;

  // mine
  for(unsigned i=0; i<BDTsmu2.size();++i)
  {
    iso[6+i*2]=BDTsmu1[i];
    iso[6+i*2+1]=BDTsmu2[i];
    // cout<<"iso muon talbe: "<<6+i*2+1<<"  "<<BDTsmu2[i]<<"  "<<BDTsmu1[i]<<endl
  }

  //std::cout<<"Returning ISO" <<std::endl;


  return iso;

}
//=============================================================================
// IsHLTGood method,used by isolation calculation
//=============================================================================
void  RelInfoBKstarMuMuBDT::IsHltGood(Gaudi::XYZPoint o,Gaudi::XYZVector p,
                                  Gaudi::XYZPoint o_mu,Gaudi::XYZVector
                                  p_mu, Gaudi::XYZPoint PV, bool& hltgood,
                                  double& fc)
{
  Gaudi::XYZVector rv;
  Gaudi::XYZPoint vtx;
  Gaudi::XYZPoint close;
  Gaudi::XYZPoint close_mu;
  bool fail(false);
  closest_point(o,p,o_mu,p_mu,close,close_mu,vtx,fail);
  if (fail)
  {
    fc = -1.;
    hltgood = -1;
  }

  else
  {
    double pete = p.Rho();
    rv = vtx - PV;
    double DOCA_b = (close-close_mu).R();
    double ZZ = rv.z();
    fc = pointer(rv,p,p_mu);
    hltgood=( (DOCA_b<0.2) && (ZZ>0.) && (ZZ<30.) && (fc<0.4) && (pete>2.) );
  }
}



double RelInfoBKstarMuMuBDT::pointer (Gaudi::XYZVector vertex,
                                  Gaudi::XYZVector p, Gaudi::XYZVector
                                  p_mu)
{

  double pt=p.Rho()+p_mu.Rho();
  Gaudi::XYZVector ptot(p+p_mu);
  double temp = arcosine(vertex,ptot);
  double  num=ptot.R()*sin(temp);
  double  den=(num+pt);
  double fc = num/den;

  return fc;
}

//=============================================================================
// Other functions needed by isolation
//=============================================================================

double RelInfoBKstarMuMuBDT::getphi(const LHCb::Particle* vdau1, const LHCb::Particle* vdau2)
{
  double dphi = vdau1->momentum().Phi() - vdau2->momentum().Phi();
  return dphi;
}



//=============================================================================
double RelInfoBKstarMuMuBDT::gettheta(const LHCb::Particle* vdau1, const LHCb::Particle* vdau2)
{
  double dtheta = vdau1->momentum().Eta() -  vdau2->momentum().Eta();
  return dtheta;

}

//=============================================================================
double RelInfoBKstarMuMuBDT::ratio( double p1, double p2)
{
  return fabs(p1 -p2)*(1./fabs(p1+p2));
}




//=============================================================================
double RelInfoBKstarMuMuBDT::IsClose(const LHCb::Particle* p1,const LHCb::Particle* p2)
{
  double deta = gettheta(p1,p2);
  double dphi = getphi(p1,p2);
  return sqrt(deta*deta+dphi*dphi);
}
//=============================================================================
void RelInfoBKstarMuMuBDT::closest_point(Gaudi::XYZPoint o,Gaudi::XYZVector p,
                                     Gaudi::XYZPoint o_mu,Gaudi::XYZVector p_mu,
                                     Gaudi::XYZPoint& close1,
                                     Gaudi::XYZPoint& close2,
                                     Gaudi::XYZPoint& vertex, bool& fail)
{

  Gaudi::XYZVector v0(o - o_mu);
  Gaudi::XYZVector v1(p.unit());
  Gaudi::XYZVector v2(p_mu.unit());
  Gaudi::XYZPoint temp1(0.,0.,0.);
  Gaudi::XYZPoint temp2(0.,0.,0.);
  fail = false;
  double  d02 = v0.Dot(v2);
  double  d21 = v2.Dot(v1);
  double  d01 = v0.Dot(v1);
  double  d22 = v2.Dot(v2);
  double  d11 = v1.Dot(v1);
  double  denom = d11 * d22 - d21 * d21;

  if (fabs(denom) <= 0.)
  {
    close1 = temp1;
    close2 = temp2;
    fail = true;
  }

  else
  {
    double numer = d02 * d21 - d01 * d22;
    double mu1 = numer / denom;
    double mu2 = (d02 + d21 * mu1) / d22;
    close1 = o+v1*mu1;
    close2 = o_mu+v2*mu2;
  }

  vertex = (close1+(close2-close1)*0.5);

}

double RelInfoBKstarMuMuBDT::arcosine(Gaudi::XYZVector p1,Gaudi::XYZVector p2)
{

  double num    = (p1.Cross(p2)).R();
  double den    = p1.R()*p2.R();
  double seno   = num/den;
  double coseno = p1.Dot(p2)/den;
  double alpha  = asin(fabs(seno));
  if (coseno < 0 )
  {
    alpha = ROOT::Math::Pi() - alpha;
  }

  return alpha;

}
//=============================================================================
void RelInfoBKstarMuMuBDT::InCone(Gaudi::XYZPoint o1,
                              Gaudi::XYZVector p1,Gaudi::XYZPoint o2,
                              Gaudi::XYZVector p2,
                              Gaudi::XYZPoint& vtx, double&
                              doca, double& angle)
{

  Gaudi::XYZPoint rv;
  Gaudi::XYZPoint close;
  Gaudi::XYZPoint close_mu;

  bool fail(false);
  closest_point(o1,p1,o2,p2,close,close_mu,vtx, fail);
  if (fail)
  {
    doca =-1.;
    angle=-1.;
  }

  else
  {
    doca = (close-close_mu).R();
    angle = arcosine(p1,p2);
  }

}


//rel infor methods 

LHCb::RelatedInfoMap* RelInfoBKstarMuMuBDT::getInfo(void) {
  return &m_map;
}

std::string RelInfoBKstarMuMuBDT::infoPath(void) {
  std::stringstream ss;
  ss << std::string("Particle2TrackIsolationRelations");
  return ss.str();
}

//=============================================================================
// Save the particles in the decay chain (recursive function)
//=============================================================================
void RelInfoBKstarMuMuBDT::saveDecayParticles( const LHCb::Particle *top)
{

  // -- Get the daughters of the top particle
  const SmartRefVector< LHCb::Particle > & daughters = top->daughters();

  // -- Fill all the daugthers in m_decayParticles
  for( SmartRefVector< LHCb::Particle >::const_iterator idau = daughters.begin() ; idau != daughters.end() ; ++idau){

    // -- If the particle is stable, save it in the vector, or...
    if( (*idau)->isBasicParticle() ){
      if ( msgLevel(MSG::DEBUG) ) debug() << "Filling particle with ID " << (*idau)->particleID().pid() << endmsg;
      m_decayParticles.push_back( (*idau) );
    }else{
      // -- if it is not stable, call the function recursively
      m_decayParticles.push_back( (*idau) );
      if ( msgLevel(MSG::DEBUG) ) debug() << "Filling particle with ID " << (*idau)->particleID().pid() << endmsg;
      saveDecayParticles( (*idau) );
    }

  }

}

//=============================================================================
// Check if the track is already in the decay
//=============================================================================
bool RelInfoBKstarMuMuBDT::isTrackInDecay(const LHCb::Track* track){

  bool isInDecay = false;

  for(  std::vector<const LHCb::Particle*>::iterator it = m_decayParticles.begin() ; it != m_decayParticles.end() ; ++it ){

    const LHCb::ProtoParticle* proto = (*it)->proto();
    if(proto){
      const LHCb::Track* myTrack = proto->track();

      if(myTrack){

        if(myTrack == track){
          if ( msgLevel(MSG::DEBUG) ) debug() << "Track is in decay, skipping it" << endmsg;
          isInDecay = true;
        }
      }
    }
  }
  return isInDecay;
}

