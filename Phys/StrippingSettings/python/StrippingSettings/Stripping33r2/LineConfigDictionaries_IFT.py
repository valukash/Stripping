###############################################################################
# (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################

HeavyIonD02KPiPi0 = {
    "BUILDERTYPE": "StrippingHeavyIonD02KPiPi0Conf", 
    "CONFIG": {
        "D0MaxIPChi2": 9, 
        "D0MaxM": 2100, 
        "D0MinDIRA": 0.9999, 
        "D0MinM": 1600, 
        "D0MinVVDChi2": 64, 
        "D0MinVtxProb": 0.001, 
        "MergedLinePostscale": 1.0, 
        "MergedLinePrescale": 0.5, 
        "Pi0MinPT_M": 2000, 
        "Pi0MinPT_R": 1000, 
        "ResPi0MinGamCL": 0.2, 
        "ResolvedLinePostscale": 1.0, 
        "ResolvedLinePrescale": 0.5, 
        "TrackMaxGhostProb": 0.3, 
        "TrackMinIPChi2": 16, 
        "TrackMinPT_M": 300, 
        "TrackMinPT_R": 600, 
        "TrackMinTrackProb": 1e-06
    }, 
    "STREAMS": [ "IFT" ], 
    "WGs": [ "IFT" ]
}

HeavyIonDiMuon = {
    "BUILDERTYPE": "HeavyIonDiMuonConf", 
    "CONFIG": {
        "AM": 2900, 
        "AMDY": 2500, 
        "AM_4000MeV": 4000, 
        "AM_8500MeV": 8500, 
        "CheckPV": False, 
        "DOCA_max": 2.0, 
        "Hlt1FilterDY": None, 
        "Hlt1FilterHighMass": None, 
        "Hlt1FilterLowMass": None, 
        "Hlt1FilterSameSign": None, 
        "Hlt2FilterDY1": None, 
        "Hlt2FilterDY2": None, 
        "Hlt2FilterDY3": None, 
        "Hlt2FilterDY4": None, 
        "Hlt2FilterDYSS": None, 
        "Hlt2FilterHighMass": None, 
        "Hlt2FilterLowMass": None, 
        "Hlt2FilterSameSign": None, 
        "PTmu_min": 500, 
        "PostscaleDY": 1.0, 
        "PostscaleHighMass": 1.0, 
        "PostscaleLowMass": 1.0, 
        "PostscaleSameSign": 1.0, 
        "PrescaleDY": 1.0, 
        "PrescaleHighMass": 1.0, 
        "PrescaleLowMass": 1.0, 
        "PrescaleSameSign": 1.0, 
        "Track_CHI2": 5, 
        "VCHI2VDOF_max": 25, 
        "odin": [
            "NoBeam", 
            "Beam1", 
            "Beam2", 
            "BeamCrossing"
        ]
    }, 
    "STREAMS": [ "IFT" ], 
    "WGs": [ "IFT" ]
}

HeavyIonGammaHadron = {
    "BUILDERTYPE": "HeavyIonGammaHadronConf", 
    "CONFIG": {
        "CheckPV": True, 
        "ConvGhostDD": 0.3, 
        "ConvGhostLL": 0.3, 
        "DDProbNNe": 0.0, 
        "LLProbNNe": 0.0, 
        "MicroBiasHlt1Filter": None, 
        "MicroBiasHlt2Filter": None, 
        "MicroBiasLowMultHlt1Filter": None, 
        "MicroBiasLowMultHlt2Filter": None, 
        "MicroBiasLowMultPostscale": 1.0, 
        "MicroBiasLowMultPrescale": 1.0, 
        "MicroBiasPostscale": 1.0, 
        "MicroBiasPrescale": 1.0, 
        "NoBiasHlt1Filter": None, 
        "NoBiasHlt2Filter": None, 
        "NoBiasPostscale": 1.0, 
        "NoBiasPrescale": 1.0, 
        "NoConvHCAL2ECAL": 0.05, 
        "gammaCL": 0.9, 
        "gammaConvIPCHI": 0, 
        "gammaConvMDD": 60, 
        "gammaConvPT": 1500, 
        "gammaPT1": 1500, 
        "gammaPT2": 5000, 
        "odin": [
            "NoBeam", 
            "Beam1", 
            "Beam2", 
            "BeamCrossing"
        ]
    }, 
    "STREAMS": [ "IFT" ], 
    "WGs": [ "IFT" ]
}

HeavyIonMiniBias = {
    "BUILDERTYPE": "HeavyIonMiniBiasConf", 
    "CONFIG": {
        "CheckPV": False, 
        "GEC_LowMult": "( recSummaryTrack(LHCb.RecSummary.nVeloTracks, TrVELO) > 1)  ", 
        "GEC_LowMultBackwardCut": "( recSummaryTrack(LHCb.RecSummary.nVeloTracks, TrVELO) > 1) & ( recSummaryTrack(LHCb.RecSummary.nBackTracks, TrBACKWARD) < 10) ", 
        "MBNoBiasHlt1Filter": "(HLT_PASS('Hlt1BENoBiasDecision'))", 
        "MBNoBiasHlt2Filter": None, 
        "MBNoBiasPostscale": 1.0, 
        "MBNoBiasPrescale": 1.0, 
        "MicroBiasHlt1Filter": "(HLT_PASS('Hlt1BEMicroBiasVeloDecision'))", 
        "MicroBiasHlt2Filter": None, 
        "MicroBiasLowMultHlt1Filter": "(HLT_PASS('Hlt1BEMicroBiasLowMultVeloDecision'))", 
        "MicroBiasLowMultHlt2Filter": None, 
        "MicroBiasLowMultPostscale": 1.0, 
        "MicroBiasLowMultPrescale": 0.01, 
        "MicroBiasNoBackwardCutPostscale": 0.5, 
        "MicroBiasPostscale": 1.0, 
        "MicroBiasPrescale": 1.0, 
        "odin": [
            "NoBeam", 
            "Beam1", 
            "Beam2", 
            "BeamCrossing"
        ]
    }, 
    "STREAMS": [ "IFT" ], 
    "WGs": [ "IFT" ]
}

HeavyIonOpenCharm = {
    "BUILDERTYPE": "HeavyIonOpenCharmConf", 
    "CONFIG": {
        "D0_ADAMASS_WIN": 150.0, 
        "D0_Comb_ADOCAMAX_MAX": 0.5, 
        "D0_Daug_AllA_PT_MIN": 500.0, 
        "D0_Daug_All_BPVIPCHI2_MIN": 4.0, 
        "D0_Daug_All_PT_MIN": 250.0, 
        "D0_Daug_ETA_MAX": 10.0, 
        "D0_Daug_ETA_MIN": 0.0, 
        "D0_Daug_P_MAX": 1000000000.0, 
        "D0_Daug_P_MIN": 3000.0, 
        "D0_Hlt1Filter": None, 
        "D0_Hlt2Filter": None, 
        "D0_K_PIDK_MIN": 5.0, 
        "D0_Pi_PIDK_MAX": 5.0, 
        "D0_Postscale": 1.0, 
        "D0_Prescale": 1.0, 
        "D0_VCHI2VDOF_MAX": 10.0, 
        "D0_acosBPVDIRA_MAX": 0.035, 
        "D_Comb_ADOCAMAX_MAX": 2.0, 
        "D_Daug_1of3_BPVIPCHI2_MIN": 9.0, 
        "D_Daug_1of3_PT_MIN": 1000.0, 
        "D_Daug_2of3_BPVIPCHI2_MIN": 4.0, 
        "D_Daug_2of3_PT_MIN": 400.0, 
        "D_Daug_All_BPVIPCHI2_MIN": 2.0, 
        "D_Daug_All_PT_MIN": 200.0, 
        "D_Daug_ETA_MAX": 10.0, 
        "D_Daug_ETA_MIN": 0.0, 
        "D_Daug_P_MAX": 1000000000.0, 
        "D_Daug_P_MIN": 3000.0, 
        "D_K_PIDK_MIN": 5.0, 
        "D_P_PIDpPIDK_MIN": 5.0, 
        "D_P_PIDp_MIN": 5.0, 
        "D_Pi_PIDK_MAX": 5.0, 
        "D_VCHI2VDOF_MAX": 25.0, 
        "D_acosBPVDIRA_MAX": 0.035, 
        "Dp_ADAMASS_WIN": 300.0, 
        "Dp_Hlt1Filter": None, 
        "Dp_Hlt2Filter": None, 
        "Dp_Postscale": 1.0, 
        "Dp_Prescale": 1.0, 
        "Ds_ADAMASS_WIN": 300.0, 
        "Ds_Hlt1Filter": None, 
        "Ds_Hlt2Filter": None, 
        "Ds_Postscale": 1.0, 
        "Ds_Prescale": 1.0, 
        "Dst_AMDiff_MAX": 200.0, 
        "Dst_Hlt1Filter": None, 
        "Dst_Hlt2Filter": None, 
        "Dst_Postscale": 1.0, 
        "Dst_Prescale": 1.0, 
        "Dst_VCHI2VDOF_MAX": 10.0, 
        "Lc_ADAMASS_WIN": 500.0, 
        "Lc_Daug_All_BPVIPCHI2_MIN": 1.0, 
        "Lc_Hlt1Filter": None, 
        "Lc_Hlt2Filter": None, 
        "Lc_Postscale": 1.0, 
        "Lc_Prescale": 1.0, 
        "Track_CHI2": 5, 
        "odin": [
            "NoBeam", 
            "Beam1", 
            "Beam2", 
            "BeamCrossing"
        ]
    }, 
    "STREAMS": [ "IFT" ], 
    "WGs": [ "IFT" ]
}

HeavyIonOthercc = {
    "BUILDERTYPE": "HeavyIonOtherccConf", 
    "CONFIG": {
        "AM": 2900, 
        "AMmax": 4000, 
        "CheckPV": False, 
        "CombMinMass": 2750.0, 
        "DOCA_max": 10, 
        "Electron_PIDe": 1, 
        "Hlt1FilterDiElectron": None, 
        "Hlt1FilterDiProton": None, 
        "Hlt2FilterDiElectron": None, 
        "Hlt2FilterDiProton": None, 
        "MaxMass": 4000.0, 
        "PT_min": 500, 
        "PostscaleDiElectron": 1.0, 
        "PostscaleDiProton": 1.0, 
        "PrescaleDiElectron": 1.0, 
        "PrescaleDiProton": 1.0, 
        "ProtonPIDpK": 3.0, 
        "ProtonPIDppi": 5.0, 
        "Track_CHI2": 5, 
        "VCHI2VDOF_max": 16, 
        "odin": [
            "NoBeam", 
            "Beam1", 
            "Beam2", 
            "BeamCrossing"
        ]
    }, 
    "STREAMS": [ "IFT" ], 
    "WGs": [ "IFT" ]
}

HeavyIonSingleElectron = {
    "BUILDERTYPE": "HeavyIonSingleElectronConf", 
    "CONFIG": {
        "Hlt1Filter": "(HLT_PASS('Hlt1BEMicroBiasLowMultVeloDecision') ) | (HLT_PASS('Hlt1BEMicroBiasLowMultVeloNoBiasDecision') )", 
        "Hlt2Filter": None, 
        "MaxEta": 7.0, 
        "MaxNBack": 2, 
        "MaxNDown": 4, 
        "MaxNSpd": 50, 
        "MaxNTT": 3, 
        "MaxNUp": 4, 
        "MaxNvelo": 5, 
        "MaxP": 25000.0, 
        "MaxPt": 180.0, 
        "MinEta": 3.5, 
        "Postscale": 1.0, 
        "Prescale": 1.0, 
        "odin": [
            "NoBeam", 
            "Beam1", 
            "Beam2", 
            "BeamCrossing"
        ]
    }, 
    "STREAMS": [ "IFT" ], 
    "WGs": [ "IFT" ]
}

VznoPID_ForIFT = {
    "BUILDERTYPE": "StrippingV0ForPIDConf_ForIFT", 
    "CONFIG": {
        "CTauK0S": 1.0, 
        "CTauK0S_DD": 10.0, 
        "CTauLambda0": 5.0, 
        "CTauLambda0_DD": 20.0, 
        "DaughtersIPChi2": 25, 
        "DeltaMassK0S": 50.0, 
        "DeltaMassLambda": 25.0, 
        "DeltaMassPhi": 20.0, 
        "HLT": "HLT_PASS_RE('Hlt1MB.*Decision')", 
        "K0s_HiPt_Pt_thre": 1250.0, 
        "KS0DD_Prescale": 0.0, 
        "KS0LL_Prescale": 1.0, 
        "KS0LL_Prescale_HiPt": 1.0, 
        "LTimeFitChi2": 49, 
        "LamDDIsMUON_Prescale": 0.0, 
        "LamDD_Prescale": 0.0, 
        "LamLLIsMUON_Prescale_HiP": 0.0, 
        "LamLLIsMUON_Prescale_LoP": 0.0, 
        "LamLL_Prescale_HiP": 0.0, 
        "LamLL_Prescale_HiPt": 1.0, 
        "LamLL_Prescale_LoP": 1.0, 
        "Lambda_HiPt_Pt_thre": 750.0, 
        "Lamstar_Prescale": 1.0, 
        "MaxMassLambdastar": 1560.0, 
        "MaxZ": 2200.0, 
        "Monitor": False, 
        "PIDpPIDK_Lambdastar": 10, 
        "PIDp_Lambdastar": 20, 
        "PhiLL_Prescale": 0.0, 
        "PhiLL_Prescale_KmProbe_HiPt": 1.0, 
        "PhiLL_Prescale_KmProbe_LoPt": 1.0, 
        "PhiLL_Prescale_KpProbe_HiPt": 1.0, 
        "PhiLL_Prescale_KpProbe_LoPt": 1.0, 
        "Phi_GhostProb_Tag": 0.025, 
        "Phi_HiPt_PIDK_Tag": 7, 
        "Phi_HiPt_PT_Tag": 1050.0, 
        "Phi_LoPt_ProbNNK_Tag": 0.75, 
        "Preambulo": [
            "from GaudiKernel.PhysicalConstants import c_light", 
            "DD =    CHILDCUT ( ISDOWN , 1 ) & CHILDCUT ( ISDOWN , 2 ) ", 
            "LL =    CHILDCUT ( ISLONG , 1 ) & CHILDCUT ( ISLONG , 2 ) "
        ], 
        "Proton_IsMUONCut": "(INTREE( (ABSID=='p+') & ISMUON ) )", 
        "TrackChi2": 5, 
        "VertexChi2": 16, 
        "WrongMassK0S": 9.0, 
        "WrongMassK0S_DD": 18.0, 
        "WrongMassLambda": 20.0, 
        "WrongMassLambda_DD": 40.0, 
        "WrongMassPhi": 9.0, 
        "ghostprob_Lambdastar": 0.15, 
        "minimum_Pt": 380.0, 
        "otherKcuts_Lambdastar": "(PIDK > 10) & ((PIDK - PIDp) > 0)", 
        "otherpcuts_Lambdastar": "ALL", 
        "phimaxDOCA": 10
    }, 
    "STREAMS": [ "IFT" ], 
    "WGs": [ "IFT" ]
}

