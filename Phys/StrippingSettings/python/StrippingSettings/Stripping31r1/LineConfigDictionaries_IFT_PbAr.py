###############################################################################
# (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
HeavyIonD02KPiPi0 = {
    "BUILDERTYPE": "StrippingHeavyIonD02KPiPi0Conf", 
    "CONFIG": {
        "D0MaxIPChi2": 9, 
        "D0MaxM": 2100, 
        "D0MinDIRA": 0.9999, 
        "D0MinM": 1600, 
        "D0MinVVDChi2": 64, 
        "D0MinVtxProb": 0.001, 
        "MergedLinePostscale": 1.0, 
        "MergedLinePrescale": 0.5, 
        "Pi0MinPT_M": 2000, 
        "Pi0MinPT_R": 1000, 
        "ResPi0MinGamCL": 0.2, 
        "ResolvedLinePostscale": 1.0, 
        "ResolvedLinePrescale": 0.5, 
        "TrackMaxGhostProb": 0.3, 
        "TrackMinIPChi2": 16, 
        "TrackMinPT_M": 300, 
        "TrackMinPT_R": 600, 
        "TrackMinTrackProb": 1e-06
    }, 
    "STREAMS": [ "IFT" ], 
    "WGs": [ "IFT" ]
}

HeavyIonDiMuon = {
    "BUILDERTYPE": "HeavyIonDiMuonConf", 
    "CONFIG": {
        "AM": 2900, 
        "AMDY": 2500, 
        "AM_4000MeV": 4000, 
        "AM_8500MeV": 8500, 
        "CheckPV": False, 
        "DOCA_max": 2.0, 
        "Hlt1FilterDY": None, 
        "Hlt1FilterHighMass": None, 
        "Hlt1FilterLowMass": None, 
        "Hlt1FilterSameSign": None, 
        "Hlt2FilterDY1": None, 
        "Hlt2FilterDY2": None, 
        "Hlt2FilterDY3": None, 
        "Hlt2FilterDY4": None, 
        "Hlt2FilterDYSS": None, 
        "Hlt2FilterHighMass": None, 
        "Hlt2FilterLowMass": None, 
        "Hlt2FilterSameSign": None, 
        "PTmu_min": 500, 
        "PostscaleDY": 1.0, 
        "PostscaleHighMass": 1.0, 
        "PostscaleLowMass": 1.0, 
        "PostscaleSameSign": 1.0, 
        "PrescaleDY": 1.0, 
        "PrescaleHighMass": 1.0, 
        "PrescaleLowMass": 1.0, 
        "PrescaleSameSign": 1.0, 
        "ProbNNmu_min": 0.5, 
        "Track_CHI2": 10, 
        "VCHI2VDOF_max": 25, 
        "odin": [
            "NoBeam", 
            "Beam1", 
            "Beam2", 
            "BeamCrossing"
        ]
    }, 
    "STREAMS": [ "IFT" ], 
    "WGs": [ "IFT" ]
}

HeavyIonMiniBias = {
    "BUILDERTYPE": "HeavyIonMiniBiasConf", 
    "CONFIG": {
        "CheckPV": False, 
        "GEC_HighMult": "( recSummaryTrack(LHCb.RecSummary.nVeloTracks, TrVELO) >= 10000) ", 
        "GEC_LowMult": "( recSummaryTrack(LHCb.RecSummary.nVeloTracks, TrVELO) > 1) ", 
        "MicroBiasHlt1Filter": "(HLT_PASS('Hlt1BBMicroBiasVeloDecision'))|(HLT_PASS('Hlt1BEMicroBiasVeloDecision'))", 
        "MicroBiasHlt2Filter": None, 
        "MicroBiasLowMultHlt1Filter": "(HLT_PASS('Hlt1BEMicroBiasLowMultVeloDecision'))", 
        "MicroBiasLowMultHlt2Filter": None, 
        "MicroBiasLowMultPostscale": 1.0, 
        "MicroBiasLowMultPrescale": 1.0, 
        "MicroBiasPostscale": 1.0, 
        "MicroBiasPrescale": 1.0, 
        "odin": [
            "NoBeam", 
            "Beam1", 
            "Beam2", 
            "BeamCrossing"
        ], 
        "odinSMOG": [
            "Beam1", 
            "Beam2"
        ]
    }, 
    "STREAMS": [ "IFT" ], 
    "WGs": [ "IFT" ]
}

HeavyIonOpenCharm = {
    "BUILDERTYPE": "HeavyIonOpenCharmConf", 
    "CONFIG": {
        "D0_ADAMASS_WIN": 100.0, 
        "D0_Comb_ADOCAMAX_MAX": 2.0, 
        "D0_Daug_AllA_PT_MIN": 500.0, 
        "D0_Daug_All_BPVIPCHI2_MIN": 16.0, 
        "D0_Daug_All_PT_MIN": 250.0, 
        "D0_Daug_ETA_MAX": 10.0, 
        "D0_Daug_ETA_MIN": 0.0, 
        "D0_Daug_P_MAX": 1000000000.0, 
        "D0_Daug_P_MIN": 3000.0, 
        "D0_Hlt1Filter": None, 
        "D0_Hlt2Filter": None, 
        "D0_K_PIDK_MIN": 0.0, 
        "D0_Pi_PIDK_MAX": 5.0, 
        "D0_Postscale": 1.0, 
        "D0_Prescale": 1.0, 
        "D0_VCHI2VDOF_MAX": 16.0, 
        "D0_acosBPVDIRA_MAX": 0.035, 
        "D_Comb_ADOCAMAX_MAX": 1.0, 
        "D_Daug_1of3_BPVIPCHI2_MIN": 9.0, 
        "D_Daug_1of3_PT_MIN": 1000.0, 
        "D_Daug_2of3_BPVIPCHI2_MIN": 4.0, 
        "D_Daug_2of3_PT_MIN": 400.0, 
        "D_Daug_All_BPVIPCHI2_MIN": 16.0, 
        "D_Daug_All_PT_MIN": 200.0, 
        "D_Daug_ETA_MAX": 10.0, 
        "D_Daug_ETA_MIN": 0.0, 
        "D_Daug_P_MAX": 1000000000.0, 
        "D_Daug_P_MIN": 3000.0, 
        "D_K_PIDK_MIN": 5.0, 
        "D_P_PIDpPIDK_MIN": 5.0, 
        "D_P_PIDp_MIN": 5.0, 
        "D_Pi_PIDK_MAX": 0.0, 
        "D_VCHI2VDOF_MAX": 25.0, 
        "D_acosBPVDIRA_MAX": 0.035, 
        "Dp_ADAMASS_WIN": 100.0, 
        "Dp_Hlt1Filter": None, 
        "Dp_Hlt2Filter": None, 
        "Dp_Postscale": 1.0, 
        "Dp_Prescale": 1.0, 
        "Ds_ADAMASS_WIN": 100.0, 
        "Ds_Hlt1Filter": None, 
        "Ds_Hlt2Filter": None, 
        "Ds_Postscale": 1.0, 
        "Ds_Prescale": 1.0, 
        "Dst_AMDiff_MAX": 160.0, 
        "Dst_Hlt1Filter": None, 
        "Dst_Hlt2Filter": None, 
        "Dst_Postscale": 1.0, 
        "Dst_Prescale": 1.0, 
        "Dst_VCHI2VDOF_MAX": 10.0, 
        "Lc_ADAMASS_WIN": 100.0, 
        "Lc_Hlt1Filter": None, 
        "Lc_Hlt2Filter": None, 
        "Lc_Postscale": 1.0, 
        "Lc_Prescale": 1.0, 
        "Track_CHI2": 5, 
        "odin": [
            "NoBeam", 
            "Beam1", 
            "Beam2", 
            "BeamCrossing"
        ]
    }, 
    "STREAMS": [ "IFT" ], 
    "WGs": [ "IFT" ]
}

HeavyIonOthercc = {
    "BUILDERTYPE": "HeavyIonOtherccConf", 
    "CONFIG": {
        "AM": 2900, 
        "AMmax": 4000, 
        "CheckPV": False, 
        "CombMinMass": 2750.0, 
        "DOCA_max": 10, 
        "Electron_PIDe": 1, 
        "Hlt1FilterDiElectron": None, 
        "Hlt1FilterDiProton": None, 
        "Hlt2FilterDiElectron": None, 
        "Hlt2FilterDiProton": None, 
        "MaxMass": 4000.0, 
        "PT_min": 500, 
        "PostscaleDiElectron": 1.0, 
        "PostscaleDiProton": 1.0, 
        "PrescaleDiElectron": 1.0, 
        "PrescaleDiProton": 1.0, 
        "ProtonPIDpK": 15.0, 
        "ProtonPIDppi": 20.0, 
        "Track_CHI2": 5, 
        "VCHI2VDOF_max": 16, 
        "odin": [
            "NoBeam", 
            "Beam1", 
            "Beam2", 
            "BeamCrossing"
        ]
    }, 
    "STREAMS": [ "IFT" ], 
    "WGs": [ "IFT" ]
}

HeavyIonSingleElectron = {
    "BUILDERTYPE": "HeavyIonSingleElectronConf", 
    "CONFIG": {
        "Hlt1Filter": "(HLT_PASS('Hlt1BEMicroBiasLowMultVeloDecision'))", 
        "Hlt2Filter": None, 
        "MaxEta": 7.0, 
        "MaxNBack": 2, 
        "MaxNDown": 4, 
        "MaxNSpd": 50, 
        "MaxNTT": 3, 
        "MaxNUp": 4, 
        "MaxNvelo": 5, 
        "MaxP": 25000.0, 
        "MaxPt": 180.0, 
        "MinEta": 3.5, 
        "Postscale": 1.0, 
        "Prescale": 1.0, 
        "odin": [
            "NoBeam", 
            "Beam1", 
            "Beam2", 
            "BeamCrossing"
        ]
    }, 
    "STREAMS": [ "IFT" ], 
    "WGs": [ "IFT" ]
}

HeavyIonTopology = {
    "BUILDERTYPE": "HeavyIonTopologyConf", 
    "CONFIG": {
        "CheckPVfalse": False, 
        "GEC": "( recSummaryTrack(LHCb.RecSummary.nLongTracks, TrLONG) < 20) & ( recSummary(LHCb.RecSummary.nSPDhits, 'Raw/Spd/Digits') < 2000) & ( recSummaryTrack(LHCb.RecSummary.nVeloTracks, TrVELO) < 11) & ( recSummaryTrack(LHCb.RecSummary.nLongTracks, TrLONG)  > 0) ", 
        "GECGamma": "( recSummaryTrack(LHCb.RecSummary.nLongTracks, TrLONG) < 20) & ( recSummary(LHCb.RecSummary.nSPDhits, 'Raw/Spd/Digits') < 2000) & ( recSummaryTrack(LHCb.RecSummary.nVeloTracks, TrVELO) < 11)  ", 
        "Hlt1FilterE": None, 
        "Hlt1FilterLowMult": None, 
        "Hlt2FilterE": None, 
        "Hlt2FilterLowMult": None, 
        "PostscaleEE": 1.0, 
        "PostscaleGammaLowMult": 1.0, 
        "PostscaleLowMult": 1.0, 
        "PrescaleEE": 1.0, 
        "PrescaleGammaLowMult": 1.0, 
        "PrescaleLowMult": 1.0, 
        "gammaPT": 200, 
        "odin": [
            "NoBeam", 
            "Beam1", 
            "Beam2", 
            "BeamCrossing"
        ]
    }, 
    "STREAMS": [ "IFT" ], 
    "WGs": [ "IFT" ]
}

HeavyIonTrackEffDownMuon = {
    "BUILDERTYPE": "StrippingHeavyIonTrackEffDownMuonConf", 
    "CONFIG": {
        "DataType": "2011", 
        "Doca": 5.0, 
        "HLT1PassOnAll": True, 
        "HLT1TisTosSpecs": {
            "Hlt1SingleMuonNoIPL0Decision%TOS": 0, 
            "Hlt1TrackMuonDecision%TOS": 0
        }, 
        "HLT2PassOnAll": False, 
        "HLT2TisTosSpecs": {
            "Hlt2SingleMuon.*Decision%TOS": 0, 
            "Hlt2TrackEffDiMuonDownstream.*Decision%TOS": 0
        }, 
        "JpsiHlt1Filter": "Hlt1.*Decision", 
        "JpsiHlt2Filter": "Hlt2.*Decision", 
        "MassPostComb": 200.0, 
        "MassPreComb": 2000.0, 
        "MuMom": 2000.0, 
        "MuTMom": 200.0, 
        "NominalLinePostscale": 1.0, 
        "NominalLinePrescale": 1.0, 
        "SeedingMinP": 1500.0, 
        "TrChi2": 10.0, 
        "UpsilonHLT1TisTosSpecs": {
            "Hlt1SingleMuonHighPTDecision%TOS": 0
        }, 
        "UpsilonHLT2TisTosSpecs": {
            "Hlt2SingleMuonLowPTDecision%TOS": 0
        }, 
        "UpsilonLinePostscale": 1.0, 
        "UpsilonLinePrescale": 1.0, 
        "UpsilonMassPostComb": 0.0, 
        "UpsilonMassPreComb": 100000.0, 
        "UpsilonMuMom": 0.0, 
        "UpsilonMuTMom": 500.0, 
        "ValidationLinePostscale": 1.0, 
        "ValidationLinePrescale": 0.0015, 
        "VertChi2": 25.0, 
        "ZHLT1TisTosSpecs": {
            "Hlt1SingleMuonHighPTDecision%TOS": 0
        }, 
        "ZHLT2TisTosSpecs": {
            "Hlt2SingleMuonHighPTDecision%TOS": 0
        }, 
        "ZLinePostscale": 1.0, 
        "ZLinePrescale": 1.0, 
        "ZMassPostComb": 1500.0, 
        "ZMassPreComb": 100000.0, 
        "ZMuMaxEta": 4.5, 
        "ZMuMinEta": 2.0, 
        "ZMuMom": 0.0, 
        "ZMuTMom": 20000.0
    }, 
    "STREAMS": {
        "IFT": [
            "StrippingHeavyIonTrackEffDownMuonNominalLine", 
            "StrippingHeavyIonTrackEffDownMuonValidationLine", 
            "StrippingHeavyIonTrackEffDownMuonZLine", 
            "StrippingHeavyIonTrackEffDownMuonUpsilonLine"
        ]
    }, 
    "WGs": [ "IFT" ]
}

HeavyIonTrackEffMuonTT = {
    "BUILDERTYPE": "StrippingHeavyIonTrackEffMuonTTConf", 
    "CONFIG": {
        "BJpsiKHlt2TriggersTOS": {
            "Hlt2TopoMu2BodyBBDTDecision%TOS": 0
        }, 
        "BJpsiKHlt2TriggersTUS": {
            "Hlt2TopoMu2BodyBBDTDecision%TUS": 0
        }, 
        "BJpsiKMINIP": 10000, 
        "BJpsiKPrescale": 1, 
        "BMassWin": 500, 
        "Hlt1PassOnAll": True, 
        "JpsiHlt1Filter": "Hlt1.*Decision", 
        "JpsiHlt1Triggers": {
            "Hlt1TrackMuonDecision%TOS": 0
        }, 
        "JpsiHlt2Filter": "Hlt2.*Decision", 
        "JpsiHlt2Triggers": {
            "Hlt2SingleMuon.*Decision%TOS": 0, 
            "Hlt2TrackEffDiMuonMuonTT.*Decision%TOS": 0
        }, 
        "JpsiLongMuonMinIP": 0.5, 
        "JpsiLongMuonTrackCHI2": 5, 
        "JpsiLongPT": 1300, 
        "JpsiMINIP": 3, 
        "JpsiMassWin": 500, 
        "JpsiMuonTTPT": 0, 
        "JpsiPT": 1000, 
        "JpsiPrescale": 1, 
        "LongMuonPID": 2, 
        "Postscale": 1, 
        "UpsilonHlt1Triggers": {
            "Hlt1SingleMuonHighPTDecision%TOS": 0
        }, 
        "UpsilonHlt2Triggers": {
            "Hlt2SingleMuonLowPTDecision%TOS": 0
        }, 
        "UpsilonLongMuonMinIP": 0, 
        "UpsilonLongMuonTrackCHI2": 5, 
        "UpsilonLongPT": 1000, 
        "UpsilonMINIP": 10000, 
        "UpsilonMassWin": 1500, 
        "UpsilonMuonTTPT": 500, 
        "UpsilonPT": 0, 
        "UpsilonPrescale": 1, 
        "VertexChi2": 5, 
        "ZHlt1Triggers": {
            "Hlt1SingleMuonHighPTDecision%TOS": 0
        }, 
        "ZHlt2Triggers": {
            "Hlt2EWSingleMuonVHighPtDecision%TOS": 0
        }, 
        "ZLongMuonMinIP": 0, 
        "ZLongMuonTrackCHI2": 5, 
        "ZLongPT": 10000, 
        "ZMINIP": 10000, 
        "ZMassWin": 40000, 
        "ZMuonTTPT": 500, 
        "ZPT": 0, 
        "ZPrescale": 1
    }, 
    "STREAMS": {
        "IFT": [
            "StrippingHeavyIonTrackEffMuonTT_JpsiLine1", 
            "StrippingHeavyIonTrackEffMuonTT_JpsiLine2", 
            "StrippingHeavyIonTrackEffMuonTT_UpsilonLine1", 
            "StrippingHeavyIonTrackEffMuonTT_UpsilonLine2", 
            "StrippingHeavyIonTrackEffMuonTT_ZLine1", 
            "StrippingHeavyIonTrackEffMuonTT_ZLine2", 
            "StrippingHeavyIonTrackEffMuonTT_BJpsiKLine1", 
            "StrippingHeavyIonTrackEffMuonTT_BJpsiKLine2"
        ]
    }, 
    "WGs": [ "IFT" ]
}

HeavyIonWMuNu = {
    "BUILDERTYPE": "HeavyIonWMuNuConf", 
    "CONFIG": {
        "HLT1_SingleTrackNoBias": "HLT_PASS( 'Hlt1BBMicroBiasVeloDecision' )| HLT_PASS( 'Hlt1BBHighMultDecision' )", 
        "HLT2_Control10": None, 
        "HLT2_Control4800": None, 
        "ProbNNmu_min": 0.5, 
        "RawEvents": [
            "Muon", 
            "Calo", 
            "Rich", 
            "Velo", 
            "Tracker", 
            "HC"
        ], 
        "STNB_Prescale": 1.0, 
        "SingMuon10_Prescale": 1.0, 
        "SingMuon10_pT": 10000.0, 
        "SingMuon48_Prescale": 1.0, 
        "SingMuon48_pT": 4800.0, 
        "WMuLow_Prescale": 1.0, 
        "WMu_Postscale": 1.0, 
        "WMu_Prescale": 1.0, 
        "pT": 20000.0, 
        "pTlow": 15000.0, 
        "pTvlow": 5000.0
    }, 
    "STREAMS": [ "IFT" ], 
    "WGs": [ "IFT" ]
}

