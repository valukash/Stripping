###############################################################################
# (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
B2CharmlessInclusive = {
    "BUILDERTYPE": "B2CharmlessInclusive", 
    "CONFIG": {
        "KS0_Child_Trk_Chi2": 6.0, 
        "KS0_DD_FDChi2": 90.0, 
        "KS0_DD_MassWindow": 50.0, 
        "KS0_LL_FDChi2": 10.0, 
        "KS0_LL_MassWindow": 40.0, 
        "KS0_MinPT": 1500, 
        "Photon_CL_Min": 0.2, 
        "Photon_Res_PT_Min": 400.0, 
        "Pi0_Res_PT_Min": 600.0, 
        "Q2BBDIRA": 0.995, 
        "Q2BBMaxM3pi": 6700.0, 
        "Q2BBMaxM4pi": 5700.0, 
        "Q2BBMinM3pi": 4500.0, 
        "Q2BBMinM4pi": 4000.0, 
        "Q2BBMinPT": 2500.0, 
        "Q2BBVtxChi2DOF": 6.0, 
        "Q2BPrescale": 1.0, 
        "Q2BResMaxMass": 1100.0, 
        "Q2BResMinHiPT": 1500.0, 
        "Q2BResMinPT": 600, 
        "Q2BResVtxChi2DOF": 6.0, 
        "Q2BTrkGhostProb": 0.5, 
        "Q2BTrkMinHiPT": 1500.0, 
        "Q2BTrkMinIPChi2": 16.0, 
        "Q2BTrkMinPT": 500
    }, 
    "STREAMS": [ "Bhadron" ], 
    "WGs": [ "BnoC" ]
}

B2HHPi0 = {
    "BUILDERTYPE": "StrippingB2HHPi0Conf", 
    "CONFIG": {
        "BMaxIPChi2": 9, 
        "BMaxM": 6400, 
        "BMinDIRA": 0.99995, 
        "BMinM": 4200, 
        "BMinPT_M": 3000, 
        "BMinPT_R": 2500, 
        "BMinVVDChi2": 64, 
        "BMinVtxProb": 0.001, 
        "MergedLinePostscale": 1.0, 
        "MergedLinePrescale": 1.0, 
        "Pi0MinPT_M": 2500, 
        "Pi0MinPT_R": 1500, 
        "PiMaxGhostProb": 0.5, 
        "PiMinIPChi2": 25, 
        "PiMinP": 5000, 
        "PiMinPT": 500, 
        "PiMinTrackProb": 1e-06, 
        "ResPi0MinGamCL": 0.2, 
        "ResolvedLinePostscale": 1.0, 
        "ResolvedLinePrescale": 1.0
    }, 
    "STREAMS": [ "Bhadron" ], 
    "WGs": [ "BnoC" ]
}

B2KShh = {
    "BUILDERTYPE": "B2KShhConf", 
    "CONFIG": {
        "BDaug_DD_IPChi2sum": 50.0, 
        "BDaug_DD_PTsum": 4200.0, 
        "BDaug_LD_IPChi2sum": 50.0, 
        "BDaug_LD_PTsum": 4200.0, 
        "BDaug_LL_IPChi2sum": 50.0, 
        "BDaug_LL_PTsum": 3000.0, 
        "BDaug_MaxDOCAChi2": 25.0, 
        "BDaug_MedPT_PT": 800.0, 
        "B_APTmin": 1000.0, 
        "B_DD_Dira": 0.999, 
        "B_DD_FDChi2": 50.0, 
        "B_DD_IPChi2": 6.0, 
        "B_LD_Dira": 0.999, 
        "B_LD_FDChi2": 50.0, 
        "B_LD_IPChi2": 7.0, 
        "B_LL_Dira": 0.999, 
        "B_LL_FDChi2": 50.0, 
        "B_LL_IPChi2": 8.0, 
        "B_Mhigh": 921.0, 
        "B_Mlow": 1279.0, 
        "B_PTmin": 1500.0, 
        "B_VtxChi2": 12.0, 
        "ConeAngles": [
            1.0, 
            1.5, 
            1.7, 
            2.0
        ], 
        "FlavourTagging": True, 
        "GEC_MaxTracks": 250, 
        "HLT1Dec": "Hlt1(Two)?TrackMVADecision|Hlt1(Phi)?IncPhiDecision", 
        "HLT2Dec": "Hlt2Topo[234]BodyDecision|Hlt2(Phi)?IncPhiDecision", 
        "KS_DD_FDChi2": 50.0, 
        "KS_DD_MassWindow": 30.0, 
        "KS_DD_Pmin": 6000.0, 
        "KS_DD_VtxChi2": 12.0, 
        "KS_FD_Z": 15.0, 
        "KS_LD_FDChi2": 50.0, 
        "KS_LD_MassWindow": 25.0, 
        "KS_LD_Pmin": 6000.0, 
        "KS_LD_VtxChi2": 12.0, 
        "KS_LL_FDChi2": 80.0, 
        "KS_LL_MassWindow": 20.0, 
        "KS_LL_Pmin": 0.0, 
        "KS_LL_VtxChi2": 12.0, 
        "MDST": False, 
        "Postscale": 1.0, 
        "Prescale": 1.0, 
        "Prescale_SameSign": 1.0, 
        "Trk_Chi2": 4.0, 
        "Trk_GhostProb": 0.5
    }, 
    "STREAMS": {
        "Bhadron": [
            "StrippingB2KShh_DD_Run2_OS_Line", 
            "StrippingB2KShh_LL_Run2_OS_Line", 
            "StrippingB2KShh_LD_Run2_OS_Line", 
            "StrippingB2KShh_DD_Run2_SS_Line", 
            "StrippingB2KShh_LL_Run2_SS_Line", 
            "StrippingB2KShh_LD_Run2_SS_Line"
        ]
    }, 
    "WGs": [ "BnoC" ]
}

B2hhpipiPhsSpcCut = {
    "BUILDERTYPE": "B2hhpipiPhsSpcCutConf", 
    "CONFIG": {
        "2PionCuts": "(PT > 200*MeV) & (PROBNNpi > 0.1) & (MIPCHI2DV(PRIMARY)> 2) & (TRGHOSTPROB < 0.4) & (TRCHI2DOF<3)", 
        "4PionCuts": "(PT > 200*MeV) & (PROBNNpi > 0.03) & (MIPCHI2DV(PRIMARY)> 1.5) & (TRGHOSTPROB < 0.4) & (TRCHI2DOF<3)", 
        "B0ComCuts": "\n\t\t\t     (ADAMASS('B0') < 300 *MeV)\n\t\t\t     & (AMAXDOCA('') < 0.2 *mm)\n\t\t\t     & (((AMASS(1,2) < 1864.83 *MeV)\n\t\t\t     & (AMASS(3,4) < 1864.83 *MeV))\n\t\t\t     | ((AMASS(1,4) < 1864.83 *MeV)\n\t\t             & (AMASS(2,3) < 1864.83 *MeV))\n\t\t\t     | (AMASS(1,2,3) < 1869.65 *MeV)\n\t\t\t     | (AMASS(1,2,4) < 1869.65 *MeV)\n\t\t\t     | (AMASS(1,3,4) < 1869.65 *MeV)\n\t\t\t     | (AMASS(2,3,4) < 1869.65 *MeV))\n                             ", 
        "B0KKpipiComCuts": "\n\t\t\t     ((ADAMASS('B0') < 300 *MeV)\n\t\t\t     | (ADAMASS('B_s0') < 300 *MeV))\n\t\t\t     & (AMAXDOCA('') < 0.2 *mm)\n\t\t\t     & (((AMASS(1,2) < 1864.83 *MeV)\n\t\t\t     & (AMASS(3,4) < 1864.83 *MeV))\n\t\t\t     | ((AMASS(1,4) < 1864.83 *MeV)\n\t\t\t     & (AMASS(2,3) < 1864.83 *MeV))\n\t\t\t     | (AMASS(1,2,3) < 1869.65 *MeV)\n\t\t\t     | (AMASS(1,2,4) < 1869.65 *MeV)\n\t\t\t     | (AMASS(1,3,4) < 1869.65 *MeV)\n\t\t\t     | (AMASS(2,3,4) < 1869.65 *MeV))\n                             ", 
        "B0MomCuts": "\n\t\t             (VFASPF(VCHI2) < 30.)\n\t\t\t     & (BPVDIRA> 0.999)\n\t\t\t     & (BPVVDCHI2 > 10.)\n\t\t\t     & (BPVIPCHI2()<30)\n\t\t\t     & (PT > 2.*GeV )\n\t                     ", 
        "B24piMVACut": "-.03", 
        "B2KKpipiMVACut": ".05", 
        "B2hhpipiXmlFile": "$TMVAWEIGHTSROOT/data/B2hhpipi_v1r0.xml", 
        "KaonCuts": "(PT > 200*MeV) & (PROBNNk > 0.1) & (MIPCHI2DV(PRIMARY)> 2) & (TRGHOSTPROB < 0.4) & (TRCHI2DOF<3)", 
        "Prescale": 1.0
    }, 
    "STREAMS": {
        "Bhadron": [ "StrippingB2hhpipiPhsSpcCut4piLine" ], 
        "BhadronCompleteEvent": [ "StrippingB2hhpipiPhsSpcCutKKpipiLine" ]
    }, 
    "WGs": [ "BnoC" ]
}

Lb2V0h = {
    "BUILDERTYPE": "Lb2V0hhConf", 
    "CONFIG": {
        "GEC_MaxTracks": 250, 
        "HLT1Dec": "Hlt1(Two)?TrackMVADecision", 
        "HLT2Dec": "Hlt2Topo[234]BodyDecision", 
        "Lambda_DD_FD": 300.0, 
        "Lambda_DD_FDChi2": 50.0, 
        "Lambda_DD_MassWindow": 25.0, 
        "Lambda_DD_Pmin": 5000.0, 
        "Lambda_DD_VtxChi2": 15.0, 
        "Lambda_LD_FD": 300.0, 
        "Lambda_LD_FDChi2": 50.0, 
        "Lambda_LD_MassWindow": 25.0, 
        "Lambda_LD_Pmin": 5000.0, 
        "Lambda_LD_VtxChi2": 15.0, 
        "Lambda_LL_FDChi2": 80.0, 
        "Lambda_LL_MassWindow": 20.0, 
        "Lambda_LL_VtxChi2": 15.0, 
        "LbDaug_DD_PTsum": 4200.0, 
        "LbDaug_DD_maxDocaChi2": 5.0, 
        "LbDaug_LD_PTsum": 4200.0, 
        "LbDaug_LD_maxDocaChi2": 5.0, 
        "LbDaug_LL_PTsum": 3000.0, 
        "LbDaug_LL_maxDocaChi2": 5.0, 
        "LbDaug_MaxPT_IP": 0.05, 
        "LbDaug_MedPT_PT": 800.0, 
        "Lb_2bodyMhigh": 800.0, 
        "Lb_2bodyMlow": 800.0, 
        "Lb_APTmin": 1000.0, 
        "Lb_DD_Dira": 0.995, 
        "Lb_DD_FDChi2": 30.0, 
        "Lb_DD_IPCHI2wrtPV": 15.0, 
        "Lb_FDwrtPV": 1.0, 
        "Lb_LD_Dira": 0.995, 
        "Lb_LD_FDChi2": 30.0, 
        "Lb_LD_IPCHI2wrtPV": 15.0, 
        "Lb_LL_Dira": 0.995, 
        "Lb_LL_FDChi2": 30.0, 
        "Lb_LL_IPCHI2wrtPV": 15.0, 
        "Lb_Mhigh": 600.0, 
        "Lb_Mlow": 1319.0, 
        "Lb_PTmin": 800.0, 
        "Lb_VtxChi2": 12.0, 
        "Lbh_DD_PTMin": 500.0, 
        "Lbh_LD_PTMin": 500.0, 
        "Lbh_LL_PTMin": 500.0, 
        "Postscale": 1.0, 
        "Prescale": 1.0, 
        "RelatedInfoTools": [
            {
                "ConeAngle": 1.7, 
                "Location": "ConeVar17", 
                "Type": "RelInfoConeVariables", 
                "Variables": [
                    "CONEANGLE", 
                    "CONEMULT", 
                    "CONEPTASYM"
                ]
            }, 
            {
                "ConeAngle": 1.5, 
                "Location": "ConeVar15", 
                "Type": "RelInfoConeVariables", 
                "Variables": [
                    "CONEANGLE", 
                    "CONEMULT", 
                    "CONEPTASYM"
                ]
            }, 
            {
                "ConeAngle": 1.0, 
                "Location": "ConeVar10", 
                "Type": "RelInfoConeVariables", 
                "Variables": [
                    "CONEANGLE", 
                    "CONEMULT", 
                    "CONEPTASYM"
                ]
            }, 
            {
                "ConeAngle": 0.8, 
                "Location": "ConeVar08", 
                "Type": "RelInfoConeVariables", 
                "Variables": [
                    "CONEANGLE", 
                    "CONEMULT", 
                    "CONEPTASYM"
                ]
            }, 
            {
                "Location": "VtxIsolationVar", 
                "Type": "RelInfoVertexIsolation"
            }
        ], 
        "Trk_Chi2": 3.0, 
        "Trk_GhostProb": 0.5
    }, 
    "STREAMS": [ "Bhadron" ], 
    "WGs": [ "BnoC" ]
}

Lb2V0p = {
    "BUILDERTYPE": "Lb2V0ppConf", 
    "CONFIG": {
        "GEC_MaxTracks": 250, 
        "Lambda_DD_FD": 300.0, 
        "Lambda_DD_FDChi2": 50.0, 
        "Lambda_DD_MassWindow": 20.0, 
        "Lambda_DD_Pmin": 5000.0, 
        "Lambda_DD_VtxChi2": 9.0, 
        "Lambda_LD_FD": 300.0, 
        "Lambda_LD_FDChi2": 50.0, 
        "Lambda_LD_MassWindow": 25.0, 
        "Lambda_LD_Pmin": 5000.0, 
        "Lambda_LD_VtxChi2": 16.0, 
        "Lambda_LL_FDChi2": 0.0, 
        "Lambda_LL_MassWindow": 20.0, 
        "Lambda_LL_VtxChi2": 9.0, 
        "LbDaug_DD_PTsum": 2000.0, 
        "LbDaug_DD_maxDocaChi2": 16.0, 
        "LbDaug_LD_PTsum": 4200.0, 
        "LbDaug_LD_maxDocaChi2": 5.0, 
        "LbDaug_LL_PTsum": 3000.0, 
        "LbDaug_LL_maxDocaChi2": 5.0, 
        "LbDaug_MaxPT_IP": 0.05, 
        "LbDaug_MedPT_PT": 450.0, 
        "Lb_2bodyMhigh": 800.0, 
        "Lb_2bodyMlow": 800.0, 
        "Lb_APTmin": 1000.0, 
        "Lb_DD_Dira": 0.999, 
        "Lb_DD_FDChi2": 0.5, 
        "Lb_DD_IPCHI2wrtPV": 25.0, 
        "Lb_FDwrtPV": 0.8, 
        "Lb_LD_Dira": 0.999, 
        "Lb_LD_FDChi2": 30.0, 
        "Lb_LD_IPCHI2wrtPV": 15.0, 
        "Lb_LL_Dira": 0.999, 
        "Lb_LL_FDChi2": 0.5, 
        "Lb_LL_IPCHI2wrtPV": 25.0, 
        "Lb_Mhigh": 581.0, 
        "Lb_Mlow": 419.0, 
        "Lb_PTmin": 1050, 
        "Lb_VtxChi2": 16.0, 
        "Lbh_DD_PTMin": 0.0, 
        "Lbh_LD_PTMin": 500.0, 
        "Lbh_LL_PTMin": 0.0, 
        "Postscale": 1.0, 
        "Prescale": 1.0, 
        "RelatedInfoTools": [
            {
                "ConeAngle": 1.7, 
                "Location": "ConeVar17", 
                "Type": "RelInfoConeVariables", 
                "Variables": [
                    "CONEANGLE", 
                    "CONEMULT", 
                    "CONEPTASYM"
                ]
            }, 
            {
                "ConeAngle": 1.5, 
                "Location": "ConeVar15", 
                "Type": "RelInfoConeVariables", 
                "Variables": [
                    "CONEANGLE", 
                    "CONEMULT", 
                    "CONEPTASYM"
                ]
            }, 
            {
                "ConeAngle": 1.0, 
                "Location": "ConeVar10", 
                "Type": "RelInfoConeVariables", 
                "Variables": [
                    "CONEANGLE", 
                    "CONEMULT", 
                    "CONEPTASYM"
                ]
            }, 
            {
                "ConeAngle": 0.8, 
                "Location": "ConeVar08", 
                "Type": "RelInfoConeVariables", 
                "Variables": [
                    "CONEANGLE", 
                    "CONEMULT", 
                    "CONEPTASYM"
                ]
            }, 
            {
                "Location": "VtxIsolationVar", 
                "Type": "RelInfoVertexIsolation"
            }
        ], 
        "Trk_Chi2": 3.0, 
        "Trk_GhostProb": 0.5
    }, 
    "STREAMS": [ "Bhadron" ], 
    "WGs": [ "BnoC" ]
}

