###############################################################################
# (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
B2EtaMuMu = {
    "BUILDERTYPE": "B2EtaMuMu", 
    "CONFIG": {
        "B2EMMPrescale": 1.0, 
        "EMMBDIRA": 0.995, 
        "EMMBMaxMass": 6000.0, 
        "EMMBMinMass": 4900.0, 
        "EMMBMinPT": 2500.0, 
        "EMMBVtxChi2DOF": 9.0, 
        "EMMDimuon_MaxMass": 6100.0, 
        "EMMResMaxMass": 1100.0, 
        "EMMResMinPT": 600, 
        "EMMResVtxChi2DOF": 9.0, 
        "EMMTrkGhostProb": 0.5, 
        "EMMTrkMinIPChi2": 9.0, 
        "EMMTrkMinPT": 500, 
        "MuonPID": -3.0, 
        "Photon_CL_Min": 0.2, 
        "Photon_Res_PT_Min": 400.0, 
        "Pi0_Res_PT_Min": 600.0
    }, 
    "STREAMS": [ "Bhadron" ], 
    "WGs": [ "RD" ]
}

B2LLXBDT = {
    "BUILDERTYPE": "B2LLXBDTConf", 
    "CONFIG": {
        "BComCuts": "(in_range(3.7*GeV, AM, 6.8*GeV))", 
        "BMomCuts": "(in_range(4.0*GeV,  M, 6.5*GeV)) & (VFASPF(VCHI2/VDOF) < 25.) & (BPVDIRA> 0.999) & (BPVDLS>0) & (BPVIPCHI2()<400)", 
        "Bd2LLKsXmlFile": "$TMVAWEIGHTSROOT/data/Bd2eeKs_BDT_v1r0.xml", 
        "Bd2LLKstarXmlFile": "$TMVAWEIGHTSROOT/data/Bd2eeKstar_BDT_v1r0.xml", 
        "Bd2eeKsMVACut": "-0.07", 
        "Bd2eeKstarMVACut": "0.", 
        "Bd2mumuKsMVACut": "-0.07", 
        "Bd2mumuKstarMVACut": "0.", 
        "Bs2LLPhiXmlFile": "$TMVAWEIGHTSROOT/data/Bs2eePhi_BDT_v1r0.xml", 
        "Bs2eePhiMVACut": "-0.06", 
        "Bs2mumuPhiMVACut": "-0.08", 
        "Bu2LLKXmlFile": "$TMVAWEIGHTSROOT/data/Bu2eeK_BDT_v1r0.xml", 
        "Bu2eeKMVACut": "0.", 
        "Bu2mumuKMVACut": "0.", 
        "DiElectronCuts": "\n                           (HASVERTEX) & (VFASPF(VCHI2)<16) & (MM<5.0*GeV)\n                           & (INTREE( (ID=='e+') & (PT>200*MeV) & (MIPCHI2DV(PRIMARY)>1.) & (PIDe>-2) & (TRGHOSTPROB<0.5) ))\n                           & (INTREE( (ID=='e-') & (PT>200*MeV) & (MIPCHI2DV(PRIMARY)>1.) & (PIDe>-2) & (TRGHOSTPROB<0.5) ))\n                          ", 
        "DiMuonCuts": "\n                           (HASVERTEX) & (VFASPF(VCHI2)<16) & (MM<5.0*GeV)\n                           & (INTREE( (ID=='mu+') & (PT>200*MeV) & (MIPCHI2DV(PRIMARY)>1.) & (TRGHOSTPROB<0.5) ))\n                           & (INTREE( (ID=='mu-') & (PT>200*MeV) & (MIPCHI2DV(PRIMARY)>1.) & (TRGHOSTPROB<0.5) ))\n                          ", 
        "KaonCuts": "(PROBNNk > 0.1) & (PT>300*MeV) & (TRGHOSTPROB<0.4)", 
        "KsDDCuts": "(ADMASS('KS0') < 30.*MeV) & (BPVVDCHI2>25)", 
        "KsLLComCuts": "(ADAMASS('KS0') < 50.*MeV) & (ADOCACHI2CUT(25, ''))", 
        "KsLLCuts": "(ADMASS('KS0') < 30.*MeV) & (BPVVDCHI2>25) & (VFASPF(VCHI2) < 25.)", 
        "KstarCuts": "(VFASPF(VCHI2/VDOF)<16) & (ADMASS('K*(892)0')< 300*MeV)", 
        "LambdaDDCuts": "(ADMASS('Lambda0') < 30.*MeV) & (BPVVDCHI2>25)", 
        "LambdaLLComCuts": "(ADAMASS('Lambda0')<50*MeV) & (ADOCACHI2CUT(30, ''))", 
        "LambdaLLCuts": "(ADMASS('Lambda0') < 30.*MeV) & (BPVVDCHI2>25) & (VFASPF(VCHI2) < 25.)", 
        "LambdastarComCuts": "(AM < 5.6*GeV)", 
        "LambdastarCuts": "(VFASPF(VCHI2) < 25.)", 
        "Lb2LLLambdaXmlFile": "$TMVAWEIGHTSROOT/data/Lb2eeLambda_BDT_v1r0.xml", 
        "Lb2LLPKXmlFile": "$TMVAWEIGHTSROOT/data/Lb2eePK_BDT_v1r0.xml", 
        "Lb2eeLambdaMVACut": "-0.11", 
        "Lb2eePKMVACut": "-0.05", 
        "Lb2mumuLambdaMVACut": "-0.15", 
        "Lb2mumuPKMVACut": "-0.11", 
        "LbComCuts": "(in_range(3.7*GeV, AM, 7.1*GeV))", 
        "LbMomCuts": "(in_range(4.0*GeV,  M, 6.8*GeV)) & (VFASPF(VCHI2/VDOF) < 25.) & (BPVDIRA> 0.999) & (BPVDLS>0) & (BPVIPCHI2()<400)", 
        "PhiCuts": "\n                          (HASVERTEX) & (VFASPF(VCHI2)<16) & (MM<1.05*GeV) & (MIPCHI2DV(PRIMARY)>2.)\n                          & (INTREE( (ID=='K+') & (PT>200*MeV) & (TRGHOSTPROB<0.4) ))\n                          & (INTREE( (ID=='K-') & (PT>200*MeV) & (TRGHOSTPROB<0.4) ))\n                          ", 
        "Pion4LPCuts": "(PROBNNpi> 0.2) & (PT>100*MeV) & (TRGHOSTPROB<0.4) & (MIPCHI2DV(PRIMARY)>9.)", 
        "PionCuts": "(PROBNNpi> 0.2) & (PT>250*MeV) & (TRGHOSTPROB<0.4)", 
        "ProtonCuts": "(PROBNNp> 0.05) & (PT>300*MeV) & (TRGHOSTPROB<0.4)", 
        "RelatedInfoTools": [
            {
                "DaughterLocations": {
                    "[Beauty ->  (X0 ->  X+  X-) ^(X0 ->  l+  l-)]CC": "VertexIsoInfoLL", 
                    "[Beauty ->  X+ ^(X0 ->  l+  l-)]CC": "VertexIsoInfoLL", 
                    "[Beauty -> ^(X0 ->  X+  X-)  (X0 ->  l+  l-)]CC": "VertexIsoInfoHH"
                }, 
                "IgnoreUnmatchedDescriptors": True, 
                "Location": "VertexIsoInfo", 
                "Type": "RelInfoVertexIsolation"
            }, 
            {
                "DaughterLocations": {
                    "[Beauty ->  (X0 ->  X+  X-) ^(X0 ->  l+  l-)]CC": "VertexIsoBDTInfoLL", 
                    "[Beauty ->  X+ ^(X0 ->  l+  l-)]CC": "VertexIsoBDTInfoLL", 
                    "[Beauty -> ^(X0 ->  X+  X-)  (X0 ->  l+  l-)]CC": "VertexIsoBDTInfoHH"
                }, 
                "IgnoreUnmatchedDescriptors": True, 
                "Location": "VertexIsoBDTInfo", 
                "Type": "RelInfoVertexIsolationBDT"
            }, 
            {
                "ConeAngle": 0.5, 
                "DaughterLocations": {
                    "[Beauty ->  (X0 ->  Xs  X-)  (X0 ->  l+ ^l-)]CC": "TrackIsoInfoL2", 
                    "[Beauty ->  (X0 ->  Xs  X-)  (X0 -> ^l+  l-)]CC": "TrackIsoInfoL1", 
                    "[Beauty ->  (X0 ->  Xs  X-) ^(X0 ->  l+  l-)]CC": "TrackIsoInfoLL", 
                    "[Beauty ->  (X0 ->  Xs ^X-)  (X0 ->  l+  l-)]CC": "TrackIsoInfoH2", 
                    "[Beauty ->  (X0 ->  p+  pi-)  (X0 ->  l+ ^l-)]CC": "TrackIsoInfoL2", 
                    "[Beauty ->  (X0 ->  p+  pi-)  (X0 -> ^l+  l-)]CC": "TrackIsoInfoL1", 
                    "[Beauty ->  (X0 ->  p+  pi-) ^(X0 ->  l+  l-)]CC": "TrackIsoInfoLL", 
                    "[Beauty ->  (X0 ->  p+ ^pi-)  (X0 ->  l+  l-)]CC": "TrackIsoInfoH2", 
                    "[Beauty ->  (X0 ->  pi+  pi-)  (X0 ->  l+ ^l-)]CC": "TrackIsoInfoL2", 
                    "[Beauty ->  (X0 ->  pi+  pi-)  (X0 -> ^l+  l-)]CC": "TrackIsoInfoL1", 
                    "[Beauty ->  (X0 ->  pi+  pi-) ^(X0 ->  l+  l-)]CC": "TrackIsoInfoLL", 
                    "[Beauty ->  (X0 ->  pi+ ^pi-)  (X0 ->  l+  l-)]CC": "TrackIsoInfoH2", 
                    "[Beauty ->  (X0 -> ^Xs  X-)  (X0 ->  l+  l-)]CC": "TrackIsoInfoH1", 
                    "[Beauty ->  (X0 -> ^p+  pi-)  (X0 ->  l+  l-)]CC": "TrackIsoInfoH1", 
                    "[Beauty ->  (X0 -> ^pi+  pi-)  (X0 ->  l+  l-)]CC": "TrackIsoInfoH1", 
                    "[Beauty ->  X+  (X0 ->  l+ ^l-)]CC": "TrackIsoInfoL2", 
                    "[Beauty ->  X+  (X0 -> ^l+  l-)]CC": "TrackIsoInfoL1", 
                    "[Beauty ->  X+ ^(X0 ->  l+  l-)]CC": "TrackIsoInfoLL", 
                    "[Beauty -> ^(X0 ->  Xs  X-)  (X0 ->  l+  l-)]CC": "TrackIsoInfoHH", 
                    "[Beauty -> ^(X0 ->  p+  pi-)  (X0 ->  l+  l-)]CC": "TrackIsoInfoHH", 
                    "[Beauty -> ^(X0 ->  pi+  pi-)  (X0 ->  l+  l-)]CC": "TrackIsoInfoHH", 
                    "[Beauty -> ^X+  (X0 ->  l+  l-)]CC": "TrackIsoInfoH"
                }, 
                "IgnoreUnmatchedDescriptors": True, 
                "Location": "TrackIsoInfo05", 
                "Type": "RelInfoConeVariables"
            }, 
            {
                "ConeSize": 0.5, 
                "DaughterLocations": {
                    "[Beauty ->  (X0 ->  Xs  X-)  (X0 ->  l+ ^l-)]CC": "ConeIsoInfoL2", 
                    "[Beauty ->  (X0 ->  Xs  X-)  (X0 -> ^l+  l-)]CC": "ConeIsoInfoL1", 
                    "[Beauty ->  (X0 ->  Xs  X-) ^(X0 ->  l+  l-)]CC": "ConeIsoInfoLL", 
                    "[Beauty ->  (X0 ->  Xs ^X-)  (X0 ->  l+  l-)]CC": "ConeIsoInfoH2", 
                    "[Beauty ->  (X0 ->  p+  pi-)  (X0 ->  l+ ^l-)]CC": "ConeIsoInfoL2", 
                    "[Beauty ->  (X0 ->  p+  pi-)  (X0 -> ^l+  l-)]CC": "ConeIsoInfoL1", 
                    "[Beauty ->  (X0 ->  p+  pi-) ^(X0 ->  l+  l-)]CC": "ConeIsoInfoLL", 
                    "[Beauty ->  (X0 ->  p+ ^pi-)  (X0 ->  l+  l-)]CC": "ConeIsoInfoH2", 
                    "[Beauty ->  (X0 ->  pi+  pi-)  (X0 ->  l+ ^l-)]CC": "ConeIsoInfoL2", 
                    "[Beauty ->  (X0 ->  pi+  pi-)  (X0 -> ^l+  l-)]CC": "ConeIsoInfoL1", 
                    "[Beauty ->  (X0 ->  pi+  pi-) ^(X0 ->  l+  l-)]CC": "ConeIsoInfoLL", 
                    "[Beauty ->  (X0 ->  pi+ ^pi-)  (X0 ->  l+  l-)]CC": "ConeIsoInfoH2", 
                    "[Beauty ->  (X0 -> ^Xs  X-)  (X0 ->  l+  l-)]CC": "ConeIsoInfoH1", 
                    "[Beauty ->  (X0 -> ^p+  pi-)  (X0 ->  l+  l-)]CC": "ConeIsoInfoH1", 
                    "[Beauty ->  (X0 -> ^pi+  pi-)  (X0 ->  l+  l-)]CC": "ConeIsoInfoH1", 
                    "[Beauty ->  X+  (X0 ->  l+ ^l-)]CC": "ConeIsoInfoL2", 
                    "[Beauty ->  X+  (X0 -> ^l+  l-)]CC": "ConeIsoInfoL1", 
                    "[Beauty ->  X+ ^(X0 ->  l+  l-)]CC": "ConeIsoInfoLL", 
                    "[Beauty -> ^(X0 ->  Xs  X-)  (X0 ->  l+  l-)]CC": "ConeIsoInfoHH", 
                    "[Beauty -> ^(X0 ->  p+  pi-)  (X0 ->  l+  l-)]CC": "ConeIsoInfoHH", 
                    "[Beauty -> ^(X0 ->  pi+  pi-)  (X0 ->  l+  l-)]CC": "ConeIsoInfoHH", 
                    "[Beauty -> ^X+  (X0 ->  l+  l-)]CC": "ConeIsoInfoH"
                }, 
                "IgnoreUnmatchedDescriptors": True, 
                "Location": "ConeIsoInfo05", 
                "Type": "RelInfoConeIsolation"
            }, 
            {
                "DaughterLocations": {
                    "[Beauty ->  (X0 ->  Xs  X-)  (X0 ->  l+ ^l-)]CC": "TrackIsoBDTInfoL2", 
                    "[Beauty ->  (X0 ->  Xs  X-)  (X0 -> ^l+  l-)]CC": "TrackIsoBDTInfoL1", 
                    "[Beauty ->  (X0 ->  Xs  X-) ^(X0 ->  l+  l-)]CC": "TrackIsoBDTInfoLL", 
                    "[Beauty ->  (X0 ->  Xs ^X-)  (X0 ->  l+  l-)]CC": "TrackIsoBDTInfoH2", 
                    "[Beauty ->  (X0 ->  p+  pi-)  (X0 ->  l+ ^l-)]CC": "TrackIsoBDTInfoL2", 
                    "[Beauty ->  (X0 ->  p+  pi-)  (X0 -> ^l+  l-)]CC": "TrackIsoBDTInfoL1", 
                    "[Beauty ->  (X0 ->  p+  pi-) ^(X0 ->  l+  l-)]CC": "TrackIsoBDTInfoLL", 
                    "[Beauty ->  (X0 ->  p+ ^pi-)  (X0 ->  l+  l-)]CC": "TrackIsoBDTInfoH2", 
                    "[Beauty ->  (X0 ->  pi+  pi-)  (X0 ->  l+ ^l-)]CC": "TrackIsoBDTInfoL2", 
                    "[Beauty ->  (X0 ->  pi+  pi-)  (X0 -> ^l+  l-)]CC": "TrackIsoBDTInfoL1", 
                    "[Beauty ->  (X0 ->  pi+  pi-) ^(X0 ->  l+  l-)]CC": "TrackIsoBDTInfoLL", 
                    "[Beauty ->  (X0 ->  pi+ ^pi-)  (X0 ->  l+  l-)]CC": "TrackIsoBDTInfoH2", 
                    "[Beauty ->  (X0 -> ^Xs  X-)  (X0 ->  l+  l-)]CC": "TrackIsoBDTInfoH1", 
                    "[Beauty ->  (X0 -> ^p+  pi-)  (X0 ->  l+  l-)]CC": "TrackIsoBDTInfoH1", 
                    "[Beauty ->  (X0 -> ^pi+  pi-)  (X0 ->  l+  l-)]CC": "TrackIsoBDTInfoH1", 
                    "[Beauty ->  X+  (X0 ->  l+ ^l-)]CC": "TrackIsoBDTInfoL2", 
                    "[Beauty ->  X+  (X0 -> ^l+  l-)]CC": "TrackIsoBDTInfoL1", 
                    "[Beauty ->  X+ ^(X0 ->  l+  l-)]CC": "TrackIsoBDTInfoLL", 
                    "[Beauty -> ^(X0 ->  Xs  X-)  (X0 ->  l+  l-)]CC": "TrackIsoBDTInfoHH", 
                    "[Beauty -> ^(X0 ->  p+  pi-)  (X0 ->  l+  l-)]CC": "TrackIsoBDTInfoHH", 
                    "[Beauty -> ^(X0 ->  pi+  pi-)  (X0 ->  l+  l-)]CC": "TrackIsoBDTInfoHH", 
                    "[Beauty -> ^X+  (X0 ->  l+  l-)]CC": "TrackIsoBDTInfoH"
                }, 
                "IgnoreUnmatchedDescriptors": True, 
                "Location": "TrackIsoBDTInfo", 
                "Type": "RelInfoTrackIsolationBDT", 
                "Variables": 2, 
                "WeightsFile": "BsMuMu_TrackIsolationBDT9vars_v1r4.xml"
            }, 
            {
                "DaughterLocations": {
                    "[Beauty ->  (X0 ->  Xs  X-)  (X0 ->  l+ ^l-)]CC": "TrackIsoBs2MMInfoL2", 
                    "[Beauty ->  (X0 ->  Xs  X-)  (X0 -> ^l+  l-)]CC": "TrackIsoBs2MMInfoL1", 
                    "[Beauty ->  (X0 ->  Xs  X-) ^(X0 ->  l+  l-)]CC": "TrackIsoBs2MMInfoLL", 
                    "[Beauty ->  (X0 ->  Xs ^X-)  (X0 ->  l+  l-)]CC": "TrackIsoBs2MMInfoH2", 
                    "[Beauty ->  (X0 ->  p+  pi-)  (X0 ->  l+ ^l-)]CC": "TrackIsoBs2MMInfoL2", 
                    "[Beauty ->  (X0 ->  p+  pi-)  (X0 -> ^l+  l-)]CC": "TrackIsoBs2MMInfoL1", 
                    "[Beauty ->  (X0 ->  p+  pi-) ^(X0 ->  l+  l-)]CC": "TrackIsoBs2MMInfoLL", 
                    "[Beauty ->  (X0 ->  p+ ^pi-)  (X0 ->  l+  l-)]CC": "TrackIsoBs2MMInfoH2", 
                    "[Beauty ->  (X0 ->  pi+  pi-)  (X0 ->  l+ ^l-)]CC": "TrackIsoBs2MMInfoL2", 
                    "[Beauty ->  (X0 ->  pi+  pi-)  (X0 -> ^l+  l-)]CC": "TrackIsoBs2MMInfoL1", 
                    "[Beauty ->  (X0 ->  pi+  pi-) ^(X0 ->  l+  l-)]CC": "TrackIsoBs2MMInfoLL", 
                    "[Beauty ->  (X0 ->  pi+ ^pi-)  (X0 ->  l+  l-)]CC": "TrackIsoBs2MMInfoH2", 
                    "[Beauty ->  (X0 -> ^Xs  X-)  (X0 ->  l+  l-)]CC": "TrackIsoBs2MMInfoH1", 
                    "[Beauty ->  (X0 -> ^p+  pi-)  (X0 ->  l+  l-)]CC": "TrackIsoBs2MMInfoH1", 
                    "[Beauty ->  (X0 -> ^pi+  pi-)  (X0 ->  l+  l-)]CC": "TrackIsoBs2MMInfoH1", 
                    "[Beauty ->  X+  (X0 ->  l+ ^l-)]CC": "TrackIsoBs2MMInfoL2", 
                    "[Beauty ->  X+  (X0 -> ^l+  l-)]CC": "TrackIsoBs2MMInfoL1", 
                    "[Beauty ->  X+ ^(X0 ->  l+  l-)]CC": "TrackIsoBs2MMInfoLL", 
                    "[Beauty -> ^(X0 ->  Xs  X-)  (X0 ->  l+  l-)]CC": "TrackIsoBs2MMInfoHH", 
                    "[Beauty -> ^(X0 ->  p+  pi-)  (X0 ->  l+  l-)]CC": "TrackIsoBs2MMInfoHH", 
                    "[Beauty -> ^(X0 ->  pi+  pi-)  (X0 ->  l+  l-)]CC": "TrackIsoBs2MMInfoHH", 
                    "[Beauty -> ^X+  (X0 ->  l+  l-)]CC": "TrackIsoBs2MMInfoH"
                }, 
                "IgnoreUnmatchedDescriptors": True, 
                "Location": "TrackIsoBs2MMInfo", 
                "Type": "RelInfoBs2MuMuTrackIsolations"
            }, 
            {
                "ConeAngle": 1.0, 
                "Location": "TrackIsoInfo10", 
                "Type": "RelInfoConeVariables"
            }, 
            {
                "ConeAngle": 1.5, 
                "Location": "TrackIsoInfo15", 
                "Type": "RelInfoConeVariables"
            }, 
            {
                "ConeAngle": 2.0, 
                "Location": "TrackIsoInfo20", 
                "Type": "RelInfoConeVariables"
            }, 
            {
                "ConeSize": 1.0, 
                "Location": "ConeIsoInfo10", 
                "Type": "RelInfoConeIsolation"
            }, 
            {
                "ConeSize": 1.5, 
                "Location": "ConeIsoInfo15", 
                "Type": "RelInfoConeIsolation"
            }, 
            {
                "ConeSize": 2.0, 
                "Location": "ConeIsoInfo20", 
                "Type": "RelInfoConeIsolation"
            }, 
            {
                "Location": "TrackIsolationBDT2", 
                "Particles": [
                    1, 
                    2
                ], 
                "Type": "RelInfoTrackIsolationBDT2"
            }
        ]
    }, 
    "STREAMS": [ "Leptonic" ], 
    "WGs": [ "RD" ]
}

B2Lambda0E = {
    "BUILDERTYPE": "B2Lambda0ELines", 
    "CONFIG": {
        "BDIRA": 0.99, 
        "BVCHI2DOF": 4.0, 
        "ElectronGHOSTPROB": 0.5, 
        "ElectronMINIPCHI2": 12, 
        "ElectronP": 3000.0, 
        "ElectronPIDK": 3.0, 
        "ElectronPIDp": 3.0, 
        "ElectronPIDpi": 3.0, 
        "ElectronPT": 250.0, 
        "ElectronTRCHI2": 4.0, 
        "GEC_nLongTrk": 300.0, 
        "Lambda0DaugMIPChi2": 10.0, 
        "Lambda0DaugP": 2000.0, 
        "Lambda0DaugPT": 250.0, 
        "Lambda0DaugTrackChi2": 4.0, 
        "Lambda0PT": 700.0, 
        "Lambda0VertexChi2": 10.0, 
        "LambdaEMassLowTight": 1500.0, 
        "LambdaZ": -1.0, 
        "MajoranaCutFDChi2": 100.0, 
        "MajoranaCutM": 1500.0, 
        "XEMassUpperHigh": 6500.0
    }, 
    "STREAMS": [ "EW" ], 
    "WGs": [ "RD" ]
}

B2Lambda0Mu = {
    "BUILDERTYPE": "B2Lambda0MuLines", 
    "CONFIG": {
        "BDIRA": 0.99, 
        "BVCHI2DOF": 4.0, 
        "GEC_nLongTrk": 300.0, 
        "Lambda0DaugMIPChi2": 10.0, 
        "Lambda0DaugP": 2000.0, 
        "Lambda0DaugPT": 250.0, 
        "Lambda0DaugTrackChi2": 4.0, 
        "Lambda0PT": 700.0, 
        "Lambda0VertexChi2": 10.0, 
        "LambdaMuMassLowTight": 1500.0, 
        "LambdaZ": -1.0, 
        "MajoranaCutFDChi2": 100.0, 
        "MajoranaCutM": 1500.0, 
        "MuonGHOSTPROB": 0.5, 
        "MuonMINIPCHI2": 12, 
        "MuonP": 3000.0, 
        "MuonPIDK": 0.0, 
        "MuonPIDmu": 0.0, 
        "MuonPIDp": 0.0, 
        "MuonPT": 250.0, 
        "MuonTRCHI2": 4.0, 
        "XMuMassUpperHigh": 6500.0
    }, 
    "STREAMS": [ "Dimuon" ], 
    "WGs": [ "RD" ]
}

B2Lambda0MuOS = {
    "BUILDERTYPE": "B2Lambda0MuOSLines", 
    "CONFIG": {
        "BDIRA": 0.99, 
        "BVCHI2DOF": 4.0, 
        "GEC_nLongTrk": 300.0, 
        "Lambda0DaugMIPChi2": 10.0, 
        "Lambda0DaugP": 2000.0, 
        "Lambda0DaugPT": 250.0, 
        "Lambda0DaugTrackChi2": 4.0, 
        "Lambda0PT": 700.0, 
        "Lambda0VertexChi2": 10.0, 
        "LambdaMuMassLowTight": 1500.0, 
        "LambdaZ": 5.0, 
        "MajoranaCutFDChi2": 100.0, 
        "MajoranaCutM": 1500.0, 
        "MuonGHOSTPROB": 0.5, 
        "MuonMINIPCHI2": 12, 
        "MuonP": 3000.0, 
        "MuonPIDK": 0.0, 
        "MuonPIDmu": 0.0, 
        "MuonPIDp": 0.0, 
        "MuonPT": 250.0, 
        "MuonTRCHI2": 4.0, 
        "XMuMassUpperHigh": 6500.0
    }, 
    "STREAMS": [ "Dimuon" ], 
    "WGs": [ "RD" ]
}

B2Lambda0SSMuMu3Pi = {
    "BUILDERTYPE": "B2Lambda0SSMuMu3PiLines", 
    "CONFIG": {
        "BDIRA": 0.99, 
        "BVCHI2DOF": 4.0, 
        "GEC_nLongTrk": 300.0, 
        "Lambda0DaugMIPChi2": 10.0, 
        "Lambda0DaugP": 2000.0, 
        "Lambda0DaugPT": 250.0, 
        "Lambda0DaugTrackChi2": 4.0, 
        "Lambda0PT": 700.0, 
        "Lambda0VertexChi2": 10.0, 
        "LambdaMuMassLowTight": 1500.0, 
        "LambdaZ": -1.0, 
        "MajoranaCutFDChi2": 100.0, 
        "MajoranaCutM": 1500.0, 
        "MuonGHOSTPROB": 0.5, 
        "MuonMINIPCHI2": 12, 
        "MuonP": 3000.0, 
        "MuonPIDK": 0.0, 
        "MuonPIDmu": 0.0, 
        "MuonPIDp": 0.0, 
        "MuonPT": 250.0, 
        "MuonTRCHI2": 4.0, 
        "XMuMassUpperHigh": 6500.0
    }, 
    "STREAMS": [ "Dimuon" ], 
    "WGs": [ "RD" ]
}

B2XMuMu = {
    "BUILDERTYPE": "B2XMuMuConf", 
    "CONFIG": {
        "A1_Comb_MassHigh": 4200.0, 
        "A1_Comb_MassLow": 0.0, 
        "A1_Dau_MaxIPCHI2": 16.0, 
        "A1_FlightChi2": 36.0, 
        "A1_MassHigh": 4000.0, 
        "A1_MassLow": 0.0, 
        "A1_MinIPCHI2": 4.0, 
        "A1_VtxChi2": 8.0, 
        "B_Comb_MassHigh": 7100.0, 
        "B_Comb_MassLow": 4600.0, 
        "B_DIRA": 0.9999, 
        "B_Dau_MaxIPCHI2": 9.0, 
        "B_FlightCHI2": 64.0, 
        "B_IPCHI2": 16.0, 
        "B_MassHigh": 7000.0, 
        "B_MassLow": 4700.0, 
        "B_VertexCHI2": 8.0, 
        "DECAYS": [
            "B0 -> J/psi(1S) phi(1020)", 
            "[B0 -> J/psi(1S) K*(892)0]cc", 
            "B0 -> J/psi(1S) rho(770)0", 
            "[B+ -> J/psi(1S) rho(770)+]cc", 
            "B0 -> J/psi(1S) f_2(1950)", 
            "B0 -> J/psi(1S) KS0", 
            "[B0 -> J/psi(1S) D~0]cc", 
            "[B+ -> J/psi(1S) K+]cc", 
            "[B+ -> J/psi(1S) pi+]cc", 
            "[B+ -> J/psi(1S) K*(892)+]cc", 
            "[B+ -> J/psi(1S) D+]cc", 
            "[B+ -> J/psi(1S) D*(2010)+]cc", 
            "[Lambda_b0 -> J/psi(1S) Lambda0]cc", 
            "[Lambda_b0 -> J/psi(1S) Lambda(1520)0]cc", 
            "B0 -> J/psi(1S) pi0", 
            "[B+ -> J/psi(1S) a_1(1260)+]cc", 
            "[B+ -> J/psi(1S) K_1(1270)+]cc", 
            "[B+ -> J/psi(1S) K_2(1770)+]cc", 
            "B0 -> J/psi(1S) K_1(1270)0", 
            "[B+ -> J/psi(1S) K_1(1400)+]cc", 
            "B0 -> J/psi(1S) K_1(1400)0", 
            "[Xi_b- -> J/psi(1S) Xi-]cc", 
            "[Omega_b- -> J/psi(1S) Omega-]cc", 
            "B0 -> J/psi(1S) f_1(1285)", 
            "B0 -> J/psi(1S) omega(782)"
        ], 
        "Dau_DIRA": -0.9, 
        "Dau_VertexCHI2": 12.0, 
        "Dimu_Dau_MaxIPCHI2": 6.0, 
        "Dimu_FlightChi2": 9.0, 
        "DimuonUPPERMASS": 7100.0, 
        "DimuonWS": True, 
        "HLT1_FILTER": None, 
        "HLT2_FILTER": None, 
        "HadronWS": False, 
        "Hadron_MinIPCHI2": 6.0, 
        "HyperonCombWindow": 65.0, 
        "HyperonMaxDocaChi2": 20.0, 
        "HyperonWindow": 50.0, 
        "K12OmegaK_CombMassHigh": 2000, 
        "K12OmegaK_CombMassLow": 400, 
        "K12OmegaK_MassHigh": 2100, 
        "K12OmegaK_MassLow": 300, 
        "K12OmegaK_VtxChi2": 8.0, 
        "KpiVXCHI2NDOF": 8.0, 
        "KsWINDOW": 30.0, 
        "Kstar_Comb_MassHigh": 6200.0, 
        "Kstar_Comb_MassLow": 0.0, 
        "Kstar_Dau_MaxIPCHI2": 6.0, 
        "Kstar_FlightChi2": 16.0, 
        "Kstar_MassHigh": 6200.0, 
        "Kstar_MassLow": 0.0, 
        "Kstar_MinIPCHI2": 0.0, 
        "KstarplusWINDOW": 300.0, 
        "L0DU_FILTER": None, 
        "LambdaWINDOW": 30.0, 
        "LongLivedPT": 0.0, 
        "LongLivedTau": 2, 
        "MuonNoPIDs_PIDmu": 0.0, 
        "MuonPID": -3.0, 
        "Muon_IsMuon": True, 
        "Muon_MinIPCHI2": 6.0, 
        "OmegaChi2Prob": 1e-05, 
        "Omega_CombMassWin": 400, 
        "Omega_MassWin": 400, 
        "Pi0ForOmegaMINPT": 800.0, 
        "Pi0MINPT": 800.0, 
        "RelatedInfoTools": [
            {
                "Location": "KSTARMUMUVARIABLES", 
                "Type": "RelInfoBKstarMuMuBDT", 
                "Variables": [
                    "MU_SLL_ISO_1", 
                    "MU_SLL_ISO_2"
                ]
            }, 
            {
                "Location": "KSTARMUMUVARIABLES2", 
                "Type": "RelInfoBKstarMuMuHad", 
                "Variables": [
                    "K_SLL_ISO_HAD", 
                    "PI_SLL_ISO_HAD"
                ]
            }, 
            {
                "Location": "ConeIsoInfo", 
                "Type": "RelInfoConeVariables", 
                "Variables": [
                    "CONEANGLE", 
                    "CONEMULT", 
                    "CONEPTASYM", 
                    "CONEPT", 
                    "CONEP", 
                    "CONEPASYM", 
                    "CONEDELTAETA", 
                    "CONEDELTAPHI"
                ]
            }, 
            {
                "Location": "VtxIsoInfo", 
                "Type": "RelInfoVertexIsolation", 
                "Variables": [
                    "VTXISONUMVTX", 
                    "VTXISODCHI2ONETRACK", 
                    "VTXISODCHI2MASSONETRACK", 
                    "VTXISODCHI2TWOTRACK", 
                    "VTXISODCHI2MASSTWOTRACK"
                ]
            }, 
            {
                "Location": "VtxIsoBDTInfo", 
                "Type": "RelInfoVertexIsolationBDT"
            }
        ], 
        "SpdMult": 600, 
        "Track_GhostProb": 0.5, 
        "UseNoPIDsHadrons": True
    }, 
    "STREAMS": [ "Leptonic" ], 
    "WGs": [ "RD" ]
}

B2XTauTau = {
    "BUILDERTYPE": "B2XTauTauConf", 
    "CONFIG": {
        "B2HTauTau_LinePostscale": 1, 
        "B2HTauTau_LinePrescale": 1, 
        "B2KTauTau_LinePostscale": 1, 
        "B2KTauTau_LinePrescale": 1, 
        "FDCHI2_B": 16, 
        "FDCHI2_BK": 16, 
        "FD_B_Max": 80, 
        "FD_Kst_Mu_KMM": 3, 
        "IPCHI2_Tr": 16, 
        "MASS_HIGH_B": 8000.0, 
        "MASS_HIGH_Etap": 1100.0, 
        "MASS_HIGH_Kst": 1100.0, 
        "MASS_HIGH_Phi": 1100.0, 
        "MASS_LOW_B": 2000.0, 
        "MASS_LOW_BK": 3000.0, 
        "MASS_LOW_Etap": 800.0, 
        "MASS_LOW_Kst": 700.0, 
        "MASS_LOW_Phi": 1000.0, 
        "PT_B": 2000.0, 
        "PT_Etap": 1000.0, 
        "PT_K": 1000.0, 
        "PT_Kst": 1000.0, 
        "PT_Phi": 1000.0, 
        "PT_Tr": 400.0, 
        "P_B": 10000.0, 
        "P_K": 4000.0, 
        "Photon_CL_Min": 0.2, 
        "Photon_Res_PT_Min": 400.0, 
        "RelInfoTools": [
            {
                "DaughterLocations": {
                    "[Beauty ->  Hadron  l ^l]CC": "Tau2_VertexIsoInfo", 
                    "[Beauty ->  Hadron ^l  l]CC": "Tau1_VertexIsoInfo", 
                    "[Beauty -> ^Hadron  l  l]CC": "H_VertexIsoInfo"
                }, 
                "Location": "BVars_VertexIsoInfo", 
                "Type": "RelInfoVertexIsolation"
            }, 
            {
                "ConeSize": 0.5, 
                "DaughterLocations": {
                    "[Beauty ->  Hadron  l ^l]CC": "Tau2_ConeIsoInfo", 
                    "[Beauty ->  Hadron ^l  l]CC": "Tau1_ConeIsoInfo", 
                    "[Beauty -> ^Hadron  l  l]CC": "H_ConeIsoInfo"
                }, 
                "Location": "BVars_ConeIsoInfo", 
                "Type": "RelInfoConeIsolation", 
                "Variables": []
            }
        ], 
        "SpdMult": "600", 
        "TRACKCHI2_Tr": 6, 
        "TRGHOPROB_Tr": 0.5, 
        "UsePID": True, 
        "VCHI2_B": 150, 
        "VCHI2_BK": 150, 
        "VCHI2_Eta": 16, 
        "VCHI2_Kst": 16, 
        "VCHI2_Phi": 16
    }, 
    "STREAMS": [ "Bhadron" ], 
    "WGs": [ "RD" ]
}

Beauty2XGamma = {
    "BUILDERTYPE": "Beauty2XGammaConf", 
    "CONFIG": {
        "B2PhiOmega2pipipi0MPrescale": 1.0, 
        "B2PhiOmega2pipipi0RPrescale": 1.0, 
        "B2XG2pi2KsPrescale": 1.0, 
        "B2XG2piCNVDDPrescale": 1.0, 
        "B2XG2piCNVLLPrescale": 1.0, 
        "B2XG2piKsPrescale": 1.0, 
        "B2XG2piPrescale": 1.0, 
        "B2XG2pipi0MPrescale": 1.0, 
        "B2XG2pipi0RPrescale": 1.0, 
        "B2XG3piCNVDDPrescale": 1.0, 
        "B2XG3piCNVLLPrescale": 1.0, 
        "B2XG3piKsPrescale": 1.0, 
        "B2XG3piPrescale": 1.0, 
        "B2XG3pipi0MPrescale": 1.0, 
        "B2XG3pipi0RPrescale": 1.0, 
        "B2XG4piPrescale": 1.0, 
        "B2XGBMaxM": 6500.0, 
        "B2XGBMinBPVDIRA": 0.0, 
        "B2XGBMinM2pi": 2400.0, 
        "B2XGBMinM3pi": 2400.0, 
        "B2XGBMinM4pi": 2000.0, 
        "B2XGBMinMLambda": 2560.0, 
        "B2XGBMinPT": 1000.0, 
        "B2XGBSumPtMin": 3000, 
        "B2XGBVtxChi2DOF": 9.0, 
        "B2XGBVtxMaxIPChi2": 9.0, 
        "B2XGG2piPrescale": 1.0, 
        "B2XGG3piPrescale": 1.0, 
        "B2XGGammaCL": 0.0, 
        "B2XGGammaCNVPTMin": 1000.0, 
        "B2XGGammaPTMin": 2000.0, 
        "B2XGLambda2piPrescale": 1.0, 
        "B2XGLambda3piPrescale": 1.0, 
        "B2XGLambdapiPrescale": 1.0, 
        "B2XGLbLambdaPrescale": 1.0, 
        "B2XGPhiOmegaMaxMass": 1300.0, 
        "B2XGPhiOmegaMinMass": 700.0, 
        "B2XGResBPVVDCHI2Min": 0.0, 
        "B2XGResIPCHI2Min": 0.0, 
        "B2XGResMaxMass": 7900.0, 
        "B2XGResMinMass": 0.0, 
        "B2XGResMinPT": 150.0, 
        "B2XGResSumPtMin": 1000.0, 
        "B2XGResVtxChi2DOF": 9.0, 
        "B2XGTrkChi2DOF": 3.0, 
        "B2XGTrkMinIPChi2": 20.0, 
        "B2XGTrkMinP": 1000, 
        "B2XGTrkMinPT": 300.0, 
        "B2XGpiKsPrescale": 1.0, 
        "Hlt1TISTOSLinesDict": {
            "Hlt1(Phi)?IncPhi.*Decision%TOS": 0, 
            "Hlt1(Two)?TrackMVA(Loose)?Decision%TOS": 0, 
            "Hlt1B2GammaGamma.*Decision%TOS": 0, 
            "Hlt1B2PhiGamma_LTUNB.*Decision%TOS": 0
        }, 
        "Hlt2TISTOSLinesDict": {
            "Hlt2(Phi)?IncPhi.*Decision%TOS": 0, 
            "Hlt2Radiative.*Decision%TOS": 0, 
            "Hlt2Topo(2|3|4)Body.*Decision%TOS": 0
        }, 
        "Pi0MPMin": 4000.0, 
        "Pi0MPTMin": 700.0, 
        "Pi0MPTReCut": 1200.0, 
        "Pi0RPMin": 4000.0, 
        "Pi0RPTMin": 700.0, 
        "Pi0RPTReCut": 1200.0, 
        "TrackGhostProb": 0.4
    }, 
    "STREAMS": [ "Leptonic" ], 
    "WGs": [ "RD" ]
}

Beauty2XGammaExclTDCPV = {
    "BUILDERTYPE": "Beauty2XGammaExclTDCPVConf", 
    "CONFIG": {
        "B0PVIPchi2": 16.0, 
        "B0VTXchi2": 16.0, 
        "BMassMax": 7000.0, 
        "BMassMin": 4000.0, 
        "B_APT": 3000.0, 
        "B_PT": 2500.0, 
        "Bd2KspipiGammaPostScale": 1.0, 
        "Bd2KspipiGammaPreScale": 1.0, 
        "Bd2KstGammaPostScale": 1.0, 
        "Bd2KstGammaPreScale": 1.0, 
        "BdDIRA": 0.2, 
        "Bs2KsKpiGammaPostScale": 1.0, 
        "Bs2KsKpiGammaPreScale": 1.0, 
        "Bs2PhiGammaPostScale": 1.0, 
        "Bs2PhiGammaPreScale": 1.0, 
        "BsPVIPchi2": 16.0, 
        "BsVTXchi2": 16.0, 
        "DTF_CL": 1e-10, 
        "GhostProb_Max": 0.6, 
        "Hlt1TISTOSLinesDict": {
            "Hlt1(Two)?TrackMVA(Loose)?Decision%TOS": 0
        }, 
        "Hlt1TISTOSLinesDict_Phi": {
            "Hlt1(Phi)?IncPhi.*Decision%TOS": 0, 
            "Hlt1(Two)?TrackMVA(Loose)?Decision%TOS": 0, 
            "Hlt1B2PhiGamma_LTUNBDecision%TOS": 0
        }, 
        "Hlt2TISTOSLinesDict": {
            "Hlt2Radiative.*Decision%TOS": 0, 
            "Hlt2Topo(2|3|4)Body.*Decision%TOS": 0
        }, 
        "Hlt2TISTOSLinesDict_Phi": {
            "Hlt2(Phi)?IncPhi.*Decision%TOS": 0, 
            "Hlt2Radiative.*Decision%TOS": 0, 
            "Hlt2Topo(2|3|4)Body.*Decision%TOS": 0
        }, 
        "KsPT": 300.0, 
        "KstMassWin": 100.0, 
        "KstVCHI2": 25.0, 
        "MinTrack_P": 3000.0, 
        "MinTrack_PT": 250.0, 
        "PhiMassWin": 15.0, 
        "PhiVCHI2": 25.0, 
        "ProbNNk": 0.05, 
        "ProbNNpi": 0.05, 
        "SumVec_PT": 500.0, 
        "TrChi2": 4.0, 
        "TrIPchi2": 9.0, 
        "mKsKpiMax": 2300.0, 
        "mKspipiMax": 2000.0, 
        "photonPT": 2500.0
    }, 
    "STREAMS": [ "BhadronCompleteEvent" ], 
    "WGs": [ "RD" ]
}

Bs2MuMuLines = {
    "BUILDERTYPE": "Bs2MuMuLinesConf", 
    "CONFIG": {
        "BFDChi2_loose": 100, 
        "BIPChi2_loose": 64, 
        "BPVVDChi2": 225, 
        "B_BPVIPChi2": 25, 
        "B_BPVIPChi2_LTUB": 25, 
        "B_BPVIPChi2_bsst": 16, 
        "B_Pt": 350, 
        "B_Pt_LTUB": 500, 
        "B_maximum_decaytime_bsst": 0.2, 
        "B_minimum_decaytime_LTUB": 0.6, 
        "B_minimum_decaytime_bsst": 0.0, 
        "BdPrescale": 1, 
        "Bs2KKLTUBLinePrescale": 1, 
        "Bs2mmLTUBLinePrescale": 1, 
        "Bs2mmWideLinePrescale": 1, 
        "BsPrescale": 1, 
        "Bsst2mmLinePrescale": 1, 
        "BuPrescale": 1, 
        "DOCA": 0.3, 
        "DOCA_LTUB": 0.3, 
        "DOCA_loose": 0.5, 
        "DefaultLinePrescale": 1, 
        "DefaultPostscale": 1, 
        "JPsiLinePrescale": 1, 
        "JPsiLooseLinePrescale": 0.1, 
        "JPsiPromptLinePrescale": 0.005, 
        "LooseLinePrescale": 0.0, 
        "MuIPChi2_loose": 9, 
        "MuTrChi2_loose": 10, 
        "ProbNN": 0.4, 
        "SSPrescale": 1, 
        "SUMPT": 4500, 
        "TrackGhostProb": 0.4, 
        "TrackGhostProb_bsst": 0.3, 
        "VCHI2_VDOF": 9, 
        "VCHI2_VDOF_LTUB": 9, 
        "VCHI2_VDOF_bsst": 6, 
        "VCHI2_VDOF_loose": 25, 
        "daughter_IPChi2": 25, 
        "daughter_TrChi2": 4, 
        "daughter_TrChi2_LTUB": 4, 
        "daughter_TrChi2_bsst": 3, 
        "muon_PT_LTUB": 40
    }, 
    "STREAMS": {
        "Dimuon": [
            "StrippingBs2MuMuLinesNoMuIDFullDSTLine", 
            "StrippingBs2MuMuLinesWideMassFullDSTLine", 
            "StrippingBs2MuMuLinesBu2JPsiKFullDSTLine"
        ], 
        "Leptonic": [
            "StrippingBs2MuMuLinesBs2JPsiPhiLine", 
            "StrippingBs2MuMuLinesBs2KKLTUBLine", 
            "StrippingBs2MuMuLinesNoMuIDLine", 
            "StrippingBs2MuMuLinesSSLine", 
            "StrippingBs2MuMuLinesBd2JPsiKstLine", 
            "StrippingBs2MuMuLinesLTUBLine", 
            "StrippingBs2MuMuLinesBu2JPsiKLine"
        ]
    }, 
    "WGs": [ "RD" ]
}

Bu2LLK = {
    "BUILDERTYPE": "Bu2LLKConf", 
    "CONFIG": {
        "BDIRA": 0.9995, 
        "BFlightCHI2": 100, 
        "BIPCHI2": 25, 
        "BMassWindow": 1500, 
        "BMassWindowTau": 5000, 
        "BVertexCHI2": 9, 
        "Bu2eeLine2Prescale": 1, 
        "Bu2eeLine3Prescale": 1, 
        "Bu2eeLine4Prescale": 1, 
        "Bu2eeLinePrescale": 1, 
        "Bu2eeSSLine2Prescale": 1, 
        "Bu2meLinePrescale": 1, 
        "Bu2meSSLinePrescale": 1, 
        "Bu2mmLinePrescale": 1, 
        "Bu2mmSSLinePrescale": 1, 
        "Bu2mtLinePrescale": 1, 
        "Bu2mtSSLinePrescale": 1, 
        "DiHadronADOCACHI2": 30, 
        "DiHadronMass": 2600, 
        "DiHadronVtxCHI2": 25, 
        "DiLeptonFDCHI2": 16, 
        "DiLeptonIPCHI2": 0, 
        "DiLeptonPT": 0, 
        "K1_MassWindow_Hi": 4200, 
        "K1_MassWindow_Lo": 0, 
        "K1_SumIPChi2Had": 48.0, 
        "K1_SumPTHad": 1200, 
        "K1_VtxChi2": 12, 
        "KaonIPCHI2": 9, 
        "KaonPT": 400, 
        "KaonPTLoose": 250, 
        "KstarPADOCACHI2": 30, 
        "KstarPMassWindow": 300, 
        "KstarPVertexCHI2": 25, 
        "LeptonIPCHI2": 9, 
        "LeptonPT": 350, 
        "LeptonPTTight": 500, 
        "PIDe": 0, 
        "Pi0PT": 600, 
        "PionPTRho": 350, 
        "ProbNNCut": 0.05, 
        "ProbNNCutTight": 0.1, 
        "ProtonP": 2000, 
        "RICHPIDe_Up": -5, 
        "RelatedInfoTools": [
            {
                "DaughterLocations": {
                    "[Beauty ->  (X0 ->  X+  X-) ^(X ->  l+  l+)]CC": "VertexIsoInfoLL", 
                    "[Beauty ->  (X0 ->  X+  X-) ^(X0 ->  l+  l-)]CC": "VertexIsoInfoLL", 
                    "[Beauty ->  X  (X+ ->  l+  ^(l+ -> X-  X-  X+))]CC": "VertexIsoInfoL", 
                    "[Beauty ->  X  (X0 ->  l+  ^(l- -> X-  X-  X+))]CC": "VertexIsoInfoL", 
                    "[Beauty ->  X+ ^(X+ ->  l+  l+)]CC": "VertexIsoInfoLL", 
                    "[Beauty ->  X+ ^(X0 ->  l+  l-)]CC": "VertexIsoInfoLL", 
                    "[Beauty -> (X+ ->  X+ ^(X0 ->  X+  X-)) (X+ ->  l+  l+)]CC": "VertexIsoInfoHH", 
                    "[Beauty -> (X+ ->  X+ ^(X0 ->  X+  X-)) (X0 ->  l+  l-)]CC": "VertexIsoInfoHH", 
                    "[Beauty -> (X+ -> X+  X0) ^(X0 ->  l+  l+)]CC": "VertexIsoInfoLL", 
                    "[Beauty -> (X+ -> X+  X0) ^(X0 ->  l+  l-)]CC": "VertexIsoInfoLL", 
                    "[Beauty -> ^(X+ -> X+  (X0 ->  X+  X-)) (X+ ->  l+  l+)]CC": "VertexIsoInfoH", 
                    "[Beauty -> ^(X+ -> X+  (X0 ->  X+  X-)) (X0 ->  l+  l-)]CC": "VertexIsoInfoH", 
                    "[Beauty -> ^(X+ -> X+  X+  X-) (X+ ->  l+  l+)]CC": "VertexIsoInfoH", 
                    "[Beauty -> ^(X+ -> X+  X+  X-) (X0 ->  l+  l-)]CC": "VertexIsoInfoH", 
                    "[Beauty -> ^(X+ -> X+  X0) (X0 ->  l+  l+)]CC": "VertexIsoInfoHH", 
                    "[Beauty -> ^(X+ -> X+  X0) (X0 ->  l+  l-)]CC": "VertexIsoInfoHH", 
                    "[Beauty -> ^(X0 ->  X+  X-)  (X ->  l+  l+)]CC": "VertexIsoInfoHH", 
                    "[Beauty -> ^(X0 ->  X+  X-)  (X0 ->  l+  l-)]CC": "VertexIsoInfoHH"
                }, 
                "IgnoreUnmatchedDescriptors": True, 
                "Location": "VertexIsoInfo", 
                "Type": "RelInfoVertexIsolation"
            }, 
            {
                "DaughterLocations": {
                    "[Beauty ->  (X0 ->  X+  X-) ^(X ->  l+  l+)]CC": "VertexIsoBDTInfoLL", 
                    "[Beauty ->  (X0 ->  X+  X-) ^(X0 ->  l+  l-)]CC": "VertexIsoBDTInfoLL", 
                    "[Beauty ->  X  (X+ ->  l+  ^(l+ -> X-  X-  X+))]CC": "VertexIsoBDTInfoL", 
                    "[Beauty ->  X  (X0 ->  l+  ^(l- -> X-  X-  X+))]CC": "VertexIsoBDTInfoL", 
                    "[Beauty ->  X+ ^(X+ ->  l+  l+)]CC": "VertexIsoBDTInfoLL", 
                    "[Beauty ->  X+ ^(X0 ->  l+  l-)]CC": "VertexIsoBDTInfoLL", 
                    "[Beauty -> (X+ ->  X+ ^(X0 ->  X+  X-)) (X+ ->  l+  l+)]CC": "VertexIsoBDTInfoHH", 
                    "[Beauty -> (X+ ->  X+ ^(X0 ->  X+  X-)) (X0 ->  l+  l-)]CC": "VertexIsoBDTInfoHH", 
                    "[Beauty -> (X+ -> X+  X0) ^(X0 ->  l+  l+)]CC": "VertexIsoBDTInfoLL", 
                    "[Beauty -> (X+ -> X+  X0) ^(X0 ->  l+  l-)]CC": "VertexIsoBDTInfoLL", 
                    "[Beauty -> ^(X+ -> X+  (X0 ->  X+  X-)) (X+ ->  l+  l+)]CC": "VertexIsoBDTInfoH", 
                    "[Beauty -> ^(X+ -> X+  (X0 ->  X+  X-)) (X0 ->  l+  l-)]CC": "VertexIsoBDTInfoH", 
                    "[Beauty -> ^(X+ -> X+  X+  X-) (X+ ->  l+  l+)]CC": "VertexIsoBDTInfoH", 
                    "[Beauty -> ^(X+ -> X+  X+  X-) (X0 ->  l+  l-)]CC": "VertexIsoBDTInfoH", 
                    "[Beauty -> ^(X+ -> X+  X0) (X0 ->  l+  l+)]CC": "VertexIsoBDTInfoHH", 
                    "[Beauty -> ^(X+ -> X+  X0) (X0 ->  l+  l-)]CC": "VertexIsoBDTInfoHH", 
                    "[Beauty -> ^(X0 ->  X+  X-)  (X ->  l+  l+)]CC": "VertexIsoBDTInfoHH", 
                    "[Beauty -> ^(X0 ->  X+  X-)  (X0 ->  l+  l-)]CC": "VertexIsoBDTInfoHH"
                }, 
                "IgnoreUnmatchedDescriptors": True, 
                "Location": "VertexIsoBDTInfo", 
                "Type": "RelInfoVertexIsolationBDT"
            }, 
            {
                "ConeAngle": 0.5, 
                "DaughterLocations": {
                    "[Beauty ->  (X0 ->  X+  X-)  (X+ ->  l+  (l+ ->  X+  X- ^X+))]CC": "TrackIsoInfoL23", 
                    "[Beauty ->  (X0 ->  X+  X-)  (X+ ->  l+  (l+ ->  X+ ^X-  X+))]CC": "TrackIsoInfoL22", 
                    "[Beauty ->  (X0 ->  X+  X-)  (X+ ->  l+  (l+ -> ^X+  X-  X+))]CC": "TrackIsoInfoL21", 
                    "[Beauty ->  (X0 ->  X+  X-)  (X0 ->  l+  (l- ->  X-  X- ^X+))]CC": "TrackIsoInfoL23", 
                    "[Beauty ->  (X0 ->  X+  X-)  (X0 ->  l+  (l- ->  X- ^X-  X+))]CC": "TrackIsoInfoL22", 
                    "[Beauty ->  (X0 ->  X+  X-)  (X0 ->  l+  (l- -> ^X-  X-  X+))]CC": "TrackIsoInfoL21", 
                    "[Beauty ->  (X0 ->  Xs  X-)  (X ->  l+ ^l+)]CC": "TrackIsoInfoL2", 
                    "[Beauty ->  (X0 ->  Xs  X-)  (X ->  l- ^l-)]CC": "TrackIsoInfoL2", 
                    "[Beauty ->  (X0 ->  Xs  X-)  (X -> ^l+  l+)]CC": "TrackIsoInfoL1", 
                    "[Beauty ->  (X0 ->  Xs  X-)  (X -> ^l-  l-)]CC": "TrackIsoInfoL1", 
                    "[Beauty ->  (X0 ->  Xs  X-)  (X0 ->  l+ ^l-)]CC": "TrackIsoInfoL2", 
                    "[Beauty ->  (X0 ->  Xs  X-)  (X0 -> ^l+  l-)]CC": "TrackIsoInfoL1", 
                    "[Beauty ->  (X0 ->  Xs  X-) ^(X ->  l+  l+)]CC": "TrackIsoInfoLL", 
                    "[Beauty ->  (X0 ->  Xs  X-) ^(X ->  l-  l-)]CC": "TrackIsoInfoLL", 
                    "[Beauty ->  (X0 ->  Xs  X-) ^(X0 ->  l+  l-)]CC": "TrackIsoInfoLL", 
                    "[Beauty ->  (X0 ->  Xs ^X-)  (X ->  l+  l+)]CC": "TrackIsoInfoH2", 
                    "[Beauty ->  (X0 ->  Xs ^X-)  (X ->  l-  l-)]CC": "TrackIsoInfoH2", 
                    "[Beauty ->  (X0 ->  Xs ^X-)  (X0 ->  l+  l-)]CC": "TrackIsoInfoH2", 
                    "[Beauty ->  (X0 ->  p+  pi-)  (X ->  l+ ^l+)]CC": "TrackIsoInfoL2", 
                    "[Beauty ->  (X0 ->  p+  pi-)  (X ->  l- ^l-)]CC": "TrackIsoInfoL2", 
                    "[Beauty ->  (X0 ->  p+  pi-)  (X -> ^l+  l+)]CC": "TrackIsoInfoL1", 
                    "[Beauty ->  (X0 ->  p+  pi-)  (X -> ^l-  l-)]CC": "TrackIsoInfoL1", 
                    "[Beauty ->  (X0 ->  p+  pi-)  (X0 ->  l+ ^l-)]CC": "TrackIsoInfoL2", 
                    "[Beauty ->  (X0 ->  p+  pi-)  (X0 -> ^l+  l-)]CC": "TrackIsoInfoL1", 
                    "[Beauty ->  (X0 ->  p+  pi-) ^(X ->  l+  l+)]CC": "TrackIsoInfoLL", 
                    "[Beauty ->  (X0 ->  p+  pi-) ^(X ->  l-  l-)]CC": "TrackIsoInfoLL", 
                    "[Beauty ->  (X0 ->  p+  pi-) ^(X0 ->  l+  l-)]CC": "TrackIsoInfoLL", 
                    "[Beauty ->  (X0 ->  p+ ^pi-)  (X ->  l+  l+)]CC": "TrackIsoInfoH2", 
                    "[Beauty ->  (X0 ->  p+ ^pi-)  (X ->  l-  l-)]CC": "TrackIsoInfoH2", 
                    "[Beauty ->  (X0 ->  p+ ^pi-)  (X0 ->  l+  l-)]CC": "TrackIsoInfoH2", 
                    "[Beauty ->  (X0 ->  pi+  pi-)  (X+ ->  l+ ^l+)]CC": "TrackIsoInfoL2", 
                    "[Beauty ->  (X0 ->  pi+  pi-)  (X+ -> ^l+  l+)]CC": "TrackIsoInfoL1", 
                    "[Beauty ->  (X0 ->  pi+  pi-)  (X0 ->  l+ ^l-)]CC": "TrackIsoInfoL2", 
                    "[Beauty ->  (X0 ->  pi+  pi-)  (X0 -> ^l+  l-)]CC": "TrackIsoInfoL1", 
                    "[Beauty ->  (X0 ->  pi+  pi-) ^(X+ ->  l+  l+)]CC": "TrackIsoInfoLL", 
                    "[Beauty ->  (X0 ->  pi+  pi-) ^(X0 ->  l+  l-)]CC": "TrackIsoInfoLL", 
                    "[Beauty ->  (X0 ->  pi+ ^pi-)  (X+ ->  l+  l+)]CC": "TrackIsoInfoH2", 
                    "[Beauty ->  (X0 ->  pi+ ^pi-)  (X0 ->  l+  l-)]CC": "TrackIsoInfoH2", 
                    "[Beauty ->  (X0 -> ^Xs  X-)  (X ->  l+  l+)]CC": "TrackIsoInfoH1", 
                    "[Beauty ->  (X0 -> ^Xs  X-)  (X ->  l-  l-)]CC": "TrackIsoInfoH1", 
                    "[Beauty ->  (X0 -> ^Xs  X-)  (X0 ->  l+  l-)]CC": "TrackIsoInfoH1", 
                    "[Beauty ->  (X0 -> ^p+  pi-)  (X ->  l+  l+)]CC": "TrackIsoInfoH1", 
                    "[Beauty ->  (X0 -> ^p+  pi-)  (X ->  l-  l-)]CC": "TrackIsoInfoH1", 
                    "[Beauty ->  (X0 -> ^p+  pi-)  (X0 ->  l+  l-)]CC": "TrackIsoInfoH1", 
                    "[Beauty ->  (X0 -> ^pi+  pi-)  (X+ ->  l+  l+)]CC": "TrackIsoInfoH1", 
                    "[Beauty ->  (X0 -> ^pi+  pi-)  (X0 ->  l+  l-)]CC": "TrackIsoInfoH1", 
                    "[Beauty ->  X+  (X+ ->  l+ ^l+)]CC": "TrackIsoInfoL2", 
                    "[Beauty ->  X+  (X+ -> ^l+  l+)]CC": "TrackIsoInfoL1", 
                    "[Beauty ->  X+  (X0 ->  l+ ^l-)]CC": "TrackIsoInfoL2", 
                    "[Beauty ->  X+  (X0 -> ^l+  l-)]CC": "TrackIsoInfoL1", 
                    "[Beauty ->  X+ ^(X+ ->  l+  l+)]CC": "TrackIsoInfoLL", 
                    "[Beauty ->  X+ ^(X0 ->  l+  l-)]CC": "TrackIsoInfoLL", 
                    "[Beauty -> (K*(892)+ -> K+  ^pi0) (X0 ->  l+  l+)]CC": "TrackIsoInfopi0H2", 
                    "[Beauty -> (K*(892)+ -> K+  ^pi0) (X0 ->  l+  l-)]CC": "TrackIsoInfopi0H2", 
                    "[Beauty -> (K*(892)+ -> K+  pi0) (X0 ->  ^l+  l+)]CC": "TrackIsoInfopi0L1", 
                    "[Beauty -> (K*(892)+ -> K+  pi0) (X0 ->  ^l+  l-)]CC": "TrackIsoInfopi0L1", 
                    "[Beauty -> (K*(892)+ -> K+  pi0) (X0 ->  l+  ^l+)]CC": "TrackIsoInfopi0L2", 
                    "[Beauty -> (K*(892)+ -> K+  pi0) (X0 ->  l+  ^l-)]CC": "TrackIsoInfopi0L2", 
                    "[Beauty -> (K*(892)+ -> ^K+  pi0) (X0 ->  l+  l+)]CC": "TrackIsoInfopi0H1", 
                    "[Beauty -> (K*(892)+ -> ^K+  pi0) (X0 ->  l+  l-)]CC": "TrackIsoInfopi0H1", 
                    "[Beauty -> (X+ ->  X+  (X0 ->  X+  X-)) (X+ ->  l+ ^l+)]CC": "TrackIsoInfoL2", 
                    "[Beauty -> (X+ ->  X+  (X0 ->  X+  X-)) (X+ -> ^l+  l+)]CC": "TrackIsoInfoL1", 
                    "[Beauty -> (X+ ->  X+  (X0 ->  X+  X-)) (X0 ->  l+ ^l-)]CC": "TrackIsoInfoL2", 
                    "[Beauty -> (X+ ->  X+  (X0 ->  X+  X-)) (X0 -> ^l+  l-)]CC": "TrackIsoInfoL1", 
                    "[Beauty -> (X+ ->  X+  (X0 ->  X+ ^X-)) (X+ ->  l+  l+)]CC": "TrackIsoInfoH3", 
                    "[Beauty -> (X+ ->  X+  (X0 ->  X+ ^X-)) (X0 ->  l+  l-)]CC": "TrackIsoInfoH3", 
                    "[Beauty -> (X+ ->  X+  (X0 -> ^X+  X-)) (X+ ->  l+  l+)]CC": "TrackIsoInfoH2", 
                    "[Beauty -> (X+ ->  X+  (X0 -> ^X+  X-)) (X0 ->  l+  l-)]CC": "TrackIsoInfoH2", 
                    "[Beauty -> (X+ ->  X+  X+ ^X-) (X+ ->  l+  l+)]CC": "TrackIsoInfoH3", 
                    "[Beauty -> (X+ ->  X+  X+ ^X-) (X0 ->  l+  l-)]CC": "TrackIsoInfoH3", 
                    "[Beauty -> (X+ ->  X+ ^(X0 ->  X+  X-)) (X+ ->  l+  l+)]CC": "TrackIsoInfoHH", 
                    "[Beauty -> (X+ ->  X+ ^(X0 ->  X+  X-)) (X0 ->  l+  l-)]CC": "TrackIsoInfoHH", 
                    "[Beauty -> (X+ ->  X+ ^X+  X-) (X+ ->  l+  l+)]CC": "TrackIsoInfoH2", 
                    "[Beauty -> (X+ ->  X+ ^X+  X-) (X0 ->  l+  l-)]CC": "TrackIsoInfoH2", 
                    "[Beauty -> (X+ -> ^X+  (X0 ->  X+  X-)) (X+ ->  l+  l+)]CC": "TrackIsoInfoH1", 
                    "[Beauty -> (X+ -> ^X+  (X0 ->  X+  X-)) (X0 ->  l+  l-)]CC": "TrackIsoInfoH1", 
                    "[Beauty -> (X+ -> ^X+  X+  X-) (X+ ->  l+  l+)]CC": "TrackIsoInfoH1", 
                    "[Beauty -> (X+ -> ^X+  X+  X-) (X0 ->  l+  l-)]CC": "TrackIsoInfoH1", 
                    "[Beauty -> ^(X0 ->  Xs  X-)  (X ->  l+  l+)]CC": "TrackIsoInfoHH", 
                    "[Beauty -> ^(X0 ->  Xs  X-)  (X ->  l-  l-)]CC": "TrackIsoInfoHH", 
                    "[Beauty -> ^(X0 ->  Xs  X-)  (X0 ->  l+  l-)]CC": "TrackIsoInfoHH", 
                    "[Beauty -> ^(X0 ->  p+  pi-)  (X ->  l+  l+)]CC": "TrackIsoInfoHH", 
                    "[Beauty -> ^(X0 ->  p+  pi-)  (X ->  l-  l-)]CC": "TrackIsoInfoHH", 
                    "[Beauty -> ^(X0 ->  p+  pi-)  (X0 ->  l+  l-)]CC": "TrackIsoInfoHH", 
                    "[Beauty -> ^(X0 ->  pi+  pi-)  (X+ ->  l+  l+)]CC": "TrackIsoInfoHH", 
                    "[Beauty -> ^(X0 ->  pi+  pi-)  (X0 ->  l+  l-)]CC": "TrackIsoInfoHH", 
                    "[Beauty -> ^X+  (X+ ->  l+  l+)]CC": "TrackIsoInfoH", 
                    "[Beauty -> ^X+  (X0 ->  l+  l-)]CC": "TrackIsoInfoH"
                }, 
                "IgnoreUnmatchedDescriptors": True, 
                "Location": "TrackIsoInfo05", 
                "Type": "RelInfoConeVariables"
            }, 
            {
                "ConeSize": 0.5, 
                "DaughterLocations": {
                    "[Beauty ->  (X0 ->  X+  X-)  (X+ ->  l+  (l+ ->  X+  X- ^X+))]CC": "ConeIsoInfoL23", 
                    "[Beauty ->  (X0 ->  X+  X-)  (X+ ->  l+  (l+ ->  X+ ^X-  X+))]CC": "ConeIsoInfoL22", 
                    "[Beauty ->  (X0 ->  X+  X-)  (X+ ->  l+  (l+ -> ^X+  X-  X+))]CC": "ConeIsoInfoL21", 
                    "[Beauty ->  (X0 ->  X+  X-)  (X0 ->  l+  (l- ->  X-  X- ^X+))]CC": "ConeIsoInfoL23", 
                    "[Beauty ->  (X0 ->  X+  X-)  (X0 ->  l+  (l- ->  X- ^X-  X+))]CC": "ConeIsoInfoL22", 
                    "[Beauty ->  (X0 ->  X+  X-)  (X0 ->  l+  (l- -> ^X-  X-  X+))]CC": "ConeIsoInfoL21", 
                    "[Beauty ->  (X0 ->  Xs  X-)  (X ->  l+ ^l+)]CC": "ConeIsoInfoL2", 
                    "[Beauty ->  (X0 ->  Xs  X-)  (X ->  l- ^l-)]CC": "ConeIsoInfoL2", 
                    "[Beauty ->  (X0 ->  Xs  X-)  (X -> ^l+  l+)]CC": "ConeIsoInfoL1", 
                    "[Beauty ->  (X0 ->  Xs  X-)  (X -> ^l-  l-)]CC": "ConeIsoInfoL1", 
                    "[Beauty ->  (X0 ->  Xs  X-)  (X0 ->  l+ ^l-)]CC": "ConeIsoInfoL2", 
                    "[Beauty ->  (X0 ->  Xs  X-)  (X0 -> ^l+  l-)]CC": "ConeIsoInfoL1", 
                    "[Beauty ->  (X0 ->  Xs  X-) ^(X ->  l+  l+)]CC": "ConeIsoInfoLL", 
                    "[Beauty ->  (X0 ->  Xs  X-) ^(X ->  l-  l-)]CC": "ConeIsoInfoLL", 
                    "[Beauty ->  (X0 ->  Xs  X-) ^(X0 ->  l+  l-)]CC": "ConeIsoInfoLL", 
                    "[Beauty ->  (X0 ->  Xs ^X-)  (X ->  l+  l+)]CC": "ConeIsoInfoH2", 
                    "[Beauty ->  (X0 ->  Xs ^X-)  (X ->  l-  l-)]CC": "ConeIsoInfoH2", 
                    "[Beauty ->  (X0 ->  Xs ^X-)  (X0 ->  l+  l-)]CC": "ConeIsoInfoH2", 
                    "[Beauty ->  (X0 ->  p+  pi-)  (X ->  l+ ^l+)]CC": "ConeIsoInfoL2", 
                    "[Beauty ->  (X0 ->  p+  pi-)  (X ->  l- ^l-)]CC": "ConeIsoInfoL2", 
                    "[Beauty ->  (X0 ->  p+  pi-)  (X -> ^l+  l+)]CC": "ConeIsoInfoL1", 
                    "[Beauty ->  (X0 ->  p+  pi-)  (X -> ^l-  l-)]CC": "ConeIsoInfoL1", 
                    "[Beauty ->  (X0 ->  p+  pi-)  (X0 ->  l+ ^l-)]CC": "ConeIsoInfoL2", 
                    "[Beauty ->  (X0 ->  p+  pi-)  (X0 -> ^l+  l-)]CC": "ConeIsoInfoL1", 
                    "[Beauty ->  (X0 ->  p+  pi-) ^(X ->  l+  l+)]CC": "ConeIsoInfoLL", 
                    "[Beauty ->  (X0 ->  p+  pi-) ^(X ->  l-  l-)]CC": "ConeIsoInfoLL", 
                    "[Beauty ->  (X0 ->  p+  pi-) ^(X0 ->  l+  l-)]CC": "ConeIsoInfoLL", 
                    "[Beauty ->  (X0 ->  p+ ^pi-)  (X ->  l+  l+)]CC": "ConeIsoInfoH2", 
                    "[Beauty ->  (X0 ->  p+ ^pi-)  (X ->  l-  l-)]CC": "ConeIsoInfoH2", 
                    "[Beauty ->  (X0 ->  p+ ^pi-)  (X0 ->  l+  l-)]CC": "ConeIsoInfoH2", 
                    "[Beauty ->  (X0 ->  pi+  pi-)  (X+ ->  l+ ^l+)]CC": "ConeIsoInfoL2", 
                    "[Beauty ->  (X0 ->  pi+  pi-)  (X+ -> ^l+  l+)]CC": "ConeIsoInfoL1", 
                    "[Beauty ->  (X0 ->  pi+  pi-)  (X0 ->  l+ ^l-)]CC": "ConeIsoInfoL2", 
                    "[Beauty ->  (X0 ->  pi+  pi-)  (X0 -> ^l+  l-)]CC": "ConeIsoInfoL1", 
                    "[Beauty ->  (X0 ->  pi+  pi-) ^(X+ ->  l+  l+)]CC": "ConeIsoInfoLL", 
                    "[Beauty ->  (X0 ->  pi+  pi-) ^(X0 ->  l+  l-)]CC": "ConeIsoInfoLL", 
                    "[Beauty ->  (X0 ->  pi+ ^pi-)  (X+ ->  l+  l+)]CC": "ConeIsoInfoH2", 
                    "[Beauty ->  (X0 ->  pi+ ^pi-)  (X0 ->  l+  l-)]CC": "ConeIsoInfoH2", 
                    "[Beauty ->  (X0 -> ^Xs  X-)  (X ->  l+  l+)]CC": "ConeIsoInfoH1", 
                    "[Beauty ->  (X0 -> ^Xs  X-)  (X ->  l-  l-)]CC": "ConeIsoInfoH1", 
                    "[Beauty ->  (X0 -> ^Xs  X-)  (X0 ->  l+  l-)]CC": "ConeIsoInfoH1", 
                    "[Beauty ->  (X0 -> ^p+  pi-)  (X ->  l+  l+)]CC": "ConeIsoInfoH1", 
                    "[Beauty ->  (X0 -> ^p+  pi-)  (X ->  l-  l-)]CC": "ConeIsoInfoH1", 
                    "[Beauty ->  (X0 -> ^p+  pi-)  (X0 ->  l+  l-)]CC": "ConeIsoInfoH1", 
                    "[Beauty ->  (X0 -> ^pi+  pi-)  (X+ ->  l+  l+)]CC": "ConeIsoInfoH1", 
                    "[Beauty ->  (X0 -> ^pi+  pi-)  (X0 ->  l+  l-)]CC": "ConeIsoInfoH1", 
                    "[Beauty ->  X+  (X+ ->  l+ ^l+)]CC": "ConeIsoInfoL2", 
                    "[Beauty ->  X+  (X+ -> ^l+  l+)]CC": "ConeIsoInfoL1", 
                    "[Beauty ->  X+  (X0 ->  l+ ^l-)]CC": "ConeIsoInfoL2", 
                    "[Beauty ->  X+  (X0 -> ^l+  l-)]CC": "ConeIsoInfoL1", 
                    "[Beauty ->  X+ ^(X+ ->  l+  l+)]CC": "ConeIsoInfoLL", 
                    "[Beauty ->  X+ ^(X0 ->  l+  l-)]CC": "ConeIsoInfoLL", 
                    "[Beauty -> (K*(892)+ -> K+  ^pi0) (X0 ->  l+  l+)]CC": "ConeIsoInfopi0H2", 
                    "[Beauty -> (K*(892)+ -> K+  ^pi0) (X0 ->  l+  l-)]CC": "ConeIsoInfopi0H2", 
                    "[Beauty -> (K*(892)+ -> K+  pi0) (X0 ->  ^l+  l+)]CC": "ConeIsoInfopi0L1", 
                    "[Beauty -> (K*(892)+ -> K+  pi0) (X0 ->  ^l+  l-)]CC": "ConeIsoInfopi0L1", 
                    "[Beauty -> (K*(892)+ -> K+  pi0) (X0 ->  l+  ^l+)]CC": "ConeIsoInfopi0L2", 
                    "[Beauty -> (K*(892)+ -> K+  pi0) (X0 ->  l+  ^l-)]CC": "ConeIsoInfopi0L2", 
                    "[Beauty -> (K*(892)+ -> ^K+  pi0) (X0 ->  l+  l+)]CC": "ConeIsoInfopi0H1", 
                    "[Beauty -> (K*(892)+ -> ^K+  pi0) (X0 ->  l+  l-)]CC": "ConeIsoInfopi0H1", 
                    "[Beauty -> (X+ ->  X+  (X0 ->  X+  X-)) (X+ ->  l+ ^l+)]CC": "ConeIsoInfoL2", 
                    "[Beauty -> (X+ ->  X+  (X0 ->  X+  X-)) (X+ -> ^l+  l+)]CC": "ConeIsoInfoL1", 
                    "[Beauty -> (X+ ->  X+  (X0 ->  X+  X-)) (X0 ->  l+ ^l-)]CC": "ConeIsoInfoL2", 
                    "[Beauty -> (X+ ->  X+  (X0 ->  X+  X-)) (X0 -> ^l+  l-)]CC": "ConeIsoInfoL1", 
                    "[Beauty -> (X+ ->  X+  (X0 ->  X+ ^X-)) (X+ ->  l+  l+)]CC": "ConeIsoInfoH3", 
                    "[Beauty -> (X+ ->  X+  (X0 ->  X+ ^X-)) (X0 ->  l+  l-)]CC": "ConeIsoInfoH3", 
                    "[Beauty -> (X+ ->  X+  (X0 -> ^X+  X-)) (X+ ->  l+  l+)]CC": "ConeIsoInfoH2", 
                    "[Beauty -> (X+ ->  X+  (X0 -> ^X+  X-)) (X0 ->  l+  l-)]CC": "ConeIsoInfoH2", 
                    "[Beauty -> (X+ ->  X+  X+ ^X-) (X+ ->  l+  l+)]CC": "ConeIsoInfoH3", 
                    "[Beauty -> (X+ ->  X+  X+ ^X-) (X0 ->  l+  l-)]CC": "ConeIsoInfoH3", 
                    "[Beauty -> (X+ ->  X+ ^(X0 ->  X+  X-)) (X+ ->  l+  l+)]CC": "ConeIsoInfoHH", 
                    "[Beauty -> (X+ ->  X+ ^(X0 ->  X+  X-)) (X0 ->  l+  l-)]CC": "ConeIsoInfoHH", 
                    "[Beauty -> (X+ ->  X+ ^X+  X-) (X+ ->  l+  l+)]CC": "ConeIsoInfoH2", 
                    "[Beauty -> (X+ ->  X+ ^X+  X-) (X0 ->  l+  l-)]CC": "ConeIsoInfoH2", 
                    "[Beauty -> (X+ -> ^X+  (X0 ->  X+  X-)) (X+ ->  l+  l+)]CC": "ConeIsoInfoH1", 
                    "[Beauty -> (X+ -> ^X+  (X0 ->  X+  X-)) (X0 ->  l+  l-)]CC": "ConeIsoInfoH1", 
                    "[Beauty -> (X+ -> ^X+  X+  X-) (X+ ->  l+  l+)]CC": "ConeIsoInfoH1", 
                    "[Beauty -> (X+ -> ^X+  X+  X-) (X0 ->  l+  l-)]CC": "ConeIsoInfoH1", 
                    "[Beauty -> ^(X0 ->  Xs  X-)  (X ->  l+  l+)]CC": "ConeIsoInfoHH", 
                    "[Beauty -> ^(X0 ->  Xs  X-)  (X ->  l-  l-)]CC": "ConeIsoInfoHH", 
                    "[Beauty -> ^(X0 ->  Xs  X-)  (X0 ->  l+  l-)]CC": "ConeIsoInfoHH", 
                    "[Beauty -> ^(X0 ->  p+  pi-)  (X ->  l+  l+)]CC": "ConeIsoInfoHH", 
                    "[Beauty -> ^(X0 ->  p+  pi-)  (X ->  l-  l-)]CC": "ConeIsoInfoHH", 
                    "[Beauty -> ^(X0 ->  p+  pi-)  (X0 ->  l+  l-)]CC": "ConeIsoInfoHH", 
                    "[Beauty -> ^(X0 ->  pi+  pi-)  (X+ ->  l+  l+)]CC": "ConeIsoInfoHH", 
                    "[Beauty -> ^(X0 ->  pi+  pi-)  (X0 ->  l+  l-)]CC": "ConeIsoInfoHH", 
                    "[Beauty -> ^X+  (X+ ->  l+  l+)]CC": "ConeIsoInfoH", 
                    "[Beauty -> ^X+  (X0 ->  l+  l-)]CC": "ConeIsoInfoH"
                }, 
                "IgnoreUnmatchedDescriptors": True, 
                "Location": "ConeIsoInfo05", 
                "Type": "RelInfoConeIsolation"
            }, 
            {
                "DaughterLocations": {
                    "[Beauty ->  (X0 ->  X+  X-)  (X+ ->  l+  (l+ ->  X+  X- ^X+))]CC": "TrackIsoBDTInfoL23", 
                    "[Beauty ->  (X0 ->  X+  X-)  (X+ ->  l+  (l+ ->  X+ ^X-  X+))]CC": "TrackIsoBDTInfoL22", 
                    "[Beauty ->  (X0 ->  X+  X-)  (X+ ->  l+  (l+ -> ^X+  X-  X+))]CC": "TrackIsoBDTInfoL21", 
                    "[Beauty ->  (X0 ->  X+  X-)  (X0 ->  l+  (l- ->  X-  X- ^X+))]CC": "TrackIsoBDTInfoL23", 
                    "[Beauty ->  (X0 ->  X+  X-)  (X0 ->  l+  (l- ->  X- ^X-  X+))]CC": "TrackIsoBDTInfoL22", 
                    "[Beauty ->  (X0 ->  X+  X-)  (X0 ->  l+  (l- -> ^X-  X-  X+))]CC": "TrackIsoBDTInfoL21", 
                    "[Beauty ->  (X0 ->  Xs  X-)  (X ->  l+ ^l+)]CC": "TrackIsoBDTInfoL2", 
                    "[Beauty ->  (X0 ->  Xs  X-)  (X ->  l- ^l-)]CC": "TrackIsoBDTInfoL2", 
                    "[Beauty ->  (X0 ->  Xs  X-)  (X -> ^l+  l+)]CC": "TrackIsoBDTInfoL1", 
                    "[Beauty ->  (X0 ->  Xs  X-)  (X -> ^l-  l-)]CC": "TrackIsoBDTInfoL1", 
                    "[Beauty ->  (X0 ->  Xs  X-)  (X0 ->  l+ ^l-)]CC": "TrackIsoBDTInfoL2", 
                    "[Beauty ->  (X0 ->  Xs  X-)  (X0 -> ^l+  l-)]CC": "TrackIsoBDTInfoL1", 
                    "[Beauty ->  (X0 ->  Xs  X-) ^(X ->  l+  l+)]CC": "TrackIsoBDTInfoLL", 
                    "[Beauty ->  (X0 ->  Xs  X-) ^(X ->  l-  l-)]CC": "TrackIsoBDTInfoLL", 
                    "[Beauty ->  (X0 ->  Xs  X-) ^(X0 ->  l+  l-)]CC": "TrackIsoBDTInfoLL", 
                    "[Beauty ->  (X0 ->  Xs ^X-)  (X ->  l+  l+)]CC": "TrackIsoBDTInfoH2", 
                    "[Beauty ->  (X0 ->  Xs ^X-)  (X ->  l-  l-)]CC": "TrackIsoBDTInfoH2", 
                    "[Beauty ->  (X0 ->  Xs ^X-)  (X0 ->  l+  l-)]CC": "TrackIsoBDTInfoH2", 
                    "[Beauty ->  (X0 ->  p+  pi-)  (X ->  l+ ^l+)]CC": "TrackIsoBDTInfoL2", 
                    "[Beauty ->  (X0 ->  p+  pi-)  (X ->  l- ^l-)]CC": "TrackIsoBDTInfoL2", 
                    "[Beauty ->  (X0 ->  p+  pi-)  (X -> ^l+  l+)]CC": "TrackIsoBDTInfoL1", 
                    "[Beauty ->  (X0 ->  p+  pi-)  (X -> ^l-  l-)]CC": "TrackIsoBDTInfoL1", 
                    "[Beauty ->  (X0 ->  p+  pi-)  (X0 ->  l+ ^l-)]CC": "TrackIsoBDTInfoL2", 
                    "[Beauty ->  (X0 ->  p+  pi-)  (X0 -> ^l+  l-)]CC": "TrackIsoBDTInfoL1", 
                    "[Beauty ->  (X0 ->  p+  pi-) ^(X ->  l+  l+)]CC": "TrackIsoBDTInfoLL", 
                    "[Beauty ->  (X0 ->  p+  pi-) ^(X ->  l-  l-)]CC": "TrackIsoBDTInfoLL", 
                    "[Beauty ->  (X0 ->  p+  pi-) ^(X0 ->  l+  l-)]CC": "TrackIsoBDTInfoLL", 
                    "[Beauty ->  (X0 ->  p+ ^pi-)  (X ->  l+  l+)]CC": "TrackIsoBDTInfoH2", 
                    "[Beauty ->  (X0 ->  p+ ^pi-)  (X ->  l-  l-)]CC": "TrackIsoBDTInfoH2", 
                    "[Beauty ->  (X0 ->  p+ ^pi-)  (X0 ->  l+  l-)]CC": "TrackIsoBDTInfoH2", 
                    "[Beauty ->  (X0 ->  pi+  pi-)  (X+ ->  l+ ^l+)]CC": "TrackIsoBDTInfoL2", 
                    "[Beauty ->  (X0 ->  pi+  pi-)  (X+ -> ^l+  l+)]CC": "TrackIsoBDTInfoL1", 
                    "[Beauty ->  (X0 ->  pi+  pi-)  (X0 ->  l+ ^l-)]CC": "TrackIsoBDTInfoL2", 
                    "[Beauty ->  (X0 ->  pi+  pi-)  (X0 -> ^l+  l-)]CC": "TrackIsoBDTInfoL1", 
                    "[Beauty ->  (X0 ->  pi+  pi-) ^(X+ ->  l+  l+)]CC": "TrackIsoBDTInfoLL", 
                    "[Beauty ->  (X0 ->  pi+  pi-) ^(X0 ->  l+  l-)]CC": "TrackIsoBDTInfoLL", 
                    "[Beauty ->  (X0 ->  pi+ ^pi-)  (X+ ->  l+  l+)]CC": "TrackIsoBDTInfoH2", 
                    "[Beauty ->  (X0 ->  pi+ ^pi-)  (X0 ->  l+  l-)]CC": "TrackIsoBDTInfoH2", 
                    "[Beauty ->  (X0 -> ^Xs  X-)  (X ->  l+  l+)]CC": "TrackIsoBDTInfoH1", 
                    "[Beauty ->  (X0 -> ^Xs  X-)  (X ->  l-  l-)]CC": "TrackIsoBDTInfoH1", 
                    "[Beauty ->  (X0 -> ^Xs  X-)  (X0 ->  l+  l-)]CC": "TrackIsoBDTInfoH1", 
                    "[Beauty ->  (X0 -> ^p+  pi-)  (X ->  l+  l+)]CC": "TrackIsoBDTInfoH1", 
                    "[Beauty ->  (X0 -> ^p+  pi-)  (X ->  l-  l-)]CC": "TrackIsoBDTInfoH1", 
                    "[Beauty ->  (X0 -> ^p+  pi-)  (X0 ->  l+  l-)]CC": "TrackIsoBDTInfoH1", 
                    "[Beauty ->  (X0 -> ^pi+  pi-)  (X+ ->  l+  l+)]CC": "TrackIsoBDTInfoH1", 
                    "[Beauty ->  (X0 -> ^pi+  pi-)  (X0 ->  l+  l-)]CC": "TrackIsoBDTInfoH1", 
                    "[Beauty ->  X+  (X+ ->  l+ ^l+)]CC": "TrackIsoBDTInfoL2", 
                    "[Beauty ->  X+  (X+ -> ^l+  l+)]CC": "TrackIsoBDTInfoL1", 
                    "[Beauty ->  X+  (X0 ->  l+ ^l-)]CC": "TrackIsoBDTInfoL2", 
                    "[Beauty ->  X+  (X0 -> ^l+  l-)]CC": "TrackIsoBDTInfoL1", 
                    "[Beauty ->  X+ ^(X+ ->  l+  l+)]CC": "TrackIsoBDTInfoLL", 
                    "[Beauty ->  X+ ^(X0 ->  l+  l-)]CC": "TrackIsoBDTInfoLL", 
                    "[Beauty -> (K*(892)+ -> K+  pi0) (X0 ->  ^l+  l+)]CC": "TrackIsoBDTInfopi0L1", 
                    "[Beauty -> (K*(892)+ -> K+  pi0) (X0 ->  ^l+  l-)]CC": "TrackIsoBDTInfopi0L1", 
                    "[Beauty -> (K*(892)+ -> K+  pi0) (X0 ->  l+  ^l+)]CC": "TrackIsoBDTInfopi0L2", 
                    "[Beauty -> (K*(892)+ -> K+  pi0) (X0 ->  l+  ^l-)]CC": "TrackIsoBDTInfopi0L2", 
                    "[Beauty -> (K*(892)+ -> ^K+  pi0) (X0 ->  l+  l+)]CC": "TrackIsoBDTInfopi0H1", 
                    "[Beauty -> (K*(892)+ -> ^K+  pi0) (X0 ->  l+  l-)]CC": "TrackIsoBDTInfopi0H1", 
                    "[Beauty -> (X+ ->  X+  (X0 ->  X+  X-)) (X+ ->  l+ ^l+)]CC": "TrackIsoBDTInfoL2", 
                    "[Beauty -> (X+ ->  X+  (X0 ->  X+  X-)) (X+ -> ^l+  l+)]CC": "TrackIsoBDTInfoL1", 
                    "[Beauty -> (X+ ->  X+  (X0 ->  X+  X-)) (X0 ->  l+ ^l-)]CC": "TrackIsoBDTInfoL2", 
                    "[Beauty -> (X+ ->  X+  (X0 ->  X+  X-)) (X0 -> ^l+  l-)]CC": "TrackIsoBDTInfoL1", 
                    "[Beauty -> (X+ ->  X+  (X0 ->  X+ ^X-)) (X+ ->  l+  l+)]CC": "TrackIsoBDTInfoH3", 
                    "[Beauty -> (X+ ->  X+  (X0 ->  X+ ^X-)) (X0 ->  l+  l-)]CC": "TrackIsoBDTInfoH3", 
                    "[Beauty -> (X+ ->  X+  (X0 -> ^X+  X-)) (X+ ->  l+  l+)]CC": "TrackIsoBDTInfoH2", 
                    "[Beauty -> (X+ ->  X+  (X0 -> ^X+  X-)) (X0 ->  l+  l-)]CC": "TrackIsoBDTInfoH2", 
                    "[Beauty -> (X+ ->  X+  X+ ^X-) (X+ ->  l+  l+)]CC": "TrackIsoBDTInfoH3", 
                    "[Beauty -> (X+ ->  X+  X+ ^X-) (X0 ->  l+  l-)]CC": "TrackIsoBDTInfoH3", 
                    "[Beauty -> (X+ ->  X+ ^(X0 ->  X+  X-)) (X+ ->  l+  l+)]CC": "TrackIsoBDTInfoHH", 
                    "[Beauty -> (X+ ->  X+ ^(X0 ->  X+  X-)) (X0 ->  l+  l-)]CC": "TrackIsoBDTInfoHH", 
                    "[Beauty -> (X+ ->  X+ ^X+  X-) (X+ ->  l+  l+)]CC": "TrackIsoBDTInfoH2", 
                    "[Beauty -> (X+ ->  X+ ^X+  X-) (X0 ->  l+  l-)]CC": "TrackIsoBDTInfoH2", 
                    "[Beauty -> (X+ -> ^X+  (X0 ->  X+  X-)) (X+ ->  l+  l+)]CC": "TrackIsoBDTInfoH1", 
                    "[Beauty -> (X+ -> ^X+  (X0 ->  X+  X-)) (X0 ->  l+  l-)]CC": "TrackIsoBDTInfoH1", 
                    "[Beauty -> (X+ -> ^X+  X+  X-) (X+ ->  l+  l+)]CC": "TrackIsoBDTInfoH1", 
                    "[Beauty -> (X+ -> ^X+  X+  X-) (X0 ->  l+  l-)]CC": "TrackIsoBDTInfoH1", 
                    "[Beauty -> ^(X0 ->  Xs  X-)  (X ->  l+  l+)]CC": "TrackIsoBDTInfoHH", 
                    "[Beauty -> ^(X0 ->  Xs  X-)  (X ->  l-  l-)]CC": "TrackIsoBDTInfoHH", 
                    "[Beauty -> ^(X0 ->  Xs  X-)  (X0 ->  l+  l-)]CC": "TrackIsoBDTInfoHH", 
                    "[Beauty -> ^(X0 ->  p+  pi-)  (X ->  l+  l+)]CC": "TrackIsoBDTInfoHH", 
                    "[Beauty -> ^(X0 ->  p+  pi-)  (X ->  l-  l-)]CC": "TrackIsoBDTInfoHH", 
                    "[Beauty -> ^(X0 ->  p+  pi-)  (X0 ->  l+  l-)]CC": "TrackIsoBDTInfoHH", 
                    "[Beauty -> ^(X0 ->  pi+  pi-)  (X+ ->  l+  l+)]CC": "TrackIsoBDTInfoHH", 
                    "[Beauty -> ^(X0 ->  pi+  pi-)  (X0 ->  l+  l-)]CC": "TrackIsoBDTInfoHH", 
                    "[Beauty -> ^X+  (X+ ->  l+  l+)]CC": "TrackIsoBDTInfoH", 
                    "[Beauty -> ^X+  (X0 ->  l+  l-)]CC": "TrackIsoBDTInfoH"
                }, 
                "IgnoreUnmatchedDescriptors": True, 
                "Location": "TrackIsoBDTInfo", 
                "Type": "RelInfoTrackIsolationBDT", 
                "Variables": 2, 
                "WeightsFile": "BsMuMu_TrackIsolationBDT9vars_v1r4.xml"
            }, 
            {
                "DaughterLocations": {
                    "[Beauty ->  (X0 ->  X+  X-)  (X+ ->  l+  (l+ ->  X+  X- ^X+))]CC": "TrackIsoBs2MMInfoL23", 
                    "[Beauty ->  (X0 ->  X+  X-)  (X+ ->  l+  (l+ ->  X+ ^X-  X+))]CC": "TrackIsoBs2MMInfoL22", 
                    "[Beauty ->  (X0 ->  X+  X-)  (X+ ->  l+  (l+ -> ^X+  X-  X+))]CC": "TrackIsoBs2MMInfoL21", 
                    "[Beauty ->  (X0 ->  X+  X-)  (X0 ->  l+  (l- ->  X-  X- ^X+))]CC": "TrackIsoBs2MMInfoL23", 
                    "[Beauty ->  (X0 ->  X+  X-)  (X0 ->  l+  (l- ->  X- ^X-  X+))]CC": "TrackIsoBs2MMInfoL22", 
                    "[Beauty ->  (X0 ->  X+  X-)  (X0 ->  l+  (l- -> ^X-  X-  X+))]CC": "TrackIsoBs2MMInfoL21", 
                    "[Beauty ->  (X0 ->  Xs  X-)  (X ->  l+ ^l+)]CC": "TrackIsoBs2MMInfoL2", 
                    "[Beauty ->  (X0 ->  Xs  X-)  (X ->  l- ^l-)]CC": "TrackIsoBs2MMInfoL2", 
                    "[Beauty ->  (X0 ->  Xs  X-)  (X -> ^l+  l+)]CC": "TrackIsoBs2MMInfoL1", 
                    "[Beauty ->  (X0 ->  Xs  X-)  (X -> ^l-  l-)]CC": "TrackIsoBs2MMInfoL1", 
                    "[Beauty ->  (X0 ->  Xs  X-)  (X0 ->  l+ ^l-)]CC": "TrackIsoBs2MMInfoL2", 
                    "[Beauty ->  (X0 ->  Xs  X-)  (X0 -> ^l+  l-)]CC": "TrackIsoBs2MMInfoL1", 
                    "[Beauty ->  (X0 ->  Xs  X-) ^(X ->  l+  l+)]CC": "TrackIsoBs2MMInfoLL", 
                    "[Beauty ->  (X0 ->  Xs  X-) ^(X ->  l-  l-)]CC": "TrackIsoBs2MMInfoLL", 
                    "[Beauty ->  (X0 ->  Xs  X-) ^(X0 ->  l+  l-)]CC": "TrackIsoBs2MMInfoLL", 
                    "[Beauty ->  (X0 ->  Xs ^X-)  (X ->  l+  l+)]CC": "TrackIsoBs2MMInfoH2", 
                    "[Beauty ->  (X0 ->  Xs ^X-)  (X ->  l-  l-)]CC": "TrackIsoBs2MMInfoH2", 
                    "[Beauty ->  (X0 ->  Xs ^X-)  (X0 ->  l+  l-)]CC": "TrackIsoBs2MMInfoH2", 
                    "[Beauty ->  (X0 ->  p+  pi-)  (X ->  l+ ^l+)]CC": "TrackIsoBs2MMInfoL2", 
                    "[Beauty ->  (X0 ->  p+  pi-)  (X ->  l- ^l-)]CC": "TrackIsoBs2MMInfoL2", 
                    "[Beauty ->  (X0 ->  p+  pi-)  (X -> ^l+  l+)]CC": "TrackIsoBs2MMInfoL1", 
                    "[Beauty ->  (X0 ->  p+  pi-)  (X -> ^l-  l-)]CC": "TrackIsoBs2MMInfoL1", 
                    "[Beauty ->  (X0 ->  p+  pi-)  (X0 ->  l+ ^l-)]CC": "TrackIsoBs2MMInfoL2", 
                    "[Beauty ->  (X0 ->  p+  pi-)  (X0 -> ^l+  l-)]CC": "TrackIsoBs2MMInfoL1", 
                    "[Beauty ->  (X0 ->  p+  pi-) ^(X ->  l+  l+)]CC": "TrackIsoBs2MMInfoLL", 
                    "[Beauty ->  (X0 ->  p+  pi-) ^(X ->  l-  l-)]CC": "TrackIsoBs2MMInfoLL", 
                    "[Beauty ->  (X0 ->  p+  pi-) ^(X0 ->  l+  l-)]CC": "TrackIsoBs2MMInfoLL", 
                    "[Beauty ->  (X0 ->  p+ ^pi-)  (X ->  l+  l+)]CC": "TrackIsoBs2MMInfoH2", 
                    "[Beauty ->  (X0 ->  p+ ^pi-)  (X ->  l-  l-)]CC": "TrackIsoBs2MMInfoH2", 
                    "[Beauty ->  (X0 ->  p+ ^pi-)  (X0 ->  l+  l-)]CC": "TrackIsoBs2MMInfoH2", 
                    "[Beauty ->  (X0 ->  pi+  pi-)  (X+ ->  l+ ^l+)]CC": "TrackIsoBs2MMInfoL2", 
                    "[Beauty ->  (X0 ->  pi+  pi-)  (X+ -> ^l+  l+)]CC": "TrackIsoBs2MMInfoL1", 
                    "[Beauty ->  (X0 ->  pi+  pi-)  (X0 ->  l+ ^l-)]CC": "TrackIsoBs2MMInfoL2", 
                    "[Beauty ->  (X0 ->  pi+  pi-)  (X0 -> ^l+  l-)]CC": "TrackIsoBs2MMInfoL1", 
                    "[Beauty ->  (X0 ->  pi+  pi-) ^(X+ ->  l+  l+)]CC": "TrackIsoBs2MMInfoLL", 
                    "[Beauty ->  (X0 ->  pi+  pi-) ^(X0 ->  l+  l-)]CC": "TrackIsoBs2MMInfoLL", 
                    "[Beauty ->  (X0 ->  pi+ ^pi-)  (X+ ->  l+  l+)]CC": "TrackIsoBs2MMInfoH2", 
                    "[Beauty ->  (X0 ->  pi+ ^pi-)  (X0 ->  l+  l-)]CC": "TrackIsoBs2MMInfoH2", 
                    "[Beauty ->  (X0 -> ^Xs  X-)  (X ->  l+  l+)]CC": "TrackIsoBs2MMInfoH1", 
                    "[Beauty ->  (X0 -> ^Xs  X-)  (X ->  l-  l-)]CC": "TrackIsoBs2MMInfoH1", 
                    "[Beauty ->  (X0 -> ^Xs  X-)  (X0 ->  l+  l-)]CC": "TrackIsoBs2MMInfoH1", 
                    "[Beauty ->  (X0 -> ^p+  pi-)  (X ->  l+  l+)]CC": "TrackIsoBs2MMInfoH1", 
                    "[Beauty ->  (X0 -> ^p+  pi-)  (X ->  l-  l-)]CC": "TrackIsoBs2MMInfoH1", 
                    "[Beauty ->  (X0 -> ^p+  pi-)  (X0 ->  l+  l-)]CC": "TrackIsoBs2MMInfoH1", 
                    "[Beauty ->  (X0 -> ^pi+  pi-)  (X+ ->  l+  l+)]CC": "TrackIsoBs2MMInfoH1", 
                    "[Beauty ->  (X0 -> ^pi+  pi-)  (X0 ->  l+  l-)]CC": "TrackIsoBs2MMInfoH1", 
                    "[Beauty ->  X+  (X+ ->  l+ ^l+)]CC": "TrackIsoBs2MMInfoL2", 
                    "[Beauty ->  X+  (X+ -> ^l+  l+)]CC": "TrackIsoBs2MMInfoL1", 
                    "[Beauty ->  X+  (X0 ->  l+ ^l-)]CC": "TrackIsoBs2MMInfoL2", 
                    "[Beauty ->  X+  (X0 -> ^l+  l-)]CC": "TrackIsoBs2MMInfoL1", 
                    "[Beauty ->  X+ ^(X+ ->  l+  l+)]CC": "TrackIsoBs2MMInfoLL", 
                    "[Beauty ->  X+ ^(X0 ->  l+  l-)]CC": "TrackIsoBs2MMInfoLL", 
                    "[Beauty -> (K*(892)+ -> K+  pi0) (X0 ->  ^l+  l+)]CC": "TrackIsoBs2MMInfopi0L1", 
                    "[Beauty -> (K*(892)+ -> K+  pi0) (X0 ->  ^l+  l-)]CC": "TrackIsoBs2MMInfopi0L1", 
                    "[Beauty -> (K*(892)+ -> K+  pi0) (X0 ->  l+  ^l+)]CC": "TrackIsoBs2MMInfopi0L2", 
                    "[Beauty -> (K*(892)+ -> K+  pi0) (X0 ->  l+  ^l-)]CC": "TrackIsoBs2MMInfopi0L2", 
                    "[Beauty -> (K*(892)+ -> ^K+  pi0) (X0 ->  l+  l+)]CC": "TrackIsoBs2MMInfopi0H1", 
                    "[Beauty -> (K*(892)+ -> ^K+  pi0) (X0 ->  l+  l-)]CC": "TrackIsoBs2MMInfopi0H1", 
                    "[Beauty -> (X+ ->  X+  (X0 ->  X+  X-)) (X+ ->  l+ ^l+)]CC": "TrackIsoBs2MMInfoL2", 
                    "[Beauty -> (X+ ->  X+  (X0 ->  X+  X-)) (X+ -> ^l+  l+)]CC": "TrackIsoBs2MMInfoL1", 
                    "[Beauty -> (X+ ->  X+  (X0 ->  X+  X-)) (X0 ->  l+ ^l-)]CC": "TrackIsoBs2MMInfoL2", 
                    "[Beauty -> (X+ ->  X+  (X0 ->  X+  X-)) (X0 -> ^l+  l-)]CC": "TrackIsoBs2MMInfoL1", 
                    "[Beauty -> (X+ ->  X+  (X0 ->  X+ ^X-)) (X+ ->  l+  l+)]CC": "TrackIsoBs2MMInfoH3", 
                    "[Beauty -> (X+ ->  X+  (X0 ->  X+ ^X-)) (X0 ->  l+  l-)]CC": "TrackIsoBs2MMInfoH3", 
                    "[Beauty -> (X+ ->  X+  (X0 -> ^X+  X-)) (X+ ->  l+  l+)]CC": "TrackIsoBs2MMInfoH2", 
                    "[Beauty -> (X+ ->  X+  (X0 -> ^X+  X-)) (X0 ->  l+  l-)]CC": "TrackIsoBs2MMInfoH2", 
                    "[Beauty -> (X+ ->  X+  X+ ^X-) (X+ ->  l+  l+)]CC": "TrackIsoBs2MMInfoH3", 
                    "[Beauty -> (X+ ->  X+  X+ ^X-) (X0 ->  l+  l-)]CC": "TrackIsoBs2MMInfoH3", 
                    "[Beauty -> (X+ ->  X+ ^(X0 ->  X+  X-)) (X+ ->  l+  l+)]CC": "TrackIsoBs2MMInfoHH", 
                    "[Beauty -> (X+ ->  X+ ^(X0 ->  X+  X-)) (X0 ->  l+  l-)]CC": "TrackIsoBs2MMInfoHH", 
                    "[Beauty -> (X+ ->  X+ ^X+  X-) (X+ ->  l+  l+)]CC": "TrackIsoBs2MMInfoH2", 
                    "[Beauty -> (X+ ->  X+ ^X+  X-) (X0 ->  l+  l-)]CC": "TrackIsoBs2MMInfoH2", 
                    "[Beauty -> (X+ -> ^X+  (X0 ->  X+  X-)) (X+ ->  l+  l+)]CC": "TrackIsoBs2MMInfoH1", 
                    "[Beauty -> (X+ -> ^X+  (X0 ->  X+  X-)) (X0 ->  l+  l-)]CC": "TrackIsoBs2MMInfoH1", 
                    "[Beauty -> (X+ -> ^X+  X+  X-) (X+ ->  l+  l+)]CC": "TrackIsoBs2MMInfoH1", 
                    "[Beauty -> (X+ -> ^X+  X+  X-) (X0 ->  l+  l-)]CC": "TrackIsoBs2MMInfoH1", 
                    "[Beauty -> ^(X0 ->  Xs  X-)  (X ->  l+  l+)]CC": "TrackIsoBs2MMInfoHH", 
                    "[Beauty -> ^(X0 ->  Xs  X-)  (X ->  l-  l-)]CC": "TrackIsoBs2MMInfoHH", 
                    "[Beauty -> ^(X0 ->  Xs  X-)  (X0 ->  l+  l-)]CC": "TrackIsoBs2MMInfoHH", 
                    "[Beauty -> ^(X0 ->  p+  pi-)  (X ->  l+  l+)]CC": "TrackIsoBs2MMInfoHH", 
                    "[Beauty -> ^(X0 ->  p+  pi-)  (X ->  l-  l-)]CC": "TrackIsoBs2MMInfoHH", 
                    "[Beauty -> ^(X0 ->  p+  pi-)  (X0 ->  l+  l-)]CC": "TrackIsoBs2MMInfoHH", 
                    "[Beauty -> ^(X0 ->  pi+  pi-)  (X+ ->  l+  l+)]CC": "TrackIsoBs2MMInfoHH", 
                    "[Beauty -> ^(X0 ->  pi+  pi-)  (X0 ->  l+  l-)]CC": "TrackIsoBs2MMInfoHH", 
                    "[Beauty -> ^X+  (X+ ->  l+  l+)]CC": "TrackIsoBs2MMInfoH", 
                    "[Beauty -> ^X+  (X0 ->  l+  l-)]CC": "TrackIsoBs2MMInfoH"
                }, 
                "IgnoreUnmatchedDescriptors": True, 
                "Location": "TrackIsoBs2MMInfo", 
                "Type": "RelInfoBs2MuMuTrackIsolations"
            }, 
            {
                "ConeAngle": 1.0, 
                "Location": "TrackIsoInfo10", 
                "Type": "RelInfoConeVariables"
            }, 
            {
                "ConeAngle": 1.5, 
                "Location": "TrackIsoInfo15", 
                "Type": "RelInfoConeVariables"
            }, 
            {
                "ConeAngle": 2.0, 
                "Location": "TrackIsoInfo20", 
                "Type": "RelInfoConeVariables"
            }, 
            {
                "ConeSize": 1.0, 
                "Location": "ConeIsoInfo10", 
                "Type": "RelInfoConeIsolation"
            }, 
            {
                "ConeSize": 1.5, 
                "Location": "ConeIsoInfo15", 
                "Type": "RelInfoConeIsolation"
            }, 
            {
                "ConeSize": 2.0, 
                "Location": "ConeIsoInfo20", 
                "Type": "RelInfoConeIsolation"
            }
        ], 
        "TauPT": 0, 
        "TauVCHI2DOF": 150, 
        "Trk_Chi2": 3, 
        "Trk_GhostProb": 0.35, 
        "UpperBMass": 5280, 
        "UpperBsMass": 5367, 
        "UpperLbMass": 5620, 
        "UpperMass": 5500
    }, 
    "STREAMS": [ "Leptonic" ], 
    "WGs": [ "RD" ]
}

DarkBoson = {
    "BUILDERTYPE": "DarkBosonConf", 
    "CONFIG": {
        "B2HHX": {
            "AM_MAX": "5800*MeV", 
            "AM_MIN": "4800*MeV", 
            "BPVIPCHI2_MAX": 10, 
            "BPVLTIME_MIN": "0.2*ps", 
            "HAD_MINIPCHI2_MIN": 9, 
            "PT_MIN": "1000*MeV", 
            "SUMPT_MIN": "0*MeV", 
            "VCHI2DOF_MAX": 15
        }, 
        "B2HHXTight": {
            "AM_MAX": "5800*MeV", 
            "AM_MIN": "4800*MeV", 
            "BPVIPCHI2_MAX": 10, 
            "BPVLTIME_MIN": "0.2*ps", 
            "HAD_MINIPCHI2_MIN": 9, 
            "PT_MIN": "3000*MeV", 
            "SUMPT_MIN": "0*MeV", 
            "VCHI2DOF_MAX": 15
        }, 
        "B2KX": {
            "AM_MAX": "5800*MeV", 
            "AM_MIN": "4800*MeV", 
            "BPVIPCHI2_MAX": 10, 
            "BPVLTIME_MIN": "0.2*ps", 
            "HAD_MINIPCHI2_MIN": 25, 
            "PT_MIN": "1000*MeV", 
            "SUMPT_MIN": "0*MeV", 
            "VCHI2DOF_MAX": 15
        }, 
        "B2KXTight": {
            "AM_MAX": "5800*MeV", 
            "AM_MIN": "4800*MeV", 
            "BPVIPCHI2_MAX": 10, 
            "BPVLTIME_MIN": "0.2*ps", 
            "HAD_MINIPCHI2_MIN": 25, 
            "PT_MIN": "3000*MeV", 
            "SUMPT_MIN": "0*MeV", 
            "VCHI2DOF_MAX": 15
        }, 
        "B2KstX_X2neutrals": {
            "AM_MAX": "5800*MeV", 
            "AM_MIN": "4800*MeV", 
            "BPVIPCHI2_MAX": 10, 
            "BPVLTIME_MIN": "0.2*ps", 
            "PT_MIN": "3000*MeV", 
            "SUMPT_MIN": "0*MeV"
        }, 
        "CommonRelInfoTools": [
            {
                "Location": "VtxIsoInfo", 
                "Type": "RelInfoVertexIsolation"
            }, 
            {
                "Location": "VtxIsoInfoBDT", 
                "Type": "RelInfoVertexIsolationBDT"
            }, 
            {
                "Location": "BsMuMuBIsolation", 
                "Type": "RelInfoBs2MuMuBIsolations", 
                "Variables": [], 
                "makeTrackCuts": False, 
                "tracktype": 3
            }
        ], 
        "E": {
            "MIPCHI2DV_MIN": 9, 
            "PIDe_MIN": 0, 
            "PT_MIN": "100*MeV", 
            "TRCHI2DOF_MAX": 5, 
            "TRGHP_MAX": 0.4
        }, 
        "GECNTrkMax": 250, 
        "J": {
            "ADAMASS_MAX": "100*MeV", 
            "VCHI2DOF_MAX": 12
        }, 
        "KB": {
            "MIPCHI2DV_MIN": 9, 
            "PROBNNK_MIN": 0.1, 
            "PT_MIN": "250*MeV", 
            "P_MIN": "2000*MeV", 
            "TRCHI2DOF_MAX": 3, 
            "TRGHP_MAX": 0.3
        }, 
        "KBhard": {
            "MIPCHI2DV_MIN": 36, 
            "PROBNNK_MIN": 0.2, 
            "PT_MIN": "250*MeV", 
            "P_MIN": "3000*MeV", 
            "TRCHI2DOF_MAX": 3, 
            "TRGHP_MAX": 0.3
        }, 
        "KDX": {
            "MIPCHI2DV_MIN": 25, 
            "PROBNNK_MIN": 0.1, 
            "PT_MIN": "125*MeV", 
            "P_MIN": "0*MeV", 
            "TRCHI2DOF_MAX": 4, 
            "TRGHP_MAX": 0.3
        }, 
        "KX": {
            "MIPCHI2DV_MIN": 25, 
            "PROBNNK_MIN": 0.1, 
            "PT_MIN": "250*MeV", 
            "P_MIN": "3000*MeV", 
            "TRCHI2DOF_MAX": 3, 
            "TRGHP_MAX": 0.3
        }, 
        "MuJ": {
            "MIPCHI2DV_MIN": 25, 
            "PIDmu_MIN": -4, 
            "PT_MIN": "125*MeV", 
            "P_MIN": "0*MeV", 
            "TRCHI2DOF_MAX": 4, 
            "TRGHP_MAX": 0.3
        }, 
        "MuX": {
            "MIPCHI2DV_MIN": 9, 
            "PIDmu_MIN": -5, 
            "PT_MIN": "100*MeV", 
            "P_MIN": "0*MeV", 
            "TRCHI2DOF_MAX": 3, 
            "TRGHP_MAX": 0.3
        }, 
        "PiB": {
            "MIPCHI2DV_MIN": 9, 
            "PROBNNpi_MIN": 0.2, 
            "PT_MIN": "250*MeV", 
            "P_MIN": "2000*MeV", 
            "TRCHI2DOF_MAX": 3, 
            "TRGHP_MAX": 0.3
        }, 
        "PiDX": {
            "MIPCHI2DV_MIN": 25, 
            "PROBNNpi_MIN": 0.1, 
            "PT_MIN": "125*MeV", 
            "P_MIN": "0*MeV", 
            "TRCHI2DOF_MAX": 4, 
            "TRGHP_MAX": 0.3
        }, 
        "PiX": {
            "MIPCHI2DV_MIN": 36, 
            "PROBNNpi_MIN": 0.2, 
            "PT_MIN": "250*MeV", 
            "P_MIN": "3000*MeV", 
            "TRCHI2DOF_MAX": 3, 
            "TRGHP_MAX": 0.3
        }, 
        "Prescales": {
            "DD": 1.0, 
            "KK": 0.25, 
            "SS": 0.1
        }, 
        "X3H": {
            "BPVVDCHI2_MIN": 25, 
            "HAD_MINIPCHI2_MIN": 16, 
            "PT_MIN": "500*MeV", 
            "SUMPT_MIN": "0*MeV", 
            "VCHI2DOF_MAX": 10
        }, 
        "XDD": {
            "BPVVDCHI2_MIN": 25, 
            "PT_MIN": "0*MeV", 
            "VCHI2DOF_MAX": 15
        }, 
        "XETAHH": {
            "BPVVDCHI2_MIN": 25, 
            "HAD_MINIPCHI2_MIN": 16, 
            "PT_MIN": "2000*MeV", 
            "SUMPT_MIN": "0*MeV", 
            "VCHI2DOF_MAX": 10
        }, 
        "XGG": {
            "PT_MIN": "2000*MeV"
        }, 
        "XLL": {
            "BPVVDCHI2_MIN": 25, 
            "PT_MIN": "250*MeV", 
            "VCHI2DOF_MAX": 10
        }, 
        "XLLhard": {
            "BPVVDCHI2_MIN": 25, 
            "PT_MIN": "250*MeV", 
            "VCHI2DOF_MAX": 5
        }, 
        "etaX": {
            "CL_MIN": "0.2", 
            "MM_MAX": "650*MeV", 
            "MM_MIN": "450*MeV", 
            "PT_MIN": "500*MeV", 
            "P_MIN": "2000*MeV"
        }, 
        "gX": {
            "CL_MIN": "0.3", 
            "PT_MIN": "500*MeV", 
            "P_MIN": "1000*MeV"
        }, 
        "pizMX": {
            "CL_MIN": "0.1", 
            "PT_MIN": "500*MeV", 
            "P_MIN": "3000*MeV"
        }, 
        "pizRX": {
            "CL_MIN": "0.1", 
            "PT_MIN": "500*MeV"
        }
    }, 
    "STREAMS": {
        "Bhadron": [
            "StrippingB2KX2KKPiDarkBosonLine", 
            "StrippingB2KpiX2KKPiDarkBosonLine", 
            "StrippingB2KX2KKPiMDarkBosonLine", 
            "StrippingB2KpiX2KKPiMDarkBosonLine", 
            "StrippingB2KX2PiPiPiDarkBosonLine", 
            "StrippingB2KpiX2PiPiPiDarkBosonLine", 
            "StrippingB2KX2PiPiPiMDarkBosonLine", 
            "StrippingB2KpiX2PiPiPiMDarkBosonLine", 
            "StrippingB2KX2EtaPiPi23PIDarkBosonLine", 
            "StrippingB2KpiX2EtaPiPi23PIDarkBosonLine", 
            "StrippingB2KX2EtaPiPi2GGDarkBosonLine", 
            "StrippingB2KpiX2EtaPiPi2GGDarkBosonLine", 
            "StrippingB2KX24PiDarkBosonLine", 
            "StrippingB2KpiX24PiDarkBosonLine", 
            "StrippingB2KX26PiDarkBosonLine", 
            "StrippingB2KpiX26PiDarkBosonLine", 
            "StrippingB2KX22K2PiDarkBosonLine", 
            "StrippingB2KpiX22K2PiDarkBosonLine", 
            "StrippingB2KX24KDarkBosonLine", 
            "StrippingB2KpiX24KDarkBosonLine"
        ], 
        "Dimuon": [
            "StrippingB2KpiX2MuMuDDDarkBosonLine", 
            "StrippingB2KpiX2MuMuDDSSDarkBosonLine", 
            "StrippingB2KX2MuMuDDDarkBosonLine", 
            "StrippingB2KX2MuMuDDSSDarkBosonLine"
        ], 
        "Leptonic": [
            "StrippingB2KpiX2MuMuDarkBosonLine", 
            "StrippingB2KpiX2MuMuSSDarkBosonLine", 
            "StrippingB2KX2MuMuDarkBosonLine", 
            "StrippingB2KX2MuMuSSDarkBosonLine", 
            "StrippingB2KpiX2PiPiDarkBosonLine", 
            "StrippingB2KX2PiPiDarkBosonLine", 
            "StrippingB2KX2PiPiSSDarkBosonLine", 
            "StrippingB2KpiX2KKDarkBosonLine", 
            "StrippingB2KX2KKDarkBosonLine", 
            "StrippingB2KX2KKSSDarkBosonLine", 
            "StrippingB2KpiX2EEDarkBosonLine", 
            "StrippingB2KpiX2EESSDarkBosonLine", 
            "StrippingB2KX2EEDarkBosonLine", 
            "StrippingB2KX2EESSDarkBosonLine", 
            "StrippingB2JKDarkBosonLine", 
            "StrippingB2JKstDarkBosonLine", 
            "StrippingB2KstX2GammaGammaDarkBosonLine", 
            "StrippingB2KstX2PiGammaDarkBosonLine", 
            "StrippingB2KstX2PiGammaMDarkBosonLine"
        ]
    }, 
    "WGs": [ "RD" ]
}

Hypb2L0HGamma = {
    "BUILDERTYPE": "Hypb2L0HGammaConf", 
    "CONFIG": {
        "CombMassWinOmega": 120.0, 
        "CombMassWinOmegab": 1000.0, 
        "CombMassWinXi": 60.0, 
        "CombMassWinXib": 800.0, 
        "Ghost_Prob": 0.4, 
        "L0_PT_Min": 850, 
        "L0_VCHI2": 9, 
        "MTDOCACHI2_MAX": 15.0, 
        "MassWinOmega": 70.0, 
        "MassWinXi": 30.0, 
        "MinPO": 10000.0, 
        "MinPOb": 15000.0, 
        "MinPTO": 1000.0, 
        "MinPTOb": 1000.0, 
        "MinPTXi": 1120.0, 
        "MinPTXib": 500.0, 
        "MinPXi": 10000.0, 
        "MinPXib": 15000.0, 
        "Photon_CL_Min": 0.15, 
        "Photon_PT_Min": 2500.0, 
        "Prescale": 1, 
        "TISTOSLinesDict": {
            "L0Electron.*Decision%TOS": 0, 
            "L0Photon.*Decision%TOS": 0
        }, 
        "TRACK_IPCHI2_MIN": 16.0, 
        "TRCHI2DOF_MAX": 4.0, 
        "Xi_VCHI2": 8, 
        "p_PT_Min": 630, 
        "pi_L0_PT_Min": 130, 
        "pi_Xi_PT_Min": 120
    }, 
    "STREAMS": [ "Leptonic" ], 
    "WGs": [ "RD" ]
}

Kshort2Leptons = {
    "BUILDERTYPE": "Kshort2LeptonsConf", 
    "CONFIG": {
        "2mu2e": {
            "LL": {
                "KsIP": 1, 
                "KsLifetime": 0.8953, 
                "KsMAXDOCA": 1.0, 
                "KsVtxChi2": 50, 
                "MaxKsMass": 800.0, 
                "Postscale": 1, 
                "Prescale": 1
            }, 
            "LU": {
                "KsDisChi2": 1500, 
                "KsIP": 5, 
                "KsMAXDOCA": 1.0, 
                "KsVtxChi2": 35, 
                "MaxKsMass": 800.0, 
                "Postscale": 1, 
                "Prescale": 1
            }, 
            "LV": {
                "KsDisChi2": 1500, 
                "KsIP": 10, 
                "KsMAXDOCA": 0.5, 
                "KsVtxChi2": 40, 
                "MaxKsMass": 900.0, 
                "Postscale": 1, 
                "Prescale": 1
            }, 
            "UU": {
                "KsDisChi2": 1500, 
                "KsIP": 1, 
                "KsMAXDOCA": 1.0, 
                "KsVtxChi2": 40, 
                "MaxKsMass": 800.0, 
                "Postscale": 1, 
                "Prescale": 1
            }, 
            "UV": {
                "KsDisChi2": 1000, 
                "KsIP": 15, 
                "KsMAXDOCA": 0.8, 
                "KsVtxChi2": 25, 
                "MaxKsMass": 878.0, 
                "Postscale": 1, 
                "Prescale": 1
            }, 
            "VV": {
                "KsDisChi2": 1500, 
                "KsIP": 8, 
                "KsMAXDOCA": 1.0, 
                "KsVtxChi2": 40, 
                "MaxKsMass": 900.0, 
                "Postscale": 1, 
                "Prescale": 1
            }
        }, 
        "2mu2pi": {
            "LL": {
                "KsDisChi2": 1500, 
                "KsIP": 5.0, 
                "KsMAXDOCA": 3.0, 
                "KsVtxChi2": 100, 
                "MaxKsMass": 800.0, 
                "Postscale": 1, 
                "Prescale": 1
            }, 
            "LU": {
                "KsDisChi2": 2500, 
                "KsIP": 5, 
                "KsMAXDOCA": 1.0, 
                "KsVtxChi2": 25, 
                "MaxKsMass": 800.0, 
                "Postscale": 1, 
                "Prescale": 1
            }, 
            "LV": {
                "KsDisChi2": 2500, 
                "KsIP": 10, 
                "KsMAXDOCA": 0.5, 
                "KsVtxChi2": 37, 
                "MaxKsMass": 900.0, 
                "Postscale": 1, 
                "Prescale": 1
            }, 
            "UU": {
                "KsDisChi2": 2500, 
                "KsIP": 1, 
                "KsMAXDOCA": 1.0, 
                "KsVtxChi2": 35, 
                "MaxKsMass": 800.0, 
                "Postscale": 1, 
                "Prescale": 1
            }, 
            "UV": {
                "KsDisChi2": 2000, 
                "KsIP": 15, 
                "KsMAXDOCA": 0.8, 
                "KsVtxChi2": 19, 
                "MaxKsMass": 878.0, 
                "Postscale": 1, 
                "Prescale": 1
            }, 
            "VV": {
                "KsDisChi2": 2500, 
                "KsIP": 8, 
                "KsMAXDOCA": 1.0, 
                "KsVtxChi2": 40, 
                "MaxKsMass": 900.0, 
                "Postscale": 1, 
                "Prescale": 1
            }
        }, 
        "2pi2e": {
            "LL": {
                "KsIP": 1, 
                "KsLifetime": 0.8953, 
                "KsMAXDOCA": 1.0, 
                "KsVtxChi2": 50, 
                "MaxKsMass": 800.0, 
                "Postscale": 1, 
                "Prescale": 1
            }, 
            "LU": {
                "KsDisChi2": 2500, 
                "KsIP": 5, 
                "KsMAXDOCA": 1.0, 
                "KsVtxChi2": 25, 
                "MaxKsMass": 800.0, 
                "Postscale": 1, 
                "Prescale": 1
            }, 
            "LV": {
                "KsDisChi2": 2500, 
                "KsIP": 10, 
                "KsMAXDOCA": 0.5, 
                "KsVtxChi2": 37, 
                "MaxKsMass": 900.0, 
                "Postscale": 1, 
                "Prescale": 1
            }, 
            "UU": {
                "KsDisChi2": 2500, 
                "KsIP": 1, 
                "KsMAXDOCA": 1.0, 
                "KsVtxChi2": 35, 
                "MaxKsMass": 800.0, 
                "Postscale": 1, 
                "Prescale": 1
            }, 
            "UV": {
                "KsDisChi2": 2000, 
                "KsIP": 15, 
                "KsMAXDOCA": 0.8, 
                "KsVtxChi2": 19, 
                "MaxKsMass": 878.0, 
                "Postscale": 1, 
                "Prescale": 1
            }, 
            "VV": {
                "KsDisChi2": 2500, 
                "KsIP": 8, 
                "KsMAXDOCA": 1.0, 
                "KsVtxChi2": 40, 
                "MaxKsMass": 900.0, 
                "Postscale": 1, 
                "Prescale": 1
            }
        }, 
        "3emu": {
            "LL": {
                "KsDisChi2": 2500, 
                "KsIP": 1, 
                "KsMAXDOCA": 1.0, 
                "KsVtxChi2": 50, 
                "MaxKsMass": 800.0, 
                "Postscale": 1, 
                "Prescale": 1
            }, 
            "LU": {
                "KsDisChi2": 2500, 
                "KsIP": 1, 
                "KsMAXDOCA": 1.0, 
                "KsVtxChi2": 35, 
                "MaxKsMass": 800.0, 
                "Postscale": 1, 
                "Prescale": 1
            }, 
            "LV": {
                "KsDisChi2": 2500, 
                "KsIP": 1, 
                "KsMAXDOCA": 1.0, 
                "KsVtxChi2": 35, 
                "MaxKsMass": 800.0, 
                "Postscale": 1, 
                "Prescale": 1
            }, 
            "UU": {
                "KsDisChi2": 2500, 
                "KsIP": 1, 
                "KsMAXDOCA": 1.0, 
                "KsVtxChi2": 35, 
                "MaxKsMass": 800.0, 
                "Postscale": 1, 
                "Prescale": 1
            }, 
            "UV": {
                "KsDisChi2": 2500, 
                "KsIP": 1, 
                "KsMAXDOCA": 1.0, 
                "KsVtxChi2": 35, 
                "MaxKsMass": 800.0, 
                "Postscale": 1, 
                "Prescale": 1
            }, 
            "VV": {
                "KsDisChi2": 2500, 
                "KsIP": 1, 
                "KsMAXDOCA": 1.0, 
                "KsVtxChi2": 35, 
                "MaxKsMass": 800.0, 
                "Postscale": 1, 
                "Prescale": 1
            }
        }, 
        "3mue": {
            "LL": {
                "KsDisChi2": 50, 
                "KsIP": 1, 
                "KsMAXDOCA": 1.0, 
                "KsVtxChi2": 50, 
                "MaxKsMass": 800.0, 
                "Postscale": 1, 
                "Prescale": 1
            }, 
            "LU": {
                "KsDisChi2": 2500, 
                "KsIP": 1, 
                "KsMAXDOCA": 1.0, 
                "KsVtxChi2": 35, 
                "MaxKsMass": 800.0, 
                "Postscale": 1, 
                "Prescale": 1
            }, 
            "LV": {
                "KsDisChi2": 2500, 
                "KsIP": 1, 
                "KsMAXDOCA": 1.0, 
                "KsVtxChi2": 35, 
                "MaxKsMass": 800.0, 
                "Postscale": 1, 
                "Prescale": 1
            }, 
            "UU": {
                "KsDisChi2": 2500, 
                "KsIP": 1, 
                "KsMAXDOCA": 1.0, 
                "KsVtxChi2": 35, 
                "MaxKsMass": 800.0, 
                "Postscale": 1, 
                "Prescale": 1
            }, 
            "UV": {
                "KsDisChi2": 2500, 
                "KsIP": 1, 
                "KsMAXDOCA": 1.0, 
                "KsVtxChi2": 35, 
                "MaxKsMass": 800.0, 
                "Postscale": 1, 
                "Prescale": 1
            }, 
            "VV": {
                "KsDisChi2": 2500, 
                "KsIP": 1, 
                "KsMAXDOCA": 1.0, 
                "KsVtxChi2": 35, 
                "MaxKsMass": 800.0, 
                "Postscale": 1, 
                "Prescale": 1
            }
        }, 
        "4e": {
            "LL": {
                "KsIP": 2, 
                "KsLifetime": 0.8953, 
                "KsMAXDOCA": 2.0, 
                "KsVtxChi2": 50, 
                "MaxKsMass": 800.0, 
                "Postscale": 1, 
                "Prescale": 1
            }, 
            "LU": {
                "KsDisChi2": 2500, 
                "KsIP": 5, 
                "KsMAXDOCA": 1.0, 
                "KsVtxChi2": 25, 
                "MaxKsMass": 800.0, 
                "Postscale": 1, 
                "Prescale": 1
            }, 
            "LV": {
                "KsDisChi2": 2500, 
                "KsIP": 10, 
                "KsMAXDOCA": 0.5, 
                "KsVtxChi2": 37, 
                "MaxKsMass": 900.0, 
                "Postscale": 1, 
                "Prescale": 1
            }, 
            "UU": {
                "KsDisChi2": 2500, 
                "KsIP": 1, 
                "KsMAXDOCA": 1.0, 
                "KsVtxChi2": 35, 
                "MaxKsMass": 800.0, 
                "Postscale": 1, 
                "Prescale": 1
            }, 
            "UV": {
                "KsDisChi2": 2000, 
                "KsIP": 15, 
                "KsMAXDOCA": 0.8, 
                "KsVtxChi2": 20, 
                "MaxKsMass": 880.0, 
                "Postscale": 1, 
                "Prescale": 1
            }, 
            "VV": {
                "KsDisChi2": 2500, 
                "KsIP": 1, 
                "KsMAXDOCA": 1.0, 
                "KsVtxChi2": 40, 
                "MaxKsMass": 900.0, 
                "Postscale": 1, 
                "Prescale": 1
            }
        }, 
        "4mu": {
            "LL": {
                "KsDisChi2": 1500.0, 
                "KsIP": 1, 
                "KsMAXDOCA": 1.0, 
                "KsVtxChi2": 50, 
                "MaxKsMass": 800.0, 
                "Postscale": 1, 
                "Prescale": 1
            }, 
            "LU": {
                "KsDisChi2": 1500, 
                "KsIP": 1, 
                "KsMAXDOCA": 1.0, 
                "KsVtxChi2": 25, 
                "MaxKsMass": 800.0, 
                "Postscale": 1, 
                "Prescale": 1
            }, 
            "LV": {
                "KsDisChi2": 1500, 
                "KsIP": 10, 
                "KsMAXDOCA": 0.5, 
                "KsVtxChi2": 35, 
                "MaxKsMass": 800.0, 
                "Postscale": 1, 
                "Prescale": 1
            }, 
            "UU": {
                "KsDisChi2": 1500, 
                "KsIP": 1, 
                "KsMAXDOCA": 1.0, 
                "KsVtxChi2": 35, 
                "MaxKsMass": 800.0, 
                "Postscale": 1, 
                "Prescale": 1
            }, 
            "UV": {
                "KsDisChi2": 1500, 
                "KsIP": 1, 
                "KsMAXDOCA": 1.0, 
                "KsVtxChi2": 19, 
                "MaxKsMass": 800.0, 
                "Postscale": 1, 
                "Prescale": 1
            }, 
            "VV": {
                "KsDisChi2": 1500, 
                "KsIP": 1, 
                "KsMAXDOCA": 1.0, 
                "KsVtxChi2": 35, 
                "MaxKsMass": 800.0, 
                "Postscale": 1, 
                "Prescale": 1
            }
        }, 
        "TISTOSDict": {
            "Hlt2RareStrangeKsLeptonsTOSDecision%TOS": 0
        }, 
        "e": {
            "Di": {
                "GhostProb": 0.5, 
                "MINIPCHI2": 16, 
                "PIDe": -4, 
                "PT": 100.0
            }, 
            "L": {
                "GhostProb": 0.35, 
                "MINIPCHI2": 50, 
                "PIDe": -1, 
                "PT": 100.0
            }, 
            "U": {
                "GhostProb": 0.35, 
                "MINIPCHI2": 10, 
                "PIDe": -3.5, 
                "PT": 50
            }, 
            "V": {
                "GhostProb": 0.5, 
                "MINIPCHI2": 40, 
                "PIDe": -1000, 
                "PT": 0
            }
        }, 
        "mu": {
            "L": {
                "GhostProb": 0.5, 
                "MINIPCHI2": 20, 
                "PIDmu": -5, 
                "PT": 50.0
            }, 
            "U": {
                "GhostProb": 0.35, 
                "MINIPCHI2": 10, 
                "PIDmu": -3.5, 
                "PT": 50
            }, 
            "V": {
                "GhostProb": 0.35, 
                "MINIPCHI2": 10, 
                "PIDmu": -3.5, 
                "PT": 50
            }
        }, 
        "pi": {
            "L": {
                "GhostProb": 0.5, 
                "MINIPCHI2": 16, 
                "PIDK": 5, 
                "PT": 100.0
            }, 
            "U": {
                "GhostProb": 0.35, 
                "MINIPCHI2": 10, 
                "PIDK": -3.5, 
                "PT": 50
            }, 
            "V": {
                "GhostProb": 0.35, 
                "MINIPCHI2": 10, 
                "PIDK": -3.5, 
                "PT": 50
            }
        }
    }, 
    "STREAMS": [ "Leptonic" ], 
    "WGs": [ "RD" ]
}

LFV = {
    "BUILDERTYPE": "LFVLinesConf", 
    "CONFIG": {
        "B2TauMuPrescale": 1, 
        "B2eMuPrescale": 1, 
        "B2eePrescale": 1, 
        "B2hTauMuPrescale": 1, 
        "B2heMuPrescale": 1, 
        "B2pMuPrescale": 1, 
        "Bu2KJPsieePrescale": 1, 
        "D2piphi2MuMuPrescale": 1, 
        "D2piphi2MuMuPromptPrescale": 1, 
        "D2piphi2eMuPrescale": 1, 
        "D2piphi2eMuPromptPrescale": 0.25, 
        "D2piphi2eePrescale": 1, 
        "D2piphi2eePromptPrescale": 0.2, 
        "JPsi2MuMuControlPrescale": 0.05, 
        "JPsi2eMuPrescale": 1, 
        "JPsi2eeControlPrescale": 0.05, 
        "Phi2MuMuControlPrescale": 1, 
        "Phi2eMuPrescale": 1, 
        "Phi2eeControlPrescale": 1, 
        "Postscale": 1, 
        "PromptJPsi2MuMuControlPrescale": 0.05, 
        "PromptJPsi2eMuPrescale": 1, 
        "PromptJPsi2eeControlPrescale": 0.05, 
        "PromptPhi2MuMuControlPrescale": 1, 
        "PromptPhi2eMuPrescale": 1, 
        "PromptPhi2eeControlPrescale": 0.6, 
        "RelatedInfoTools_B2eMu": [
            {
                "Location": "BSMUMUVARIABLES", 
                "Type": "RelInfoBs2MuMuBIsolations", 
                "Variables": [
                    "BSMUMUCDFISO", 
                    "BSMUMUOTHERBMAG", 
                    "BSMUMUOTHERBANGLE", 
                    "BSMUMUOTHERBBOOSTMAG", 
                    "BSMUMUOTHERBBOOSTANGLE", 
                    "BSMUMUOTHERBTRACKS"
                ], 
                "makeTrackCuts": False, 
                "tracktype": 3
            }, 
            {
                "DaughterLocations": {
                    "[B_s0 -> ^e+ [mu-]cc]CC": "Electron_ISO", 
                    "[B_s0 -> e+ ^[mu-]cc]CC": "Muon_ISO"
                }, 
                "IsoTwoBody": True, 
                "Type": "RelInfoBs2MuMuTrackIsolations", 
                "Variables": [
                    "BSMUMUTRACKPLUSISO", 
                    "BSMUMUTRACKPLUSISOTWO", 
                    "ISOTWOBODYQPLUS", 
                    "ISOTWOBODYMASSISOPLUS", 
                    "ISOTWOBODYCHI2ISOPLUS", 
                    "ISOTWOBODYISO5PLUS"
                ], 
                "angle": 0.27, 
                "doca_iso": 0.13, 
                "fc": 0.6, 
                "ips": 3.0, 
                "makeTrackCuts": False, 
                "pvdis": 0.5, 
                "pvdis_h": 40.0, 
                "svdis": -0.15, 
                "svdis_h": 30.0, 
                "tracktype": 3
            }, 
            {
                "Location": "VtxIsoInfo", 
                "Type": "RelInfoVertexIsolation"
            }, 
            {
                "Location": "ConeIsoInfo", 
                "Type": "RelInfoConeVariables"
            }, 
            {
                "Location": "VtxIsoInfoBDT", 
                "Type": "RelInfoVertexIsolationBDT"
            }, 
            {
                "DaughterLocations": {
                    "[B_s0 -> ^e+ [mu-]cc]CC": "Electron_TrackIsoBDT", 
                    "[B_s0 -> e+ ^[mu-]cc]CC": "Muon_TrackIsoBDT"
                }, 
                "Type": "RelInfoTrackIsolationBDT", 
                "Variables": 0
            }, 
            {
                "Location": "TrackIsolationBDT2", 
                "Type": "RelInfoTrackIsolationBDT2"
            }
        ], 
        "RelatedInfoTools_B2ee": [
            {
                "Location": "BSMUMUVARIABLES", 
                "Type": "RelInfoBs2MuMuBIsolations", 
                "Variables": [
                    "BSMUMUCDFISO", 
                    "BSMUMUOTHERBMAG", 
                    "BSMUMUOTHERBANGLE", 
                    "BSMUMUOTHERBBOOSTMAG", 
                    "BSMUMUOTHERBBOOSTANGLE", 
                    "BSMUMUOTHERBTRACKS"
                ], 
                "makeTrackCuts": False, 
                "tracktype": 3
            }, 
            {
                "DaughterLocations": {
                    "[B_s0 -> ^e+ [e-]cc]CC": "Electron1_ISO", 
                    "[B_s0 -> e+ ^[e-]cc]CC": "Electron2_ISO"
                }, 
                "IsoTwoBody": True, 
                "Type": "RelInfoBs2MuMuTrackIsolations", 
                "Variables": [
                    "BSMUMUTRACKPLUSISO", 
                    "BSMUMUTRACKPLUSISOTWO", 
                    "ISOTWOBODYQPLUS", 
                    "ISOTWOBODYMASSISOPLUS", 
                    "ISOTWOBODYCHI2ISOPLUS", 
                    "ISOTWOBODYISO5PLUS"
                ], 
                "angle": 0.27, 
                "doca_iso": 0.13, 
                "fc": 0.6, 
                "ips": 3.0, 
                "makeTrackCuts": False, 
                "pvdis": 0.5, 
                "pvdis_h": 40.0, 
                "svdis": -0.15, 
                "svdis_h": 30.0, 
                "tracktype": 3
            }, 
            {
                "Location": "VtxIsoInfo", 
                "Type": "RelInfoVertexIsolation"
            }, 
            {
                "Location": "ConeIsoInfo", 
                "Type": "RelInfoConeVariables"
            }, 
            {
                "Location": "VtxIsoInfoBDT", 
                "Type": "RelInfoVertexIsolationBDT"
            }, 
            {
                "DaughterLocations": {
                    "[B_s0 -> ^e+ [e-]cc]CC": "Electron2_TrackIsoBDT", 
                    "[B_s0 -> e+ ^[e-]cc]CC": "Electron1_TrackIsoBDT"
                }, 
                "Type": "RelInfoTrackIsolationBDT", 
                "Variables": 0
            }, 
            {
                "Location": "TrackIsolationBDT2", 
                "Type": "RelInfoTrackIsolationBDT2"
            }
        ], 
        "RelatedInfoTools_B2hemu": [
            {
                "Location": "BSMUMUVARIABLES", 
                "Type": "RelInfoBs2MuMuBIsolations", 
                "Variables": [
                    "BSMUMUCDFISO", 
                    "BSMUMUOTHERBMAG", 
                    "BSMUMUOTHERBANGLE", 
                    "BSMUMUOTHERBBOOSTMAG", 
                    "BSMUMUOTHERBBOOSTANGLE", 
                    "BSMUMUOTHERBTRACKS"
                ], 
                "makeTrackCuts": False, 
                "tracktype": 3
            }, 
            {
                "DaughterLocations": {
                    "[[B+]cc -> X [e+]cc ^[mu-]cc]CC": "Muon_ISO", 
                    "[[B+]cc -> X ^[e+]cc [mu-]cc]CC": "Electron_ISO", 
                    "[[B+]cc -> ^X [e+]cc [mu-]cc]CC": "Hadron_ISO"
                }, 
                "IsoTwoBody": True, 
                "Type": "RelInfoBs2MuMuTrackIsolations", 
                "Variables": [
                    "BSMUMUTRACKPLUSISO", 
                    "BSMUMUTRACKPLUSISOTWO", 
                    "ISOTWOBODYQPLUS", 
                    "ISOTWOBODYMASSISOPLUS", 
                    "ISOTWOBODYCHI2ISOPLUS", 
                    "ISOTWOBODYISO5PLUS"
                ], 
                "angle": 0.27, 
                "doca_iso": 0.13, 
                "fc": 0.6, 
                "ips": 3.0, 
                "makeTrackCuts": False, 
                "pvdis": 0.5, 
                "pvdis_h": 40.0, 
                "svdis": -0.15, 
                "svdis_h": 30.0, 
                "tracktype": 3
            }, 
            {
                "DaughterLocations": {
                    "[[B+]cc -> X [e+]cc ^[mu-]cc]CC": "Muon_TrackIso_BDT6vars", 
                    "[[B+]cc -> X ^[e+]cc [mu-]cc]CC": "Electron_TrackIso_BDT6vars", 
                    "[[B+]cc -> ^X [e+]cc [mu-]cc]CC": "Hadron_TrackIso_BDT6vars"
                }, 
                "Type": "RelInfoTrackIsolationBDT", 
                "Variables": 1, 
                "WeightsFile": "BsMuMu_TrackIsolationBDT6varsB_v1r4.xml"
            }, 
            {
                "DaughterLocations": {
                    "[[B+]cc -> X [e+]cc ^[mu-]cc]CC": "Muon_TrackIso_BDT9vars", 
                    "[[B+]cc -> X ^[e+]cc [mu-]cc]CC": "Electron_TrackIso_BDT9vars", 
                    "[[B+]cc -> ^X [e+]cc [mu-]cc]CC": "Hadron_TrackIso_BDT9vars"
                }, 
                "Type": "RelInfoTrackIsolationBDT", 
                "Variables": 2, 
                "WeightsFile": "BsMuMu_TrackIsolationBDT9vars_v1r4.xml"
            }, 
            {
                "Location": "coneInfo", 
                "Type": "RelInfoConeVariables", 
                "Variables": [
                    "CONEANGLE", 
                    "CONEMULT", 
                    "CONEPT", 
                    "CONEPTASYM"
                ]
            }, 
            {
                "Location": "VtxIsoInfo", 
                "Type": "RelInfoVertexIsolation"
            }, 
            {
                "Location": "VtxIsoInfoBDT", 
                "Type": "RelInfoVertexIsolationBDT"
            }, 
            {
                "Location": "TrackIsolationBDT2", 
                "Particles": [
                    1, 
                    2
                ], 
                "Type": "RelInfoTrackIsolationBDT2"
            }
        ], 
        "RelatedInfoTools_Bu2KJPsiee": [
            {
                "DaughterLocations": {
                    "[B+ -> ^(J/psi(1S) -> e+ e-) K+]CC": "Jpsi_ISO"
                }, 
                "Location": "BSMUMUVARIABLES", 
                "Type": "RelInfoBs2MuMuBIsolations", 
                "Variables": [
                    "BSMUMUCDFISO", 
                    "BSMUMUOTHERBMAG", 
                    "BSMUMUOTHERBANGLE", 
                    "BSMUMUOTHERBBOOSTMAG", 
                    "BSMUMUOTHERBBOOSTANGLE", 
                    "BSMUMUOTHERBTRACKS"
                ], 
                "makeTrackCuts": False, 
                "tracktype": 3
            }, 
            {
                "DaughterLocations": {
                    "[B+ -> (J/psi(1S) -> ^e+ e-) K+]CC": "Electron1_ISO", 
                    "[B+ -> (J/psi(1S) -> e+ ^e-) K+]CC": "Electron2_ISO", 
                    "[B+ -> (J/psi(1S) -> e+ e-) ^K+]CC": "Kplus_ISO"
                }, 
                "IsoTwoBody": True, 
                "Type": "RelInfoBs2MuMuTrackIsolations", 
                "Variables": [
                    "BSMUMUTRACKPLUSISO", 
                    "BSMUMUTRACKPLUSISOTWO", 
                    "ISOTWOBODYQPLUS", 
                    "ISOTWOBODYMASSISOPLUS", 
                    "ISOTWOBODYCHI2ISOPLUS", 
                    "ISOTWOBODYISO5PLUS"
                ], 
                "angle": 0.27, 
                "doca_iso": 0.13, 
                "fc": 0.6, 
                "ips": 3.0, 
                "makeTrackCuts": False, 
                "pvdis": 0.5, 
                "pvdis_h": 40.0, 
                "svdis": -0.15, 
                "svdis_h": 30.0, 
                "tracktype": 3
            }, 
            {
                "Location": "VtxIsoInfo", 
                "Type": "RelInfoVertexIsolation"
            }, 
            {
                "Location": "ConeIsoInfo", 
                "Type": "RelInfoConeVariables"
            }, 
            {
                "Location": "VtxIsoInfoBDT", 
                "Type": "RelInfoVertexIsolationBDT"
            }, 
            {
                "DaughterLocations": {
                    "[B+ -> (J/psi(1S) -> ^e+ e-) K+]CC": "Electron1_TrackIsoBDT", 
                    "[B+ -> (J/psi(1S) -> e+ ^e-) K+]CC": "Electron2_TrackIsoBDT"
                }, 
                "Type": "RelInfoTrackIsolationBDT", 
                "Variables": 0
            }, 
            {
                "Location": "TrackIsolationBDT2", 
                "Particles": [
                    1, 
                    2
                ], 
                "Type": "RelInfoTrackIsolationBDT2"
            }
        ], 
        "RelatedInfoTools_D2piphi": [
            {
                "Location": "BSMUMUVARIABLES", 
                "Type": "RelInfoBs2MuMuBIsolations", 
                "Variables": [
                    "BSMUMUCDFISO", 
                    "BSMUMUOTHERBMAG", 
                    "BSMUMUOTHERBANGLE", 
                    "BSMUMUOTHERBBOOSTMAG", 
                    "BSMUMUOTHERBBOOSTANGLE", 
                    "BSMUMUOTHERBTRACKS"
                ], 
                "makeTrackCuts": False, 
                "tracktype": 3
            }, 
            {
                "DaughterLocations": {
                    "[Charm -> [(phi(1020) -> ^l+ [l-]CC)]CC pi+ ]CC": "lplus_ISO", 
                    "[Charm -> [(phi(1020) -> l+ [^l-]CC)]CC pi+ ]CC": "lminus_ISO", 
                    "[Charm -> [(phi(1020) -> l+ [l-]CC)]CC ^pi+ ]CC": "pi_ISO", 
                    "[Charm -> [^(phi(1020) -> l+ [l-]CC)]CC pi+ ]CC": "phi_ISO"
                }, 
                "IgnoreUnmatchedDescriptors": True, 
                "IsoTwoBody": True, 
                "Type": "RelInfoBs2MuMuTrackIsolations", 
                "Variables": [
                    "BSMUMUTRACKPLUSISO", 
                    "BSMUMUTRACKPLUSISOTWO", 
                    "ISOTWOBODYQPLUS", 
                    "ISOTWOBODYMASSISOPLUS", 
                    "ISOTWOBODYCHI2ISOPLUS", 
                    "ISOTWOBODYISO5PLUS"
                ], 
                "angle": 0.27, 
                "doca_iso": 0.13, 
                "fc": 0.6, 
                "ips": 3.0, 
                "makeTrackCuts": False, 
                "pvdis": 0.5, 
                "pvdis_h": 40.0, 
                "svdis": -0.15, 
                "svdis_h": 30.0, 
                "tracktype": 3
            }, 
            {
                "DaughterLocations": {
                    "[Charm -> [(phi(1020) -> ^l+ [l-]CC)]CC pi+ ]CC": "lplus_TrackIso_BDT6vars", 
                    "[Charm -> [(phi(1020) -> l+ [^l-]CC)]CC pi+ ]CC": "lminus_TrackIso_BDT6vars", 
                    "[Charm -> [(phi(1020) -> l+ [l-]CC)]CC ^pi+ ]CC": "pi_TrackIso_BDT6vars", 
                    "[Charm -> [^(phi(1020) -> l+ [l-]CC)]CC pi+ ]CC": "phi_TrackIso_BDT6vars"
                }, 
                "IgnoreUnmatchedDescriptors": True, 
                "Type": "RelInfoTrackIsolationBDT", 
                "Variables": 1, 
                "WeightsFile": "BsMuMu_TrackIsolationBDT6varsB_v1r4.xml"
            }, 
            {
                "DaughterLocations": {
                    "[Charm -> [(phi(1020) -> ^l+ [l-]CC)]CC pi+ ]CC": "lplus_TrackIso_BDT9vars", 
                    "[Charm -> [(phi(1020) -> l+ [^l-]CC)]CC pi+ ]CC": "lminus_TrackIso_BDT9vars", 
                    "[Charm -> [(phi(1020) -> l+ [l-]CC)]CC ^pi+ ]CC": "pi_TrackIso_BDT9vars", 
                    "[Charm -> [^(phi(1020) -> l+ [l-]CC)]CC pi+ ]CC": "phi_TrackIso_BDT9vars"
                }, 
                "IgnoreUnmatchedDescriptors": True, 
                "Type": "RelInfoTrackIsolationBDT", 
                "Variables": 2, 
                "WeightsFile": "BsMuMu_TrackIsolationBDT9vars_v1r4.xml"
            }, 
            {
                "Location": "coneInfo", 
                "Type": "RelInfoConeVariables", 
                "Variables": [
                    "CONEANGLE", 
                    "CONEMULT", 
                    "CONEPT", 
                    "CONEPTASYM"
                ]
            }, 
            {
                "Location": "VtxIsoInfo", 
                "Type": "RelInfoVertexIsolation"
            }, 
            {
                "Location": "VtxIsoInfoBDT", 
                "Type": "RelInfoVertexIsolationBDT"
            }
        ], 
        "RelatedInfoTools_JPsi2MuMuControl": [
            {
                "Location": "BSMUMUVARIABLES", 
                "Type": "RelInfoBs2MuMuBIsolations", 
                "Variables": [
                    "BSMUMUCDFISO", 
                    "BSMUMUOTHERBMAG", 
                    "BSMUMUOTHERBANGLE", 
                    "BSMUMUOTHERBBOOSTMAG", 
                    "BSMUMUOTHERBBOOSTANGLE", 
                    "BSMUMUOTHERBTRACKS"
                ], 
                "makeTrackCuts": False, 
                "tracktype": 3
            }, 
            {
                "DaughterLocations": {
                    "[J/psi(1S) -> ^mu+ [mu-]cc]CC": "Electron_ISO", 
                    "[J/psi(1S) -> mu+ ^[mu-]cc]CC": "Muon_ISO"
                }, 
                "IsoTwoBody": True, 
                "Type": "RelInfoBs2MuMuTrackIsolations", 
                "Variables": [
                    "BSMUMUTRACKPLUSISO", 
                    "BSMUMUTRACKPLUSISOTWO", 
                    "ISOTWOBODYQPLUS", 
                    "ISOTWOBODYMASSISOPLUS", 
                    "ISOTWOBODYCHI2ISOPLUS", 
                    "ISOTWOBODYISO5PLUS"
                ], 
                "angle": 0.27, 
                "doca_iso": 0.13, 
                "fc": 0.6, 
                "ips": 3.0, 
                "makeTrackCuts": False, 
                "pvdis": 0.5, 
                "pvdis_h": 40.0, 
                "svdis": -0.15, 
                "svdis_h": 30.0, 
                "tracktype": 3
            }, 
            {
                "DaughterLocations": {
                    "[J/psi(1S) -> ^mu+ [mu-]cc]CC": "Electron_TrackIso_BDT6vars", 
                    "[J/psi(1S) -> mu+ ^[mu-]cc]CC": "Muon_TrackIso_BDT6vars"
                }, 
                "Type": "RelInfoTrackIsolationBDT", 
                "Variables": 1, 
                "WeightsFile": "BsMuMu_TrackIsolationBDT6varsB_v1r4.xml"
            }, 
            {
                "DaughterLocations": {
                    "[J/psi(1S) -> ^mu+ [mu-]cc]CC": "Electron_TrackIso_BDT9vars", 
                    "[J/psi(1S) -> mu+ ^[mu-]cc]CC": "Muon_TrackIso_BDT9vars"
                }, 
                "Type": "RelInfoTrackIsolationBDT", 
                "Variables": 2, 
                "WeightsFile": "BsMuMu_TrackIsolationBDT9vars_v1r4.xml"
            }, 
            {
                "Location": "coneInfo", 
                "Type": "RelInfoConeVariables", 
                "Variables": [
                    "CONEANGLE", 
                    "CONEMULT", 
                    "CONEPT", 
                    "CONEPTASYM"
                ]
            }, 
            {
                "Location": "VtxIsoInfo", 
                "Type": "RelInfoVertexIsolation"
            }, 
            {
                "Location": "VtxIsoInfoBDT", 
                "Type": "RelInfoVertexIsolationBDT"
            }, 
            {
                "Location": "TrackIsolationBDT2", 
                "Type": "RelInfoTrackIsolationBDT2"
            }
        ], 
        "RelatedInfoTools_JPsi2eMu": [
            {
                "Location": "BSMUMUVARIABLES", 
                "Type": "RelInfoBs2MuMuBIsolations", 
                "Variables": [
                    "BSMUMUCDFISO", 
                    "BSMUMUOTHERBMAG", 
                    "BSMUMUOTHERBANGLE", 
                    "BSMUMUOTHERBBOOSTMAG", 
                    "BSMUMUOTHERBBOOSTANGLE", 
                    "BSMUMUOTHERBTRACKS"
                ], 
                "makeTrackCuts": False, 
                "tracktype": 3
            }, 
            {
                "DaughterLocations": {
                    "[J/psi(1S) -> ^e+ [mu-]cc]CC": "Electron_ISO", 
                    "[J/psi(1S) -> e+ ^[mu-]cc]CC": "Muon_ISO"
                }, 
                "IsoTwoBody": True, 
                "Type": "RelInfoBs2MuMuTrackIsolations", 
                "Variables": [
                    "BSMUMUTRACKPLUSISO", 
                    "BSMUMUTRACKPLUSISOTWO", 
                    "ISOTWOBODYQPLUS", 
                    "ISOTWOBODYMASSISOPLUS", 
                    "ISOTWOBODYCHI2ISOPLUS", 
                    "ISOTWOBODYISO5PLUS"
                ], 
                "angle": 0.27, 
                "doca_iso": 0.13, 
                "fc": 0.6, 
                "ips": 3.0, 
                "makeTrackCuts": False, 
                "pvdis": 0.5, 
                "pvdis_h": 40.0, 
                "svdis": -0.15, 
                "svdis_h": 30.0, 
                "tracktype": 3
            }, 
            {
                "DaughterLocations": {
                    "[J/psi(1S) -> ^e+ [mu-]cc]CC": "Electron_TrackIso_BDT6vars", 
                    "[J/psi(1S) -> e+ ^[mu-]cc]CC": "Muon_TrackIso_BDT6vars"
                }, 
                "Type": "RelInfoTrackIsolationBDT", 
                "Variables": 1, 
                "WeightsFile": "BsMuMu_TrackIsolationBDT6varsB_v1r4.xml"
            }, 
            {
                "DaughterLocations": {
                    "[J/psi(1S) -> ^e+ [mu-]cc]CC": "Electron_TrackIso_BDT9vars", 
                    "[J/psi(1S) -> e+ ^[mu-]cc]CC": "Muon_TrackIso_BDT9vars"
                }, 
                "Type": "RelInfoTrackIsolationBDT", 
                "Variables": 2, 
                "WeightsFile": "BsMuMu_TrackIsolationBDT9vars_v1r4.xml"
            }, 
            {
                "Location": "coneInfo", 
                "Type": "RelInfoConeVariables", 
                "Variables": [
                    "CONEANGLE", 
                    "CONEMULT", 
                    "CONEPT", 
                    "CONEPTASYM"
                ]
            }, 
            {
                "Location": "VtxIsoInfo", 
                "Type": "RelInfoVertexIsolation"
            }, 
            {
                "Location": "VtxIsoInfoBDT", 
                "Type": "RelInfoVertexIsolationBDT"
            }, 
            {
                "Location": "TrackIsolationBDT2", 
                "Type": "RelInfoTrackIsolationBDT2"
            }
        ], 
        "RelatedInfoTools_JPsi2eeControl": [
            {
                "Location": "BSMUMUVARIABLES", 
                "Type": "RelInfoBs2MuMuBIsolations", 
                "Variables": [
                    "BSMUMUCDFISO", 
                    "BSMUMUOTHERBMAG", 
                    "BSMUMUOTHERBANGLE", 
                    "BSMUMUOTHERBBOOSTMAG", 
                    "BSMUMUOTHERBBOOSTANGLE", 
                    "BSMUMUOTHERBTRACKS"
                ], 
                "makeTrackCuts": False, 
                "tracktype": 3
            }, 
            {
                "DaughterLocations": {
                    "[J/psi(1S) -> ^e+ [e-]cc]CC": "Electron_ISO", 
                    "[J/psi(1S) -> e+ ^[e-]cc]CC": "Muon_ISO"
                }, 
                "IsoTwoBody": True, 
                "Type": "RelInfoBs2MuMuTrackIsolations", 
                "Variables": [
                    "BSMUMUTRACKPLUSISO", 
                    "BSMUMUTRACKPLUSISOTWO", 
                    "ISOTWOBODYQPLUS", 
                    "ISOTWOBODYMASSISOPLUS", 
                    "ISOTWOBODYCHI2ISOPLUS", 
                    "ISOTWOBODYISO5PLUS"
                ], 
                "angle": 0.27, 
                "doca_iso": 0.13, 
                "fc": 0.6, 
                "ips": 3.0, 
                "makeTrackCuts": False, 
                "pvdis": 0.5, 
                "pvdis_h": 40.0, 
                "svdis": -0.15, 
                "svdis_h": 30.0, 
                "tracktype": 3
            }, 
            {
                "DaughterLocations": {
                    "[J/psi(1S) -> ^e+ [e-]cc]CC": "Electron_TrackIso_BDT6vars", 
                    "[J/psi(1S) -> e+ ^[e-]cc]CC": "Muon_TrackIso_BDT6vars"
                }, 
                "Type": "RelInfoTrackIsolationBDT", 
                "Variables": 1, 
                "WeightsFile": "BsMuMu_TrackIsolationBDT6varsB_v1r4.xml"
            }, 
            {
                "DaughterLocations": {
                    "[J/psi(1S) -> ^e+ [e-]cc]CC": "Electron_TrackIso_BDT9vars", 
                    "[J/psi(1S) -> e+ ^[e-]cc]CC": "Muon_TrackIso_BDT9vars"
                }, 
                "Type": "RelInfoTrackIsolationBDT", 
                "Variables": 2, 
                "WeightsFile": "BsMuMu_TrackIsolationBDT9vars_v1r4.xml"
            }, 
            {
                "Location": "coneInfo", 
                "Type": "RelInfoConeVariables", 
                "Variables": [
                    "CONEANGLE", 
                    "CONEMULT", 
                    "CONEPT", 
                    "CONEPTASYM"
                ]
            }, 
            {
                "Location": "VtxIsoInfo", 
                "Type": "RelInfoVertexIsolation"
            }, 
            {
                "Location": "VtxIsoInfoBDT", 
                "Type": "RelInfoVertexIsolationBDT"
            }, 
            {
                "Location": "TrackIsolationBDT2", 
                "Type": "RelInfoTrackIsolationBDT2"
            }
        ], 
        "RelatedInfoTools_Phi2MuMuControl": [
            {
                "Location": "BSMUMUVARIABLES", 
                "Type": "RelInfoBs2MuMuBIsolations", 
                "Variables": [
                    "BSMUMUCDFISO", 
                    "BSMUMUOTHERBMAG", 
                    "BSMUMUOTHERBANGLE", 
                    "BSMUMUOTHERBBOOSTMAG", 
                    "BSMUMUOTHERBBOOSTANGLE", 
                    "BSMUMUOTHERBTRACKS"
                ], 
                "makeTrackCuts": False, 
                "tracktype": 3
            }, 
            {
                "DaughterLocations": {
                    "[phi(1020) -> ^mu+ [mu-]cc]CC": "Electron_ISO", 
                    "[phi(1020) -> mu+ ^[mu-]cc]CC": "Muon_ISO"
                }, 
                "IsoTwoBody": True, 
                "Type": "RelInfoBs2MuMuTrackIsolations", 
                "Variables": [
                    "BSMUMUTRACKPLUSISO", 
                    "BSMUMUTRACKPLUSISOTWO", 
                    "ISOTWOBODYQPLUS", 
                    "ISOTWOBODYMASSISOPLUS", 
                    "ISOTWOBODYCHI2ISOPLUS", 
                    "ISOTWOBODYISO5PLUS"
                ], 
                "angle": 0.27, 
                "doca_iso": 0.13, 
                "fc": 0.6, 
                "ips": 3.0, 
                "makeTrackCuts": False, 
                "pvdis": 0.5, 
                "pvdis_h": 40.0, 
                "svdis": -0.15, 
                "svdis_h": 30.0, 
                "tracktype": 3
            }, 
            {
                "DaughterLocations": {
                    "[phi(1020) -> ^mu+ [mu-]cc]CC": "Electron_TrackIso_BDT6vars", 
                    "[phi(1020) -> mu+ ^[mu-]cc]CC": "Muon_TrackIso_BDT6vars"
                }, 
                "Type": "RelInfoTrackIsolationBDT", 
                "Variables": 1, 
                "WeightsFile": "BsMuMu_TrackIsolationBDT6varsB_v1r4.xml"
            }, 
            {
                "DaughterLocations": {
                    "[phi(1020) -> ^mu+ [mu-]cc]CC": "Electron_TrackIso_BDT9vars", 
                    "[phi(1020) -> mu+ ^[mu-]cc]CC": "Muon_TrackIso_BDT9vars"
                }, 
                "Type": "RelInfoTrackIsolationBDT", 
                "Variables": 2, 
                "WeightsFile": "BsMuMu_TrackIsolationBDT9vars_v1r4.xml"
            }, 
            {
                "Location": "coneInfo", 
                "Type": "RelInfoConeVariables", 
                "Variables": [
                    "CONEANGLE", 
                    "CONEMULT", 
                    "CONEPT", 
                    "CONEPTASYM"
                ]
            }, 
            {
                "Location": "VtxIsoInfo", 
                "Type": "RelInfoVertexIsolation"
            }, 
            {
                "Location": "VtxIsoInfoBDT", 
                "Type": "RelInfoVertexIsolationBDT"
            }, 
            {
                "Location": "TrackIsolationBDT2", 
                "Type": "RelInfoTrackIsolationBDT2"
            }
        ], 
        "RelatedInfoTools_Phi2eMu": [
            {
                "Location": "BSMUMUVARIABLES", 
                "Type": "RelInfoBs2MuMuBIsolations", 
                "Variables": [
                    "BSMUMUCDFISO", 
                    "BSMUMUOTHERBMAG", 
                    "BSMUMUOTHERBANGLE", 
                    "BSMUMUOTHERBBOOSTMAG", 
                    "BSMUMUOTHERBBOOSTANGLE", 
                    "BSMUMUOTHERBTRACKS"
                ], 
                "makeTrackCuts": False, 
                "tracktype": 3
            }, 
            {
                "DaughterLocations": {
                    "[phi(1020) -> ^e+ [mu-]cc]CC": "Electron_ISO", 
                    "[phi(1020) -> e+ ^[mu-]cc]CC": "Muon_ISO"
                }, 
                "IsoTwoBody": True, 
                "Type": "RelInfoBs2MuMuTrackIsolations", 
                "Variables": [
                    "BSMUMUTRACKPLUSISO", 
                    "BSMUMUTRACKPLUSISOTWO", 
                    "ISOTWOBODYQPLUS", 
                    "ISOTWOBODYMASSISOPLUS", 
                    "ISOTWOBODYCHI2ISOPLUS", 
                    "ISOTWOBODYISO5PLUS"
                ], 
                "angle": 0.27, 
                "doca_iso": 0.13, 
                "fc": 0.6, 
                "ips": 3.0, 
                "makeTrackCuts": False, 
                "pvdis": 0.5, 
                "pvdis_h": 40.0, 
                "svdis": -0.15, 
                "svdis_h": 30.0, 
                "tracktype": 3
            }, 
            {
                "DaughterLocations": {
                    "[phi(1020) -> ^e+ [mu-]cc]CC": "Electron_TrackIso_BDT6vars", 
                    "[phi(1020) -> e+ ^[mu-]cc]CC": "Muon_TrackIso_BDT6vars"
                }, 
                "Type": "RelInfoTrackIsolationBDT", 
                "Variables": 1, 
                "WeightsFile": "BsMuMu_TrackIsolationBDT6varsB_v1r4.xml"
            }, 
            {
                "DaughterLocations": {
                    "[phi(1020) -> ^e+ [mu-]cc]CC": "Electron_TrackIso_BDT9vars", 
                    "[phi(1020) -> e+ ^[mu-]cc]CC": "Muon_TrackIso_BDT9vars"
                }, 
                "Type": "RelInfoTrackIsolationBDT", 
                "Variables": 2, 
                "WeightsFile": "BsMuMu_TrackIsolationBDT9vars_v1r4.xml"
            }, 
            {
                "Location": "coneInfo", 
                "Type": "RelInfoConeVariables", 
                "Variables": [
                    "CONEANGLE", 
                    "CONEMULT", 
                    "CONEPT", 
                    "CONEPTASYM"
                ]
            }, 
            {
                "Location": "VtxIsoInfo", 
                "Type": "RelInfoVertexIsolation"
            }, 
            {
                "Location": "VtxIsoInfoBDT", 
                "Type": "RelInfoVertexIsolationBDT"
            }, 
            {
                "Location": "TrackIsolationBDT2", 
                "Type": "RelInfoTrackIsolationBDT2"
            }
        ], 
        "RelatedInfoTools_Phi2eeControl": [
            {
                "Location": "BSMUMUVARIABLES", 
                "Type": "RelInfoBs2MuMuBIsolations", 
                "Variables": [
                    "BSMUMUCDFISO", 
                    "BSMUMUOTHERBMAG", 
                    "BSMUMUOTHERBANGLE", 
                    "BSMUMUOTHERBBOOSTMAG", 
                    "BSMUMUOTHERBBOOSTANGLE", 
                    "BSMUMUOTHERBTRACKS"
                ], 
                "makeTrackCuts": False, 
                "tracktype": 3
            }, 
            {
                "DaughterLocations": {
                    "[phi(1020) -> ^e+ [e-]cc]CC": "Electron_ISO", 
                    "[phi(1020) -> e+ ^[e-]cc]CC": "Muon_ISO"
                }, 
                "IsoTwoBody": True, 
                "Type": "RelInfoBs2MuMuTrackIsolations", 
                "Variables": [
                    "BSMUMUTRACKPLUSISO", 
                    "BSMUMUTRACKPLUSISOTWO", 
                    "ISOTWOBODYQPLUS", 
                    "ISOTWOBODYMASSISOPLUS", 
                    "ISOTWOBODYCHI2ISOPLUS", 
                    "ISOTWOBODYISO5PLUS"
                ], 
                "angle": 0.27, 
                "doca_iso": 0.13, 
                "fc": 0.6, 
                "ips": 3.0, 
                "makeTrackCuts": False, 
                "pvdis": 0.5, 
                "pvdis_h": 40.0, 
                "svdis": -0.15, 
                "svdis_h": 30.0, 
                "tracktype": 3
            }, 
            {
                "DaughterLocations": {
                    "[phi(1020) -> ^e+ [e-]cc]CC": "Electron_TrackIso_BDT6vars", 
                    "[phi(1020) -> e+ ^[e-]cc]CC": "Muon_TrackIso_BDT6vars"
                }, 
                "Type": "RelInfoTrackIsolationBDT", 
                "Variables": 1, 
                "WeightsFile": "BsMuMu_TrackIsolationBDT6varsB_v1r4.xml"
            }, 
            {
                "DaughterLocations": {
                    "[phi(1020) -> ^e+ [e-]cc]CC": "Electron_TrackIso_BDT9vars", 
                    "[phi(1020) -> e+ ^[e-]cc]CC": "Muon_TrackIso_BDT9vars"
                }, 
                "Type": "RelInfoTrackIsolationBDT", 
                "Variables": 2, 
                "WeightsFile": "BsMuMu_TrackIsolationBDT9vars_v1r4.xml"
            }, 
            {
                "Location": "coneInfo", 
                "Type": "RelInfoConeVariables", 
                "Variables": [
                    "CONEANGLE", 
                    "CONEMULT", 
                    "CONEPT", 
                    "CONEPTASYM"
                ]
            }, 
            {
                "Location": "VtxIsoInfo", 
                "Type": "RelInfoVertexIsolation"
            }, 
            {
                "Location": "VtxIsoInfoBDT", 
                "Type": "RelInfoVertexIsolationBDT"
            }, 
            {
                "Location": "TrackIsolationBDT2", 
                "Type": "RelInfoTrackIsolationBDT2"
            }
        ], 
        "RelatedInfoTools_Tau2MuEtaPrime": [
            {
                "Location": "VtxIsoInfo", 
                "Type": "RelInfoVertexIsolation"
            }, 
            {
                "Location": "VtxIsoInfoBDT", 
                "Type": "RelInfoVertexIsolationBDT"
            }
        ], 
        "RelatedInfoTools_Tau2PhiMu": [
            {
                "ConeAngle": 0.5, 
                "DaughterLocations": {
                    "[tau+ -> (phi(1020)->K+ K-) ^mu+]CC": "coneInfoMu05", 
                    "[tau+ -> (phi(1020)->K+ ^K-) mu+]CC": "coneInfoKminus05", 
                    "[tau+ -> (phi(1020)->^K+ K-) mu+]CC": "coneInfoKplus05", 
                    "[tau+ -> ^(phi(1020)->K+ K-) mu+]CC": "coneInfoPhi05"
                }, 
                "Location": "coneInfoTau05", 
                "Type": "RelInfoConeVariables", 
                "Variables": [
                    "CONEANGLE", 
                    "CONEMULT", 
                    "CONEPT", 
                    "CONEPTASYM"
                ]
            }, 
            {
                "ConeAngle": 0.8, 
                "DaughterLocations": {
                    "[tau+ -> (phi(1020)->K+ K-) ^mu+]CC": "coneInfoMu08", 
                    "[tau+ -> (phi(1020)->K+ ^K-) mu+]CC": "coneInfoKminus08", 
                    "[tau+ -> (phi(1020)->^K+ K-) mu+]CC": "coneInfoKplus08", 
                    "[tau+ -> ^(phi(1020)->K+ K-) mu+]CC": "coneInfoPhi08"
                }, 
                "Location": "coneInfoTau08", 
                "Type": "RelInfoConeVariables", 
                "Variables": [
                    "CONEANGLE", 
                    "CONEMULT", 
                    "CONEPT", 
                    "CONEPTASYM"
                ]
            }, 
            {
                "ConeAngle": 1.0, 
                "DaughterLocations": {
                    "[tau+ -> (phi(1020)->K+ K-) ^mu+]CC": "coneInfoMu10", 
                    "[tau+ -> (phi(1020)->K+ ^K-) mu+]CC": "coneInfoKminus10", 
                    "[tau+ -> (phi(1020)->^K+ K-) mu+]CC": "coneInfoKplus10", 
                    "[tau+ -> ^(phi(1020)->K+ K-) mu+]CC": "coneInfoPhi10"
                }, 
                "Location": "coneInfoTau10", 
                "Type": "RelInfoConeVariables", 
                "Variables": [
                    "CONEANGLE", 
                    "CONEMULT", 
                    "CONEPT", 
                    "CONEPTASYM"
                ]
            }, 
            {
                "ConeAngle": 1.2, 
                "DaughterLocations": {
                    "[tau+ -> (phi(1020)->K+ K-) ^mu+]CC": "coneInfoMu12", 
                    "[tau+ -> (phi(1020)->K+ ^K-) mu+]CC": "coneInfoKminus12", 
                    "[tau+ -> (phi(1020)->^K+ K-) mu+]CC": "coneInfoKplus12", 
                    "[tau+ -> ^(phi(1020)->K+ K-) mu+]CC": "coneInfoPhi12"
                }, 
                "Location": "coneInfoTau12", 
                "Type": "RelInfoConeVariables", 
                "Variables": [
                    "CONEANGLE", 
                    "CONEMULT", 
                    "CONEPT", 
                    "CONEPTASYM"
                ]
            }, 
            {
                "Location": "VtxIsoInfo", 
                "Type": "RelInfoVertexIsolation", 
                "Variables": [
                    "VTXISONUMVTX", 
                    "VTXISODCHI2ONETRACK", 
                    "VTXISODCHI2MASSONETRACK", 
                    "VTXISODCHI2TWOTRACK", 
                    "VTXISODCHI2MASSTWOTRACK"
                ]
            }, 
            {
                "DaughterLocations": {
                    "[tau+ -> (phi(1020)->K+ K-) ^mu+]CC": "MuonTrackIsoBDTInfo", 
                    "[tau+ -> (phi(1020)->K+ ^K-) mu+]CC": "KminusTrackIsoBDTInfo", 
                    "[tau+ -> (phi(1020)->^K+ K-) mu+]CC": "KplusTrackIsoBDTInfo"
                }, 
                "Type": "RelInfoTrackIsolationBDT"
            }
        ], 
        "RelatedInfoTools_Upsilon2eMu": [
            {
                "Location": "BSMUMUVARIABLES", 
                "Type": "RelInfoBs2MuMuBIsolations", 
                "Variables": [
                    "BSMUMUCDFISO", 
                    "BSMUMUOTHERBMAG", 
                    "BSMUMUOTHERBANGLE", 
                    "BSMUMUOTHERBBOOSTMAG", 
                    "BSMUMUOTHERBBOOSTANGLE", 
                    "BSMUMUOTHERBTRACKS"
                ], 
                "makeTrackCuts": False, 
                "tracktype": 3
            }, 
            {
                "DaughterLocations": {
                    "[Upsilon(1S) -> ^e+ [mu-]cc]CC": "Electron_ISO", 
                    "[Upsilon(1S) -> e+ ^[mu-]cc]CC": "Muon_ISO"
                }, 
                "IsoTwoBody": True, 
                "Type": "RelInfoBs2MuMuTrackIsolations", 
                "Variables": [
                    "BSMUMUTRACKPLUSISO", 
                    "BSMUMUTRACKPLUSISOTWO", 
                    "ISOTWOBODYQPLUS", 
                    "ISOTWOBODYMASSISOPLUS", 
                    "ISOTWOBODYCHI2ISOPLUS", 
                    "ISOTWOBODYISO5PLUS"
                ], 
                "angle": 0.27, 
                "doca_iso": 0.13, 
                "fc": 0.6, 
                "ips": 3.0, 
                "makeTrackCuts": False, 
                "pvdis": 0.5, 
                "pvdis_h": 40.0, 
                "svdis": -0.15, 
                "svdis_h": 30.0, 
                "tracktype": 3
            }, 
            {
                "Location": "VtxIsoInfo", 
                "Type": "RelInfoVertexIsolation"
            }, 
            {
                "Location": "ConeIsoInfo", 
                "Type": "RelInfoConeVariables"
            }, 
            {
                "Location": "VtxIsoInfoBDT", 
                "Type": "RelInfoVertexIsolationBDT"
            }, 
            {
                "DaughterLocations": {
                    "[Upsilon(1S) -> ^e+ [mu-]cc]CC": "Electron_TrackIsoBDT", 
                    "[Upsilon(1S) -> e+ ^[mu-]cc]CC": "Muon_TrackIsoBDT"
                }, 
                "Type": "RelInfoTrackIsolationBDT", 
                "Variables": 0
            }, 
            {
                "Location": "TrackIsolationBDT2", 
                "Type": "RelInfoTrackIsolationBDT2"
            }
        ], 
        "RelatedInfoTools_Upsilon2ee": [
            {
                "Location": "BSMUMUVARIABLES", 
                "Type": "RelInfoBs2MuMuBIsolations", 
                "Variables": [
                    "BSMUMUCDFISO", 
                    "BSMUMUOTHERBMAG", 
                    "BSMUMUOTHERBANGLE", 
                    "BSMUMUOTHERBBOOSTMAG", 
                    "BSMUMUOTHERBBOOSTANGLE", 
                    "BSMUMUOTHERBTRACKS"
                ], 
                "makeTrackCuts": False, 
                "tracktype": 3
            }, 
            {
                "DaughterLocations": {
                    "[Upsilon(1S) -> ^e+ [e-]cc]CC": "Electron1_ISO", 
                    "[Upsilon(1S) -> e+ ^[e-]cc]CC": "Electron2_ISO"
                }, 
                "IsoTwoBody": True, 
                "Type": "RelInfoBs2MuMuTrackIsolations", 
                "Variables": [
                    "BSMUMUTRACKPLUSISO", 
                    "BSMUMUTRACKPLUSISOTWO", 
                    "ISOTWOBODYQPLUS", 
                    "ISOTWOBODYMASSISOPLUS", 
                    "ISOTWOBODYCHI2ISOPLUS", 
                    "ISOTWOBODYISO5PLUS"
                ], 
                "angle": 0.27, 
                "doca_iso": 0.13, 
                "fc": 0.6, 
                "ips": 3.0, 
                "makeTrackCuts": False, 
                "pvdis": 0.5, 
                "pvdis_h": 40.0, 
                "svdis": -0.15, 
                "svdis_h": 30.0, 
                "tracktype": 3
            }, 
            {
                "Location": "VtxIsoInfo", 
                "Type": "RelInfoVertexIsolation"
            }, 
            {
                "Location": "ConeIsoInfo", 
                "Type": "RelInfoConeVariables"
            }, 
            {
                "Location": "VtxIsoInfoBDT", 
                "Type": "RelInfoVertexIsolationBDT"
            }, 
            {
                "DaughterLocations": {
                    "[Upsilon(1S) -> ^e+ [e-]cc]CC": "Electron1_TrackIsoBDT", 
                    "[Upsilon(1S) -> e+ ^[e-]cc]CC": "Electron2_TrackIsoBDT"
                }, 
                "Type": "RelInfoTrackIsolationBDT", 
                "Variables": 0
            }, 
            {
                "Location": "TrackIsolationBDT2", 
                "Type": "RelInfoTrackIsolationBDT2"
            }
        ], 
        "Tau2MuEtaPrimePrescale": 1, 
        "Tau2MuMuePrescale": 1, 
        "TauPrescale": 1, 
        "Upsilon2eMuPrescale": 1, 
        "Upsilon2eePrescale": 0.6, 
        "config_B2eMu": {
            "max_ADAMASS": 1200.0, 
            "max_AMAXDOCA": 0.3, 
            "max_BPVIPCHI2": 25.0, 
            "max_TRCHI2DV": 3.0, 
            "max_TRGHOSTPROB": 0.3, 
            "min_BPVDIRA": 0.0, 
            "min_BPVVDCHI2": 225.0, 
            "min_MIPCHI2DV": 25.0
        }, 
        "config_D2piphi": {
            "comb_cuts": "in_range(1300, AM, 2400) & (AMAXDOCA('') < .3*mm)", 
            "comb_cuts_prompt": "in_range(1300, AM, 2400) & (AMAXDOCA('') < .3*mm)", 
            "mother_cuts": "(VFASPF(VCHI2/VDOF) < 3) & (MM > 1300) & (MM < 2400)& (BPVDIRA > 0)", 
            "mother_cuts_prompt": "(VFASPF(VCHI2/VDOF) < 2.5) & (MM > 1300) & (MM < 2400)& (BPVDIRA > 0) & (BPVVD > 0.3)", 
            "pi_cuts": "(PT>250*MeV) & (TRCHI2DOF < 3.) & (TRGHOSTPROB<.3)", 
            "pi_cuts_prompt": "(PT>250*MeV) & (TRCHI2DOF < 2.5) & (TRGHOSTPROB<.2) & (MIPDV(PRIMARY)>0.025) & (MIPCHI2DV(PRIMARY) < 9)"
        }, 
        "config_JPsi2eMu": {
            "max_ADAMASS": 1000.0, 
            "max_AMAXDOCA": 0.3, 
            "max_JpsiMass": 4096.0, 
            "max_Phi2mumuMass": 1220.0, 
            "max_PhiMass": 2020.0, 
            "max_TRCHI2DV": 3, 
            "max_TRGHOSTPROB": 0.3, 
            "max_VtxChi2DoF": 2.5, 
            "max_VtxChi2DoF_phi": 3.0, 
            "min_BPVDIRA": 0, 
            "min_BPVVDCHI2": 324.0, 
            "min_BPVVDCHI2_phi": 144.0, 
            "min_JpsiMass": 2200.0, 
            "min_MIPCHI2DV": 36.0, 
            "min_MIPCHI2DV_phi": 25.0, 
            "min_Phi2mumuMass": 820.0, 
            "min_PhiMass": 300.0, 
            "min_ProbNN": 0.3, 
            "min_ProbNN_phi2ee": 0.05, 
            "min_ProbNN_phi_e": 0.3, 
            "min_ProbNN_phi_mu": 0.4, 
            "min_Pt": 300.0
        }, 
        "config_Phi2ll_forD": {
            "max_ADAMASS": 1000.0, 
            "max_AMAXDOCA": 0.3, 
            "max_Phi2mumuMass": 1220.0, 
            "max_PhiMass": 2020.0, 
            "max_TRCHI2DV": 3, 
            "max_TRGHOSTPROB": 0.3, 
            "max_VtxChi2DoF_phi": 3.0, 
            "min_BPVDIRA": 0, 
            "min_BPVVDCHI2_phi": 9.0, 
            "min_MIPCHI2DV_phi": 9.0, 
            "min_Phi2mumuMass": 820.0, 
            "min_PhiMass": 150.0, 
            "min_ProbNN": 0.3, 
            "min_ProbNN_phi2ee": 0.05, 
            "min_ProbNN_phi_e": 0.3, 
            "min_ProbNN_phi_mu": 0.4, 
            "min_Pt": 300.0
        }, 
        "config_PromptJPsi2eMu": {
            "max_AMAXDOCA": 0.3, 
            "max_BPVIPCHI2": 9.0, 
            "max_BPVVDCHI2": 9.0, 
            "max_JpsiMass": 4096.0, 
            "max_MIPCHI2DV": 9.0, 
            "max_Phi2mumuMass": 1220.0, 
            "max_PhiMass": 2020.0, 
            "max_SPD": 350, 
            "max_TRCHI2DV": 3, 
            "max_TRGHOSTPROB": 0.2, 
            "max_VtxChi2DoF": 2.5, 
            "max_VtxChi2DoF_phi": 3.0, 
            "min_JpsiMass": 2200.0, 
            "min_Phi2mumuMass": 820.0, 
            "min_PhiMass": 300.0, 
            "min_ProbNN": 0.8, 
            "min_ProbNN_Control": 0.65, 
            "min_ProbNN_phi": 0.8, 
            "min_ProbNN_phi2mumu": 0.4, 
            "min_Pt": 300.0
        }, 
        "config_Tau2MuEtaPrime": {
            "config_EtaPrime2pipigamma": {
                "etap_cuts": "(PT > 500*MeV) & (VFASPF(VCHI2/VDOF) < 6.0)", 
                "etap_mass_window": "(ADAMASS('eta') < 80*MeV) | (ADAMASS('eta_prime') < 80*MeV)", 
                "gamma_cuts": "(PT > 300*MeV) & (CL > 0.1)", 
                "piminus_cuts": "(PROBNNpi > 0.1) & (PT > 250*MeV) & (TRGHOSTPROB < 0.3) & (TRCHI2DOF < 3.0) & (MIPCHI2DV(PRIMARY) > 9.)", 
                "pipi_cuts": "(ACHI2DOCA(1,2)<16)", 
                "piplus_cuts": "(PROBNNpi > 0.1) & (PT > 250*MeV) & (TRGHOSTPROB < 0.3) & (TRCHI2DOF < 3.0) & (MIPCHI2DV(PRIMARY) > 9.)"
            }, 
            "config_EtaPrime2pipipi": {
                "etap_cuts": "(PT > 500*MeV) & (VFASPF(VCHI2/VDOF) < 6.0)", 
                "etap_mass_window": "(ADAMASS('eta') < 80*MeV) | (ADAMASS('eta_prime') < 80*MeV)", 
                "pi0_cuts": "(PT > 250*MeV)", 
                "piminus_cuts": "(PROBNNpi > 0.1) & (PT > 250*MeV) & (TRGHOSTPROB < 0.3) & (TRCHI2DOF < 3.0) & (MIPCHI2DV(PRIMARY) > 9.)", 
                "pipi_cuts": "ACHI2DOCA(1,2)<16", 
                "piplus_cuts": "(PROBNNpi > 0.1) & (PT > 250*MeV) & (TRGHOSTPROB < 0.3) & (TRCHI2DOF < 3.0) & (MIPCHI2DV(PRIMARY) > 9.)"
            }, 
            "muplus_cuts": "(ISLONG) & (TRCHI2DOF < 3 )  & (MIPCHI2DV(PRIMARY) >  9.) & (PT > 300*MeV) & (TRGHOSTPROB < 0.3)", 
            "tau_cuts": "(BPVIPCHI2()< 100) & (VFASPF(VCHI2/VDOF)<6.) & (BPVLTIME()*c_light > 50.*micrometer) & (BPVLTIME()*c_light < 400.*micrometer) & (PT>500*MeV) & (D2DVVD(2) < 80*micrometer)", 
            "tau_mass_window": "(ADAMASS('tau+')<150*MeV)"
        }, 
        "config_Upsilon2eMu": {
            "comb_cuts": "in_range(7250, AM, 11000) & ACUTDOCA(0.3*mm,'')", 
            "e_cuts": "(PT > 1000.*MeV) & (P > 8000.*MeV) & (TRCHI2DOF < 3.) & (PIDe > 1)", 
            "mu_cuts": "(PT > 1000.*MeV) & (P > 8000.*MeV) & (TRCHI2DOF < 3.) & (PIDmu > 0)", 
            "upsilon_cuts": "(VFASPF(VCHI2) < 25) & (BPVIPCHI2() < 9) & (BPVVDCHI2 < 25)"
        }, 
        "config_Upsilon2ee": {
            "comb_cuts": "in_range(6000, AM, 11000) & ACUTDOCA(0.3*mm,'')", 
            "e_cuts": "(PT > 1000.*MeV) & (P > 8000.*MeV) & (TRCHI2DOF < 3.) & (PIDe > 1)", 
            "upsilon_cuts": "(VFASPF(VCHI2) < 25) & (BPVIPCHI2() < 9) & (BPVVDCHI2 < 25)"
        }
    }, 
    "STREAMS": {
        "Leptonic": [
            "StrippingLFVTau2PhiMuLine", 
            "StrippingLFVTau2eMuMuLine", 
            "StrippingLFVB2eMuLine", 
            "StrippingLFVJPsi2eMuLine", 
            "StrippingLFVPromptJPsi2eMuLine", 
            "StrippingLFVJPsi2MuMuControlLine", 
            "StrippingLFVJPsi2eeControlLine", 
            "StrippingLFVPromptJPsi2MuMuControlLine", 
            "StrippingLFVPromptJPsi2eeControlLine", 
            "StrippingLFVPhi2eMuLine", 
            "StrippingLFVPromptPhi2eMuLine", 
            "StrippingLFVPhi2MuMuControlLine", 
            "StrippingLFVPhi2eeControlLine", 
            "StrippingLFVPromptPhi2MuMuControlLine", 
            "StrippingLFVPromptPhi2eeControlLine", 
            "StrippingLFVB2eeLine", 
            "StrippingLFVB2heMuLine", 
            "StrippingLFVB2hMuLine", 
            "StrippingLFVBu2KJPsieeLine", 
            "StrippingLFVB2hTauMuLine", 
            "StrippingLFVTau2MuEtaP2pipigLine", 
            "StrippingLFVTau2MuEtaP2pipipiLine", 
            "StrippingLFVupsilon2eMuLine", 
            "StrippingLFVupsilon2eeLine", 
            "StrippingLFVD2piphi2eeLine", 
            "StrippingLFVD2piphi2eMuLine", 
            "StrippingLFVD2piphi2MuMuLine", 
            "StrippingLFVD2piphi2eePromptLine", 
            "StrippingLFVD2piphi2eMuPromptLine", 
            "StrippingLFVD2piphi2MuMuPromptLine"
        ]
    }, 
    "WGs": [ "RD" ]
}

Lb2L0Gamma = {
    "BUILDERTYPE": "StrippingLb2L0GammaConf", 
    "CONFIG": {
        "Bd2KstGammaPrescale": 1.0, 
        "Bd2KstJpsiPrescale": 1.0, 
        "HLT1": [], 
        "HLT2": [], 
        "Kst_MassWindow": 100.0, 
        "L0": [
            "Photon", 
            "Electron", 
            "Hadron"
        ], 
        "L0_Conv": [], 
        "L0_Jpsi": [], 
        "Lambda0DD_MassWindow": 30.0, 
        "Lambda0LL_IP_Min": 0.05, 
        "Lambda0LL_MassWindow": 20.0, 
        "Lambda0_Pt_Min": 1000.0, 
        "Lambda0_VtxChi2_Max": 9.0, 
        "Lambdab_IPChi2_Max": 25.0, 
        "Lambdab_MTDOCAChi2_Max": 7.0, 
        "Lambdab_MassWindow": 1100.0, 
        "Lambdab_Pt_Min": 1000.0, 
        "Lambdab_SumPt_Min": 5000.0, 
        "Lambdab_VtxChi2_Max": 9.0, 
        "Lb2L0GammaConvertedPrescale": 1.0, 
        "Lb2L0GammaPrescale": 1.0, 
        "Lb2L0JpsiPrescale": 1.0, 
        "PhotonCnv_MM_Max": 100.0, 
        "PhotonCnv_PT_Min": 1000.0, 
        "PhotonCnv_VtxChi2_Max": 9.0, 
        "Photon_CL_Min": 0.2, 
        "Photon_PT_Min": 2500.0, 
        "Pion_P_Min": 2000.0, 
        "Pion_Pt_Min": 300.0, 
        "Proton_P_Min": 7000.0, 
        "Proton_Pt_Min": 800.0, 
        "TrackLL_IPChi2_Min": 16.0, 
        "Track_Chi2ndf_Max": 3.0, 
        "Track_GhostProb_Max": 0.4, 
        "Track_MinChi2ndf_Max": 2.0
    }, 
    "STREAMS": [ "Leptonic" ], 
    "WGs": [ "RD" ]
}

Lc23Mu = {
    "BUILDERTYPE": "Lc23MuLinesConf", 
    "CONFIG": {
        "CommonRelInfoTools": [
            {
                "Location": "VtxIsoInfo", 
                "Type": "RelInfoVertexIsolation"
            }, 
            {
                "Location": "VtxIsoInfoBDT", 
                "Type": "RelInfoVertexIsolationBDT"
            }, 
            {
                "Location": "BsMuMuBIsolation", 
                "Type": "RelInfoBs2MuMuBIsolations", 
                "Variables": [], 
                "makeTrackCuts": False, 
                "tracktype": 3
            }
        ], 
        "Lc23muPrescale": 1, 
        "Lc2mueePrescale": 1, 
        "Lc2pKpiPrescale": 0.01, 
        "Lc2pKpipi0MPrescale": 0.1, 
        "Lc2pKpipi0RPrescale": 0.01, 
        "Lc2peePrescale": 1, 
        "Lc2peepi0MPrescale": 1, 
        "Lc2peepi0RPrescale": 1, 
        "Lc2pemuPrescale": 1, 
        "Lc2pemupi0MPrescale": 1, 
        "Lc2pemupi0RPrescale": 1, 
        "Lc2pmumuPrescale": 1, 
        "Lc2pmumupi0MPrescale": 1, 
        "Lc2pmumupi0RPrescale": 1, 
        "MDSTflag": False, 
        "MaxDoca": 0.3, 
        "MaxDocaChi2": 10.0, 
        "MaxIPChi2": 100, 
        "MaxTrChi2Dof": 4.0, 
        "MaxTrGhp": 0.4, 
        "MaxVtxChi2": 15, 
        "MinBPVDira": 0.999, 
        "MinPhotonCL": 0.2, 
        "MinPi0M_PT": 500, 
        "MinPi0R_PT": 300, 
        "MinTrIPChi2": 9, 
        "MinTrPT": 300, 
        "MinVD": 70, 
        "Pi0M_DMASS": 60, 
        "Pi0R_DMASS": 25, 
        "Postscale": 1, 
        "Sigmac_AMDiff_MAX": 400, 
        "Sigmac_VCHI2VDOF_MAX": 30.0, 
        "SigmacppRefPi0MPrescale": 0.1, 
        "SigmacppRefPrescale": 0.01, 
        "SigmacppSignalPrescale": 1, 
        "SigmaczRefPi0MPrescale": 0.1, 
        "SigmaczRefPrescale": 0.01, 
        "SigmaczSignalPrescale": 1, 
        "mDiffLcLoose": 350, 
        "mDiffLcTight": 150, 
        "mDiffLcVeryLoose": 500
    }, 
    "STREAMS": [ "Leptonic" ], 
    "WGs": [ "RD" ]
}

StrangeHadrons = {
    "BUILDERTYPE": "StrangeHadronsConf", 
    "CONFIG": {
        "DiDOCA": 2, 
        "LambdaIPMin": 0.2, 
        "LambdaMassMax": 1516.0, 
        "LambdaMassWin": 500.0, 
        "LambdaMassWinTight": 30.0, 
        "LambdaMaxDOCA": 1.0, 
        "LambdaMaxIpChi2": 36.0, 
        "LambdaMinDIRA": 0.9, 
        "LambdaMinIpChi2": 16.0, 
        "LambdaMinPt": 500, 
        "LambdaMinTauPs": 9.0, 
        "LambdaPrimMinVDChi2": 50, 
        "LambdaSecMinVDChi2": 100, 
        "LambdaVtxChi2": 9.0, 
        "Omega-2Lambda0piPrescale": 1, 
        "OmegaMassMax": 1972.0, 
        "OmegaMassMin": 0.0, 
        "OmegaMassWin": 300.0, 
        "OmegaMinPt": 500.0, 
        "OmegaMinTauPs": 4.0, 
        "OmegaVtxChi2": 25.0, 
        "Postscale": 1, 
        "SigmaIPMin": 0.05, 
        "SigmaMassWin": 150.0, 
        "SigmaMaxDOCA": 2.0, 
        "SigmaMaxIpChi2": 36.0, 
        "SigmaMinDIRA": 0.9, 
        "SigmaMinPt": 500.0, 
        "SigmaMinTauPs": 6.0, 
        "SigmaVtxChi2": 36.0, 
        "TriDOCA": 3, 
        "Xi-2Sigma0munuPrescale": 1, 
        "Xi-2ppipiPrescale": 1, 
        "Xi02ppiPrescale": 1, 
        "XiIPMin": 0.05, 
        "XiIpChi2": 36.0, 
        "XiMassMax": 1620.0, 
        "XiMassMin": 0.0, 
        "XiMassWin": 300.0, 
        "XiMassWinTight": 50.0, 
        "XiMaxDOCA": 1, 
        "XiMinPt": 500.0, 
        "XiMinTauPs": 6.0, 
        "XiMinVDChi2": 30, 
        "XiVtxChi2": 25.0, 
        "electronMinIp": 1, 
        "electronMinIpChi2": 60.0, 
        "electronProbNNe": 0.3, 
        "muonMinIp": 1, 
        "muonMinIpChi2": 60.0, 
        "muonProbNNmu": 0.3, 
        "pi0MinPt": 500.0, 
        "pionMinIpChi2": 60.0, 
        "pionProbNNpi": 0.4, 
        "protonMinIpChi2": 16.0, 
        "protonProbNNp": 0.3, 
        "trchi2dof": 3, 
        "trghostprob": 0.2
    }, 
    "STREAMS": [ "Leptonic" ], 
    "WGs": [ "RD" ]
}

StrangeSL = {
    "BUILDERTYPE": "StrangeSLConf", 
    "CONFIG": {
        "DiDOCA": 2, 
        "K2PiPiMuNuPrescale": 1, 
        "KaonMassMax": 600.0, 
        "KaonMassMin": 200.0, 
        "KaonMassSeparation": 420.0, 
        "KaonMinDIRA": 0.98, 
        "KaonPlusLifetime": 1.610411922, 
        "KaonPlusMinVDChi2": 100.0, 
        "KaonRho": 4, 
        "KaonSVZ": 650, 
        "KaonVtxChi2": 25.0, 
        "Ks2PiMuNuHighMassPrescale": 1, 
        "Ks2PiMuNuLowMassPrescale": 1, 
        "Ks2PiMuNuPrescale": 1, 
        "KsDiDOCA": 0.2, 
        "KsIPChi2Min": 4, 
        "KsIPMin": 0.2, 
        "KsLifetime": 1.610411922, 
        "KsMaxIpDistRatio": 0.02, 
        "KsMinTauPs": 6.0, 
        "KsMinVDChi2": 100.0, 
        "KsP": 3000, 
        "KsVtxChi2": 6.0, 
        "L02PMuNuPrescale": 1, 
        "L02penuPrescale": 1, 
        "L02ppiPrescale": 1, 
        "LambdaIPMin": 0.2, 
        "LambdaMassMax": 1516.0, 
        "LambdaMassWin": 500.0, 
        "LambdaMassWinTight": 30.0, 
        "LambdaMaxDOCA": 1.0, 
        "LambdaMaxIpChi2": 36.0, 
        "LambdaMinDIRA": 0.9, 
        "LambdaMinIpChi2": 16.0, 
        "LambdaMinPt": 0.0, 
        "LambdaMinTauPs": 9.0, 
        "LambdaPLeptonNuMassMax": 1141.0, 
        "LambdaPLeptonNuMaxDOCA": 0.3, 
        "LambdaPrimMinVDChi2": 50, 
        "LambdaSecMinVDChi2": 100, 
        "LambdaVtxChi2": 9.0, 
        "Omega2Xi0PiPrescale": 1, 
        "Omega2XiStarmunuPrescale": 1, 
        "OmegaMassMax": 1972.0, 
        "OmegaMassMin": 0.0, 
        "OmegaMassWin": 300.0, 
        "OmegaMinPt": 500.0, 
        "OmegaMinTauPs": 4.0, 
        "OmegaVtxChi2": 25.0, 
        "Postscale": 1, 
        "SigmaIPMin": 0.05, 
        "SigmaMassWin": 150.0, 
        "SigmaMaxDOCA": 2.0, 
        "SigmaMaxIpChi2": 36.0, 
        "SigmaMinDIRA": 0.9, 
        "SigmaMinPt": 500.0, 
        "SigmaMinTauPs": 6.0, 
        "SigmaVtxChi2": 36.0, 
        "TriDOCA": 3, 
        "Xi02Lambdapi0Prescale": 1, 
        "Xi02SigmamunuPrescale": 1, 
        "Xi2L0MuNuPrescale": 1, 
        "Xi2LambdaPiPrescale": 1, 
        "XiIPMin": 0.05, 
        "XiIpChi2": 36.0, 
        "XiMassMax": 1620.0, 
        "XiMassMin": 0.0, 
        "XiMassWin": 300.0, 
        "XiMassWinTight": 50.0, 
        "XiMaxDOCA": 1, 
        "XiMinPt": 500.0, 
        "XiMinTauPs": 6.0, 
        "XiMinVDChi2": 30, 
        "XiStarMassMax": 1580.0, 
        "XiStarMassMin": 1480.0, 
        "XiStarMassWin": 50.0, 
        "XiStarMinTauPs": 0.0, 
        "XiStarVtxChi2": 1480.0, 
        "XiVtxChi2": 25.0, 
        "electronMinIp": 1, 
        "electronMinIpChi2": 60.0, 
        "electronProbNNe": 0.3, 
        "muonMinIp": 1, 
        "muonMinIpChi2": 60.0, 
        "muonProbNNmu": 0.3, 
        "pi0MinPt": 500.0, 
        "pionMinIpChi2": 60.0, 
        "pionProbNNpi": 0.4, 
        "protonMinIpChi2": 16.0, 
        "protonProbNNp": 0.3, 
        "trchi2dof": 3, 
        "trghostprob": 0.2
    }, 
    "STREAMS": [ "Leptonic" ], 
    "WGs": [ "RD" ]
}

Tau23Mu = {
    "BUILDERTYPE": "Tau23MuLinesConf", 
    "CONFIG": {
        "Ds23PiPrescale": 0.0, 
        "Ds23PiTISPrescale": 0.0, 
        "Ds2PhiPiPrescale": 1.0, 
        "Tau25Prescale": 1.0, 
        "Tau2PMuMuPrescale": 1.0, 
        "TauPostscale": 1.0, 
        "TauPrescale": 1.0, 
        "TrackGhostProb": 0.45
    }, 
    "STREAMS": {
        "Leptonic": [
            "StrippingTau23MuTau23MuLine", 
            "StrippingTau23MuDs2PhiPiLine", 
            "StrippingTau23MuTau2PMuMuLine", 
            "StrippingTau23MuDs23PiLine", 
            "StrippingTau23MuTau25MuLine"
        ]
    }, 
    "WGs": [ "RD" ]
}

