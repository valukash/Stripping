###############################################################################
# (c) Copyright 2019 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
A1MuMu = {
    "BUILDERTYPE": "A1MuMuConf", 
    "CONFIG": {
        "A1MuMu_LinePostscale": 1.0, 
        "A1MuMu_LinePrescale": 1.0, 
        "A1MuMu_checkPV": False, 
        "DIMUON_LOW_MASS": 5000.0, 
        "PT_DIMUON_MIN": 7500.0, 
        "PT_MUON_MIN": 2500.0, 
        "P_MUON_MIN": 2500.0, 
        "TRACKCHI2_MUON_MAX": 10, 
        "VCHI2_DIMUON_MAX": 12
    }, 
    "STREAMS": [ "Dimuon" ], 
    "WGs": [ "QEE" ]
}

A2MuMu = {
    "BUILDERTYPE": "A2MuMuConf", 
    "CONFIG": {
        "A2MuMu_Postscale": 1.0, 
        "A2MuMu_Prescale": 1.0, 
        "MMmax": 60000.0, 
        "MMmin": 12000.0, 
        "pT": 2500.0
    }, 
    "STREAMS": [ "EW" ], 
    "WGs": [ "QEE" ]
}

A2MuMuSameSign = {
    "BUILDERTYPE": "A2MuMuSameSignConf", 
    "CONFIG": {
        "A2MuMuSameSign_Postscale": 1.0, 
        "A2MuMuSameSign_Prescale": 1.0, 
        "MMmax": 60000.0, 
        "MMmin": 12000.0, 
        "pT": 2500.0
    }, 
    "STREAMS": [ "EW" ], 
    "WGs": [ "QEE" ]
}

B2LLPV0 = {
    "BUILDERTYPE": "B2LLPV0_sConf", 
    "CONFIG": {
        "2MixedElPrescale": 1.0, 
        "2MixedKPrescale": 1.0, 
        "2MixedMuPrescale": 1.0, 
        "2MixedPPrescale": 1.0, 
        "Multi_DELTAM": "100*MeV", 
        "Multi_DaughIPCHI2": 25, 
        "Multi_FDCHI2": 50, 
        "Multi_ProbNNK": 0.2, 
        "Multi_ProbNNe": 0.2, 
        "Multi_ProbNNmu": 0.0, 
        "Multi_ProbNNp": 0.2, 
        "Multi_SUMPT": "1000*MeV", 
        "Multi_VCHI2DOF": 25, 
        "Multi_combChi2": 15.0, 
        "Multi_daugsChi2dv": 50.0, 
        "Multi_daugsP": 2000.0, 
        "Multi_motherChi2dv": 25.0, 
        "Multi_motherChi2vx": 10.0
    }, 
    "STREAMS": [ "BhadronCompleteEvent" ], 
    "WGs": [ "QEE" ]
}

B2SS = {
    "BUILDERTYPE": "B2SSConf", 
    "CONFIG": {
        "B2KstSSElectronDetLinePostscale": 1, 
        "B2KstSSElectronDetLinePrescale": 1, 
        "B2KstSSElectronLinePostscale": 1, 
        "B2KstSSElectronLinePrescale": 1, 
        "B2KstSSKaonDetLinePostscale": 1, 
        "B2KstSSKaonDetLinePrescale": 1, 
        "B2KstSSKaonLinePostscale": 1, 
        "B2KstSSKaonLinePrescale": 1, 
        "B2KstSSMuonDetLinePostscale": 1, 
        "B2KstSSMuonDetLinePrescale": 1, 
        "B2KstSSMuonLinePostscale": 1, 
        "B2KstSSMuonLinePrescale": 1, 
        "B2KstSSPionDetLinePostscale": 1, 
        "B2KstSSPionDetLinePrescale": 1, 
        "B2KstSSPionLinePostscale": 1, 
        "B2KstSSPionLinePrescale": 1, 
        "B2KstSSProtonDetLinePostscale": 1, 
        "B2KstSSProtonDetLinePrescale": 1, 
        "B2KstSSProtonLinePostscale": 1, 
        "B2KstSSProtonLinePrescale": 1, 
        "Bs2SSElectronDetLinePostscale": 1, 
        "Bs2SSElectronDetLinePrescale": 1, 
        "Bs2SSElectronLinePostscale": 1, 
        "Bs2SSElectronLinePrescale": 1, 
        "Bs2SSKaonDetLinePostscale": 1, 
        "Bs2SSKaonDetLinePrescale": 1, 
        "Bs2SSKaonLinePostscale": 1, 
        "Bs2SSKaonLinePrescale": 1, 
        "Bs2SSPionDetLinePostscale": 1, 
        "Bs2SSPionDetLinePrescale": 1, 
        "Bs2SSPionLinePostscale": 1, 
        "Bs2SSPionLinePrescale": 1, 
        "Bs2SSProtonDetLinePostscale": 1, 
        "Bs2SSProtonDetLinePrescale": 1, 
        "Bs2SSProtonLinePostscale": 1, 
        "Bs2SSProtonLinePrescale": 1, 
        "Bu2KSSElectronDetLinePostscale": 1, 
        "Bu2KSSElectronDetLinePrescale": 1, 
        "Bu2KSSElectronLinePostscale": 1, 
        "Bu2KSSElectronLinePrescale": 1, 
        "Bu2KSSKaonDetLinePostscale": 1, 
        "Bu2KSSKaonDetLinePrescale": 1, 
        "Bu2KSSKaonLinePostscale": 1, 
        "Bu2KSSKaonLinePrescale": 1, 
        "Bu2KSSMuonDetLinePostscale": 1, 
        "Bu2KSSMuonDetLinePrescale": 1, 
        "Bu2KSSMuonLinePostscale": 1, 
        "Bu2KSSMuonLinePrescale": 1, 
        "Bu2KSSPionDetLinePostscale": 1, 
        "Bu2KSSPionDetLinePrescale": 1, 
        "Bu2KSSPionLinePostscale": 1, 
        "Bu2KSSPionLinePrescale": 1, 
        "Bu2KSSProtonDetLinePostscale": 1, 
        "Bu2KSSProtonDetLinePrescale": 1, 
        "Bu2KSSProtonLinePostscale": 1, 
        "Bu2KSSProtonLinePrescale": 1, 
        "DAUGHHADIPCHI2": 25, 
        "DAUGHHADPT": "250*MeV", 
        "DAUGHKAONPID": 0, 
        "DAUGHLEPIPCHI2": 15, 
        "DAUGHLEPPT": "25*MeV", 
        "DAUGHPIIPCHI2": 25, 
        "DAUGHPIPT": "500*MeV", 
        "DAUGHPROTONPID": 0, 
        "KSTARFDCHI2": 25, 
        "KSTARMASSWIN": "130*MeV", 
        "KSTARPT": "750*MeV", 
        "KSTARVCHI2DOF": 5, 
        "SDet_BDIRA": 0.0, 
        "SDet_BFDCHI2": 50, 
        "SDet_BIPCHI2": 15, 
        "SDet_BMASS_MAX": "6000*MeV", 
        "SDet_BMASS_MIN": "4600*MeV", 
        "SDet_BSUMPT": "2000*MeV", 
        "SDet_BVCHI2DOF": 5, 
        "SDet_PAIRFDCHI2": 25, 
        "SDet_PAIRIPCHI2": 25, 
        "SDet_PAIRLEFDCHI2": 15, 
        "SDet_PAIRLEIPCHI2": 10, 
        "SDet_PAIRLEMAXDOCA": "0.5*mm", 
        "SDet_PAIRLESUMPT": "50*MeV", 
        "SDet_PAIRLEVCHI2DOF": 16, 
        "SDet_PAIRMAXDOCA": "0.5*mm", 
        "SDet_PAIRPIFDCHI2": 25, 
        "SDet_PAIRPIIPCHI2": 25, 
        "SDet_PAIRPIMAXDOCA": "0.25*mm", 
        "SDet_PAIRPISUMPT": "2000*MeV", 
        "SDet_PAIRPIVCHI2DOF": 5, 
        "SDet_PAIRSUMPT": "1000*MeV", 
        "SDet_PAIRVCHI2DOF": 16, 
        "SDet_SDELTAM": "100*MeV", 
        "SPrompt_BDIRA": 0, 
        "SPrompt_BFDCHI2": 100, 
        "SPrompt_BIPCHI2": 15, 
        "SPrompt_BMASS_MAX": "6000*MeV", 
        "SPrompt_BMASS_MIN": "4600*MeV", 
        "SPrompt_BMAXDOCA": " 0.3*mm", 
        "SPrompt_BPIDIRA": 0, 
        "SPrompt_BPIFDCHI2": 125, 
        "SPrompt_BPIIPCHI2": 10, 
        "SPrompt_BPIMASS_MAX": "5800*MeV", 
        "SPrompt_BPIMASS_MIN": "4900*MeV", 
        "SPrompt_BPIMAXDOCA": " 0.2*mm", 
        "SPrompt_BPIVCHI2DOF": 3, 
        "SPrompt_BVCHI2DOF": 9, 
        "SPrompt_SDELTAM": "100*MeV", 
        "SPrompt_SDELTAMPI": "50*MeV"
    }, 
    "STREAMS": [ "EW" ], 
    "WGs": [ "QEE" ]
}

ConvertedPhoton = {
    "BUILDERTYPE": "ConvertedPhotonConf", 
    "CONFIG": {
        "HighPt": {
            "AddBrem": True, 
            "Hlt1Filter": None, 
            "Hlt2Filter": "HLT_PASS('Hlt2EWConvPhotonLLHighPtDecision') | HLT_PASS('Hlt2EWConvPhotonDDHighPtDecision')", 
            "MaxIPChi2": 200.0, 
            "MaxMass": 100.0, 
            "MaxPt": 100000.0, 
            "MinPIDe": 0.0, 
            "MinPt": 10000.0, 
            "Prescale": 0.25
        }, 
        "LowPt": {
            "AddBrem": True, 
            "Hlt1Filter": None, 
            "Hlt2Filter": "HLT_PASS('Hlt2EWConvPhotonLLHighPtDecision') | HLT_PASS('Hlt2EWConvPhotonDDHighPtDecision')", 
            "MaxIPChi2": 200.0, 
            "MaxMass": 100.0, 
            "MaxPt": 10000.0, 
            "MinPIDe": 0.0, 
            "MinPt": 5000.0, 
            "Prescale": 0.1
        }, 
        "odin": [
            "NoBeam", 
            "Beam1", 
            "Beam2", 
            "BeamCrossing"
        ]
    }, 
    "STREAMS": [ "EW" ], 
    "WGs": [ "QEE" ]
}

DY2MuMu = {
    "BUILDERTYPE": "DY2MuMuConf", 
    "CONFIG": {
        "DY1MaxMass": 5000.0, 
        "DY1MinMass": 3200.0, 
        "DY2MaxMass": 10000.0, 
        "DY2MinMass": 5000.0, 
        "DY2MuMu1HLT2": "HLT_PASS_RE( 'Hlt2(EW)?DiMuonDY[1-4]Decision' )", 
        "DY2MuMu1LineHltPrescale": 0.1, 
        "DY2MuMu1LinePrescale": 0.05, 
        "DY2MuMu2HLT2": "HLT_PASS_RE( 'Hlt2(EW)?DiMuonDY[1-4]Decision' )", 
        "DY2MuMu2LineHltPrescale": 0.5, 
        "DY2MuMu2LinePrescale": 0.25, 
        "DY2MuMu3LinePrescale": 1.0, 
        "DY2MuMu4LinePrescale": 1.0, 
        "DY2MuMuLinePostscale": 1.0, 
        "DY3MaxMass": 20000.0, 
        "DY3MinMass": 10000.0, 
        "DY4MaxMass": 40000.0, 
        "DY4MinMass": 20000.0, 
        "p": 10000.0, 
        "pT1": 1500.0, 
        "pT2": 3000.0, 
        "pid": -3.0, 
        "trkpchi2": 0.001
    }, 
    "STREAMS": [ "EW" ], 
    "WGs": [ "QEE" ]
}

DY2ee = {
    "BUILDERTYPE": "DY2eeConf", 
    "CONFIG": {
        "DY2eeLine3Prescale": 1.0, 
        "DY2eeLine4Prescale": 1.0, 
        "DY2eeLinePostscale": 1.0, 
        "DY3MaxMass": 15000.0, 
        "DY3MinMass": 10000.0, 
        "DY4MaxMass": 40000.0, 
        "DY4MinMass": 15000.0, 
        "ECalMin": 0.1, 
        "HCalMax": 0.05, 
        "PrsCalMin": 50.0, 
        "ePID": 1.0, 
        "p3": 10000.0, 
        "p4": 10000.0, 
        "pT3": 3000.0, 
        "pT4": 3000.0
    }, 
    "STREAMS": [ "EW" ], 
    "WGs": [ "QEE" ]
}

DisplJets = {
    "BUILDERTYPE": "DisplJetsConf", 
    "CONFIG": {
        "HLTCuts": "HLT_PASS('Hlt2JetsJetDisplacedSVDecision')|HLT_PASS('Hlt2JetsDiJetDisplacedSVSVLowPtDecision')", 
        "HltJetsLine_Prescale": 0.015, 
        "RequiredRawEvents": [
            "Calo", 
            "Trigger", 
            "Velo", 
            "Tracker"
        ]
    }, 
    "STREAMS": [ "BhadronCompleteEvent" ], 
    "WGs": [ "QEE" ]
}

DisplVertices = {
    "BUILDERTYPE": "DisplVerticesConf", 
    "CONFIG": {
        "DoubleSelection": {
            "ApplyMatterVeto": False, 
            "ApplyMatterVetoOne": True, 
            "MaxFractE1Track": 10.0, 
            "MaxFractTrwHitBefore": 10.0, 
            "MinHighestMass": 4500.0, 
            "MinMass": 4000.0, 
            "MinNumTracks": 4, 
            "MinRho": 0.4, 
            "MinSumPT": 3000.0, 
            "PreScale": 1.0, 
            "RawEvents": []
        }, 
        "FilterVelo": {
            "Apply": True, 
            "MinDOCABeamLine": -2.0, 
            "MinIP": 0.1, 
            "MinIPChi2": -1.0, 
            "MinNumTracks": 4, 
            "PVLocation": "Rec/Vertex/Primary", 
            "RemoveBackwardTracks": True, 
            "RemovePVTracks": False, 
            "RemoveVeloClones": True
        }, 
        "HLT": {
            "HLTPS": [
                [
                    [
                        "0x001c0028", 
                        "0x002f002c"
                    ], 
                    "HLT_PASS_RE('Hlt2DisplVerticesSinglePostScaledDecision')"
                ], 
                [
                    [
                        "0x00340032", 
                        "0x00730035"
                    ], 
                    "HLT_PASS_RE('Hlt2DisplVerticesSinglePostScaledDecision')"
                ], 
                [
                    [
                        "0x00750037", 
                        "0x007b0038"
                    ], 
                    "HLT_PASS_RE('Hlt2DisplVertices(Single|Double|SingleMV)PostScaledDecision')"
                ], 
                [
                    [
                        "0x007e0039", 
                        "0x0097003d"
                    ], 
                    "HLT_PASS_RE('Hlt2DisplVertices(Single|Double|SingleMV)PostScaledDecision')"
                ], 
                [
                    [
                        "0x00990042", 
                        "0x40000000"
                    ], 
                    "HLT_PASS_RE('Hlt2DisplVertices(Single|SingleLoose|Double)PSDecision')"
                ]
            ], 
            "SignalLines": [
                [
                    [
                        "0x001c0028", 
                        "0x002f002c"
                    ], 
                    [ "Hlt2DisplVerticesSingleDecision" ]
                ], 
                [
                    [
                        "0x00340032", 
                        "0x00730035"
                    ], 
                    [
                        "Hlt2DisplVerticesHighFDSingleDecision", 
                        "Hlt2DisplVerticesHighMassSingleDecision", 
                        "Hlt2DisplVerticesLowMassSingleDecision", 
                        "Hlt2DisplVerticesSingleDownDecision"
                    ]
                ], 
                [
                    [
                        "0x00750037", 
                        "0x007b0038"
                    ], 
                    [
                        "Hlt2DisplVerticesSingleDecision", 
                        "Hlt2DisplVerticesSingleDownDecision", 
                        "Hlt2DisplVerticesSingleHighFDPostScaledDecision", 
                        "Hlt2DisplVerticesSingleHighMassPostScaledDecision"
                    ]
                ], 
                [
                    [
                        "0x007e0039", 
                        "0x0097003d"
                    ], 
                    [
                        "Hlt2DisplVerticesSingleDecision", 
                        "Hlt2DisplVerticesSingleDownDecision", 
                        "Hlt2DisplVerticesSingleHighFDPostScaledDecision", 
                        "Hlt2DisplVerticesSingleHighMassPostScaledDecision"
                    ]
                ], 
                [
                    [
                        "0x00990042", 
                        "0x40000000"
                    ], 
                    [
                        "Hlt2DisplVerticesSingleDecision", 
                        "Hlt2DisplVerticesSingleDownDecision", 
                        "Hlt2DisplVerticesSingleHighFDDecision", 
                        "Hlt2DisplVerticesSingleHighMassDecision", 
                        "Hlt2DisplVerticesSingleVeryHighFDDecision"
                    ]
                ]
            ]
        }, 
        "HLTPS": {
            "PreScale": 0.2, 
            "RawEvents": []
        }, 
        "JetHltSingleHighMassSelection": {
            "ConeSize": 0.7, 
            "JetIDCut": None, 
            "MinDOCABL": -2.0, 
            "MinNJetMass": 0.0, 
            "MinNJetTransvMass": None, 
            "MinNumJets": 2, 
            "PreScale": 0.1, 
            "RawEvents": [], 
            "SingleJet": False
        }, 
        "JetHltSingleLowMassSelection": {
            "ConeSize": 1.2, 
            "JetIDCut": "( JNWITHPVINFO >= 5 ) & ( JMPT > 1800. ) & ( (PT/M) > 1.5 )", 
            "MinDOCABL": 0.1, 
            "MinNJetMass": 0.0, 
            "MinNJetTransvMass": None, 
            "MinNumJets": 1, 
            "PreScale": 0.1, 
            "RawEvents": [], 
            "SingleJet": True
        }, 
        "JetID": "( 0.8 > JMPF ) & ( 0.1 < JCPF ) & ( 900. < JMPT )", 
        "JetSingleHighMassSelection": {
            "ApplyMatterVeto": True, 
            "ConeSize": 0.7, 
            "JetIDCut": None, 
            "MaxFractE1Track": 0.8, 
            "MaxFractTrwHitBefore": 0.49, 
            "MinDOCABL": -2.0, 
            "MinMass": 5000.0, 
            "MinNJetMass": 0.0, 
            "MinNJetTransvMass": None, 
            "MinNumJets": 2, 
            "MinNumTracks": 5, 
            "MinRho": 0.4, 
            "MinSumPT": 7000.0, 
            "PreScale": 1.0, 
            "RawEvents": [], 
            "SingleJet": False
        }, 
        "JetSingleLowMassSelection": {
            "ApplyMatterVeto": True, 
            "ConeSize": 1.2, 
            "JetIDCut": "( JNWITHPVINFO >= 5 ) & ( JMPT > 1800. ) & ( (PT/M) > 2.5 )", 
            "MaxFractE1Track": 0.8, 
            "MaxFractTrwHitBefore": 0.49, 
            "MinDOCABL": 0.1, 
            "MinMass": 0.0, 
            "MinNJetMass": 0.0, 
            "MinNJetTransvMass": None, 
            "MinNumJets": 1, 
            "MinNumTracks": 5, 
            "MinRho": 0.4, 
            "MinSumPT": 10000.0, 
            "PreScale": 0.5, 
            "RawEvents": [], 
            "SingleJet": True
        }, 
        "RV2PDown": {
            "ApplyMatterVeto": False, 
            "ComputeMatterVeto": False, 
            "FirstPVMaxRho": 0.3, 
            "FirstPVMaxZ": 500.0, 
            "FirstPVMinNumTracks": 10, 
            "FirstPVMinZ": -300.0, 
            "MaxChi2NonVeloOnly": 5.0, 
            "MaxFractE1Track": 10.0, 
            "MaxFractTrwHitBefore": 10.0, 
            "MinMass": 3000.0, 
            "MinNumTracks": 4, 
            "MinRho": 2.0, 
            "MinSumPT": 0.0, 
            "UseVeloTracks": False
        }, 
        "RV2PWithVelo": {
            "ApplyMatterVeto": False, 
            "ComputeMatterVeto": True, 
            "FirstPVMaxRho": 0.3, 
            "FirstPVMaxZ": 500.0, 
            "FirstPVMinNumTracks": 10, 
            "FirstPVMinZ": -300.0, 
            "MaxChi2NonVeloOnly": 5.0, 
            "MaxFractE1Track": 10.0, 
            "MaxFractTrwHitBefore": 10.0, 
            "MinMass": 0.0, 
            "MinNumTracks": 4, 
            "MinRho": 0.4, 
            "MinSumPT": 0.0, 
            "UseVeloTracks": False
        }, 
        "SinglePSSelection": {
            "ApplyMatterVeto": False, 
            "MaxFractE1Track": 10.0, 
            "MaxFractTrwHitBefore": 10.0, 
            "MinMass": 3000.0, 
            "MinNumTracks": 5, 
            "MinRho": 0.5, 
            "MinSumPT": 0.0, 
            "PreScale": 0.005, 
            "RawEvents": []
        }, 
        "VeloGEC": {
            "Apply": True, 
            "MaxPhiVectorSize": 250.0, 
            "MaxVeloRatio": 0.1
        }
    }, 
    "STREAMS": [ "EW" ], 
    "WGs": [ "QEE" ]
}

Ditau = {
    "BUILDERTYPE": "DitauConf", 
    "CONFIG": {
        "CONSTRUCTORS": {
            "EX": {
                "RelatedInfoTools": [
                    {
                        "ConeAngle": 0.5, 
                        "DaughterLocations": {
                            "X0 ->  X  ^X+": "IsoMinus", 
                            "X0 -> ^X-  X+": "IsoPlus"
                        }, 
                        "Type": "RelInfoConeVariables"
                    }
                ], 
                "checkPV": True, 
                "prescale": 1.0
            }, 
            "EXnoiso": {
                "RelatedInfoTools": [
                    {
                        "ConeAngle": 0.5, 
                        "DaughterLocations": {
                            "X0 ->  X  ^X+": "IsoMinus", 
                            "X0 -> ^X-  X+": "IsoPlus"
                        }, 
                        "Type": "RelInfoConeVariables"
                    }
                ], 
                "checkPV": True, 
                "prescale": 0.02
            }, 
            "EXssnoiso": {
                "checkPV": True, 
                "prescale": 0.02
            }, 
            "HH": {
                "RelatedInfoTools": [
                    {
                        "ConeAngle": 0.5, 
                        "DaughterLocations": {
                            "X0 ->  X  ^X+": "IsoMinus", 
                            "X0 -> ^X-  X+": "IsoPlus"
                        }, 
                        "Type": "RelInfoConeVariables"
                    }
                ], 
                "checkPV": True, 
                "prescale": 1.0
            }, 
            "HHnoiso": {
                "RelatedInfoTools": [
                    {
                        "ConeAngle": 0.5, 
                        "DaughterLocations": {
                            "X0 ->  X  ^X+": "IsoMinus", 
                            "X0 -> ^X-  X+": "IsoPlus"
                        }, 
                        "Type": "RelInfoConeVariables"
                    }
                ], 
                "checkPV": True, 
                "prescale": 0.02
            }, 
            "HHssnoiso": {
                "checkPV": True, 
                "prescale": 0.02
            }, 
            "MX": {
                "RelatedInfoTools": [
                    {
                        "ConeAngle": 0.5, 
                        "DaughterLocations": {
                            "X0 ->  X  ^X+": "IsoMinus", 
                            "X0 -> ^X-  X+": "IsoPlus"
                        }, 
                        "Type": "RelInfoConeVariables"
                    }
                ], 
                "checkPV": True, 
                "prescale": 1.0
            }, 
            "MXnoiso": {
                "RelatedInfoTools": [
                    {
                        "ConeAngle": 0.5, 
                        "DaughterLocations": {
                            "X0 ->  X  ^X+": "IsoMinus", 
                            "X0 -> ^X-  X+": "IsoPlus"
                        }, 
                        "Type": "RelInfoConeVariables"
                    }
                ], 
                "checkPV": True, 
                "prescale": 0.05
            }, 
            "MXssnoiso": {
                "checkPV": True, 
                "prescale": 0.05
            }
        }, 
        "DITAU": {
            "ee": {
                "ccuts": {
                    "min_AM": 12000.0, 
                    "min_APTMAX": 9000.0
                }, 
                "dcuts": {
                    "e": {
                        "extracut": "ALL"
                    }
                }, 
                "mcuts": {
                    "extracut": "ALL"
                }
            }, 
            "eh1": {
                "ccuts": {
                    "extracut": "ATRUE", 
                    "min_AM": 12000.0, 
                    "min_APTMAX": 9000.0
                }, 
                "dcuts": {
                    "e": {
                        "extracut": "ALL"
                    }, 
                    "pi": {
                        "extracut": "ALL"
                    }
                }, 
                "mcuts": {
                    "extracut": "ALL"
                }
            }, 
            "eh3": {
                "ccuts": {
                    "min_AM": 12000.0, 
                    "min_APTMAX": 9000.0
                }, 
                "dcuts": {
                    "e": {
                        "extracut": "ALL"
                    }, 
                    "tau": {
                        "extracut": "ALL"
                    }
                }, 
                "mcuts": {
                    "extracut": "ALL"
                }
            }, 
            "emu": {
                "ccuts": {
                    "min_AM": 12000.0, 
                    "min_APTMAX": 9000.0
                }, 
                "dcuts": {
                    "e": {
                        "extracut": "ALL"
                    }, 
                    "mu": {
                        "extracut": "ALL"
                    }
                }, 
                "mcuts": {
                    "extracut": "ALL"
                }
            }, 
            "h1h1": {
                "ccuts": {
                    "min_AM": 16000.0, 
                    "min_APTMAX": 12000.0
                }, 
                "dcuts": {
                    "pi": {
                        "extracut": "ALL"
                    }
                }, 
                "mcuts": {
                    "extracut": "ALL"
                }
            }, 
            "h1h3": {
                "ccuts": {
                    "min_AM": 16000.0, 
                    "min_APTMAX": 12000.0
                }, 
                "dcuts": {
                    "pi": {
                        "extracut": "ALL"
                    }, 
                    "tau": {
                        "extracut": "ALL"
                    }
                }, 
                "mcuts": {
                    "extracut": "ALL"
                }
            }, 
            "h1mu": {
                "ccuts": {
                    "min_AM": 12000.0, 
                    "min_APTMAX": 9000.0
                }, 
                "dcuts": {
                    "mu": {
                        "extracut": "ALL"
                    }, 
                    "pi": {
                        "extracut": "ALL"
                    }
                }, 
                "mcuts": {
                    "extracut": "ALL"
                }
            }, 
            "h3h3": {
                "ccuts": {
                    "min_AM": 16000.0, 
                    "min_APTMAX": 12000.0
                }, 
                "dcuts": {
                    "tau": {
                        "extracut": "ALL"
                    }
                }, 
                "mcuts": {
                    "extracut": "ALL"
                }
            }, 
            "h3mu": {
                "ccuts": {
                    "min_AM": 12000.0, 
                    "min_APTMAX": 9000.0
                }, 
                "dcuts": {
                    "mu": {
                        "extracut": "ALL"
                    }, 
                    "tau": {
                        "extracut": "ALL"
                    }
                }, 
                "mcuts": {
                    "extracut": "ALL"
                }
            }, 
            "mumu": {
                "ccuts": {
                    "min_AM": 8000.0, 
                    "min_APTMAX": 4000.0
                }, 
                "dcuts": {
                    "mu": {
                        "extracut": "ALL"
                    }
                }, 
                "mcuts": {
                    "extracut": "ALL"
                }
            }
        }, 
        "PVRefitter": None, 
        "TES_e": "Phys/StdAllNoPIDsElectrons/Particles", 
        "TES_mu": "Phys/StdAllLooseMuons/Particles", 
        "TES_pi": "Phys/StdAllNoPIDsPions/Particles", 
        "ditau_pcomb": {
            "": "MomentumCombiner:PUBLIC"
        }, 
        "preambulo": "\n", 
        "tau_e": {
            "ISMUONLOOSE": False, 
            "InAccEcal": True, 
            "InAccHcal": True, 
            "extracut": "ALL", 
            "max_ETA": 4.5, 
            "max_HCALFrac": 0.05, 
            "max_TRCHI2DOF": 5, 
            "min_CaloPrsE": 50.0, 
            "min_ECALFrac": 0.05, 
            "min_ETA": 2.0, 
            "min_PT": 4000.0
        }, 
        "tau_h1": {
            "ISMUONLOOSE": False, 
            "InAccHcal": True, 
            "extracut": "ALL", 
            "max_ETA": 4.5, 
            "max_TRCHI2DOF": 5, 
            "min_ETA": 2.0, 
            "min_HCALFrac": 0.05, 
            "min_PT": 4000.0
        }, 
        "tau_h3": {
            "ccuts": {
                "max_AM": 1500.0, 
                "min_AM": 700.0, 
                "min_APTMAX": 2000.0
            }, 
            "dcuts": {
                "ISMUONLOOSE": False, 
                "InAccHcal": True, 
                "extracut": "ALL", 
                "max_ETA": 4.5, 
                "max_TRCHI2DOF": 5, 
                "min_ETA": 2.0, 
                "min_HCALFrac": 0.01, 
                "min_PT": 500.0
            }, 
            "mcuts": {
                "max_DRTRIOMAX": 0.3, 
                "max_VCHI2PDOF": 20.0, 
                "min_PT": 4000.0
            }
        }, 
        "tau_isolation": {
            "tau_e": 0.7, 
            "tau_h1": 0.8, 
            "tau_h3": 0.8, 
            "tau_mu": 0.8
        }, 
        "tau_mu": {
            "ISMUON": True, 
            "extracut": "ALL", 
            "max_ETA": 4.5, 
            "max_TRCHI2DOF": 5, 
            "min_ETA": 2.0, 
            "min_PT": 4000.0
        }
    }, 
    "STREAMS": [ "EW" ], 
    "WGs": [ "QEE" ]
}

Exotica = {
    "BUILDERTYPE": "ExoticaConf", 
    "CONFIG": {
        "Common": {
            "GhostProb": 0.3
        }, 
        "DiRHNu": {
            "PT": 0, 
            "VChi2": 10
        }, 
        "DisplDiE": {
            "EIPChi2": 4, 
            "EProbNNe": 0.1, 
            "FDChi2": 0, 
            "IPChi2": 32, 
            "MaxMM": 500.0, 
            "PT": 500.0, 
            "TisTosSpec": "Hlt1TrackMVA.*Decision"
        }, 
        "DisplDiEHighMass": {
            "EIPChi2": 4, 
            "EProbNNe": 0.1, 
            "FDChi2": 0, 
            "IPChi2": 32, 
            "MinMM": 500.0, 
            "PT": 500.0, 
            "TAU": 0.001, 
            "TisTosSpec": "Hlt1TrackMVA.*Decision"
        }, 
        "DisplDiELowMassFull": {
            "EIPChi2": 4, 
            "EProbNNe": 0.1, 
            "HLT1FILTER": "HLT_PASS_RE('Hlt1DiElectronLowMassDecision')", 
            "HLT2FILTER": None, 
            "L0FILTER": None, 
            "MMAX": 300.0, 
            "MMIN": 5.0, 
            "PT": 1000.0, 
            "TisTosSpec": None
        }, 
        "DisplDiELowMassMicro": {
            "EIPChi2": 4, 
            "EProbNNe": 0.1, 
            "HLT1FILTER": "HLT_PASS_RE('Hlt1DiElectronLowMassDecision')", 
            "HLT2FILTER": None, 
            "L0FILTER": None, 
            "MMAX": 300.0, 
            "MMIN": 5.0, 
            "PT": 1000.0, 
            "TisTosSpec": None
        }, 
        "DisplDiMuon": {
            "FDChi2": 4, 
            "IPChi2": 16, 
            "MaxMM": 500.0, 
            "MuIPChi2": 4, 
            "MuProbNNmu": 0.5, 
            "PT": 1000.0
        }, 
        "DisplDiMuonHighMass": {
            "FDChi2": 4, 
            "IPChi2": 16, 
            "MinMM": 500.0, 
            "MuIPChi2": 9, 
            "MuProbNNmu": 0.8, 
            "PT": 1000.0, 
            "TAU": 0.001
        }, 
        "DisplDiMuonNoPoint": {
            "FDChi2": 16, 
            "MSwitch": 500.0, 
            "MuIPChi2_highmass": 25, 
            "MuIPChi2_lowmass": 4, 
            "MuProbNNmu_highmass": 0.8, 
            "MuProbNNmu_lowmass": 0.5, 
            "PT": 1000.0, 
            "R": 0.0
        }, 
        "DisplDiMuonNoPointR": {
            "FDChi2": 16, 
            "MSwitch": 500.0, 
            "MuIPChi2_highmass": 25, 
            "MuIPChi2_lowmass": 4, 
            "MuProbNNmu_highmass": 0.8, 
            "MuProbNNmu_lowmass": 0.5, 
            "PT": 1000.0, 
            "R": 2.75
        }, 
        "DisplPhiPhi": {
            "FDChi2": 45, 
            "KIPChi2": 16, 
            "KPT": 500.0, 
            "KProbNNk": 0.1, 
            "PhiMassWindow": 20.0, 
            "PhiPT": 1000.0, 
            "TisTosSpec": "Hlt1IncPhi.*Decision", 
            "VChi2": 10, 
            "input": "Phys/StdLoosePhi2KK/Particles"
        }, 
        "EtaToDiEGammaFull": {
            "EEPT": 1000.0, 
            "EIPChi2": -1, 
            "EProbNNe": 0.1, 
            "GCL": 0.2, 
            "GPT": 500.0, 
            "HLT1FILTER": "HLT_PASS_RE('Hlt1DiElectronLowMass.*Decision')", 
            "HLT2FILTER": None, 
            "L0FILTER": None, 
            "MMAX": 700.0, 
            "MMIN": 400.0, 
            "PT": 1000.0, 
            "TisTosSpec": None, 
            "input": "Phys/StdLooseAllPhotons/Particles"
        }, 
        "EtaToDiEGammaMicro": {
            "EEPT": 1000.0, 
            "EIPChi2": -1, 
            "EProbNNe": 0.1, 
            "GCL": 0.2, 
            "GPT": 500.0, 
            "HLT1FILTER": "HLT_PASS_RE('Hlt1DiElectronLowMass.*Decision')", 
            "HLT2FILTER": None, 
            "L0FILTER": None, 
            "MMAX": 700.0, 
            "MMIN": 400.0, 
            "PT": 1000.0, 
            "TisTosSpec": None, 
            "input": "Phys/StdLooseAllPhotons/Particles"
        }, 
        "Pi0ToDiEGammaFull": {
            "EEPT": 1000.0, 
            "EIPChi2": -1, 
            "EProbNNe": 0.1, 
            "GCL": 0.2, 
            "GPT": 500.0, 
            "HLT1FILTER": "HLT_PASS_RE('Hlt1DiElectronLowMass.*Decision')", 
            "HLT2FILTER": None, 
            "L0FILTER": None, 
            "MMAX": 300.0, 
            "MMIN": 0.0, 
            "PT": 1000.0, 
            "TisTosSpec": None, 
            "input": "Phys/StdLooseAllPhotons/Particles"
        }, 
        "Pi0ToDiEGammaMicro": {
            "EEPT": 1000.0, 
            "EIPChi2": -1, 
            "EProbNNe": 0.1, 
            "GCL": 0.2, 
            "GPT": 500.0, 
            "HLT1FILTER": "HLT_PASS_RE('Hlt1DiElectronLowMass.*Decision')", 
            "HLT2FILTER": None, 
            "L0FILTER": None, 
            "MMAX": 300.0, 
            "MMIN": 0.0, 
            "PT": 1000.0, 
            "TisTosSpec": None, 
            "input": "Phys/StdLooseAllPhotons/Particles"
        }, 
        "Prescales": {
            "DiRHNu": 1.0, 
            "DisplDiE": 1.0, 
            "DisplDiEHighMass": 1.0, 
            "DisplDiELowMassFull": 0.1, 
            "DisplDiELowMassMicro": 1.0, 
            "DisplDiMuon": 1.0, 
            "DisplDiMuonHighMass": 1.0, 
            "DisplDiMuonNoPoint": 1.0, 
            "DisplDiMuonNoPointR": 1.0, 
            "DisplPhiPhi": 1.0, 
            "EtaToDiEGammaFull": 0.1, 
            "EtaToDiEGammaMicro": 1.0, 
            "Pi0ToDiEGammaFull": 0.1, 
            "Pi0ToDiEGammaMicro": 1.0, 
            "PrmptDiMuonHighMass": 0.0, 
            "QuadMuonNoIP": 1.0, 
            "RHNu": 0.15, 
            "RHNuHighMass": 1.0
        }, 
        "PrmptDiMuonHighMass": {
            "FDChi2": 45, 
            "M": 3200.0, 
            "M_switch_ab": 740.0, 
            "M_switch_bc": 1100.0, 
            "M_switch_cd": 3000.0, 
            "M_switch_de": 3200.0, 
            "M_switch_ef": 9000.0, 
            "MuIPChi2": 6, 
            "MuP": 10000.0, 
            "MuPT": 500.0, 
            "MuPTPROD": 1000000.0, 
            "MuProbNNmu_a": 0.8, 
            "MuProbNNmu_b": 0.8, 
            "MuProbNNmu_c": 0.95, 
            "MuProbNNmu_d": 2.0, 
            "MuProbNNmu_e": 0.95, 
            "MuProbNNmu_f": 0.9, 
            "PT": 1000.0
        }, 
        "QuadMuonNoIP": {
            "PT": 0, 
            "VChi2": 10
        }, 
        "RHNu": {
            "M": 0, 
            "TAU": 0.001
        }, 
        "RHNuHighMass": {
            "M": 5000.0, 
            "ProbNNmu": 0.5, 
            "TAU": 0.001
        }, 
        "SharedDiENoIP": {
            "DOCA": 0.5, 
            "EP": 3000.0, 
            "EPT": 500.0, 
            "EProbNNe": 0.1, 
            "MM": 0.0, 
            "VChi2": 25, 
            "input": "Phys/StdAllLooseElectrons/Particles"
        }, 
        "SharedDiMuonNoIP": {
            "DOCA": 0.5, 
            "MuP": 10000.0, 
            "MuPT": 500.0, 
            "MuProbNNmu": 0.5, 
            "VChi2": 10, 
            "input": "Phys/StdAllLooseMuons/Particles"
        }, 
        "SharedRHNu": {
            "FDChi2": 45, 
            "IPChi2": 16, 
            "M": 0, 
            "P": 10000.0, 
            "PT": 500.0, 
            "ProbNNmu": 0.5, 
            "TAU": 0.001, 
            "VChi2": 10, 
            "XIPChi2": 16, 
            "input": [
                "Phys/StdAllLooseMuons/Particles", 
                "Phys/StdNoPIDsPions/Particles"
            ]
        }
    }, 
    "STREAMS": {
        "Dimuon": [
            "StrippingExoticaDisplDiMuonLine", 
            "StrippingExoticaDisplDiMuonHighMassLine", 
            "StrippingExoticaDisplDiMuonNoPointLine", 
            "StrippingExoticaDisplDiMuonNoPointRLine"
        ], 
        "EW": [
            "StrippingExoticaDisplPhiPhiLine", 
            "StrippingExoticaRHNuLine", 
            "StrippingExoticaRHNuHighMassLine", 
            "StrippingExoticaDiRHNuLine", 
            "StrippingExoticaPrmptDiMuonHighMassLine", 
            "StrippingExoticaQuadMuonNoIPLine", 
            "StrippingExoticaDisplDiELine", 
            "StrippingExoticaDisplDiEHighMassLine", 
            "StrippingExoticaDisplDiELowMassFullLine", 
            "StrippingExoticaPi0ToDiEGammaFullLine", 
            "StrippingExoticaEtaToDiEGammaFullLine"
        ], 
        "Leptonic": [
            "StrippingExoticaDisplDiELowMassMicroLine", 
            "StrippingExoticaPi0ToDiEGammaMicroLine", 
            "StrippingExoticaEtaToDiEGammaMicroLine"
        ]
    }, 
    "WGs": [ "QEE" ]
}

FullDiJets = {
    "BUILDERTYPE": "FullDiJetsConf", 
    "CONFIG": {
        "FullDiJetsLine_Postscale": 1.0, 
        "FullDiJetsLine_Prescale": 0.013, 
        "RequiredRawEvents": [], 
        "TOS_HLT2": None, 
        "min_jet_pT": 20000.0
    }, 
    "STREAMS": [ "EW" ], 
    "WGs": [ "QEE" ]
}

GluinosJets = {
    "BUILDERTYPE": "GluinosJetsConf", 
    "CONFIG": {
        "DG_NDISPLONG": 1, 
        "DisplacedGluinoPrescale": 1.0, 
        "HF_GDAUGS": 5, 
        "HF_NDISPLONG": 3, 
        "JET_PT": 20000.0, 
        "MIN_IPCHI2": 25, 
        "RawBanks": [
            "Calo", 
            "Trigger", 
            "Velo", 
            "Tracker"
        ], 
        "SixLightPrescale": 1.0, 
        "TwoLightOneHFPrescale": 1.0
    }, 
    "STREAMS": [ "BhadronCompleteEvent" ], 
    "WGs": [ "QEE" ]
}

H24Mu = {
    "BUILDERTYPE": "H24MuConf", 
    "CONFIG": {
        "A1Dira": 0, 
        "A1Doca": 0.2, 
        "A1DocaTight": 0.1, 
        "A1Doca_loose": 10.0, 
        "A1FDChi2": 4, 
        "A1Vchi2": 7.5, 
        "A1Vchi2Tight": 1, 
        "A1Vchi2_loose": 20, 
        "A1maxIPchi2": 25, 
        "A1maxMass": 2000.0, 
        "A1maxMass_loose": 5000.0, 
        "DefaultPostscale": 1, 
        "DetachedLinePrescale": 1, 
        "HVchi2": 10, 
        "HVchi2Tight": 2, 
        "HVchi2_loose": 50, 
        "HmaxDOCA": 0.75, 
        "HmaxDOCATight": 0.25, 
        "HmaxDOCA_loose": 1000000.0, 
        "HpT": 1200.0, 
        "HpT_loose": 300.0, 
        "LooseLinePrescale": 0.01, 
        "MDSTFlag": False, 
        "MuGhostProb": 0.4, 
        "MuMaxIPchi2": 3, 
        "MuMaxIPchi2_loose": 1000000, 
        "MuMinIPchi2": 1, 
        "MuNShared": 3, 
        "MuPIDdll": -3, 
        "MuTrackChi2DoF": 3, 
        "MuTrackChi2DoF_loose": 10, 
        "MupT_loose": 0.0, 
        "MupTdetached": 250.0, 
        "MupTprompt": 500.0, 
        "PromptLinePrescale": 1, 
        "RequiredRawEvents": [ "Muon" ], 
        "SimpleLinePrescale": 1
    }, 
    "STREAMS": [ "Leptonic" ], 
    "WGs": [ "QEE" ]
}

HighPtMuons = {
    "BUILDERTYPE": "HighPtMuonsConf", 
    "CONFIG": {
        "MZ0": 40000.0, 
        "MmaxY": 11000.0, 
        "MminY": 8500.0, 
        "Postscale": 1.0, 
        "Prescale": 1.0, 
        "RawEvents": [
            "Muon", 
            "Calo", 
            "Rich", 
            "Velo", 
            "Tracker", 
            "HC"
        ], 
        "TrackChi2Mu": 5, 
        "VtxChi2MuMu": 12, 
        "YPIDcut": 0.5, 
        "pT": 3000.0
    }, 
    "STREAMS": {
        "EW": [ "StrippingHighPtMuonsZ02MuMuNoPIDsLine" ], 
        "Leptonic": [ "StrippingHighPtMuonsY2MuMuNoPIDsLine" ]
    }, 
    "WGs": [ "QEE" ]
}

HltQEEExotica = {
    "BUILDERTYPE": "HltQEEConf", 
    "CONFIG": {
        "common": {
            "checkPV": False
        }, 
        "lines": {
            "DisplDiE": {
                "HLT2": "HLT_PASS('Hlt2ExoticaDisplDiEDecision')", 
                "prescale": 0.0
            }, 
            "DisplDiMuon": {
                "HLT2": "HLT_PASS('Hlt2ExoticaDisplDiMuonDecision')", 
                "prescale": 0.0
            }, 
            "DisplDiMuonNoPoint": {
                "HLT2": "HLT_PASS('Hlt2ExoticaDisplDiMuonNoPointDecision')", 
                "prescale": 0.0
            }, 
            "DisplPhiPhi": {
                "HLT2": "HLT_PASS('Hlt2ExoticaDisplPhiPhiDecision')", 
                "prescale": 0.0
            }, 
            "LFVPrmpt": {
                "HLT2": "HLT_PASS('Hlt2ExoticaLFVPrmptDecision')", 
                "prescale": 0.0
            }, 
            "QuadMuonNoIP": {
                "HLT2": "HLT_PASS('Hlt2ExoticaQuadMuonNoIPDecision')", 
                "prescale": 0.0
            }, 
            "RHNu": {
                "HLT2": "HLT_PASS('Hlt2ExoticaRHNuDecision')", 
                "prescale": 0.0
            }
        }
    }, 
    "STREAMS": [ "EW" ], 
    "WGs": [ "QEE" ]
}

HltQEEJets = {
    "BUILDERTYPE": "HltQEEConf", 
    "CONFIG": {
        "common": {
            "RequiredRawEvents": [ "Calo" ], 
            "checkPV": False
        }, 
        "lines": {
            "DiJetMuMu": {
                "HLT2": "HLT_PASS('Hlt2JetsDiJetMuMuDecision')", 
                "prescale": 1.0
            }, 
            "DiJetMuMuLowPt": {
                "HLT2": "HLT_PASS('Hlt2JetsDiJetMuMuLowPtDecision')", 
                "prescale": 1.0
            }, 
            "DiJetSV": {
                "HLT2": "HLT_PASS('Hlt2JetsDiJetSVDecision')", 
                "prescale": 1.0
            }, 
            "DiJetSVHighPt": {
                "HLT2": "HLT_PASS('Hlt2JetsDiJetSVHighPtDecision')", 
                "prescale": 1.0
            }, 
            "DiJetSVLowPt": {
                "HLT2": "HLT_PASS('Hlt2JetsDiJetSVLowPtDecision')", 
                "prescale": 1.0
            }, 
            "DiJetSVMu": {
                "HLT2": "HLT_PASS('Hlt2JetsDiJetSVMuDecision')", 
                "prescale": 1.0
            }, 
            "DiJetSVMuLowPt": {
                "HLT2": "HLT_PASS('Hlt2JetsDiJetSVMuLowPtDecision')", 
                "prescale": 1.0
            }, 
            "DiJetSVSV": {
                "HLT2": "HLT_PASS('Hlt2JetsDiJetSVSVDecision')", 
                "prescale": 1.0
            }, 
            "DiJetSVSVLowPt": {
                "HLT2": "HLT_PASS('Hlt2JetsDiJetSVSVLowPtDecision')", 
                "prescale": 1.0
            }, 
            "JetLowPt": {
                "HLT2": "HLT_PASS('Hlt2JetsJetLowPtDecision')", 
                "prescale": 1.0
            }, 
            "JetSV": {
                "HLT2": "HLT_PASS('Hlt2JetsJetSVDecision')", 
                "prescale": 1.0
            }
        }
    }, 
    "STREAMS": [ "BhadronCompleteEvent" ], 
    "WGs": [ "QEE" ]
}

InclQQ = {
    "BUILDERTYPE": "InclQQConf", 
    "CONFIG": {
        "DZSVPVCut": 1.0, 
        "HLT2": "HLT_PASS_RE('Hlt2.*Topo.*Decision')", 
        "NrPVsCut": 1, 
        "NrSeedsCut": 2, 
        "PrtIPSCut": 2.5, 
        "PrtMomCut": 2.0, 
        "PrtPtCut": 0.6, 
        "SumMomSVCut": 1.0, 
        "TrkChi2Cut": 3.0, 
        "VertexFitter": "LoKi::VertexFitter:PUBLIC", 
        "VtxChi2Cut": 20.0, 
        "scale": 0.032
    }, 
    "STREAMS": [ "EW" ], 
    "WGs": [ "QEE" ]
}

LFVExotica = {
    "BUILDERTYPE": "LFVExoticaConf", 
    "CONFIG": {
        "Common": {
            "checkPV": False
        }, 
        "StrippingLFVExoticaDetachedLine": {
            "Detached_FDChi2": 45, 
            "Detached_GhostProb": 0.3, 
            "Detached_IPChi2": 16, 
            "Detached_M": 0, 
            "Detached_P": 10000.0, 
            "Detached_PT": 500.0, 
            "Detached_ProbNNe": 0.0, 
            "Detached_ProbNNmu": 0.5, 
            "Detached_TAU": 0.001, 
            "Detached_VChi2": 10, 
            "Detached_XIPChi2": 16, 
            "Postscale": 1.0, 
            "Postscale_SS": 1.0, 
            "Prescale": 1.0, 
            "Prescale_SS": 1.0
        }, 
        "StrippingLFVExoticaEMuXDetachedLine": {
            "Detached_FDChi2": 225, 
            "Detached_GhostProb": 0.2, 
            "Detached_IPChi2": 25, 
            "Detached_M": 0, 
            "Detached_P": 10000.0, 
            "Detached_PT": 1600.0, 
            "Detached_ProbNNe": 0.0, 
            "Detached_ProbNNmu": 0.5, 
            "Detached_TAU": 0.0005, 
            "Detached_VChi2": 8, 
            "Postscale": 1.0, 
            "Postscale_SS": 1.0, 
            "Prescale": 1.0, 
            "Prescale_SS": 1.0, 
            "max_TRCHI2DV": 3.0
        }, 
        "StrippingLFVExoticaPromptLine": {
            "Postscale": 1.0, 
            "Postscale_SS": 1.0, 
            "Prescale": 1.0, 
            "Prescale_SS": 0.5, 
            "Prompt_FDChi2": 1, 
            "Prompt_GhostProb": 0.3, 
            "Prompt_IPChi2": 1, 
            "Prompt_M": 0, 
            "Prompt_P": 10000.0, 
            "Prompt_PT": 500.0, 
            "Prompt_ProbNNe": 0.25, 
            "Prompt_ProbNNmu": 0.5, 
            "Prompt_VChi2": 5, 
            "Prompt_XIPChi2": 1
        }, 
        "StrippingLFVExoticaW2EN2EMuXDetachedLine": {
            "Detached_FDChi2": 225, 
            "Detached_GhostProb": 0.2, 
            "Detached_IPChi2": 16, 
            "Detached_M": 0, 
            "Detached_P": 10000.0, 
            "Detached_PT": 500.0, 
            "Detached_ProbNNe": 0.0, 
            "Detached_ProbNNmu": 0.5, 
            "Detached_TAU": 0.0005, 
            "Detached_VChi2": 8, 
            "Postscale": 1.0, 
            "Postscale_SS": 1.0, 
            "Prescale": 1.0, 
            "Prescale_SS": 1.0, 
            "Prompt_GhostProb": 0.3, 
            "Prompt_IPChi2": 5, 
            "Prompt_P": 10000.0, 
            "Prompt_PT": 2000.0, 
            "Prompt_ProbNN": 0.0, 
            "W_M": 0.0, 
            "max_TRCHI2DV": 3.0
        }, 
        "StrippingLFVExoticaW2MuN2EMuXDetachedLine": {
            "Detached_FDChi2": 225, 
            "Detached_GhostProb": 0.2, 
            "Detached_IPChi2": 16, 
            "Detached_M": 0, 
            "Detached_P": 10000.0, 
            "Detached_PT": 500.0, 
            "Detached_ProbNNe": 0.0, 
            "Detached_ProbNNmu": 0.5, 
            "Detached_TAU": 0.0005, 
            "Detached_VChi2": 8, 
            "Postscale": 1.0, 
            "Postscale_SS": 1.0, 
            "Prescale": 1.0, 
            "Prescale_SS": 1.0, 
            "Prompt_GhostProb": 0.3, 
            "Prompt_IPChi2": 5, 
            "Prompt_P": 10000.0, 
            "Prompt_PT": 1000.0, 
            "Prompt_ProbNN": 0.5, 
            "W_M": 0.0, 
            "max_TRCHI2DV": 3.0
        }
    }, 
    "STREAMS": [ "Leptonic" ], 
    "WGs": [ "QEE" ]
}

LLP2Jets = {
    "BUILDERTYPE": "LLP2Jets_sConf", 
    "CONFIG": {
        "BSRHODISP": 0.6, 
        "D0DISP": 0.9, 
        "DoublePrescale": 1.0, 
        "JET_PT_HM": 10000.0, 
        "JET_PT_LM": 15000.0, 
        "JET_PT_NOVX": 10000.0, 
        "MAXJETS": 10000000000.0, 
        "MINDAUGS_LM": 3, 
        "MINGD_HM": 9, 
        "MINGD_LM": 7, 
        "MINJETS": 2, 
        "MIN_IPCHI2": 25, 
        "NDISPLONG_HM": 5, 
        "NDISPLONG_LM": 5, 
        "NDISPLONG_NOVX": 4, 
        "RawBanks": [
            "Calo", 
            "Trigger", 
            "Velo", 
            "Tracker"
        ], 
        "SingleHighMassPrescale": 1.0, 
        "SingleLowMassPrescale": 1.0, 
        "SinglePrescale": 0.01, 
        "gtrk": "ISBASIC & HASTRACK & ISLONG"
    }, 
    "STREAMS": [ "BhadronCompleteEvent" ], 
    "WGs": [ "QEE" ]
}

LLP2MuX = {
    "BUILDERTYPE": "LLP2MuXConf", 
    "CONFIG": {
        "MinIP": 0.25, 
        "MinPT": 12000.0
    }, 
    "STREAMS": [ "EW" ], 
    "WGs": [ "QEE" ]
}

LLPV0 = {
    "BUILDERTYPE": "LLPV0_sConf", 
    "CONFIG": {
        "2HHDoubleKaonPrescale": 1.0, 
        "2HHMultiPrescale": 1.0, 
        "2HHSingleKaonPrescale": 0.035, 
        "2HHSinglePrescale": 0.0015, 
        "2MixedPrescale": 1.0, 
        "2mumuSinglePrescale": 1.0, 
        "combChi2": 25.0, 
        "daugsChi2dv": 25.0, 
        "daugsP": 2000.0, 
        "dihHHMulti": 6, 
        "dihHHSingle": 1, 
        "dihMixed": 2, 
        "dihMuons": 0, 
        "dihProbNNk": 0.0, 
        "dihProbNNmu": 0.0, 
        "dihhalt": 100, 
        "dikHHDoubleKaon": 2, 
        "dikHHSingleKaon": 1, 
        "dikProbNNk": 0.2, 
        "dimuMixed": 1, 
        "dimuMuonsSingle": 1, 
        "dimuProbNNk": 0.0, 
        "dimuProbNNmu": 0.5, 
        "mixedProbNNk": 0.0, 
        "motherChi2dv": 10.0, 
        "motherChi2vx": 16.0
    }, 
    "STREAMS": [ "BhadronCompleteEvent" ], 
    "WGs": [ "QEE" ]
}

Lb2dp = {
    "BUILDERTYPE": "Lb2dpConf", 
    "CONFIG": {
        "KaonP": 20000, 
        "KaonPT": 500, 
        "KaonP_pipi": 30000, 
        "LbDIRA": 0.9999, 
        "LbFDChi2": 81, 
        "LbFDChi2_pipi": 150, 
        "LbIPChi2_pipi": 25, 
        "LbMassMax": 7000.0, 
        "LbMassMin": 5000.0, 
        "LbPT": 1500, 
        "LbVtxChi2": 20.0, 
        "LbVtxChi2_pipi": 20.0, 
        "PionP": 1500, 
        "PionPIDKpi": 2, 
        "PionPT": 500, 
        "Prescale": 1.0, 
        "Prescale_pipi": 1.0, 
        "ProtonP": 15000, 
        "ProtonPIDpK": 10, 
        "ProtonPIDppi": 10, 
        "ProtonPT": 500, 
        "SumPT": 1000, 
        "TrackChi2Ndof": 4.0, 
        "TrackGhostProb": 0.4, 
        "TrackIPChi2": 16.0, 
        "TrackIPChi2_pipi": 16.0
    }, 
    "STREAMS": [ "BhadronCompleteEvent" ], 
    "WGs": [ "QEE" ]
}

MicroDiJets = {
    "BUILDERTYPE": "MicroDiJetsConf", 
    "CONFIG": {
        "MDSTFlag": False, 
        "MicroDiJetsLine_Postscale": 1.0, 
        "MicroDiJetsLine_Prescale": 0.1, 
        "RequiredRawEvents": [ "Calo" ], 
        "TOS_HLT2": None, 
        "min_jet_pT": 20000.0
    }, 
    "STREAMS": [ "Leptonic" ], 
    "WGs": [ "QEE" ]
}

MuMuSS = {
    "BUILDERTYPE": "MuMuSSConf", 
    "CONFIG": {
        "MuMuSSLine1MaxMass": 5000.0, 
        "MuMuSSLine1MinMass": 3200.0, 
        "MuMuSSLine1Prescale": 0.1, 
        "MuMuSSLine2MaxMass": 10000.0, 
        "MuMuSSLine2MinMass": 5000.0, 
        "MuMuSSLine2Prescale": 1.0, 
        "MuMuSSLine3MaxMass": 16000.0, 
        "MuMuSSLine3MinMass": 10000.0, 
        "MuMuSSLine3Prescale": 1.0, 
        "MuMuSSLine4MinMass": 16000.0, 
        "MuMuSSLine4Prescale": 1.0, 
        "p": 10000.0, 
        "pT1": 1500.0, 
        "pT2": 3000.0
    }, 
    "STREAMS": [ "EW" ], 
    "WGs": [ "QEE" ]
}

SbarSCorrelations = {
    "BUILDERTYPE": "SbarSCorrelationsConf", 
    "CONFIG": {
        "F2prescale": 1.0, 
        "Fisher": 10, 
        "HLT": "HLT_PASS_RE('Hlt1.*NoBias.*Decision')|HLT_PASS_RE('Hlt1.*MB.*Bias.*Decision')|HLT_PASS_RE('Hlt1.*MicroBias.*Decision')", 
        "KAON_PIDK_MIN": 8, 
        "KAON_PIDKp_MIN": 0, 
        "KAON_ipChi2_MAX": 49, 
        "LambdaCprescale": 1.0, 
        "Lambda_Adamass": 50.0, 
        "Lambda_V_Chi2_Max": 9, 
        "Lambda_ipChi2_MAX": 49, 
        "LongTrackGEC": 1000, 
        "PION_P_MIN": 2000.0, 
        "PION_ipChi2_MIN": 9, 
        "PROTON_P_MIN": 2000.0, 
        "PROTON_ipChi2_MIN": 9, 
        "Phiprescale": 0.05, 
        "Trk_P_MIN": 5000.0, 
        "isLong": "(ISLONG)", 
        "postscale": 1.0
    }, 
    "STREAMS": [ "EW" ], 
    "WGs": [ "QEE" ]
}

SingleTrackTIS = {
    "BUILDERTYPE": "SingleTrackTISConf", 
    "CONFIG": {
        "SingleTrackTISLow_Prescale": 0.01, 
        "SingleTrackTIS_Postscale": 1.0, 
        "SingleTrackTIS_Prescale": 0.1, 
        "pT": 20000.0, 
        "pTlow": 15000.0
    }, 
    "STREAMS": [ "EW" ], 
    "WGs": [ "QEE" ]
}

StrangeBaryons = {
    "BUILDERTYPE": "StrippingStrangeBaryonsConf", 
    "CONFIG": {
        "CHI2VTX_L": 15.0, 
        "CHI2VTX_Omega": 9.0, 
        "CHI2VTX_Xi": 25.0, 
        "COS_L_Omega": 0.9996, 
        "COS_L_Xi": 0.9996, 
        "HLT1": "HLT_PASS_RE('Hlt1.*NoBias.*Decision')|HLT_PASS_RE('Hlt1.*MB.*Bias.*Decision')|HLT_PASS_RE('Hlt1.*MicroBias.*Decision')", 
        "L_FDCHI2_OWNPV": 100.0, 
        "L_FDCHI2_OWNPV_LL": 150.0, 
        "L_FDCHI2_OWNPV_LL_Omega": 70.0, 
        "L_FDCHI2_OWNPV_Omega": 70.0, 
        "Lambda0MassWindow": 30.0, 
        "Lambda0MassWindowPost": 6.0, 
        "OmegaMassWindow": 50.0, 
        "Omega_FDCHI2_OWNPV": 10.0, 
        "PionPIDpiK": 0.0, 
        "PreScale": 1, 
        "ProtonPIDppi": -5.0, 
        "TRCHI2DOF": 4.0, 
        "XiMassWindow": 50.0, 
        "Xi_FDCHI2_OWNPV": 5.0, 
        "Xi_FDCHI2_OWNPV_DDL": 15.0, 
        "Xi_FDCHI2_OWNPV_LLL": 30.0, 
        "checkPV": True, 
        "minCHI2IPPV_Bachelor": 3.0, 
        "minCHI2IPPV_K_Bachelor_D": 3.0, 
        "minCHI2IPPV_K_Bachelor_L": 3.0, 
        "minCHI2IPPV_L": 2.0, 
        "minCHI2IPPV_L_LL": 9.0, 
        "minCHI2IPPV_Pi_Bachelor_DDD": 4.0, 
        "minCHI2IPPV_Pi_Bachelor_LLL": 10.0, 
        "minCHI2IPPV_pPi": 4.0, 
        "minCHI2IPPV_pPi_LL": 20.0
    }, 
    "STREAMS": [ "EW" ], 
    "WGs": [ "QEE" ]
}

StrangeBaryonsNoPID = {
    "BUILDERTYPE": "StrippingStrangeBaryonsNoPIDConf", 
    "CONFIG": {
        "CHI2VTX_L": 15.0, 
        "CHI2VTX_Omega": 9.0, 
        "CHI2VTX_Xi": 25.0, 
        "COS_L_Omega": 0.9996, 
        "COS_L_Xi": 0.9996, 
        "HLT1": "HLT_PASS_RE('Hlt1.*NoBias.*Decision')|HLT_PASS_RE('Hlt1.*MB.*Bias.*Decision')|HLT_PASS_RE('Hlt1.*MicroBias.*Decision')", 
        "L_FDCHI2_OWNPV": 100.0, 
        "L_FDCHI2_OWNPV_LL": 150.0, 
        "L_FDCHI2_OWNPV_LL_Omega": 70.0, 
        "L_FDCHI2_OWNPV_Omega": 70.0, 
        "Lambda0MassWindow": 30.0, 
        "Lambda0MassWindowPost": 6.0, 
        "OmegaMassWindow": 50.0, 
        "Omega_FDCHI2_OWNPV": 10.0, 
        "PionPIDpiK": 0.0, 
        "PreScale": 1, 
        "ProtonPIDppi": -5.0, 
        "TRCHI2DOF": 4.0, 
        "XiMassWindow": 50.0, 
        "Xi_FDCHI2_OWNPV": 5.0, 
        "Xi_FDCHI2_OWNPV_DDL": 15.0, 
        "Xi_FDCHI2_OWNPV_LLL": 30.0, 
        "checkPV": False, 
        "minCHI2IPPV_Bachelor": 3.0, 
        "minCHI2IPPV_K_Bachelor_D": 3.0, 
        "minCHI2IPPV_K_Bachelor_L": 3.0, 
        "minCHI2IPPV_L": 2.0, 
        "minCHI2IPPV_L_LL": 9.0, 
        "minCHI2IPPV_Pi_Bachelor_DDD": 4.0, 
        "minCHI2IPPV_Pi_Bachelor_LLL": 10.0, 
        "minCHI2IPPV_pPi": 4.0, 
        "minCHI2IPPV_pPi_LL": 20.0
    }, 
    "STREAMS": [ "EW" ], 
    "WGs": [ "QEE" ]
}

TaggedJets = {
    "BUILDERTYPE": "TaggedJetsConf", 
    "CONFIG": {
        "DiTaggedJetsPair_Postscale": 1.0, 
        "DiTaggedJetsPair_Prescale": 1.0, 
        "TaggedJetsPairExclusiveDiJet_Postscale": 1.0, 
        "TaggedJetsPairExclusiveDiJet_Prescale": 1.0, 
        "TaggedJetsPair_Postscale": 1.0, 
        "TaggedJetsPair_Prescale": 1.0, 
        "min_jet_pT": 30000.0, 
        "min_jet_pT_ExclusiveDiJet": 20000.0
    }, 
    "STREAMS": [ "BhadronCompleteEvent" ], 
    "WGs": [ "QEE" ]
}

WMu = {
    "BUILDERTYPE": "WMuConf", 
    "CONFIG": {
        "HLT1_SingleTrackNoBias": "HLT_PASS( 'Hlt1MBNoBiasDecision' )", 
        "HLT2_Control10": "HLT_PASS_RE('Hlt2(EW)?SingleMuon(V)?High.*')", 
        "HLT2_Control4800": "HLT_PASS_RE('Hlt2(EW)?SingleMuonLow.*')", 
        "HLT2_NoMuID": "HLT_PASS( 'Hlt2EWSingleMuonHighPtNoMUIDDecision' )", 
        "IsoMax": 4000.0, 
        "MinIP": 0.12, 
        "MinpT_HighIP": 15000.0, 
        "MinpTiso": 15000.0, 
        "RawEvents": [
            "Muon", 
            "Calo", 
            "Rich", 
            "Velo", 
            "Tracker", 
            "HC"
        ], 
        "STNB_Prescale": 0.2, 
        "SingMuon10_Prescale": 0.01, 
        "SingMuon10_pT": 10000.0, 
        "SingMuon48_Prescale": 0.4, 
        "SingMuon48_pT": 4800.0, 
        "WMuHighIP_Postscale": 1.0, 
        "WMuHighIP_Prescale": 1.0, 
        "WMuIso_Postscale": 1.0, 
        "WMuIso_Prescale": 1.0, 
        "WMuLow_Prescale": 0.1, 
        "WMuNoMuID_Postcale": 1.0, 
        "WMuNoMuID_Prescale": 1.0, 
        "WMu_Postscale": 1.0, 
        "WMu_Prescale": 1.0, 
        "pT": 20000.0, 
        "pTlow": 15000.0, 
        "pTvlow": 5000.0
    }, 
    "STREAMS": [ "EW" ], 
    "WGs": [ "QEE" ]
}

WRareDecay = {
    "BUILDERTYPE": "WRareDecayConf", 
    "CONFIG": {
        "BPVLTIME": 0.0002, 
        "BuMassWin": 400.0, 
        "DpMassWin": 150.0, 
        "DsMassWin": 150.0, 
        "KSLTIME": 0.0005, 
        "KSMassWin": 40.0, 
        "KstMassWin": 120.0, 
        "MesonPT": 13000.0, 
        "RawEvents": [], 
        "RhoMassWin": 150.0, 
        "TrChi2": 5.0, 
        "TrGhost": 0.4, 
        "TrIPChi2": 9.0, 
        "W2BuGammaPostScale": 1.0, 
        "W2BuGammaPreScale": 1.0, 
        "W2DpGammaPostScale": 1.0, 
        "W2DpGammaPreScale": 1.0, 
        "W2DsGammaPostScale": 1.0, 
        "W2DsGammaPreScale": 1.0, 
        "W2KaonGammaPostScale": 1.0, 
        "W2KaonGammaPreScale": 1.0, 
        "W2KstPGammaPostScale": 1.0, 
        "W2KstPGammaPreScale": 1.0, 
        "W2PionGammaPostScale": 1.0, 
        "W2PionGammaPreScale": 1.0, 
        "W2RhoGammaPostScale": 1.0, 
        "W2RhoGammaPreScale": 1.0, 
        "WMassWin": 50000.0, 
        "kaonPT": 400.0, 
        "photonHighPT": 10000.0, 
        "photonPT": 2500.0, 
        "pion0PT": 800.0
    }, 
    "STREAMS": [ "EW" ], 
    "WGs": [ "QEE" ]
}

We = {
    "BUILDERTYPE": "WeConf", 
    "CONFIG": {
        "ECalMin": 0.1, 
        "HCalMax": 0.05, 
        "PrsCalMin": 50.0, 
        "RawEvents": [
            "Muon", 
            "Calo", 
            "Rich", 
            "Velo", 
            "Tracker", 
            "HC"
        ], 
        "WeLow_Prescale": 0.1, 
        "We_Postscale": 1.0, 
        "We_Prescale": 1.0, 
        "pT": 20000.0, 
        "pTlow": 15000.0
    }, 
    "STREAMS": [ "EW" ], 
    "WGs": [ "QEE" ]
}

WeJets = {
    "BUILDERTYPE": "WJetsConf", 
    "CONFIG": {
        "RequiredRawEvents": [], 
        "TOS_Jet": "Hlt2Topo.*Decision", 
        "WJetsTagged_Prescale": 1.0, 
        "WJets_Prescale": 1.0, 
        "config_W": {
            "ECalMin": 0.1, 
            "HCalMax": 0.05, 
            "PrsCalMin": 50.0, 
            "TOS": "Hlt2EWSingleElectronVHighPtDecision", 
            "max_e_pT": 200000000.0, 
            "min_e_pT": 10000.0
        }, 
        "dr_lepton_jet": 0.5, 
        "min_jet_pT": 15000.0
    }, 
    "STREAMS": [ "BhadronCompleteEvent" ], 
    "WGs": [ "QEE" ]
}

WmuJets = {
    "BUILDERTYPE": "WJetsConf", 
    "CONFIG": {
        "RequiredRawEvents": [], 
        "TOS_Jet": "Hlt2Topo.*Decision", 
        "WJetsTagged_Prescale": 1.0, 
        "WJets_Prescale": 1.0, 
        "config_W": {
            "TOS": "Hlt2EWSingleMuonVHighPtDecision", 
            "max_mu_pT": 200000000.0, 
            "min_mu_pT": 10000.0
        }, 
        "dr_lepton_jet": 0.5, 
        "min_jet_pT": 15000.0
    }, 
    "STREAMS": [ "BhadronCompleteEvent" ], 
    "WGs": [ "QEE" ]
}

Z02MuMu = {
    "BUILDERTYPE": "Z02MuMuConf", 
    "CONFIG": {
        "MMmin": 40000.0, 
        "Postscale": 1.0, 
        "Prescale": 1.0, 
        "RawEvents": [
            "Muon", 
            "Calo", 
            "Rich", 
            "Velo", 
            "Tracker", 
            "HC"
        ], 
        "pT": 3000.0
    }, 
    "STREAMS": [ "EW" ], 
    "WGs": [ "QEE" ]
}

Z02ee = {
    "BUILDERTYPE": "Z02eeConf", 
    "CONFIG": {
        "ECalMin": 0.1, 
        "HCalMax": 0.05, 
        "MMmin": 40000.0, 
        "Postscale": 1.0, 
        "Prescale": 1.0, 
        "PrsCalMin": 50.0, 
        "RawEvents": [ "HC" ], 
        "pT": 10000.0
    }, 
    "STREAMS": [ "EW" ], 
    "WGs": [ "QEE" ]
}

Z0RareDecay = {
    "BUILDERTYPE": "Z0RareDecayConf", 
    "CONFIG": {
        "D0MassWin": 100.0, 
        "JpsiMassMax": 3200.0, 
        "JpsiMassMin": 3000.0, 
        "KstMassWin": 120.0, 
        "MesonPT": 13000.0, 
        "MuonP": -8000.0, 
        "MuonPT": 650.0, 
        "OmegaMassWin": 230.0, 
        "PhiMassWin": 200.0, 
        "Pi0Pi0PT": 10000.0, 
        "RawEvents": [], 
        "RhoMassWin": 230.0, 
        "TrChi2": 5.0, 
        "UpsilonMassMin": 8500.0, 
        "VtxChi2": 20.0, 
        "Z2D0GammaPostScale": 1.0, 
        "Z2D0GammaPreScale": 1.0, 
        "Z2GammaGammaPostScale": 1.0, 
        "Z2GammaGammaPreScale": 1.0, 
        "Z2KstGammaPostScale": 1.0, 
        "Z2KstGammaPreScale": 1.0, 
        "Z2OmegaGammaPostScale": 1.0, 
        "Z2OmegaGammaPreScale": 1.0, 
        "Z2PhiGammaPostScale": 1.0, 
        "Z2PhiGammaPreScale": 1.0, 
        "Z2Pi0GammaPostScale": 1.0, 
        "Z2Pi0GammaPreScale": 1.0, 
        "Z2Pi0Pi0PostScale": 1.0, 
        "Z2Pi0Pi0PreScale": 1.0, 
        "Z2QONGammaPostScale": 1.0, 
        "Z2QONGammaPreScale": 1.0, 
        "Z2RhoGammaPostScale": 1.0, 
        "Z2RhoGammaPreScale": 1.0, 
        "ZMassWin": 60000.0, 
        "photonPT": 2500.0, 
        "pion0PT": 860.0
    }, 
    "STREAMS": [ "EW" ], 
    "WGs": [ "QEE" ]
}

