#!/usr/bin/env python
###############################################################################
# (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
# =============================================================================
# =============================================================================
## @file  CommonParticles/StandardBasicNeutral.py
#  configuration file for 'Standard Basic Neutral' 
#  @author Vanya BELYAEV Ivan.Belyaev@nikhef.nl
#  @date 2009-01-14
# =============================================================================
"""
Configuration file for 'Standard Basic Neutral' particles 
"""
__author__  = "Vanya BELYAEV Ivan.Belyaev@nikhef.nl"
# =============================================================================

_locations = {} 

from CommonParticles.StandardPhotons import *
_locations.update ( locations )

from CommonParticles.StandardPi0 import *
_locations.update ( locations )

from CommonParticles.StandardEta import *
_locations.update ( locations )

from CommonParticles.StdLooseDiElectron import *
_locations.update ( locations )

# redefine the locations 
locations = _locations

## ============================================================================
if '__main__' == __name__ :

    print __doc__
    print __author__
    from CommonParticles.Utils import locationsDoD
    print locationsDoD ( locations )
    
# =============================================================================
# The END 
# =============================================================================


