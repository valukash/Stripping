Before creating a merge request, please consider the following guidelines:

- the master branch is intended to be used for Upgrade specific MRs,
- bug fixes for 2018 data-taking should be committed as MRs to 2018-patches,
- any other changes for Run1 and Run2 analysis should go to run2-patches.

We will propagate those changes to master if relevant for the upgrade or run2-patches if appropriate.

Please contact the Stripping managers if you are unsure which target branch to choose. 

For new campaigns, we will inform you of the dedicated branches.
